<?php

// Initialisierung der Session.
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("include/mysqlconnect.inc.php");
include ("include/variablen.php");
include ("include/funktionen.php");

// logout in Datenbank schreiben

$outdate = date("Ymd");
$outtime = date("H:i");
$sessionid = session_id();

$sql = "UPDATE logins SET outdate = '$outdate', outtime = '$outtime' WHERE session_id = '$sessionid'";
//$abfrage = mysqli_query($db, $sql);
$abfrage = myqueryi($db, $sql);

// löschen der Session.
session_destroy(); 


?>

<!DOCTYPE >
<html lang="de">
<head>
<title>Ausgeloggt!</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/preisagentur.css" rel="stylesheet" type="text/css" />
    <style>
        #loginform, #logfailform, #logoutform, #logfailform, #loggedinform {
            background: #ddd;
            border: 1px solid #666;
            font-size: 1.4em;
            color: #333;
            position: absolute;
            width: 400px;
            height: 260px;
            top: 50%;
            left: 50%;
            margin-top: -130px;
            margin-left: -200px;
            padding: 20px;
            border-radius: 10px;
        }

        #logfailform {
            text-align: center;
            border: 8px solid green;
        }

        #logoutform {
            text-align: center;
            border: 8px solid red;
        }

        #logfailform, #loggedinform {
            text-align: center;
            border: 8px solid orange;
        }

        .innerdiv {
            background-color: white;
            height: 100%;
        }

        input[type="text"], input[type="password"] {
            font-size: 1.3em; padding: 5px 12px;
            font-family: Arial, sans-serif;
            font-weight: 300;
            color: teal;
            border: 1px solid silver;
            background-image: linear-gradient(to top, gainsboro 0%, white 90%);
            border-radius: 5px;
            margin-left: 1em;
        }

        input[type="submit"].login {
            font-size: 1.3em; padding: 5px 12px;
            font-family: Arial, sans-serif;
            font-weight: 300;
            color: teal;
            border: 1px solid silver;
            background-image: linear-gradient(to top, gainsboro 0%, white 90%);
            border-radius: 10px;
        }

        label { display: inline-block; width: 7em; }

        .submitlogin {
            margin-left: 9em;
            margin-top: 1em;
        }

        .inputlogin {
            margin-bottom: 1em;
        }

        #einloggen {
            margin-top: 2em;
        }

        #logfailform h3, #logfailform h3 {
            margin-top: 0;
            line-height: 150%;
        }

        .back {
            width: 80%;
            margin: 0 auto;
            border: 1px solid silver;
            background-image: linear-gradient(to top, gainsboro 0%, white 90%);
            border-radius: 10px;
        }

        .back a{
            font-size: 2em;
            padding: 5px 12px;
            font-family: Arial, sans-serif;
            font-weight: 300;
            color: teal;
        }

        .back a:hover{
            font-size: 2em;
            padding: 5px 12px;
            font-family: Arial, sans-serif;
            font-weight: 300;
            color: orange;
        }
    </style>
</head>

<body>


<div id="logfailform">
	<div class="innerdiv">
  <h3>Sie sind ausgeloggt.<br>Auf Wiedersehen</h3>
  <div class="back"><a href="cc4pa.php">zurück zum Login</a></div>
  </div>

  

</div> <!-- ende logoutform -->

</body>
</html>
