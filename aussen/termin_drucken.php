<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();


$aussendienst = $_SESSION['benutzer_kurz'];
$aussendienst_name = $_SESSION['benutzer_name'];
$aussendienst_vorname = $_SESSION['benutzer_vorname'];
if ($aussendienst_vorname === 'NULL') {
	$aussendienst_vorname = "";
}
$datum = quote_smart($_GET["t_id"]);
$datum = substr($datum, 2, 10);
list($tag, $monat, $jahr) = explode(".", $datum);
$aktuell = $jahr . "-" . $monat . "-" . $tag;

// Daten Kunden

$sql = "SELECT branche AS Branche, name AS Name, vorname AS Vorname, plz AS PLZ, ort AS Ort, Ortsteil AS Ortsteil, strasse AS Strasse, ";
$sql .= "vorwahl1 AS Vorwahl, telefon AS Telefon, termin AS Dat, zeit AS Zeit, rem_termin AS Bemerkung ";
//$sql .= "ges_alt AS Ges_alt, tarif_alt AS Tarif ";
$sql .= "FROM branche, kunden, vorname, name, termin, plz, ort, ortsteil, poo, vorwahl1 ";
//, produkt_alt, tarif_alt, ges_alt ";
$sql .= "WHERE termin.aussendienst = '$aussendienst' AND termin.termin = '$aktuell' AND termin.sperrzeit = '0' ";
$sql .= "And kunden.kunden_id = termin.kd_id AND kunden.vorname_id = vorname.vorname_id AND kunden.name_id = name.name_id ";
$sql .= "And kunden.branche_id = branche.branche_id And kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id AND termin.alt = '0'";
$sql .= "ORDER BY ZEIT ";

$abfrage = myqueryi($db, $sql);

$anzahl = mysqli_num_rows($abfrage);
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>T drucken</title>
	<!-- aussen/termin_drucken.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/drucken.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "left">
<table border="0" width= "1100" cellpadding="2" cellspacing="2">
<tr>
<td align ="left" valign="top"><?php echo "Termine am: $datum&nbsp;&nbsp;erstellt am:" . date("d.m.Y H:i") . "&nbsp;&nbsp;&nbsp; <span style=\"font-weight:bold;\">Kunden-Termine</span>&nbsp;&nbsp;&nbsp;
					      Name Kundenbetreuer: $aussendienst_vorname&nbsp;$aussendienst_name&nbsp;&nbsp;&nbsp;&nbsp;Datensätze: $anzahl"; ?><td>
</tr>
<tr><td><hr></td></tr>
<tr><td valign = "top">

<?php
$span = mysqli_num_fields($abfrage);

//Ausgabe
echo "<table width=\"100%\">";
	echo "<tr>";													// Tabellenkopf
	
	echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">Branche</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">Name</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">Vorname</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"40\"><span style = \"font-size: 11pt;\">PLZ</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">Ort</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">Ortsteil</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">Straße</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"70\"><span style = \"font-size: 11pt;\">Vorw.</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">Telefon</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"40\"><span style = \"font-size: 11pt;\">Dat.</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"40\"><span style = \"font-size: 11pt;\">Zeit</span></td>";
	echo "<td align =\"left\" valign=\"top\" width = \"300\"><span style = \"font-size: 11pt;\">Bemerkung</span></td>";
	
	
	
	
	
	
	
	/*
	for($i = 0; $i < mysqli_num_fields($abfrage); $i++)				// Anzahl der Tabellenzellen pro Zeile
	{
		$feldname = mysqli_field_name($abfrage, $i);				// Name der Tabellenzelle
		if ($i != 11) {
			echo "<th align = \"left\" valign=\"top\" width = \"80\">$feldname</th>";
		}
		else {
			echo "<th align = \"left\" valign=\"top\" width = \"300\">$feldname</th>";
		}
		
		//echo "<th align = \"left\" valign=\"top\" width = \"80\">$feldname</th>";
		
		
	}
	*/
	echo "</tr>\n";													// Ende Tabellenkopf
	echo "<tr><td colspan = \"$span\"><hr></td></tr>";
													
													
													
	for ($i = 0; $i < $anzahl; $i++) {
	
		$daten = mysqli_fetch_array($abfrage);
		
		echo "<tr>";
		if ($daten[0] === "NULL") {
				$daten[0] = "&nbsp;";
		}
		echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">" . $daten[0] . "</span></td>";
		echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">" . $daten[1] . "</span></td>";
		echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">" . $daten[2] . "</span></td>";
		echo "<td align =\"left\" valign=\"top\" width = \"40\"><span style = \"font-size: 11pt;\">" . $daten[3] . "</span></td>";
		echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">" . $daten[4] . "</span></td>";
		if ($daten[5] === "NULL") {
				$daten[5] = "&nbsp;";
		}
		echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">" . $daten[5] . "</span></td>";
		echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">" . $daten[6] . "</span></td>";
		echo "<td align =\"left\" valign=\"top\" width = \"70\"><span style = \"font-size: 11pt;\">" . $daten[7] . "</span></td>";
		echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">" . $daten[8] . "</span></td>";
		list($t_jahr, $t_monat, $t_tag) = explode("-", $daten[9]);
		echo "<td align =\"left\" valign=\"top\" width = \"40\"><span style = \"font-size: 11pt;\">" . $t_tag . ".<br>" . $t_monat . "." . "</span></td>";
		echo "<td align =\"left\" valign=\"top\" width = \"40\"><span style = \"font-size: 11pt;\">" . $daten[10] . "</span></td>";
		if ($daten[11] === "NULL") {
				$daten[11] = "&nbsp;";
		}
		echo "<td align =\"left\" valign=\"top\" width = \"300\"><span style = \"font-size: 11pt;\">" . $daten[11] . "</span></td>";
		echo "</tr>";
		echo "<tr><td colspan = \"$span\"><hr></td></tr>";
	}
													
	/*											
	while($zeile = mysqli_fetch_row($abfrage))						// Schleife f�r Daten-Zeilen
	{
		echo "<tr>";
		
		for($i = 0; $i < mysqli_num_fields($abfrage); $i++)			// Schleife f�r Felder
		{
			if ($zeile[$i] === "NULL") {
				$zeile[$i] = "&nbsp;";
			}
		
			if ($i == 9) {
				list($t_jahr, $t_monat, $t_tag) = explode("-", $zeile[$i]);
				echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">" . $t_tag . ".<br>" . $t_monat . "." . "</span></td>";
			}
			if ($i == 11) {
				echo "<td align =\"left\" valign=\"top\" width = \"300\"><span style = \"font-size: 11pt;\">" . $zeile[$i] . "</span></td>";
				
			}
			
			else {
				echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 11pt;\">" . $zeile[$i] . "</span></td>";
			}
		
			
		}				
	echo "</tr>";
	echo "<tr><td colspan = \"$span\"><hr></td></tr>";
    }
	*/
	echo "</tr>";
echo "</table>";
// Ende Ausgabe

?>
</td></tr></table>
</body>
</html>