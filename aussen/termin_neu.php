<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();


// Variablendefinition ------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

$speich = date("d.m.y");						// Datum von heute (für Speicherung)

$v1_tag = $_POST["v1_tag"];					// von
$v1_monat = $_POST["v1_monat"];				// von
$v1_jahr = $_POST["v1_jahr"];					// von
$g1_tag = $_POST["g1_tag"];					// von

$termin_id = $_GET["t_id"];			// kommt vom Kalender

$aussendienst = $_SESSION['benutzer_kurz'];				// eingeloggter Außendienstler

// Variablen zur Steuerung des Designs der Eingabetabelle, 4 Spalten
$width = "550";			// Tabellenbreite gesamt
$s1 = "90";				// Spalte 1
$s2 = "225";			// Spalte 2
$s3 = "75";				// Spalte 3
$s4 = "150";			// Spalte 4

// Variable zur Unterscheidung, ob Gesell. alt vor Gesell.neu gedrückt wurde
//---------------------------------------------------------------------------------------------------------------------------
	$alt = 0;															// Variable wird zurückgesetzt
	
	if (isset($gesellschaft_alt) OR (isset($_POST["alt"]))) { 
		$alt = 1; 
		$tarif_alt = quote_smart($_POST["tarif_alt"]);				// Tarif neu - aus select-Feld
	}	// Ges.alt-Suchen ist gedrückt worden
	
	$neu = 0;														// Variable wird zurückgesetzt
	
	if (isset($gesellschaft_neu) OR (isset($_POST["neu"]))) { 
			$tarif_neu = quote_smart($_POST["tarif_neu"]);				// Tarif neu - aus select-Feld
			$neu = 1;
	}	// Ges.neu-Suchen ist gedrückt worden

// Testausgaben von Variablen --------------
// -----------------------------------------	

//echo "TID: $termin_id<br />";
//echo "ges_alt: $ges_alt<br />";
//var_dump($_POST);
//echo "alt: $alt<br />";
//echo "neu: $neu<br />";
//echo "v_tag = $v1_tag<br />";
//echo "v_monat = $v1_monat<br />";
//echo "v_jahr = $v1_jahr<br />";
//echo "g_tag = $g1_tag<br />";

//-----------------------------------------

$muster = "/^\d{2}$/";							// Testmuster für Datum, genau 2 Ziffern

$erledigt = date("Y-m-d");						// heute
$aquiriert = date("Y-m-d H:i:s");

// zwei Variablen (der heutige Tag) zum Vergleich mit dem aquiriert-Feld in der DB -
// zur Vermeidung von mehrfachen WVLs durch mehrfaches Speichern hintereinander
$start = $erledigt . " 00:00:01";
$ende = $erledigt . " 23:59:59";

$t_zeit = array("09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", 
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");			// Array für Zeiten
				
$wvl_zeit = array("", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", 
				"15:00", "16:00", "17:00", "18:00", "19:00", "20:00");			// Array für Wiedervorlage-Zeiten

$sql = "SELECT kd_id, termin, zeit, rem_termin, ges_alt, tarif_alt, ges_neu, tarif_neu, von, bis, seit, einzug, nichtkunde ";
$sql .= " FROM termin, produkt_alt, ges_alt, tarif_alt, produkt_neu, ges_neu, tarif_neu ";
$sql .= " WHERE termin.termin_id = '$termin_id' ";
$sql .= " AND termin.produkt_alt_id = produkt_alt.produkt_alt_id AND produkt_alt.ges_alt_id = ges_alt.ges_alt_id ";
$sql .= " AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
$sql .= " AND termin.produkt_neu_id = produkt_neu.produkt_neu_id AND produkt_neu.ges_neu_id = ges_neu.ges_neu_id ";
$sql .= " AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
$abfrage = myqueryi($db, $sql);
$termin = mysqli_fetch_array($abfrage, MYSQLI_ASSOC);

$kunden_id = $termin[kd_id];
$rem_termin = $termin[rem_termin];
list($d_jahr, $d_monat, $d_tag) = explode("-", $termin[termin]);
list($v_jahr, $v_monat, $v_tag) = explode("-", $termin[von]);
list($b_jahr, $b_monat, $b_tag) = explode("-", $termin[bis]);
list($s_jahr, $s_monat, $s_tag) = explode("-", $termin[seit]);
$d_jahr = substr($d_jahr, 2, 2);
$v_jahr = substr($v_jahr, 2, 2); 
$b_jahr = substr($b_jahr, 2, 2); 
$s_jahr = substr($s_jahr, 2, 2);
$einzug = $termin[einzug];															// Einzugsermächtigung erteilt
$nichtkunde = $termin[nichtkunde];	
	
$sql  = "SELECT vorname, name, vorwahl1, telefon, plz, ort, ortsteil,  strasse, fax, email, rem_kunde, geboren, ";
$sql .= "einzug, nichtkunde, belegt, wiedervorlage, vertragskunde, vorwahl2, fax, branche, verzogen, datenschutz ";
$sql .= "FROM kunden, vorname, name, vorwahl1, poo, plz, ort, ortsteil, vorwahl2, branche ";
$sql .= "WHERE kunden.kunden_id = '$termin[kd_id]' AND kunden.vorname_id = vorname.vorname_id ";
$sql .= "AND name.name_id = kunden.name_id AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
$sql .= "AND kunden.vorwahl2_id = vorwahl2.vorwahl2_id AND kunden.branche_id = branche.branche_id ";
$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
$ergebnis = myqueryi($db, $sql);
$kunde = mysqli_fetch_array($ergebnis, MYSQLI_ASSOC);

$t_bemerkung = $kunde[rem_kunde];
$t_ortsteil = $kunde[ortsteil];
$t_branche = $kunde[branche];
$t_vorname = $kunde[vorname];
$t_name = $kunde[name];
$t_strasse = $kunde[strasse];
$t_vorwahl2 = $kunde[vorwahl2];
$t_fax = $kunde[fax];
$t_email = $kunde[email];
list($g_jahr, $g_monat, $g_tag) = explode("-", $kunde[geboren]);
$g_jahr = substr($g_jahr, 2,2);														// Nichtkunde
$verzogen = $kunde[verzogen];														// verzogen
$datenschutz = $kunde[datenschutz];														// datenschutz

	
// Laden der Terminübersicht für den ermittelten Kunden im unteren Fenster
echo "<script>onload=parent['aq_unten'].location.href='termin_check.php?kd_id=$kunden_id'</script>";

// -----------------------------------------------------------------------------------------------------------------------------------

if (isset($gesellschaft_alt) OR isset($gesellschaft_neu) OR ($alt == 1) OR ($neu == 1)) {

	$t_vorname = quote_smart($kd1_vorname);
	$t_name = quote_smart($kd1_name);
	$g_tag = quote_smart($g1_tag);
	$g_monat = quote_smart($g1_monat);
	$g_jahr = quote_smart($g1_jahr);
	$t_strasse = quote_smart($t1_strasse);
	$t_vorwahl2 = quote_smart($t1_vorwahl2);
	$t_fax = quote_smart($t1_fax);
	$t_email = quote_smart($t1_email);
	$t_branche = quote_smart($t1_branche);
	$branche_neu = quote_smart($branche_neu);
	$ortsteil_neu = quote_smart($ortsteil_neu);
	$t_ortsteil = quote_smart($ortsteil1);

	$w_tag = quote_smart($w1_tag);
	$w_monat = quote_smart($w1_monat);
	$w_jahr = quote_smart($w1_jahr);
	$w_zeit = quote_smart($w1_zeit);
	$d_tag = quote_smart($d1_tag);
	$d_monat = quote_smart($d1_monat);
	$d_jahr = quote_smart($d1_jahr);
	$b_tag = quote_smart($b1_tag);
	$b_monat = quote_smart($b1_monat);
	$b_jahr = quote_smart($b1_jahr);
	$s_tag = quote_smart($s1_tag);
	$s_monat = quote_smart($s1_monat);
	$s_jahr = quote_smart($s1_jahr);
	$v_tag = quote_smart($v1_tag);
	$v_monat = quote_smart($v1_monat);
	$v_jahr = quote_smart($v1_jahr);
	$rem_termin = quote_smart($rem1_termin);
	$zeit = quote_smart($zeit);
	$w_zeit = quote_smart($w1_zeit);
	$t_bemerkung = quote_smart($t_bemerkung);
	$g_tag = quote_smart($g1_tag);
	$g_monat = quote_smart($g1_monat);
	$g_jahr = quote_smart($g1_jahr);
	$storno = quote_smart($storno1);
	$einzug = ($_POST["einzug_neu"]);								// Einzugsermächtigung
	$nichtkunde = ($_POST["nichtkunde_neu"]);						// Nichtkunde
	$verzogen = ($_POST["verzogen_neu"]);							// verzogen
	$datenschutz = ($_POST["datenschutz_neu"]);						// Datenschutzerklärung
	
	$einzug = checkbox($einzug);									// Einzugsermächtigung
	$nichtkunde = checkbox($nichtkunde);							// Nichtkunde
	$verzogen = checkbox($verzogen);								// verzogen
	$datenschutz = checkbox($datenschutz);							// Datenschutzerklärung erhalten

	
} // ende if isset ges_alt oder ges_neu

if (isset($t_speichern)) {								// Termin speichern wurde gedrückt

$timestamp = date("YmdHis");

if (!isset($aussendienst) OR empty($aussendienst)) {
	$ausgeloggt = "Sie sind ausgeloggt!! Bitte einloggen und Termin erneut speichern!!";
}
else {	// eingeloggt

	$t_vorname = quote_smart($kd1_vorname);
	$t_name = quote_smart($kd1_name);
	$g_tag = quote_smart($g1_tag);
	$g_monat = quote_smart($g1_monat);
	$g_jahr = quote_smart($g1_jahr);
	$t_strasse = quote_smart($t1_strasse);
	$t_vorwahl2 = quote_smart($t1_vorwahl2);
	$t_fax = quote_smart($t1_fax);
	$t_email = quote_smart($t1_email);
	$t_branche = quote_smart($t1_branche);
	$branche_neu = quote_smart($branche_neu);
	$ortsteil_neu = quote_smart($ortsteil_neu);
	$t_ortsteil = quote_smart($ortsteil1);

	$w_tag = quote_smart($w1_tag);
	$w_monat = quote_smart($w1_monat);
	$w_jahr = quote_smart($w1_jahr);
	$w_zeit = quote_smart($w1_zeit);
	$d_tag = quote_smart($d1_tag);
	$d_monat = quote_smart($d1_monat);
	$d_jahr = quote_smart($d1_jahr);
	$b_tag = quote_smart($b1_tag);
	$b_monat = quote_smart($b1_monat);
	$b_jahr = quote_smart($b1_jahr);
	$s_tag = quote_smart($s1_tag);
	$s_monat = quote_smart($s1_monat);
	$s_jahr = quote_smart($s1_jahr);
	$v_tag = quote_smart($v1_tag);
	$v_monat = quote_smart($v1_monat);
	$v_jahr = quote_smart($v1_jahr);
	$rem_termin = quote_smart($rem1_termin);
	$zeit = quote_smart($zeit);
	$w_zeit = quote_smart($w1_zeit);
	$t_bemerkung = $_POST['t_bemerkung'];
	$t_bemerkung = quote_smart($t_bemerkung);

	$g_tag = quote_smart($g1_tag);
	$g_monat = quote_smart($g1_monat);
	$g_jahr = quote_smart($g1_jahr);
	$storno = quote_smart($storno1);
	$einzug = ($_POST["einzug_neu"]);								// Einzugsermächtigung
	$nichtkunde = ($_POST["nichtkunde_neu"]);						// Nichtkunde
	$verzogen = ($_POST["verzogen_neu"]);							// verzogen
	$datenschutz = ($_POST["datenschutz_neu"]);						// Datenschutzerklärung erhalten

	$einzug = checkbox($einzug);									// Einzugsermächtigung
	$nichtkunde = checkbox($nichtkunde);							// Nichtkunde
	$verzogen = checkbox($verzogen);								// verzogen
	$datenschutz = checkbox($datenschutz);							// Datenschutzerklärung erhalten

	// Überprüfung der persönlichen Daten ------------------------------------------------------------------------------------------
	// -----------------------------------------------------------------------------------------------------------------------------
	
	// Vorname ----------------------------------------------------------------------------------------------------
	$sql1 = "SELECT vorname_id FROM vorname WHERE vorname = '$t_vorname'";	// Vorname schon vorhanden?
	$sql2 = "INSERT INTO vorname (vorname) VALUES ('$t_vorname')";			// Vorname neu
	$vorname_id = id_ermitteln($db, $sql1, $sql2);

	// Name ----------------------------------------------------------------------------------------------------

	$sql1 = "SELECT name_id FROM name WHERE name = '$t_name'";				// Name schon vorhanden?
	$sql2 = "INSERT INTO name (name) VALUES ('$t_name')";					// Name neu
	$name_id = id_ermitteln($db, $sql1, $sql2);
			
	// Ortsteil eingegeben -----------------------------------------------------------------------------------------------------------------
		
	if (empty($ortsteil_neu)) {												// keine Eingabe im Feld neuer Ortsteil
		if ($t_ortsteil == $kunde[ortsteil]) {								// Ortteil für Kunde schon gespeichert oder kein Ortsteil
			if (empty($t_ortsteil)) {										// kein OT eingegeben - ortsteil_id = 1
				$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
				$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
				$sql .="AND ortsteil.ortsteil_id = '1' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
				$query = myqueryi($db, $sql);
				$daten = mysqli_fetch_row($query);
				$poo_id = $daten[0];
			}
			else {	//Ortsteil eingegeben
				$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
				$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
				$sql .="AND ortsteil.ortsteil = '$t_ortsteil' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
				$query = myqueryi($db, $sql);
				$daten = mysqli_fetch_row($query);
				$poo_id = $daten[0];
			}
		}	// ende alter Ortsteil
		
		else {	// neuer Ortsteil
			if (empty($t_ortsteil)) {									// kein OT eingegeben - ortsteil_id = 1
				$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
				$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
				$sql .="AND ortsteil.ortsteil_id = '1' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
				$query = myqueryi($db, $sql);
				$daten = mysqli_fetch_row($query);
				$poo_id = $daten[0];
			}
			else {	//Ortsteil eingegeben
				$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
				$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
				$sql .="AND ortsteil.ortsteil = '$t_ortsteil' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
				$query = myqueryi($db, $sql);
				$daten = mysqli_fetch_row($query);
				$poo_id = $daten[0];
			}
		}	// ende else neuer Ortsteil
		} // keine Eingabe im Feld neuer Ortsteil
	
	else {	// $ortsteil_neu ist nicht leer
		$ortsteil_neu = quote_smart($ortsteil_neu);
		$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
		$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
		$sql .="AND ortsteil.ortsteil = '$ortsteil_neu' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
		$query = myqueryi($db, $sql);
		$daten = mysqli_fetch_row($query);
				
		if ($daten) {						// diese Kombi Ort/Ortsteil ist schon vorhanden
			$poo_id = $daten[0];
		}
		else {	// neue Kombi Ort/ortsteil
			$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil = '$ortsteil_neu'";	// ortsteil schon vorhanden?
			$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil_neu')";				// neuer Ortsteil
			$ortsteil_id = id_ermitteln($db, $sql1, $sql2);
			
			$sql = "SELECT ort_id FROM ort WHERE ort = '$kunde[ort]' ";
			$query = myqueryi($db, $sql);
			$ort = mysqli_fetch_row($query);
			
			$sql = "SELECT plz_id FROM plz WHERE plz = '$kunde[plz]' ";
			$query = myqueryi($db, $sql);
			$plz = mysqli_fetch_row($query);
			
			$sql = "INSERT INTO poo (ort_id, ortsteil_id, plz_id) VALUES ('$ort[0]', '$ortsteil_id', '$plz[0]')";
			$query = myqueryi($db, $sql);
			$poo_id = mysqli_insert_id($db);
		}	// ende neue Kombi Ort/ortsteil
	}	//ende else $ortsteil ist nicht leer
		
	// Ende Ortsteil -----------------------------------------------------------------------------------------------------------------------

	// Vorwahl 2 ----------------------------------------------------------------------------------------------------

	if (empty($t_vorwahl2)) {														// Feld Vorwahl2 ist nicht leer
		$vorwahl2_id = "1";
	}
	else {
		$vorwahl2 = quote_smart($t_vorwahl2);
		$sql1 = "SELECT vorwahl2_id FROM vorwahl2 WHERE vorwahl2 = '$t_vorwahl2'";	// Vorwahl2 schon vorhanden?
		$sql2 = "INSERT INTO vorwahl2 (vorwahl2) VALUES ('$t_vorwahl2')";			// Vorwahl2 neu
		$vorwahl2_id = id_ermitteln($db, $sql1, $sql2);
	}
			
	// Bearbeitung Branche
	// Eingabe von branche_neu überschreibt select-feld
	// ---------------------------------------------------------------------------------------------------------------------------------------
	
	if (empty($branche_neu)) {					// keine Branche eingegeben
		
		if (empty($t_branche)) {				// nichts selektiert
			$branche_id = "1";			
		} //nichts selektiert
			
		else {	// Gesellschaft im Select-Feld ausgewählt
			
			$sql = "SELECT branche_id FROM branche WHERE branche = '$t_branche'";
			$query_branche = myqueryi($db, $sql);
			$daten = mysqli_fetch_array($query_branche);
			$branche_id = $daten[0];
				
		}  // ende else Branche im Select-Feld ausgewählt
	} // ende if keine branche eingegeben
		
	else { // Branche in Textfeld eingegeben
			
		$sql = "SELECT branche_id FROM branche WHERE branche = '$branche_neu'";
		$query_branche = myqueryi($db, $sql);
		$daten = mysqli_fetch_array($query_branche);
					
		if ($daten) {	// die Branche war schon vorhanden
			$branche_id = $daten[0];
		}  // ende if Branche war schon vorhanden
				
		else { // neue Branche
					
			$sql = "INSERT INTO branche (branche) VALUES ('$branche_neu')";
			$query = myqueryi($db, $sql);
			$branche_id = mysqli_insert_id($id);
		}  // ende else neue Branche
	} // ende else Branche in Textfeld eingegeben
	// ende Branche

	// geboren ----------------------------------------------------------------------------------------------------
	
	if (empty($g_tag) OR empty($g_monat) OR empty($g_jahr)) {
		$geboren = "0000-00-00";
	}
	else {
		if ($g_jahr > 20) {
			$geboren = "19$g_jahr-$g_monat-$g_tag";
		}
		else {
			$geboren = "20$g_jahr-$g_monat-$g_tag";
		}
	}
	// Ende geboren ---------------------------------------------------------------------------------------------------------------------------------------

	// Bearbeitung Gesellschaft-ALT / Tarif alt
	// ---------------------------------------------------------------------------------------------------------------------------------------
	
	$ges_alt = quote_smart($ges_alt);
	$tarif_alt = quote_smart($tarif_alt);

	if (empty($ges_alt) AND empty($tarif_alt)) {				// keine Gesellschaft alt und Tarif alt eingegeben
		$produkt_alt_id = "1";
	}
	elseif (empty($ges_alt) AND (!empty($tarif_alt))) {			// nur Tarif alt eingegeben
		$produkt_alt_id = "1";
	}
	elseif (!empty($ges_alt) AND (empty($tarif_alt))) {			// nur Gesellschaft alt eingegeben
		$sql = "SELECT produkt_alt_id FROM produkt_alt, ges_alt, tarif_alt ";
		$sql .= " WHERE produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND ges_alt.ges_alt = '$ges_alt' ";
		$sql .= " AND produkt_alt.tarif_alt_id = '1' ";
		$query = myqueryi($db, $sql);
		$ergebnis = mysqli_fetch_array($query);
		$produkt_alt_id = $ergebnis[0];
	}
	else {														// Gesellschaft alt und Tarif alt eingegeben
		$sql = "SELECT produkt_alt_id FROM produkt_alt, ges_alt, tarif_alt ";
		$sql .= " WHERE produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND ges_alt.ges_alt = '$ges_alt' ";
		$sql .= " AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id AND tarif_alt.tarif_alt = '$tarif_alt' ";
		$query = myqueryi($db, $sql);
		$ergebnis = mysqli_fetch_array($query);
		$produkt_alt_id = $ergebnis[0];
	}

	// Bearbeitung Gesellschaft-NEU / Tarif NEU
	// ---------------------------------------------------------------------------------------------------------------------------------------
	
	$ges_neu = quote_smart($ges_neu);
	$tarif_neu = quote_smart($tarif_neu);
	if (empty($ges_neu)) {											// keine Gesellschaft neu und Tarif neu eingegeben
		$produkt_neu_id = "1";
	}
	elseif (!empty($ges_neu) AND (empty($tarif_neu))) {				// nur Gesellschaft neu eingegeben
		$sql = "SELECT produkt_neu_id FROM produkt_neu, ges_neu, tarif_neu ";
		$sql .= " WHERE produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND ges_neu.ges_neu = '$ges_neu' ";
		$sql .= " AND produkt_neu.tarif_neu_id = '1' ";
		$query = myqueryi($db, $sql);
		$ergebnis = mysqli_fetch_array($query);
		$produkt_neu_id = $ergebnis[0];
	}
	else {															// Gesellschaft neu und Tarif neu eingegeben
		$sql = "SELECT produkt_neu_id FROM produkt_neu, ges_neu, tarif_neu ";
		$sql .= " WHERE produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND ges_neu.ges_neu = '$ges_neu' ";
		$sql .= " AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id AND tarif_neu.tarif_neu = '$tarif_neu' ";
		$query = myqueryi($db, $sql);
		$ergebnis = mysqli_fetch_array($query);
		$produkt_neu_id = $ergebnis[0];
	}

// Ende Bearbeitung Kundendaten ---------------------------------------------------------------------------------

// Start Terminbearbeitung --------------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------------------------

	if (empty($s_tag) AND (empty($s_monat)) AND (empty($s_jahr))) {	// kein seit-Datum eingegeben
		$seit = "0000-00-00";
	}
	else {
		$seit = "20$s_jahr-$s_monat-$s_tag";
	}

	if (empty($v_tag) AND (empty($v_monat)) AND (empty($v_jahr))) {	// kein von-Datum eingegeben
		$von = "0000-00-00";
	}
	else {
		if ($v_jahr > 50) {
			$von = "19$v_jahr-$v_monat-$v_tag";
		}
		else {
			$von = "20$v_jahr-$v_monat-$v_tag";
		}
	}
		
	if (empty($b_tag) AND (empty($b_monat)) AND (empty($b_jahr))) {	// kein bis-Datum eingegeben
		$bis = "0000-00-00";
	}
	else {
		$bis = "20$b_jahr-$b_monat-$b_tag";
	}
	
// Termin aus dem Kalender soll bearbeitet werden
	
	// Nichtkunde --------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------------
	
	if ($nichtkunde == 1) {
	
		// Kundendaten speichern
		
		$t_bemerkung = $t_bemerkung . " (NK:$aussendienst: $speich) ";
	
		$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
		$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
		$sql .= "nichtkunde = '1', wiedervorlage = '0', belegt = '0', neukunde = '0', datenschutz ='$datenschutz' ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		$query = myqueryi($db, $sql);
		
		// Termindaten speichern
		
		$rem_termin1 = $rem_termin . " (NK:$aussendienst: $speich) ";
		
		$sql = "UPDATE termin SET erledigt_date = '$erledigt', rem_termin = '$rem_termin1', nichtkunde = '$nichtkunde',  ";
		$sql .= " abschluss = '0', einzug = '0', wiedervorlage = '0', edit = '$timestamp' ";
		$sql .= " WHERE termin.termin_id = '$termin_id' ";
		$query = myqueryi($db, $sql);;
		
		// alle offenen Termine bearbeiten
			$termin_nk = termin_nk($kunden_id, $heute, $speich, $telefonist);
						
		// alle offenen Wiedervorlagen m�ssen auf "alt" gesetzt werden
			$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);	
		
		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) wurde NICHTKUNDE";
		
	} // Ende Nichtkunde
	
	
	// verzogen -------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------
	
	elseif ($verzogen == 1) {
	
		// Kundendaten speichern
		
		$t_bemerkung1 = $t_bemerkung . " (verz:$aussendienst: $speich) ";
	
		$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
		$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
		$sql .= " verzogen = '1', nichtkunde = '1', wiedervorlage = '0', neukunde = '0', belegt = '0', datenschutz ='$datenschutz' ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		$query = myqueryi($db, $sql);
	
		// Termindaten speichen
		
		$rem_termin1 = $rem_termin . " (verz:$aussendienst: $speich) ";
		
		$sql = "UPDATE termin SET erledigt_date = '$erledigt', rem_termin = '$rem_termin1', nichtkunde = '$nichtkunde', edit = '$timestamp'  ";
		$sql .= " WHERE termin.termin_id = '$termin_id' ";
		$query = myqueryi($db, $sql);
		
		// alle offenen Termine bearbeiten
			$termin_nk = termin_nk($kunden_id, $heute, $speich, $telefonist);
						
		// alle offenen Wiedervorlagen m�ssen auf "alt" gesetzt werden
			$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);	

		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) ist unbekannt verzogen";
		
	} // Ende Kunde ist verzogen
	
	// Speichern gedrückt, ohne Eingabe Wiedervorlage oder Tarif-neu - kein Abschluss, nur Datenänderung --------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------

	elseif ((empty($w_tag) AND empty($w_tag) AND empty($w_tag)) AND ($produkt_neu_id == '1')) {
	
		// Termindaten updaten
		$rem_termin1 = $rem_termin . " ($aussendienst: $speich) ";
		$sql = "UPDATE termin SET rem_termin = '$rem_termin1', produkt_alt_id = '$produkt_alt_id', ";
		$sql .= " produkt_neu_id = '$produkt_neu_id', seit = '$seit', von = '$von', bis = '$bis', erledigt_date = '$erledigt', abschluss = '0', wiedervorlage = '0', nichtkunde = '$nichtkunde', edit = '$timestamp' ";
		$sql .= " WHERE termin_id = '$termin_id' ";
		$query = myqueryi($db, $sql);
		
		// Test, ob es noch weitere offene Termine gibt
		$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND termin > 2000-01-01 AND erledigt_date = 0000-00-00 AND alt = '0' AND sperrzeit = '0' AND termin_id != '$termin_id' ";
		$query = myqueryi($db, $sql);
		$test = mysqli_num_rows($query);
		
		if(mysqli_num_rows($query) > 0) {	// es gibt noch offene Termine, Kunde bleibt belegt
			$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
			$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', datenschutz ='$datenschutz' ";
			$sql .= "WHERE kunden_id = '$kunden_id'";
			$query = myqueryi($db, $sql);
		
		}
		else {								// keine offenen Termine mehr, Kunde wird freigegeben
			$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
			$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
			$sql .= " belegt = '0', datenschutz ='$datenschutz' ";
			$sql .= "WHERE kunden_id = '$kunden_id'";
			$query = myqueryi($db, $sql);
		}

		// alle Wiedervorlagen, die älter oder von heute sind aus DB auslesen und abschließen
		
		$sql = "SELECT termin_id, rem_termin FROM termin WHERE kd_id = '$termin[kd_id]' AND termin.wiedervorlage_date <= '$erledigt' AND termin.wiedervorlage = '1' AND termin.alt = '0'";	// alle alten Wiedervorlagen auslesen
		$query = myqueryi($db, $sql);
		$anzahl = mysqli_num_rows($query);
		for ($i = 0; $i < $anzahl; $i++) {
			$daten = mysqli_fetch_array($query, MYSQLI_ASSOC);
			$rem_termin2 = $daten[rem_termin] . " (alt:$aussendienst: $speich) ";			// Signatur wird an Bemerkung angehangen
			$sql = "UPDATE termin SET rem_termin = '$rem_termin2', erledigt_date = '$erledigt', alt = '1', edit = '$timestamp' ";
			$sql .= "WHERE termin_id = '$daten[termin_id]'";
			$abfrage = myqueryi($db, $sql);
		}
				
		$fehler = "Der Termin für $kunde[vorname] $kunde[name] (ID: $kunden_id) wurde abgeschlossen - kein Abschluss / keine Wiedervorlage!!";
		
	}
	
	else { // Abschluss (mit/ohne WVL) oder kein Abschluss (mit/ohne WVL) folgen
	
		// Test, ob Abschluss - erfolgreich --------------------------------------------
		
		if ($produkt_neu_id > '1') {
		
			// Test, ob neue Wiedervorlage erstellt werden soll - nein
			
			if (empty($w_tag) AND empty($w_monat) AND empty($w_jahr)) {
				// Termindaten speichern
				$rem_termin1 = $rem_termin . " (ab:$aussendienst: $speich) ";
				$sql = "UPDATE termin SET rem_termin = '$rem_termin1', produkt_alt_id = '$produkt_alt_id', ";
				$sql .= " produkt_neu_id = '$produkt_neu_id', seit = '$seit', von = '$von', bis = '$bis', erledigt_date = '$erledigt', abschluss = '1', wiedervorlage = '0', einzug = '$einzug', nichtkunde = '$nichtkunde', edit = '$timestamp' ";
				$sql .= " WHERE termin_id = '$termin_id' ";
				$query = myqueryi($db, $sql);
				$fehler = "Der Termin mit der ID: $termin_id wurde abgeschlossen (Vertrag)";
				
				// Test, ob es noch weitere offene Termine gibt
				$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND termin > 2000-01-01 AND erledigt_date = 0000-00-00 AND alt = '0' AND sperrzeit = '0' AND termin_id != '$termin_id' ";
				$query = myqueryi($db, $sql);
				$test = mysqli_num_rows($query);
												
				if ($test > 0) {						// es gibt noch offene Termine
												
					if ($einzug == 1) {					// Vertragsabschluss mit Bankeinzug
						// Kundendaten speichern
						$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
						$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
						$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', einzug = '1', datenschutz ='$datenschutz'  ";
						$sql .= "WHERE kunden_id = '$kunden_id'";
						$query = myqueryi($db, $sql);
					}
					else {								// Vertragsabschluss ohne Bankeinzug
						// Test, ob es noch weitere Termine mit Bankeinzug gibt
						$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND produkt_neu_id > '1' AND (termin.storno IS NULL OR termin.storno = '0') AND abschluss = '1' AND einzug = '1' AND termin_id != '$termin_id' ";
						$query = myqueryi($db, $sql);
						if (mysqli_num_rows($query) == 0) {				// nein
							// Kundendaten speichern
							$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
							$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
							$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', datenschutz ='$datenschutz'  ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);
						}
						else {											//	ja
							// Kundendaten speichern
							$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
							$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
							$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', datenschutz ='$datenschutz'  ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);
						}														
					}
					$fehler = $fehler . "<br />der Kunde bleibt aber belegt, da noch offene Termine existieren";
				}	// ende noch offene Termine
												
				else {									// keine offenen Termine mehr da
												
					if ($einzug == 1) {					// Vertragsabschluss mit Bankeinzug
						// Kundendaten speichern
						$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
						$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
						$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', einzug = '1', belegt = '0', datenschutz ='$datenschutz'  ";
						$sql .= "WHERE kunden_id = '$kunden_id'";
						$query = myqueryi($db, $sql);
					}
					else {								// Vertragsabschluss ohne Bankeinzug
						// Test, ob es noch weitere Termine mit Bankeinzug gibt
						$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND produkt_neu_id > '1' AND (termin.storno IS NULL OR termin.storno = '0') AND abschluss = '1' AND einzug = '1' AND termin_id != '$t_id' ";
						$query = myqueryi($db, $sql);
						if (mysqli_num_rows($query) == 0) {				// nein
							$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
							$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
							$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', belegt = '0', datenschutz ='$datenschutz'  ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);
						}
						else {											//	ja
							$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
							$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
							$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', belegt = '0', datenschutz ='$datenschutz'  ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);
						}														
					}
					$fehler = $fehler . "<br />der Kunde ist freigegeben (keine offenen Termine mehr)";
				}	// ende keine offenen Termine mehr da
				
				// alle Wiedervorlagen, die älter als heute sind aus DB auslesen und abschließen
				
				$sql = "SELECT termin_id, rem_termin FROM termin WHERE kd_id = '$termin[kd_id]' AND termin.wiedervorlage_date <= '$erledigt' AND termin.wiedervorlage = '1' AND termin.alt = '0'";	// alle alten Wiedervorlagen auslesen
				$query = myqueryi($db, $sql);
				$anzahl = mysqli_num_rows($query);
				for ($i = 0; $i < $anzahl; $i++) {
					$daten = mysqli_fetch_array($query, MYSQLI_ASSOC);
					$rem_termin2 = $daten[rem_termin] . " (alt:$aussendienst: $speich) ";			// Signatur wird an Bemerkung angehangen
					$sql = "UPDATE termin SET rem_termin = '$rem_termin2', erledigt_date = '$erledigt', alt = '1', edit = '$timestamp' ";
					$sql .= "WHERE termin_id = '$daten[termin_id]'";
					$abfrage = myqueryi($db, $sql);
				}
			} // Ende Abschluss, keine neue Wiedervorlage
			
			else { // Abschluss und neue Wiedervorlage
			
				// Test auf WVL-Datum
				
				if(!preg_match($muster, $w_tag) OR !preg_match($muster, $w_monat) OR !preg_match($muster, $w_jahr)) {	// Muster Wiedervorlagedatum passt nicht
					$fehler = "Fehler: Das ist kein gültiges Wiedervorlage-Datum!!";
				}
				
				else {  																								// Muster Wiedervorlagedatum stimmt
			
					$wiedervorlage_date = "20$w_jahr-$w_monat-$w_tag";
			
					if ($wiedervorlage_date < $erledigt) {
						$fehler = "Der Wiedervorlagetermin muss in der Zukunft liegen!!";
					}
					
					else { // Wiedervorlagetermin in der Zukunft
				
						if (empty($w_zeit)) {
							$w_zeit = "00:00";
						}
						
						// Test, ob schon eine WVL am eingegebenen WVL-Datum existiert oder ob heute bereits eine WVL gespeichert wurde
						// diese WVL muss geupdated werden
						// wenn nicht, dann neue Wiedervorlage einfügen
		
						$sql = "SELECT termin_id, rem_termin FROM termin ";
						$sql .= " WHERE kd_id = '$kunden_id' AND (wiedervorlage_date = '$wiedervorlage_date' OR aquiriert BETWEEN '$start' AND '$ende') ";
						$sql .= "AND wiedervorlage = '1' AND alt = '0'";
						
						$query= myqueryi($db, $sql);
						$anzahl =  mysqli_num_rows($query);

						if ($anzahl > 0) {
							$daten = mysqli_fetch_array($query, MYSQLI_ASSOC);
							$rem_termin2 = $rem_termin . " ($aussendienst: $speich) ";		// Signatur wird an Bemerkung angehangen
							$sql = "UPDATE termin SET rem_termin = '$rem_termin2', telefonist = '$aussendienst', aquiriert = '$aquiriert', ";
							$sql .= " wiedervorlage_date = '$wiedervorlage_date', w_zeit = '$w_zeit', produkt_alt_id = '$produkt_alt_id', bis = '$bis', ";
							$sql .= " produkt_neu_id = '$produkt_neu_id', seit = '$seit', von = '$von', wiedervorlage = '1', edit = '$timestamp' ";
							$sql .= "WHERE termin_id = '$daten[termin_id]'";
							$abfrage = myqueryi($db, $sql);
						}
						else {
							$rem_termin2 = $rem_termin . " ($aussendienst: $speich) ";			// Signatur wird an Bemerkung angehangen
							$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, wiedervorlage_date, w_zeit, rem_termin, ";
							$sql .= "produkt_alt_id, bis, produkt_neu_id, seit, von, aquiriert, wiedervorlage, edit) ";
							$sql .= "VALUES ('$kunden_id', '$aussendienst', '$aussendienst', '$wiedervorlage_date', '$w_zeit', '$rem_termin2', ";
							$sql .= "'$produkt_alt_id', '$bis', '$produkt_neu_id', '$seit', '$von', '$aquiriert', '1', '$timestamp') ";
							$abfrage = myqueryi($db, $sql);
							$termin_id_neu = mysqli_insert_id($db);
						}
			
			
						// Termindaten speichern
						$rem_termin1 = $rem_termin . " (ab:$aussendienst: $speich) ";
						$sql = "UPDATE termin SET rem_termin = '$rem_termin1', produkt_alt_id = '$produkt_alt_id', ";
						$sql .= " produkt_neu_id = '$produkt_neu_id', seit = '$seit', von = '$von', bis = '$bis', erledigt_date = '$erledigt', abschluss = '1', wiedervorlage = '0', einzug = '$einzug', nichtkunde = '$nichtkunde', edit = '$timestamp' ";
						$sql .= " WHERE termin_id = '$termin_id' ";
						$query = myqueryi($db, $sql);
						$fehler = "Der Termin mit der ID: $termin_id wurde abgeschlossen (Vertrag), eine neue Wiedervorlage mit ID: $termin_id_neu wurde eingefügt";
				
						// Test, ob es noch weitere offene Termine gibt
						$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND termin > 2000-01-01 AND erledigt_date = 0000-00-00 AND alt = '0' AND sperrzeit = '0' AND termin_id != '$termin_id' ";
						$query = myqueryi($db, $sql);
						$test = mysqli_num_rows($query);
												
						if ($test > 0) {						// es gibt noch offene Termine
												
							if ($einzug == 1) {					// Vertragsabschluss mit Bankeinzug
								// Kundendaten speichern
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
								$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', einzug = '1', wiedervorlage = '1', datenschutz ='$datenschutz'  ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);;
							}
							else {								// Vertragsabschluss ohne Bankeinzug
								// Test, ob es noch weitere Termine mit Bankeinzug gibt
								$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND produkt_neu_id > '1' AND (termin.storno IS NULL OR termin.storno = '0') AND abschluss = '1' AND einzug = '1' AND termin_id != '$termin_id' ";
								$query = myqueryi($db, $sql);
								if (mysqli_num_rows($query) == 0) {				// nein
									// Kundendaten speichern
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
									$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', wiedervorlage = '1', datenschutz ='$datenschutz' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
								}
								else {											//	ja
									// Kundendaten speichern
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
									$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', wiedervorlage = '1', datenschutz ='$datenschutz'  ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
								}														
							}
							$fehler = $fehler . "<br />der Kunde bleibt aber belegt, da noch offene Termine existieren";
						}	// ende noch offene Termine
												
						else {									// keine offenen Termine mehr da
												
							if ($einzug == 1) {					// Vertragsabschluss mit Bankeinzug
								// Kundendaten speichern
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
								$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', einzug = '1', belegt = '0', wiedervorlage = '1', datenschutz ='$datenschutz'  ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
							}
							else {								// Vertragsabschluss ohne Bankeinzug
								// Test, ob es noch weitere Termine mit Bankeinzug gibt
								$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND produkt_neu_id > '1' AND (termin.storno IS NULL OR termin.storno = '0') AND abschluss = '1' AND einzug = '1' AND termin_id != '$t_id' ";
								$query = myqueryi($db, $sql);
								if (mysqli_num_rows($query) == 0) {				// nein
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
									$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', belegt = '0', wiedervorlage = '1', datenschutz ='$datenschutz' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
								}
								else {											//	ja
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
									$sql .= "nichtkunde = '0', vertragskunde = '1', neukunde = '0', belegt = '0', wiedervorlage = '1', datenschutz ='$datenschutz'  ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
								}														
							}
							$fehler = $fehler . "<br />der Kunde ist freigegeben (keine offenen Termine mehr)";
						}	// ende keine offenen Termine mehr da
						
						// alle Wiedervorlagen, die älter als heute sind aus DB auslesen und abschließen
				
						$sql = "SELECT termin_id, rem_termin FROM termin WHERE kd_id = '$termin[kd_id]' AND termin.wiedervorlage_date <= '$erledigt' AND termin.wiedervorlage = '1' AND termin.alt = '0'";				// alle alten Wiedervorlagen auslesen
						$query = myqueryi($db, $sql);
						$anzahl = mysqli_num_rows($query);
						for ($i = 0; $i < $anzahl; $i++) {
							$daten = mysqli_fetch_array($query, MYSQLI_ASSOC);
							$rem_termin2 = $daten[rem_termin] . " (alt:$aussendienst: $speich) ";			// Signatur wird an Bemerkung angehangen
							$sql = "UPDATE termin SET rem_termin = '$rem_termin2', erledigt_date = '$erledigt', alt = '1', edit = '$timestamp' ";
							$sql .= "WHERE termin_id = '$daten[termin_id]'";
							$abfrage = myqueryi($db, $sql);
						}
					} // Ende else WVL-Termin in der Zukunft
				} // Ende else Muster WVL-Datum stimmt
			} // Ende Abschluss und neue Wiedervorlage
		} // Ende Abschluss
		
		// kein Abschluss, neue Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------------------
		// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		else {
		
		// Test, ob neue Wiedervorlage erstellt werden soll - ja
			
			if (!empty($w_tag) AND !empty($w_monat) AND !empty($w_jahr)) {  
			
				// Test auf WVL-Datum
				
				if(!preg_match($muster, $w_tag) OR !preg_match($muster, $w_monat) OR !preg_match($muster, $w_jahr)) {	// Muster Wiedervorlagedatum passt nicht
					$fehler = "Fehler: Das ist kein gültiges Wiedervorlage-Datum!!";
				}
				
				else {  																								// Muster Wiedervorlagedatum stimmt
			
					$wiedervorlage_date = "20$w_jahr-$w_monat-$w_tag";
			
					if ($wiedervorlage_date < $erledigt) {
						$fehler = "Der Wiedervorlagetermin muss in der Zukunft liegen!!";
					}
					
					else { // Wiedervorlagetermin in der Zukunft
				
						if (empty($w_zeit)) {
							$w_zeit = "00:00";
						}
						
						// Termindaten speichern
						
						$rem_termin1 = $rem_termin . " (WVL:$aussendienst: $speich) ";
			
						$sql = "UPDATE termin SET rem_termin = '$rem_termin1', produkt_alt_id = '$produkt_alt_id', ";
						$sql .= " produkt_neu_id = '$produkt_neu_id', seit = '$seit', von = '$von', bis = '$bis', erledigt_date = '$erledigt', abschluss = '$abschluss', wiedervorlage = '0', nichtkunde = '$nichtkunde', edit = '$timestamp' ";
						$sql .= " WHERE termin_id = '$termin_id' ";
						$query = myqueryi($db, $sql);
						
						// alle Wiedervorlagen, die älter oder von heute sind aus DB auslesen und abschließen
						
						$sql = "SELECT termin_id, rem_termin FROM termin WHERE kd_id = '$termin[kd_id]' AND termin.wiedervorlage_date <= '$erledigt'  AND termin.wiedervorlage = '1' AND termin.alt = '0'";				// alle alten Wiedervorlagen auslesen
						$query = myqueryi($db, $sql);
						$anzahl = mysqli_num_rows($query);
						for ($i = 0; $i < $anzahl; $i++) {
							$daten = mysqli_fetch_array($query, MYSQLI_ASSOC);
							$rem_termin2 = $daten[rem_termin] . " (alt:$aussendienst: $speich) ";			// Signatur wird an Bemerkung angehangen
							$sql = "UPDATE termin SET rem_termin = '$rem_termin2', erledigt_date = '$erledigt', alt = '1', edit = '$timestamp' ";
							$sql .= "WHERE termin_id = '$daten[termin_id]'";
							$abfrage = myqueryi($db, $sql);
						}
						
						// Test, ob schon eine WVL am eingegebenen WVL-Datum existiert oder ob heute bereits eine WVL gespeichert wurde
						// diese WVL muss geupdated werden
						// wenn nicht, dann neue Wiedervorlage einfügen
		
						$sql = "SELECT termin_id, rem_termin FROM termin ";
						$sql .= " WHERE kd_id = '$kunden_id' AND (wiedervorlage_date = '$wiedervorlage_date' OR aquiriert BETWEEN '$start' AND '$ende') ";
						$sql .= "AND wiedervorlage = '1' AND alt = '0'";
						$query= myqueryi($db, $sql);
						$anzahl =  mysqli_num_rows($query);

						if ($anzahl > 0) {
							$daten = mysqli_fetch_array($query, MYSQLI_ASSOC);
							$rem_termin2 = $rem_termin . " ($aussendienst: $speich) ";		// Signatur wird an Bemerkung angehangen
							$sql = "UPDATE termin SET rem_termin = '$rem_termin2', telefonist = '$aussendienst', aquiriert = '$aquiriert', ";
							$sql .= " wiedervorlage_date = '$wiedervorlage_date', w_zeit = '$w_zeit', produkt_alt_id = '$produkt_alt_id', bis = '$bis', ";
							$sql .= " produkt_neu_id = '$produkt_neu_id', seit = '$seit', von = '$von', wiedervorlage = '1', edit = '$timestamp' ";
							$sql .= "WHERE termin_id = '$daten[termin_id]'";
							$abfrage = myqueryi($db, $sql);
							
							$fehler = "Der Termin mit der ID: $termin_id wurde abgeschlossen, die Wiedervorlage mit ID: $daten[termin_id] wurde verändert";
						}
						else {
							$rem_termin2 = $rem_termin . " ($aussendienst: $speich) ";			// Signatur wird an Bemerkung angehangen
							$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, wiedervorlage_date, w_zeit, rem_termin, ";
							$sql .= "produkt_alt_id, bis, produkt_neu_id, seit, von, aquiriert, wiedervorlage, edit) ";
							$sql .= "VALUES ('$kunden_id', '$aussendienst', '$aussendienst', '$wiedervorlage_date', '$w_zeit', '$rem_termin2', ";
							$sql .= "'$produkt_alt_id', '$bis', '$produkt_neu_id', '$seit', '$von', '$aquiriert', '1', '$timestamp') ";
							$abfrage = myqueryi($db, $sql);
							$termin_id_neu = mysqli_insert_id($db);
							
							$fehler = "Der Termin mit der ID: $termin_id wurde abgeschlossen, eine neue Wiedervorlage mit ID: $termin_id_neu wurde eingefügt";
						}
						
						// Test, ob es noch weitere offene Termine gibt
							$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND termin > 2000-01-01 AND erledigt_date = 0000-00-00 AND alt = '0' AND sperrzeit = '0' AND termin_id != '$termin_id' ";
							$query = myqueryi($db, $sql);
		
							if(mysqli_num_rows($query) > 0) {	// es gibt noch offene Termine, Kunde bleibt belegt
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id'; ";
								$sql .= " wiedervorlage = '1', datenschutz ='$datenschutz' ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
		
							}
							else {								// keine offenen Termine mehr, Kunde wird freigegeben
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
								$sql .= " belegt = '0', wiedervorlage = '1', datenschutz ='$datenschutz' ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
							}
						
					} // Ende else WVL-Termin in der Zukunft
				} // Ende else Muster WVL-Datum stimmt
			} // Ende kein Abschluss aber neue Wiedervorlage
		} // Ende kein Abschluss
	} // ende else Terminvereinbarung

	if ($norefresh == 0) {
		// Verzögerung um 100 Millisekunden (usleep verlangt Mikrosekunden!)
		usleep(100000);			
	
		// Nachladen der Termin-Check-Seite im unteren Frame												
		echo "<script>onload=parent['aq_unten'].location.href='termin_check.php?kd_id=$kunden_id'</script>";
	
		// Nachladen der Termin-Anzeige-Seite im linken Frame, Benutzer ist als Session-Variable gespeichert												
		echo "<script>onload=parent['aq_links'].location.href='termin_anzeige.php'</script>";
	}
	}	// ende else eingeloggt
} // Ende isset($_speichern)

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Termine-neu</title>
<!-- aussen/termin_neu.php --->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">

<table border="1" cellspacing="0" cellpadding="0" width="<?php echo "$width"; ?>" bordercolor="#006699">
<?php
	if ($fehler) {
 		echo "<tr bgcolor=\"#F0F8FF\">";
        echo "<td colspan=\"8\">";
		echo "<span style=\"font-weight:bold; color:red;\">$fehler</span><br>";
		echo "</td></tr>";
	}
		elseif ($ausgeloggt) {
 		echo "<tr bgcolor=\"red\">";
        echo "<td colspan=\"8\">";
		echo "<div align = \"center\"><span style=\"font-weight:bold; font-size: 13pt; color:white; line-height: 150%;\">$ausgeloggt</span></div>";
		echo "</td></tr>";
	}
?>
  <!-- Tabelle Rand -->
<form name="terminvereinbarung" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
	<tr>
    	<td colspan="4">
	<tr bgcolor="#F0F8FF">
  <?php
			echo "<td colspan=\"4\"><span style=\"font-weight:bold;\">Kd.: $kunden_id $kunde[vorname] $kunde[name] - Tel.: ($kunde[vorwahl1]) $kunde[telefon] - $kunde[plz] $kunde[ort]</span></td>";
		?>
	</tr>
    <tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Termin</td>
      <td width="<?php echo "$s2"; ?>">
   <?php
	  	// Termin aus Kalender: Eingabefelder für Termin werden nicht dargestellt, um Eingabe zu verhindern,
		// müssen trotzdem als "hidden" mitgeführt werden, damit Variablewn nach Ges.(alt/neu)-suchen und Speichern noch existieren
			echo "<input type=\"hidden\" name=\"d1_tag\" value = \"$d_tag\">";
        	echo "<input type=\"hidden\" name=\"d1_monat\" value = \"$d_monat\">";
        	echo "<input type=\"hidden\" name=\"d1_jahr\" value = \"$d_jahr\">";
			
        	echo "<strong>$d_tag.$d_monat.$d_jahr</strong>";
		?>
      </td>
      <td width="<?php echo "$s3"; ?>">Zeit</td>
	  <td width="<?php echo "$s4"; ?>">
	  <?php
		  	// Termin aus Kalender: Select-Feld für Zeit wird nicht dargestellt, um Eingabe zu verhindern,
			// muss trotzdem als "hidden" mitgeführt werden, damit Variable nach Ges.(alt/neu)-suchen und Speichern noch existiert
				echo "<input type=\"hidden\" name=\"zeit\" value = \"$termin[zeit]\">";
				echo "<strong>$termin[zeit]</strong>";
	 	?>
      </td>
    </tr>
	    <tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">WVL&nbsp;</td>
      <td width="<?php echo "$s2"; ?>"><input type="text" class = "eingabe" name="w1_tag" size = "2" maxlength ="2" value = "<?php echo "$w_tag"; ?>"/>
        . 
        <input type="text" class = "eingabe" name="w1_monat" size = "2" maxlength ="2" value = "<?php echo "$w_monat"; ?>"/>
        . 
        <input type="text" class = "eingabe" name="w1_jahr" size = "2" maxlength ="2" value = "<?php echo "$w_jahr"; ?>"/></td>
	  <td width="<?php echo "$s3"; ?>">Zeit</td>
	  <td width="<?php echo "$s4"; ?>"><select name="w1_zeit" class = "eingabe">
	  <?php
			for ($i = 0; $i < count($wvl_zeit); $i++) { // Erzeugung der Zeilen
				if ($w_zeit === $wvl_zeit[$i]) {
					echo "<option selected>$w_zeit</option>";
				}
				else {
					echo "<option>$wvl_zeit[$i]</option>";
				}
			}
	 	?>
	 	</select>      
      </td>
	</tr>
	
<!-- Gesellschaft alt ----------------------------------------------------------------------------------------------------------------------->
<!-- ---------------------------------------------------------------------------------------------------------------------------------------->	
		
	<tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Ges. alt</td>
      <td width="<?php echo "$s2"; ?>">
	<?php
	   
	   	if ($alt == 1 OR isset($t_speichern)) {
			echo "<input type=\"hidden\" name=\"alt\" value = \"1\"/>";			// verstecktes Feld, das anzeigt, dass Gesell.alt-Suchen gedrückt wurde
			//if ($alt == 1 AND (isset($gesellschaft_alt) OR isset($gesellschaft_neu) OR isset($t_speichern))) {
			// nach dem Drücken des Such-Buttons für Ges.alt/neu oder Speichern muss das Feld als hidden mitgeführt werden, damit Variable erhalten bleibt
			// Darstellung nur als flacher text (verhindert Eingabe)
			if (empty($ges_alt)) {
					echo "--";
				}
			else {
				echo "<input type=\"hidden\" name=\"ges_alt\" value = \"$ges_alt\"/>$ges_alt";
			}
		}
		else {												// die Such-Buttons oder Speichern wurde noch nicht gedrückt - normales Select-Feld
			echo "<select name=\"ges_alt\">";
	  
	   		$sql = "SELECT ges_alt FROM ges_alt ORDER BY ges_alt ASC ";
			$query_ges_alt = myqueryi($db, $sql);
			
			while ($zeile = mysqli_fetch_array($query_ges_alt)) {
			
				if ($termin[ges_alt] == $zeile[0]) {
					echo "<option selected>$zeile[0]</option>";
				}
				else {
					echo "<option>$zeile[0]</option>";
				}
			}
			echo "</select>";
			echo "&nbsp;<input type=\"submit\" name=\"gesellschaft_alt\" value = \"?\" class=\"suche\"/>";
			
		}
		?>
		
      </td>
	  <td width="<?php echo "$s3"; ?>">Tarif alt</td>
	  <td width="<?php echo "$s4"; ?>">
	   <?php
		   	// falls keine Gesell-alt eingegeben wurde, wird "--" bei Tarif ausgegeben, anderenfalls werden aus der tarif-Tabelle die passenden Tarife
			// ausgewählt und als Select-Feld zur Verfugung gestellt, wenn keine Tarife gespeichert sind, wird "keine Tarife verfügbar" ausgegeben
	   		if ($alt == 1 OR isset($t_speichern)) {
			//if (isset($gesellschaft_alt) OR isset($gesellschaft_neu) OR isset($t_speichern)) {
			
				if (empty($ges_alt)) {
					echo "--";
				}
				else {
			
					$sql  = "SELECT tarif_alt, tarif_alt.tarif_alt_id FROM produkt_alt, tarif_alt, ges_alt ";
					$sql .= "WHERE produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
					$sql .= "AND ges_alt.ges_alt = '$ges_alt' ";
					$query_tarif_alt = myqueryi($db, $sql);
					
					$anzahl = mysqli_num_rows($query_tarif_alt);
					
					if ($anzahl == 1) {										// nur 1 Tarif gespeichert - Standard-Tarif!!
						$zeile = mysqli_fetch_array($query_tarif_alt);
						if ($zeile[1] == 1) {
							echo "keine Tarife verfügbar";
						}
					}
					else {
						echo "<select name=\"tarif_alt\">";
						while ($zeile = mysqli_fetch_array($query_tarif_alt)) {
							if ($tarif_alt == $zeile[0]) {
								echo "<option selected>$zeile[0]</option>";
							}
							else {
								echo "<option>$zeile[0]</option>";
							}
						}
						echo "</select>";
					}
				}
			}
			else {											// Darstellung beim Start des Programms
				echo "<select name=\"tarif_alt\">";
				$sql  = "SELECT tarif_alt FROM produkt_alt, tarif_alt, ges_alt ";
				$sql .= "WHERE produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
				$sql .= "AND ges_alt.ges_alt = '$termin[ges_alt]' ";
	
				$query_tarif_alt = myqueryi($db, $sql);
		
				while ($zeile = mysqli_fetch_array($query_tarif_alt)) {
			
					if ($termin[tarif_alt] == $zeile[0]) {
						echo "<option selected>$zeile[0]</option>";
					}
					else {
						echo "<option>$zeile[0]</option>";
					}
				}
				echo "</select>";
			}
		?>	  
	  	</td>
    </tr>
 <?php
	// wenn keine Ges.alt eingegeben ist, wird die Zeile von/bis komplett ausgeblendet!!
	if ($alt == 1 OR isset($t_speichern)) {
	//if (isset($gesellschaft_alt) OR isset($gesellschaft_neu) OR isset($t_speichern)) {
		if (!empty($ges_alt)) {
		
		?>
	<tr bgcolor="#F0F8FF"> 
      	<td width="<?php echo "$s1"; ?>">von</td>
      	<td width="<?php echo "$s2"; ?>">
	 <?php
				if (($v_tag == "00") AND ($v_monat == "00") AND ($v_jahr == "0000")) {
	  				echo "<input type=\"text\" class=\"eingabe\" name=\"v1_tag\" size = \"2\" maxlength =\"2\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"v1_monat\" size = \"2\" maxlength =\"2\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"v1_jahr\" size = \"2\" maxlength =\"2\">";
				} 
				else {
					echo "<input type=\"text\" class=\"eingabe\" name=\"v1_tag\" size = \"2\" maxlength =\"2\" value = \"$v_tag\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"v1_monat\" size = \"2\" maxlength =\"2\" value = \"$v_monat\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"v1_jahr\" size = \"2\" maxlength =\"2\" value = \"$v_jahr\">";
				}
			?>

	 	</td>
	 	<td width="<?php echo "$s3"; ?>">bis </td>
	  	<td width="<?php echo "$s4"; ?>">
	 <?php
				if (($b_tag == "00") AND ($b_monat == "00") AND ($b_jahr == "0000")) {
	  				echo "<input type=\"text\" class=\"eingabe\" name=\"b1_tag\" size = \"2\" maxlength =\"2\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"b1_monat\" size = \"2\" maxlength =\"2\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"b1_jahr\" size = \"2\" maxlength =\"2\">";
				} 
				else {
					echo "<input type=\"text\" class=\"eingabe\" name=\"b1_tag\" size = \"2\" maxlength =\"2\" value = \"$b_tag\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"b1_monat\" size = \"2\" maxlength =\"2\" value = \"$b_monat\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"b1_jahr\" size = \"2\" maxlength =\"2\" value = \"$b_jahr\">";
				}
			?>
	 	</td>
   	</tr>
  <?php
			}
		}
		else {												// Darstellung beim erstmaligen Aufruf des Formulars
			?>
	<tr bgcolor="#F0F8FF"> 
    	<td width="<?php echo "$s1"; ?>">von</td>
      	<td width="<?php echo "$s2"; ?>">
	 <?php
				if (($v_tag == "00") AND ($v_monat == "00") AND ($v_jahr == "0000")) {
	  				echo "<input type=\"text\" class=\"eingabe\" name=\"v1_tag\" size = \"2\" maxlength =\"2\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"v1_monat\" size = \"2\" maxlength =\"2\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"v1_jahr\" size = \"2\" maxlength =\"2\">";
				} 
				else {
					echo "<input type=\"text\" class=\"eingabe\" name=\"v1_tag\" size = \"2\" maxlength =\"2\" value = \"$v_tag\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"v1_monat\" size = \"2\" maxlength =\"2\" value = \"$v_monat\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"v1_jahr\" size = \"2\" maxlength =\"2\" value = \"$v_jahr\">";
				}
			?>
	  
	 	</td>
	 	<td width="<?php echo "$s3"; ?>">bis </td>
	  	<td width="<?php echo "$s4"; ?>">
	 <?php
				if (($b_tag == "00") AND ($b_monat == "00") AND ($b_jahr == "0000")) {
	  				echo "<input type=\"text\" class=\"eingabe\" name=\"b1_tag\" size = \"2\" maxlength =\"2\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"b1_monat\" size = \"2\" maxlength =\"2\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"b1_jahr\" size = \"2\" maxlength =\"2\">";
				} 
				else {
					echo "<input type=\"text\" class=\"eingabe\" name=\"b1_tag\" size = \"2\" maxlength =\"2\" value = \"$b_tag\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"b1_monat\" size = \"2\" maxlength =\"2\" value = \"$b_monat\"> . "; 
        			echo "<input type=\"text\" class=\"eingabe\" name=\"b1_jahr\" size = \"2\" maxlength =\"2\" value = \"$b_jahr\">";
				}
			
		}
	?>
	
<!-- Gesellschaft neu ----------------------------------------------------------------------------------------------------------------------->
<!-- ---------------------------------------------------------------------------------------------------------------------------------------->

	<tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Ges. neu</td>
      <td width="<?php echo "$s2"; ?>">
	<?php
		if ($neu == 1 OR isset($t_speichern)) {
			echo "<input type=\"hidden\" name=\"neu\" value = \"1\"/>";			// verstecktes Feld, das anzeigt, dass Gesell.neu-Suchen gedrückt wurde
	   		//if (isset($gesellschaft_neu) OR isset($t_speichern)) {
			// nach dem Drücken des Such-Buttons für Ges.neu oder Speichern muss das Feld als hidden mitgeführt werden, damit Variable erhalten bleibt
			// Darstellung nur als flacher text (verhindert Eingabe)
			if (empty($ges_neu)) {
					echo "--";
				}
			else {
				echo "<input type=\"hidden\" name=\"ges_neu\" value = \"$ges_neu\"/>$ges_neu";
			}
		}
		else {					// erster Aufruf des Formulars - normales Select-Feld
			echo "<select name=\"ges_neu\">";
	  
	   		$sql = "SELECT ges_neu FROM ges_neu ORDER BY ges_neu ASC ";
			$query_ges_neu = myqueryi($db, $sql);
			
			while ($zeile = mysqli_fetch_array($query_ges_neu)) {
			
				if ($termin[ges_neu] == $zeile[0]) {
					echo "<option selected>$zeile[0]</option>";
				}
				else {
					echo "<option>$zeile[0]</option>";
				}
			}
			echo "</select>";
			echo "&nbsp;<input type=\"submit\" name=\"gesellschaft_neu\" value = \"?\" class=\"suche\"/>";

		}
		?>	
      </td>
	  
	  <td width="<?php echo "$s3"; ?>">Tarif neu</td>
	  <td width="<?php echo "$s4"; ?>">
	   <?php
		   	// falls keine Gesell-neu eingegeben wurde, wird "--" bei Tarif ausgegeben, anderenfalls werden aus der tarif-Tabelle die passenden Tarife
			// ausgewählt und als Select-Feld zur Verfügung gestellt, wenn keine Tarife gespeichert sind, wird "keine Tarife verfügbar" ausgegeben
	   		if ($neu == 1 OR isset($t_speichern)) {
			
				if (empty($ges_neu)) {
					echo "--";
				}
				else {
			
					$sql  = "SELECT tarif_neu, tarif_neu.tarif_neu_id  FROM produkt_neu, tarif_neu, ges_neu ";
					$sql .= "WHERE produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
					$sql .= "AND ges_neu.ges_neu = '$ges_neu' ";
	
					$query_tarif_neu = myqueryi($db, $sql);
					
					$anzahl = mysqli_num_rows($query_tarif_neu);
					
					if ($anzahl == 1) {										// nur 1 Tarif gespeichert - Standard-Tarif!!
						$zeile = mysqli_fetch_array($query_tarif_neu);
						if ($zeile[1] == 1) {
							echo "keine Tarife verfügbar";
						}
					}
					else {
						echo "<select name=\"tarif_neu\">";
						while ($zeile = mysqli_fetch_array($query_tarif_neu)) {
							if ($tarif_neu == $zeile[0]) {
								echo "<option selected>$zeile[0]</option>";
							}
							else {
								echo "<option>$zeile[0]</option>";
							}
						}
						echo "</select>";
					}
				}
			}
			else {											// Darstellung beim Start des Programms
				echo "<select name=\"tarif_neu\">";
				$sql  = "SELECT tarif_neu FROM produkt_neu, tarif_neu, ges_neu ";
				$sql .= "WHERE produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
				$sql .= "AND ges_neu.ges_neu = '$termin[ges_neu]' ";
	
				$query_tarif_neu = myqueryi($db, $sql);
		
				while ($zeile = mysqli_fetch_array($query_tarif_neu)) {
			
					if ($termin[tarif_neu] == $zeile[0]) {
						echo "<option selected>$zeile[0]</option>";
					}
					else {
						echo "<option>$zeile[0]</option>";
					}
				}
				echo "</select>";
			}
		?>	  
	  	</td>
    </tr>
	
 <?php
		// wenn keine Ges.neu eingegeben ist, wird die Zeile seit komplett ausgeblendet!!
		if ($neu == 1 OR isset($t_speichern)) {
			if (!empty($ges_neu)) {
		
			?>
				<tr bgcolor="#F0F8FF"> 
      				<td width="<?php echo "$s1"; ?>">&nbsp;</td>
      				<td width="<?php echo "$s2"; ?>">&nbsp;</td>
	 		 		<td width="<?php echo "$s3"; ?>">seit</td>
	  				<td width="<?php echo "$s4"; ?>">
	   <?php
						if ($s_tag == "00") {
							$s_tag = "";
						}
						if ($s_monat == "00") {
							$s_monat = "";
						}
						if ($s_jahr == "00") {
							$s_jahr = "";
						}
					?>
       		 		<input type="text" class="eingabe" name="s1_tag" size = "2" maxlength ="2" value = "<?php echo "$s_tag"; ?>"/>
       		 		. 
       		 		<input type="text" class="eingabe" name="s1_monat" size = "2" maxlength ="2" value = "<?php echo "$s_monat"; ?>"/>
       		 		. 
       		 		<input type="text" class="eingabe" name="s1_jahr" size = "2" maxlength ="2" value = "<?php echo "$s_jahr"; ?>"/>
     		 	</td>
   		 		</tr>
   <?php
			}
		}
		else {													// Darstellung beim ersten Aufruf des Formulars
			?>
				<tr bgcolor="#F0F8FF"> 
      			<td width="<?php echo "$s1"; ?>">&nbsp;</td>
      			<td width="<?php echo "$s2"; ?>">&nbsp;</td>
	 		 	<td width="<?php echo "$s3"; ?>">seit</td>
	  			<td width="<?php echo "$s4"; ?>">
	  <?php
					if ($s_tag == "00") {
						$s_tag = "";
					}
					if ($s_monat == "00") {
						$s_monat = "";
					}
					if ($s_jahr == "00") {
						$s_jahr = "";
					}
				?>
	   		 		<input type="text" class="eingabe" name="s1_tag" size = "2" maxlength ="2" value = "<?php echo "$s_tag"; ?>"/>
       		 		. 
       		 		<input type="text" class="eingabe" name="s1_monat" size = "2" maxlength ="2" value = "<?php echo "$s_monat"; ?>"/>
       		 		. 
       		 		<input type="text" class="eingabe" name="s1_jahr" size = "2" maxlength ="2" value = "<?php echo "$s_jahr"; ?>"/>
     		 	</td>
   		 		</tr>
  <?php
		}
	?>

   <tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Bemerkung</td>
      <td colspan="3"><textarea cols="50" rows="6" class = "eingabe" name="rem1_termin"><?php echo "$rem_termin"; ?></textarea></td>
    </tr>
		 <tr bgcolor="#F0F8FF"> 
      <td colspan="4" align="center">
	 	&nbsp;Einzug&nbsp;
	<?php
			if ($einzug == 0) {
				echo "<input type=\"checkbox\" name=\"einzug_neu\">";
			}
			else {
				echo "<input type=\"checkbox\" name=\"einzug_neu\" checked>";
		}
		?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nichtkd.&nbsp;
  <?php
			if ($nichtkunde == 0) {
				echo "<input type=\"checkbox\" name=\"nichtkunde_neu\">";
			}
			else {
				echo "<input type=\"checkbox\" name=\"nichtkunde_neu\" checked>";
			}
		?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(UNBEKANNT) verzogen&nbsp;
  <?php
			if ($verzogen == 0) {
				echo "<input type=\"checkbox\" name=\"verzogen_neu\">";
			}
			else {
				echo "<input type=\"checkbox\" name=\"verzogen_neu\" checked>";
			}
		?>
		</td>
    </tr>
	

    <!-- Ende Zeile oben für Termin -->
	    <tr bgcolor="#F0F8FF"> 
      <td colspan ="4" align = "center"> 
        <input type="submit" name="t_speichern" value = "Speichern" class="submitt"/>
      </td>
    </tr>

  <!-- ENDE Termin / ANFANG Kunde -->
		<tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Vorname</td>
      <td width="<?php echo "$s2"; ?>">
        <input type="text" class = "eingabe" name="kd1_vorname" maxlength = "20" size = "20" value = "<?php echo "$t_vorname"; ?>"/>
      </td>
      <td width="<?php echo "$s3"; ?>">Name</td>
      <td width="<?php echo "$s4"; ?>">
	  	<input type="text" class = "eingabe" name="kd1_name" maxlength = "20" size = "20" value = "<?php echo "$t_name"; ?>"/>
      </td>
    </tr>
	<tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Ort</td>
      <td width="<?php echo "$s2"; ?>">
        <input type="text" class = "eingabe" name="t_ort" maxlength = "20" size = "20" value = "<?php echo "$kunde[ort]"; ?>"/>
      </td>
      <td width="<?php echo "$s3"; ?>">Straße</td>
      <td width="<?php echo "$s4"; ?>">
	  	<input type="text" class = "eingabe" name="t1_strasse" maxlength = "60" size = "20" value = "<?php echo "$t_strasse"; ?>"/>
      </td>
    </tr>
	
    <tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Ortsteil</td>
      <td width="<?php echo "$s2"; ?>">
	  <select name="ortsteil1" class = "eingabe">
   <?php
	  	$sql = "SELECT ortsteil FROM poo, plz, ort, ortsteil ";
		$sql .= "WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id ";
		$sql .= "AND ort.ort = '$kunde[ort]' ";
		$ortsteil = myqueryi($db, $sql);
		
		for ($j = 0; $j < mysqli_num_rows($ortsteil); $j++)	{				// Anzahl der Datensätze
				$zeile = mysqli_fetch_row($ortsteil);						// Schleife für Daten-Zeilen
				if 	($zeile[0] == $t_ortsteil) {
					echo "<option selected>$zeile[0]</option>";
				}
				else {
					echo "<option>$zeile[0]</option>";
				}
			}
	  ?>
	  </select>
      </td>
      <td width="<?php echo "$s3"; ?>"><strong>OT neu</strong></td>
      <td width="<?php echo "$s4"; ?>">
	  <input type="text" class = "eingabe" name="ortsteil_neu" maxlength = "25" size = "20" value = "<?php echo "$ortsteil_neu"; ?>"/>
      </td>
    </tr>
	  <tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">geboren</td>
      <td width="<?php echo "$s2"; ?>">
	   <?php
		if (($g_tag == "00") AND ($g_monat == "00") AND ($g_jahr == "0000")) {
	  		echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_tag\" size = \"2\" maxlength =\"2\"> . "; 
        	echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_monat\" size = \"2\" maxlength =\"2\"> . "; 
        	echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_jahr\" size = \"2\" maxlength =\"2\">";
		} 
		else {
			echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_tag\" size = \"2\" maxlength =\"2\" value = \"$g_tag\"> . "; 
        	echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_monat\" size = \"2\" maxlength =\"2\" value = \"$g_monat\"> . "; 
        	echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_jahr\" size = \"2\" maxlength =\"2\" value = \"$g_jahr\">";
		}
	?>
      </td>
      <td width="<?php echo "$s3"; ?>">&nbsp;</td>
      <td width="<?php echo "$s4"; ?>">&nbsp;</td>
    </tr>
    <tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Vorwahl 2</td>
      <td width="<?php echo "$s2"; ?>">
        <input type="text" class = "eingabe" name="t1_vorwahl2" size="8" value = "<?php echo "$t_vorwahl2"; ?>"/>
      </td>
      <td width="<?php echo "$s3"; ?>">Telefon 2</td>
      <td width="<?php echo "$s4"; ?>">
        <input type="text" class = "eingabe" name="t1_fax" size="12" value = "<?php echo "$t_fax"; ?>"/>
      </td>
    </tr>
    <tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">E-Mail</td>
      <td width="<?php echo "$s2"; ?>">
        <input type="text" class = "eingabe" name="t1_email" size="20" value = "<?php echo "$t_email"; ?>"/>
      </td>
      <td width="<?php echo "$s3"; ?>">&nbsp;</td>
      <td width="<?php echo "$s4"; ?>">&nbsp;</td>
    </tr>
	<tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Branche</td>
      <td width="<?php echo "$s2"; ?>">
         <select name="t1_branche" class = "eingabe">
	  <?php
		   	if (isset($gesellschaft_alt) OR isset($gesellschaft_alt)) {
			
				$sql = "SELECT branche FROM branche ORDER BY branche ASC";
				$query_branche = myqueryi($db, $sql);
				
				for ($j = 0; $j < mysqli_num_rows($query_branche); $j++)	{		// Anzahl der Datensätze
					$zeile = mysqli_fetch_row($query_branche);						// Schleife für Daten-Zeilen
					if ($zeile[0] == $t_branche) {
						if ($zeile[0] === "NULL"){
							$zeile[0] = "";
						}
						echo " <option selected>$zeile[0]</option>";
					}
					else {
						echo " <option>$zeile[0]</option>";
					}
				}
			}
			else {
			
				$sql = "SELECT branche FROM branche WHERE branche_id > '1' ORDER BY branche ASC";
				$query_branche = myqueryi($db, $sql);
				
				echo " <option></option>";
				for ($j = 0; $j < mysqli_num_rows($query_branche); $j++)	{		// Anzahl der Datensätze
					$zeile = mysqli_fetch_row($query_branche);						// Schleife für Daten-Zeilen
					if ($zeile[0] == $t_branche) {
						if ($zeile[0] === "NULL"){
							$zeile[0] = "";
						}
						echo " <option selected>$zeile[0]</option>";
					}
					else {
						if ($zeile[0] === "NULL"){
							$zeile[0] = "";
						}
						echo " <option>$zeile[0]</option>";
					}
				}
			}
	 	?>
    </select>
      </td>
      <td width="<?php echo "$s3"; ?>">Eingabe</td>
      <td width="<?php echo "$s4"; ?>"><input type="text" class = "eingabe" name="branche_neu" size ="15" maxlength="20" value = "<?php echo "$branche_neu"; ?>"></td>
    </tr>
    <tr bgcolor="#F0F8FF"> 
      <td width="<?php echo "$s1"; ?>">Bemerkung</td>
      <td ><textarea cols="48" rows="4" class = "eingabe" name="t_bemerkung"><?php echo "$t_bemerkung"; ?></textarea></td>
		<td ><strong>Datenschutz-<br>erklärung</strong></td>
		<td>
			<?php
			if ($datenschutz == 0) { echo "<input type=\"checkbox\" name=\"datenschutz_neu\">"; }
			else { echo "<input type=\"checkbox\" name=\"datenschutz_neu\" checked>"; }
			?>
			erhalten
		</td>
	</tr>



	    <!-- Ende Zeile unten für Kunde -->
  </table>
</form> 
<!-- Ende Tabelle Rand -->

</div>
</body>
</html>

<?php $close = mysqli_close($db); ?>