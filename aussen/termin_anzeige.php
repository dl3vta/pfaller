<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();


$aussendienst = $_SESSION['benutzer_kurz'];

$name = $_SESSION['benutzer_name'];
$vorname = $_SESSION['benutzer_vorname'];

$zahl = "23";			// wie viele Tage sollen angezeigt werden - Zählung beginnt bei Null
$vergangen = "-16";		// Anzahl der zurückliegenden Tage, die angezeigt werden sollen
$zukunft = "7";			// Anzahl der Tage in der Zukunft, die angezeigt werden sollen
$end = $zukunft - 1;	// Endezeitpunkt für Termin-Select - end = heute + zukunft -1 Tag (heute!!)


$span = $zahl + 1 ;		// Anzahl der anzuzeigenden Tage + eine Zelle vorn in der Tabelle für die Uhrzeit

$tag = substr($start, 6,2);
$monat = substr($start, 4,2);
$jahr = substr($start, 0,4);



$span = $zahl +3 ;		// eine Zelle für die Uhrzeit
$t_zeit = array("09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", 
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");
				
$zahl_t_zeit = count($t_zeit);								// Anzahl der Elemente im Zeit-Array

$dat1 = date("Ymd", strtotime(" $vergangen days"));		// heute vor 16 Tagen
$dat2 = date("Ymd", strtotime("+ $end days"));				// heute + 6 tage

$sql = "SELECT termin_id, termin, zeit, sperrzeit, plz, abschluss, termin.nichtkunde, erledigt_date, termin.storno, kalt FROM termin, kunden, poo, plz ";
$sql .= "WHERE termin.kd_id = kunden.kunden_id AND kunden.poo_id=poo.poo_id AND poo.plz_id=plz.plz_id AND termin.alt = '0' ";
$sql .= "AND termin BETWEEN $dat1 AND $dat2 AND (wiedervorlage_date IS NULL OR wiedervorlage_date='0000-00-00') AND aussendienst = '$aussendienst' ";
$ergebnis = myqueryi($db, $sql);

$datensatz = mysqli_num_rows($ergebnis);								// Anzahl der aus der Datenbank ausgelesenen Termine


// Debugging -------------------------------------------------------------------------------------------------------------//
/*
echo "Aussendienst: $aussendienst <br />";
echo "Name: $name <br />";
echo "Vorname: $vorname <br />";
echo "Datum1: $dat1 <br />";
echo "Datum2: $dat2 <br />";
echo "Anzahl Termine: $datensatz <br />";
*/
// ende Debugging ---------------------------------------------------------------------------------------------------------//

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Termine</title>
	<!-- aussen/termin_anzeige.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { 	margin: 5px; }
-->
</style>
</head>
<body>
<div align = "center">

<table border="1" width="800" class="zeiten" cellspacing="0" cellpadding="0">
<form name="user_neu" method="post" action="<?php $PHP_SELF ?>">
<tr bgcolor="#006699">

<?php
    echo "<td colspan=\"$span\">";
      echo "<div align=\"center\"><b><font color=\"#FFFFFF\">aktuelle Termine für: $aussendienst</font></b></div>";
    echo "</td>";
  echo "</tr>";
  
  echo "<tr>";
    echo "<td>";
  echo "<tr>";
    echo "<td class=\"spalten\">&nbsp;</td>";
	
for ($i= $vergangen; $i < $zukunft; $i++) { 				// Termine von gestern bis in 13 Tagen
	$datum = datum($i);
	$heute = date("d")+0 . "-" . date("m") . "-" . date("Y");		// damit bei einstelligen Daten nur eine Stelle erscheint
	$aktuell = date("d")+$i . "-" . date("m") . "-" . date("Y");
	if ($heute == $aktuell) {							// Spalte HEUTE - rot markiert
		echo "<td class=\"spalten\"><a href=\"termin_drucken.php?t_id=$datum\" target=\"_blank\"><span style=\"font-weight:bold; color:red;\">$datum</span></a></td>";
	}
	else {
		echo "<td class=\"spalten\"><a href=\"termin_drucken.php?t_id=$datum\" target=\"_blank\">$datum</a></td>";
	}
}

echo "</tr>";

$treffer = "0";

for ($j = 0; $j < $zahl_t_zeit; $j++) { // Erzeugung der Zeilen

	// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// es muss ermittelt werden, an welchem Tag zu der bestimmten Zeit $t_zeit[$j] mehrere Termine vorliegen
	// dazu werden alle aus der Datenbank ausgelesenen Termine durchlaufen und das Array "Teiler" geschaffen
	// das Array hat folgende Struktur: $teiler = ("0", "0", "2", "0"; ...) und wird am Anfang komplett mit 0 gefüllt
	// die einzelnen Elemente stehen für die Spalte, d.h. $datum($m), für $m=-1 (gestern) ...$m < $zahl (Anzahl der anzuzeigenden Tage)
	// für jeden gefundenen Datensatz wird das entsprechende Element um 1 erhöht
	// dies wird einmal pro Zeile gemacht, ansonsten müsste die Prozedur in jeder Zelle wiederholt werden
	
	for ($n = "0"; $n < ($zahl+1); $n++) {																// Initialisierung des Arrays "Teiler" für die Teilung der Zellen für Mehrfachtermine
		$teiler[$n] = "0";
	}
	
	for ($m = $vergangen; $m < $zahl; $m++) { 																	// Schleife für Tage (gestern + 14 Tage)
		$timestamp = mktime(0,0,0,date("m"), date("d")+$m);
		$datum = (strftime("%Y", $timestamp)) . "-" .(strftime("%m", $timestamp)) . "-" . (strftime("%d", $timestamp));
		
		if ($datensatz > 0) {																			// überhaupt Termine da?
		
			for ($p = 0; $p < $datensatz; $p++) {	// 

				$termine = mysqli_fetch_row($ergebnis);
				
				if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0')) {// Vergleich Datum mit Termin und Zeit, Sperrzeiten ausgeschlossen
				
					$teiler[($m+1)] = $teiler[$m+1] + 1;												// da $m von -1 läuft, muss Zähler im Array um 1 erhöht werden (läuft von 0)
				}
			}
			mysqli_data_seek($ergebnis, '0');													// Rücksetzen der Ergebnisliste für nächste Zeile (Zeit)
		}
	}																									// Ende Schleife Tage
	
	// Ende Ermittlung Termin-Mehrfachbelegung
	// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

	// Auswertung der Daten und Darstellung in Kalenderform beginnt

	echo "<tr>";
	echo "<td class=\"zeiten\">$t_zeit[$j]</td>";
	
	for ($k = $vergangen; $k < $zukunft; $k++) { // Erzeugung der Zellen Termine von gestern bis in 13 Tagen
	
		// Hier muss die Abfrage des Arrays rein
		// Zeiten: j = zeile 0 = 9.00 Uhr, (zahl_t_zeit-1) = 20.00
		// Datum: k = Spalte k=0: heute, k=$zahl: in 14 Tagen
		// das Array heißt $termine, Datum = $termine[1], zeit = $termine[2]
		// Lösung: Suche im Array nach der Zeit $t_zeit[$j]
		// und dann nach Datum heute + $k
	
		$termin = mktime(0,0,0,date("m"), date("d")+$k);
		//setlocale (LC_TIME, 'ge');
		setlocale (LC_TIME, 'de_DE');// deutsche Benutzerumgebung eingestellt
		date_default_timezone_set("Europe/Berlin");
		$datum = (strftime("%Y", $termin)) . "-" .(strftime("%m", $termin)) . "-" . (strftime("%d", $termin));
		$wochentag = (strftime("%a", $termin));
		
		if ($datensatz > 0) {													// überhaupt Termine da?
				
			for ($l = 0; $l < $datensatz; $l++) {								// zeilenweises Auslesen der Datenbank-Query

				$termine = mysqli_fetch_row($ergebnis);
		
				
				if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j])) {	// Datum und Zeit stimmen überein
				
					if ($termine[3] == 1) {										// Termin ist Sperrzeit
						echo "<td class =\"sperrzeit\">S</td>";
					}															// Ende Sperrzeit
					
					elseif ($teiler[($k+1)] > 1) {								// mehrere Termine auf einer Zeit
					
						mysqli_data_seek($ergebnis, '0');
					
						echo "<td class =\"doppelt\">";
						echo "<table width = \"100%\" cellspacing = \"0\" cellpadding = \"0\">";
		
							for ($p = 0; $p < $datensatz; $p++) {					// alle Datensätze überprüfen

								$termine = mysqli_fetch_row($ergebnis);
				
								if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[8])) AND ($termine[9] == '0')) {		// Abschluss
									echo "<tr>";
									echo "<td class =\"abschluss\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[8])) AND ($termine[9] == '1')) {		// kalter Abschluss
									echo "<tr>";
									echo "<td class =\"test\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '0') AND ($termine[6] == '0') AND (empty($termine[7]) OR ($termine[7] == '0000-00-00')) AND (empty($termine[8])) AND ($termine[9] == '0')) {	// offener Termin
									echo "<tr>";
									echo "<td class =\"toa\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[6] == '1')) {		// Nichtkunde
									echo "<tr>";
									echo "<td class =\"nichtkunde\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[5] == '0') AND ($termine[6] == '0') AND (empty($termine[8])) AND ($termine[9] == '0') AND $termine[7] > '2000-01-01') {	// bearbeitet
									echo "<tr>";
									echo "<td class =\"toas\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
									}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND (!empty($termine[8]))) {		// storno
									echo "<tr>";
									echo "<td class =\"storno\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[9] == '1')) {		// kalt
									echo "<tr>";
									echo "<td class =\"kalt\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
							}
							
						echo "</table>";
						echo "</td>";
					} // ende mehrere Termine auf einer Zeit
			
					else { // nur ein Termin
					
						if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[8])) AND ($termine[9] == '0')) {		// Abschluss
							echo "<td class =\"abschluss\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[8])) AND ($termine[9] == '1')) {		// kalter Abschluss
							echo "<td class =\"test\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '0') AND ($termine[6] == '0') AND (empty($termine[7]) OR ($termine[7] == '0000-00-00')) AND (empty($termine[8])) AND ($termine[9] == '0')) {	// offener Termin
							echo "<td class =\"toa\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[6] == '1')) {		// Nichtkunde
							echo "<td class =\"nichtkunde\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[5] == '0') AND ($termine[6] == '0') AND ($termine[7] > '2000-01-01') AND (empty($termine[8])) AND ($termine[9] == '0')) {	// bearbeitet
							echo "<td class =\"toas\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND (!empty($termine[8]))) {		// storno
							echo "<td class =\"storno\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[9] == '1')) {		// kalt
							echo "<td class =\"kalt\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aq_rechts\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
					}		// Ende else nur ein Termin
				$treffer = "1";
			}
		}
		if (mysqli_num_rows($ergebnis) > 0) {
				mysqli_data_seek($ergebnis, '0');
			}
			if ($treffer == 0) {
				if ($wochentag === "Sun" OR $wochentag === "So") {
					echo "<td class=\"sonntag\">&nbsp;</td>";
				}
				elseif ($wochentag == "Sat" OR $wochentag == "Sa") {
					echo "<td class=\"samstag\">F</td>";
				}
				else {
					echo "<td class =\"zeiten\">F</td>";
				}
			}
			else {
				$treffer = "0";
			}
		} // ende if überhaupt Termine da?
		
		else {
			if ($wochentag === "Sun" OR $wochentag === "So") {
				echo "<td class=\"sonntag\">&nbsp;</td>";
			}
			elseif ($wochentag == "Sat" OR $wochentag == "Sa") {
				echo "<td class=\"samstag\">F</td>";
			}
			else {
				echo "<td class =\"zeiten\">F</td>";
			}
		
		}
		
	}
	//echo "<td class=\"zeiten\">$t_zeit[$j]</td>";
	
	echo "</tr>";
	
} // ende for zeilen

echo "<tr><td colspan=\"$span\">";
?>
<form name="user_neu" method="post" action="<?php $PHP_SELF ?>">
	<table>
		<tr>
			<td><input type="submit" name="speichern" value="REFRESH" class = "submitt"></td>
		</tr>
	</table>
</form>
</td></tr></table>
</div>
</body>
</html>