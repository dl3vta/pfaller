<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();

if (isset($speichern)) {																		// Speichern-Button wurde gedrückt
	if (empty($gruppen)) {
		$fehler = "Fehler: Die Benutzergruppe kann nicht leer sein!";				// kein User-Name oder Passwort
	}
	
	else { 	// neue Gruppe -----------------------------------------------------------------------------------------------------------
	
		$gruppen = quote_smart($gruppen);
		$sql = "INSERT INTO gruppen (gruppen) VALUES ('$gruppen') ";
		$abfrage = myqueryi($db, $sql);
		$gruppen_id = mysqli_insert_id($db);
		$fehler = "Die Gruppe \"$gruppen\" mit der ID: $gruppen_id wurde eingefügt";

	} // Ende ELSE neue Gruppe
}	// Ende IF ISSET speichern


if (isset($update)) {
	if (empty($gruppen)) {
		$fehler = "Der Gruppenname kann nicht leer sein!!";
	}
	else {
		$gruppen = quote_smart($gruppen);
		$sql = "UPDATE gruppen SET gruppen = '$gruppen' WHERE gruppen_id = '$gruppen_id'";
		$ergebnis = myqueryi($db, $sql);
	}
} // Ende IF UPDATE

//---------------------------------------------------------------------------------------------------------------------------------------

// Start Script-Ablauf vor dem Drücken des Speichern-Buttons

	$sql  = "SELECT gruppen_id AS ID, gruppen AS Gruppe ";
	$sql .= "FROM gruppen  ";
	$ergebnis = myqueryi($db, $sql);
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="db">
<head>
<title>Preisagentur: Gruppen-Registrierung</title>
	<!-- admin/gruppen_registration.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<table width="600" border="0" cellpadding="8" cellspacing="8">
<tr>
<td><br><br><h2 class="Stil1">Gruppen bearbeiten</h2><td>
</tr>
<tr><td>
<table width="80%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">
<?php
echo "<table id=\"ausgabe\"  cellspacing=\"8\" >";

//Ausgabe der bereits vorhandenen Gruppen +++++++++++++++++++++++++++++++++++++++++++++++

	echo "<tr><strong>vorhandene Gruppen:</strong></tr>";
	echo "<tr>";													// Tabellenkopf
	for($i = 0; $i < mysqli_num_fields($ergebnis); $i++)				// Anzahl der Tabellenzellen pro Zeile
	{
		$row = mysqli_fetch_assoc($ergebnis);
		$feldname = array_keys($row);						// Name der Tabellenzelle
		mysqli_data_seek($ergebnis, 0);
		echo "<td><b>$feldname[$i]</b></td>";
	}
	echo "<td>&nbsp;</td>";											// eine Zelle für den BEARBEITEN-Button angehängt
	echo "</tr>\n";													// Tabellenkopf Ende
	for ($j = 0; $j < mysqli_num_rows($ergebnis); $j++)				// Anzahl der Datensätze
	{
		$zeile = mysqli_fetch_row($ergebnis);						// Schleife für Daten-Zeilen
			echo "<tr>";
			for($i = 0; $i < mysqli_num_fields($ergebnis); $i++)			// Schleife für Felder
			{	
				echo "<td>" . $zeile[$i] . "</td>";
			}					
			echo "<td>";															// die Zelle Bearbeiten wird 
			echo '<a href="gruppen_update.php?id=', urlencode($zeile[0]), '">';		// mit dem Link zum Bearbeiten-Script
			echo "BEARBEITEN</a></td></tr>";										// + angehängter ID gefüllt
    }
	echo "</table>";
// Ende Ausgabe vorhandene Gruppen +++++++++++++++++++++++++++++++++++++++++++++++
?>
</td>
<td valign = "top">
<!-- Start Formular für neue Gruppen ++++++++++++++++++++++++++++++++++ // -->
<form name="user_neu" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
<table id="formular"   cellspacing="8" >
<tr><td colspan = "6"><strong>Eingabe neue Gruppe:</strong></td></tr>
  <tr>
    <td>Gruppe:</td>
    <td><input type="text" name="gruppen"></td>
    <tr>
    <td colspan ="6"><input type="submit" name="speichern" value="Speichern" class = "submitt"></td>
    </tr>
</table>      
</form>
<!-- Ende Formular für neue Gruppen ++++++++++++++++++++++++++++++++++ // -->
</td>
</tr>
<?php
if ($fehler) {
	echo "<tr><td colspan = \"2\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold;\">$fehler</span></td></tr>";
}
?>
</table>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>