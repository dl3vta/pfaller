<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/mysqlconnect.inc.php");
include ("../include/variablen.php");
include ("../include/funktionen.php");

sessiondauer();

//$aussendienst = quote_smart($_GET["aussendienst"]);		// kommt von der Startseite
//$typ = quote_smart($_GET["typ"]);							// kommt von der Startseite
//$ad = $_SESSION['aussendienst'];

if (isset($_GET['aussendienst'])) {
	$aussendienst = quote_smart($_GET['aussendienst']);
}

if (isset($_GET['typ'])) {
	$typ = quote_smart($_GET['typ']);

	if ($typ == 2) {		// Außendienstler belegen
		$sql = "UPDATE user SET belegt = NULL, belegt_von = NULL ";
		$sql .= "WHERE user = :aussendienst";
		$statement = $pdo->prepare($sql);
		$statement->execute(array(':aussendienst' => $aussendienst));

		if (!$statement->rowCount()) {
			echo "SQL Error <br />";
			echo $statement->queryString . "<br />";
			echo $statement->errorInfo()[2];
		}
	}
}

$heute = date("Y-m-d");
$tage = "5"; // anzahl der anzuzeigenden Tage
$max = "23"; // maximale Anzahl von Terminen pro Tag
$span = $tage + 3;										// für Tabelle : anzahl der Tage + 1 Spalte für Aussendienstler + Spalte termin + Spalte freigeben= colspan

// Ermittlung der Außendienster = Zeilen der tabelle
$sql = "SELECT user, belegt_von FROM user WHERE gruppen_id = '3' ORDER BY user ASC";
$statement = $pdo->prepare($sql);
$statement->execute();


if($statement->execute()) {
	$zeilen = $statement->rowCount();
} else {
	echo "SQL Error <br />";
	echo $statement->queryString."<br />";
	echo $statement->errorInfo()[2];
}

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Start</title>
	<!-- admin/admin_content.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">

	<table width="700" border="0" cellpadding="8" cellspacing="8">
		<tr>
			<td>
				<table width="80%" cellpadding="1" cellspacing="0" bgcolor="#000000">
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#eeeeee">
								<tr>
									<td valign = "top" align="center">
										<table>
											<tr>
												<td valign = "top" align="center"><!-- Zelle zur Auflistung der Wiedervorlagen -->
			 <?php
														echo "<table border=\"1\" width=\"700\" class=\"zeiten\" cellspacing=\"0\" cellpadding=\"0\">";
															echo "<tr bgcolor=\"#006699\">
																	<td colspan='$span'><div style='text-align: center; font-size: 1.5em; font-weight: bold; color: white '>noch freie Termine</div></td>
																</tr>";
															echo "<tr>
																<td class=\"spalten\">&nbsp;</td>";
																for ($i= "0"; $i < $tage; $i++) { 				// Termine 
																	$datum = datum($i);
																	echo "<td class=\"spalten\" >$datum</td>";
																}
															echo "</tr>";
															for ($j = 0; $j < $zeilen; $j++) { // Erzeugung der Zeilen
																$aussendienst = $statement->fetch();

																$sql = "SELECT user_id, belegt FROM user WHERE user = '$aussendienst[0]' AND belegt = '$heute'";
																$sth = $pdo->prepare($sql);
																$sth->execute();

															echo "<tr>";
																if($sth->rowCount() > 0) {	// Aussendienstler bereits in bearbeitung
																	$farbe = "red";
																}
																else {								// nicht in Bearbeitung
																	$farbe = "green";
																}
																echo "<td class=\"zeiten\"><span style=\"font-weight:bold; color:$farbe;\">$aussendienst[0]<span></td>";
																	for ($k = 0; $k <$tage; $k++) { 												// Erzeugung der Datumszellen für den Aussendienstler
																		$termin = mktime(0,0,0,date("m"), date("d")+$k);
																		$datum = (strftime("%Y", $termin)) . "-" .(strftime("%m", $termin)) . "-" . (strftime("%d", $termin));
																		$wochentag = (strftime("%a", $termin));
																		if ($wochentag === "Sun" OR $wochentag === "So") {
																			echo "<td class=\"sonntag\">&nbsp;</td>";
																		} // ende if wochentag
																		elseif ($wochentag == "Sat" OR $wochentag == "Sa") {
      																		$sql = "SELECT COUNT(termin_id) FROM termin WHERE termin = '$datum' AND aussendienst = '$aussendienst[0]' AND alt = '0' ";
																			$sth = $pdo->prepare($sql);
																			$sth->execute();
																			$termine = $sth->fetch();
																			$freie_termine = $max - $termine[0];
																			echo "<td class=\"samstag\"<span style=\"color:$farbe;\">$freie_termine</span></td>";
																		} // ende elseif wochentag
																		else { // Abfrage Termine + Sperrzeiten für Außendienstler
																			$sql = "SELECT COUNT(termin_id) FROM termin WHERE termin = '$datum' AND aussendienst = '$aussendienst[0]' AND alt = '0' ";
																			$sth = $pdo->prepare($sql);
																			$sth->execute();
																			$termine = $sth->fetch();
																			$ergebnis = myquery($sql, $conn);
																			$freie_termine = $max - $termine[0];
																			echo "<td class =\"zeiten\"><span style=\"color:$farbe;\">$freie_termine</span></td>";
																		} // ende else Sperrzeiten
																	}
																echo "<td class =\"zeiten\"><a href=\"../allgemein/aquiseframe.php?aussendienst=$aussendienst[0]&typ=1\" target=\"_self\"><span style=\"font-weight:bold; color:red;\">bearbeitet von: $aussendienst[1]</span></a></td>";
																echo "<td class =\"zeiten\"><a href=\"admin_content.php?aussendienst=$aussendienst[0]&typ=2\" target=\"_self\"><span style=\"font-weight:bold; color:green;\">freigeben</span></a></td>";
																}
															echo "</tr>";
														echo "</table>";
													?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
</body>
</html>
