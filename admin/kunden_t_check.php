<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();

$kunden_id = quote_smart($_GET["kd_id"]);

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Preisagentur: Termine für Kunde</title>
	<!-- admin/kunden_t_check.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<table width="900" border="0" cellpadding="1" cellspacing="1">
<tr>
<td><h2 class="Stil1">alle Vorgänge für Kunde:</h2><td>
</tr>
<tr><td>
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">

<?php
// Daten Kunden
$sql = "SELECT kunden_id AS K_ID, vorname AS Vorname, name AS Name, plz AS PLZ, ort AS Ort, Ortsteil AS OT, strasse AS Strasse, ";
$sql .= "vorwahl1 AS Vorwahl, telefon AS Telefon, vorwahl2 AS Vorwahl2, fax AS Telefon2, email AS Mail, branche AS Branche, geboren AS geboren, rem_kunde AS Bemerkung ";

$sql .= "FROM kunden, vorname, name, poo, plz, ort, ortsteil, vorwahl1, vorwahl2, branche ";

$sql .= "WHERE kunden.kunden_id = '$kunden_id'  ";
$sql .= "And kunden.vorname_id = vorname.vorname_id  AND kunden.name_id = name.name_id ";
$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
$sql .= "And kunden.vorwahl1_id = vorwahl1.vorwahl1_id And kunden.vorwahl2_id = vorwahl2.vorwahl2_id And kunden.branche_id = branche.branche_id ";

$ergebnis = myqueryi($db, $sql);

// Daten Termine
$sql = "SELECT termin_id AS T_ID, aquiriert AS angerufen, telefonist AS Tel, termin AS Termin, zeit AS Zeit, erledigt_date AS erledigt, aussendienst AS Aus, termin.wiedervorlage_date AS Wiedervorlage, ";
$sql .= "ges_alt AS Ges_alt, tarif_alt AS Tarif_alt, von AS von, bis AS bis, ges_neu AS Ges_neu, tarif_neu AS Tarif_neu, seit AS seit, vonr AS VoNr, rem_termin AS Bemerkung ";

$sql .= "FROM kunden, termin, produkt_alt, ges_alt, tarif_alt, produkt_neu, ges_neu, tarif_neu ";

$sql .= "WHERE termin.kd_id = '$kunden_id' ";
$sql .= "And termin.produkt_alt_id = produkt_alt.produkt_alt_id  And produkt_alt.ges_alt_id = ges_alt.ges_alt_id And produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
$sql .= "And termin.produkt_neu_id = produkt_neu.produkt_neu_id  And produkt_neu.ges_neu_id = ges_neu.ges_neu_id And produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";

$abfrage = $ergebnis = myqueryi($db, $sql);

//Kundendaten - nur 1 Mal durchlaufen
	echo "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">";
	echo "<tr>";													
	for($i = 0; $i < 14; $i++)			// Anzahl der Tabellenzellen pro Zeile - Bemerkung auf neue Zeile
	{
		$feldname = mysqli_field_name($ergebnis, $i);				// Name der Tabellenzelle
		echo "<td><b>$feldname</b></td>";
	}
	echo "</tr>\n";													// Tabellenkopf Ende
	echo "</tr>\n";													// Tabellenkopf Ende
	
	$z=0;  //zähler der datensätze für bg_colour der zeilen
	$bg1 = "#eeeeee"; //die beiden hintergrundfarben
	$bg2 = "#dddddd";
	
	echo "<tr><td colspan = \"14\"><hr></td></tr>";
	$kunde = mysqli_fetch_row($ergebnis);							// 1 Datenzeile = 1 Kunde
	echo "<tr>";
	for($i = 0; $i < 14; $i++)			// Schleife für Felder
	{	
		if (empty($kunde[13]) OR $kunde[13] == "0000-00-00") {
			$zeile[13] = "";
		}
		else {
			$kunde[13] = mysqldate_in_de($kunde[13]);
		}
		
			
		$bg=($z++ % 2) ? $bg1 : $bg2;
		echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
		
		for($i = 0; $i < 14; $i++)			// Schleife für Felder - Bemerkung auf neue Zeile
		{	
			echo "<td>" . $kunde[$i] . "</td>";
		}
		echo "</tr>";
		echo "<tr>";
		echo "<td colspan = \"6\">&nbsp;</td>";
		echo "<td colspan = \"8\"><b>$kunde[14]</b></td>";
		echo "</tr>";
		echo "<tr><td colspan = \"14\"><hr></td></tr>";
    }
	echo "</tr>";
	echo "</table>";
// Ende Kundendaten
echo "</td></tr>";
echo "<tr><td>";

// -----------------------------------------------------------------------------------------------------------------------
//Termindaten
// -----------------------------------------------------------------------------------------------------------------------


	echo "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">";
	echo "<tr>";													
	for($i = 0; $i < 15; $i++)			// Anzahl der Tabellenzellen pro Zeile - Bemerkung auf neue Zeile
	{
		$row = mysqli_fetch_assoc($ergebnis);		// Name der Tabellenzelle
		$spalte = array_keys($row);
		mysqli_data_seek($ergebnis, 0);
		echo "<td><b>$spalte[$i]</b></td>";
	}
	echo "</tr>\n";													// Tabellenkopf Ende
	echo "</tr>\n";													// Tabellenkopf Ende
	
	$z=0;  //zähler der datensätze für bg_colour der zeilen
	$bg1 = "#eeeeee"; //die beiden hintergrundfarben
	$bg2 = "#dddddd";
	
	echo "<tr><td colspan = \"15\"><hr></td></tr>";
	$zeile = mysqli_fetch_row($abfrage);							// 1 Datenzeile = 1 Kunde
	echo "<tr>";
	for($i = 0; $i < 15; $i++)			// Schleife für Felder
	{	
		if (empty($zeile[3]) OR $zeile[3] == "0000-00-00") {	// Termin
			$zeile[3] = "";
		}
		else {
			$zeile[3] = mysqldate_in_de($zeile[3]);
		}
		
		if (empty($zeile[5]) OR $zeile[5] == "0000-00-00") {	//erledigt
			$zeile[5] = "";
		}
		else {
			$zeile[5] = mysqldate_in_de($zeile[5]);
		}
		
		if (empty($zeile[7]) OR $zeile[7] == "0000-00-00") {	//wiedervorlage
			$zeile[7] = "";
		}
		else {
			$zeile[7] = mysqldate_in_de($zeile[7]);
		}
		
		if (empty($zeile[10]) OR $zeile[10] == "0000-00-00") {	//von
			$zeile[10] = "";
		}
		else {
			$zeile[10] = mysqldate_in_de($zeile[10]);
		}
		
		if (empty($zeile[11]) OR $zeile[11] == "0000-00-00") {	//bis
			$zeile[11] = "";
		}
		else {
			$zeile[11] = mysqldate_in_de($zeile[11]);
		}
		
		if (empty($zeile[14]) OR $zeile[14] == "0000-00-00") {	//seit
			$zeile[14] = "";
		}
		else {
			$zeile[14] = mysqldate_in_de($zeile[14]);
		}
		
		$bg=($z++ % 2) ? $bg1 : $bg2;
		echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
		
		for($i = 0; $i < 15; $i++)			// Schleife für Felder - Bemerkung auf neue Zeile
		{	
			echo "<td class=\"liste\" align = \"left\"><a href=\"kunden_t_neu.php?t_id=$zeile[0]&kd_id=$kunde[0]\" target=\"kd_rechts\">" . $zeile[$i] . "</a></td>";
			
		}
		echo "</tr>";
		echo "<tr>";
		echo "<td colspan = \"6\">&nbsp;</td>";
		echo "<td colspan = \"7\"><b>$zeile[16]</b></td>";
		echo "</tr>";
    }
	echo "</tr>";
	echo "</table>";
?>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</body>
</html>