<?php
include ("include/ini.php");		// Session-Lifetime

session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();

// Variablendefinition ----------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------

$w1_zeit = $_POST["w1_zeit"];				// Wiedervorlage

//$d1_tag = $_POST["d1_tag"];				// Termin (d = Datum)
//$d1_monat = $_POST["d1_monat"];			// Termin (d = Datum)
//$d1_jahr = $_POST["d1_jahr"];				// Termin (d = Datum)
$d1_zeit = $_POST["d1_zeit"];				// Termin (d = Datum)

$kunden_id = $_GET["kd_id"];				// kommt von termin_check.php
$t_id = $_GET["t_id"];						// kommt von termin_check.php

$telefonist = $_SESSION['benutzer_kurz'];
$benutzer_id = $_SESSION['benutzer_id'];
$benutzer_name = $_SESSION['benutzer_name'];
$benutzer_gruppen = $_SESSION['benutzer_gruppen'];
$aussendienst = $_SESSION['aussendienst'];
$aussen_id = $_SESSION['aussen_id'];
$kundentyp = $_SESSION['kundentyp'];

$t_zeit = array("09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", 
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");			// Array für Zeiten
				
$wvl_zeit = array("", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", 
				"15:00", "16:00", "17:00", "18:00", "19:00", "20:00");			// Array für Wiedervorlage-Zeiten

$termin_id = $t_id;		// kommt vom Kalender

$sql = "SELECT termin_id FROM termin WHERE termin.kd_id = '$kunden_id' ";	// existieren schon Termine?
$vorgang = myqueryi($db, $sql);

// Daten kommen vom Kalender
if ($t_id != 0) {
	$sql = "SELECT kd_id, termin, zeit, rem_termin, ges_alt, tarif_alt, ges_neu, tarif_neu, von, bis, seit ";
	$sql .= " FROM termin, produkt_alt, ges_alt, tarif_alt, produkt_neu, ges_neu, tarif_neu ";
	$sql .= " WHERE termin.termin_id = '$t_id' ";
	$sql .= " AND termin.produkt_alt_id = produkt_alt.produkt_alt_id AND produkt_alt.ges_alt_id = ges_alt.ges_alt_id ";
	$sql .= " AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
	$sql .= " AND termin.produkt_neu_id = produkt_neu.produkt_neu_id AND produkt_neu.ges_neu_id = ges_neu.ges_neu_id ";
	$sql .= " AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
	$abfrage = myqueryi($db, $sql);
	$termin = mysqli_fetch_array($abfrage, MYSQLI_ASSOC);
	$kunden_id = $termin[kd_id];
	
	$sql  = "SELECT vorname, name, vorwahl1, telefon, plz, ort, ortsteil, strasse, fax, email, rem_kunde, geboren, ";
	$sql  .= "einzug, nichtkunde, belegt, wiedervorlage, vertragskunde, vorwahl2, fax, branche ";
	$sql .= "FROM kunden, vorname, name, vorwahl1, poo, plz, ort, ortsteil, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$termin[kd_id]' AND kunden.vorname_id = vorname.vorname_id ";
	$sql .= "AND name.name_id = kunden.name_id AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	$sql .= "AND kunden.vorwahl2_id = vorwahl2.vorwahl2_id AND kunden.branche_id = branche.branche_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$ergebnis = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($ergebnis, MYSQLI_ASSOC);
}

// -----------------------------------------------------------------------------------------------------------------------------------

// Daten kommen aus der Telefonliste

elseif ($kunden_id != 0) {
	$sql  = "SELECT vorname, name, vorwahl1, telefon, plz, ort, ortsteil,  strasse, fax, email, rem_kunde, geboren, ";
	$sql  .= "einzug, nichtkunde, belegt, wiedervorlage, vertragskunde, vorwahl2, fax, branche ";
	$sql .= "FROM kunden, vorname, name, vorwahl1, poo, plz, ort, ortsteil, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id' AND kunden.vorname_id = vorname.vorname_id ";
	$sql .= "AND name.name_id = kunden.name_id AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	$sql .= "AND kunden.vorwahl2_id = vorwahl2.vorwahl2_id AND kunden.branche_id = branche.branche_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";

	$ergebnis = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($ergebnis, MYSQLI_ASSOC);
	
	$sql = "SELECT termin_id, rem_termin, wiedervorlage_date, w_zeit, ges_alt, ges_neu, tarif_neu, erledigt_date FROM termin, ges_alt, produkt_alt, produkt_neu, ges_neu, tarif_neu ";
	$sql .= "WHERE termin.kd_id = '$kunden_id' ";
	$sql .= "AND termin.produkt_alt_id = produkt_alt.produkt_alt_id AND produkt_alt.ges_alt_id = ges_alt.ges_alt_id ";
	$sql .= " AND termin.produkt_neu_id = produkt_neu.produkt_neu_id AND produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
	
	switch($kundentyp) {
	case Vertragskunde:
		$sql .= " AND abschluss = '1' ORDER BY erledigt_date DESC";	
		break;
	case Wiedervorlage:
		$sql .= " AND termin.wiedervorlage = '1' AND termin.alt = '0' ";
		break;
	case Nichtkunde:
		//$sql .= " AND kunden.nichtkunde ='1' AND kunden.verzogen = '0'  AND kunden.belegt = '0' AND kunden.ne != '$heute' ";
		break;
	case Handy:
		$sql .= " AND kunden.handy = '1'";	
		break;
	//case ne:
		$sql .= " AND termin.abschluss = '1' AND termin.alt = '0' ";
		//$sql .= " (AND termin.abschluss = '1' OR termin.wiedervorlage = '1' OR kunden.handy = '1') AND termin.alt = '0' ";
		//break;
	}
	
	$abfrage = myqueryi($db, $sql);	
	$termin = mysqli_fetch_array($abfrage, MYSQLI_ASSOC);
	
	list($w_jahr, $w_monat, $w_tag) = explode("-", $termin[wiedervorlage_date]);		// Datum
	$w_jahr = substr($w_jahr, 2, 2);
	$w_zeit = $termin[w_zeit];
	
	$w_jahr_alt = $w_jahr;
	$w_monat_alt = $w_monat;
	$w_tag_alt = $w_tag;
	$w_zeit_alt = $termin[w_zeit];
	
	if ($termin) {
		mysqli_data_seek($abfrage, 0);
	}
}

	$einzug = $kunde[einzug];
	$nichtkunde = $kunde[nichtkunde];
	$t_bemerkung = $kunde[rem_kunde];
	$t_ortsteil = $kunde[ortsteil];
	$t_ort = $kunde[ort];
	$t_branche = $kunde[branche];
	$t_vorname = $kunde[vorname];
	$t_name = $kunde[name];
	$t_strasse = $kunde[strasse];
	$t_vorwahl2 = $kunde[vorwahl2];
	$t_fax = $kunde[fax];
	$t_email = $kunde[email];
	list($g_jahr, $g_monat, $g_tag) = explode("-", $kunde[geboren]);
	$g_jahr = substr($g_jahr, 2,2);
	
	$rem_termin = $termin[rem_termin];
	list($v_jahr, $v_monat, $v_tag) = explode("-", $termin[von]);
	$v_jahr = substr($v_jahr, 2, 2); 
	list($b_jahr, $b_monat, $b_tag) = explode("-", $termin[bis]);
	$b_jahr = substr($b_jahr, 2, 2); 
	list($s_jahr, $s_monat, $s_tag) = explode("-", $termin[seit]);
	$s_jahr = substr($s_jahr, 2, 2);
	
	list($d_jahr, $d_monat, $d_tag) = explode("-", $termin[termin]);		// Datum
	$d_jahr = substr($d_jahr, 2, 2);
	$d_jahr_alt = $d_jahr;
	$d_monat_alt = $d_monat;
	$d_tag_alt = $d_tag;
	$zeit_alt = $termin[zeit];
	
	$d_zeit = $termin[zeit];
	
	$ges_alt_alt = $termin[ges_alt];
	
// -----------------------------------------------------------------------------------------------------------------------------------
// es wurde der "Speichern"-Button gedrückt
// POST-Variablen werden zwecks Anzeige nach dem Wiederaufbau des Formulars in die entsprechenden Formularvariablen gespeichert
// -----------------------------------------------------------------------------------------------------------------------------------

if (isset($t_speichern)) {								// Termin speichern wurde gedrückt

$heute = date("Y-m-d");			// Datum von heute
$aquiriert = date("Y-m-d H:i");

$t_vorname = quote_smart($kd_vorname);
$t_name = quote_smart($kd_name);
$g_tag = quote_smart($g1_tag);
$g_monat = quote_smart($g1_monat);
$g_jahr = quote_smart($g1_jahr);
$t_strasse = quote_smart($t1_strasse);
$t_vorwahl2 = quote_smart($t1_vorwahl2);
$t_fax = quote_smart($t1_fax);
$t_email = quote_smart($t1_email);
$t_branche = quote_smart($t1_branche);
$t_ortsteil_neu = quote_smart($ortsteil_neu);
$t_ortsteil = quote_smart($ortsteil_alt);
$ges_alt_alt = quote_smart($ges_alt_alt1);

$w_tag = quote_smart($w1_tag);
$w_monat = quote_smart($w1_monat);
$w_jahr = quote_smart($w1_jahr);
$w_zeit = quote_smart($w1_zeit);			// Wiedervorlage
		
$d_tag = quote_smart($d1_tag);
$d_monat = quote_smart($d1_monat);
$d_jahr = quote_smart($d1_jahr);
$d_zeit = quote_smart($d1_zeit);			// Termin (d = Datum)

$rem_termin = quote_smart($rem1_termin);
$t_bemerkung = quote_smart($t1_bemerkung);

if (isset($einzug1)) {
	$einzug = "1";
}
else {
	$einzug = "0";
}

if (isset($nichtkunde1)) {
	$nichtkunde = "1";
}
else {
	$nichtkunde = "0";
}

if (isset($doppelt1)) {
	$doppelt = "1";
}
else {
	$doppelt = "0";
}

if (isset($verzogen1)) {
	$verzogen = "1";
}
else {
	$verzogen = "0";
}

if (isset($ne1)) {
	$ne = "1";
}
else {
	$ne = "0";
}

/*
echo "Kunden-Id: $kunden_id<br>";
echo "Kunden-Id: $kd_id<br>";
echo "WVL: $termin[wiedervorlage_date]<br>";
echo "Tag-alt: $w_tag_alt<br>";
echo "Tag-neu: $w_tag<br>";
echo "Monat-alt: $w_monat_alt<br>";
echo "Monat-neu: $w_monat<br>";
echo "Jahr-alt: $w_jahr_alt<br>";
echo "Jahr-neu: $w_jahr<br>";
echo "Vorwahl: $kunde[vorwahl1]<br>";
echo "Telefon: $kunde[telefon]<br>";
echo "Zeit: $d_zeit<br>";
echo "vorwahl2 = $t_vorwahl2<br>";
echo "fax = $t_fax<br>";
echo "email = $t_email<br>";
echo "t_branche = $t_branche<br>";
echo "rem_termin = $rem_termin<br>";
echo "bemerkung = $t_bemerkung<br>";
echo "g_tag = $g1_tag<br>";
echo "g_monat = $g1_monat<br>";
echo "g_jahr = $g1_jahr<br>";
*/

	// Ortsteil eingegeben -----------------------------------------------------------------------------------------------------------------
		
				if (empty($ortsteil_neu)) {							// keine Eingabe im Feld neuer Ortsteil
					$ortsteil_alt = quote_smart($ortsteil_alt);
					if ($ortsteil_alt == $kunde[ortsteil]) {		// alter Ortsteil
						if (empty($ortsteil_alt)) {					// kein OT eingegeben - ortsteil_id = 1
							$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
							$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
							$sql .="AND ortsteil.ortsteil_id = '1' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
							$query = myqueryi($db, $sql);
							$daten = mysqli_fetch_row($query);
							$poo_id = $daten[0];
						}
						else {	//Ortsteil eingegeben
							$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
							$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
							$sql .="AND ortsteil.ortsteil = '$ortsteil_alt' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
							$query = myqueryi($db, $sql);
							$daten = mysqli_fetch_row($query);
							$poo_id = $daten[0];
						}
					}	// ende alter Ortsteil
		
					else {	// neuer Ortsteil
						if (empty($ortsteil_alt)) {					// kein OT eingegeben - ortsteil_id = 1
							$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
							$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
							$sql .="AND ortsteil.ortsteil_id = '1' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
							$query = myqueryi($db, $sql);
							$daten = mysqli_fetch_row($query);
							$poo_id = $daten[0];
						}
						else {	//Ortsteil eingegeben
							$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
							$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
							$sql .="AND ortsteil.ortsteil = '$ortsteil_alt' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
							$query = myqueryi($db, $sql);
							$daten = mysqli_fetch_row($query);
							$poo_id = $daten[0];
						}
					}	// ende else neuer Ortsteil
				} // keine Eingabe im Feld neuer Ortsteil
	
				else {	// $ortsteil_neu ist nicht leer
					$ortsteil_neu = quote_smart($ortsteil_neu);
					$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
					$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
					$sql .="AND ortsteil.ortsteil = '$ortsteil_neu' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
					$query = myqueryi($db, $sql);
					$daten = mysqli_fetch_row($query);
					if ($daten) {						// diese Kombi Ort/Ortsteil ist schon vorhanden
						$poo_id = $daten[0];
					}
					else {	// neue Kombi Ort/ortsteil
						$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil = '$ortsteil_neu'";	// ortsteil schon vorhanden?
						$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil_neu')";				// neuer Ortsteil
						$ortsteil_id = id_ermitteln($db, $sql1, $sql2);
			
						$sql = "SELECT ort_id FROM ort WHERE ort = '$kunde[ort]' ";
						$query = myqueryi($db, $sql);
						$ort = mysqli_fetch_row($query);
			
						$sql = "SELECT plz_id FROM plz WHERE plz = '$kunde[plz]' ";
						$query = myqueryi($db, $sql);
						$plz = mysqli_fetch_row($query);
			
						$sql = "INSERT INTO poo (ort_id, ortsteil_id, plz_id) VALUES ('$ort[0]', '$ortsteil_id', '$plz[0]')";
						$query = myqueryi($db, $sql);
						$poo_id = mysqli_insert_id($db);
					}	// ende neue Kombi Ort/ortsteil
				}	//ende else $ortsteil ist nicht leer
				
		
			// Ende Ortsteil -----------------------------------------------------------------------------------------------------------------------

			// Vorname ----------------------------------------------------------------------------------------------------
				$sql1 = "SELECT vorname_id FROM vorname WHERE vorname = '$t_vorname'";	// Vorname schon vorhanden?
				$sql2 = "INSERT INTO vorname (vorname) VALUES ('$t_vorname')";			// Vorname neu
				$vorname_id = id_ermitteln($db, $sql1, $sql2);;

			// Name ----------------------------------------------------------------------------------------------------

				$sql1 = "SELECT name_id FROM name WHERE name = '$t_name'";				// Name schon vorhanden?
				$sql2 = "INSERT INTO name (name) VALUES ('$t_name')";					// Name neu
				$name_id = id_ermitteln($db, $sql1, $sql2);

			// Vorwahl 2 ----------------------------------------------------------------------------------------------------

				if (empty($t_vorwahl2)) {														// Feld Vorwahl2 ist nicht leer
					$vorwahl2_id = "1";
				}
				else {
					$vorwahl2 = quote_smart($t_vorwahl2);
					$sql1 = "SELECT vorwahl2_id FROM vorwahl2 WHERE vorwahl2 = '$t_vorwahl2'";	// Vorwahl2 schon vorhanden?
					$sql2 = "INSERT INTO vorwahl2 (vorwahl2) VALUES ('$t_vorwahl2')";			// Vorwahl2 neu
					$vorwahl2_id = id_ermitteln($db, $sql1, $sql2);
				}

				// Branche ----------------------------------------------------------------------------------------------------

			if (empty($t_branche)) {
				$branche_id = "1";
			}
			else {
				$sql = "SELECT branche_id FROM branche WHERE branche = '$t_branche'";
				$abfrage_branche = myqueryi($db, $sql);
				$daten = mysqli_fetch_array($abfrage_branche);
				$branche_id = $daten[0];
			}

// geboren ----------------------------------------------------------------------------------------------------
	if (empty($g_tag) OR empty($g_monat) OR empty($g_jahr)) {
		$geboren = "0000-00-00";
	}
	else {
		if ($g_jahr > 20) {
			$geboren = "19$g_jahr-$g_monat-$g_tag";
		}
		else {
			$geboren = "20$g_jahr-$g_monat-$g_tag";
		}
	}

// Ende Ortsteil ---------------------------------------------------------------------------------------------------------------------------------------

// Bearbeitung Gesellschaft-ALT
// Eingabe von ges_alt überschreibt select-feld
	// ---------------------------------------------------------------------------------------------------------------------------------------
	
		$ges_alt = quote_smart($ges_alt);
	
		if (empty($ges_alt)) {					// keine ges_alt eingegeben
		
			if (empty($ges_alt_alt)) {	// nichts selektiert
				$produkt_alt_id = "1";			
			} //nichts selektiert
			
			else {	// Gesellschaft im Select-Feld ausgewählt
			
				$sql = "SELECT produkt_alt_id FROM produkt_alt, ges_alt ";
				$sql .= " WHERE produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND ges_alt.ges_alt = '$ges_alt_alt' AND produkt_alt.tarif_alt_id = '1' ";
				$query = myqueryi($db, $sql);
				$ergebnis = mysqli_fetch_array($query);
				$produkt_alt_id = $ergebnis[0];
				
			}  // ende else Gesellschaft im Select-Feld ausgewählt
		} // ende if keine ges_alt eingegeben
		
		else { // Gesellschaft in Textfeld eingegeben
		
			$ges_alt = quote_smart($ges_alt);
			
			$sql = "SELECT produkt_alt_id FROM produkt_alt, ges_alt ";
			$sql .= " WHERE produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND ges_alt.ges_alt = '$ges_alt' AND produkt_alt.tarif_alt_id = '1' ";
			$query = myqueryi($db, $sql);
			$ergebnis = mysqli_fetch_array($query);
					
			if ($ergebnis) {	// die Gesellschaft war schon vorhanden
					$produkt_alt_id = $ergebnis[0];
			}  // ende if die Gesellschaft war schon vorhanden
				
			else { // neue Gesellschaft
					
				$sql = "INSERT INTO ges_alt (ges_alt) VALUES ('$ges_alt')";
				$query = myqueryi($db, $sql);
				$ges_alt_id = mysqli_insert_id($id);
					
				$sql = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '1')";
				$query = myqueryi($db, $sql);
				$produkt_alt_id = mysqli_insert_id($id);
					
				// für Gesellschaft neu
					
				$sql = "INSERT INTO ges_neu (ges_neu) VALUES ('$ges_alt')";
				$query = myqueryi($db, $sql);
				$ges_neu_id = mysqli_insert_id($id);
					
				$sql = "INSERT INTO produkt_neu (ges_neu_id, tarif_neu_id) VALUES ('$ges_neu_id', '1')";
				$query = myqueryi($db, $sql);
				$produkt_neu_id = mysqli_insert_id($id);
			}  // ende else neue Gesellschaft
		} // ende else Gesellschaft in Textfeld eingegeben

// ende Gesellschaft alt

// Ende Bearbeitung Kundendaten ---------------------------------------------------------------------------------


// Nichtkunde ------------------------------------------------------------------------------------------------------------------------
	if ($nichtkunde == 1) {
	
		$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
		$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
		$sql .= " einzug = '$einzug', nichtkunde = '1', wiedervorlage = '0', vertragskunde = '0', einzug = '0', belegt = '0', neukunde = '0' ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		
		$query = myqueryi($db, $sql);
		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) wurde gesperrt";
		
		if ($t_id != 0) {			// Termin vom Kalender ausgewählt
		
			$sql = "UPDATE termin SET erledigt_date = '$heute', alt = '1', rem_termin = '$rem_termin' ";
			$sql .= "WHERE termin_id = '$t_id'";
			$query = myqueryi($db, $sql);
			
			$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, aquiriert, rem_termin, nichtkunde) ";
			$sql .= " VALUES ('$kunden_id', '$telefonist', '$aussendienst', '$aquiriert', '$rem_termin', '1')";
			$query = myqueryi($db, $sql);
		}
		else {						// Daten kommen aus der Telefonliste
		
			for ($i = 0; $i < mysqli_num_rows($abfrage); $i++) {		// Wiedervorlage auf alt setzen
				$daten = mysqli_fetch_array($abfrage);
				$sql = "UPDATE termin SET  erledigt_date = '$heute', alt = '1', rem_termin = '$rem_termin'  ";
				$sql .= "WHERE termin_id = '$daten[termin_id]'";
				$query = myqueryi($db, $sql);
				
				$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, aquiriert, rem_termin, nichtkunde) ";
				$sql .= " VALUES ('$kunden_id', '$telefonist', '$aussendienst', '$aquiriert', '$rem_termin', '1')";
				$query = myqueryi($db, $sql);
			}
		}
	} // Ende Kunde wurde gesperrt
	
	// Nichtkunde wird wieder freigegeben
	
	elseif (($nichtkunde == 0) AND ($kunde[nichtkunde] == 1)) {			
	
		$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
		$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
		$sql .= " einzug = '$einzug', nichtkunde = '0', wiedervorlage = '0', vertragskunde = '0', einzug = '0', belegt = '1', neukunde = '0' ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		
		$query = myqueryi($db, $sql);
		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) wurde wieder freigegeben";
		
		if ($t_id != 0) {			// Termin vom Kalender ausgewählt
		
			$sql = "UPDATE termin SET erledigt_date = '', nichtkunde= '0' ";
			$sql .= "WHERE termin_id = '$t_id'";
			$query = myqueryi($db, $sql);
		}
	}
	
// verzogen -------------------------------------------------------------------------------------------------------------------------
	
	elseif ($verzogen == 1) {
	
		$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
		$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
		$sql .= " einzug = '$einzug', verzogen = '1', wiedervorlage = '0', vertragskunde = '0', einzug = '0', neukunde = '0' ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		
		$query = myqueryi($db, $sql);
		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) ist verzogen";
	
		for ($i = 0; $i < mysqli_num_rows($abfrage); $i++) {		// Wiedervorlage auf alt setzen
			$daten = mysqli_fetch_array($abfrage);
			$sql = "UPDATE termin SET erledigt_date = '$heute', alt = '1'";
			$sql .= "WHERE termin_id = '$daten[termin_id]'";
			$query = myqueryi($db, $sql);
		}
	} // Ende Kunde ist verzogen
	
// nicht angetroffen -------------------------------------------------------------------------------------------------------------------------
	
	elseif ($ne == 1) {
		
		$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
		$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
		$sql .= " ne = '$heute'  ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		
		$query = myqueryi($db, $sql);
		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) wurde nicht angetroffen";
	} // Ende Kunde nicht angetroffen
	
	else { //Terminbearbeitung
		
			if (empty($d_tag) OR empty($d_monat) OR empty($d_jahr)) {		// kein Termin eingetragem
				if (empty($w_tag) OR empty($w_monat) OR empty($w_jahr)) {	// kein Wiedervorlagetermin eingetragem
		
					$fehler = "Sie müssen einen Termin/Wiedervorlagetermin eingeben!";
				}
				else {	// Wiedervorlagetermin
			
					$rem_termin = quote_smart($rem_termin);
					$t_bemerkung = quote_smart($t_bemerkung);

// Bearbeitung Wiedervorlagetermin

					$muster = "/^\d{2}$/";							// Test Datum, genau 2 Ziffern

					if(!preg_match($muster, $w_tag) OR !preg_match($muster, $w_monat) OR !preg_match($muster, $w_jahr)) {	// Muster Datum passt nicht
						$fehler = "Fehler: Kein gültiges Datum für Wiedervorlage!";
					}
					else {  // Muster wiedervorlage Datum stimmt
			
						$wiedervorlage_datum = "20$w_jahr-$w_monat-$w_tag";
					
						if ($wiedervorlage_datum < $heute) {	// Termin in der Vergangenheit!!
							$fehler = "Die Wiedervorlage kann nicht in der Vergangenheit liegen!";
						}
						
						else { // Wiedervorlage in der Zukunft
						
							if ($kundentyp = 'Wiedervorlage') {	// Kundentyp ist Wiedervorlage, in der Abfrage stehen nur Wiedervorlagen
						
								if (mysqli_num_rows($abfrage) !=0) {	// es gibt mindestens eine Wiedervorlage
								
									for ($i = 0; $i < mysqli_num_rows($abfrage); $i++) {		// Terminliste durchgehen - alle Wiedervorlage abschließen
			
										$daten = mysqli_fetch_array($abfrage);
									
										$sql = "UPDATE termin SET erledigt_date = '$aquiriert', alt = '1'  "; // Wiedervorlage-termin wird auf "alt" gesetzt
										$sql .= "WHERE termin_id = '$daten[termin_id]'";
										$query = myqueryi($db, $sql);
									} // ende for
								}	// ende if alte Wiedervorlagen vorhanden
								
								// neue Wiedervorlage einfügen!!
								
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
								$sql .= "wiedervorlage = '1', einzug = '$einzug',  belegt = '0', nichtkunde = '0', neukunde = '0', ne = '0000-00-00' ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
				
								$sql = "INSERT INTO termin (wiedervorlage_date, w_zeit, kd_id, telefonist, aussendienst, rem_termin, ";
								$sql .= "produkt_alt_id, aquiriert, wiedervorlage) ";
								$sql .= "VALUES ('$wiedervorlage_datum', '$w_zeit', '$kunden_id', '$telefonist', '$aussendienst', '$rem_termin', ";
								$sql .= "'$produkt_alt_id', '$aquiriert', '1') ";
								$query = myqueryi($db, $sql);
								$termin_id = mysqli_insert_id($db);
									
								$fehler = "Der (Wiedervorlage)-Termin mit der ID: $daten[termin_id] wurde geändert - ein Wiedervorlage-Termin mit der ID: $termin_id um w-zeit: $w_zeit wurde eingefügt";
								
							} // ende Kundentyp ist Wiedervorlage

							else { // keine alte Wiedervorlage - neue Wiedervorlage
								
									$sql = "INSERT INTO termin (wiedervorlage_date, w_zeit, kd_id, telefonist, aussendienst, rem_termin, ";
									$sql .= "produkt_alt_id, aquiriert, wiedervorlage) ";
									$sql .= "VALUES ('$wiedervorlage_datum', '$w_zeit', '$kunden_id', '$telefonist', '$aussendienst', '$rem_termin', ";
									$sql .= "'$produkt_alt_id', '$aquiriert', '1') ";
									$query = myqueryi($db, $sql);
									$termin_id = mysqli_insert_id($id);
				
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
									$sql .= "wiedervorlage = '1', einzug = '$einzug',  belegt = '0', nichtkunde = '0', neukunde = '0', ne = '0000-00-00' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
			
									$fehler = "Ein Wiedervorlage-Termin mit der ID: $termin_id wurde eingefügt";
			
								} // ende else neue Wiedervorlage
						} // ende else Termin in der Zukunft
					} // Ende else Muster wiedervorlage Datum stimmt
				}	// Ende else Wiedervorlage
			}	// kein if Termin eingegeben 
		
// Termin eingegeben ----------------------------------------------------------------------------------------------------------------------------
		
			else {	// termin eingegeben
			
			// Termindatum vorhanden, WVL-Datum eingegeben - Termin soll in WVL gewandelt werden ----------------------------------------------------------------------
			
				if($t_id AND (!empty($w_tag) AND !empty($w_monat) AND !empty($w_jahr))) { // daten kommen aus Kalender
			
				//if (!empty($w_tag) AND !empty($w_monat) AND !empty($w_jahr)) { 
							
					$muster = "/^\d{2}$/";							// Test Datum, genau 2 Ziffern

					if(!preg_match($muster, $w_tag) OR !preg_match($muster, $w_monat) OR !preg_match($muster, $w_jahr)) {	// Muster Datum passt nicht
						$fehler = "Fehler: Kein gültiges Datum für Wiedervorlage!";
					}
					else {  // Muster wiedervorlage Datum stimmt
			
						$wiedervorlage_datum = "20$w_jahr-$w_monat-$w_tag";
					
						if ($wiedervorlage_datum < $heute) {	// Termin in der Vergangenheit!!
							$fehler = "Die Wiedervorlage kann nicht in der Vergangenheit liegen!";
						}
						
						else { // Wiedervorlage in der Zukunft
						
							$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
							$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
							$sql .= "wiedervorlage = '1', einzug = '$einzug',  belegt = '0', nichtkunde = '0', neukunde = '0', ne = '0000-00-00' ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);
							
							$sql = "DELETE FROM termin WHERE termin_id = '$t_id'";		// Termin wird gelöscht
							$query = myqueryi($db, $sql);
							
							$sql = "INSERT INTO termin (wiedervorlage_date, w_zeit, kd_id, telefonist, rem_termin, ";
							$sql .= "produkt_alt_id, aquiriert, wiedervorlage) ";
							$sql .= "VALUES ('$wiedervorlage_datum', '$w_zeit', '$kunden_id', '$telefonist', '$rem_termin', ";
							$sql .= "'$produkt_alt_id', '$aquiriert', '1') ";
							$query = myqueryi($db, $sql);
							$termin_id = mysqli_insert_id();
							
							$fehler = "Der Termin ID: $t_id wurde abgeschlossen - ein WVL-Termin mit ID: $termin_id wurde eingefügt.";
						
						} // ende else WVL in der Zukunft
					} // ende else Muster WVL stimmt
			
			// Ende if Termin in WVL umwandeln -----------------------------------------------------------------------------------------------------------------------
			
			} // ende daten kommen vom Kalender
				
				else { // kein WVL-Datum eingegeben

				$muster = "/^\d{2}$/";							// Test Datum, genau 2 Ziffern
				if(!preg_match($muster, $d_tag) OR !preg_match($muster, $d_monat) OR !preg_match($muster, $d_jahr)) {	// Muster Datum passt nicht
					$fehler = "Fehler: Das ist kein gültiges Datum!!";
				}
				else {  // Muster Datum stimmt
	
					$termin_datum = "20$d_jahr-$d_monat-$d_tag";
				
					if ($termin_datum < $heute) {	// Termin in der Vergangenheit!!
						$fehler = "Der Termin kann nicht in der Vergangenheit liegen!";
					}
					
					else { // Termin in der Zukunft
					
						if ($d_tag_alt == $d_tag AND $d_monat_alt == $d_monat AND $d_jahr_alt == $d_jahr ) { // bestehenden termin bearbeiten
							if ($zeit_alt == $d_zeit) {  // termin nur bearbeiten
								
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
								$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00' ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
								
								$sql = "UPDATE termin SET termin = '$termin_datum', zeit = '$d_zeit', rem_termin = '$rem_termin', ";
								$sql .= "produkt_alt_id = '$produkt_alt_id', aquiriert = '$aquiriert' ";
								$sql .= "WHERE termin_id = '$t_id'";
								$query = myqueryi($db, $sql);
							
								$fehler = "Die Termindaten wurden geändert!";
							} // ende termin nur bearbeiten
							else { // termin soll verschoben werden, "alte" Termine werden nicht gezählt
								$sql = "SELECT termin_id FROM termin WHERE termin = '$termin_datum' AND zeit = '$d_zeit' AND aussendienst = '$aussendienst' AND alt = '0' ";		// ist der Termin schon belegt?
								$query = myqueryi($db, $sql);
								$anz_doppelt = mysqli_num_rows($query);
				
								if (($anz_doppelt > 0) AND ($doppelt == 0)) {
									$fehler = "Fehler: Der Termin ist bereits belegt - soll er mehrfach belegt werden bitte \"2x\" anklicken!!";
									$doppler = '1';			// wird im Formular ausgewertet - Zeile Doppelt = Rot
								}
								else {	// Termin ist noch frei oder soll doppelt belegt werden
							
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
									$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
								
									$sql = "UPDATE termin SET termin = '$termin_datum', zeit = '$d_zeit', rem_termin = '$rem_termin', ";
									$sql .= "produkt_alt_id = '$produkt_alt_id', aquiriert = '$aquiriert' ";
									$sql .= "WHERE termin_id = '$t_id'";
									$query = myqueryi($db, $sql);
						
									$fehler = "Der Termin mit der ID: $termin_id wurde auf $termin_datum, $d_zeit verschoben!";
								} // ende else termin noch frei
							} // ende Termin soll verschoben werden
						} // ende bestehenden termin bearbeiten
						
						else { // neue Daten für termin, "alte" Termine werden nicht gezählt
				
						$sql = "SELECT termin_id FROM termin WHERE termin = '$termin_datum' AND zeit = '$d_zeit' AND aussendienst = '$aussendienst' AND alt = '0' ";		// ist der Termin schon belegt?
						$query = myqueryi($db, $sql);
						$anzahl = mysqli_num_rows($query);
				
						if (($anzahl > 0) AND ($doppelt == 0)) {
									$fehler = "Fehler: Der Termin ist bereits belegt - soll er mehrfach belegt werden bitte \"2x\" anklicken!!";
									$doppler = '1';			// wird im Formular ausgewertet - Zeile Doppelt = Rot
						}
						else {	// Termin ist noch frei
						
							if (!empty($d_tag_alt)) { // Termin bereits vorhanden - soll verlegt werden
								
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
								$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00' ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
								
								$sql = "UPDATE termin SET termin = '$termin_datum', zeit = '$d_zeit', rem_termin = '$rem_termin', ";
								$sql .= "produkt_alt_id = '$produkt_alt_id', aquiriert = '$aquiriert' ";
								$sql .= "WHERE termin_id = '$t_id'";
								$query = myqueryi($db, $sql);
							
								$fehler = "Der Termin mit der ID: $termin_id wurde auf $termin_datum, $d_zeit verschoben!";
									
								} // ende termin verlegen
								
							elseif(mysqli_num_rows($abfrage) !=0) {	// es gibt eine Wiedervorlage
						
									$daten = mysqli_fetch_array($abfrage);
					
									$sql = "UPDATE termin SET erledigt_date = '$aquiriert', alt = '1'  ";   // Wiedervorlage-termin wird auf "alt" gesetzt 
									$sql .= "WHERE termin_id = '$daten[termin_id]'";
									$query = myqueryi($db, $sql);
										
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
									$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
					
									$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, rem_termin, ";
									$sql .= "produkt_alt_id, aquiriert) ";
									$sql .= "VALUES ('$termin_datum', '$d_zeit', '$kunden_id', '$telefonist', '$aussendienst', '$rem_termin', ";
									$sql .= "'$produkt_alt_id', '$aquiriert') ";
									$query = myqueryi($db, $sql);
									$termin_id = mysqli_insert_id($db);
						
									$fehler = "Die Wiedervorlage mit der ID: $daten[termin_id] wurde abgeschlossen - ein Termin mit ID: $termin_id wurde eingefügt";

							} // ende elseif alte Wiedervorlagen vorhanden
							
							else { // neuer Termin
				
								$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, rem_termin, ";
								$sql .= "produkt_alt_id, aquiriert) ";
								$sql .= "VALUES ('$termin_datum', '$d_zeit', '$kunden_id', '$telefonist', '$aussendienst', '$rem_termin', ";
								$sql .= "'$produkt_alt_id', '$aquiriert') ";
								$query = myqueryi($db, $sql);
								$termin_id = mysqli_insert_id();
				
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
								$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00' ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
			
								$fehler = "Ein Termin mit der ID: $termin_id wurde eingefügt";
							
							} // ende else neuer Termin
							} //neue Daten für termin
						} // Ende else Termin in der Zukunft
					} // ende else Termin noch frei
				}	// ende else Muster Datum stimmt
				} // ende else kein WVL-Datum eingegeben
			}	// ende else Termin eingegeben	
		} // ende else Terminbearbeitung
	//}	// Ende else kann nur Termin oder Wiedervorlage folgen
	
	// Verzögerung um 300 Millisekunden (usleep verlangt Mikrosekunden!)
	usleep(300000);			
	
	// Nachladen der Termin-Anzeige-Seite im linken Frame, Benutzer und Aussendienst sind als Session-Variable gespeichert												
	echo "<script>onload=parent['aqset_mr'].location.href='../allgemein/termin_aktuell.php'</script>";
	
	
} // Ende isset($_speichern)

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Termine-neu</title>
	<!-- admin/termin_neu.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">

<table border="1" width="800" bordercolor="#006699">
<?php
	if ($fehler) {
 		echo "<tr bgcolor=\"#F0F8FF\">";
        echo "<td colspan=\"3\">";
		echo "<span style=\"font-weight:bold; color:red;\">$fehler</span><br>";
		echo "</td></tr>";
	}
?>
  <!-- Tabelle Rand -->
<form name="terminvereinbarung" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
	<tr cellspacing="0" cellpadding="0">
    	<td colspan="3">
	<tr bgcolor="#F0F8FF">
      <td colspan="2"><span style="font-weight:bold;"><?php echo "$aussendienst: Kd.: $kunden_id $kunde[vorname] $kunde[name] - Tel.: ($kunde[vorwahl1]) $kunde[telefon] - $kunde[plz] $kunde[ort]"; ?></span></td>
	  <?php echo "<td align = \"right\"><a href='../allgemein/termin_check.php?kd_id=$kunden_id & t_id=$t_id & check=1' TARGET= '_self'><span style=\"font-weight:bold; color:red;\">Für Termin-Übersicht bitte hier klicken</span></a></td>";?>
    </tr>

<!-- Zeile zur Darstellung Termindaten / Checkboxen / Kundendaten ------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

    <tr bgcolor="#F0F8FF">

<!-- Zelle zur Darstellung Termindaten ---------------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

		<td align = "left" valign = "top">
			<table cellpadding = "2" bgcolor = "#006699">
				<tr bgcolor="#F0F8FF">
					<td width="89">Termin</td>
      				<td><input type="text"  class="eingabe" name="d1_tag" size = "2" maxlength ="2" value = "<?php echo "$d_tag"; ?>"/>
        				.
        				<input type="text"  class="eingabe" name="d1_monat" size = "2" maxlength ="2" value = "<?php echo "$d_monat"; ?>"/>
        				.
        				<input type="text"  class="eingabe" name="d1_jahr" size = "2" maxlength ="2" value = "<?php echo "$d_jahr"; ?>"/>
      				</td>
      				<td width="56">Zeit</td>
	  				<td width="101"><select name="d1_zeit" class = "eingabe">
		  <?php
							for ($i = 0; $i < count($t_zeit); $i++) { // Erzeugung der Zeilen
								if ($d_zeit === $t_zeit[$i]) {
									echo " <option selected>$t_zeit[$i]</option>";
								}
								else {
									echo " <option>$t_zeit[$i]</option>";
								}
							}
	 					?>
	 					</select>
      				</td>
    			</tr>

	    		<tr bgcolor="#F0F8FF">
      				<td width="89">WVL&nbsp;</td>
      				<td><input type="text" class = "eingabe" name="w1_tag" size = "2" maxlength ="2" value = "<?php echo "$w_tag"; ?>"/>
        				.
        				<input type="text" class = "eingabe" name="w1_monat" size = "2" maxlength ="2" value = "<?php echo "$w_monat"; ?>"/>
        				.
        				<input type="text" class = "eingabe" name="w1_jahr" size = "2" maxlength ="2" value = "<?php echo "$w_jahr"; ?>"/>
					</td>
	  				<td>Zeit</td>
	  				<td><select name="w1_zeit" class = "eingabe">
		   <?php
		   					echo "$w_zeit<br>";
							for ($i = 0; $i < count($wvl_zeit); $i++) { // Erzeugung der Zeilen
								if ($w_zeit === $wvl_zeit[$i]) {
									echo " <option selected>$wvl_zeit[$i]</option>";
								}
								else {
									echo " <option>$wvl_zeit[$i]</option>";
								}
							}
	 					?>
	 					</select>
      				</td>
	  			</tr>

				<tr bgcolor="#F0F8FF">
      				<td width="89">Ges. alt</td>
      				<td><select name="ges_alt_alt1">
		 <?php
	  						$sql = "SELECT ges_alt FROM ges_alt ORDER BY ges_alt ASC ";
							$query_ges_alt = myqueryi($db, $sql);
							while ($zeile = mysqli_fetch_array($query_ges_alt)) {

								if ($ges_alt_alt == $zeile[0]) {
									echo "<option selected>$zeile[0]</option>";
								}
								else {
									echo "<option>$zeile[0]</option>";
								}
							}
						?>
						</select>
      				</td>
	  	  			<td>Eingabe</td>
	  				<td><input type="text" class = "eingabe" name="ges_alt" size ="15" maxlength="20" value = "<?php echo "$ges_alt"; ?>"></td>
    			</tr>

				<tr bgcolor="#F0F8FF">
      				<td width="89">Ges. neu</td>
      				<td><?php echo "$termin[ges_neu]"; ?></td>
	  				<td>Tarif-Neu</td>
	  				<td><?php echo "$termin[tarif_neu]"; ?></td>
    			</tr>

   				<tr bgcolor="#F0F8FF">
      				<td width="89">Bemerkung</td>
      				<td colspan="3"><textarea cols="50" rows="6" class = "eingabe" name="rem1_termin"><?php echo "$rem_termin"; ?></textarea></td>
    			</tr>
			</table>
		</td>

<!-- Ende Zelle zur Darstellung Termindaten ----------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- Zelle zur Darstellung Checkboxen  ---------------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

		<td align = "center" valign = "top">
			<table cellpadding = "2" bgcolor = "#006699">
				 <tr bgcolor="#ffffcc">
      				<td align = "center" valign = "top">
	   					<strong>ne</strong><br />
		 <?php
							if ($ne == 0) {
								echo "<input type=\"checkbox\"  name=\"ne1\">";
							}
							else {
								echo "<input type=\"checkbox\"  name=\"ne1\" checked>";
							}
						?>
					</td>
				</tr>

	<?php
					if ($doppler == 1) {					// Termin belegt und "2x" nicht angeklickt - Zeile in Rot
						echo "<tr bgcolor=\"red\">";
					}
					else {
						echo "<tr bgcolor=\"#ffff99\">";
					}
				?>
      				<td align = "center" valign = "top">
	   					<strong>2x</strong><br />
		 <?php
								if ($doppelt == 0) {
								echo "<input type=\"checkbox\"  name=\"doppelt1\">";
							}
							else {
								echo "<input type=\"checkbox\" name=\"doppelt1\" checked>";
							}
						?>
					</td>
				</tr>

				 <tr bgcolor="#ffff66">
      				<td align = "center" valign = "top">
	   					<strong>Nichtk.</strong><br />
		 <?php
							if ($nichtkunde == 0) {
								echo "<input type=\"checkbox\" name=\"nichtkunde1\">";
							}
							else {
								echo "<input type=\"checkbox\"  name=\"nichtkunde1\" checked>";
							}
						?>
					</td>
				</tr>

				 <tr bgcolor="#ffff33">
      				<td align = "center" valign = "top">
	   					<strong>verzog.</strong><br />
		   <?php
							if ($verzogen == 0) {
								echo "<input type=\"checkbox\"  name=\"verzogen1\">";
							}
							else {
								echo "<input type=\"checkbox\"  name=\"verzogen1\" checked>";
							}
						?>
					</td>
				</tr>

				<tr bgcolor="#ffff00">
      				<td align = "center" valign = "top">
	   					<strong>Einzug</strong><br />
		   <?php
							if ($einzug == 0) {
								echo "<input type=\"checkbox\"  name=\"einzug1\">";
							}
							else {
								echo "<input type=\"checkbox\"  name=\"einzug1\" checked>";
							}
						?>
					</td>
				</tr>
			</table>
		</td>

<!-- Ende Zelle zur Darstellung Checkboxen ------------------------------------------------------------------------------------------------------------------------>
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- Zelle zur Darstellung Kundendaten ---------------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

		<td align = "left" valign = "top">
			<table cellpadding = "2" bgcolor = "#006699">

				<tr bgcolor="#F0F8FF">
      				<td width="89">Vorname</td>
      				<td width="130"><input type="text" class = "eingabe" name="kd_vorname" maxlength = "20" size = "20" value = "<?php echo "$t_vorname"; ?>"/></td>
      				<td width="89">Name</td>
      				<td><input type="text" class = "eingabe" name="kd_name" maxlength = "25" size = "20" value = "<?php echo "$t_name"; ?>"/></td>
    			</tr>

				<tr bgcolor="#F0F8FF">
      				<td width="89">Ort</td>
      				<td width="130"><input type="text" class = "eingabe" name="t_ort" maxlength = "30" size = "20" value = "<?php echo "$t_ort"; ?>"/></td>
      				<td width="89">Straße</td>
      				<td><input type="text" class = "eingabe" name="t1_strasse" maxlength = "60" size = "20" value = "<?php echo "$t_strasse"; ?>"/></td>
    			</tr>

    			<tr bgcolor="#F0F8FF">
      				<td  width="89">Ortsteil</td>
      				<td  width="130">
						<select name="ortsteil_alt" class = "eingabe">
		<?php
	  						$sql = "SELECT ortsteil FROM poo, plz, ort, ortsteil ";
							$sql .= "WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id ";
							$sql .= "AND ort.ort = '$kunde[ort]' ";
							$ortsteil = myqueryi($db, $sql);

							for ($j = 0; $j < mysqli_num_rows($ortsteil); $j++)	{		// Anzahl der Datensätze
								$zeile = mysqli_fetch_row($ortsteil);						// Schleife für Daten-Zeilen
								if 	($zeile[0] == $t_ortsteil) {
									echo "<option selected>$zeile[0]</option>";
								}
								else {
									echo "<option>$zeile[0]</option>";
								}
							}
	 					?>
	  					</select>
      				</td>
      				<td  width="89"><strong>OT neu</strong></td>
      				<td><input type="text" class = "eingabe" name="ortsteil_neu" maxlength = "25" size = "20" value = "<?php echo "$t_ortsteil_neu"; ?>"/></td>
    			</tr>

	  			<tr bgcolor="#F0F8FF">
      				<td width="89">geboren</td>
      				<td>
			<?php
							if (($g_tag == "00") AND ($g_monat == "00") AND ($g_jahr == "0000")) {
	  							echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_tag\" size = \"2\" maxlength =\"2\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_monat\" size = \"2\" maxlength =\"2\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_jahr\" size = \"2\" maxlength =\"2\">";
							}
							else {
								echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_tag\" size = \"2\" maxlength =\"2\" value = \"$g_tag\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_monat\" size = \"2\" maxlength =\"2\" value = \"$g_monat\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_jahr\" size = \"2\" maxlength =\"2\" value = \"$g_jahr\">";
							}
						?>
      				</td>
      				<td width="89">&nbsp;</td>
      				<td>&nbsp;</td>
   				 </tr>

    			<tr bgcolor="#F0F8FF">
      				<td width="89">Vorwahl 2</td>
      				<td width="130"><input type="text" class = "eingabe" name="t1_vorwahl2" size="8" value = "<?php echo "$t_vorwahl2"; ?>"/></td>
      				<td width="">Tel. 2</td>
      				<td><input type="text" class = "eingabe" name="t1_fax" size="12" value = "<?php echo "$t_fax"; ?>"/></td>
    			</tr>

    			<tr bgcolor="#F0F8FF">
     				<td width="89">E-Mail</td>
      				<td width="130"><input type="text" class = "eingabe" name="t1_email" size="20" maxlength = "20" value = "<?php echo "$t_email"; ?>"/></td>
      				<td width="89">Branche</td>
      				<td>
        				<select name="t1_branche" class = "eingabe">
		  <?php
		   					$sql = "SELECT branche FROM branche WHERE branche_id > '1' ORDER BY branche ASC";
							$query_branche = myqueryi($db, $sql);

							echo " <option></option>";
							for ($j = 0; $j < mysqli_num_rows($query_branche); $j++)	{			// Anzahl der Datensätze
								$zeile = mysqli_fetch_row($query_branche);						// Schleife für Daten-Zeilen
								if ($zeile[0] == $t_branche) {
									echo " <option selected>$zeile[0]</option>";
								}
								else {
									echo " <option>$zeile[0]</option>";
								}
							}
	 					?>
    					</select>
     				</td>
    			</tr>

   				<tr bgcolor="#F0F8FF">
      				<td width="89">Bemerkung</td>
      				<td colspan="3"><textarea cols="50" rows="2" class = "eingabe" name="t1_bemerkung"><?php echo "$t_bemerkung"; ?></textarea></td>
    			</tr>
			</table>
		</td>

<!-- Ende Zelle zur Darstellung Kundendaten ----------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->


	</tr>

<!-- Ende Zeile zur Darstellung Termindaten / Checkboxen/ Kundendaten -------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->


	<tr bgcolor="#F0F8FF">
      <td colspan ="3" align = "center">
        <input type="submit" name="t_speichern" value = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Speichern&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" class="submitt"/>
      </td>
    </tr>
  </table>
</form>
<!-- Ende Tabelle Rand -->

</div>
</body>
</html>