<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();


// Variablendefinition -----------------------------------------------------
//--------------------------------------------------------------------------

$start = $_GET["start"];
$ende = $_GET["ende"];
$user = $_GET["user"];

// Sonderzeichen und Whitespaces entfernen

$start = quote_smart($start);
$ende = quote_smart($ende);
$user = quote_smart($user);

// -------------------------------------------------------------------------

// Standard-Anzahl der auszuwertenden Tage, wenn kein Endedatum übermittelt wird

$tage = "4";		// Starttag + 4 weitere Tage = 5 Tage

// Initialisierung der Zähler für die Termine

$termin_gesamt = "0";			// Termine gesamt im Auswertezeitraum
$abschluss_gesamt = "0";		// Abschlüsse gesamt im Auswertezeitraum

$termin_tag = "0";				// Termine für einen Tag
$abschluss_tag = "0";			// Abschlüsse für einen Tag

// Name des Außendienstlers ermitteln

$sql = "SELECT vorname, name FROM user, vorname, name ";
$sql .= " WHERE user.user = '$user' AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
$query = myqueryi($db, $sql);
$ad = mysqli_fetch_array($query);

// Zerlegung der Variablen Start in Tag/Monat/Jahr

$starttag = substr($start, 6, 2);
$startmonat = substr($start, 4, 2);
$startjahr = substr($start, 0, 4);
$startdatum = $starttag . "." . $startmonat . "." . $startjahr;

// Ermittlung Endedatum, falls kein Endedatum bei ad_termin_select.php eingegeben wurde

if (empty($ende)) {

	$endedatum = mktime(0, 0, 0, $startmonat, ($starttag + $tage), $startjahr);		// Unix-Timestamp für Starttag + Anzahl der Tage
	
	$endetag = strftime("%d", $endedatum);											// Zerlegung des Timestamps in Tag, Monat Jahr
	$endemonat = strftime("%m", $endedatum);
	$endejahr = strftime("%Y", $endedatum);

	$ende = $endejahr . $endemonat . $endetag;
	$endedatum = $endetag . "." . $endemonat . "." . $endejahr;

} // ende keine Endedatum übermittelt
else { 													// Endedatum wurde eingegeben, Zeitspanne der Anzeige muss ermittelt werden

	// Zerlegung der Variablen ende in Tag/Monat/Jahr
	// Anzeigezeitraum muss ermittelt werden
	
	$endetag = substr($ende, 6, 2);
	$endemonat = substr($ende, 4, 2);
	$endejahr = substr($ende, 0, 4);
	$endedatum = $endetag . "." . $endemonat . "." . $endejahr;
	
	$endestamp = mktime(0, 0, 0, $endemonat, $endetag, $endejahr);			// Unix-Timestamp für Endedatum
	$startstamp = mktime(0, 0, 0, $startmonat, $starttag + $i, $startjahr);	// Unix-Timestamp für Startdatum
	
	$differenz = $endestamp - $startstamp;
	
	$tage = ($differenz / 86400);
	

} // Ende else Endedatum wurde eingegeben

// Abfrage der Termine aus dem Anzeigezeitraum, um zu sehen, ob überhaupt Termine da sind
// keine Sperrzeiten und keine Wiedervorlagen

$sql = "SELECT name, telefon, ort, ges_neu, telefonist, aussendienst, rem_termin, zeit, termin_id, termin ";
$sql .= " FROM termin, name, kunden, ort, poo, plz, ortsteil, ges_neu, produkt_neu ";
$sql .= " WHERE termin.kd_id = kunden.kunden_id AND kunden.name_id = name.name_id ";
$sql .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id AND poo.plz_id = plz.plz_id ";
$sql .= " AND termin.produkt_neu_id = produkt_neu.produkt_neu_id AND produkt_neu.ges_neu_id = ges_neu.ges_neu_id ";
$sql .= " AND (termin.termin BETWEEN '$start' AND '$ende') ";
$sql .= " AND termin.sperrzeit = '0' AND termin.wiedervorlage = '0' AND termin.telefonist = '$user' AND termin.alt = '0'";
//$sql .= " AND termin.sperrzeit = '0' AND termin.wiedervorlage = '0' AND termin.telefonist = '$user' ";

$abfrage = myqueryi($db, $sql);

$anzahl = mysqli_num_rows($abfrage);				// Anzahl der ausgelesenen Datensätze
$span = mysqli_num_fields($abfrage);				// Anzahl der Felder in der Abfrage
$span = $span - 1;								// Spalte Termin wird für Anzeige ausgeblendet

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang =de"">
<head>
<title>T drucken</title>
	<!-- admin/aq_termin_druck.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/drucken.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "left">
<table border="0" width= "700" cellpadding="2" cellspacing="2">
<tr>
<td align ="left" valign="top">
<?php echo "<strong>Rücklauf für Telefonist:&nbsp;$ad[vorname]&nbsp;$ad[name]&nbsp;($user)<br />
		 Termine vom: $startdatum&nbsp;bis&nbsp;$endedatum<br />
		 erstellt am:&nbsp;" . date("d.m.Y H:i") . "</strong>"?><td>
</tr>
<tr><td><hr></td></tr>
<tr><td valign = "top" align = "left">

<table width="100%" border = "0" cellpadding = "1">							<!-- Tabelle für Ergebnisse -->
<?php

// Tabellenkopf Anfang

echo "<tr>";
echo "<th align = \"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 9pt;\">Name</span></th>";
echo "<th align = \"center\" valign=\"top\" width = \"40\"><span style = \"font-size: 9pt;\">Telefon</span></th>";
echo "<th align = \"left\" valign=\"top\" width = \"70\"><span style = \"font-size: 9pt;\">Ort</span></th>";
echo "<th align = \"center\" valign=\"top\" width = \"60\"><span style = \"font-size: 9pt;\">Ges.-Neu</span></th>";
echo "<th align = \"center\" valign=\"top\" width = \"30\"><span style = \"font-size: 9pt;\">TV</span></th>";
echo "<th align = \"center\" valign=\"top\" width = \"30\"><span style = \"font-size: 9pt;\">AD</span></th>";
echo "<th align = \"left\" valign=\"top\" width = \"300\"><span style = \"font-size: 9pt;\">Bemerkung</span></th>";
echo "<th align = \"center\" valign=\"top\" width = \"30\"><span style = \"font-size: 7pt;\">Zeit<br />T-ID</span></th>";
echo "</tr>\n";										// Ende Tabellenkopf

/*
echo "<tr>";
	
	for($j = 0; $j < ($span); $j++)												// Anzahl der Tabellenzellen pro Zeile, Zelle Termin wird ausgeblendet
{
	$row = mysqli_fetch_assoc($abfrage);
	$feldname = array_keys($row);
						mysqli_data_seek($ergebnis, 0);								// Name der Tabellenzelle
	echo "<th align = \"left\" valign=\"top\" width = \"80\">$feldname[$j]</th>";
}
	
echo "</tr>\n";																	
*/

echo "<tr><td colspan = \"$span\"><hr></td></tr>";								// Trennzeile

for ($i = 0; $i < ($tage +1); $i++) {		// Schleife zur Anzeige der Tage im Auswertezeitraum

	$aktuellstamp = mktime(0, 0, 0, $startmonat, ($starttag + $i), $startjahr);	// Unix-Timestamp für Starttag + Anzahl der Tage
	
	$aktuelltag = strftime("%d", $aktuellstamp);										// Zerlegung des Timestamps in Tag, Monat Jahr
	$aktuellmonat = strftime("%m", $aktuellstamp);
	$aktuelljahr = strftime("%Y", $aktuellstamp);

	$aktuelldatum = $aktuelltag . "." . $aktuellmonat . "." . $aktuelljahr;
	
	$aktuell = $aktuelljahr . $aktuellmonat . $aktuelltag;
		
	
	echo "<tr><td valign = \"top\" colspan = \"$span\"><strong>$aktuelldatum</strong></td></tr>";	// Gibt das Datum des aktuellen Tages aus
	echo "<tr><td valign = \"top\" colspan = \"$span\"><hr></td></tr>";							// Gibt eine Trennlinie aus
	
// Abfrage der Termine vom aktuellen Tag
// keine Sperrzeiten und keine Wiedervorlagen, keine "alten" Termine

	$sql = "SELECT name, telefon, ort, ges_neu, telefonist, aussendienst, rem_termin, zeit, termin_id, termin ";
	$sql .= " FROM termin, name, kunden, ort, poo, plz, ortsteil, ges_neu, produkt_neu ";
	$sql .= " WHERE termin.kd_id = kunden.kunden_id AND kunden.name_id = name.name_id ";
	$sql .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id AND poo.plz_id = plz.plz_id ";
	$sql .= " AND termin.produkt_neu_id = produkt_neu.produkt_neu_id AND produkt_neu.ges_neu_id = ges_neu.ges_neu_id ";
	$sql .= " AND termin.termin = '$aktuell' ";
	$sql .= " AND termin.sperrzeit = '0' AND termin.wiedervorlage = '0' AND termin.telefonist = '$user' AND termin.alt = '0'";
	//$sql .= " AND termin.sperrzeit = '0' AND termin.wiedervorlage = '0' AND termin.telefonist = '$user' ";
	$sql .= " ORDER BY aussendienst ASC, zeit ASC ";
	
	$query = myqueryi($db, $sql);
	
	$termin_tag = mysqli_num_rows($query);
	
	while($zeile = mysqli_fetch_row($query)) {						// Schleife für Daten-Zeilen
		
		if (!empty($zeile[3])) {									// Aufsummierung der abgeschlossenen Verträge
				$abschluss_tag = $abschluss_tag + "1";
		}
	
		echo "<tr>";
		
		echo "<td align = \"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 9pt;\">$zeile[0]</span></td>";
		echo "<td align = \"center\" valign=\"top\" width = \"40\"><span style = \"font-size: 9pt;\">$zeile[1]</span></td>";
		echo "<td align = \"left\" valign=\"top\" width = \"70\"><span style = \"font-size: 9pt;\">$zeile[2]</span></td>";
		echo "<td align = \"center\" valign=\"top\" width = \"60\"><span style = \"font-size: 9pt;\">$zeile[3]</span></td>";
		echo "<td align = \"center\" valign=\"top\" width = \"30\"><span style = \"font-size: 9pt;\">$zeile[4]</span></td>";
		echo "<td align = \"center\" valign=\"top\" width = \"30\"><span style = \"font-size: 9pt;\">$zeile[5]</span></td>";
		echo "<td align = \"left\" valign=\"top\" width = \"300\"><span style = \"font-size: 9pt;\">$zeile[6]</span></td>";
		echo "<td align = \"center\" valign=\"middle\" width = \"30\"><span style = \"font-size: 7pt;\">$zeile[7]<br />$zeile[8]</span></td>";
		
		/*for($k = 0; $k < $span; $k++) {			// Schleife für Felder
			echo "<td align =\"left\" valign=\"top\" width = \"80\"><span style = \"font-size: 10pt;\">" . $zeile[$k] . "</span></td>";
		} // ende for Schleife Felder */
		
		echo "</tr>";
	} // Ende while-Schleife
	
	if ($termin_tag > 0) {
		echo "<tr><td valign = \"top\" colspan = \"$span\"><hr></td></tr>";			// Trennzeile zwischen Terminen und Summen, nur wenn wirklich Termine vorhanden sind
	}
	echo "<tr><td colspan = \"3\"><span style = \"font-size: 10pt;\"><strong>Tagessumme</strong></span></td>";
	echo "<td align = \"center\"><span style = \"font-size: 10pt;\"><strong>$abschluss_tag</strong></span</td>";
	echo "<td align = \"center\"><span style = \"font-size: 10pt;\"><strong>$termin_tag</strong></span</td><td colspan = \"2\">&nbsp;</td></tr>";
	echo "<tr><td valign = \"top\" colspan = \"$span\"><hr><br /><br /></td></tr>";
	
	$termin_gesamt = $termin_gesamt + $termin_tag;
	$abschluss_gesamt = $abschluss_gesamt + $abschluss_tag;

	$termin_tag = "0";				// Termine für einen Tag
	$abschluss_tag = "0";			// Abschlüsse für einen Tag

} // Ende for-Schleife Anzeige Tage im Anzeigezeitraum

	echo "<tr><td valign = \"top\" colspan = \"$span\"><hr></td></tr>";
	echo "<tr><td colspan = \"3\"><span style = \"font-size: 10pt;\"><strong>Gesamtsumme</strong></span></td>";
	echo "<td align = \"center\"><span style = \"font-size: 10pt;\"><strong>$abschluss_gesamt</strong></span</td>";
	echo "<td align = \"center\"><span style = \"font-size: 10pt;\"><strong>$termin_gesamt</strong></span</td><td colspan = \"2\">&nbsp;</td></tr>";
	echo "<tr><td valign = \"top\" colspan = \"$span\"><hr><br /><br /></td></tr>";


?>
</table>				<!-- Ende Tabelle Ergebnisse -->
</td></tr></table>
</body>
</html>