<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Preisagentur: Logins</title>
	<!-- admin/logins.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<table width="600" border="0" cellpadding="4" cellspacing="4">
<tr>
<td><h2 class="Stil1">Übersicht Über Logins</h2><td>
</tr>
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">

<?php
$heute = date("Y"."m"."d");
//echo "$heute";

$sql = "SELECT logins_id AS ID, user AS Benutzer, indate AS InDatum, intime AS InZeit, outdate AS OutDatum, outtime AS OutZeit ";
$sql .= "FROM logins WHERE indate = '$heute' ";
$sql .= "ORDER BY indate ASC ";

$ergebnis = myqueryi($db, $sql);
$span = mysqli_num_fields($ergebnis) + 2;

//Logindaten
	echo "<table width = \"100%\"  cellspacing=\"2\">";
	echo "<tr>";													// Tabellenkopf
	for($i = 0; $i < mysqli_num_fields($ergebnis); $i++)				// Anzahl der Tabellenzellen pro Zeile
	{
		$row = mysqli_fetch_assoc($ergebnis);

		$feldname = array_keys($row);				// Name der Tabellenzelle

		mysqli_data_seek($ergebnis, 0);
		echo "<td><b>$feldname[$i]</b></td>";
	}
	echo "<td>&nbsp;</td>";											// eine Zelle für den BEARBEITEN-Button angehängt
	echo "</tr>\n";													// Tabellenkopf Ende

	echo "<tr><td colspan = \"$span\"><hr></td></tr>";
	
	$z=0;  //zähler der datensätze für bg_colour der zeilen
	$bg1 = "#eeeeee"; //die beiden hintergrundfarben
	$bg2 = "#dddddd";
	
	while($zeile = mysqli_fetch_row($ergebnis))						// Schleife für Daten-Zeilen
	{
		//echo "<tr bgcolor=";
		//$bg=($z++ % 2) ? $bg1 : $bg2;
		//echo $bg;
		//echo ">";
		
		$bg=($z++ % 2) ? $bg1 : $bg2;
		echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
		
		for($i = 0; $i < mysqli_num_fields($ergebnis); $i++)			// Schleife für Felder
		{	
			if ($i == "2" OR $i == "4") {
				$zeile[$i] = substr($zeile[$i],6,2) . "." . substr($zeile[$i],4,2) . "." . substr($zeile[$i],0,4);
				//$zeile[$i] = "test";
				echo "<td>" . $zeile[$i] . "</td>";
			}
			else {
				echo "<td>" . $zeile[$i] . "</td>";
			}
		}
		echo "<td>";	
		echo "<a href=\"logins_loeschen.php?schliesse=$zeile[0]\"><span style=\"color:green;\">Schließen</span></a>";
		echo "</td>";										// + angehängter ID gefüllt
		echo "<td>";	
		echo "<a href=\"logins_loeschen.php?loesche=$zeile[0]\"><span style=\"color:red;\">Löschen</span></a>";
		echo "</td>";										// + angehängter ID gefüllt
	echo "</tr>";
	
    }
echo "</table>";
// Ende Logindaten

?>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</body>
</html>