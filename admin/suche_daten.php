<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();

$telefonist = $_SESSION['benutzer_kurz'];

$t_zeit = array("", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", 
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");			// Array für Zeiten
				
$wvl_zeit = array("", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", 
				"15:00", "16:00", "17:00", "18:00", "19:00", "20:00");			// Array für Wiedervorlage-Zeiten

//echo "kunden_id = $kunden_id<br>";
//echo "termin_id = $t_id<br>";
//echo "telefonist = $telefonist<br>";

// wenn $kunden__id gesetzt ist - nur Kundendaten sollen geändert werden

if ($kunden_id) {

	$sql  = "SELECT vorname, name, vorwahl1, telefon, plz, ort, ortsteil,  strasse, fax, email, rem_kunde, geboren, ";
	$sql  .= "einzug, nichtkunde, belegt, wiedervorlage, vertragskunde, vorwahl2, fax, branche ";
	$sql .= "FROM kunden, vorname, name, vorwahl1, poo, plz, ort, ortsteil, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id' AND kunden.vorname_id = vorname.vorname_id ";
	$sql .= "AND name.name_id = kunden.name_id AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	$sql .= "AND kunden.vorwahl2_id = vorwahl2.vorwahl2_id AND kunden.branche_id = branche.branche_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$ergebnis = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($ergebnis, MYSQLI_ASSOC);
}

// wenn $t_id gesetzt ist - Kunden und Termindaten sollen geändert werden

elseif ($t_id) {

	$sql = "SELECT kd_id, termin, zeit, rem_termin, produkt_alt_id, produkt_neu_id, w_zeit, aussendienst FROM termin ";
	$sql .= " WHERE termin.termin_id = '$t_id' ";
	$abfrage = myqueryi($db, $sql);
	$termin = mysqli_fetch_array($abfrage, MYSQLI_ASSOC);
	list($d_jahr, $d_monat, $d_tag) = explode("-", $termin[termin]);
	$d_jahr = substr($d_jahr, 2, 2); 
	
	$ad = $termin[aussendienst];
	$kunden_id = $termin[kd_id];
	
	$sql  = "SELECT vorname, name, vorwahl1, telefon, plz, ort, ortsteil,  strasse, fax, email, rem_kunde, geboren, ";
	$sql  .= "einzug, nichtkunde, belegt, wiedervorlage, vertragskunde, vorwahl2, fax, branche ";
	$sql .= "FROM kunden, vorname, name, vorwahl1, poo, plz, ort, ortsteil, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id' AND kunden.vorname_id = vorname.vorname_id ";
	$sql .= "AND name.name_id = kunden.name_id AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	$sql .= "AND kunden.vorwahl2_id = vorwahl2.vorwahl2_id AND kunden.branche_id = branche.branche_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$ergebnis = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($ergebnis, MYSQLI_ASSOC);
}

list($g_jahr, $g_monat, $g_tag) = explode("-", $kunde[geboren]);
$g_jahr = substr($g_jahr, 2,2);										// nur die letzten 2 Stellen darstellen

// -----------------------------------------------------------------------------------------------------------------------------------

if (isset($t_speichern)) {								// Termin speichern wurde gedr�ckt

	$aquiriert = date("Y-m-d H:i");
	$heute = date("Y-m-d");				// Datum von heute

// Nichtkunde ------------------------------------------------------------------------------------------------------------------------
	if ($nichtkunde) {
	
		$sql = "UPDATE kunden SET nichtkunde = '1', wiedervorlage = '0', vertragskunde = '0', einzug = '0', belegt = '0', neukunde = '0' ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		$query = myqueryi($db, $sql);
		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) wurde gesperrt";
		
		$sql = "SELECT termin_id FROM termin WHERE termin.kd_id = '$kunden_id' AND wiedervorlage = '1' ";
		$query = myqueryi($db, $sql);
	
		for ($i = 0; $i < mysqli_num_rows($query); $i++) {		// Terminliste durchgehen -  - alle Wiedervorlagen abschließen
			$daten = mysqli_fetch_array($query);
			$sql = "UPDATE termin SET wiedervorlage_date = '', w_zeit = '', erledigt_date = '$heute', wiedervorlage = '0', nichtkunde='1' ";
			$sql .= "WHERE termin_id = '$daten[termin_id]'";
			$query = myqueryi($db, $sql);
		}
		//$kunde = array();								// Formulardaten löschen
		//$kunden_id = NULL;
	} // Ende Kunde wurde gesperrt
	
// verzogen -------------------------------------------------------------------------------------------------------------------------
	
	elseif ($verzogen) {
	
		$sql = "UPDATE kunden SET verzogen = '1', wiedervorlage = '0', vertragskunde = '0', einzug = '0', neukunde = '0' ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		$query = myqueryi($db, $sql);
		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) ist verzogen";
		
		$sql = "SELECT termin_id FROM termin WHERE termin.kd_id = '$kunden_id' AND wiedervorlage = '1' ";
		$query = myqueryi($db, $sql);
	
		for ($i = 0; $i < mysqli_num_rows($query); $i++) {		// Terminliste durchgehen - alle Wiedervorlagen abschließen
			$daten = mysqli_fetch_array($query);
			$sql = "UPDATE termin SET wiedervorlage_date = '', w_zeit = '', erledigt_date = '$heute', wiedervorlage = '0'";
			$sql .= "WHERE termin_id = '$daten[termin_id]'";
			$query = myqueryi($db, $sql);
		}
		//$kunde = array();								// Formulardaten l�schen
		//$kunden_id = NULL;
	} // Ende Kunde ist verzogen
	
	else {	// kann nur Termin oder Wiedervorlage folgen
	
		// Ortsteil --------------------------------------------------------------------------------------------
				
					if (empty($ortsteil_neu)) {							// keine Eingabe im Feld neuer Ortsteil
						$ortsteil_alt = quote_smart($ortsteil_alt);
						if ($ortsteil_alt == $kunde[ortsteil]) {		// alter Ortsteil
							if (empty($ortsteil_alt)) {					// kein OT eingegeben - ortsteil_id = 1
								$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
								$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
								$sql .="AND ortsteil.ortsteil_id = '1' AND ort.ort = '$kunde[ort]'";
								$query = myqueryi($db, $sql);
								$daten = mysqli_fetch_row($query);
								$poo_id = $daten[0];
							}
							else {	//Ortsteil eingegeben
								$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
								$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
								$sql .="AND ortsteil.ortsteil = '$ortsteil_alt' AND ort.ort = '$kunde[ort]'";
								$query = myqueryi($db, $sql);
								$daten = mysqli_fetch_row($query);
								$poo_id = $daten[0];
							}
						}	// ende alter Ortsteil
		
						else {	// neuer Ortsteil
							if (empty($ortsteil_alt)) {					// kein OT eingegeben - ortsteil_id = 1
								$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
								$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
								$sql .="AND ortsteil.ortsteil_id = '1' AND ort.ort = '$kunde[ort]'";
								$query = myqueryi($db, $sql);
								$daten = mysqli_fetch_row($query);
								$poo_id = $daten[0];
							}
							else {	//Ortsteil eingegeben
								$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
								$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
								$sql .="AND ortsteil.ortsteil = '$ortsteil_alt' AND ort.ort = '$kunde[ort]'";
								$query = myqueryi($db, $sql);
								$daten = mysqli_fetch_row($query);
								$poo_id = $daten[0];
							}
						}	// ende else neuer Ortsteil
					} // keine Eingabe im Feld neuer Ortsteil
	
					else {	// $ortsteil_neu ist nicht leer
						$ortsteil_neu = quote_smart($ortsteil_neu);
						$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
						$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
						$sql .="AND ortsteil.ortsteil = '$ortsteil_neu' AND ort.ort = '$kunde[ort]'";
						$query = myqueryi($db, $sql);
						$daten = mysqli_fetch_row($query);
						if ($daten) {						// diese Kombi Ort/Ortsteil ist schon vorhanden
							$poo_id = $daten[0];
						}
						else {	// neue Kombi Ort/ortsteil
							$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil = '$ortsteil_neu'";	// ortsteil schon vorhanden?
							$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil_neu')";				// neuer Ortsteil
							$ortsteil_id = id_ermitteln($db, $sql1, $sql2);
			
							$sql = "SELECT ort_id FROM ort WHERE ort = '$kunde[ort]' ";
							$query = myqueryi($db, $sql);
							$ort = mysqli_fetch_row($query);
			
							$sql = "SELECT plz_id FROM plz WHERE plz = '$kunde[plz]' ";
							$query = myqueryi($db, $sql);
							$plz = mysqli_fetch_row($query);
			
							$sql = "INSERT INTO poo (ort_id, ortsteil_id, plz_id) VALUES ('$ort[0]', '$ortsteil_id', '$plz[0]')";
							$query = myqueryi($db, $sql);
							$poo_id = mysqli_insert_id($db);
						}	// ende neue Kombi Ort/ortsteil
					}	//ende else $ortsteil ist nicht leer
		
					// Ende Ortsteil -----------------------------------------------------------------------------------------------------------------------

				$kd_vorname = quote_smart($kd_vorname);
				$kd_name = quote_smart($kd_name);
				$g_tag = quote_smart($g_tag);
				$g_monat = quote_smart($g_monat);
				$g_jahr = quote_smart($g_jahr);
				$t_strasse = quote_smart($t_strasse);
				$t_vorwahl2 = quote_smart($t_vorwahl2);
				$t_fax = quote_smart($t_fax);
				$t_email = quote_smart($t_email);
				$t_branche = quote_smart($t_branche);

			// Vorname ----------------------------------------------------------------------------------------------------
				$sql1 = "SELECT vorname_id FROM vorname WHERE vorname = '$kd_vorname'";	// Vorname schon vorhanden?
				$sql2 = "INSERT INTO vorname (vorname) VALUES ('$kd_vorname')";			// Vorname neu
				$vorname_id = id_ermitteln($db, $sql1, $sql2);

			// Name ----------------------------------------------------------------------------------------------------

				$sql1 = "SELECT name_id FROM name WHERE name = '$kd_name'";				// Name schon vorhanden?
				$sql2 = "INSERT INTO name (name) VALUES ('$kd_name')";					// Name neu
				$name_id = id_ermitteln($db, $sql1, $sql2);

			// Vorwahl 2 ----------------------------------------------------------------------------------------------------

				if (empty($t_vorwahl2)) {														// Feld Vorwahl2 ist nicht leer
					$vorwahl2_id = "1";
				}
				else {
					$vorwahl2 = quote_smart($t_vorwahl2);
					$sql1 = "SELECT vorwahl2_id FROM vorwahl2 WHERE vorwahl2 = '$t_vorwahl2'";	// Vorwahl2 schon vorhanden?
					$sql2 = "INSERT INTO vorwahl2 (vorwahl2) VALUES ('$t_vorwahl2')";			// Vorwahl2 neu
					$vorwahl2_id = id_ermitteln($db, $sql1, $sql2);
				}

				// Branche ----------------------------------------------------------------------------------------------------

			if (empty($t_branche)) {
				$branche_id = "1";
			}
			else {
				$sql = "SELECT branche_id FROM branche WHERE branche = '$t_branche'";
				$abfrage_branche = myqueryi($db, $sql);
				$daten = mysqli_fetch_array($abfrage_branche);
				$branche_id = $daten[0];
			}

		// geboren ----------------------------------------------------------------------------------------------------
			if (empty($g1_tag) OR empty($g1_monat) OR empty($g1_jahr)) {
				$geboren = "0000-00-00";
			}
			else {
				if ($g1_jahr > 20) {
					$geboren = "19$g1_jahr-$g1_monat-$g1_tag";
				}
				else {
					$geboren = "20$g1_jahr-$g1_monat-$g1_tag";
				}
			}

// Bearbeitung Gesellschaft-ALT und Tarif-ALT
// Eingabe von ges_alt und tarif_alt überschreibt select-feld
	// ---------------------------------------------------------------------------------------------------------------------------------------
	
		$ges_alt = quote_smart($ges_alt);
		$tarif_alt = quote_smart($tarif_alt);
	
		if (empty($ges_alt)) {					// keine ges_alt
			if (empty($tarif_alt)) {			// kein neuer tarif eingegeben
			
				list($ges, $tarif) = explode(" -> ", $produkt_alt);	// Gesellschaft und Tarif trennen
		
				if (empty($ges)) {	// keine Gesellschaft - kann auch kein Tarif sein
					$produkt_alt_id = "1";
				}
				else {	// Gesellschaft vorhanden
		
					$sql = "SELECT produkt_alt_id FROM produkt_alt, ges_alt, tarif_alt ";
					$sql .= "WHERE produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND produkt_alt.tarif_alt_id=tarif_alt.tarif_alt_id ";
				
					if (empty($tarif)) {													// Gesellschaft vorhanden kein Tarif
			
						$sql .= "AND ges_alt.ges_alt = '$ges' AND tarif_alt.tarif_alt_id = '1'";
						$query = myqueryi($db, $sql);
						$ergebnis = mysqli_fetch_array($query);
						$produkt_alt_id = $ergebnis[0];
					}	// Ende Gesellschaft vorhanden kein Tarif
		
					else {																		// Gesellschaft und Tarif vorhanden
						$sql .= "AND ges_alt.ges_alt = '$ges' AND tarif_alt.tarif_alt = '$tarif'";
						$query = myqueryi($db, $sql);
						$ergebnis = mysqli_fetch_array($query);
						$produkt_alt_id = $ergebnis[0];
					}	// Ende Gesellschaft und Tarif vorhanden
				}	// Ende Gesellschaft vorhanden
			} // ende if kein neuer Tarif eingegeben
			
			
			else { // neuer Tarif eingegeben - gesellschaft muss aus dem Select-Feld kommen!!
				list($ges, $tarif) = explode(" -> ", $produkt_alt);	// Gesellschaft und Tarif trennen
				if (empty($ges)) {	// keine Gesellschaft - kann auch kein Tarif sein
					$fehler = "Sie müssen eine Gesellschaft auswählen!";
				}
				else {
					$sql1 = "SELECT tarif_alt_id FROM tarif_alt WHERE tarif_alt = '$tarif_alt'";	// Tarif_alt schon vorhanden?
					$sql2 = "INSERT INTO tarif_alt (tarif_alt) VALUES ('$tarif_alt')";				// Tarif_alt neu
					$tarif_alt_id = id_ermitteln($db, $sql1, $sql2);
					
					$sql = "SELECT ges_alt_id FROM ges_alt WHERE ges_alt = '$ges'";
					$query = myqueryi($db, $sql);
					$daten = mysqli_fetch_array($query);
					$ges_alt_id = $daten[0];
					
					$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE tarif_alt_id = '$tarif_alt_id' AND ges_alt_id = '$ges_alt_id' ";
					$sql2 = "INSERT INTO produkt_alt (tarif_alt_id, ges_alt_id) VALUES ('$tarif_alt_id', '$ges_alt_id')";		// Tarif_neu neu
					$produkt_alt_id = id_ermitteln($db, $sql1, $sql2);
					
					// auch für Produkt_neu
					
					$sql1 = "SELECT tarif_neu_id FROM tarif_neu WHERE tarif_neu = '$tarif_alt'";	// Tarif_neu schon vorhanden?
					$sql2 = "INSERT INTO tarif_neu (tarif_neu) VALUES ('$tarif_alt')";				// Tarif_neu neu
					$tarif_neu_id = id_ermitteln($db, $sql1, $sql2);
					
					$sql = "SELECT ges_neu_id FROM ges_neu WHERE ges_neu = '$ges'";
					$query = myqueryi($db, $sql);
					$daten = mysqli_fetch_array($query);
					$ges_neu_id = $daten[0];
					
					$sql1 = "SELECT produkt_neu_id FROM produkt_neu WHERE tarif_neu_id = '$tarif_neu_id' AND ges_neu_id = '$ges_neu_id' ";
					$sql2 = "INSERT INTO produkt_neu (tarif_neu_id, ges_neu_id) VALUES ('$tarif_neu_id', '$ges_neu_id')";		// Tarif_neu neu
					$produkt_id = id_ermitteln($db, $sql1, $sql2);
				}
			}	// ende else neuer tarif eingegeben
		}	// ende if keine ges_alt
		
		else {	// feld ges_alt ist belegt
			
			$sql1 = "SELECT ges_alt_id FROM ges_alt WHERE ges_alt = '$ges_alt'";	// Gesellschaft_alt schon vorhanden
			$sql2 = "INSERT INTO ges_alt (ges_alt) VALUES ('$ges_alt')";			// Gesellschaft_alt neu
			$ges_alt_id = id_ermitteln($db, $sql1, $sql2);
			
			$sql1 = "SELECT ges_neu_id FROM ges_neu WHERE ges_neu = '$ges_alt'";	// Gesellschaft_alt schon vorhanden
			$sql2 = "INSERT INTO ges_neu (ges_neu) VALUES ('$ges_alt')";			// Gesellschaft_alt neu
			$ges_neu_id = id_ermitteln($db, $sql1, $sql2);
			
			if (empty($tarif_alt)) {													// Gesellschaft vorhanden kein Tarif
			
				$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE ges_alt_id = '$ges_alt_id' AND tarif_alt_id = '1' ";	// kein tarif eingegeben
				$sql2 = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '1')";			// neue Kombi
				$produkt_alt_id = id_ermitteln($db, $sql1, $sql2);
				
				$sql1 = "SELECT produkt_neu_id FROM produkt_neu WHERE ges_neu_id = '$ges_neu_id' AND tarif_neu_id = '1' ";	// kein tarif eingegeben
				$sql2 = "INSERT INTO produkt_neu (ges_neu_id, tarif_neu_id) VALUES ('$ges_neu_id', '1')";			// neue Kombi
				$produkt_id = id_ermitteln($db, $sql1, $sql2);

			}	// Ende Gesellschaft eingegeben kein Tarif
		
			else {	 // Gesellschaft und Tarif eingegeben
				
				echo "Tarif: $tarif_alt<br>";
			
				$sql1 = "SELECT tarif_alt_id FROM tarif_alt WHERE tarif_alt = '$tarif_alt'";	// Tarif schon vorhanden
				$sql2 = "INSERT INTO tarif_alt (tarif_alt) VALUES ('$tarif_alt')";				// Tarif_alt neu
				$tarif_alt_id = id_ermitteln($db, $sql1, $sql2);
				
				echo "Tarif-ID: $tarif_alt_id<br>";
			
				$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE ges_alt_id = '$ges_alt_id' AND tarif_alt_id = '$tarif_alt_id' ";	// kein tarif eingegeben
				$sql2 = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '$tarif_alt_id')";			// neue Kombi
				$produkt_alt_id = id_ermitteln($db, $sql1, $sql2);
				
				$sql1 = "SELECT tarif_neu_id FROM tarif_neu WHERE tarif_neu = '$tarif_alt'";	// Tarif schon vorhanden
				$sql2 = "INSERT INTO tarif_neu (tarif_neu) VALUES ('$tarif_alt')";				// Tarif_alt neu
				$tarif_neu_id = id_ermitteln($db, $sql1, $sql2);
			
				$sql1 = "SELECT produkt_neu_id FROM produkt_neu WHERE ges_neu_id = '$ges_neu_id' AND tarif_neu_id = '$tarif_neu_id' ";	// kein tarif eingegeben
				$sql2 = "INSERT INTO produkt_neu (ges_neu_id, tarif_neu_id) VALUES ('$ges_neu_id', '$tarif_neu_id')";			// neue Kombi
				$produkt_id = id_ermitteln($db, $sql1, $sql2);
				
			}	// Ende Gesellschaft und Tarif vorhanden
		} // ende else Feld ges_alt ist belegt
		
// Ende Bearbeitung Gesellschaft-ALT und Tarif-ALT --------------------------------------------------------------------------------------------------

if ($produkt_alt_id > 1) {
	// bis ----------------------------------------------------------------------------------------------------
	if (empty($b_tag) OR empty($b_monat) OR empty($b_jahr)) {
		$bis = "0000-00-00";
	}
	else {
		$bis = "20$b_jahr-$b_monat-$b_tag";
	}
}

// Bearbeitung Gesellschaft-NEU und Tarif-NEU
// Eingabe von ges_neu und tarif_neu überschreibt select-feld
	// ---------------------------------------------------------------------------------------------------------------------------------------
	
		$ges_neu = quote_smart($ges_neu);
		$tarif_neu = quote_smart($tarif_neu);
	
		if (empty($ges_neu)) {					// keine ges_neu
			if (empty($tarif_neu)) {			// kein neuer tarif eingegeben
			
				list($ges, $tarif) = explode(" -> ", $produkt_neu);	// Gesellschaft und Tarif trennen
		
				if (empty($ges)) {	// keine Gesellschaft - kann auch kein Tarif sein
					$produkt_neu_id = "1";
				}
				else {	// Gesellschaft vorhanden
		
					$sql = "SELECT produkt_neu_id FROM produkt_neu, ges_neu, tarif_neu ";
					$sql .= "WHERE produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id=tarif_neu.tarif_neu_id ";
				
					if (empty($tarif)) {													// Gesellschaft vorhanden kein Tarif
			
						$sql .= "AND ges_neu.ges_neu = '$ges' AND tarif_neu.tarif_neu_id = '1'";
						$query = myqueryi($db, $sql);
						$ergebnis = mysqli_fetch_array($query);
						$produkt_neu_id = $ergebnis[0];
					}	// Ende Gesellschaft vorhanden kein Tarif
		
					else {																		// Gesellschaft und Tarif vorhanden
						$sql .= "AND ges_neu.ges_neu = '$ges' AND tarif_neu.tarif_neu = '$tarif'";
						$query = myqueryi($db, $sql);
						$ergebnis = mysqli_fetch_array($query);
						$produkt_neu_id = $ergebnis[0];
					}	// Ende Gesellschaft und Tarif vorhanden
				}	// Ende Gesellschaft vorhanden
			} // ende if kein neuer Tarif eingegeben
			
			
			else { // neuer Tarif eingegeben - gesellschaft muss aus dem Select-Feld kommen!!
				list($ges, $tarif) = explode(" -> ", $produkt_neu);	// Gesellschaft und Tarif trennen
				if (empty($ges)) {	// keine Gesellschaft - kann auch kein Tarif sein
					$fehler = "Sie müssen eine Gesellschaft auswählen!";
				}
				else {
					$sql1 = "SELECT tarif_neu_id FROM tarif_neu WHERE tarif_neu = '$tarif_neu'";	// Tarif_neu schon vorhanden?
					$sql2 = "INSERT INTO tarif_neu (tarif_neu) VALUES ('$tarif_neu')";				// Tarif_neu neu
					$tarif_neu_id = id_ermitteln($db, $sql1, $sql2);
					
					$sql = "SELECT ges_neu_id FROM ges_neu WHERE ges_neu = '$ges'";
					$query = myqueryi($db, $sql);
					$daten = mysqli_fetch_array($query);
					$ges_neu_id = $daten[0];
					
					$sql1 = "SELECT produkt_neu_id FROM produkt_neu WHERE tarif_neu_id = '$tarif_neu_id' AND ges_neu_id = '$ges_neu_id' ";
					$sql2 = "INSERT INTO produkt_neu (tarif_neu_id, ges_neu_id) VALUES ('$tarif_neu_id', '$ges_neu_id')";		// neue Kombi
					$produkt_neu_id = id_ermitteln($db, $sql1, $sql2);
					
					// auch für Produkt_neu
					
					$sql1 = "SELECT tarif_alt_id FROM tarif_alt WHERE tarif_alt = '$tarif_neu'";	// Tarif_neu schon vorhanden?
					$sql2 = "INSERT INTO tarif_alt (tarif_alt) VALUES ('$tarif_neu')";				// Tarif_neu neu
					$tarif_alt_id = id_ermitteln($db, $sql1, $sql2);
					
					$sql = "SELECT ges_alt_id FROM ges_alt WHERE ges_alt = '$ges'";
					$query = myqueryi($db, $sql);
					$daten = mysqli_fetch_array($query);
					$ges_alt_id = $daten[0];
					
					$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE tarif_alt_id = '$tarif_alt_id' AND ges_alt_id = '$ges_alt_id' ";
					$sql2 = "INSERT INTO produkt_alt (tarif_alt_id, ges_alt_id) VALUES ('$tarif_alt_id', '$ges_alt_id')";		// neue Kombi
					$produkt_id = id_ermitteln($db, $sql1, $sql2);
				}
			}	// ende else neuer tarif eingegeben
		}	// ende if keine ges_alt
		
		else {	// feld ges_neu ist belegt
		
			$sql1 = "SELECT ges_neu_id FROM ges_neu WHERE ges_neu = '$ges_neu'";	// Gesellschaft_alt schon vorhanden
			$sql2 = "INSERT INTO ges_neu (ges_neu) VALUES ('$ges_neu')";			// Gesellschaft_alt neu
			$ges_neu_id = id_ermitteln($db, $sql1, $sql2);
			
			$sql1 = "SELECT ges_alt_id FROM ges_alt WHERE ges_alt = '$ges_neu'";	// Gesellschaft_alt schon vorhanden
			$sql2 = "INSERT INTO ges_alt (ges_alt) VALUES ('$ges_neu')";			// Gesellschaft_alt neu
			$ges_alt_id = id_ermitteln($db, $sql1, $sql2);
			
			if (empty($tarif_neu)) {													// Gesellschaft vorhanden kein Tarif
			
				$sql1 = "SELECT produkt_neu_id FROM produkt_neu WHERE ges_neu_id = '$ges_neu_id' AND tarif_neu_id = '1' ";	// kein tarif eingegeben
				$sql2 = "INSERT INTO produkt_neu (ges_neu_id, tarif_neu_id) VALUES ('$ges_neu_id', '1')";			// neue Kombi
				$produkt_neu_id = id_ermitteln($db, $sql1, $sql2);
			
				$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE ges_alt_id = '$ges_alt_id' AND tarif_alt_id = '1' ";	// kein tarif eingegeben
				$sql2 = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '1')";			// neue Kombi
				$produkt_id = id_ermitteln($db, $sql1, $sql2);

			}	// Ende Gesellschaft eingegeben kein Tarif
		
			else {	 // Gesellschaft und Tarif eingegeben
			
				$sql1 = "SELECT tarif_neu_id FROM tarif_neu WHERE tarif_neu = '$tarif_neu'";	// Tarif schon vorhanden
				$sql2 = "INSERT INTO tarif_neu (tarif_neu) VALUES ('$tarif_neu')";				// Tarif_alt neu
				$tarif_neu_id = id_ermitteln($db, $sql1, $sql2);
			
				$sql1 = "SELECT produkt_neu_id FROM produkt_neu WHERE ges_neu_id = '$ges_neu_id' AND tarif_neu_id = '$tarif_neu_id' ";	// tarif eingegeben
				$sql2 = "INSERT INTO produkt_neu (ges_neu_id, tarif_neu_id) VALUES ('$ges_neu_id', '$tarif_neu_id')";			// neue Kombi
				$produkt_neu_id = id_ermitteln($db, $sql1, $sql2);
			
				$sql1 = "SELECT tarif_alt_id FROM tarif_alt WHERE tarif_alt = '$tarif_neu'";	// Tarif schon vorhanden
				$sql2 = "INSERT INTO tarif_alt (tarif_alt) VALUES ('$tarif_neu')";				// Tarif_alt neu
				$tarif_alt_id = id_ermitteln($db, $sql1, $sql2);
			
				$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE ges_alt_id = '$ges_alt_id' AND tarif_alt_id = '$tarif_alt_id' ";	// kein tarif eingegeben
				$sql2 = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '$tarif_alt_id')";			// neue Kombi
				$produkt_id = id_ermitteln($db, $sql1, $sql2);
				
			}	// Ende Gesellschaft und Tarif vorhanden
		} // ende else Feld ges_alt ist belegt
		
		// Ende Bearbeitung Gesellschaft-NEU und Tarif-NEU --------------------------------------------------------------------------------------------------

		if ($produkt_neu_id > 1) {
			// seit ----------------------------------------------------------------------------------------------------
			if (empty($s_tag) OR empty($s_monat) OR empty($s_jahr)) {
				$seit = "0000-00-00";
			}
			else {
				$seit = "20$s_jahr-$s_monat-$s_tag";
			}
		}
		
		if(isset($einzug)) {
			$einzug = "1";
		}
	
// Ende Bearbeitung Kundendaten ---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------

$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
$sql .= "wiedervorlage = '1', einzug = '$einzug',  belegt = '0', nichtkunde = '0', neukunde = '0' ";
$sql .= "WHERE kunden_id = '$kunden_id'";
$query = myqueryi($db, $sql);

$fehler = "Daten für den Kunden ID: $kunden_id wurden geändert!";


if ($t_id) {

	$w_tag = quote_smart($w_tag);
	$w_monat = quote_smart($w_monat);
	$w_jahr = quote_smart($w_jahr);
		
	$d_tag_neu = quote_smart($d_tag_neu);
	$d_monat_neu = quote_smart($d_monat_neu);
	$d_jahr_neu = quote_smart($d_jahr_neu);
	
	$s_tag = quote_smart($s_tag);
	$s_monat = quote_smart($s_monat);
	$s_jahr = quote_smart($s_jahr);

	$b_tag = quote_smart($b_tag);
	$b_monat = quote_smart($b_monat);
	$b_jahr = quote_smart($b_jahr);
	
	$rem_termin = quote_smart($rem_termin);
	$t_bemerkung = quote_smart($t_bemerkung);
	

	// Wiedervorlage ---------------------------------------------------------------------------------------------------------------------
	if (isset($t_wiedervorlage)) { // Wiedervorlage
	
		if (empty($w_tag) OR empty($w_monat) OR empty($w_jahr)) {	// kein Wiedervorlagetermin eingetragem
	
			$fehler = "Wiedervorlagedatum nicht korrekt!";
	
		} // ende if kein Wiedervorlagetermin eingetragen
		else { // Wiedervorlagetermin eingegeben
		
			$muster = "/^\d{2}$/";							// Test Datum, genau 2 Ziffern

			if(!preg_match($muster, $w_tag) OR !preg_match($muster, $w_monat) OR !preg_match($muster, $w_jahr)) {	// Muster Datum passt nicht
				$fehler = "Fehler: Wiedervorlagedatum nicht korrekt!";
			}
			else {  // Muster wiedervorlage Datum stimmt
			
				$wiedervorlage_datum = "20$w_jahr-$w_monat-$w_tag";
				
				if ($wiedervorlage_datum < $heute) {	// Termin in der Vergangenheit!!
						$fehler = "Die Wiedervorlage kann nicht in der Vergangenheit liegen!";
				}
				else { // Wiedervorlage in der Zukunft
				
					$sql = "UPDATE termin SET wiedervorlage_date = '$wiedervorlage_datum', w_zeit = '$w_zeit', wiedervorlage = '1', nichtkunde='0', ";
					$sql .= "produkt_alt_id = '$produkt_alt_id', bis = '$bis', produkt_neu_id = '$produkt_neu_id', seit = '$seit', ";
					$sql .= "aussendienst = '$ad', rem_termin = '$rem_termin', aquiriert = '$aquiriert', ";
					$sql .= "termin = '', zeit = '00:00' ";
					$sql .= "WHERE termin_id = '$t_id'";
					$query = myqueryi($db, $sql);
									
					$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
					$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
					$sql .= "wiedervorlage = '1', einzug = '$einzug',  belegt = '0', nichtkunde = '0', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kunden_id'";
					$query = myqueryi($db, $sql);
					
					$fehler = "Der Termin mit der ID: $t_id wurde zur Wiedervorlage";
					
				} // Ende else Wiedervorlage in der Zukunft
			} // ende else Muster Datum stimmt
		} // ende else Wiedervorlagetermin eingegeben
	} // ende if Wiedervorlage
	
	// Terminverlegung ------------------------------------------------------------------------------------------------------------------------------------------

	else { // Terminverlegung
	
		if (empty($d_tag_neu) OR empty($d_monat_neu) OR empty($d_jahr_neu)) {	// kein Termin eingetragem

			$fehler = "Datum nicht korrekt!";
	
		} // ende if kein Termin eingetragen
		
		
		else { // Termin eingegeben
		
			$muster = "/^\d{2}$/";							// Test Datum, genau 2 Ziffern

			if(!preg_match($muster, $d_tag_neu) OR !preg_match($muster, $d_monat_neu) OR !preg_match($muster, $d_jahr_neu)) {	// Muster Datum passt nicht
				$fehler = "Fehler: Datum nicht korrekt!";
			}
			else {  // Muster wiedervorlage Datum stimmt
			
				$termin_neu = "20$d_jahr_neu-$d_monat_neu-$d_tag_neu";
			
				if ($termin_neu < $heute) {	// Termin in der Vergangenheit!!
						$fehler = "Der Termin kann nicht in der Vergangenheit liegen!";
				}
				else { // Termin in der Zukunft
					
					$sql = "UPDATE termin SET termin = '$termin_neu', zeit = '$zeit', wiedervorlage = '0', nichtkunde='0', ";
					$sql .= "produkt_alt_id = '$produkt_alt_id', bis = '$bis', produkt_neu_id = '$produkt_neu_id', seit = '$seit', ";
					$sql .= "aussendienst = '$ad', rem_termin = '$rem_termin', aquiriert = '$aquiriert' ";
					$sql .= "WHERE termin_id = '$t_id'";
					$query = myqueryi($db, $sql);
									
					$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
					$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$t_bemerkung', branche_id = '$branche_id', ";
					$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kunden_id'";
					$query = myqueryi($db, $sql);
					
					$fehler = "Der Termin mit der ID: $t_id wurde verlegt";
				
					
				} // Ende else Wiedervorlage in der Zukunft	
			} // ende else Muster Datum stimmt
		} // ende else Termin eingegeben
	} // ende else Termin eingetragen
} // ende if  termin id - terminvereinbarung
	}	// Ende else kann nur Termin oder Wiedervorlage folgen
} // Ende isset($_speichern)


// Ende Termindaten ------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------------------
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Kd.-T neu</title>
	<!-- admin/suche_daten.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />


</head>
<body>
<div align = "center">

<table border="1" width="380" bordercolor="#006699">
<?php
	if ($fehler OR $termin_anzahl[0]) {
 		echo "<tr bgcolor=\"#F0F8FF\">";
        echo "<td colspan=\"8\">";
		if ($fehler) {
			echo "<span style=\"font-weight:bold; color:red;\">$fehler</span><br>";
		}
      	echo "</td></tr>";
	}

?>
  <!-- Tabelle Rand -->
<form name="terminvereinbarung" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
	<tr cellspacing="0" cellpadding="0">
    	<td colspan="4"> 
	<tr bgcolor="#F0F8FF">
      <td colspan="4"><span style="font-weight:bold;">
   <?php
	  echo "AD:&nbsp;";
	  // Abfrage der Außendienstler
	  	$sql = "SELECT user FROM user WHERE gruppen_id = '3' ORDER BY user ASC";
		$query_user = myqueryi($db, $sql);
		
	  // Darstellung der Außendienster als SELECT-Liste
	 
		echo "<select name=\"t_user\" class = \"eingabe\">";
		echo " <option></option>";			 						// erste Zeile als Leerzeile, falls noch kein AD ausgewählt wurde
		for ($j = 0; $j < mysqli_num_rows($query_user); $j++)	{	// Anzahl der Datensätze
			$zeile = mysqli_fetch_row($query_user);					// Schleife für Daten-Zeilen
			if ($zeile[0] == $ad) {									// bestehender Termin wurde angeklickt - AD bereits vergeben
				echo " <option selected>$zeile[0]</option>";
			}
			else {
				echo " <option>$zeile[0]</option>";
			}
		}
	  echo "<select>";
	  
	  echo "&nbsp;&nbsp;Kd.: $kunden_id $kunde[vorname] $kunde[name] - Tel.: ($kunde[vorwahl1]) $kunde[telefon] - $kunde[plz] $kunde[ort]";
	  ?>
	  </span></td>
    </tr>
    <tr bgcolor="#F0F8FF"> 
      <td width="89">Termin</td>
      <td> 
        <input type="text"  class="eingabe" name="d_tag_neu" size = "2" maxlength ="2" value = "<?php echo "$d_tag"; ?>"/>
        . 
        <input type="text"  class="eingabe" name="d_monat_neu" size = "2" maxlength ="2" value = "<?php echo "$d_monat"; ?>"/>
        . 
        <input type="text"  class="eingabe" name="d_jahr_neu" size = "2" maxlength ="2" value = "<?php echo "$d_jahr"; ?>"/>
      </td>
      <td width="56">Zeit</td>
	  <td width="101"><select name="zeit">
	  <?php
			for ($i = 0; $i < count($t_zeit); $i++) { // Erzeugung der Zeilen
				if ($termin[zeit] === $t_zeit[$i]) {
					echo " <option selected>$t_zeit[$i]</option>";
				}
				else {
					echo " <option>$t_zeit[$i]</option>";
				}
			}
	 	?>
	 	</select>
      </td>
	 
    </tr>
	    <tr bgcolor="#F0F8FF"> 
      <td width="89">WVL&nbsp; <input type="checkbox" name="t_wiedervorlage"></td>
      <td><input type="text"  class="eingabe" name="w_tag" size = "2" maxlength ="2"/>
        . 
        <input type="text"  class="eingabe" name="w_monat" size = "2" maxlength ="2"/>
        . 
        <input type="text"  class="eingabe" name="w_jahr" size = "2" maxlength ="2"/></td>
	  <td>Zeit</td>
	  <td><select name="w_zeit">
	  <?php
			for ($i = 0; $i < count($wvl_zeit); $i++) { // Erzeugung der Zeilen
				echo " <option>$wvl_zeit[$i]</option>";
			}
	 	?>
	 	</select>      
      </td>
	  </tr>
<tr bgcolor="#F0F8FF"> 
      <td width="89">Tarif-alt</td>
      <td>
	<?php
	  		$sql = "select ges_alt, tarif_alt, produkt_alt_id FROM produkt_alt, tarif_alt, ges_alt ";
			$sql .= "WHERE produkt_alt.ges_alt_id = ges_alt.ges_alt_id ";
			$sql .= "And produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
			$sql .= "ORDER BY ges_alt ASC, tarif_alt ASC ";

			$query_ges_alt = myqueryi($db, $sql);
			
			for ($i = 0; $i < mysqli_num_rows($query_ges_alt); $i++) {
			
				$daten = mysqli_fetch_array($query_ges_alt);
					
				if ($termin[produkt_alt_id] == 1) {					// Produkt = Standard 1
					
					mysqli_data_seek($query_ges_alt,0);
					echo "<select name=\"produkt_alt\" class=\"eingabe\">";			

					for ($i = 0; $i < mysqli_num_rows($query_ges_alt); $i++) {
	
						$daten = mysqli_fetch_array($query_ges_alt);
		
						$produkt_alt = $daten[0];
		
						if (!empty($daten[1])) {
		
						$produkt_alt = $daten[0].chr(32)."->".chr(32).$daten[1];			//chr[32) = Leerzeichen
						}
					echo "<option>$produkt_alt</option>";
					}
		
    			 echo "</select>";
				} // Ende Produkt = Standard 1
				else {  // Produkt hat einen Wert
					if ($termin[produkt_alt_id] == $daten[produkt_alt_id]) {
				
						if (empty($daten[1])) {
							echo " <input type=\"text\"  class=\"eingabe\" name=\"produkt_alt\" value=\"$daten[0]\"/>";
						}
						else {
							echo " <input type=\"text\"  class=\"eingabe\" name=\"produkt_alt\" value=\"$daten[0]" .chr(32). "->" . chr(32). "$daten[1]\"/>";
						}
					}
				}  // ende Produkt hat einen Wert
			}
			
			if (empty($termin[produkt_alt_id])) {
				mysqli_data_seek($query_ges_alt,0);
				echo "<select name=\"produkt_alt\" class=\"eingabe\">";			

					for ($i = 0; $i < mysqli_num_rows($query_ges_alt); $i++) {
	
						$daten = mysqli_fetch_array($query_ges_alt);
		
						$produkt_alt = $daten[0];
		
						if (!empty($daten[1])) {
		
						$produkt_alt = $daten[0].chr(32)."->".chr(32).$daten[1];			//chr[32) = Leerzeichen
						}
					echo "<option>$produkt_alt</option>";
					}
		
    			 echo "</select>";
			}
		?>
      </td>
	  	  <td>bis</td>
	  <td>
        <input type="text"  class="eingabe" name="b_tag" size = "2" maxlength ="2"/>
        . 
        <input type="text"  class="eingabe" name="b_monat" size = "2" maxlength ="2"/>
        . 
        <input type="text"  class="eingabe" name="b_jahr" size = "2" maxlength ="2"/>
      </td>
    </tr>
	<tr bgcolor="#F0F8FF"> 
      <td width="89">&nbsp;</td>
      <td>Ges.&nbsp;<input type="text"  class="eingabe" name="ges_alt" size ="15" maxlengt="20"></td>
	  <td>Tarif alt:
	  <td><input type="text"  class="eingabe" name="tarif_alt" size ="20" maxlengt="20"></td>
	  </tr>
		<tr bgcolor="#F0F8FF"> 
      <td width="89">Tarif-neu</td>
      <td>
	 <?php
	  		$sql = "select ges_neu, tarif_neu, produkt_neu_id FROM produkt_neu, tarif_neu, ges_neu ";
			$sql .= "WHERE produkt_neu.ges_neu_id = ges_neu.ges_neu_id ";
			$sql .= "And produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
			$sql .= "ORDER BY ges_neu ASC, tarif_neu ASC ";

			$query_ges_neu = myqueryi($db, $sql);
			
			for ($i = 0; $i < mysqli_num_rows($query_ges_neu); $i++) {
			
				$daten_neu = mysqli_fetch_array($query_ges_neu);
					
				if ($termin[produkt_neu_id] == 1) {					// Produkt = Standard 1
					
					mysqli_data_seek($query_ges_neu,0);
					echo "<select name=\"produkt_neu\" class=\"eingabe\">";			

					for ($i = 0; $i < mysqli_num_rows($query_ges_neu); $i++) {
	
						$daten_neu = mysqli_fetch_array($query_ges_neu);
		
						$produkt_neu = $daten_neu[0];
		
						if (!empty($daten_neu[1])) {
		
						$produkt_neu = $daten_neu[0].chr(32)."->".chr(32).$daten_neu[1];			//chr[32) = Leerzeichen
						}
					echo "<option>$produkt_neu</option>";
				}
		
    			 echo "</select>";
				} // Ende Produkt = Standard 1
				else {  // Produkt hat einen Wert
					if ($termin[produkt_neu_id] == $daten_neu[produkt_neu_id]) {
				
						if (empty($daten_neu[1])) {
							echo " <input type=\"text\"  class=\"eingabe\" name=\"produkt_neu\" value=\"$daten_neu[0]\"/>";
						}
						else {
							echo " <input type=\"text\"  class=\"eingabe\" name=\"produkt_neu\" value=\"$daten_neu[0]" .chr(32). "->" . chr(32). "$daten_neu[1]\"/>";
						}
					}
				}  // ende Produkt hat einen Wert
			}
			
			if (empty($termin[produkt_neu_id])) {
				mysqli_data_seek($query_ges_neu,0);
				echo "<select name=\"produkt_neu\" class=\"eingabe\">";			

					for ($i = 0; $i < mysqli_num_rows($query_ges_neu); $i++) {
	
						$daten_neu = mysqli_fetch_array($query_ges_neu);
		
						$produkt_neu = $daten_neu[0];
		
						if (!empty($daten_neu[1])) {
		
						$produkt_neu = $daten_neu[0].chr(32)."->".chr(32).$daten_neu[1];			//chr[32) = Leerzeichen
						}
					echo "<option>$produkt_neu</option>";
					}
		
    			 echo "</select>";
			}
		?>
      </td>
	  	  <td>seit</td>
	  <td>
        <input type="text"  class="eingabe" name="s_tag" size = "2" maxlength ="2"/>
        . 
        <input type="text"  class="eingabe" name="s_monat" size = "2" maxlength ="2"/>
        . 
        <input type="text"  class="eingabe" name="s_jahr" size = "2" maxlength ="2"/>
      </td>
    </tr>
		<tr bgcolor="#F0F8FF"> 
      <td width="89">&nbsp;</td>
      <td>Ges.&nbsp;<input type="text"  class="eingabe" name="ges_neu" size ="15" maxlengt="20"></td>
	  <td>Tarif neu:
	  <td><input type="text"  class="eingabe" name="tarif_neu" size ="20" maxlengt="20"></td>
	  </tr>
   <tr bgcolor="#F0F8FF"> 
      <td width="89">Bemerkung</td>
      <td colspan="3"><textarea cols="50" rows="3" class="eingabe" name="rem_termin"><?php echo "$termin[rem_termin]"; ?></textarea></td>
    </tr>
	 <tr bgcolor="#F0F8FF"> 
      <td colspan="4" align="center">
	  Wiedervorl.
	<?php
			if ($kunde[wiedervorlage] == 0) {
				echo "<input type=\"checkbox\"  name=\"wiedervorlage\">";
			}
			else {
				echo "<input type=\"checkbox\" name=\"wiedervorlage\" checked>";
		}
		?>
		&nbsp;Vertragskd.
	<?php
			if ($kunde[vertragskunde] == 0) {
				echo "<input type=\"checkbox\" name=\"t_vertragskunde\">";
			}
			else {
				echo "<input type=\"checkbox\" name=\"t_vertragskunde\" checked>";
		}
		?>
		&nbsp;Einzug
	<?php
			if ($kunde[einzug] == 0) {
				echo "<input type=\"checkbox\" name=\"einzug\">";
			}
			else {
				echo "<input type=\"checkbox\" name=\"einzug\" checked>";
		}
		?>
		&nbsp;Storno<input type="checkbox"  name="t_storno">
		&nbsp;Nichtkd.<input type="checkbox"  name="nichtkunde">
		&nbsp;verzogen<input type="checkbox"  name="verzogen">
		</td>
    </tr>
	

    <!-- Ende Zeile oben für Termin -->
	    <tr bgcolor="#F0F8FF"> 
      <td colspan ="4" align = "center"> 
        <input type="submit" name="t_speichern" value = "Speichern" class="submitt"/>
      </td>
    </tr>

  <!-- ENDE Termin / ANFANG Kunde -->
		<tr bgcolor="#F0F8FF"> 
      <td width="89">Vorname</td>
      <td width="130"> 
        <input type="text" class="eingabe" name="kd_vorname" maxlength = "20" size = "20" value = "<?php echo "$kunde[vorname]"; ?>"/>
      </td>
      <td width="89">Name</td>
      <td>
	  	<input type="text"  class="eingabe" name="kd_name" maxlength = "20" size = "20" value = "<?php echo "$kunde[name]"; ?>"/>
      </td>
    </tr>
	<tr bgcolor="#F0F8FF"> 
      <td width="89">Ort</td>
      <td width="130"> 
        <input type="text"  class="eingabe" name="t_ort" maxlength = "20" size = "20" value = "<?php echo "$kunde[ort]"; ?>"/>
      </td>
      <td width="89">Straße</td>
      <td>
	  	<input type="text" class="eingabe" name="t_strasse" maxlength = "35" size = "20" value = "<?php echo "$kunde[strasse]"; ?>"/>
      </td>
    </tr>
	
    <tr bgcolor="#F0F8FF"> 
      <td  width="89">Ortsteil</td>
      <td  width="130">
	  <select name="ortsteil_alt" class="eingabe">
   <?php
	  	$sql = "SELECT ortsteil FROM poo, plz, ort, ortsteil ";
		$sql .= "WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id ";
		$sql .= "AND ort.ort = '$kunde[ort]' ";
		$ortsteil = myqueryi($db, $sql);
		
		for ($j = 0; $j < mysqli_num_rows($ortsteil); $j++)	{		// Anzahl der Datensätze
				$zeile = mysqli_fetch_row($ortsteil);						// Schleife für Daten-Zeilen
				if 	($zeile[0] == $kunde[ortsteil]) {
					echo "<option selected>$zeile[0]</option>";
				}
				else {
					echo "<option>$zeile[0]</option>";
				}
			}
	  ?>
	  </select>
      </td>
      <td  width="89"><strong>OT neu</strong></td>
      <td>
	  <input type="text"  class="eingabe" name="ortsteil_neu" maxlength = "25" size = "20" />
      </td>
    </tr>
	  <tr bgcolor="#F0F8FF"> 
      <td width="89">geboren</td>
      <td> 
	   <?php
		if (($g_tag == "00") AND ($g_monat == "00") AND ($g_jahr == "0000")) {
	  		echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_tag\" size = \"2\" maxlength =\"2\"> . "; 
        	echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_monat\" size = \"2\" maxlength =\"2\"> . "; 
        	echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_jahr\" size = \"2\" maxlength =\"2\">";
		} 
		else {
			echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_tag\" size = \"2\" maxlength =\"2\" value = \"$g_tag\"> . "; 
        	echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_monat\" size = \"2\" maxlength =\"2\" value = \"$g_monat\"> . "; 
        	echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_jahr\" size = \"2\" maxlength =\"2\" value = \"$g_jahr\">";
		}
	?>
      </td>
      <td width="89">&nbsp;</td>
      <td>
	  	&nbsp;
      </td>
    </tr>
    <tr bgcolor="#F0F8FF"> 
      <td width="89">Vorwahl 2</td>
      <td width="130"> 
        <input type="text"  class="eingabe" name="t_vorwahl2" size="8" value = "<?php echo "$kunde[vorwahl2]"; ?>"/>
      </td>
      <td width="89">Telefon 2</td>
      <td> 
        <input type="text"  class="eingabe" name="t_fax" size="12" value = "<?php echo "$kunde[fax]"; ?>"/>
      </td>
    </tr>
    <tr bgcolor="#F0F8FF"> 
      <td width="89">E-Mail</td>
      <td width="130"> 
        <input type="text"  class="eingabe" name="t_email" size="20" value = "<?php echo "$kunde[email]"; ?>"/>
      </td>
      <td width="89">Branche</td>
      <td> 
		 <select name="t_branche" class = "eingabe">
	  <?php
		   	$sql = "SELECT branche FROM branche WHERE branche_id > '1' ORDER BY branche ASC";
			$query_branche = myqueryi($db, $sql);
			
			echo " <option></option>";
			for ($j = 0; $j < mysqli_num_rows($query_branche); $j++)	{		// Anzahl der Datensätze
				$zeile = mysqli_fetch_row($query_branche);						// Schleife für Daten-Zeilen
				if ($zeile[0] == $kunde[branche]) {
					echo " <option selected>$zeile[0]</option>";
				}
				else {
					echo " <option>$zeile[0]</option>";
				}
			}
	 	?>
    	</select>
      </td>
    </tr>
    <tr bgcolor="#F0F8FF"> 
      <td width="89">Bemerkung</td>
      <td colspan="3"> 
        <textarea cols="50" rows="1" class="eingabe" name="t_bemerkung"><?php echo "$kunde[rem_kunde]"; ?></textarea>
      </td>
    </tr>	
	    <!-- Ende Zeile unten für Kunde -->
  </table>
</form> 
<!-- Ende Tabelle Rand -->

</div>
</body>
</html>