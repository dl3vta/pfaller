<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();

$telefonist = $_SESSION['benutzer_kurz'];
//echo "telefonist = $telefonist";

$begriff1 = array("alle", "Vorwahl", "Telefon", "Name", "Vorname", "Ort", "Ortsteil", "Straße", "Bemerkung", "geboren");	// Suchbegriffe für Feld1
$begriff2 = array("Telefon", "Vorwahl", "Name", "Vorname", "Ort", "Ortsteil", "Straße", "Bemerkung", "geboren");			// Suchbegriffe für Feld1
$begriff3 = array("Name", "Vorname", "Ort", "Ortsteil", "Straße", "Telefon", "Vorwahl", "Bemerkung", "geboren");			// Suchbegriffe für Feld1

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++ SUCHEN GEDRüCKT +++++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if ($suchen) {
/*
echo "wert1: $wert1<br>";
echo "wert2: $wert2<br>";
echo "wert3: $wert3<br>";
echo "feld1: $feld1<br>";
echo "feld2: $feld2<br>";
echo "feld3: $feld3<br>";
*/

?>

<!DOCTYPE html>
<html xlang="de">
<head>
<title>Suche-Input</title>
	<!-- admin/suche_input.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align="center">
<table width="600" border="0" cellpadding="4" cellspacing="4">
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">
<form name="input" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
  <table width="100%"  border="0" cellspacing="1" cellpadding="1">
    <tr>
      <td><input type="text" name="wert1" maxlength = "20" size ="20" class = "eingabe"></td>
      <td><select name="feld1" class = "eingabe">	  
		  <?php
			for ($i = 0; $i < count($begriff1); $i++) { // Erzeugung der Zeilen
				echo " <option>$begriff1[$i]</option>";
			}
	 	?>
      </select></td>
    </tr>
    <tr>
      <td><input type="text" name="wert2" maxlength = "20" size ="20" class = "eingabe"></td>
      <td><select name="feld2" class = "eingabe">
		  <?php
			for ($i = 0; $i < count($begriff2); $i++) { // Erzeugung der Zeilen
				echo " <option>$begriff2[$i]</option>";
			}
	 	?>
      </select></td>
    </tr>
    <tr>
      <td><input type="text" name="wert3" maxlength = "20" size ="20" class = "eingabe"></td>
      <td><select name="feld3" class = "eingabe">
		  <?php
			for ($i = 0; $i < count($begriff3); $i++) { // Erzeugung der Zeilen
				echo " <option>$begriff3[$i]</option>";
			}
	 	?>
      </select></td>
    </tr>
    <tr>
      <td colspan="2"><input type="submit" name="suchen" value="Suchen" class = "suche">&nbsp;&nbsp;<input type="reset" name="reset" value="Löschen" class = "correct"></td>
    </tr>
  </table>
</form>
</td></tr>
<!-- Ende Zeile Abfrage / Start Zeile Ergebnisse ++++++++++++++++++++++++++++++++++ // -->
<tr>
<?php

if (empty($wert1) AND empty($wert2) AND empty($wert3)) {

	echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: keine Suchbegriffe eingetragen!</span></td>";
}

elseif (empty($wert1) AND (!empty($wert2) OR (!empty($wert3)))) {
	echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Feld 1 muss belegt sein!</span></td>";
}

else { // Daten sind eingegeben

	$wert1 = quote_smart($wert1);
	$wert2 = quote_smart($wert2);
	$wert3 = quote_smart($wert3);
	
	// Feld1 ist belegt und alle ausgewählt - die anderen Felder werden ignoriert
	
	if ($feld1 == "alle") {
		$sql = "SELECT kunden_id AS ID, vorname AS Vorname, name AS Name, ";
		$sql .= "plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, ";
		$sql .= "strasse AS Strasse, vorwahl1 AS Vorwahl, telefon AS Telefon ";
		$sql .= "FROM kunden, branche, name, vorname, ";
		$sql .= "plz, poo, ort, ortsteil, ";
		$sql .= "vorwahl1 ";
		$sql .= "WHERE kunden.branche_id = branche.branche_id AND kunden.name_id = name.name_id AND kunden.vorname_id = vorname.vorname_id ";
		$sql .= "AND kunden.poo_id = poo.poo_id ";
		$sql .= "AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
		$sql .= "AND poo.plz_id = plz.plz_id ";
		$sql .= "AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
		$sql .= "AND (kunden.telefon LIKE '%$wert1%' OR vorwahl1.vorwahl1 LIKE '%$wert1%' OR vorname.vorname LIKE '%$wert1%' OR name.name LIKE '%$wert1%' OR kunden.geboren LIKE '%$wert1%' ";
		$sql .= " OR ort.ort LIKE '%$wert1%' OR ortsteil.ortsteil LIKE '%$wert1%' OR kunden.strasse LIKE '%$wert1%' OR kunden.rem_kunde LIKE '%$wert1%') ";
		$sql .= " ORDER BY ort ASC, strasse ASC, ortsteil ASC";
		$abfrage = myqueryi($db, $sql);
	}
	
	// die anderen Varianten werden durchgespielt
	
	else { // Feld1 ist nicht "alle"
	
		$sql = "SELECT kunden_id AS ID, vorname AS Vorname, name AS Name, ";
		$sql .= "plz AS plz, ort AS ort, ortsteil AS Ortsteil, ";
		$sql .= "strasse AS Strasse, vorwahl1 AS Vorwahl, telefon AS Telefon ";
		$sql .= "FROM kunden, branche, name, vorname, ";
		$sql .= "plz, poo, ort, ortsteil, ";
		$sql .= "vorwahl1 ";
		$sql .= "WHERE kunden.branche_id = branche.branche_id AND kunden.name_id = name.name_id AND kunden.vorname_id = vorname.vorname_id ";
		$sql .= "AND kunden.poo_id = poo.poo_id ";
		$sql .= "AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
		$sql .= "AND poo.plz_id = plz.plz_id ";
		$sql .= "AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	
		// Behandlung wert1
		
		if (empty($wert2) AND empty($wert3)) {		// nur wert1 eingegeben, wert2 und wert3 sind leer
		
			switch($feld1) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert1%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert1%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert1%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert1%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert1%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert1%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert1%' ";
				break;
				case Bemerkung:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert1%' ";
				break;
				case geboren:
					$sql .= "AND kunden.geboren LIKE '%$wert1%' ";
				break;
			}// ende switch feld1
			$abfrage = myqueryi($db, $sql);
		} // ende if nur wert1 eingegeben, wert2 und wert3 sind leer
		
		// Behandlung wert2
		
		elseif (!empty($wert2) AND empty($wert3)) {		// wert1 und wert2 eingegeben, wert3 ist leer
		
			if ($feld1 == $feld2) { // die beiden Suchkriterien dürfen nicht gleich sein
				echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Suchkriterien müssen unterschiedlich sein!</span></td>";
			}
			
			else { // suchkriterien sind unterschiedlich
			
			switch($feld1) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert1%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert1%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert1%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert1%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert1%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert1%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert1%' ";
				break;
				case Bemerkung:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert1%' ";
				break;
				case geboren:
					$sql .= "AND kunden.geboren LIKE '%$wert1%' ";
				break;
			}// ende switch feld1
			
			switch($feld2) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert2%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert2%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert2%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert2%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert2%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert2%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert2%' ";
				break;
				case Bemerkung:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert2%' ";
				break;
				case geboren:
					$sql .= "AND kunden.geboren LIKE '%$wert2%' ";
				break;
			}// ende switch feld1
			$abfrage = myqueryi($db, $sql);
			} // ende else suchkriterien sind unterschiedlich
		} // ende elseif wert1 und wert2 eingegeben, wert3 ist leer
		
		elseif (empty($wert2) AND !empty($wert3)) {		// wert1 und wert3 eingegeben, wert2 ist leer
			echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Bitte Feld2 ausfüllen!</span></td>";
		}
		
		// Behandlung wert1,2,3
		
		elseif (!empty($wert2) AND !empty($wert3)) {		// wert1, wert2 und wert3 eingegeben
		
			if (($feld1 == $feld2) OR ($feld1 == $feld3) OR ($feld2 == $feld3) ) { // die Suchkriterien dürfen nicht gleich sein
				echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Suchkriterien müssen unterschiedlich sein!</span></td>";
			}
			
			else { // suchkriterien sind unterschiedlich
			
			switch($feld1) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert1%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert1%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert1%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert1%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert1%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert1%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert1%' ";
				break;
				case Bemerkung:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert1%' ";
				break;
				case geboren:
					$sql .= "AND kunden.geboren LIKE '%$wert1%' ";
				break;
			}// ende switch feld1
			
			switch($feld2) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert2%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert2%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert2%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert2%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert2%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert2%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert2%' ";
				break;
				case Bemerkung:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert2%' ";
				break;
				case geboren:
					$sql .= "AND kunden.geboren LIKE '%$wert2%' ";
				break;
			}// ende switch feld2
			
			switch($feld3) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert3%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert3%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert3%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert3%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert3%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert3%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert3%' ";
				break;
				case Bemerkung:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert3%' ";
				break;
				case geboren:
					$sql .= "AND kunden.geboren LIKE '%$wert3%' ";
				break;
			}// ende switch feld3
			$abfrage = myqueryi($db, $sql);
			} // ende else suchkriterien sind unterschiedlich
		} // ende elseif alle Felder belegt
	} // ende else Feld1 ist nicht alle

if ($abfrage) { // es existiert ein Abfrageergebnis

echo "<td valign = \"top\">";
echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">";

//Ausgabe der kundenliste +++++++++++++++++++++++++++++++++++++++++++++++

	//zähler der datensätze für bg_colour der zeilen
	$z=0;
	
	//die beiden hintergrundfarben
	$bg1 = "#eeeeee";
	$bg2 = "#dddddd";

	echo "<tr>";													// Tabellenkopf
	for($i = 0; $i < mysqli_num_fields($abfrage); $i++)				// Anzahl der Tabellenzellen pro Zeile
	{
		$row = mysqli_fetch_assoc($abfrage);
		$feldname = array_keys($row);
		mysqli_data_seek($abfrage, 0);					// Name der Tabellenzelle
		echo "<th>$feldname[$i]</th>";
	}
	echo "</tr>\n";													// Tabellenkopf Ende
	while($zeile = mysqli_fetch_row($abfrage))						// Schleife für Daten-Zeilen
	{
		echo "<tr ";
		echo "bgcolor=";
		$bg=($z++ % 2) ? $bg1 : $bg2;
		echo $bg;
		echo ">";
		for($i = 0; $i < mysqli_num_fields($abfrage); $i++)			// Schleife für Felder
		{	
			//echo "<td class=\"liste\" align = \"left\"><a href=\"kunden_t_neu.php?kd_id=$zeile[0]\" target=\"kd_rechts\">" . $zeile[$i] . "</a></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"suche_results.php?kd_id=$zeile[0]\" target=\"_self\">" . $zeile[$i] . "</a></td>";
		}					
		echo "</tr>";
    }

echo "</table>";
echo "</td>";
} // ende if es existiert ein Abfrageergebnis
} // ende else Daten sind eingegeben


?>

</tr></table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>

<?php

} // Ende IF SUCHEN


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++ NICHTS GEDRüCKT - SCRIPT STARTET++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

else {
?>

<!DOCTYPE html>
<html lang="de">
<head>
<title>Suche-Input</title>
	<!-- admin/suche_input.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align="center">
<table width="600" border="0" cellpadding="4" cellspacing="4">
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">
<form name="input" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
  <table width="100%"  border="0" cellspacing="1" cellpadding="1">
    <tr>
      <td><input type="text" name="wert1" maxlength = "20" size ="20" class = "eingabe"></td>
      <td><select name="feld1" class = "eingabe">
		  <?php
			for ($i = 0; $i < count($begriff1); $i++) { // Erzeugung der Zeilen
					echo " <option>$begriff1[$i]</option>";
			}
	 	?>
      </select></td>
    </tr>
    <tr>
      <td><input type="text" name="wert2" maxlength = "20" size ="20" class = "eingabe"></td>
      <td><select name="feld2" class = "eingabe">
		  <?php
			for ($i = 0; $i < count($begriff2); $i++) { // Erzeugung der Zeilen
					echo " <option>$begriff2[$i]</option>";
			}
	 	?>
      </select></td>
    </tr>
    <tr>
      <td><input type="text" name="wert3" maxlength = "20" size ="20" class = "eingabe"></td>
      <td><select name="feld3" class = "eingabe">
		  <?php
			for ($i = 0; $i < count($begriff3); $i++) { // Erzeugung der Zeilen
					echo " <option>$begriff3[$i]</option>";
			}
	 	?>
      </select></td>
    </tr>
    <tr>
      <td colspan="2"><input type="submit" name="suchen" value="Suchen" class = "suche">&nbsp;&nbsp;<input type="reset" name="reset" value="Löschen" class = "correct"></td>
    </tr>
  </table>
</form>
</td></tr>
<!-- Ende Zeile Abfrage / Start Zeile Ergebnisse ++++++++++++++++++++++++++++++++++ // -->
</table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>

<?php } // ende else nicht gedrückt ?>