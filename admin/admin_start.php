<?php


//include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

//include ("../include/init.php");


//echo "pre>";
//print_r($_SESSION);
//die;
//sessiondauer();



?>

<!DOCTYPE HTML>
<HTML lang="de">

<style type="text/css">

    body {
        margin: 0;
        padding: 0;
        overflow: hidden;
        height: 100%;
        max-height: 100%;
        font-family:Sans-serif;
        line-height: 1.5em;
    }

    #header {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 80px;
        overflow: hidden; /* Disables scrollbars on the navigation frame. To enable scrollbars, change "hidden" to "scroll" */
        background: #BCCE98;
    }

    #logo {
        padding:10px;
        float:left;
    }

    main {
        position: fixed;
        top: 60px; /* Set this to the height of the header */
        left: 0;
        right: 0;
        bottom: 0;
        overflow: auto;
        background: #fff;
    }

    .innertube {
        margin: 15px; /* Provides padding for the content */
    }

    p {
        color: #555;
    }

    /* Menu */
    nav {
        margin:0 auto;
        padding:0;
        float:left;
    }
    nav ul {
        list-style:none;
        padding:0;
        float:left;
    }
    nav ul li {
        margin:0;
        padding:0 0 0 8px;
        float:left;
    }
    nav ul li a {
        display:block;
        margin:0;
        padding:8px 20px;
        color:darkgreen;
        text-decoration:none;
    }
    nav ul li.active a, #top-nav ul li a:hover {
        color:#d3d3f9;
    }

    /*IE6 fix*/
    * html body{
        padding: 100px 0 0 0; /* Set the first value to the height of the header */
    }

    * html main{
        height: 100%;
        width: 100%;
    }

</style>

<HEAD>

    <TITLE>Preisagentur</TITLE>
    <!-- admin/admin_start.php -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/preisagentur.css" rel="stylesheet" type="text/css" />

</head>

<!-- frames -->
<frameset  rows="70,*" border="1">
    <frame name="navi" src="admin_navi.php" marginwidth="2" marginheight="2" scrolling="auto" frameborder="0">
    <frame name="unten" src="admin_content.php" marginwidth="2" marginheight="2" scrolling="auto" frameborder="0">
    <noframes>

        <body bgcolor="#FFFFFF">Alter Browser</body></noframes>
</frameset>


</html>