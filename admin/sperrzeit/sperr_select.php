<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();

$speichern = $_POST["speichern"];

$bg_datum = "#cefdff";							// Hintergrundfarbe Datum
$bg_fehler = "#ff9966";							// Fehler-Farbe

if ($speichern) {

$ad = $_POST["ad_neu"];							// gewählter Außendienstler
$sperr_tag = ($_POST["sperr_tag_neu"]);			// Start-Tag zum Speichern der Sperrzeiten
$sperr_monat = ($_POST["sperr_monat_neu"]);		// Start-Monat zum Speichern der Sperrzeiten
$sperr_jahr = ($_POST["sperr_jahr_neu"]);		// Start-Jahr zum Speichern der Sperrzeiten
$zeitraum = ($_POST["zeitraum_neu"]);			// Anzahl der Wochen zum Speichern der Sperrzeiten

$heute = date("Ymd");							// Datum von heute

$muster_datum = "/^\d{2}$/";					// Test Datum, genau 2 Ziffern

$datum_test = datum_check($sperr_tag, $sperr_monat, $sperr_jahr, $muster_datum, $art);
	if (!empty($datum_test)) {
		$bg_datum = $bg_fehler;
		$fehler = $datum_test;
	}
	else {
		$zahl_test = 0;
		if (!empty($sperr_tag)) {
			$zahl_test = $zahl_test +1;
		}
		if (!empty($sperr_monat)) {
			$zahl_test = $zahl_test +1;
		}
		if (!empty($sperr_jahr)) {
			$zahl_test = $zahl_test +1;
		}
		if ($zahl_test == 1 OR $zahl_test == 2) {							// nur ein oder zwei Werte eingegeben - Fehlermeldung
			$fehler = "Start-Datum unvollständig!!";
		}
		elseif ($zahl_test == 3) {
			$start_date = "20$sperr_jahr$sperr_monat$sperr_tag";
			if ($start_date < $heute) {
				$fehler = "Startdatum muss in der Zukunft liegen!";
			}
		}
		elseif ($zahl_test == 0) {
			$start_date = $heute;
		
		}
	}

	if (!empty($fehler)) {													// es gab Fehler bei der Eingabe der Formularfelder
		$fehler = $fehler;
	}
	else {																	// die Seite sperr_speichern.php wird aufgerufen und die Parameter übergeben
	
		echo "<script>location.href='sperr_speichern.php?ad=$ad&start=$start_date&zeitraum=$zeitraum'</script>";		// Rücksprung zu sperr_speichern.php
	}

// Debugging -------------------------------//
/*
echo "ad: $ad<br />";						//
echo "Tag: $sperr_tag<br />";				//
echo "Monat: $sperr_monat<br />";			//
echo "Jahr: $sperr_jahr<br />";				//
echo "Start: $start_date<br />";			//
echo "Zeitraum: $zeitraum Wochen<br />";	//
*/
// -----------------------------------------//

}

?>

<!DOCTYPE html>
<html lang = "de">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sperrzeit - AD-Select</title>
	<!-- admin/sperrzeit/sperr_select.php -->
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align="center">
<table width="500" border="0" cellpadding="4" cellspacing="4">
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#fcffe6">
<tr><td valign = "top">
<!-- <form name="input" method="post" action="sperr_speichern.php" target="_self"> -->
<form name="input" method="post" action="<?php $_SERVER["PHP_SELF"] ?>" target="_self">
  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <?php
	if ($fehler) {
 		echo "<tr bgcolor=\"red\">";
        echo "<td colspan=\"2\" align = \"center\" valign = \"middle\">";
		echo "<span style=\"font-weight:bold; font-size: 9pt; color:white; line-height:250%;\">$fehler</span><br>";
		echo "</td></tr>";
	}
  ?>
  	<tr>
		<td colspan = "2">
			<table width="100%"  border="0" cellspacing="3" cellpadding="0">
  				<tr>
					<td align = "left"><span style = "font-size:9pt; line-height:150%;">Auf dieser Seite können Sie einen Außendienstler, einen Starttermin (kann nicht in der Vergangenheit liegen) und einen Zeitraum auswählen,
		                                                 für den Sie Sperrzeiten eingeben wollen.<br />
														 Wenn Sie den Starttermin nicht ausfüllen, werden die Sperrzeiten ab heute eingetragen.<br />
														 Durch Drücken von "Start!" gelangen Sie auf die nächste Seite,
														 wo Sie die konkreten Termine festlegen und abspeichern können.</span><br><br>
					</td>
				</tr>
			</table>
		</td>
	</tr>
    <tr bgcolor="#F0F8FF">
		<td align = "left"><span style = "line-height: 300%;">&nbsp;Sperrzeiten für den Außendienstler:</span></td>
		<td align = "left">
	 <?php
	  			// Abfrage der Außendienstler
	  				$sql = "SELECT user FROM user WHERE gruppen_id = '3' ORDER BY user ASC";
					$query_user = myqueryi ($db, $sql);
		
	 			 // Ausgabe der Außendienster als SELECT-Liste - alphabetisch aufsteigend geordnet
	 
					echo "<select name=\"ad_neu\" class = \"adselect\">";
					for ($j = 0; $j < mysqli_num_rows($query_user); $j++)	{	// Anzahl der Datensätze
						$zeile = mysqli_fetch_row($query_user);					// Schleife für Daten-Zeilen
						if ($zeile[0] == $ad) {									// bestehender Termin wurde angeklickt - AD bereits vergeben
							echo " <option selected>$zeile[0]</option>";
						}
						else {
							echo " <option>$zeile[0]</option>";
						}
					}
	  				echo "</select>";
	 		?>
	  </td>
    </tr>
	<tr bgcolor="<?php echo "$bg_datum"; ?>">
		<td align = "left"><span style = "line-height: 300%;">&nbsp;ab dem Datum:</span></td>
		<td align = "left">	
  <?php
			if (($sperr_tag === "00") AND ($sperr_monat === "00") AND ($sperr_jahr === "00")) {
	  			echo "<input type=\"text\" class=\"eingabe\" name=\"sperr_tag_neu\" size = \"2\" maxlength =\"2\"> . "; 
        		echo "<input type=\"text\" class=\"eingabe\" name=\"sperr_monat_neu\" size = \"2\" maxlength =\"2\"> . "; 
        		echo "<input type=\"text\" class=\"eingabe\" name=\"sperr_jahr_neu\" size = \"2\" maxlength =\"2\">";
			} 
			else {
				echo "<input type=\"text\" class=\"eingabe\" name=\"sperr_tag_neu\" size = \"2\" maxlength =\"2\" value = \"$sperr_tag\"> . "; 
        		echo "<input type=\"text\" class=\"eingabe\" name=\"sperr_monat_neu\" size = \"2\" maxlength =\"2\" value = \"$sperr_monat\"> . "; 
        		echo "<input type=\"text\" class=\"eingabe\" name=\"sperr_jahr_neu\" size = \"2\" maxlength =\"2\" value = \"$sperr_jahr\">";
			}
		?>	
	  </td>
    </tr>
	
 <?php
		if (isset($speichern)) {					// Ausgabe der Radioboxen im Fehlerfall
		
			echo"<tr bgcolor=\"#beebfc\">";
			echo"<td align = \"left\"><span style = \"line-height: 300%;\">&nbsp;im Zeitraum von::&nbsp;</span></td>";
			if ($zeitraum == 1) {
				echo"<td align = \"left\"><input type=\"radio\" name=\"zeitraum_neu\" value=\"1\" checked=\"checked\" />&nbsp;1 Woche</td>";
			}
			else {
				echo"<td align = \"left\"><input type=\"radio\" name=\"zeitraum_neu\" value=\"1\"/>&nbsp;1 Woche</td>";
			}
			echo"</tr><tr>";
			echo"<td align = \"left\"><span style = \"line-height: 300%;\">&nbsp;</span></td>";
			if ($zeitraum == 2) {
				echo"<td align = \"left\"><input type=\"radio\" name=\"zeitraum_neu\" value=\"2\" checked=\"checked\" />&nbsp;2 Wochen</td>";
			}
			else {
				echo"<td align = \"left\"><input type=\"radio\" name=\"zeitraum_neu\" value=\"2\"/>&nbsp;2 Wochen</td>";
			}
			echo"</tr><tr>";
			echo"<td align = \"left\"><span style = \"line-height: 300%;\">&nbsp;</span></td>";
			if ($zeitraum == 3) {
				echo"<td align = \"left\"><input type=\"radio\" name=\"zeitraum_neu\" value=\"3\" checked=\"checked\" />&nbsp;3 Wochen</td>";
			}
			else {
				echo"<td align = \"left\"><input type=\"radio\" name=\"zeitraum_neu\" value=\"3\"/>&nbsp;3 Wochen</td>";
			}
			echo"</tr><tr>";
			echo"<td align = \"left\"><span style = \"line-height: 300%;\">&nbsp;</span></td>";
			if ($zeitraum == 4) {
				echo"<td align = \"left\"><input type=\"radio\" name=\"zeitraum_neu\" value=\"4\" checked=\"checked\" />&nbsp;4 Wochen</td>";
			}
			else {
				echo"<td align = \"left\"><input type=\"radio\" name=\"zeitraum_neu\" value=\"4\"/>&nbsp;4 Wochen</td>";
			}
			echo"</tr>";
		}
		else {							// Ausgabe bei Start des Programms, 1 Woche ist vorausgewählt
	?>
	<tr bgcolor="#beebfc">
		<td align = "left"><span style = "line-height: 300%;">&nbsp;im Zeitraum von::&nbsp;</span></td>
		<td align = "left"><input type="radio" name="zeitraum_neu" value="1" checked="checked" />&nbsp;1 Woche</td>
	</tr>
	<tr>
		<td align = "left"><span style = "line-height: 300%;">&nbsp;</span></td>
		<td align = "left" bgcolor="#beebfc"><input type="radio" name="zeitraum_neu" value="2"/>&nbsp;2 Wochen</td>
	</tr>
	<tr>
		<td align = "left"><span style = "line-height: 300%;">&nbsp;</span></td>
		<td align = "left" bgcolor="#beebfc"><input type="radio" name="zeitraum_neu" value="3"/>&nbsp;3 Wochen</td>
	</tr>
	<tr>
		<td align = "left"><span style = "line-height: 300%;">&nbsp;</span></td>
		<td align = "left" bgcolor="#beebfc"><input type="radio" name="zeitraum_neu" value="4"/>&nbsp;4 Wochen</td>
	</tr>
	<?php } ?>
	<tr>
      <td colspan="2" align = "center"><br /><input type="submit" name="speichern" value="Start!" class = "submitt">&nbsp;&nbsp;<input type="reset" name="reset" value="Korrektur" class = "correct"></td>
    </tr>
  </table>
</form>
</td></tr>
<!-- Ende Zeile Abfrage / Start Zeile Ergebnisse ++++++++++++++++++++++++++++++++++ // -->
</table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>