<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script können Branchen bearbeitet													//
// die Branche kommt von branche_select															//
// alle betroffenen Kunden werden geupdated                                                     //
// falls nicht mehr verwendet, wird die alte Branche gelöscht                                   //
// nach dem Löschen wird eine Meldung über Erfolg oder Misserfolg der Löschaktion ausgegeben    //
// die Seite branche_select.php wird refreshed		   							                //
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();

// Funktion testet die Übereinstimmung zwischen jeweils aktuellem Wochentag und Wochentag der in der Tabelle USER abgelegten Sperrzeiten
// sollte es eine Übereinstimmung geben, wird nach einer Übereinstimmung zwischen der Termin-Zeit und der Sperzeit-Zeit gesucht
// sollte es eine Übereinstimmung geben, wird diese Zeit aus dem ARRAY Sperrzeit gelöscht - es bleiben am Ende nur die "neuen" (nicht belegten)
// Sperrzeiten übrig
// das neue ARRAY Sperrzeit_neu wird anschließend in die Tabelle TERMIN eingelesen

function sperrzeit ($variable1, $tag, $ergebnis_zahl, $sperrzeit_neu, $query, $ad ) {

			// Test, ob überhaupt eine Sperrzeit für AD eingegeben worden ist
			
			if ($variable1 > 0) {															// = $anzahl[$wochentag]
			
				// Umwandlung aktuelles Datum in SQL-Format
				
					$datum = date("Y-m-d", $tag);
					
					// keine Termine im Zeitraum gefunden - die Sperrzeiten werden unverändert übernommen
					// -----------------------------------------------------------------------------------
					
					if ($ergebnis_zahl === 0) {
					
						$position = "0";														// Anfangsinitialisierung für Suche nach Zeiten - erstes Zeichen = 0. Stelle
							
						for ($m = 0; $m < $variable1; $m++) {									// Schleife zum Durchlauf der übriggebliebenen Sperrzeiten

							$zeit = substr($sperrzeit_neu, $position, 5);						// Zeichenkette mit Länge 5, Startpos. = $position (1. Durchlauf = 0) auslesen
							
							$sql = "INSERT INTO termin (termin, zeit, sperrzeit, kd_id, aussendienst) ";
							$sql .= " VALUES ('$datum', '$zeit', '1', '1', '$ad') ";
									
							$insert = myqueryi($db, $sql);
							//$termin_id = mysqli_insert_id($db);
								
							$position = $position+5;											// Position um 5 Stellen nach rechts verschieben
						}
					}
					
					// alle in Termine gespeicherten Termine werden auf Übereinstimmung zwischen gespeichertem Termin-Datum
					// und aktuellem Datum getestet (nur, wenn überhaupt gespeicherte Termine vorhanden sind!!)
					
					else {
					
						for ($j = 0; $j < $ergebnis_zahl; $j++) {					// Schleife $ergebnis_zahl - Anzahl der im Zeitraum gefundenen Termine

							$ergebnis = mysqli_fetch_array($query, MYSQLI_ASSOC);
						
							if ($ergebnis[termin] == $datum) {

							// jetzt Test auf Übereinstimmung Sperrzeit-Zeit und Termin-Zeit
														
								$position = "0";													// Anfangsinitialisierung für Suche nach Zeiten - erstes Zeichen = 0. Stelle
								
								for ($k = 0; $k < $variable1; $k++) {								// Schleife zum Durchlauf der Sperrzeiten für Montag

									$zeit = substr($sperrzeit_neu, $position, 5);					// Zeichenkette mit Länge 5, Startpos. = $position (1. Durchlauf = 0) auslesen
									if ($ergebnis[zeit] == $zeit) {									// Uhrzeit Termin stimmt mit Sperrzeit überein
									
										$sperrzeit_neu = str_replace("$zeit", "", $sperrzeit_neu);	// die Zeichenkette $zeit wird gelöscht
									}
								$position = $position+5;											// Position um 5 Stellen nach rechts verschieben
		
								} // Ende for-Schleife Sperrzeiten aus Arrray
							} // ende test auf Übereinstimmung Datum Termin - Sperrzeit
						} // Ende for-Schleife $ergebnis_zahl
						
						// jetzt müssen die Sperrzeiten in die Tabelle "Termine" eingespeichert werden
						
							$anzahl_neu =  (strlen($sperrzeit_neu))/5;								// Anzahl der übriggebliebenen Sperrzeiten
							
							$position = "0";													// Anfangsinitialisierung für Suche nach Zeiten - erstes Zeichen = 0. Stelle
							
							for ($m = 0; $m < $anzahl_neu; $m++) {									// Schleife zum Durchlauf der übriggebliebenen Sperrzeiten

								$zeit = substr($sperrzeit_neu, $position, 5);						// Zeichenkette mit Länge 5, Startpos. = $position (1. Durchlauf = 0) auslesen
								
								$sql = "INSERT INTO termin (termin, zeit, sperrzeit, kd_id, aussendienst) ";
								$sql .= " VALUES ('$datum', '$zeit', '1', '1', '$ad') ";
									
								$insert = myqueryi($db, $sql);
								//$termin_id = mysqli_insert_id($db);
								
								$position = $position+5;											// Position um 5 Stellen nach rechts verschieben

							}
						
						// Rücksetzen des SQL-Ergebnisstrings auf Null für Durchlauf nächster Tag
						
							mysqli_data_seek($query, '0');
						
					} // Ende else bereits Sperrzeiten gespeichert
			} // Ende if-Test überhaupt Sperrzeiten vorhanden
} // Ende Funktionsdefinition

// Variable für Script
// ----------------------------------------------------------------------------------------------------------------------------------

$ad = $_POST["ad"];						// kommt als POST von sperr_speichern.php
$zeitraum = $_POST["zeitraum"];			// kommt als POST von sperr_speichern.php
$start_date = $_POST["start"];			// kommt als POST von sperr_speichern.php

// Sperrzeiten aus Datenbank(user) für den gewählten AD auslesen
//-----------------------------------------------------------------------------------------------------------------------------------

$sql = "SELECT sperrzeit_mo, sperrzeit_di, sperrzeit_mi, sperrzeit_do, sperrzeit_fr, sperrzeit_sa FROM user where user='$ad'";
$output = myqueryi($db, $sql);
$sperrzeit = mysqli_fetch_array($output);

// Ermittlung der Anzahl der Sperrzeiten je Wochentag, eine Zeit besteht aus 5 Zeichen (09:00)
// ----------------------------------------------------------------------------------------------------------------------------------

$anzahl = array();
$anzahl[0] =  (strlen($sperrzeit[0]))/5;				// Länge des 0-Elements des Arrays, dividiert durch 5 = Anzahl Sperrzeiten Montag
$anzahl[1] =  (strlen($sperrzeit[1]))/5;				// Länge des 1-Elements des Arrays, dividiert durch 5 = Anzahl Sperrzeiten Dienstag
$anzahl[2] =  (strlen($sperrzeit[2]))/5;				// Länge des 2-Elements des Arrays, dividiert durch 5 = Anzahl Sperrzeiten Mittwoch
$anzahl[3] =  (strlen($sperrzeit[3]))/5;				// Länge des 3-Elements des Arrays, dividiert durch 5 = Anzahl Sperrzeiten Donnerstag
$anzahl[4] =  (strlen($sperrzeit[4]))/5;				// Länge des 4-Elements des Arrays, dividiert durch 5 = Anzahl Sperrzeiten Freitag
$anzahl[5] =  (strlen($sperrzeit[5]))/5;				// Länge des 5-Elements des Arrays, dividiert durch 5 = Anzahl Sperrzeiten Sonnabend
$anzahl[6] =  "0";										// Länge des 6-Elements des Arrays, Sonntag

// Ermittlung des zu speichernden Zeitraumes
//-----------------------------------------------------------------------------------------------------------------------------------------

$zeitraum = $zeitraum * 7;								// Tage = Anzahl der Wochen * 7

if (empty($start_date)) {											// kein Startdatum im Script sperr_select.php eingegeben
	$startdatum = date("Ymd");										// heute
	$endedatum = date("Ymd", strtotime("+ $zeitraum days"));		// heute + Anzahl der Tage (durch Zeitraum festgelegt)
	$timestamp = mktime();											// heute = Startdatum
}
else {
	$startdatum = $start_date;
	$endedatum = date("$start_date", strtotime("+ $zeitraum days"));
	$timestamp = strtotime($start_date);
	$endedatum = $timestamp + (86400 * $zeitraum);
	$endedatum = date("Ymd", $endedatum);
}

// die bereits gespeicherten Termine werden für den AD und Zeitraum aus der DB (Tabelle Termine) ausgelesen
//-----------------------------------------------------------------------------------------------------------------------------------------

$sql = "SELECT termin, zeit FROM termin  ";
$sql .= " WHERE termin BETWEEN $startdatum AND $endedatum AND (wiedervorlage_date IS NULL OR wiedervorlage_date='0000-00-00') AND aussendienst = '$ad' ";
$sql .= " ORDER BY termin ASC"; 
$query = myqueryi($db, $sql);
$ergebnis_zahl = mysqli_num_rows($query);						// wieviele Termine sind im Zeitraum schon gespeichert?

// Schleife Tage = Anzahl der anzuzeigenden Wochen *7 ($zeitraum)
//-------------------------------------------------------------------------------------------------------------------------------------------

for ($i = 0; $i < $zeitraum; $i++) {

	$tag = $timestamp + ($i * 86400);									// Startdatum + 86400 Sekunden (= 1 Tag) * nummer des Durchlaufs (Zeitraum)
	$tag_aktuell = getdate($timestamp + ($i * 86400));					// Ermittlung des Wochentags (wday) - Sonntag = 0, Montag = 1 ... 
																		// und Datum des aktuellen Duchlaufs 

	// Nummer des aktuellen Wochentages - 1: entspricht der Nummerierung des Arrays $ergebnis (= Sperrzeiten - Montag =0)
	// Sonntag wäre $wochentag = (-1) - aber Sonntag werden keine Termine vergeben
	
	$wochentag = $tag_aktuell[wday]-1;
	$datum = date("Y-m-d", $tag);

	
// Debug-Variablen
// ------------------------------------------------------------
/*
echo "Startdatum: $startdatum<br />";
echo "Endedatum: $endedatum<br />";
echo "Datum: $datum<br />";
echo "Wochentag: $tag_aktuell[weekday]<br />";
echo "Wochentag-numerisch: $wochentag<br />";
echo "AD: $user<br />";
echo "zeitraum: $zeitraum<br />";
var_dump($sperrzeit);
echo "<br />";
echo "Termine: $ergebnis_zahl<br />";
echo "<br />";
*/
//---------------------------------------------------------------

	// der aktuelle Wochentag (aktuell = Schleife) wird bearbeitet
	// ---------------------------------------------------------------------------------------------------------------------------------------
	
	switch($tag_aktuell[wday]) {
	
		// Sonntag, es werden keine Termine vergeben
		// ----------------------------------------------------------------------------------------------------------------------------------
		case 0:																
		break;
	
		// Montag
		// -----------------------------------------------------------------------------------------------------------------------------------
		case 1:

			$sperrzeit_neu = $sperrzeit[$wochentag];					// neue Variable, damit "Standard"-Sperrzeit für nächste Woche erhalten bleibt
			$variable1 = $anzahl[$wochentag];
		
			sperrzeit ($variable1, $tag, $ergebnis_zahl, $sperrzeit_neu, $query, $ad );
		
		break;
	
		// Dienstag
		// -----------------------------------------------------------------------------------------------------------------------------------
		case 2:

			$sperrzeit_neu = $sperrzeit[$wochentag];					// neue Variable, damit "Standard"-Sperrzeit für nächste Woche erhalten bleibt
			$variable1 = $anzahl[$wochentag];
		
			sperrzeit ($variable1, $tag, $ergebnis_zahl, $sperrzeit_neu, $query, $ad );
		
		break;
	
		// Mittwoch
		// -----------------------------------------------------------------------------------------------------------------------------------
		case 3:

			$sperrzeit_neu = $sperrzeit[$wochentag];					// neue Variable, damit "Standard"-Sperrzeit für nächste Woche erhalten bleibt
			$variable1 = $anzahl[$wochentag];
		
			sperrzeit ($variable1, $tag, $ergebnis_zahl, $sperrzeit_neu, $query, $ad );
		
		break;
	
		// Donnerstag
		// -----------------------------------------------------------------------------------------------------------------------------------
		case 4:

			$sperrzeit_neu = $sperrzeit[$wochentag];					// neue Variable, damit "Standard"-Sperrzeit für nächste Woche erhalten bleibt
			$variable1 = $anzahl[$wochentag];
		
			sperrzeit ($variable1, $tag, $ergebnis_zahl, $sperrzeit_neu, $query, $ad );
		
		break;
	
		// Freitag
		// -----------------------------------------------------------------------------------------------------------------------------------
		case 5:

			$sperrzeit_neu = $sperrzeit[$wochentag];					// neue Variable, damit "Standard"-Sperrzeit für nächste Woche erhalten bleibt
			$variable1 = $anzahl[$wochentag];
		
			sperrzeit ($variable1, $tag, $ergebnis_zahl, $sperrzeit_neu, $query, $ad );
		
		break;
	
		// Sonnabend
		// -----------------------------------------------------------------------------------------------------------------------------------
	
		case 6:
		
			$sperrzeit_neu = $sperrzeit[$wochentag];					// neue Variable, damit "Standard"-Sperrzeit für nächste Woche erhalten bleibt
			$variable1 = $anzahl[$wochentag];
		
			sperrzeit ($variable1, $tag, $ergebnis_zahl, $sperrzeit_neu, $query, $ad );
			
		break;
	}	// Ende der Schleife aktueller Wochentag (aktuell = Schleife)
	

} // Ende Schleife Tage


echo "<script>location.href='sperr_select.php?'</script>";		// Rücksprung zu sperr_speichern.php
?>

<!-- der HTML-Teil dient nur zur Erzeugung der Hintergrundfarbe -->

<!DOCTYPE html>
<html lang = "de">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sperrzeit - Termine anlegen</title>
	<!-- admin/sperrzeit/sperr_termin.php -->
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
</body>
</html>

