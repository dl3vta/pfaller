<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");

sessiondauer();

unset($gespeichert);								// Variable zurücksetzen

?>

<!-- der HTML-Teil dient nur zur Erzeugung der Hintergrundfarbe -->

<!DOCTYPE html>
<html lang = "de">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sperrzeit - Termine speichern</title>
	<!-- admin/sperrzeit/sperr_ad.php -->
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
</body>
</html>

<?php

$wochentag = quote_smart($_GET['wochentag']);
$s_zeit = quote_smart($_GET['s_zeit']);

$ad = $_GET['ad'];						// die GET-Variable AD kommt von sperr_speichern.php und muss dorthin wieder zurück
$zeitraum = $_GET['zeitraum'];			// die GET-Variable Zeitraum kommt von sperr_speichern.php und muss dorthin wieder zurück
$start = $_GET['start'];				// die GET-Variable Start kommt von sperr_speichern.php und muss dorthin wieder zurück

// Sperrzeiten aus Datenbank(user) für den gewählten AD und Tag auslesen

switch($wochentag) {
	case Montag:
		$sql = "SELECT sperrzeit_mo FROM user where user='$ad'";
	break;
	case Dienstag:
		$sql = "SELECT sperrzeit_di FROM user where user='$ad'";
	break;
	case Mittwoch:
		$sql = "SELECT sperrzeit_mi FROM user where user='$ad'";
	break;
	case Donnerstag:
		$sql = "SELECT sperrzeit_do FROM user where user='$ad'";
	break;
	case Freitag:
		$sql = "SELECT sperrzeit_fr FROM user where user='$ad'";
	break;
	case Sonnabend:
		$sql = "SELECT sperrzeit_sa FROM user where user='$ad'";
	break;
}
$output = myqueryi($db, $sql);
//$output = myquery($sql);
$zeit = mysqli_fetch_array($output);

// eine Zeit besteht aus 5 Zeichen - Ermittlung der Anzahl der Sperrzeiten
$anzahl = (strlen($zeit[0]))/5;				// Länge des 0-Elements des Arrays, dividiert durch 5

// suche nach Zeit im Array
// vorhanden - dann löschen
// nicht vorhanden - löschen

unset($start_alt);										// Startwert vorhandener Termin löschen (falls einer vorhanden ist)
$position = "0";										// Anfangsinitialisierung für Suche nach Zeiten - ersten Zeichen = 0. Stelle
for ($i = 0; $i < $anzahl; $i++) {

	$termin = substr($zeit[0], $position, 5);			// Zeichenkette mit Länge 5, Startpsos. = $position (1. Durchlauf = 0) auslesen
	if ($termin == $s_zeit) {							// Termin schon vorhanden
		$start_alt = $position;							// vorhandener Termin beginnt an Stelle $start_alt im Array $zeit
	}
	$position = $position+5;							// Position um 5 Stellen nach rechts verschieben
		
}

if (isset($start_alt)) {								// Termin ist bereits vorhanden gewesen - muss gelöscht werden
	$sperrzeit = str_replace("$s_zeit", "", $zeit[0]);	// die Zeichenkette $s_zeit wird gelöscht
} // ende if termin schon vorhanden
else {													// Termin ist neu - Array updaten und Datenbank aktualisieren
	$sperrzeit = $zeit[0] . $s_zeit;					// neuer Termin wird an bereits existierende angehangen
}  // ende else termin neu

// Update der Datenbank

switch($wochentag) {
	case Montag:
		$sql = "UPDATE user SET sperrzeit_mo = '$sperrzeit' WHERE user='$ad'";
	break;
	case Dienstag:
		$sql = "UPDATE user SET sperrzeit_di = '$sperrzeit' WHERE user='$ad'";
	break;
	case Mittwoch:
		$sql = "UPDATE user SET sperrzeit_mi = '$sperrzeit' WHERE user='$ad'";
	break;
	case Donnerstag:
		$sql = "UPDATE user SET sperrzeit_do = '$sperrzeit' WHERE user='$ad'";
	break;
	case Freitag:
		$sql = "UPDATE user SET sperrzeit_fr = '$sperrzeit' WHERE user='$ad'";
	break;
	case Sonnabend:
		$sql = "UPDATE user SET sperrzeit_sa = '$sperrzeit' WHERE user='$ad'";
	break;
}
	
	$output = myqueryi($db, $sql);
	
	$gespeichert = 1;													// Kennzeichen für erfolgreiche Datenbank-Aktion - geht nach sperr_speichernm.php zurück

echo "<script>location.href='sperr_speichern.php?s_ad=$ad & s_zeitraum=$zeitraum & s_start=$start & gespeichert=$gespeichert'</script>";		// Rücksprung zu sperr_speichern.php
																													// Außendienstler, Zeitraum und Startdatum werden zurückgegeben
?>

