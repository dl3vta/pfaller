<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();

if ($_GET["gespeichert"] == 1) {
	$ad = $_GET["s_ad"];
	$zeitraum = $_GET["s_zeitraum"];
	$start_date = $_GET["s_start"];

}
else {
	$ad = $_GET["ad"];
	$zeitraum = $_GET["zeitraum"];
	$start_date = $_GET["start"];

}

// Sperrzeiten aus Datenbank(user) für den gewählten AD auslesen
// mo = Montag, $di = Dienstag ...
$sql = "SELECT sperrzeit_mo, sperrzeit_di, sperrzeit_mi, sperrzeit_do, sperrzeit_fr, sperrzeit_sa FROM user where user='$ad'";
$output = myqueryi($db, $sql);
$sperrzeit = mysqli_fetch_array($output);

// Ermittlung der Anzahl der Sperrzeiten je Wochentag
$anzahl = array();
$anzahl[0] =  (strlen($sperrzeit[0]))/5;				// Länge des 0-Elements des Arrays, dividiert durch 5 = Montag
$anzahl[1] =  (strlen($sperrzeit[1]))/5;				// Länge des 1-Elements des Arrays, dividiert durch 5 = Dienstag
$anzahl[2] =  (strlen($sperrzeit[2]))/5;				// Länge des 2-Elements des Arrays, dividiert durch 5 = Mittwoch
$anzahl[3] =  (strlen($sperrzeit[3]))/5;				// Länge des 3-Elements des Arrays, dividiert durch 5 = Donnerstag
$anzahl[4] =  (strlen($sperrzeit[4]))/5;				// Länge des 4-Elements des Arrays, dividiert durch 5 = Freitag
$anzahl[5] =  (strlen($sperrzeit[5]))/5;				// Länge des 4-Elements des Arrays, dividiert durch 5 = Sonnabend

// Variablen für die Darstellung der Tabelle
$zahl = "7";			// wie viele Tage sollen angezeigt werden - Zählung beginnt bei Null
$span = $zahl +1 ;		// + eine Zelle für die Uhrzeit
$t_zeit = array("09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", 
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");
$t_tag = array("Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Sonnabend", "Sonntag");

$tab_width = "900";								// Tabellenbreite
$td_width = floor(($tab_width - "50")/7);		// Zellenbreite für Wochentage, abzüglich 50 px für Uhrzeit, ganzzahliger Teil der Division

/*
// Debugging -------------------------------//
echo "ad: $ad<br />";						//
echo "Start: $start_date<br />";			//
echo "Zeitraum: $zeitraum Wochen<br />";	//
// -----------------------------------------//
*/

?>

<!DOCTYPE html>
<html lang = "de">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sperrzeit - Sperrzeiten anlegen</title>
	<!-- admin/sperrzeit/sperr_speichern.php -->
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">

<table width="<?php echo "$tab_width";?>" border="0" cellpadding="4" cellspacing="4">
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="1" cellpadding="3" cellspacing="0" bgcolor="#fcffe6">
	<tr>
		<td colspan="<?php echo "$span";?>" align = "left" valign = "top"><span style = " font-size: 9pt; line-height:130%;">
			Auf dieser Seite können Sie die Sperrzeiten für den auf der vorherigen Seite ausgewählten Außendienstler eingeben und abspeichern.
			<br><br>
			Durch Klicken auf die F/S-Zellen können Sie einen "Standard" für den Außendienstler festlegen <strong>(NICHT den Speichern-Button benutzen!!!!)</strong><br />
			Diese Zeiten werden speziell für den Außendienstler gespeichert und erscheinen immer auf dieser Seite, wenn diese aufgerufen wird.<br />
			Selbstverständlich können Sie auf diese Art und Weise den "Standard" jederzeit an evtl. Änderungen anpassen.
			<br><br>
			Wenn Sie den "Standard" gespeichert haben (beim 2. Mal ist er ja bereits vorhanden) können Sie ihn durch Drücken des SPEICHERN-Buttons in Termine umwandeln -
 			abhängig vom gewählten Zeitraum bis zu 4 Wochen im Voraus.<br /><br />
			<strong>Eventuell im Zeitraum bereits angelegte Termine werden nicht überschrieben - bleiben also erhalten.</strong><br /><br />
			Das bedeutet:
			<ol type="1">
				<li>Sperrzeiten, die vom Standard abweichen, müssen manuell (z.B. im Aquise-Fenster) eingegeben oder gelöscht werden</li>
				<li>Manuell abgespeicherte Sperrzeiten können nicht durch erneutes Abspeichern eines veränderten "Standard"-Schemas gelöscht werden - müssen ebenfalls manuell gelöscht werden</li>
			</ol></span>
		</td>
		<td>
			<table>
			<form name="sperrzeit" method="post" action="sperr_termin.php">
			
<?php
echo "<tr bgcolor=\"#9b0013\">";
	echo "<td colspan=\"8\" align = \"center\">
	<span style = \"color: #ffffff; font-size: 9pt; font-weight: bold; line-height: 200%;\">Sperrzeiten für  <span style = \"font-size: 11pt;\">\"$ad\"</span>  anlegen:</span></td>";
echo "</tr>";
echo "<tr>";
	echo "<td>";
		echo "<tr>";
			echo "<td class=\"spalten\">Zeit</td>";
				for ($i= "0"; $i<$zahl; $i++) { 				// Anzahl der Tage (7)
					if ($t_tag[$i] == "Sonntag") {
						echo "<td class=\"sonntag\" width=\"$td_width\">$t_tag[$i]</td>";
					}
					else {
						echo "<td class=\"spalten\" width=\"$td_width\" >$t_tag[$i]</td>";
					}
				}
		echo "</tr>";
		
$zahl_t_zeit = count($t_zeit);

$treffer = "0";																// Übereinstimmung Datenbank-Tabellenzelle

for ($j = 0; $j < $zahl_t_zeit; $j++) { 									// Zeiten: Erzeugung der Zeilen
	echo "<tr>";
	echo "<td class=\"zeiten\">$t_zeit[$j]</td>";
	
	for ($k = 0; $k < $zahl; $k++) { 										// Tage: Erzeugung der Spalten (7 Taqe)
	
		if ($t_tag[$k] === "Sonntag") {
			echo "<td class=\"sonntag\" width=\"$td_width\">&nbsp;</td>";
		}
		//elseif ($t_tag[$k] == "Sonnabend") {
			//echo "<td class=\"samstag\" width=\"$td_width\">&nbsp;</td>";
		//}
		else {																// normaler Wochentag
		
			// Zeiten: Montag = $sperrzeit[0], Dienstag = $sperrzeit[1], Mittwoch = $sperrzeit[2], Donnerstag = $sperrzeit[3], Freitag = $sperrzeit[4]
			// Tage:   Montag = $k=0, Dienstag = $k=1, Mittwoch = $k=2, Donnerstag = $k=3, Freitag = $k=4
			
			// das Array $sperrzeit muss nach $t_zeit[$j] durchsucht werden
			
			if ($anzahl[$k] > 0) {   										// es sind bereits Sperrzeiten eingetragen
			
				// im zum Wochentag gehörigen Array-Element $sperrzeit[$k] nach Übereinstimmeung mit der Zeile Zeit suchen
				
				$position = "0";											// Anfangsinitialisierung für Suche nach Zeiten - ersten Zeichen = 0. Stelle
				
				for ($m = 0; $m < $anzahl[$k]; $m++) {				
					$termin = substr($sperrzeit[$k], $position, 5);
					if ($termin == $t_zeit[$j]) {							// Termin schon vorhanden
					
						// Übereinstimmung festgestellt, Zelle mit Sperrzeit schreiben, aber keine Freizelle für die anderen Zeiten im Array-Element $sperrzeit[$k] !!!!
						
						$treffer = "1";									
						echo "<td class =\"sperrzeit\" width=\"$td_width\"><a href=\"sperr_ad.php?s_zeit=$t_zeit[$j] & wochentag=$t_tag[$k] & ad=$ad & zeitraum=$zeitraum & start=$start_date \">S</a></td>";
					}
					$position = $position+5;								// Position im String um 5 Stellen nach rechts verschieben
				}
				
				// kein Treffer beim kompletten Durchlauf des Array-Elements $sperrzeit[$k] - Freizelle schreiben
				
				if ($treffer == 0) {								
					echo "<td class =\"zeiten\" width=\"$td_width\"><a href=\"sperr_ad.php?s_zeit=$t_zeit[$j] & wochentag=$t_tag[$k] & ad=$ad & zeitraum=$zeitraum & start=$start_date \">F</a></td>";
				}
				else {
					$treffer = "0";											// war Treffer, Variable für nächsten Durchlauf zurücksetzen
				}
			}  																// ende if bereits Sperrzeiten eingetragen
			else {															// keine Sperrzeiten vorhanden
				echo "<td class =\"zeiten\" width=\"$td_width\"><a href=\"sperr_ad.php?s_zeit=$t_zeit[$j] & wochentag=$t_tag[$k] & ad=$ad & zeitraum=$zeitraum & start=$start_date \">F</a></td>";
			}  																// ende else keine Sperrzeiten vorhanden
		} 																	// ende else normaler Wochentag
	} 																		// ende for Erzeugung Spalten
	echo "</tr>";
} 																			// ende for zeilen
echo "<tr>";
echo "<td align = \"center\" colspan=\"$span\" height = \"30\">";

// 3 Hidden-Felder zur Übergabe von Außendienstler, Startdatum und Zeitraum an sperr_termin.php
echo "<input type=\"Hidden\" name=\"ad\" value=\"$ad\">";
echo "<input type=\"Hidden\" name=\"zeitraum\" value=\"$zeitraum\">";
echo "<input type=\"Hidden\" name=\"start\" value=\"$start_date\">";
echo "<input type=\"submit\" name=\"suchen\" value=\"Sperrzeiten speichern\" class = \"submitt\"></td>";
echo "</tr>";
?>
</form>
			
			
			
			
			</table>
		</td>
	</tr>
	
	


</table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>