<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();

$gruppen_id = $_GET["id"];
$gruppen_id = quote_smart($gruppen_id);

$sql = "SELECT gruppen FROM gruppen WHERE gruppen_id = '$gruppen_id'";
$ergebnis = myqueryi($db, $sql);
$daten = mysqli_fetch_array($ergebnis, MYSQLI_ASSOC);
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Preisagentur: Gruppen-Update</title>
    <!-- admin/gruppen_update.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<table width="600" border="0" cellpadding="8" cellspacing="8">
<tr>
<td><br><br><h2 class="Stil1">Gruppen-Update</h2><td>
</tr>
<tr><td>
<table width="80%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">
	
<!-- Start Formular für Gruppen-Update ++++++++++++++++++++++++++++++++++ // -->

<form name="gruppen_update" method="post" action="gruppen_registration.php">
<input type="hidden" name="gruppen_id" value="<?php echo "$gruppen_id"; ?>">
<table id="formular">
<tr><td colspan = "2"><strong>Achtung! Hiermit ändern Sie die aktuelle Gruppe!!</strong></td></tr>
  <tr>
    <td>Gruppe:</td>
	<td><input type="text" name="gruppen" size = "25" value="<?php echo "$daten[gruppen]"; ?>"></td>
  </tr>
  <tr>   
    <td>&nbsp;</td>
	<td><input type="submit" name="update" value="Update!" class = "submitt">&nbsp;&nbsp;<input type="submit" name="reset" value="Abbrechen!" class = "correct"></td>
  </tr>
</table>    
</form>
<!-- Ende Formular für Gruppen-Update ++++++++++++++++++++++++++++++++++ // -->
</td></tr></table>
</td></tr></table>
</td></tr></table>
</body>
</html>