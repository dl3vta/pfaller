<?php
//include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

//sessiondauer();

//echo "pre>";
//print_r($_SESSION);
//die;

$benutzer_vorname = $_SESSION['benutzer_vorname'];
$benutzer_name = $_SESSION['benutzer_name'];
$benutzer_gruppen = $_SESSION['benutzer_gruppen'];
if ($benutzer_vorname === "NULL") {
	$benutzer_vorname = "";
}
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html>
<head>
<title>Navi</title>
	<!-- admin/admin_navi.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style type="text/css">
<!--
body { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 0.5em; background-color: #006699; margin: 3px;  }
A.navi {
 	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9pt;
	line-height: 150%;
 	background-color: #F0F8FF;
	text-align: left;
	font-weight : bold;
	color : #006699;
	text-decoration : none;
	padding: 5px 5px 5px 5px;
	display: block;
 }
 A.navi:hover {
 	text-decoration: underline;
	color: #000000;
 }
-->
</style>
</head>
<body>
<div align="left">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td align = "left" height="30px"><span style="font-size: 1.8em; color: white;">Sie sind eingeloggt als:
				<?php echo "$benutzer_vorname&nbsp;$benutzer_name - $benutzer_gruppen";?></span></td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="1">
		<tr>
			<td width = "70" class="tdnavi"><a href="admin_start.php" target="_parent" class="navi">Startseite</a></td>
			<td width = "70" class="tdnavi"><a href="../allgemein/kunden_suche.php" target="_blank" class="navi">Suche</a></td>
			<td width = "90" class="tdnavi"><a href="../allgemein/plz.php" target="_blank" class="navi">PLZ-Kd.</a></td>
			<td width = "70" class="tdnavi"><a href="../allgemein/branche/branche_frame.php" target="_blank" class="navi">Branchen</a></td>
			<td width = "70" class="tdnavi"><a href="../allgemein/tarife/tarife_frame.php" target="_blank" class="navi">Gesellschaften/Tarife</a></td>
			<td width = "70" class="tdnavi"><a href="../allgemein/orte/orte_frame.php" target="_blank" class="navi">Ortsteile</a></td>
			<td width = "70" class="tdnavi"><a href="sperrzeit/sperr_select.php" target="unten" class="navi">Sperrzeiten</a></td>
			<td width = "70" class="tdnavi"><a href="../allgemein/termin_uebersicht.php" target="_blank" class="navi">Termine</a></td>
			<td  width = "120" class="tdnavi"><div align = "center"><a href="ad_termin_select.php" target="_blank" class="navi">R&uumlcklauf->AD</a></div></td>
			<td width = "115" class="tdnavi"><div align = "center"><a href="aq_termin_select.php" target="_blank" class="navi">R&uumlcklauf->Tel.</a></div></td>
			<td width = "70" class="tdnavi"><a href="user_registration.php" target="unten" class="navi">Benutzer</a></td>
			<td  width = "70" class="tdnavi"><a href="gruppen_registration.php" target="unten" class="navi">Gruppen</a></td>
			<td width = "70" class="tdnavi"><a href="logins.php" target="unten" class="navi">Logins</a></td>
			<td width = "70" class="tdnavi"><a href="../hilfe/hilfe_admin.htm" target="_blank" class="navi">Hilfe</a></td>
			<td width = "70" class="tdnavi"><a href="../logout.php" target="_parent" class="navi"><span style="color:red">Abmelden</span></a></td>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
</body>
</html>  