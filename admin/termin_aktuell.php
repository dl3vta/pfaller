<?php

include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();

$telefonist = $_SESSION['benutzer_kurz'];														//Session wird erstellt
$benutzer_name = $_SESSION['benutzer_name'];
$benutzer_vorname = $_SESSION['benutzer_vorname'];
$benutzer_gruppen = $_SESSION['benutzer_gruppen'];
//$aussendienst = $_SESSION['aussendienst'];

$aussendienst = "ZZ";

$zahl = "13";			// wie viele Tage sollen angezeigt werden - Zählung beginnt bei Null
$span = $zahl +3 ;		// eine Zelle für die Uhrzeit
$t_zeit = array("09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", 
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");
	
$zahl_t_zeit = count($t_zeit);										// Anzahl der Elemente im Zeit-Array
				
$dat1 = date("Ymd", strtotime("-1 days"));							// gestern
$dat2 = date("Ymd", strtotime("+13 days"));							// 14 Tage (von gestern ab)

$sql = "SELECT termin_id, termin, zeit, sperrzeit, plz, abschluss, termin.nichtkunde, storno, kalt FROM termin, kunden, poo, plz ";
$sql .= "WHERE termin.kd_id = kunden.kunden_id AND kunden.poo_id=poo.poo_id AND poo.plz_id=plz.plz_id AND termin.alt = '0' ";
$sql .= "AND termin BETWEEN $dat1 AND $dat2 AND termin.wiedervorlage = '0' AND aussendienst = '$aussendienst' ";
//$sql .= "AND termin BETWEEN $dat1 AND $dat2 AND (wiedervorlage_date IS NULL OR wiedervorlage_date='0000-00-00') AND aussendienst = '$aussendienst' ";
$ergebnis = myqueryi($db, $sql);

$datensatz = mysqli_num_rows($ergebnis);								// Anzahl der aus der Datenbank ausgelesenen Termine
echo "anzahl: $datensatz<br />"

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>T-aktuell</title>
	<!-- admin/termin_aktuell.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">

<table border="1" width="690" class="zeiten" cellspacing="0" cellpadding="0">
<tr><td colspan="<?php echo "$span";?>">
<form name="user_neu" method="post" action="<?php $PHP_SELF ?>">
<table>
<tr><td><span style="font-size: 10pt;font-weight: bold; color: red;">Termine für: <?php echo "$aussendienst"; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><input type="submit" name="speichern" value="REFRESH" class = "submitt"></td></tr>
</table>
</td></tr>

<?php
 
  echo "<tr>";
    echo "<td>";
  echo "<tr>";

for ($i= -1; $i < $zahl; $i++) { 				// Termine von gestern bis in 13 Tagen
	$datum = datum($i);
	$heute = date("d")+0 . "-" . date("m");		// damit bei einstelligen Daten nur eine Stelle erscheint
	$aktuell = date("d")+$i . "-" . date("m");
	if ($heute === $aktuell) {					// Spalte HEUTE - rot markiert
		echo "<td class=\"spalten\"><span style=\"font-weight:bold; color:red;\">$datum</span></td>";
	}
	else {
		echo "<td class=\"spalten\">$datum</td>";
	}
}

echo "</tr>";
$treffer = "0";
for ($j = 0; $j < $zahl_t_zeit; $j++) { 			// Schleife für Zeiten = Zeilen

	// ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// es muss ermittelt werden, an welchem Tag zu der bestimmten Zeit $t_zeit[$j] mehrere Termine vorliegen
	// dazu werden alle aus der Datenbank ausgelesenen Termine durchlaufen und das Array "Teiler" geschaffen
	// das Array hat folgende Struktur: $teiler = ("0", "0", "2", "0"; ...) und wird am Anfang komplett mit 0 gefüllt
	// die einzelnen Elemente stehen für die Spalte, d.h. $datum($m), für $m=-1 (gestern) ...$m < $zahl (Anzahl der anzuzeigenden Tage)
	// für jeden gefundenen Datensatz wird das entsprechende Element um 1 erhöht
	// dies wird einmal pro Zeile gemacht, ansonsten mösste die Prozedur in jeder Zelle wiederholt werden
	
	for ($n = "0"; $n < ($zahl+1); $n++) {																// Initialisierung des Arrays "Teiler" für die Teilung der Zellen für Mehrfachtermine
		$teiler[$n] = "0";
	}
	
	for ($m = -1; $m < $zahl; $m++) { 																	// Schleife für Tage (gestern + 14 Tage)
		$timestamp = mktime(0,0,0,date("m"), date("d")+$m);
		$datum = (strftime("%Y", $timestamp)) . "-" .(strftime("%m", $timestamp)) . "-" . (strftime("%d", $timestamp));
		
		if ($datensatz > 0) {																			// überhaupt Termine da?
		
			for ($p = 0; $p < $datensatz; $p++) {	// 

				$termine = mysqli_fetch_row($ergebnis);
				
				if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0')) {// Vergleich Datum mit Termin und Zeit, Sperrzeiten ausgeschlossen
				
					$teiler[($m+1)] = $teiler[$m+1] + 1;												// da $m von -1 läuft, muss Zähler im Array um 1 erhöht werden (läuft von 0)
				}
			}
			mysqli_data_seek($ergebnis, '0');													// Rücksetzen der Ergebnisliste für nächste Zeile (Zeit)
		}
	}																									// Ende Schleife Tage
	
	// Ende Ermittlung Termin-Mehrfachbelegung
	// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

	// Auswertung der Daten und Darstellung in Kalenderform beginnt
	
	echo "<tr>";
	
	for ($k = -1; $k < $zahl; $k++) { 									// Schleife für Tage (von gestern bis in 13 Tagen)
	
		// Hier muss die Abfrage des Arrays rein
		// Zeiten: j = zeile 0 = 9.00 Uhr, (zahl_t_zeit-1) = 20.00
		// Datum: k = Spalte k=0: heute, k=$zahl: in 14 Tagen
		// das Array heißt $termine, Datum = $termine[1], zeit = $termine[2]
		// Lösung: Suche im Array nach der Zeit $t_zeit[$j]
		// und dann nach Datum heute + $k
	
		$termin = mktime(0,0,0,date("m"), date("d")+$k);
		//setlocale (LC_TIME, 'de');								// deutsche Benutzerumgebung eingestellt
		setlocale (LC_TIME, 'de_DE');// deutsche Benutzerumgebung eingestellt
		date_default_timezone_set("Europe/Berlin");
		$datum = (strftime("%Y", $termin)) . "-" .(strftime("%m", $termin)) . "-" . (strftime("%d", $termin));
		$wochentag = (strftime("%a", $termin));

		if ($datensatz > 0) {													// überhaupt Termine da?
				
			for ($l = 0; $l < $datensatz; $l++) {								// zeilenweises Auslesen der Datenbank-Query

				$termine = mysqli_fetch_row($ergebnis);
				
				if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j])) {	// Datum und Zeit stimmen überein
				
					if ($termine[3] == 1) {										// Termin ist Sperrzeit
						echo "<td class =\"sperrzeit\"><a href=\"termin_sperrzeit.php?t_id=$termine[0]\">S</a></td>";
					}															// Ende Sperrzeit
										
					elseif ($teiler[($k+1)] > 1) {									// mehrere Termine auf einer Zeit
						mysqli_data_seek($ergebnis, '0');
					
						echo "<td class =\"doppelt\">";
						echo "<table width = \"100%\" cellspacing = \"0\" cellpadding = \"0\">";
		
							for ($p = 0; $p < $datensatz; $p++) {					// alle Datensätze überprüfen

								$termine = mysqli_fetch_row($ergebnis);
				
								if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[7])) AND ($termine[8] == '0')) {		// Abschluss
				
									//if ($termine[5] == 1) {									// Termin ist Abschluss
									echo "<tr>";
									echo "<td class =\"abschluss\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '0') AND ($termine[6] == '0') AND (empty($termine[7])) AND ($termine[8] == '0')) {	// offener Termin
									echo "<tr>";
									echo "<td class =\"termin\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[6] == '1')) {		// Nichtkunde
									echo "<tr>";
									echo "<td class =\"nichtkunde\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND (!empty($termine[7]))) {		// storno
									echo "<tr>";
									echo "<td class =\"storno\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
								elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[8] == '1')) {		// kalt
									echo "<tr>";
									echo "<td class =\"kalt\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
									echo "<tr>";
								}
							}
						echo "</table>";
						echo "</td>";
					} // ende mehrere Termine auf einer Zeit
						
					else { // nur ein Termin
					
						if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[7])) AND ($termine[8] == '0')) {		// Abschluss
							echo "<td class =\"abschluss\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '0') AND ($termine[6] == '0') AND (empty($termine[7])) AND ($termine[8] == '0')) {	// offener Termin
							echo "<td class =\"termin\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[6] == '1')) {		// Nichtkunde
							echo "<td class =\"nichtkunde\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND (!empty($termine[7]))) {		// storno
							echo "<td class =\"storno\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
						elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[8] == '1')) {		// kalt
							echo "<td class =\"kalt\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
						}
					}		// Ende else nur ein Termin
				$treffer = "1";
			}
		}
			if (mysqli_num_rows($ergebnis) > 0) {
				mysqli_data_seek($ergebnis, '0');
			}
			if ($treffer == 0) {
				if ($wochentag === "Sun" OR $wochentag === "So") {
					echo "<td class=\"sonntag\">&nbsp;</td>";
				}
				elseif ($wochentag == "Sat" OR $wochentag == "Sa") {
					echo "<td class=\"samstag\">&nbsp;</td>";
				}
				else {
					echo "<td class =\"zeiten\"><a href=\"termin_sperrzeit.php?s_zeit=$t_zeit[$j] & s_datum=$datum\">$t_zeit[$j]</a></td>";
				}
			}
			else {
				$treffer = "0";
			}
		} // ende if überhaupt Termine da?
		else {
			if ($wochentag === "Sun" OR $wochentag === "So") {
				echo "<td class=\"sonntag\">&nbsp;</td>";
			}
			elseif ($wochentag == "Sat" OR $wochentag == "Sa") {
				echo "<td class=\"samstag\">&nbsp;</td>";
			}
			else {
				echo "<td class =\"zeiten\"><a href=\"termin_sperrzeit.php?s_zeit=$t_zeit[$j] & s_datum=$datum\">$t_zeit[$j]</a></td>";
			}
		
		}
		
	}
	//echo "<td class=\"zeiten\">$t_zeit[$j]</td>";
	
	echo "</tr>";
	
} // ende for zeilen

echo "<tr><td colspan=\"$span\">";
?>
<form name="user_neu" method="post" action="<?php $PHP_SELF ?>">
	<table>
		<tr>
			<td><input type="submit" name="speichern" value="REFRESH" class = "submitt">			</td>
		</tr>
	</table>
	</form>
</td></tr></table>
</div>
</body></html>