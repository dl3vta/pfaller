<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen


include ("../include/init.php");
sessiondauer();

// Variablendefinition -------------------------------------------------------
// ---------------------------------------------------------------------------

$a_tag = $_POST["a_tag"];						// Auswerte-Tag
$a_monat = $_POST["a_monat"];					// Auswerte-Monat
$a_jahr = $_POST["a_jahr"];					// Auswerte-Jahr
$heute = $_POST["heute"];						// Auswerte-Datum HEUTE
$aq = $_POST["aq"];							// Aquise-alle
$ad = $_POST["ad"];							// Aussendienst-alle
$periode = $_POST["periode"];					// Anzeige-tage

$speichern = $_POST["speichern"];

$gruppe = $_SESSION['benutzer_gruppen'];

// Übername der Variablen aus dem Formular -------------------------------------
// -----------------------------------------------------------------------------

$tag = quote_smart($a_tag);
$monat = quote_smart($a_monat);
$jahr = quote_smart($a_jahr);
$periode = quote_smart($periode);

// Startwerte festlegen ----------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------------------

$t_zeit = array("09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", 
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");
	
$zahl_t_zeit = count($t_zeit);										// Anzahl der Elemente im Zeit-Array


// speichern gedrückt -----------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------

if (isset($speichern)) {

	$fehler = "0";	// fehlermeldung zurückgesetzt

	// ----------------------------------------------------------------------------------------------------------
	// heute angeklickt  ----------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------

	if (isset($heute)) {	// heute gesetzt
	
		if (empty($periode)) {	// keine Periode eingegeben
			$zahl = "14";			// wie viele Tage sollen angezeigt werden - Zählung beginnt bei Null
		}
		else {	//periode eingegeben
	
			$muster = "/^\d{1,2}$/";							// Test Datum, genau 2 Ziffern
			if(!preg_match($muster, $periode)) {	// Muster Periode passt nicht
				$fehler = "Fehler: Das ist kein gültiger Anzeigebereich!!";
			}
			
			else {	//Muster periode passt
					
				if ($periode > 31) {	// periode zu groß
					$fehler = "Der Anzeigezeitraum ist zu groß!!";
				}
				else { //Periode liegt im Wertebereich
						
					$zahl = $periode;			// wie viele Tage sollen angezeigt werden - Zählung beginnt bei Null, deshalb -1
						
				} // ende else Periode liegt im Wertebereich
			} // ende else Muster periode passt
		} // ende else periode eingegeben
		
		$start = date("Ymd");
		$termin = mktime(0,0,0,date("m"), date("d")+$zahl);
		$ende = (strftime("%Y", $termin)) .(strftime("%m", $termin)) . (strftime("%d", $termin));
		
		if (empty($aq) AND (empty($ad))) { 								// kein user gestetzt
			$fehler = "Bitte wählen Sie eine Mitarbeitergruppe/einen Mitarbeiter!";
		} // ende kein user gesetzt
		elseif (isset($ad_alle)) {	// Außendienstler gesetzt
			$user = "3";
		}	// ende elseif Außendienstler gesetzt
		else {	// Aquise gesetzt
			$user = "2";
		} // ende else Aquise gesetzt
	} // ende if heute gesetzt
	
	// ----------------------------------------------------------------------------------------------------------
	// termin eingegeben ----------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------

	else { // termineingabe erwartet
	
		if (empty($a_tag) OR empty($a_monat) OR empty($a_jahr)) {
			
			$fehler = "Sie müssen einen Termin eingeben!!";
		
		} // ende if keine termin eingegeben
		
		else {	// termin eingegeben
		
		$muster = "/^\d{2}$/";							// Test Datum, genau 2 Ziffern
			if(!preg_match($muster, $tag) OR !preg_match($muster, $monat) OR !preg_match($muster, $jahr)) {	// Muster Datum passt nicht
				$fehler = "Fehler: Das ist kein gültiges Datum!!";
			}
			else {  // Muster Datum stimmt		
				if (empty($periode)) {	// keine Periode eingegeben
					$zahl = "14";			// wie viele Tage sollen angezeigt werden - Zählung beginnt bei Null
					$start = "20" .$jahr . $monat . $tag;
					$jahr = "20" . $jahr;
					
					$offset = $zahl * 86400; 							// = Zahl (Tage) * Sekunden/tag
					$termin = mktime(0,0,0,$monat, $tag, $jahr);
					$termin = $termin + $offset;
					$ende = (strftime("%Y", $termin)) .(strftime("%m", $termin)) . (strftime("%d", $termin));
				}
				else {	//periode eingegeben
	
					$muster = "/^\d{1,2}$/";							// Test Periode, max 2 Ziffern
					if(!preg_match($muster, $periode)) {	// Muster Periode passt nicht
						$fehler = "Fehler: Das ist kein gültiger Anzeigebereich!!";
					}
			
					else {	//Muster periode passt
					
						if ($periode > 31) {	// periode zu groß
							$fehler = "Der Anzeigezeitraum ist zu groß!!";
						}
						else { //Periode liegt im Wertebereich
						
							$zahl = $periode;			// wie viele Tage sollen angezeigt werden - Zählung beginnt bei Null, deshalb -1
							$start = "20" .$jahr . $monat . $tag;
							$jahr = "20" . $jahr;
					
							$offset = $zahl * 86400; 							// = Zahl (Tage) * Sekunden/tag
							$termin = mktime(0,0,0,$monat, $tag, $jahr);
							$termin = $termin + $offset;
							$ende = (strftime("%Y", $termin)) .(strftime("%m", $termin)) . (strftime("%d", $termin));
						
						} // ende else Periode liegt im Wertebereich
					} // ende else Muster periode passt
				} // ende else periode eingegeben
				
				if (!isset($aq_alle) AND (!isset($ad_alle))) { // kein user gestetzt
					$fehler = "Bitte wählen Sie eine Nutzergruppe!";
		
				} // ende kein user gesetzt
			} // ende Muster Datum stimmt
		} // ende else termin eingegeben
	} // ende else termineingabe erwartet
	
	$span = $zahl + 1 ;		// Anzahl der anzuzeigenden Tage + eine Zelle für die Uhrzeit
	
	if ($fehler === '0') {	// kein Fehler bei der Eingabe

		// --------------------------------------------------------------------------------------------------------------------------------------------------
		// Ausgabe der Tabelle ------------------------------------------------------------------------------------------------------------------------------
		// --------------------------------------------------------------------------------------------------------------------------------------------------

		$tag = substr($start, 6,2);
		$monat = substr($start, 4,2);
		$jahr = substr($start, 0,4);

		?>

		<!-- Hier fängt das HTML-Dokument an -->
		<!DOCTYPE html>
		<html lang="de">
		<head>
		<title>Termine auswerten</title>
			<!-- admin/admin_auswertung.php -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div align = "center">
		<table border="1" width="600" class="zeiten" cellspacing="0" cellpadding="0">

  <?php

		// Mitarbeiter aus DB einlesen ----------------------------------------------------------------------------------------------------------
		// --------------------------------------------------------------------------------------------------------------------------------------
		
		if (!empty($aq)) {						//Telefonisten sollen angezeigt werden
		
			if ($aq == 'alle') {						// alle 
				$sql = "SELECT user, vorname, name ";
				$sql .= " FROM user, name, vorname ";
				$sql .= " WHERE user.gruppen_id = '2' AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
				$sql .= " ORDER BY user ASC";
			}
			else {
				$sql = "SELECT user, vorname, name ";
				$sql .= " FROM user, name, vorname ";
				$sql .= " WHERE user.gruppen_id = '2' AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id AND user = '$aq' ";
				$sql .= " ORDER BY user ASC";
			}
		}
		elseif (!empty($ad)) {						//Außendienstler sollen angezeigt werden
		
			if ($aq == 'alle') {						// alle 
				$sql = "SELECT user, vorname, name ";
				$sql .= " FROM user, name, vorname ";
				$sql .= " WHERE user.gruppen_id = '3' AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
				$sql .= " ORDER BY user ASC";
			}
			else {
				$sql = "SELECT user, vorname, name ";
				$sql .= " FROM user, name, vorname ";
				$sql .= " WHERE user.gruppen_id = '3' AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id AND user = '$ad' ";
				$sql .= " ORDER BY user ASC";
			}
		}
		
		$abfrage = myqueryi($db, $sql);

		for ($z = 0; $z < mysqli_num_rows($abfrage); $z++) { 	// Beginn schleife für Mitarbeiter
	
			$mitarbeiter = mysqli_fetch_row($abfrage);
		
			if (!empty($ad)) { // Ausgabe für Aussendienst
		
				$sql = "SELECT termin_id, termin, zeit, sperrzeit, plz, abschluss, termin.nichtkunde, termin.storno, kalt FROM termin, kunden, poo, plz ";
				$sql .= "WHERE termin.kd_id = kunden.kunden_id AND kunden.poo_id=poo.poo_id AND poo.plz_id=plz.plz_id AND termin.alt = '0' ";
				$sql .= "AND termin BETWEEN $start AND $ende AND termin.wiedervorlage = '0' AND aussendienst = '$mitarbeiter[0]' ";
			}
			
			elseif (!empty($aq)) { // Ausgabe für Aquise
		
				$sql = "SELECT termin_id, termin, zeit, sperrzeit, plz, abschluss, termin.nichtkunde, termin.storno, kalt FROM termin, kunden, poo, plz ";
				$sql .= "WHERE termin.kd_id = kunden.kunden_id AND kunden.poo_id=poo.poo_id AND poo.plz_id=plz.plz_id AND termin.alt = '0' ";
				$sql .= "AND termin BETWEEN $start AND $ende AND termin.wiedervorlage = '0' AND telefonist = '$mitarbeiter[0]' ";
			}

			$ergebnis = myqueryi($db, $sql);

			$datensatz = mysqli_num_rows($ergebnis);								// Anzahl der aus der Datenbank ausgelesenen Termine
		
			?>

			<table border="1" width="690" class="zeiten" cellspacing="0" cellpadding="0">
			<tr><td colspan="<?php echo "$span";?>">
			<table>
			<tr><td><span style="font-size: 10pt;font-weight: bold; color: red;">Termine für: <?php echo "$mitarbeiter[1]&nbsp;$mitarbeiter[2]&nbsp;($mitarbeiter[0])"; ?></span></td></tr>
			</table>
			</td></tr>

   <?php
 
  				echo "<tr>";
    				echo "<td>";
  						echo "<tr>";
  							echo "<td class=\"spalten\">&nbsp;</td>";						// Leerzelle, in den darunter liegenden Zellen steht die Uhrzeit 

  							for ($i= "0"; $i < $zahl; $i++) { 								// Termine (Datum) vom Start- bis Endewert als Kopfzeile ausgeben

								$start = mktime(0,0,0, $monat, $tag+$i);
								//setlocale (LC_TIME, 'ge');								// deutsche Benutzerumgebung eingestellt
								setlocale (LC_TIME, 'de_DE');// deutsche Benutzerumgebung eingestellt
								date_default_timezone_set("Europe/Berlin");
								$wochentag = strftime("%a", $start);						// Umstellung englischer Wochentag auf deutscher
			
								switch($wochentag) {
									case Sat: $wochentag = "Sa"; break;
									case Sun: $wochentag = "So"; break;
									case Mon: $wochentag = "Mo"; break;
									case Tue: $wochentag = "Di"; break;
									case Wed: $wochentag = "Mi"; break;
									case Thu: $wochentag = "Do"; break;
									case Fri: $wochentag = "Fr"; break;
								}
	
								$datum = ($wochentag) . "<br>" .(strftime("%d", $start)) . "." . (strftime("%m", $start)) . ".<br>" . (strftime("%Y", $start));
								$heute_datum = date("d")+0 . "-" . date("m");		// damit bei einstelligen Daten nur eine Stelle erscheint
								$aktuell = (strftime("%d", $start)) . "-" . (strftime("%m", $start));

								if ($heute_datum == $aktuell) {							// Spalte HEUTE - rot markiert
									echo "<td class=\"spalten\"><span style=\"font-weight:bold; color:red;\">$datum</span></td>";
								}
								else {
									echo "<td class=\"spalten\">$datum</td>";
								}
							} // Ende Termine (Datum) vom Start- bis Endewert als Kopfzeile ausgeben
							
						echo "</tr>";
						
						$treffer = "0";
						
						for ($j = 0; $j < $zahl_t_zeit; $j++) { 			// Schleife für Zeiten = Zeilen

							// ---------------------------------------------------------------------------------------------------------------------------------
							// es muss ermittelt werden, an welchem Tag zu der bestimmten Zeit $t_zeit[$j] mehrere Termine vorliegen
							// dazu werden alle aus der Datenbank ausgelesenen Termine durchlaufen und das Array "Teiler" geschaffen
							// das Array hat folgende Struktur: $teiler = ("0", "0", "2", "0"; ...) und wird am Anfang komplett mit 0 gefüllt
							// die einzelnen Elemente stehen für die Spalte, d.h. $datum($m), für $m=-1 (gestern) ...$m < $zahl (Anzahl der anzuzeigenden Tage)
							// für jeden gefundenen Datensatz wird das entsprechende Element um 1 erhöht
							// dies wird einmal pro Zeile gemacht, ansonsten müsste die Prozedur in jeder Zelle wiederholt werden
	
							for ($n = "0"; $n < ($zahl); $n++) {		// Initialisierung des Arrays "Teiler" für die Teilung der Zellen für Mehrfachtermine
								$teiler[$n] = "0";
							}
	
							for ($m = 0; $m < $zahl; $m++) { 			// Schleife für Tage (gestern + 14 Tage)
		
								$timestamp = mktime(0,0,0, $monat, $tag+$m);
								$datum = (strftime("%Y", $timestamp)) . "-" .(strftime("%m", $timestamp)) . "-" . (strftime("%d", $timestamp));
		
								if ($datensatz > 0) {																			// überhaupt Termine da?
		
									for ($p = 0; $p < $datensatz; $p++) {	// 

										$termine = mysqli_fetch_row($ergebnis);
				
										if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0')) {// Vergleich Datum mit Termin und Zeit, Sperrzeiten ausgeschlossen
				
											$teiler[($m+1)] = $teiler[$m+1] + 1;												// da $m von -1 läuft, muss Zähler im Array um 1 erhöht werden (läuft von 0)
										}
									}
									mysqli_data_seek($ergebnis, '0');													// Rücksetzen der Ergebnisliste für nächste Zeile (Zeit)
								}
							}																									// Ende Schleife Tage
	
							// Ende Ermittlung Termin-Mehrfachbelegung
							// ---------------------------------------------------------------------------------------------------------------------------------

							// Auswertung der Daten und Darstellung in Kalenderform beginnt
	
							echo "<tr>";
								echo "<td class=\"zeiten\">$t_zeit[$j]</td>";						// Uhrzeit für Zeile
								for ($k = 0; $k < $zahl; $k++) { 									// Schleife für Tage (von gestern bis in 13 Tagen)
	
									// Hier muss die Abfrage des Arrays rein
									// Zeiten: j = zeile 0 = 9.00 Uhr, (zahl_t_zeit-1) = 20.00
									// Datum: k = Spalte k=0: heute, k=$zahl: in 14 Tagen
									// das Array heißt $termine, Datum = $termine[1], zeit = $termine[2]
									// Lösung: Suche im Array nach der Zeit $t_zeit[$j]
									// und dann nach Datum heute + $k
	
									$termin = mktime(0,0,0, $monat, $tag+$k);
									$datum = (strftime("%Y", $termin)) . "-" .(strftime("%m", $termin)) . "-" . (strftime("%d", $termin));
									$wochentag = (strftime("%a", $termin));

									if ($datensatz > 0) {													// überhaupt Termine da?
				
										for ($l = 0; $l < $datensatz; $l++) {								// zeilenweises Auslesen der Datenbank-Query

											$termine = mysqli_fetch_row($ergebnis);
				
											if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j])) {	// Datum und Zeit stimmen überein
				
												if ($termine[3] == 1) {										// Termin ist Sperrzeit
													echo "<td class =\"sperrzeit\">S</td>";
													//echo "<td class =\"sperrzeit\"><a href=\"termin_sperrzeit.php?t_id=$termine[0]\">S</a></td>";
												}															// Ende Sperrzeit
										
											elseif ($teiler[($k+1)] > 1) {									// mehrere Termine auf einer Zeit
												mysqli_data_seek($ergebnis, '0');
					
												echo "<td class =\"doppelt\">";
												echo "<table width = \"100%\" cellspacing = \"0\" cellpadding = \"0\">";
		
												for ($p = 0; $p < $datensatz; $p++) {					// alle Datensätze überprüfen

													$termine = mysqli_fetch_row($ergebnis);
				
													if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[7])) AND ($termine[8] == '0')) {		// Abschluss
				
														echo "<tr>";
														echo "<td class =\"abschluss\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
														//echo "<td class =\"abschluss\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '0') AND ($termine[6] == '0') AND (empty($termine[7])) AND ($termine[8] == '0')) {	// offener Termin
														echo "<tr>";
														echo "<td class =\"termin\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
														//echo "<td class =\"termin\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[6] == '1')) {		// Nichtkunde
														echo "<tr>";
														echo "<td class =\"nichtkunde\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
														//echo "<td class =\"nichtkunde\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND (!empty($termine[7]))) {		// storno
														echo "<tr>";
														echo "<td class =\"storno\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
														//echo "<td class =\"storno\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[8] == '1')) {		// kalt
														echo "<tr>";
														echo "<td class =\"kalt\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
														//echo "<td class =\"kalt\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
												}
												echo "</table>";
												echo "</td>";
											} // ende mehrere Termine auf einer Zeit
						
											else { // nur ein Termin
					
												if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[7])) AND ($termine[8] == '0')) {		// Abschluss
													echo "<td class =\"abschluss\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
												//echo "<td class =\"abschluss\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
											}
											elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '0') AND ($termine[6] == '0') AND (empty($termine[7])) AND ($termine[8] == '0')) {	// offener Termin
												echo "<td class =\"termin\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
												//echo "<td class =\"termin\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
											}
											elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[6] == '1')) {		// Nichtkunde
												echo "<td class =\"nichtkunde\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
												//echo "<td class =\"nichtkunde\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
											}
											elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND (!empty($termine[7]))) {		// storno
												echo "<td class =\"storno\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
												//echo "<td class =\"storno\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
											}
											elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[8] == '1')) {		// kalt
												echo "<td class =\"kalt\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></td>";
												//echo "<td class =\"kalt\"><a href=\"termin_neu.php?t_id=$termine[0]\" target=\"aqframe_u\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
											}
										}		// Ende else nur ein Termin
										$treffer = "1";
									}
								}
								if (mysqli_num_rows($ergebnis) > 0) {
									mysqli_data_seek($ergebnis, '0');
								}
								if ($treffer == 0) {
									if ($wochentag === "Sun" OR $wochentag === "So") {
										echo "<td class=\"sonntag\">&nbsp;</td>";
									}
									elseif ($wochentag == "Sat" OR $wochentag == "Sa") {
										echo "<td class=\"samstag\">&nbsp;</td>";
									}
									else {
										echo "<td class =\"zeiten\">F</td>";
										//echo "<td class =\"zeiten\"><a href=\"termin_sperrzeit.php?s_zeit=$t_zeit[$j] & s_datum=$datum\">$t_zeit[$j]</a></td>";
									}
								}
								else {
									$treffer = "0";
								}
							} // ende if überhaupt Termine da?
							else {
								if ($wochentag === "Sun" OR $wochentag === "So") {
									echo "<td class=\"sonntag\">&nbsp;</td>";
								}
								elseif ($wochentag == "Sat" OR $wochentag == "Sa") {
									echo "<td class=\"samstag\">&nbsp;</td>";
								}
								else {
									echo "<td class =\"zeiten\">F</td>";
									//echo "<td class =\"zeiten\"><a href=\"termin_sperrzeit.php?s_zeit=$t_zeit[$j] & s_datum=$datum\">$t_zeit[$j]</a></td>";
								}
							}
						}
						echo "</tr>";
					} // ende for zeilen
				echo "<br />";						// trennt die einzelnen Tabellen voneinander
			?>
</table>
</td></tr></table>
<?php

echo "</td></tr>";
	
	} // ende schleife für User

?>

</table>
</div>
</body></html>


<?php
} // ende if kein Fehler bei der Eingabe

		
} // ende if isset speichen

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Termin-Auswertung</title>
	<!-- admin/admin_auswertung.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<form name="startdaten" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
<table width="580" border="0" cellpadding="2" cellspacing="2">
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%"  border="0" cellspacing="5" cellpadding="5"  bgcolor="#eeeeee">
<?php
	// Fehlerausgabe eine Zeile ---------------------------------------------------

	if ($fehler) { echo "<tr><td colspan = \"2\" align = \"center\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold; line-height: 200%\">$fehler</span></td></tr>"; }

	// Überschrift für eingeloggten Telefonisten ---------------------------------
	
	if ($gruppe == 2) {
		echo "<tr bgcolor=\"#006699\">";
  			echo "<td colspan=\"2\"><div align = \"center\"><span style = \"color: white; font-size: 12pt; font-weight: bold; line-height: 200%;\">Anzeige Termine für alle Außendienstler</span></div></td>";
		echo "</tr>";
	}
	
	// Überschrift für eingeloggten Admin -----------------------------------------
	
	elseif ($gruppe == 1) {
		echo "<tr bgcolor=\"#006699\">";
  			echo "<td colspan=\"2\"><div align = \"center\"><span style = \"color: white; font-size: 12pt; font-weight: bold; line-height: 200%;\">Anzeige Termine für alle Außendienstler/Telefonisten</span></div></td>";
		echo "</tr>";
	}

?>

<!-- Beginn der Dateneingabe 1. Zeile Kennzeichnung der Daten je Spalte -->
<tr>
	<td valign = "top" width = "50%" bgcolor = "#ffffcc"><div align = "center"><span style = "font-weight: bold; font-size:10pt; line-height: 200%;">Auswertezeitraum</span></div><br />
        <span style = "font-size: xx-small;">(Standard ist ein Zeitraum von 14 Tagen<br /> ab Startdatum - Max. sind 31!)</span>
	</td>
	<td valign = "top"  bgcolor = "#ccffff"><div align = "center"><span style = "font-weight: bold; font-size:10pt; line-height: 200%;">Mitarbeitergruppe/<br />Mitarbeiter</span></div>
	</td>
</tr>

<!-- 2. Zeile Dateneingabe: Spalte 1 - Termin, Spalte 2 - Außendienstler -->

<tr>
	<td nowrap  bgcolor = "#ffffcc"><span style = "font-weight: bold;">ab heute</span>&nbsp;<input type="checkbox" name="heute" value="" />
				<span style = "font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;max</span>&nbsp;<input type="text" name="periode" size = "2" maxlength = "2" />&nbsp;Tage
	</td>
	<td bgcolor = "#ccffff"><span style = "font-weight: bold; font-size: 9pt; line-height: 150%;">Außendienst:&nbsp;</span>
	     <select name="ad" class = "eingabe">
	 <?php
		   	$sql = "SELECT user FROM user WHERE gruppen_id = '3' ORDER BY user ASC";
			$query = myqueryi($db, $sql);
			
			echo " <option></option>";
			echo " <option>alle</option>";
			for ($j = 0; $j < mysqli_num_rows($query); $j++)	{					// Anzahl der Datensätze
				$zeile = mysqli_fetch_row($query);						// Schleife für Daten-Zeilen
					//if ($zeile[0] == $t_branche) {
					//	echo " <option selected>$zeile[0]</option>";
					//}
					//else {
						echo " <option>$zeile[0]</option>";
					//}
				}
	 	?>
    	</select>
	</td>
</tr>

<!-- 3. Zeile Dateneingabe: Spalte 1 - Termin, Spalte 2 - Telefonist, für eingeloggten Telefonisten ausgeblendet -->

<tr>
	<td nowrap  bgcolor = "#ffffcc"><span style = "font-weight: bold;">oder</span>&nbsp;ab&nbsp;	<input type="text"  class="effecteingabe" name="a_tag" size = "2" maxlength ="2" value = ""/>
					.
					<input type="text"  class="effecteingabe" name="a_monat" size = "2" maxlength ="2" value = ""/>
					.
					<input type="text"  class="effecteingabe" name="a_jahr" size = "2" maxlength ="2" value = ""/>
	</td>
			
	<?php	if ($gruppe == '1') {		// eingeloggter User ist Admin
	
			echo "<td  bgcolor = \"#ccffff\"><span style = \"font-weight: bold; font-size: 9pt; line-height: 150%;\">Telefonist:&nbsp;</span>";
	     	echo "<select name=\"aq\" class = \"eingabe\">";

		   	$sql = "SELECT user FROM user WHERE gruppen_id = '2' ORDER BY user ASC";
			$query = myqueryi($db, $sql);
			
			echo " <option></option>";
			echo " <option>alle</option>";
			for ($j = 0; $j < mysqli_num_rows($query); $j++)	{					// Anzahl der Datensätze
				$zeile = mysqli_fetch_row($query);						// Schleife für Daten-Zeilen
					//if ($zeile[0] == $t_branche) {
					//	echo " <option selected>$zeile[0]</option>";
					//}
					//else {
						echo " <option>$zeile[0]</option>";
					//}
			}
	 	    echo "</select>";
			echo "</td>";
		}
		elseif ($gruppe == '2') {	// eingeloggter User ist Admin
			echo "<td  bgcolor = \"#ccffff\"><span style = \"font-weight: bold; font-size: 9pt; line-height: 150%;\">&nbsp;</span>";
		}
	?>
</tr>
<tr>
	<td colspan = "2" bgcolor = "#ffcccc"><div align="center"><input type="submit" name="speichern" value="Anzeigen" class = "submitt"></div></td>
</tr>
   
</table>
</td></tr></table>
</td></tr>
</table>
</form>
</div>
</body>
</html>