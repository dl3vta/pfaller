<?php

include ("include/ini.php");		// Session-Lifetime

session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/mysqlconnect.inc.php");
include ("../include/variablen.php");
include ("../include/funktionen.php");

sessiondauer();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++ SPEICHERN GEDRüCKT +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if (isset($speichern)) {																		// Speichern-Button wurde gedrückt
	if (empty($user) OR empty($passwort)) {
		$fehler = "Fehler: Sie müssen mindestens User-Name UND Passwort eingeben!";				// kein User-Name oder Passwort
	}
	else  {																						// Daten eingegeben
	// USER -------------------------------------------------------------------------------------------------------------
		$user = quote_smart($user);														// evtl. ESCAPE-Zeichen entfernt																// evtl. Whitespaces entfernt
					
		//$sql = "SELECT user FROM user WHERE user = '$user' ";								// USER schon vorhanden?
		$sql = "SELECT user FROM user WHERE user = '$user' AND user_id > '1'";							// Micha ausgeblendet
		$abfrage = myqueryi($db, $sql);

		if ($ergebnis = mysqli_fetch_array($abfrage, MYSQLI_NUM)) {
			$fehler = "Fehler: Dieser User ist bereits vorhanden!";
		}
		else { 	// neuer User -----------------------------------------------------------------------------------------------------------
		// Vorname ----------------------------------------------------------------------------------------------------------
			if (empty($vorname)) {															// kein Vorname eingetragen
				$vorname_id = 1;
			}
			else {												
				$vorname = quote_smart($vorname);											// evtl. ESCAPE-Zeichen entfernt
				$vorname = ucfirst($vorname);												// erstes Zeichen groß
				$sql1 = "SELECT vorname_id FROM vorname WHERE vorname = '$vorname'";		// Vorname schon vorhanden?
				$sql2 = "INSERT INTO vorname (vorname) VALUES ('$vorname')";				// Vorname neu
				$vorname_id = id_ermitteln($db, $sql1, $sql2);
			}
	
		// Name ----------------------------------------------------------------------------------------------------------
			if (empty($name)) {																// kein Name eingetragen
				$name_id = 1;
			}
			else {
				$name = quote_smart($name);												// evtl. ESCAPE-Zeichen entfernt													// evtl. Whitespaces entfernt
				$name = ucfirst($name);														// erstes Zeichen groß
				$sql1 = "SELECT name_id FROM name WHERE name = '$name'";					// Name schon vorhanden?
				$sql2 = "INSERT INTO name (name) VALUES ('$name')";							// Name neu
				$name_id = id_ermitteln($db, $sql1, $sql2);
			}

		// USER ----------------------------------------------------------------------------------------------------------------
			$user = quote_smart($user);
			
		// Gruppe ------------------------------------------------------------------------------------------------------------
			$sql = "SELECT gruppen_id FROM gruppen WHERE gruppen = '$gruppe'";
			$abfrage = myqueryi($db, $sql);
			$ergebnis = mysqli_fetch_array($abfrage, MYSQLI_NUM);
			$gruppen_id = $ergebnis[0];
			
		// Passwort ---------------------------------------------------------------------------------------------------------
			$pwd = md5($passwort);															// Passwort verschlüsselt
			
		// Ort --------------------------------------------------------------------------------------------------------------
			if (empty($ort)) {																// Feld Ort ist leer
				$ort_id = 1;																	// Standard gesetzt
			}
			else {
				$ort = quote_smart($ort);														// Whitespaces entfernen
				$sql1 = "SELECT ort_id FROM ort WHERE ort.ort = '$ort'";					// Ort schon vorhanden?
				$sql2 = "INSERT INTO ort (ort) VALUES ('$ort')";							// Ort neu
				$ort_id = id_ermitteln($db, $sql1, $sql2);
			}
					
		// Ortsteil --------------------------------------------------------------------------------------------------------------
			if (empty($ortsteil)) {																// Feld Ort ist leer
				$ortsteil_id = 1;																	// Standard gesetzt
			}
			else {
				$ortsteil = quote_smart($ortsteil);
				$ortsteil = trim($ortsteil);														// Whitespaces entfernen
				$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil.ortsteil = '$ortsteil'";					// Ort schon vorhanden?
				$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil')";							// Ort neu
				$ortsteil_id = id_ermitteln($db, $sql1, $sql2);
			}
					
	   	// PLZ --------------------------------------------------------------------------------------------------------------
			if (empty($plz)) {																// Feld PLZ ist leer
				$plz_id = 1;																	// Standard gesetzt
			}
			else {
				$plz = quote_smart($plz);													// Whitespaces entfernen
						
				$muster = "/^\d{5}$/";											// Test PLZ, Muster genau 5 Zahlen
				if(preg_match($muster, $plz)) {
						
				$sql1 = "SELECT plz_id FROM plz WHERE plz.plz = '$plz'";		// PLZ+Ort schon vorhanden?
				$sql2 = "INSERT INTO plz (plz) VALUES ('$plz')";				         // PLZ neu
				$plz_id = id_ermitteln($db, $sql1, $sql2);
			}
			else {
				$fehler = "<b>\"$plz\"</b> ist keine gültige Postleitzahl";
			}
			}
					
		// Kombination PLZ/Ort/Ortsteil
			$sql1 = "SELECT poo_id FROM poo WHERE ort_id = '$ort_id' AND ortsteil_id = '$ortsteil_id' AND plz_id = '$plz_id'";
			$sql2 = "INSERT INTO poo (ort_id, ortsteil_id, plz_id) VALUES ('$ort_id', '$ortsteil_id', '$plz_id')";
			$poo_id = id_ermitteln($db, $sql1, $sql2);

		// Strasse ---------------------------------------------------------------------------------------------------------------
			if (!empty($strasse)) {															// Straße eingetragen
				$strasse = quote_smart($strasse);
				
				$strasse = ucfirst($strasse);												// erstes Zeichen groß
			}
		
		// vorwahl1 ---------------------------------------------------------------------------------------------------------------
			if (empty($vorwahl1)) {															// keine Vorwahl1 eingetragen
				$vorwahl1_id = 1;
			}
			else {
				$vorwahl1 = quote_smart($vorwahl1);
				$sql1 = "SELECT vorwahl1_id FROM vorwahl1 WHERE vorwahl1 = '$vorwahl1'";	// Vorwahl1 schon vorhanden?
				$sql2 = "INSERT INTO vorwahl1 (vorwahl1) VALUES ('$vorwahl1')";				// Vorwahl1 neu
				$vorwahl1_id = id_ermitteln($db, $sql1, $sql2);
			}
			
		// Telefon1 ---------------------------------------------------------------------------------------------------------------
			if (!empty($telefon1)) {														// kein Telefon1 eingetragen
				$telefon1 = quote_smart($telefon1);
			}
			
		// vorwahl2 ---------------------------------------------------------------------------------------------------------------
			if (empty($vorwahl2)) {															// keine Vorwahl1 eingetragen
				$vorwahl2_id = 1;
			}
			else {
				$vorwahl2 = quote_smart($vorwahl2);
				$sql1 = "SELECT vorwahl2_id FROM vorwahl2 WHERE vorwahl2 = '$vorwahl2'";	// Vorwahl2 schon vorhanden?
				$sql2 = "INSERT INTO vorwahl2 (vorwahl2) VALUES ('$vorwahl2')";				// Vorwahl2 neu
				$vorwahl2_id = id_ermitteln($db, $sql1, $sql2);
			}
			
		// Telefon2 ---------------------------------------------------------------------------------------------------------------
			if (!empty($telefon2)) {														// kein Telefon2 eingetragen
				$telefon2 = quote_smart($telefon2);
			}
			
		// Mail ---------------------------------------------------------------------------------------------------------------
			if (!empty($mail)) {															// keine Mailadresse eingetragen
				$mail = quote_smart($mail);
			}
			
	$sql = "INSERT INTO user (gruppen_id, user, pwd, name_id, vorname_id, poo_id, ";
	$sql .= "strasse, vorwahl1_id, telefon1, vorwahl2_id, telefon2, mail) ";
	$sql .= "VALUES ('$gruppen_id', '$user', '$pwd', '$name_id', '$vorname_id', '$poo_id', ";
	$sql .= "'$strasse', '$vorwahl1_id', '$telefon1', '$vorwahl2_id', '$telefon2', '$mail') ";
	
	$abfrage = myqueryi($db, $sql);

	$user_id = mysqli_insert_id($db);
	$fehler = "Der Benutzer \"$user\" mit der ID: $user_id wurde eingefügt";
	
	} // Ende ELSE neuer User
	} // Ende ELSE Daten eingegeben
}	// Ende IF ISSET speichern

// User-Update------------------------------------------------------------------------------------------------------------------------

if (isset($update)) {																		// Update-Button von user_update.php

	if (empty($user)) {
		$fehler = "Fehler: Sie müssen mindestens den User-Namen eingeben!";						// kein User-Name oder Passwort
	}
	else { // User eingegeben
	
	// Vorname ----------------------------------------------------------------------------------------------------------
			if (empty($vorname)) {															// kein Vorname eingetragen
				$vorname_id = 1;
			}
			else {												
				$vorname = quote_smart($vorname);											// evtl. ESCAPE-Zeichen entfernt
				$vorname = ucfirst($vorname);												// erstes Zeichen groß
				$sql1 = "SELECT vorname_id FROM vorname WHERE vorname = '$vorname'";		// Vorname schon vorhanden?
				$sql2 = "INSERT INTO vorname (vorname) VALUES ('$vorname')";				// Vorname neu
				$vorname_id = id_ermitteln($db, $sql1, $sql2);
			}
	
		// Name ----------------------------------------------------------------------------------------------------------
			if (empty($name)) {																// kein Name eingetragen
				$name_id = 1;
			}
			else {
				$name = quote_smart($name);												// evtl. ESCAPE-Zeichen entfernt	// evtl. Whitespaces entfernt
				$name = ucfirst($name);														// erstes Zeichen groß
				$sql1 = "SELECT name_id FROM name WHERE name = '$name'";					// Name schon vorhanden?
				$sql2 = "INSERT INTO name (name) VALUES ('$name')";							// Name neu
				$name_id = id_ermitteln($db, $sql1, $sql2);
			}

		// USER ----------------------------------------------------------------------------------------------------------------
			$user = quote_smart($user);
			
		// Gruppe ------------------------------------------------------------------------------------------------------------
			$sql = "SELECT gruppen_id FROM gruppen WHERE gruppen = '$gruppe'";
			$abfrage = myqueryi($db, $sql);
			$ergebnis = mysqli_fetch_array($abfrage, MYSQLI_NUM);
			$gruppen_id = $ergebnis[0];
			
		// Passwort ---------------------------------------------------------------------------------------------------------
			$pwd = md5($passwort);															// Passwort verschlüsselt
			
		// Ort --------------------------------------------------------------------------------------------------------------
			if (empty($ort)) {																// Feld Ort ist leer
				$ort_id = 1;																	// Standard gesetzt
			}
			else {
				$ort = quote_smart($ort);														// Whitespaces entfernen
				$sql1 = "SELECT ort_id FROM ort WHERE ort.ort = '$ort'";					// Ort schon vorhanden?
				$sql2 = "INSERT INTO ort (ort) VALUES ('$ort')";							// Ort neu
				$ort_id = id_ermitteln($db, $sql1, $sql2);
			}
					
		// Ortsteil --------------------------------------------------------------------------------------------------------------
			if (empty($ortsteil)) {																// Feld Ort ist leer
				$ortsteil_id = 1;																	// Standard gesetzt
			}
			else {
				$ortsteil = quote_smart($ortsteil);
				$ortsteil = trim($ortsteil);														// Whitespaces entfernen
				$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil.ortsteil = '$ortsteil'";					// Ort schon vorhanden?
				$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil')";							// Ort neu
				$ortsteil_id = id_ermitteln($db, $sql1, $sql2);
			}
					
	   	// PLZ --------------------------------------------------------------------------------------------------------------
			if (empty($plz)) {																// Feld PLZ ist leer
				$plz_id = 1;																	// Standard gesetzt
			}
			else {
				$plz = quote_smart($plz);													// Whitespaces entfernen
						
				$muster = "/^\d{5}$/";											// Test PLZ, Muster genau 5 Zahlen
				if(preg_match($muster, $plz)) {
						
				$sql1 = "SELECT plz_id FROM plz WHERE plz.plz = '$plz'";		// PLZ+Ort schon vorhanden?
				$sql2 = "INSERT INTO plz (plz) VALUES ('$plz')";				         // PLZ neu
				$plz_id = id_ermitteln($db, $sql1, $sql2);
			}
			else {
				$fehler = "<b>\"$plz\"</b> ist keine gültige Postleitzahl";
			}
			}
					
		// Kombination PLZ/Ort/Ortsteil
			$sql1 = "SELECT poo_id FROM poo WHERE ort_id = '$ort_id' AND ortsteil_id = '$ortsteil_id' AND plz_id = '$plz_id'";
			$sql2 = "INSERT INTO poo (ort_id, ortsteil_id, plz_id) VALUES ('$ort_id', '$ortsteil_id', '$plz_id')";
			$poo_id = id_ermitteln($db, $sql1, $sql2);

		// Strasse ---------------------------------------------------------------------------------------------------------------
			if (!empty($strasse)) {															// Straße eingetragen
				$strasse = quote_smart($strasse);
				
				$strasse = ucfirst($strasse);												// erstes Zeichen groß
			}
		
		// vorwahl1 ---------------------------------------------------------------------------------------------------------------
			if (empty($vorwahl1)) {															// keine Vorwahl1 eingetragen
				$vorwahl1_id = 1;
			}
			else {
				$vorwahl1 = quote_smart($vorwahl1);
				$sql1 = "SELECT vorwahl1_id FROM vorwahl1 WHERE vorwahl1 = '$vorwahl1'";	// Vorwahl1 schon vorhanden?
				$sql2 = "INSERT INTO vorwahl1 (vorwahl1) VALUES ('$vorwahl1')";				// Vorwahl1 neu
				$vorwahl1_id = id_ermitteln($db, $sql1, $sql2);
			}
			
		// Telefon1 ---------------------------------------------------------------------------------------------------------------
			if (!empty($telefon1)) {														// kein Telefon1 eingetragen
				$telefon1 = quote_smart($telefon1);
			}
			
		// vorwahl2 ---------------------------------------------------------------------------------------------------------------
			if (empty($vorwahl2)) {															// keine Vorwahl1 eingetragen
				$vorwahl2_id = 1;
			}
			else {
				$vorwahl2 = quote_smart($vorwahl2);
				$sql1 = "SELECT vorwahl2_id FROM vorwahl2 WHERE vorwahl2 = '$vorwahl2'";	// Vorwahl2 schon vorhanden?
				$sql2 = "INSERT INTO vorwahl2 (vorwahl2) VALUES ('$vorwahl2')";				// Vorwahl2 neu
				$vorwahl2_id = id_ermitteln($db, $sql1, $sql2);
			}
			
		// Telefon2 ---------------------------------------------------------------------------------------------------------------
			if (!empty($telefon2)) {														// kein Telefon2 eingetragen
				$telefon2 = quote_smart($telefon2);
			}
			
		// Mail ---------------------------------------------------------------------------------------------------------------
			if (!empty($mail)) {															// keine Mailadresse eingetragen
				$mail = quote_smart($mail);
			}
	
		$sql = "UPDATE user SET vorname_id = '$vorname_id', name_id = '$name_id', user = '$user', ";
		$sql .= "gruppen_id = '$gruppen_id', poo_id = '$poo_id', strasse = '$strasse', ";
		$sql .= "vorwahl1_id = '$vorwahl1_id', telefon1 = '$telefon1', vorwahl2_id = '$vorwahl2_id', telefon2 = '$telefon2', ";
		$sql .= "mail = '$mail' ";
		if (!empty($passwort)) {																// neues Passwort
			$pwd = md5($passwort);																// Passwort verschlüsselt
			$sql .= ", pwd = '$pwd' ";
		}
		$sql .= "WHERE user_id = '$user_id'";
		
		$abfrage = myqueryi($db, $sql);
			
		$fehler = "Die Daten für den Benutzer \"$user_id\" : \"$user\" wurden geändert";
		
		
} // ende else User nicht leer
}	// Ende IF ISSET update
//---------------------------------------------------------------------------------------------------------------------------------------

// Start Script-Ablauf vor dem Drücken des Speichern-Buttons

	$sql  = "SELECT user_id AS ID, user AS Benutzer, gruppen AS Gruppe, vorname AS Vorname, name AS Name, ";
	$sql .= "Vorwahl1 AS Vorwahl1, telefon1 AS Telefon, vorwahl2 AS Vorwahl2, telefon2 AS Telefon2 ";
	$sql .= "FROM user, gruppen, vorname, name, vorwahl1, vorwahl2 ";
	$sql .= "WHERE user.gruppen_id = gruppen.gruppen_id AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
	//$sql .= "AND user.vorwahl1_id = vorwahl1.vorwahl1_id AND user.vorwahl2_id = vorwahl2.vorwahl2_id ";
	$sql .= "AND user.vorwahl1_id = vorwahl1.vorwahl1_id AND user.vorwahl2_id = vorwahl2.vorwahl2_id AND user.user_id > '1'";				// micha ausgeblendet
	$sql .= "ORDER BY gruppen ASC, user ASC ";
	
	$ergebnis = myqueryi ($db, $sql);
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>User-Registrierung</title>
	<!-- admin/user_registration.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<table width="750" border="0" cellpadding="8" cellspacing="8">
<tr>
<td><br><h2 class="Stil1">Benutzer bearbeiten</h2><td>
</tr>
<tr><td>
<table width="80%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">

<?php
echo "<table width=\"100%\" border=\"0\"  cellspacing=\"8\"";

//Ausgabe der bereits vorhandenen Benutzer +++++++++++++++++++++++++++++++++++++++++++++++

	echo "<tr><strong>bereits registrierte Benutzer:</strong></tr>";
	echo "<tr>";													// Tabellenkopf
	for($i = 0; $i < mysqli_num_fields($ergebnis); $i++)				// Anzahl der Tabellenzellen pro Zeile
	{
		$row = mysqli_fetch_assoc($ergebnis);
		$spalte = array_keys($row);
		mysqli_data_seek($ergebnis, 0);
		// Name der Tabellenzelle
		echo "<td><b>$spalte[$i]</b></td>";
	}
	echo "<td>&nbsp;</td>";											// eine Zelle für den BEARBEITEN-Button angehängt
	echo "</tr>\n";													// Tabellenkopf Ende
	while($zeile = mysqli_fetch_row($ergebnis))						// Schleife für Daten-Zeilen
	{
		echo "<tr>";
		for($i = 0; $i < mysqli_num_fields($ergebnis); $i++)			// Schleife für Felder
		{	
			if ($zeile[$i] == "0" OR $zeile[$i] == "NULL" ) {								// Standard-0 wird durch Leerzeichen ersetzt
				$zeile[$i] = "&nbsp;";
			}
			echo "<td>" . $zeile[$i] . "</td>";
		}					
		
		echo "<td>";																// die Zelle Bearbeiten wird 
		echo '<a href="user_update.php?id=', urlencode($zeile[0]), '">';		// mit dem Link zum Bearbeiten-Script
		echo "BEARBEITEN</a></td></tr>";													// + angehängter ID gefüllt
    }
echo "</table>";

// Ende Ausgabe vorhandene Benutzer +++++++++++++++++++++++++++++++++++++++++++++++

echo "<br><hr>";
echo "</td></tr>";

if ($fehler) {
	echo "<tr><td  bgcolor = \"red\"><span style=\"color:white; font-weight:bold;\">$fehler<br></span></td></tr>";
}
echo "<tr><td>";
?>

<!-- Start Formular für neue Benutzer ++++++++++++++++++++++++++++++++++ // -->

<form name="user_neu" method="post" action="<?php $PHP_SELF ?>">
<table>
<tr><td colspan = "6"><strong>Eingabe neuer Benutzer:</strong></td></tr>
  <tr>
    <td>Vorname:</td>
    <td><input type="text" name="vorname"></td>
    <td>Name:</td>
    <td><input type="text" name="name"></td>
    <td>&nbsp;</td>
	<td>&nbsp;</td>
  </tr>
  <tr>
    <td>User</td>
    <td><input type="text" name="user"></td>
	<td>Passwort</td>
	<td><input type="text" name="passwort"></td>
	<td>Gruppe</td>
	<td><select name="gruppe">
 <?php
		$abfrage = mysqli_query($db, "SELECT gruppen FROM gruppen");			// die Gruppen werden aus Tabelle Gruppen gelesen
		while ($zeile = mysqli_fetch_row($abfrage)) {							// Anzahl der auszugebenden Zeilen
			echo "<option value = \"" . $zeile[0] . "\">";						// das iat der Name der Option!
			echo $zeile[0];														// das ist die Anzeige!
			echo "</option>\n";
		}
	?>
    </select></td>
  </tr>
    <tr>
    <td>PLZ</td>
    <td><input type="text" name="plz"></td>
    <td>Ort</td>
    <td><input type="text" name="ort"></td>
    <td>Ortsteil</td>
	<td><input type="text" name="ortsteil"></td>
 </tr>
    <tr>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Straße</td>
	<td><input type="text" name="strasse"></td>

    <td>&nbsp;</td>
	<td>&nbsp;</td>
  </tr>
  </tr>
    <tr>
    <td>Vorwahl1</td>
    <td><input type="text" name="vorwahl1"></td>
    <td>Telefon</td>
    <td><input type="text" name="telefon1"></td>
    <td>&nbsp;</td>
	<td>&nbsp;</td>
    </tr>
  <tr>
    <tr>
    <td>Vorwahl2</td>
    <td><input type="text" name="vorwahl2"></td>
    <td>Telefon2</td>
    <td><input type="text" name="telefon2"></td>
    <td>&nbsp;</td>
	<td>&nbsp;</td>    
    </tr>
  <tr>
  <tr>
    <td>E-Mail</td>
    <td><input type="text" name="mail"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
	<td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan ="6"><input type="submit" name="speichern" value="Speichern" class = "submitt"></td>
    </tr>
</table>      
</form>

<!-- Ende Formular für neue Benutzer ++++++++++++++++++++++++++++++++++ // -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

</div>
</body>
</html>