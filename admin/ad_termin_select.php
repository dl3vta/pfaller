<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");
sessiondauer();

// Variablen definieren ---------------------------------------
// ------------------------------------------------------------

$starttag = $_POST["starttag"];
$startmonat = $_POST["startmonat"];
$startjahr = $_POST["startjahr"];
$endetag = $_POST["endetag"];
$endemonat = $_POST["endemonat"];
$endejahr = $_POST["endejahr"];

$senden = $_POST["senden"];
$user = $_POST["user"];

// Ende Variablendefinition -----------------------------------
// ------------------------------------------------------------

$zahl = "5";				// Standardauswertezeitraum
$muster = "/^\d{1,2}$/";	// Muster für Datumstest, genau 2 Ziffern

empty($fehler);				// Variable Fehler leeren


// Speichern wurde gedrückt -----------------------------------
// -------------------------------------------------------------

if (isset($speichern)) {

// Post-Variablen von Sonderzeichen befreien ------------------

	$starttag1 = quote_smart($starttag);
	$startmonat1 = quote_smart($startmonat);
	$startjahr1 = quote_smart($startjahr);
	$endetag1 = quote_smart($endetag);
	$endemonat1 = quote_smart($endemonat);
	$endejahr1 = quote_smart($endejahr);
	$user1 = quote_smart($user);

// überprüfung der Eingaben ---------------------------------------------

	if (empty($user1)) {													// kein Außendienstler eingegeben
		$fehler = "Bitte Außendienstler auswählen!";
	}

	elseif (empty($starttag) OR empty($startmonat)) {							// kein Startdatum eingegeben (Jahr kann freibleiben - aktuelles Jahr!)
	$fehler = "Bitte Startdatum eingeben!";
	}

	else {																		// alle Daten vorhanden
	
		// überprÜfung Startdatum auf korrekte Eingabe und  korrekte Werte für Tag, Monat, Jahr

		if(!preg_match($muster, $starttag) OR ($starttag < 1) OR ($starttag > 31)) {		// Muster Starttag passt nicht oder außerhalb des Wertebereiches
			$fehler = "&nbsp;Starttag: Eingabe falsch!";
		}
		if(!preg_match($muster, $startmonat) OR ($startmonat < 1) OR ($startmonat > 12)) {	// Muster Startmonat passt nicht oder außerhalb des Wertebereiches
			$fehler = $fehler . "&nbsp;Startmonat: Eingabe falsch!";
		}
		if (!empty($startjahr)) {															// Startjahr wurde eingegeben
			if(!preg_match($muster, $startjahr) OR ($startjahr < 06)) {						// Muster Startjahr passt nicht oder außerhalb des Wertebereiches
				$fehler = $fehler . "&nbsp;Startjahr: Eingabe falsch!";
			}
			else {																			// Eingabe Startjahr ist korrekt
				$startjahr = "20" . $startjahr;
			} 
		}
		else {																				// kein Startjahr eingegeben - aktuelles Jahr wird angenommen
			$startjahr = date("Y");
		}
		
		// Überprüfung Endedatum auf korrekte Eingabe und  korrekte Werte für Tag, Monat
		
		if (!empty($endetag) OR !empty($endemonat)) {				// irgend ein Wert für das Endedatum ist eingegeben worden
		
			if(!preg_match($muster, $endetag) OR ($endetag < 1) OR ($endetag > 31)) {	// Muster Endetag passt nicht oder außerhalb des Wertebereiches
				$fehler = $fehler = $fehler . "&nbsp;Endetag: Eingabe falsch!";
			}
			if(!preg_match($muster, $endemonat) OR ($endemonat < 1) OR ($endemonat > 12)) {	// Muster Endemonat passt nicht oder außerhalb des Wertebereiches
				$fehler = $fehler . "&nbsp;Endemonat: Eingabe falsch!";
			}
			if (!empty($endejahr)) {														// Endejahr wurde eingegeben
				if(!preg_match($muster, $endejahr) OR ($endejahr < 06)) {					// Muster Endejahr passt nicht oder außerhalb des Wertebereiches
					$fehler = $fehler . "&nbsp;Endejahr: Eingabe falsch!";
				}
				else {																		// Eingabe Endejahr ist korrekt
					$endejahr = "20" . $endejahr;
				} 
			}
			else {																			// kein Endejahr eingegeben - aktuelles Jahr wird angenommen
				$endejahr = date("Y");
			}
			
		} // Ende if ein wert für Endedatum eingetragen
	} // ende else alle Daten eingegeben
	
	if (empty($fehler)) {																	// kein Fehler aufgetreten
	
		$startdatum = $startjahr . $startmonat . $starttag;
		$endedatum = $endejahr . $endemonat . $endetag;
		echo "<script>location.href='ad_termin_druck.php?start=$startdatum & ende=$endedatum & user=$user1'</script>";	// Übergabe der Daten per GET
	}
} // ende if isset speichern

?>

<!DOCTYPE html>
<html lang ="de">
<head>
<title>Auswertung - Außendienst</title>
	<!-- admin/ad_termin_select.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<form name="startdaten" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
<table width="500" border="0" cellpadding="4" cellspacing="4">
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="#eeeeee">
<?php
	if ($fehler) {
		echo "<tr><td colspan = \"6\" align = \"left\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold;\">$fehler</span></td></tr>";
	}
?>
  <tr>
    <td colspan="4"><div align="left"><span style = "font-weight: bold;">Bitte geben Sie den Außendienstler und den Auswertezeitraum ein.</span><br /><br />
      				<span style = "font-size: small;">Wenn Sie kein Jahr beim Startdatum eingeben,<br />nimmt das Programm <strong>das aktuelle Jahr</strong>.<br /><br />
	  				Wenn Sie kein Endedatum eingeben, gibt das Programm<br /><strong>automatisch 5 Tage ab Startdatum</strong> aus.<br /><br />
					Wenn Sie kein Jahr beim Endedatum eingeben,<br />nimmt das Programm <strong>das aktuelle Jahr</strong>.</span></div>
	</td>
   </tr>
   <tr>
    <td colspan="4"><hr /></td>
    </tr>
  <tr>
  	<td><div align="right"><span style = "font-weight: bold;">Außendienst</span></div></td>
  	<td><div align="left">
  <?php
		// Abfrage der Außendienstler
	  		$sql = "SELECT user FROM user WHERE (gruppen_id = '3' OR gruppen_id = '5') AND user != 'ZZ' ORDER BY user ASC";
  			//$sql = "SELECT user FROM user WHERE gruppen_id = '3' AND user != 'ZZ' ORDER BY user ASC";
			$query_user = myqueryi($db, $sql);
		
	  	// Darstellung der Außendienstler als SELECT-Liste
	 
			echo "<select name=\"user\" class = \"eingabe\">";
			echo " <option></option>";			 						// erste Zeile als Leerzeile, falls noch kein Telefonist ausgewählt wurde
			for ($j = 0; $j < mysqli_num_rows($query_user); $j++)	{	// Anzahl der Datensätze
				$zeile = mysqli_fetch_row($query_user);					// Schleife für Daten-Zeilen
					if ($zeile[0] == $user1) {							// Anzeige des gewählten Users, falls Fehler bei der Datumseingabe auftreten
						echo " <option selected>$zeile[0]</option>";
					}
					else {
						echo " <option>$zeile[0]</option>";
					}
			}
	  		echo "<select>";
		?>
		</div>
	</td>
    <td width="80"><div align="right"><span style = "font-weight: bold;">Startdatum:</span></div></td>
    <td><div align="left">
		<input type="text" class="eingabe" name="starttag" size = "2" maxlength ="2" value = "<?php echo "$starttag1"; ?>"/>&nbsp;.&nbsp;
		<input type="text"  class="eingabe" name="startmonat" size = "2" maxlength ="2" value = "<?php echo "$startmonat1"; ?>"/>&nbsp;.&nbsp;
		<input type="text"  class="eingabe" name="startjahr" size = "2" maxlength ="2" value = "<?php echo "$startjahr1"; ?>"/>
		</div>
	</td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>&nbsp;</td>
	<td width="80"><div align="right"><span style = "font-weight: bold;">Endedatum:</span></div></td>
    <td><div align="left">
		<input type="text"  class="eingabe" name="endetag" size = "2" maxlength ="2" value = "<?php echo "$endetag1"; ?>"/>&nbsp;.&nbsp;
		<input type="text"  class="eingabe" name="endemonat" size = "2" maxlength ="2" value = "<?php echo "$endemonat1"; ?>"/>&nbsp;.&nbsp;
		<input type="text"  class="eingabe" name="endejahr" size = "2" maxlength ="2" value = "<?php echo "$endejahr1"; ?>" />
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="4"><hr /></td>
  </tr>
<td colspan ="4" align = "center" height="30"><input type="submit" name="speichern" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Auswertung starten&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" class = "submitt">
</tr>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</form>
</div>
</body>
</html>