<?php

// -----------------------------------------------------------------------------------------------------//
// Dieses Script stellt ein Eingabeformular zur Anzeige der Termin-�bersicht f�r Au�endienstler oder	//
// Telefonist zur Verf�gung.																			//
// Die Eingaben werden verifiziert und dann per GET an uebersicht_anzeige.PHP �bergeben					//
// -----------------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

// Feststellen der Benutzergruppe des USERS -------------------
$gruppe = $_SESSION['benutzer_gruppen'];

// Definition der im Formular verwendeten Variablen ------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------

$speichern = $_POST["speichern"];
$tag = quote_smart($_POST["a_tag"]);								// Auswerte-Start-Tag
$monat = quote_smart($_POST["a_monat"]);							// Auswerte-Start-Monat
$jahr = quote_smart($_POST["a_jahr"]);								// Auswerte-Start-Jahr
$heute = $_POST["heute"];											// Auswertung ab HEUTE
$aq = $_POST["aq"];												// Aquise
$ad = $_POST["ad"];												// Aussendienst
$periode = quote_smart($_POST["periode"]);							// Anzahl der anzuzeigenden Tage

// speichern gedrückt ------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------

if (isset($speichern)) {

	unset($fehler);															// Fehlermeldung zurückgesetzt

	if (empty($aq) AND (empty($ad))) { 												// kein user gestetzt
		$fehler = "Bitte wählen Sie eine Mitarbeitergruppe/einen Mitarbeiter!";
	} 																				// ende kein user gesetzt
	
	if (!empty($aq) AND (!empty($ad))) { 											// beide Mitarbeitergruppen gesetzt
			$fehler = "Sie können nur eine Mitarbeitergruppe bearbeiten!!!";
	} 																				// Ende beide Mitarbeitergruppen gesetzt
	
	
	// ----------------------------------------------------------------------------------------------------------
	// heute angeklickt  ----------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------

	if (isset($heute)) {														// heute gesetzt
	
		if (empty($periode)) {													// keine Periode eingegeben
			$zahl = "14";														// Standard-Anzeigezeitraum - 14 Tage
		}
		else {																	//periode eingegeben
	
			$muster = "/^\d{1,2}$/";											// Test Datum, genau 2 Ziffern
			if(!preg_match($muster, $periode)) {								// Muster Periode passt nicht
				$fehler = "Fehler: Das ist kein gültiger Anzeigebereich!!";
			}
			
			else {																//Muster periode passt
					
				if ($periode > 31) {											// periode zu groß
					$fehler = "Der Anzeigezeitraum ist zu groü!!";
				}
				else { 															//Periode liegt im Wertebereich
						
					$zahl = $periode;											// wie viele Tage sollen angezeigt werden - Zählung beginnt bei Null, deshalb -1
						
				} 																// ende else Periode liegt im Wertebereich
			} 																	// ende else Muster periode passt
		} 																		// ende else periode eingegeben
		
		$start = date("Ymd");
		$termin = mktime(0,0,0,date("m"), date("d")+$zahl);
		$ende = (strftime("%Y", $termin)) .(strftime("%m", $termin)) . (strftime("%d", $termin));
		
	} 																			// ende if heute gesetzt
	
	// ----------------------------------------------------------------------------------------------------------
	// termin eingegeben ----------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------

	else { 																		// termineingabe erwartet
	
		if (empty($tag) OR empty($monat) OR empty($jahr)) {
			
			$fehler = "Sie müssen einen Start-Termin eingeben!!";
		
		} 																		// ende if keine termin eingegeben
		
		else {																	// termin eingegeben
		
		$muster = "/^\d{2}$/";													// Test Datum, genau 2 Ziffern
			if(!preg_match($muster, $tag) OR !preg_match($muster, $monat) OR !preg_match($muster, $jahr)) {	// Muster Datum passt nicht
				$fehler = "Fehler: Das ist kein gültiges Datum!!";
			}
			else {  															// Muster Datum stimmt		
				if (empty($periode)) {											// keine Periode eingegeben
					$zahl = "14";												// Standard-Anzeigezeitraum - 14 Tage
					$start = "20" . $jahr . $monat . $tag;
					$jahr = "20" . $jahr;
					
					$offset = $zahl * 86400; 									// = Zahl (Tage) * Sekunden/tag
					$termin = mktime(0,0,0,$monat, $tag, $jahr);
					$termin = $termin + $offset;
					$ende = (strftime("%Y", $termin)) .(strftime("%m", $termin)) . (strftime("%d", $termin));
				}
				else {															//periode eingegeben
	
					$muster = "/^\d{1,2}$/";									// Test Periode, max 2 Ziffern
					if(!preg_match($muster, $periode)) {						// Muster Periode passt nicht
						$fehler = "Fehler: Das ist kein gültiger Anzeigebereich!!";
					}
			
					else {														//Muster periode passt
					
						if ($periode > 31) {									// periode zu groß
							$fehler = "Der Anzeigezeitraum ist zu groß!!";
						}
						else { 													//Periode liegt im Wertebereich
						
							$zahl = $periode;									// wie viele Tage sollen angezeigt werden - Zählung beginnt bei Null, deshalb -1
							$start = "20" .$jahr . $monat . $tag;
							$jahr = "20" . $jahr;
					
							$offset = $zahl * 86400; 							// = Zahl (Tage) * Sekunden/tag
							$termin = mktime(0,0,0,$monat, $tag, $jahr);
							$termin = $termin + $offset;
							$ende = (strftime("%Y", $termin)) .(strftime("%m", $termin)) . (strftime("%d", $termin));
						
						} 														// ende else Periode liegt im Wertebereich
					} 															// ende else Muster periode passt
				} 																// ende else periode eingegeben
			} 																	// ende Muster Datum stimmt
		} 																		// ende else termin eingegeben
	} 																			// ende else termineingabe erwartet
	
	$span = $zahl + 1 ;															// Anzahl der anzuzeigenden Tage + eine Zelle für die Uhrzeit
	
	if (!isset($fehler)) {														// kein Fehler bei der Eingabe
	
	// Debugging ------------------------------------------
	/*
	echo "Periode: $zahl<br />";
	echo "AD: $ad<br />";
	echo "AQ: $aq<br />";
	echo "Start: $start<br />";
	echo "Ende: $ende<br />";
	*/
	// ----------------------------------------------------
	
	// Sprung zur Anzeige!!!!
	echo "<script>location.href='uebersicht_anzeige.php?start=$start&ende=$ende&zahl=$zahl&ad=$ad&aq=$aq'</script>";

} // ende if kein Fehler bei der Eingabe

} // ende if isset speichen

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Termin-Übersicht</title>
	<!-- allgemein/uebersicht_input.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<form name="startdaten" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
<table width="580" border="0" cellpadding="2" cellspacing="2">
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%"  border="0" cellspacing="5" cellpadding="5"  bgcolor="#eeeeee">
<?php
	// Fehlerausgabe eine Zeile ---------------------------------------------------

	if ($fehler) { echo "<tr><td colspan = \"2\" align = \"center\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold; line-height: 200%\">$fehler</span></td></tr>"; }

	// Überschrift für eingeloggten Telefonisten ---------------------------------
	
	if ($gruppe === 'Aquise') {
		echo "<tr bgcolor=\"#006699\">";
  			echo "<td colspan=\"2\"><div align = \"center\"><span style = \"color: white; font-size: 12pt; font-weight: bold; line-height: 200%;\">Anzeige Termine für alle Außendienstler</span></div></td>";
		echo "</tr>";
	}
	
	// Überschrift für eingeloggten Admin -----------------------------------------
	
	elseif ($gruppe === 'Administrator') {
		echo "<tr bgcolor=\"#006699\">";
  			echo "<td colspan=\"2\"><div align = \"center\"><span style = \"color: white; font-size: 12pt; font-weight: bold; line-height: 200%;\">Anzeige Termine für alle Außendienstler/Telefonisten</span></div></td>";
		echo "</tr>";
	}

?>

<!-- Beginn der Dateneingabe 1. Zeile Kennzeichnung der Daten je Spalte -->
<tr>
	<td valign = "top" width = "50%" bgcolor = "#ffffcc"><div align = "center"><span style = "font-weight: bold; font-size:10pt; line-height: 200%;">Auswertezeitraum</span></div><br />
        <span style = "font-size: xx-small;">(Standard ist ein Zeitraum von 14 Tagen<br /> ab Startdatum - Max. sind 31!)</span>
	</td>
	<td valign = "top"  bgcolor = "#ccffff"><div align = "center"><span style = "font-weight: bold; font-size:10pt; line-height: 200%;">Mitarbeitergruppe/<br />Mitarbeiter</span></div>
	</td>
</tr>

<!-- 2. Zeile Dateneingabe: Spalte 1 - Termin, Spalte 2 - Außendienstler -->

<tr>
	<td nowrap  bgcolor = "#ffffcc">
		<span style = "font-weight: bold;">ab heute</span>&nbsp;
			<?php	if (isset($heute)) {
					echo "<input type=\"checkbox\" name=\"heute\" checked />";
				}
				else {
					echo "<input type=\"checkbox\" name=\"heute\" />";
				}
			?>
		<span style = "font-weight: bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;max</span>&nbsp;
   <?php
				echo "<input type=\"text\" name=\"periode\" size = \"2\" maxlength = \"2\" value = \"$periode\" />";
			?>
				&nbsp;Tage
	</td>
	<td bgcolor = "#ccffff"><span style = "font-weight: bold; font-size: 9pt; line-height: 150%;">Außendienst:&nbsp;</span>
	     <select name="ad" class = "eingabe">
	 <?php
		   	//$sql = "SELECT user FROM user ORDER BY user ASC";
	 		//$sql = "SELECT user FROM user WHERE (gruppen_id = '3' OR gruppen_id = '5') AND user != 'ZZ' ORDER BY user ASC";
			$sql = "SELECT user FROM user WHERE (gruppen_id = '3' OR gruppen_id = '5') AND user != 'ZZ' ORDER BY user ASC";
			$query = myqueryi($db, $sql);
			
			echo " <option></option>";
			
			if (isset($speichern) AND (!empty($ad))) {										// Ausgabe Select_feld nach Speichen und Auswahl Telefonist
			
				if ($ad == 'alle') {
					echo " <option selected>alle</option>";
				}
				else {
					echo " <option>alle</option>";
				}
				for ($j = 0; $j < mysqli_num_rows($query); $j++)	{				// Anzahl der Datensätze
					$zeile = mysqli_fetch_row($query);							// Schleife für Daten-Zeilen
					if ($zeile[0] == $ad) {
						echo " <option selected>$zeile[0]</option>";
					}
					else {
						echo " <option>$zeile[0]</option>";
					}
				}
			}
			else {
			
			
			if ($ad == 'alle') {
				echo " <option selected>alle</option>";
			}
			else {
				echo " <option>alle</option>";
			}
			for ($j = 0; $j < mysqli_num_rows($query); $j++)	{					// Anzahl der Datensätze
				$zeile = mysqli_fetch_row($query);								// Schleife für Daten-Zeilen
				if ($zeile[0] == $ad) {
					echo " <option selected>$zeile[0]</option>";
				}
				else {
					echo " <option>$zeile[0]</option>";
				}
			}
		}
	 	?>
    	</select>
	</td>
</tr>

<!-- 3. Zeile Dateneingabe: Spalte 1 - Termin, Spalte 2 - Telefonist, für eingeloggten Telefonisten ausgeblendet -->

<tr>
	<td nowrap  bgcolor = "#ffffcc">
		<span style = "font-weight: bold;">oder</span>&nbsp;ab&nbsp;
			<?php	if (isset($heute)) {
					echo "<input type=\"text\"  class=\"eingabe\" name=\"a_tag\" size = \"2\" maxlength = \"2\" value = \"\"/>";
					echo ".";
					echo "<input type=\"text\"  class=\"eingabe\" name=\"a_monat\" size = \"2\" maxlength = \"2\" value = \"\"/>";
					echo ".";
					echo "<input type=\"text\"  class=\"eingabe\" name=\"a_jahr\" size = \"2\" maxlength = \"2\" value = \"\"/>";
				}
				else {
					echo "<input type=\"text\"  class=\"eingabe\" name=\"a_tag\" size = \"2\" maxlength = \"2\" value = \"$tag\"/>";
					echo ".";
					echo "<input type=\"text\"  class=\"eingabe\" name=\"a_monat\" size = \"2\" maxlength = \"2\" value = \"$monat\"/>";
					echo ".";
					echo "<input type=\"text\"  class=\"eingabe\" name=\"a_jahr\" size = \"2\" maxlength = \"2\" value = \"$a_jahr\"/>";
				}
			?>
		
	</td>
			
	<?php	if ($gruppe === 'Administrator') {		// eingeloggter User ist Admin
	
			echo "<td  bgcolor = \"#ccccff\"><span style = \"font-weight: bold; font-size: 9pt; line-height: 150%;\">Telefonist:&nbsp;</span>";
	     	echo "<select name=\"aq\" class = \"eingabe\">";

		   	//$sql = "SELECT user FROM user WHERE gruppen_id = '2' AND user != 'aq-test' ORDER BY user ASC";		// Benutzer aus DB einlesen
			$sql = "SELECT user FROM user WHERE gruppen_id = '2' OR gruppen_id = '1' ORDER BY user ASC";
			$query = myqueryi($db, $sql);
			
			echo " <option></option>";														// leere Select-Zeile
			
			if (isset($speichern) AND (!empty($aq))) {										// Ausgabe Select_feld nach Speichen und Auswahl Telefonist
			
				if ($aq == 'alle') {
					echo " <option selected>alle</option>";
				}
				else {
					echo " <option>alle</option>";
				}
				for ($j = 0; $j < mysqli_num_rows($query); $j++)	{				// Anzahl der Datensätze
					$zeile = mysqli_fetch_row($query);							// Schleife für Daten-Zeilen
					if ($zeile[0] == $aq) {
						echo " <option selected>$zeile[0]</option>";
					}
					else {
						echo " <option>$zeile[0]</option>";
					}
				}
			}
			else {	
																	// Ausgabe Select-Feld im Normalfall
				echo " <option>alle</option>";
				for ($j = 0; $j < mysqli_num_rows($query); $j++)	{				// Anzahl der Datensätze
					$zeile = mysqli_fetch_row($query);							// Schleife für Daten-Zeilen
					if ($zeile[0] == $aq) {
						echo " <option selected>$zeile[0]</option>";
					}
					else {
						echo " <option>$zeile[0]</option>";
					}
				}
			}
	 	    echo "</select>";
			echo "</td>";
		}		
		elseif ($gruppe === 'Aquise') {	// eingeloggter User ist Telefonist
			echo "<td  bgcolor = \"#ccffff\"><span style = \"font-weight: bold; font-size: 9pt; line-height: 150%;\">&nbsp;</span>";
		}
	?>
</tr>
<tr>
	<td colspan = "2" bgcolor = "#ffcccc"><div align="center"><input type="submit" name="speichern" value="Anzeigen" class = "submitt"></div></td>
</tr>
   
</table>
</td></tr></table>
</td></tr>
</table>
</form>
</div>
</body>
</html>