<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];
$ot_neu = $_POST["ot_neu"];
$plz_alt = $_GET["plz"];					// kommt per get von ort_select.php
$ort_alt = $_GET["ort"];					// kommt per get von ort_select.php
$speichern = $_POST["speichern"];			// Speichern neuer Ortsteil
$nein = $_POST["nein"];						// Zurück-Button

$ot_select = $_POST["ot_select"];			// vom Ortsteil-Select-Feld

// Bearbeitung ist abgebrochen  ---------------------------------------------
if (isset($nein)) {							// Leeren-Button gedrückt
	unset($plz_neu);
	unset($ort_neu);
	unset($ot_neu);
	unset($plz_alt);
	unset($ort_alt);
}
// ---------------------------------------------------------------------------

if (isset($speichern)) {																		// Speichern-Button wurde gedr�ckt

	$plz_neu = $_POST["plz_alt"];
	$ort_neu = $_POST["ort_alt"];

	if (empty($ot_neu) AND empty($ot_select)) { $fehler = "Fehler: Sie müssen einen Namen für den Ortsteil eingeben!"; }
	
	elseif (empty($ort_neu) OR empty($plz_neu)) { $fehler = "Fehler: Sie müssen links einen Ort auswühlen!"; }
	
	else { 	// neuer Ortsteil eingegeben -----------------------------------------------------------------------------------------------------------
	
		if (!empty($ot_neu)) {	$ortsteil_neu = quote_smart($ot_neu); }
		else {$ortsteil_neu = $ot_select; }
		
		$sql  = "SELECT poo_id ";
		$sql .= " FROM poo, plz, ort, ortsteil ";
		$sql .= " WHERE poo.ort_id = ort.ort_id AND ort.ort = '$ort_neu' ";
		$sql .= " AND poo.plz_id = plz.plz_id AND plz.plz = '$plz_neu' ";
		$sql .= " AND poo.ortsteil_id = ortsteil.ortsteil_id AND ortsteil.ortsteil = '$ortsteil_neu' ";
		$abfrage = myqueryi($db, $sql);
		
		if (mysqli_num_rows($abfrage) != 0) {
			$fehler = "Fehler: Diesen Ortsteil gibt es schon für diesen Ort!";
		}
		else {

			$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil = '$ortsteil_neu'";
			$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil_neu')";
			$ortsteil_id = id_ermitteln($db, $sql1, $sql2);
			
			$sql = "SELECT plz_id FROM plz WHERE plz = '$plz_neu'";
			$abfrage = myqueryi($db, $sql);
			$plz_id = mysqli_fetch_array($abfrage);
			
			$sql = "SELECT ort_id FROM ort WHERE ort = '$ort_neu'";
			$abfrage = myqueryi($db, $sql);
			$ort_id = mysqli_fetch_array($abfrage);
			
			$sql = "INSERT INTO poo (plz_id, ort_id, ortsteil_id) VALUES ('$plz_id[0]', '$ort_id[0]', '$ortsteil_id')";
			$abfrage = myqueryi($db, $sql);
			$poo_id = mysqli_insert_id($db);
			
			$fehler = "Der Ortsteil $ortsteil_neu wurde unter ID: $poo_id hinzugefügt";
		}
	}
	
	flush();				// Ausgabepuffer auf den Bildschirm schreiben
	usleep(500000);			// 0,5 Sekunden warten, dann Refresh
	
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	//echo "<script>onload=parent['ortlinks'].location.href='ort_select.php'</script>";
	//echo "<script>onload=parent['ortsteile'].location.href='ortsteile.php?plz=$plz_neu & ort=$ort_neu'</script>";
	//echo "<script>onload=location.href='ort_bearbeiten.php'</script>";
	
}	// Ende IF ISSET speichern

// Start Script-Ablauf vor dem Dr�cken des Speichern-Buttons - Auslesen der Ortsteile zum gewählten Ort
$sql  = "SELECT poo_id AS ID, ortsteil AS Ortsteil, poo.ortsteil_id ";
$sql .= " FROM poo, plz, ort, ortsteil ";
$sql .= " WHERE poo.ort_id = ort.ort_id AND ort.ort = '$ort_alt' ";
$sql .= " AND poo.plz_id = plz.plz_id AND plz.plz = '$plz_alt' ";
$sql .= " AND poo.ortsteil_id = ortsteil.ortsteil_id ";
$sql .= " AND poo.ortsteil_id > '1' ";
$sql .= " ORDER BY Ortsteil ASC ";
$ergebnis = myqueryi($db, $sql);

?>

<!-- Hier f�ngt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Ortsteile</title>
	<!-- allgemein/orte/ortsteile.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
     		<tr>
        		<td>
					<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
					
          				<tr bgcolor="moccasin"><td colspan="3" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Ortsteile eingeben/bearbeiten: <?php echo "<span style=\"color:red;\">$plz_alt&nbsp;$ort_alt</span>"; ?></span></td></tr>
		 
		 				<?php if ($fehler) {
 								echo "<tr bgcolor=\"red\">";
        						echo "<td colspan=\"3\" align = \"left\" valign = \"middle\">";
								echo "<span style=\"font-weight:bold; color:white; line-height:150%;\">$fehler</span><br>";
								echo "</td></tr>";
							}
						?>
<!-- Start Formular für neuen Ortsteil ++++++++++++++++++++++++++++++++++ // -->
<form name="tarif_neu" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
<?php
	echo "<input type=\"hidden\" name=\"plz_alt\" value = \"$plz_alt\">";
	echo "<input type=\"hidden\" name=\"ort_alt\" value = \"$ort_alt\">";
?>

	<tr>
		<td align="left" valign = "middle">Ortsteil eingeben:</td>
		<td align="left" valign = "middle"><input type="text" name="ot_neu" size="25" maxlength="25" value="<?php echo"$ortsteil_neu";?>"></td>
		<td align="left" valign = "middle"><input type="reset" name="nein" value="Leeren" class="correct"></td>
	</tr> 
	<tr>
		<td align="left" valign = "middle">Ortsteil auswählen:</td>
		<td align="left" valign = "middle">
			<?php 	$sql  = "SELECT ortsteil FROM ortsteil ORDER BY ortsteil ASC ";
				$query = myqueryi($db, $sql);
			
				echo "<select name=\"ot_select\">";
				while ($ortsteile = mysqli_fetch_array($query)) {
					if ($ot_select == $ortsteile[0]) { echo "<option selected>$ortsteile[0]</option>"; }
					else { echo "<option>$ortsteile[0]</option>"; }
				}
				echo "</select>";			
			?>
		</td>
		<td align="left" valign = "middle"><input type="submit" name="speichern" value="Speichern" class="submitt"></td>
	</tr>     
</form>
<!-- Ende Formular für neuen Ortsteil ++++++++++++++++++++++++++++++++++ // -->
</td>
</tr>
<?php

if (mysqli_num_rows($ergebnis) !=0) {

	echo "<tr><td valign = \"top\" colspan=\"3\">";
	
	//Ausgabe der bereits vorhandenen Ortsteile +++++++++++++++++++++++++++++++++++++++++++++++
	
	if ($gruppe == "Administrator") {
	
		echo "<table id=\"ausgabe\" cellspacing=\"4\" width = \"100%\">";

		echo "<tr><td colspan = \"6\">Bereits gespeicherte Ortsteile:</td></tr>";
	
		echo "<tr>";													// Tabellenkopf
			echo "<td>ID</td>";
			echo "<td>PLZ</td>";
			echo "<td>Ort</td>";
			echo "<td><span style = font-weight:bold;>Ortsteil</span></td>";	
			echo "<td>&nbsp;</td>";										// eine Zelle für den BEARBEITEN-Button angehängt
			echo "<td>&nbsp;</td>";										// eine Zelle für den LöSCHEN-Button angehängt
		echo "</tr>\n";													// Tabellenkopf Ende
	
		$z=0;  															//zähler der datensätze für bg_colour der zeilen
		$bg1 = "#eeeeee"; 												//die beiden hintergrundfarben
		$bg2 = "#dddddd";

		for ($j = 0; $j < mysqli_num_rows($ergebnis); $j++) {			// Anzahl der Datensätze
		
			$zeile = mysqli_fetch_row($ergebnis);						// Schleife für Daten-Zeilen
			
			$bg=($z++ % 2) ? $bg1 : $bg2;
			
			echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
				echo "<td width = \"20\">" . $zeile[0] . "</td>";
				echo "<td >" . $plz_alt . "</td>";
				echo "<td >" . $ort_alt . "</td>";
				echo "<td ><span style = font-weight:bold;>" . $zeile[1] . "</span></td>";
  				echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"ot_bearbeiten.php?poo=$zeile[0]&plz=$plz_alt&ort=$ort_alt&ortsteil=$zeile[1]\" target=\"_self\">
					  <img src=\"../../images/bearbeite.png\" alt=\"bearbeiten\" title=\"bearbeiten\" border=\"0\" /></a></td>";
				echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"ot_loeschen.php?poo=$zeile[0]&plz=$plz_alt&ort=$ort_alt&ortsteil=$zeile[1]\" target=\"_self\">
					  <img src=\"../../images/loesche.png\" alt=\"loeschen\" title=\"loeschen\" border=\"0\" /></a></td>";
			echo "</tr>";
   	 	}
		echo "</table>";
	}	// ende Adminsitrator
	elseif ($gruppe == "Aquise") { // Telefonist
	
		echo "<table id=\"ausgabe\" cellspacing=\"4\" width = \"100%\">";

		echo "<tr><td colspan = \"5\">Bereits gespeicherte Ortsteile:</td></tr>";
	
		echo "<tr>";													// Tabellenkopf
			echo "<td>ID</td>";
			echo "<td>PLZ</td>";
			echo "<td>Ort</td>";
			echo "<td><span style = font-weight:bold;>Ortsteil</span></td>";	
			echo "<td>&nbsp;</td>";										// eine Zelle für den BEARBEITEN-Button angehängt
		echo "</tr>\n";													// Tabellenkopf Ende
	
		$z=0;  															//zähler der datensätze für bg_colour der zeilen
		$bg1 = "#eeeeee"; 												//die beiden hintergrundfarben
		$bg2 = "#dddddd";

		for ($j = 0; $j < mysqli_num_rows($ergebnis); $j++) {			// Anzahl der Datensätze
		
			$zeile = mysqli_fetch_row($ergebnis);						// Schleife für Daten-Zeilen
			
			$bg=($z++ % 2) ? $bg1 : $bg2;
			
			echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
				echo "<td width = \"20\">" . $zeile[0] . "</td>";
				echo "<td >" . $plz_alt . "</td>";
				echo "<td >" . $ort_alt . "</td>";
				echo "<td ><span style = font-weight:bold;>" . $zeile[1] . "</span></td>";
  				echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"ot_bearbeiten.php?poo=$zeile[0]&plz=$plz_alt&ort=$ort_alt&ortsteil=$zeile[1]\" target=\"_self\">
					  <img src=\"../../images/bearbeite.png\" alt=\"bearbeiten\" title=\"bearbeiten\" border=\"0\" /></a></td>";
			echo "</tr>";
   	 	}
		echo "</table>";
	}	// ende Telefonist
	
// Ende Ausgabe vorhandene Ortsteile +++++++++++++++++++++++++++++++++++++++++++++++
?>
</table>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>
</table>
</td></tr>
</table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>
<?php
}
$close = mysqli_close($db);
?>