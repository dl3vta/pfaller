<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script können Orte komplett gelöscht werden										//
// nur ein Administrator darf das Script nutzen													//
// Daten (PLZ und Ort kommen von ort_bearbeiten.php												//
// vor dem Löschen erfolgt eine Sicherheitsabfrage												//
// es werden alle Kunden und Termine gelöscht													//
// falls PLZ und Ort nicht mehr verwendet werden, werden sie ebenfalls gelöscht					//
// nach dem Löschen wird eine Meldung über Erfolg oder Misserfolg der Löschaktion ausgegeben	//
// danach erfolgt ein Refresh von ort_select, ortsteile und ort_bearbeiten.php					//
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();

$gruppe = $_SESSION['benutzer_gruppen'];						// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php

//  Feststellen, ob Benutzer überhaupt löschen darf --------------------------------------------------------------

if ($gruppe != "Administrator") {
	echo "<script>location.href='../forbidden.php'</script>";	// kein Administrator - Zugriff nicht erlaubt
}

// ----------------------------------------------------------------------------------------------------------------

// POST- und GET-Variablen ----------------------------------------------------------------------------------------

$ja = $_POST["ja"];									// Ja-Button, Daten sollen gelöscht werden
$nein = $_POST["nein"];								// Nein-Button, Löschen wird abgebrochen

$plz = $_GET["plz"];									// PLZ kommt vom ort_bearbeiten.php
$ort = $_GET["ort"];									// Ort kommt vom ort_bearbeiten.php

$plz1 = $_POST["plz"];									// PLZ kommt als hidden vom Script
$ort1 = $_POST["ort"];									// Ort kommt als hidden vom Script

// ----------------------------------------------------------------------------------------------------------------

// Debugging -------------------------------------
//echo "PLZ: $plz<br />";
//echo "Ort: $ort<br />";

// -----------------------------------------------

// Löschen ist abgebrochen, Rücksprung zu such_check.php ----------------------------------------------------------

if (isset($nein)) {																// Nein-Button gedrückt
	//echo "<script>onload=location.href='ort_bearbeiten.php?plz=$plz&ort=$ort'</script>";
	
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['ortlinks'].location.href='ort_select.php'</script>";
	echo "<script>onload=parent['ortsteile'].location.href='ortsteile.php'</script>";
	echo "<script>onload=location.href='ort_bearbeiten.php'</script>";
}
// ----------------------------------------------------------------------------------------------------------------

// Ort sollen gelöscht werden -----------------------------------------------------------------------------

elseif (isset($ja)) {															// Ja-Button gedrückt

	// Ermittlung, ob noch Kunden vorhanden sind ----------------------------------------------------------------------
	$sql = " SELECT kunden_id FROM kunden, poo, plz, ort, ortsteil ";
	$sql .= " WHERE kunden.poo_id = poo.poo_id AND poo.plz_id = plz.plz_id ";
	$sql .= " AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= " AND ort = '$ort1' AND plz = '$plz1' ";
	$sql .= " ORDER BY kunden_id ASC ";
	$query = myqueryi($db, $sql);
	
	$termine = 0;
	
	if (mysqli_num_rows($query) > 0) {						// Kunden vorhanden
		for ($i=0; $i<mysqli_num_rows($query); $i++) {		// alle Kunden werden durchlaufen
			$ergebnis = mysqli_fetch_array($query);
			
			// Lösche alle Termine für den aktuellen Kunden
			$sql = " DELETE FROM termin WHERE kd_id = '$ergebnis[0]' ";
			$abfrage = myqueryi($db, $sql);
			$anzahl = mysqli_affected_rows($db);
			$termine = $termine + $anzahl;
			
			// Lösche Kunde
			$sql = " DELETE FROM kunden WHERE kunden_id = '$ergebnis[0] LIMIt 1' ";
			$abfrage = myqueryi($db, $sql);
		}	// ende alle Kunden durchlaufen
	}	// ende Kunden vorhanden
	
	// Sicherheitsabfrage, ob alle Kunden gel�scht wurden
	$sql = " SELECT kunden_id FROM kunden, poo, plz, ort, ortsteil ";
	$sql .= " WHERE kunden.poo_id = poo.poo_id AND poo.plz_id = plz.plz_id ";
	$sql .= " AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= " AND ort = '$ort1' AND plz = '$plz1' ";
	$sql .= " ORDER BY kunden_id ASC ";
	$result = myqueryi($db, $sql);
			
	if (mysqli_num_rows($result) > 0) {						// noch Kunden vorhanden
		$fehler = "<br />Es sind noch weitere Kunden vorhanden - bitte das Script noch einmal ausführen!<br />";
	}
	else {	//keine Kunden mehr da
	
		// alle poo_ids für diesen Ort löschen, falls nötig, alle die PLZ, den Ort und evtl. Ortsteile aus der DB löschen
				
			$sql = " SELECT poo.poo_id, ortsteil.ortsteil_id FROM poo, plz, ort, ortsteil ";
			$sql .= " WHERE poo.plz_id = plz.plz_id ";
			$sql .= " AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
			$sql .= " AND ort = '$ort1' AND plz = '$plz1' ";
			$sql .= " ORDER BY poo_id ASC ";
			$result = myqueryi($db, $sql);
				
			for ($j=0; $j<mysqli_num_rows($result); $j++) {								// alle Poos werden durchlaufen
				$resultat = mysqli_fetch_array($result);
				$sql = " DELETE FROM poo WHERE poo_id = '$resultat[0]' LIMIT 1 ";		// lösche poo_id
				$result1 = myqueryi($db, $sql);
					
				// Test, ob der Ortsteil noch irgendwo verwendet wird
				$sql = " SELECT poo_id FROM poo WHERE ortsteil_id = '$resultat[1]' ";
				$pootest = myqueryi($db, $sql);
				if (mysqli_num_rows($pootest) == 0) {									// Ortsteil-ID in keiner weiteren poo - löschen
					$sql = " DELETE FROM ortsteil WHERE ortsteil_id = '$resultat[1]' LIMIT 1";
					$otdelete = myqueryi($db, $sql);
				}
			} // ende for alle poos
				
			// alle poos gelöscht, jetzt Test ob PLZ und Ort noch weiter verwendet werden, wenn nicht - löschen
	
			// Test, ob der Ort noch irgendwo verwendet wird
			$sql = " SELECT poo_id FROM poo, ort WHERE poo.ort_id = ort.ort_id AND ort.ort = '$ort1' ";
			$orttest = myqueryi($db, $sql);
			if (mysqli_num_rows($orttest) == 0) {									// Ort-ID in keiner weiteren poo - löschen
				$sql = " DELETE FROM ort WHERE ort = '$ort1' LIMIT 1";
				$ortdelete = myqueryi($db, $sql);
			}
		
			// Test, ob die PLZ noch irgendwo verwendet wird
		$sql = " SELECT poo_id FROM poo, plz WHERE poo.plz_id = plz.plz_id AND plz.plz = '$plz1' ";
		$plztest = myqueryi($db, $sql);
			if (mysqli_num_rows($plztest) == 0) {									// Ort-ID in keiner weiteren poo - löschen
				$sql = " DELETE FROM plz WHERE plz = '$plz1' LIMIT 1";
				$plzdelete = myqueryi($db, $sql);
			}
	}	// ende else keine Kunden mehr da

?>
	
	<!DOCTYPE html>
<html lang = "de">
<head>
<title>Ort löschen</title>
	<!-- allgemein/orte/ort_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 100px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="400" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
		  <?php	// Ausgabe der Meldung
					if (isset($fehler)) {	// noch Kunden vorhanden
						echo "<tr bgcolor = \"#ff6633\"><td colspan=\"2\"><span style = \"font-weight:bold; color:red; font-size:10pt; line-height:200%;\">$fehler</span></td></tr>";
					}
					else {
						echo "<tr bgcolor = \"#ff6633\"><td colspan=\"2\"><span style = \"font-weight:bold; color:maroon; font-size:10pt; line-height:200%;\"><br />
						Der Ort $plz1 $ort1 wurde gelöscht.<br />
						Alle eventuell vorhandenen Kunden und dazugeh�rigen Termine wurden ebenfalls gelöscht.<br /><br />
						</span></td></tr>";
					}
				?>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php
	flush();				// Ausgabepuffer auf den Bildschirm schreiben
	usleep(3000000);		// 2 Sekunden warten, dann Refresh
	
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['ortlinks'].location.href='ort_select.php'</script>";
	//echo "<script>onload=parent['ortsteile'].location.href='ortsteile.php'</script>";
	echo "<script>onload=location.href='ort_bearbeiten.php'</script>";

} // ende löschen ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


else {	// kein Button gedrückt - Script startet erstmals

// Ermittlung, ob noch Kunden vorhanden sind ----------------------------------------------------------------------
$sql = " SELECT kunden_id, kunden.poo_id FROM kunden, poo, plz, ort, ortsteil ";
$sql .= " WHERE kunden.poo_id = poo.poo_id AND poo.plz_id = plz.plz_id ";
$sql .= " AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
$sql .= " AND ort = '$ort' AND plz = '$plz' ";
$query = myqueryi($db, $sql);
if (mysqli_num_rows($query) > 0) {
	$bemerkung1 = "Für den Ort $plz $ort existieren noch " . mysqli_num_rows($query) . " Kunden. <br />
				Soll der Ort trotzdem gelöscht werden?<br />
				Alle Kunden und deren Termine werden dabei unwiederbringlich gelöscht.<br />
				Bei großen Datenmengen (vielen Kunden) muss der Vorgang u.U. wiederholt werden.<br />";
}
else {
	$bemerkung2 = "Für den Ort $plz $ort existieren keine Kunden mehr.<br />
					Der Ort kann gelöscht werden<br />";
}
?>

<!-- Hier f�ngt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang = "de">
<head>
<title>Ort löschen</title>
	<!-- allgemein/orte/ort_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 100px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="400" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><form name="loeschen" method="post" action="<?php $_SERVER[PHP_SELF] ?>">
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
          <tr bgcolor="#006699"><td colspan="2" align = "center"><span style = "font-size:12pt; color:white; line-height:200%; font-weight:bold;">Ort löschen</span></td></tr>
		  <?php	// Ausgabe der Warnmeldung
					if (mysql_num_rows($query) > 0) {	// noch Kunden vorhanden
						echo "<tr><td colspan=\"2\"><span style = \"color: red; font-weight: bold;\">$bemerkung1</span></td></tr>";
					}
					else {
						echo "<tr><td colspan=\"2\"><span style = \"color: black; font-weight: bold;\">$bemerkung2</span></td></tr>";
					}
					echo "<tr>";
						echo "<td align = \"center\" width = \"50%\"><input type=\"hidden\" name=\"plz\" value=\"$plz\"><input type=\"hidden\" name=\"ort\" value=\"$ort\">
								<input type=\"submit\" name=\"ja\" value=\"LÖSCHEN!\" class = \"ja\"></td>";
            			echo "<td align = \"center\" width = \"50%\"><input type=\"submit\" name=\"nein\" value=\"Zur�ck!\" class=\"nein\"></td>";
					echo "</tr>";
				?>
        </table></form></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php
}	// ende else Script startet erstmalig, kein Button gedrückt
$close = mysqli_close($db);	// DB-Verbindung schließen
?>