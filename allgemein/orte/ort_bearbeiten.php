<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script können Orte komplett gelöscht werden		                                //
// nur ein Administrator darf das Script nutzen												    //
// PLZ und Ort kommen von ort_select.php													    //
// der zu löschende Ort muss durch einen anderen ersetzt werden									//
// die Ortsteile beider angegebener Orte werden gemischt                                        //
// alle betroffenen Kunden werden geupdated                                                     //
// danach werden der alte Ort/PLZ gelöscht                                                      //
// nach dem Löschen wird eine Meldung über Erfolg oder Misserfolg der Löschaktion ausgegeben    //
// danach erfolgt(über einen Link) der Rücksprung zu ort_select.php		                        //
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];			// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php
$bg_fehler = "#ff9966";								// Fehler-Farbe
unset($fehler);										// Fehlerausgabe wird zurückgesetzt

// POST- und GET-Variablen --------------------------------------------------------------------------------------

$ja = $_POST["ja"];								// Ja-Button, Ort soll ersetzt werden
$nein = $_POST["nein"];							// Nein-Button, ersetzen wird abgebrochen
$update = $_POST["update"];						// Ort soll upgedated werden
$delete = $_POST["delete"];						// Ort soll gelöscht werde werden

$plz = $_GET["plz"];  								// von ort_select.php, zur Anzeige in den Textfeldern links
$ort = $_GET["ort"];								// von ort_select.php, zur Anzeige in den Textfeldern links
$plz_alt = $_POST["$plz"];  						// wird als hidden-Feld zur Auswertung übertragen
$ort_alt = $_POST["$ort"];							// wird als hidden-Feld zur Auswertung übertragen

$ort_neu_up = $_POST["ort_neu_up"];				// Ort-alt für Update
$ort_alt_up = $_POST["ort_alt_up"];				// Ort-neu für Update


// Bearbeitung ist abgebrochen, Rücksprung zu ort_select.php -------------------------
if (isset($nein)) {											// Zurück-Button gedrückt
	unset($plz);
	unset($ort);
	unset($plz_alt);
	unset($ort_alt);
	unset($ort_neu_up);
	unset($ort_alt_up);
	unset($_POST["plz_alt"]);
	unset($_POST["ort_alt"]);
	unset($_POST["plz"]);
	unset($_POST["ort"]);
	unset($_GET["plz"]);
	unset($_GET["ort"]);
}
// ------------------------------------------------------------------------------------


// Ort soll komplett gelöscht werden --------------------------------------------------

if (isset($delete)) {
	if (empty($plz) OR empty($ort)) { $fehler = "Sie müssen links einen Ort auswählen!"; }
	else {	echo "<script>onload=location.href='ort_loeschen.php?plz=$plz&ort=$ort'</script>"; }
}

// ------------------------------------------------------------------------------------

// Ort soll ersetzt werden ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------

elseif (isset($ja)) {									// Ja-Button gedrückt

	$muster_plz = "/^\d{5}$/";							// Test PLZ, genau 5 Ziffern
	
	$plz_alt = $_POST["plz_alt"];  			// aus hidden-Feld - zur Verarbeitung
	$ort_alt = $_POST["ort_alt"];				// aus hidden-Feld - zur Verarbeitung
 	$plzort_neu = $_POST["plzort_neu"];  		// aus Formular
 	$plz_neu = $_POST["plz_neu"];  			// aus Formular
	$ort_neu = $_POST["ort_neu"];  			// aus Formular
	$ortsteil_neu = $_POST["ortsteil_neu"];  	// aus Formular
	
	
	// Überprüfung der Eingabewerte --------------------------------------------------------------------------------------------------
	
	if (empty($plz_alt) OR empty($ort_alt)) {			// kein Ort zum  Ersetzen ausgewählt
		$fehler = "Sie müssen zuerst links einen Ort auswählen!";
		$bgalt = $bg_fehler;
	}	// ende Muster PLZ passt nicht
	
	elseif (!empty($plz_neu) AND $plz_neu != 'PLZ!') {				// PLZ eingegeben - muss Ort eingegeben werden, Ortsteil optional
		if(!preg_match($muster_plz, $plz_neu)) {				// Muster PLZ passt nicht
			$fehler = "Das ist keine gültige Postleitzahl!";
			$bg_plz = $bg_fehler;
		}	// ende Muster PLZ passt nicht
		else {													// Muster PLZ passt
			if (empty($ort_neu) OR $ort_neu == 'Ort!') {
				$fehler = "Sie müssen zur Postleitzahl einen Ort eingeben!!";
				$bg_ort = $bg_fehler;
			}
		}	// ende PLZ passt
	}		// ende PLZ eingegeben - muss Ort eingegeben werden, Ortsteil optional
	elseif ($plz_neu == '00000') {
		$fehler = "Keine gültige PLZ/ kein gültiger Ort!";
		$bg_ort = $bg_fehler;
		$bg_plz = $bg_fehler;
	}
	elseif (empty($plzort_neu)) {								// Select Feld nicht ausgewählt
		$fehler = "Sie müssen einen Ort zum Ersetzen auswählen!";
		$bg_plz = $bg_fehler;
		$bg_ort = $bg_fehler;
		$bg_plzort = $bg_fehler;
	}
	else {														// Ende SELECT Feld ausgewählt
		$plz_neu = substr($plzort_neu, 0,5);
		$ort_neu = substr($plzort_neu, 6);
	
	}	// Ende SELECT Feld ausgewählt
	
	if (empty($ortsteil_neu) OR $ortsteil_neu == 'Ortsteil eingeben!') {	// kein Ortsteil eigegeben
		unset($ortsteil_neu);												// Variable Ortsteil_neu wird zerstört!!
	}
	
// Eingabe PLZ=Null und Ort
	if ($plz_neu == "00000" AND $ort_neu !== "Kein!") {
		$fehler = "Keine gültige PLZ/ kein gültiger Ort!";
		$bg_ort = $bg_fehler;
		$bg_plz = $bg_fehler;
	}
	
	
	if ($plz_neu == $plz_alt AND $ort_neu == $ort_alt) { $fehler = "Diese Eingabe ist sinnlos"; }
			
		else {	// Eingabe PLZ und Ort korrekt
			if (empty($fehler)) {
			
			?>

				<!-- Ausgabe solange der Rechner arbeitet -->
				<!DOCTYPE html>
				<html lang = "de">
				<head>
				<title>Ort bearbeiten</title>
					<!-- allgemein/orte/ort_bearbeiten.php -->
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
				<style type="text/css">
				<!--
				body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }
				-->
				</style>
				</head><body><div align = "center">
					<table width="400" border="0" cellspacing="0" cellpadding="3">
 						<tr>
    						<td>
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
      								<tr>
        								<td>
											<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
												<tr bgcolor = "#ff6633">
													<td><span style = "font-weight:bold; color:black; font-size:10pt; line-height:200%;">
														<div align = "center">In Arbeit ...</div><br />
														Bei vielen Kunden kann unter Umständen die maximale Serverlaufzeit
														überschritten werden, in diesem Fall das Programm bitte noch einmal
														ausführen.
														</span>
													</td>
												</tr>
        									</table>
										</td>
      								</tr>
    							</table>
							</td>
  						</tr>
					</table>
				</div></body>
				</html>
<?php
	

	// Überprufung der Eingabewerte beendet
	// Auswertung startet
	
	//	Folgende Fälle sind möglich: ----------------------------------------------
	//	Ort alt ohne Ortsteile
	//		1.		Ort neu schon vorhanden
	//		1.1. 	Ort neu ohne Ortsteile
	//		1.2.	Ort neu mit Ortsteilen
	//		1.3.1.	Ort neu mit eingegebenem Ortsteil - schon vorhanden
	//		1.3.2.	Ort neu mit eingegebenem Ortsteil - neu
	//		2.		Ort neu neu
	//		2.1. 	Ort neu ohne Ortsteile
	//		2.2.1.	Ort neu mit eingegebenem Ortsteil - schon vorhanden
	//		2.2.2.	Ort neu mit eingegebenem Ortsteil - neu
	//	Ort alt mit Ortsteilen
	//		3.		Ort neu schon vorhanden
	//		3.1. 	Ort neu ohne Ortsteile
	//		3.2.	Ort neu mit Ortsteilen
	//		3.3.1.	Ort neu mit eingegebenem Ortsteil - schon vorhanden
	//		3.3.2.	Ort neu mit eingegebenem Ortsteil - neu
	//		4.		Ort neu neu
	//		4.1. 	Ort neu ohne Ortsteile
	//		4.2.1.	Ort neu mit eingegebenem Ortsteil - schon vorhanden
	//		4.2.2.	Ort neu mit eingegebenem Ortsteil - neu
	// --------------------------------------------------------------------------
	
	
// Abfrage des alten Ortes, bei mehr als einer Zeile im query existieren Ortsteile, oder bei einer Zeile wenn ortsteil_id > 1
	
		$sql  = " SELECT poo_id, ort.ort_id, ortsteil.ortsteil_id, plz.plz_id ";
		$sql .= " FROM poo, plz, ort, ortsteil ";
		$sql .= " WHERE poo.ort_id = ort.ort_id AND ort.ort = '$ort_alt' ";
		$sql .= " AND poo.plz_id = plz.plz_id AND plz.plz = '$plz_alt' ";
		$sql .= " AND poo.ortsteil_id = ortsteil.ortsteil_id ";
		$altquery = myqueryi($db, $sql);
		
		// PLZ_id neu ----------------------------------------------------------------------------
		$sql1 = "SELECT plz_id FROM plz WHERE plz = '$plz_neu'";			// PLZ schon vorhanden
		$sql2 = "INSERT INTO plz (plz) VALUES ('$plz_neu')";				// PLZ neu
		$plz_id = id_ermitteln($db, $sql1, $sql2);
					
		// Ort_id neu ----------------------------------------------------------------------------
		$sql1 = "SELECT ort_id FROM ort WHERE ort = '$ort_neu'";			// ORT schon vorhanden
		$sql2 = "INSERT INTO ort (ort) VALUES ('$ort_neu')";				// ORT neu
		$ort_id = id_ermitteln($db, $sql1, $sql2);
		
		
// Ortsteil eingegeben - der GESAMTE alte Ort mit allen Ortsteilen wird zum Ortsteil des neuen Ortes!!-----
		if (!empty($ortsteil_neu)) {
		
			// Ortsteil_neu schon vorhanden?
			$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil = '$ortsteil_neu'";	// Ortsteil schon vorhanden
			$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil_neu')";				// Ortsteil neu
			$ortsteil_id = id_ermitteln($db, $sql1, $sql2);

			// Ermittlung der POO-ID des neuen Ortes
			// Abfrage, ob es diese Poo-ID aus PLZ, neuem Ort und neuem Ortsteil schon gibt
			$sql  = "SELECT poo_id FROM poo   ";
			$sql .= " WHERE ort_id = '$ort_id' AND plz_id = '$plz_id' AND ortsteil_id = '$ortsteil_id' ";
			$pooquery = myqueryi($db, $sql);
				
			// ja, gibt es schon, alle Kunden mit alten Poos auf neue Poo updaten
			if (mysqli_num_rows($pooquery)>0) {
				$array = mysqli_fetch_array($pooquery);
				$poo_neu = $array[0];
			}
					
			// nein, gibt es noch nicht, neue Poo einfügen
			else {
				$sql = "INSERT INTO poo (plz_id, ort_id, ortsteil_id) VALUES ('$plz_id', '$ort_id', '$ortsteil_id') ";
				$query = myqueryi($db, $sql);
				$poo_neu = mysqli_insert_id($db);
			}

			// alle alten Kunden auslesen
			for ($i=0; $i<mysqli_num_rows($altquery); $i++) {
				$kundealt = mysqli_fetch_array($altquery);
							
				$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$kundealt[poo_id]' ";
				$result = myqueryi($db, $sql);
							
				// keine Kunden für poo da - poo löschen
				if (mysqli_num_rows($result) == 0) {
					$sql = " DELETE FROM poo WHERE poo_id = '$kundealt[poo_id]' LIMIT 1 ";
					$delete = myqueryi($db, $sql);
				}
					
				// Kunden in poo vorhanden - updaten
				else {
							
					// Kunden updaten
					for ($j=0; $j<mysqli_num_rows($result); $j++) {
						$kundeneu = mysqli_fetch_array($result);
								
						$sql = " UPDATE kunden SET poo_id = '$poo_neu' WHERE kunden_id = '$kundeneu[kunden_id]' ";
						$result1 = myqueryi($db, $sql);

					}	// ende for Kunden updaten
				}	// ende else Kunden in poo vorhanden - updaten
			}	// ende for alle alten Kunden auslesen
				
			// Test, ob Ort und Ortsteil noch verwendet wird, wenn nicht, löschen
			if (mysqli_num_rows($altquery) > 0) {
				mysqli_data_seek($altquery, 0);
							
				for ($i=0; $i<mysqli_num_rows($altquery); $i++) {
					$kundealt = mysqli_fetch_array($altquery);
						
					//Sicherheitsabfrage, ab alle Kunden upgedated
					usleep(100000);		// 0,1 Sekunden warten
					$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$kundealt[poo_id]' ";
					$result2 = myqueryi($db, $sql);
		
					// keine Kunden mehr da
					if (mysqli_num_rows($result2) == 0) {
						$sql = " DELETE FROM poo WHERE poo_id = '$kundealt[poo_id]' LIMIT 1 ";
						$delete = myqueryi($db, $sql);
					}
					
					else { $fehler = "Es konnten nicht alle Kunden geupdated werden - bitte noch einmal!"; }
						
					// abfrage ob alter OT noch verwendet wird
					$sql = " SELECT poo_id FROM poo WHERE ortsteil_id = '$kundealt[ortsteil_id]' ";
					$query = myqueryi($db, $sql);
								
					if (mysqli_num_rows($query) == 0) {
						$sql = " DELETE FROM ortsteil WHERE ortsteil_id = '$kundealt[ortsteil_id]' LIMIT 1 ";
						$delete = myqueryi($db, $sql);
					}
				}

				// abfrage ob alter Ort noch verwendet wird
				$sql = " SELECT poo_id FROM poo WHERE ort_id = '$kundealt[ort_id]' ";
				$query = myqueryi($db, $sql);
								
				if (mysqli_num_rows($query) == 0) {
					$sql = " DELETE FROM ort WHERE ort_id = '$kundealt[ort_id]'  LIMIT 1 ";
					$delete = myqueryi($db, $sql);
				}
						
				// abfrage ob alte PLZ noch verwendet wird
				$sql = " SELECT poo_id FROM poo WHERE plz_id = '$kundealt[plz_id]' ";
				$query = myqueryi($db, $sql);
								
				if (mysqli_num_rows($query) == 0) {
					$sql = " DELETE FROM plz WHERE plz_id = '$kundealt[plz_id]'  LIMIT 1 ";
					$delete = myqueryi($db, $sql);
				}
			}	// ende if test alter OT noch verwendet
		} // ende if Ortsteil eingegeben
		


// kein Ortsteil eingegeben, Ortsteile von altem und neuem Ort werden gemischt
		else {
			// Ermittlung aller poos für neuen Ort
				$sql  = "SELECT poo_id, ort.ort_id, ortsteil.ortsteil_id, plz.plz_id ";
				$sql .= " FROM poo, plz, ort, ortsteil ";
				$sql .= " WHERE poo.ort_id = ort.ort_id AND ort.ort_id = '$ort_id' ";
				$sql .= " AND poo.plz_id = plz.plz_id AND plz.plz_id = '$plz_id' ";
				$sql .= " AND poo.ortsteil_id = ortsteil.ortsteil_id ";
				$neuquery = myqueryi($db, $sql);
				
				// noch keine poo vorhanden - einfügen
				if (mysql_num_rows($neuquery) == 0) {
					$sql = " INSERT INTO poo (plz_id, ort_id, ortsteil_id) VALUES ('$plz_id', '$ort_id', '1') ";
					$query = myqueryi($db, $sql);
					$pooneu_id = mysqli_insert_id($db);
				}

				// Ermittlung aller poos für neuen Ort nach eventuellem Einfügen eines neuen Ortes
				$sql  = "SELECT poo_id, ort.ort_id, ortsteil.ortsteil_id, plz.plz_id ";
				$sql .= " FROM poo, plz, ort, ortsteil ";
				$sql .= " WHERE poo.ort_id = ort.ort_id AND ort.ort_id = '$ort_id' ";
				$sql .= " AND poo.plz_id = plz.plz_id AND plz.plz_id = '$plz_id' ";
				$sql .= " AND poo.ortsteil_id = ortsteil.ortsteil_id ";
				$neuquery = myqueryi($db, $sql);
				
				// Schleife über alle poos des alten Ortes
				for ($i=0; $i<mysqli_num_rows($altquery); $i++) {
					$kundealt = mysqli_fetch_array($altquery);
					
					// schleife über alle poos des neuen Ortes
					for ($k=0; $k<mysqli_num_rows($neuquery); $k++) {
						$kundeneu = mysqli_fetch_array($neuquery);
						$pooneu = $kundeneu[poo_id];
					
						$treffer = 0; 									// 1 wenn Tarif alt = Tarif neu
						
						// vergleich ortsteil-alt mit ortsteil-neu - stimmt, alle Kunden auslesen und auf neue poo-id updaten
						if  ($kundealt[ortsteil_id] ==  $kundeneu[ortsteil_id]) {
								
							$treffer = 1; 								// 1 wenn Tarif alt = Tarif neu
								
							// Kunden für alte poo_id auslesen
							$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$kundealt[poo_id]' ";
							$result = myqueryi($db, $sql);
							
							// keine Kunden mit poo_id da - poo-id löschen
							if (mysqli_num_rows($result) == 0) {
								$sql = " DELETE FROM poo WHERE poo_id = '$kundealt[poo_id]' LIMIT 1 ";
								$delete = myqueryi($db, $sql);
							}
							
							// Kunden mit poo-id vorhanden - updaten
							else {
								for ($m=0; $m<mysqli_num_rows($result); $m++) {
									$neukunde = mysqli_fetch_array($result);
								
									$sql = " UPDATE kunden SET poo_id = '$kundeneu[poo_id]' WHERE kunden_id = '$neukunde[kunden_id]' ";
									$result1 = myqueryi($db, $sql);
						
								}	// ende for Kunden updaten
							}	// ende else Kunden mit poo_id vorhanden - updaten
							break;	// Schleife verlassen
						}	// ende if vergleich Ortsteil alt neu stimmt
						
					}	// ende for über alle poo-id der neuen Gesellschaft
					
					mysqli_data_seek($neuquery, 0);	// neuquery für nächsten Durchlauf zurückstellen
								
					// keine Übereinstimmung in Ortsteilen gefunden - Ortsteil alt in neuen Ort einfügen (neue poo-id)
					if ($treffer == 0) {
						// neue poo_id für neuen Ort
						$sql = " INSERT INTO poo (plz_id, ort_id, ortsteil_id) VALUES ('$plz_id', '$ort_id', '$kundealt[ortsteil_id]') ";
						$query = myqueryi($db, $sql);
						$pooneu = mysqli_insert_id($db);
									
						// Kunden mit alter poo auslesen
						$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$kundealt[poo_id]' ";
						$result = myqueryi($db, $sql);
							
						// keine Kunden mit alter poo-id da - löschen
						if (mysqli_num_rows($result) == 0) {
							$sql = " DELETE FROM poo WHERE poo_id = '$kundealt[poo_id]' LIMIT 1 ";
							$delete = myqueryi($db, $sql);
						}
							
						// Kunden mit alter poo-id vorhanden - updaten
						else {
							for ($n=0; $n<mysqli_num_rows($result); $n++) {
								$neukunde = mysqli_fetch_array($result);
								
								$sql = " UPDATE kunden SET poo_id = '$pooneu' WHERE kunden_id = '$neukunde[kunden_id]' ";
								$result1 = myqueryi($db, $sql);
							
							}	// ende for Kunden updaten
						}	// ende else Kunden mit poo-id vorhanden - updaten
					}	// ende if keine Übereinstimmung in Ortsteilen gefunden - Ortsteil alt in neuen Ortt einfügen (neue poo-id)
				}	// ende for schleife alter Ort
				
				
			// Test, ob Ort und Ortsteil noch verwendet wird, wenn nicht, löschen
			if (mysqli_num_rows($altquery) > 0) {
				mysqli_data_seek($altquery, 0);
				
				// alte poos und Ortsteile löschen
				for ($i=0; $i<mysqli_num_rows($altquery); $i++) {
					$kundealt = mysqli_fetch_array($altquery);
						
					//Sicherheitsabfrage, ab alle Kunden upgedated
					usleep(100000);		// 0,1 Sekunden warten
					$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$kundealt[poo_id]' ";
					$result2 = myqueryi($db, $sql);
		
					// keine Kunden mehr da
					if (mysqli_num_rows($result2) == 0) {
						$sql = " DELETE FROM poo WHERE poo_id = '$kundealt[poo_id]' LIMIT 1 ";
						$delete = myqueryi($db, $sql);
					}
					
					else { $fehler = "Es konnten nicht alle Kunden geupdated werden - bitte noch einmal!"; }
						
					// abfrage ob alter OT noch verwendet wird
					$sql = " SELECT poo_id FROM poo WHERE ortsteil_id = '$kundealt[ortsteil_id]' ";
					$query = myqueryi($db, $sql);
								
					if (mysqli_num_rows($query) == 0) {
						$sql = " DELETE FROM ortsteil WHERE ortsteil_id = '$kundealt[ortsteil_id]' LIMIT 1 ";
						$delete = myqueryi($db, $sql);
					}
				}
				
				// abfrage ob alter Ort noch verwendet wird
				$sql = " SELECT poo_id FROM poo WHERE ort_id = '$kundealt[ort_id]' ";
				$query = myqueryi($db, $sql);
								
				if (mysqli_num_rows($query) == 0) {
					$sql = " DELETE FROM ort WHERE ort_id = '$kundealt[ort_id]'  LIMIT 1 ";
					$delete = myqueryi($db, $sql);
				}
						
				// abfrage ob alte PLZ noch verwendet wird
				$sql = " SELECT poo_id FROM poo WHERE plz_id = '$kundealt[plz_id]' ";
				$query = myqueryi($db, $sql);
								
				if (mysqli_num_rows($query) == 0) {
					$sql = " DELETE FROM plz WHERE plz_id = '$kundealt[plz_id]'  LIMIT 1 ";
					$delete = myqueryi($db, $sql);
				}
			}	// ende if test alter OT noch verwendet
			
		}	// ende else kein Ortsteil eingegeben
	}	// Ende keine Fehler bei der Dateneingabe - Datenverarbeitung
	
/*
// Debugging -------------------------------------------------------//
//while ($daten = mysqli_fetch_array($altquery)) { echo"<br />"; var_dump($daten); echo "<br />"; }
//while ($daten = mysqli_fetch_array($neuquery)) { echo"<br />"; var_dump($daten); echo "<br />"; }
echo "PLZalt: $plz_alt<br />";										//
echo "Ortalt: $ort_alt<br />";										//
echo "PLZneu: $plz_neu<br />";										//
echo "PLZ-IDneu: $plz_id<br />";									//
echo "Ortneu: $ort_neu<br />";										//
echo "Ort-IDneu: $ort_id<br />";										//
echo "select: $plzort_neu<br />";								//
echo "Ortsteil: $ortsteil_neu<br />";							//
echo "OT-ID neu: $ortsteil_id<br />";							//
echo "poo-neu:$pooneu_id<br />";
echo "poo-alt: $kundealt[0]<br />";
echo "kundealt: $kundeneu[0]<br />";							//
//------------------------------------------------------------------//
*/
	
	if (!isset($fehler)) {
		unset($plz);
		unset($ort);
		unset($plz_alt);
		unset($ort_alt);
		unset($ort_neu_up);
		unset($ort_alt_up);
		
		// Refresh der einzelnen Seiten in den entsprechenden Frames
		flush();
		sleep(3);
		echo "<script>onload=parent['ortlinks'].location.href='ort_select.php'</script>";
		echo "<script>onload=location.href='ort_bearbeiten.php'</script>";
	}
	}	// ende else Eingabe PLZ und Ort korrekt
} // ende ja gedr&uuml;ckt
  
if (isset($fehler) OR !isset($ja)) {	// kein Button gedrückt oder Fehler bei der Eingabe - Formular wird angezeigt
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang = "de">
<head>
<title>Ort loeschen</title>
	<!-- allgemein/orte/ort_bearbeiten.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.delete {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #ff0000; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
     		<tr>
        		<td>
					<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
					
          				<tr bgcolor="moccasin"><td colspan="2" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Ort <?php echo "<span style=\"color:red;\">$plz $ort</span>"; ?> bearbeiten</span></td></tr>
		 
		 				<?php if ($fehler) {
 								echo "<tr bgcolor=\"red\">";
        						echo "<td colspan=\"2\" align = \"left\" valign = \"middle\">";
								echo "<span style=\"font-weight:bold; color:white; line-height:150%;\">$fehler</span><br>";
								echo "</td></tr>";
							}
						?>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">

	<?php	//  Feststellen, ob Benutzer überhaupt löschen darf ---------------------------------------------------------------------------
		if ($gruppe == "Administrator") {
	?>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="3" cellspacing="2" bgcolor="#cccccc">
				<form name="loeschen" method="post" action="<?php $_SERVER[PHP_SELF] ?>">
				<tr>
					<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">Der Ort</span></td>
					<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">soll ersetzt werden durch</span></td>
				</tr>
				<tr>
	 <?php
					echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ffffcc\">";
								echo "<table width=\"100%\">";
									echo "<tr>";
										// Anzeige des gewählten Ortes (alt) in Text-Feldern - aber keine Verarbeitung der Felder
										echo "<td align = \"left\"><span style=\"font-weight:bold; font-size:10pt; color:red; line-height:150%;\">
										<input type=\"text\" size=\"5\" maxlength=\"5\" name=\"plz_alt\" value=\"$plz\" span style=\"background-color:$bgalt;\">&nbsp;
										<input type=\"text\" name=\"ort_alt\" value=\"$ort\" span style=\"background-color:$bgalt;\"></span></td>";
									echo "</tr>";
									echo "</tr>";
										echo "<td>";
											// Auslesen der Ortsteile zum gewählten Ort
											$sql  = "SELECT ortsteil ";
											$sql .= " FROM poo, plz, ort, ortsteil ";
											$sql .= " WHERE poo.ort_id = ort.ort_id AND ort.ort = '$ort' ";
											$sql .= " AND poo.plz_id = plz.plz_id AND plz.plz = '$plz' ";
											$sql .= " AND poo.ortsteil_id = ortsteil.ortsteil_id ";
											$sql .= " AND poo.ortsteil_id > '1' ";
											$sql .= " ORDER BY ortsteil ASC ";
											$query = myqueryi($db, $sql);
											if (mysqli_num_rows($query) > 0) {
												echo "Mit den Ortsteilen<br />";
												for ($i = 0; $i < mysqli_num_rows($query); $i++) {
													$ortsteil = mysqli_fetch_array($query);
													echo " - $ortsteil[0]";
												}
											}
										echo "</td>";
									echo "</tr>";
								echo "</table>";
							echo "</td>";
								
				// Zelle rechts - neue PLZ/Orte/Ortsteil				
							echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ccffff\">";
								echo "<table width=\"100%\">";
									echo "<tr>";
										echo "<td colspan = \"2\">";
											// Orte und PLZ werden aus der DB abgefragt
											$sql  = "SELECT DISTINCT plz AS PLZ, ort AS Ort ";
											$sql .= " FROM poo, plz, ort, ortsteil ";
											$sql .= " WHERE poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id AND poo_id > '1'";
											$sql .= " ORDER BY Ort ASC, PLZ ASC  ";
											$ergebnis = myqueryi($db, $sql);
											
											echo "<select name=\"plzort_neu\" style=\"background-color:$bg_plzort\">";
											echo "<option></option>";										// Leerzeile am Anfang
											for ($i = 0; $i < mysqli_num_rows($ergebnis); $i++) {			// Anzahl der Datensätze
												$zeile = mysqli_fetch_array($ergebnis);						// Schleife für Daten-Zeilen
												echo "<option>$zeile[0] $zeile[1]</option>";
											}
	  										echo "</select>";
										echo "</tr>";
										echo "<tr>";
											if (empty($fehler)) {
												echo "<td align = \"left\"><input type=\"text\" size=\"5\" maxlength=\"5\" name=\"plz_neu\" value=\"PLZ!\"></td>";
												echo "<td align = \"left\"><input type=\"text\" name=\"ort_neu\" value=\"Ort!\"></span></td>";
											}
											else {
												echo "<td align = \"left\"><input type=\"text\" size=\"5\" maxlength=\"5\" name=\"plz_neu\" value=\"$plz_neu\" style=\"background-color:$bg_plz\"></td>";
												echo "<td align = \"left\"><input type=\"text\" name=\"ort_neu\" value=\"$ort_neu\" style=\"background-color:$bg_ort\"></span></td>";
											}
										echo "</tr>";
										echo "<tr>";
											echo "<td align = \"left\">&nbsp;</td>";
											if (isset($fehler)) { echo "<td align = \"left\"><input type=\"text\" name=\"ortsteil_neu\" value=\"$ortsteil_neu\" value></span></td>";}
											else { echo "<td align = \"left\"><input type=\"text\" name=\"ortsteil_neu\" value=\"Ortsteil eingeben!\" value></span></td>"; }
										echo "</tr>";
									echo "</table>";
								echo"</td>";
							echo "</tr>";
						echo "<tr>";
							echo "<td colspan = \"2\" align = \"center\">
									<input type=\"hidden\" name=\"plz_alt\" value=\"$plz\">
                  					<input type=\"hidden\" name=\"ort_alt\" value=\"$ort\">
									<input type=\"submit\" name=\"ja\" value=\"Ersetzen!\" class = \"ja\">&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"nein\" value=\"Leeren\" class=\"nein\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"delete\" value=\"LÖSCHEN!\" class = \"delete\">
								<td>";
						echo "</tr>";
					?>
				</form>
			</table>
		</td>
	</tr>
	<?php } // Ende Administrator darf löschen ?>
</table>
</div>
</body>
</html>

<?php
}	// ende else Script startet erstmalig, kein Button gedrückt

$close = mysqli_close($db);	// DB-Verbindung schließen
?>