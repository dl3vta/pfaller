<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];
$plz_neu = $_POST["plz_neu"];
$ort_neu = $_POST["ort_neu"];
$refresh = $_POST_["refresh"];

$bg_fehler = "#ff9966";								// Fehler-Farbe

if (isset($refresh)) {								// Refresh-Button wurde gedrückt
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['ortsteile'].location.href='ortsteile.php'</script>";
	echo "<script>onload=parent['orte'].location.href='ort_bearbeiten.php'</script>";
}

if (isset($speichern)) {							// Speichern-Button wurde gedrückt

	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['ortsteile'].location.href='ortsteile.php'</script>";
	echo "<script>onload=parent['orte'].location.href='ort_bearbeiten.php'</script>";
	
	if (empty($plz_neu) OR (empty($ort_neu))) {								// nicht alle Felder ausgefüllt
		$fehler = "Fehler: Sie müssen PLZ und Ort eingeben!";
		$bginput = $bg_fehler;
	}
	
	else { 	// Felder ausgefüllt--------------------------------------------------------------------------------
	
		$plz_neu = quote_smart($plz_neu);
		$ort_neu = quote_smart($ort_neu);
		
		$sql = "SELECT poo_id FROM poo, ort, plz ";
		$sql .= " WHERE poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND ort.ort = '$ort_neu' AND plz.plz = '$plz_neu'";
		$abfrage = myqueryi($db, $sql);
				
		if (mysqli_num_rows($abfrage) != 0) {
			$fehler = "Fehler: Den Ort $ort_neu mit der PLZ $plz_neu gibt es schon!";
			$bginput = $bg_fehler;
		}
		
		else {
			$sql1 = "SELECT plz_id FROM plz WHERE plz = '$plz_neu'";		// PLZ schon vorhanden?
			$sql2 = "INSERT INTO plz (plz) VALUES ('$plz_neu')";			// PLZ neu
			$plz_neu_id = id_ermitteln($db, $sql1, $sql2);
			
			$sql1 = "SELECT ort_id FROM ort WHERE ort = '$ort_neu'";			// Ort schon vorhanden?
			$sql2 = "INSERT INTO ort (ort) VALUES ('$ort_neu')";			// Ort neu
			$ort_neu_id = id_ermitteln($db, $sql1, $sql2);
	
			$sql = "INSERT INTO poo (plz_id, ort_id, ortsteil_id) VALUES ('$plz_neu_id', '$ort_neu_id', '1') ";
			$abfrage = myqueryi($db, $sql);
			$poo_id = mysqli_insert_id($db);
			
			$fehler = "Der Ort $ort_neu mit der PLZ $plz_neu wurde eingefügt";
		}
	}
}	// Ende IF ISSET speichern

//---------------------------------------------------------------------------------------------------------------------------------------

// Start Script-Ablauf vor dem Drücken des Speichern-Buttons

	$sql  = "SELECT DISTINCT plz AS PLZ, ort AS Ort ";
	$sql .= " FROM poo, plz, ort, ortsteil ";
	$sql .= " WHERE poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id AND poo_id > '1'";
	$sql .= " ORDER BY PLZ ASC, Ort ASC  ";
	$ergebnis = myqueryi($db, $sql);
?>

<!-- Ausgabe bei Start des Programms -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Orte/Ortsteile auswählen</title>
	<!-- allgemein/orte/ort_select.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function FrameAendern (URI, Framename) {
  parent[Framename].location.href = URI;
}
</script>
<style>
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.submitt {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.refresh {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">

<table width="500px" border="0" cellpadding="4" cellspacing="4">
<tr>
	<td>
		<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
			<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
	<?php
					if ($fehler) {
						echo "<tr><td colspan = \"4\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold;\">$fehler</span></td></tr>";
					}
				?>
				<tr>
					<td valign = "top">
						<!-- Start Formular f�r neuen Ort ++++++++++++++++++++++++++++++++++ // -->
						<form name="ort_neu" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
						<table width="100%" cellspacing="5" bgcolor="moccasin">
							<tr>
    							<td colspan = "5" ><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">Eingabe neuer PLZ/Orte</span></td>
    						</tr>
  							<tr>
    							<td>neue PLZ:</td>
    							<td>
								<?php echo "<input type=\"text\" name=\"plz_neu\" size = \"5\" maxlength =\"5\" value=\"$plz_neu\" style=\"background-color:$bginput\">"; ?>
								</td>
								<td>neuer Ort:</td>
    							<td>
								<?php echo "<input type=\"text\" name=\"ort_neu\" size = \"20\" maxlength =\"20\" value=\"$ort_neu\" style=\"background-color:$bginput\">"; ?>
    							<td align="right"><input type="submit" name="speichern" value="Speichern" class = "submitt"></td>
							</tr>
						</table>      
						</form>
						<!-- Ende Formular für neuen Ort ++++++++++++++++++++++++++++++++++ // -->
					</td>
				</tr>

	<?php
					echo "<tr><td valign = \"top\">";
					echo "<table id=\"ausgabe\" cellspacing=\"4\" width = \"100%\">";

					//Ausgabe der bereits vorhandenen Orte --------------------------------------------------------------

					echo "<tr>";
					echo "</tr>";
						echo "<td colspan=\"3\" valign=\"middle\"><span style=\"font-weight:bold; font-size:10pt; color:red; line-height:150%;\">vorhandene Orte:</span></td>";
						echo "<td align=\"right\"><form name=\"reset\" method=\"post\" action=\"$PHP_SELF\"><input type=\"submit\" name=\"refresh\" value=\"REFRESH\" class = \"refresh\"></form></td>";
					
					
					echo "<tr>";													// Tabellenkopf
					for($i = 0; $i < mysqli_num_fields($ergebnis); $i++) {		    // Anzahl der Tabellenzellen
						$row = mysqli_fetch_assoc($ergebnis);
						$spalte = array_keys($row);
						mysqli_data_seek($ergebnis, 0);

						echo "<td align\"left\" valign=\"middle\"><b>$spalte[$i]</b></td>";

					}
					echo "<td align\"center\" valign=\"middle\"><div align=\"center\"><strong>Ort</strong></br>bearbeiten/löschen</div></td>";										// eine Zelle für den Löschen-Button angehängt
					echo "<td align\"center\" valign=\"middle\"><div align=\"center\"><strong>Ortsteile</strong></br>eingeben/bearbeiten</div></td>";										// eine Zelle für den Ortsteil-Button angehängt

					echo "</tr>\n";													// Tabellenkopf Ende
	
					$z=0;  //zähler der datensätze für bg_colour der zeilen
					$bg1 = "#eeeeee"; //die beiden hintergrundfarben
					$bg2 = "#dddddd";	
	
					for ($j = 0; $j < mysqli_num_rows($ergebnis); $j++) {			// Anzahl der Datensätze
						$zeile = mysqli_fetch_row($ergebnis);						// Schleife für Daten-Zeilen
						
						// Abfrage, ob Ortsteile vorhanden sind
						$sql  = "SELECT ortsteil ";
						$sql .= " FROM poo, plz, ort, ortsteil ";
						$sql .= " WHERE poo.ort_id = ort.ort_id AND ort.ort = '$zeile[1]' ";
						$sql .= " AND poo.plz_id = plz.plz_id AND plz.plz = '$zeile[0]' ";
						$sql .= " AND poo.ortsteil_id = ortsteil.ortsteil_id ";
						$sql .= " AND poo.ortsteil_id > '1' ";
						$result = myqueryi($db, $sql);
						
						$bg=($z++ % 2) ? $bg1 : $bg2;
						echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
			
						for($i = 0; $i < mysqli_num_fields($ergebnis); $i++) {		// Schleife für Felder pro Zeile + bearbeiten- + Termin-Link
							echo "<td align\"left\" valign=\"middle\">" . $zeile[$i] . "</td>";
						}
			
  					echo "<td valign = \"middle\" align = \"center\">
						<a href=\"ort_bearbeiten.php?plz=$zeile[0]&ort=$zeile[1]\" target=\"orte\" onclick=\"parent['ortsteile'].location.href='ortsteile.php'\">
				  		<img src=\"../../images/bearbeite.png\" alt=\"bearbeiten\" title=\"bearbeiten\" border=\"0\" /></a></td>";
					
					if (mysqli_num_rows($result) > 0) {
						echo "<td valign = \"middle\" align = \"center\">
						<a href=\"ortsteile.php?plz=$zeile[0]&ort=$zeile[1]\" target=\"ortsteile\" onclick=\"parent['orte'].location.href='ort_bearbeiten.php'\">
				  		<img src=\"../../images/bearbeite.png\" alt=\"bearbeiten\" title=\"bearbeiten\" border=\"0\" /></a></td>";
					}
					else {
						echo "<td align\"center\" valign=\"middle\"><div align=\"center\">
						<a href=\"ortsteile.php?plz=$zeile[0]&ort=$zeile[1]\" target=\"ortsteile\" onclick=\"parent['orte'].location.href='ort_bearbeiten.php'\">
						noch keine</a></div></td>";
					}
				echo "</tr>";
    		}
		echo "</table>";
				// Ende Ausgabe vorhandenene Orte +++++++++++++++++++++++++++++++++++++++++++++++
			?>
		</table>
	</table>
</table>
</td>
</tr>
</table>
</div>
</body>
</html>

<?php $close = mysqli_close($db); ?>