<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script können Ortsteile komplett gelöscht werden									//
// nur ein Administrator darf das Script nutzen													//
// Daten (poo-id) kommen von ortsteile.php														//
// vor dem Löschen erfolgt eine Sicherheitsabfrage												//
// wenn Kunden vorhanden sind (nur für diese poo!!), werden diese auf Ortsteil_id =1 gesetzt	//
// falls der Ortsteil sonst nicht mehr verwendet wird, wird er ebenfalls gelöscht				//
// nach dem Löschen wird eine Meldung über Erfolg oder Misserfolg der Löschaktion ausgegeben	//
// danach erfolgt ein Refresh von ort_select, ortsteile und ort_bearbeiten.php					//
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();

$gruppe = $_SESSION['benutzer_gruppen'];						// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php

//  Feststellen, ob Benutzer überhaupt löschen darf --------------------------------------------------------------

if ($gruppe != "Administrator") {
	echo "<script>location.href='../../forbidden.php'</script>";	// kein Administrator - Zugriff nicht erlaubt
}

// ----------------------------------------------------------------------------------------------------------------

// POST- und GET-Variablen ----------------------------------------------------------------------------------------

$ja = $_POST["ja"];									// Ja-Button, Daten sollen gelöscht werden
$nein = $_POST["nein"];								// Nein-Button, Löschen wird abgebrochen

$poo = $_GET["poo"];									// POO-ID kommt vom ortsteile.php
$plz = $_GET["plz"];									// PLZ kommt vom ortsteile.php
$ort = $_GET["ort"];									// Ort kommt vom ortsteile.php
$ortsteil = $_GET["ortsteil"];							// Ortsteil kommt vom ortsteile.php


// ----------------------------------------------------------------------------------------------------------------

// Debugging -------------------------------------
//echo "poo: $poo<br />";
//echo "PLZ: $plz<br />";
//echo "Ort: $ort<br />";
//echo "Ortsteil: $ortsteil<br />";

// -----------------------------------------------

// Löschen ist abgebrochen, Rücksprung zu ortsteile.php ----------------------------------------------------------

if (isset($nein)) {												// Nein-Button gedrückt
	$plzneu = $_POST["plzneu"];									// PLZ kommt als hidden vom Script, geht zurück an ortsteile.php
	$ortneu = $_POST["ortneu"];									// Ort kommt als hidden vom Script, geht zurück an ortsteile.php
	$poo = $_POST["poo"];										// poo_id-alt kommt als hidden vom Script
	
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	//echo "<script>onload=parent['ortlinks'].location.href='ort_select.php'</script>";
	echo "<script>onload=parent['ortsteile'].location.href='ortsteile.php?plz=$plzneu & ort=$ortneu'</script>";
	//echo "<script>onload=location.href='ort_bearbeiten.php'</script>";
}
// ----------------------------------------------------------------------------------------------------------------

// Ortsteil soll gelöscht werden -----------------------------------------------------------------------------

elseif (isset($ja)) {											// Ja-Button gedrückt

	$plzneu = $_POST["plzneu"];									// PLZ kommt als hidden vom Script, geht zurück an ortsteile.php
	$ortneu = $_POST["ortneu"];									// Ort kommt als hidden vom Script, geht zurück an ortsteile.php
	$poo = $_POST["poo"];										// poo_id-alt kommt als hidden vom Script
	
	// Ermittlung, ob noch Kunden vorhanden sind ----------------------------------------------------------------------
	$sql = " SELECT kunden_id FROM kunden WHERE kunden.poo_id = '$poo' ";
	$query = myqueryi($db, $sql);
	
	if (mysqli_num_rows($query) > 0) {						// Kunden vorhanden

		// Ermittlung der neuen poo_id (mit ortsteil_id = 1)
		$sql  = "SELECT poo_id FROM poo, plz, ort ";
		$sql .= " WHERE poo.ort_id = ort.ort_id AND ort.ort = '$ortneu' ";
		$sql .= " AND poo.plz_id = plz.plz_id AND plz.plz = '$plzneu' ";
		$sql .= " AND poo.ortsteil_id = '1' ";
		$pooquery = myqueryi($db, $sql);
		$pooneu = mysqli_fetch_array($pooquery);

		for ($i=0; $i<mysqli_num_rows($query); $i++) {		// alle Kunden werden durchlaufen
			$ergebnis = mysqli_fetch_array($query);
			
			// UPDATE Kunde
			$sql = " UPDATE kunden SET poo_id = '$pooneu[0]' WHERE poo_id = '$poo' ";
			$abfrage = myqueryi($db, $sql);
		}	// ende alle Kunden durchlaufen
	}	// ende Kunden vorhanden
	
	// Sicherheitsabfrage, ob alle Kunden geupdated wurden
	$sql = " SELECT kunden_id FROM kunden WHERE kunden.poo_id = '$poo' ";
	$result = myqueryi($db, $sql);
			
	if (mysqli_num_rows($result) > 0) {						// noch Kunden vorhanden
		$fehler = "<br />Es sind noch weitere Kunden vorhanden - bitte das Script noch einmal ausführen!<br />";
	}
			
	else {													// alle Kunden geupdated
			
		// poo_id für diese Ortsteil löschen
		$sql = " DELETE FROM poo WHERE poo_id = '$poo' LIMIT 1 ";		// lösche poo_id
		$result = myqueryi($db, $sql);
					
		// Test, ob der Ortsteil noch irgendwo verwendet wird
		$sql = " SELECT poo_id FROM poo, ortsteil WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND ortsteil.ortsteil = '$ortsteil' ";
		$pootest = myqueryi($db, $sql);
		$pooalt=mysqli_fetch_array($pootest);
		if (mysqli_num_rows($pootest) == 0) {									// Ortsteil-ID in keiner weiteren poo - löschen
			$sql = " DELETE FROM ortsteil WHERE ortsteil = '$ortsteil' LIMIT 1";
			$otdelete = myqueryi($db, $sql);
		}
	}

?>
	
	<!DOCTYPE html>
<html lang = "de">
<head>
<title>Ortsteil löschen</title>
	<!-- allgemein/orte/ot_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 100px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="400" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
		  <?php	// Ausgabe der Meldung
					if (isset($fehler)) {	// noch Kunden vorhanden
						echo "<tr bgcolor = \"#ff6633\"><td colspan=\"2\"><span style = \"font-weight:bold; color:red; font-size:10pt; line-height:200%;\">$fehler</span></td></tr>";
					}
					else {
						echo "<tr bgcolor = \"#ff6633\"><td colspan=\"2\" align=\"center\" valign=\"middle\"><span style = \"font-weight:bold; color:maroon; font-size:10pt; line-height:200%;\"><br />
						Der Ortsteil $ortsteil <br />aus $ort ($plz) wurde gelöscht.<br /><br />
						</span></td></tr>";
					}
				?>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php
	flush();				// Ausgabepuffer auf den Bildschirm schreiben
	usleep(3000000);		// 3 Sekunden warten, dann Refresh
	
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['ortlinks'].location.href='ort_select.php'</script>";
	echo "<script>onload=parent['ortsteile'].location.href='ortsteile.php?plz=$plzneu & ort=$ortneu'</script>";
	//echo "<script>onload=location.href='ort_bearbeiten.php'</script>";

} // ende löschen ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


else {	// kein Button gedrückt - Script startet erstmals

// Ermittlung, ob noch Kunden vorhanden sind ----------------------------------------------------------------------
$sql = " SELECT kunden_id FROM kunden WHERE kunden.poo_id = '$poo' ";
$kundenquery = myqueryi($db, $sql);
if (mysqli_num_rows($kundenquery) > 0) {
	$bemerkung1 = "Für den Ortsteil $ortsteil <br />aus $ort ($plz) <br />existieren noch<br /> " . mysqli_num_rows($kundenquery) . " Kunden. <br />
				Soll der Ortsteil trotzdem gelöscht werden?<br />
				Alle betroffenen Kunden verlieren dabei den Ortsteil.<br />
				Bei großen Datenmengen (vielen Kunden) muss der Vorgang u.U. wiederholt werden.<br />";
}
else {
	$bemerkung2 = "Für den Ortsteil $ortsteil <br />aus $ort ($plz) <br />existieren keine Kunden mehr.<br />
					Der Ortsteil kann gelöscht werden<br />";
}
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang = "de">
<head>
<title>Ort löschen</title>
	<!-- allgemein/orte/ot_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 100px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="400" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><form name="loeschen" method="post" action="<?php $_SERVER[PHP_SELF] ?>">
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
          <tr bgcolor="#006699"><td colspan="2" align = "center"><span style = "font-size:12pt; color:white; line-height:200%; font-weight:bold;">Ortsteil "<?php echo "$ortsteil"; ?>" löschen</span></td></tr>
		  <?php	// Ausgabe der Warnmeldung
					if (mysqli_num_rows($kundenquery) > 0) {	// noch Kunden vorhanden
						echo "<tr><td colspan=\"2\" align=\"center\" valign=\"middle\"><span style = \"color: red; font-weight: bold;\">$bemerkung1</span></td></tr>";
					}
					else {
						echo "<tr><td colspan=\"2\" align=\"center\" valign=\"middle\"><span style = \"color: black; font-weight: bold;\">$bemerkung2</span></td></tr>";
					}
					echo "<tr>";
						echo "<td align = \"center\" width = \"50%\">
									<input type=\"hidden\" name=\"poo\" value=\"$poo\">
									<input type=\"hidden\" name=\"plzneu\" value=\"$plz\">
									<input type=\"hidden\" name=\"ortneu\" value=\"$ort\">
								<input type=\"submit\" name=\"ja\" value=\"LÖSCHEN!\" class = \"ja\"></td>";
            			echo "<td align = \"center\" width = \"50%\"><input type=\"submit\" name=\"nein\" value=\"ZurÜck!\" class=\"nein\"></td>";
					echo "</tr>";
				?>
        </table></form></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php
}	// ende else Script startet erstmalig, kein Button gedrückt
?>