<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script können Ortsteile geändert werden								            //
// PLZ und Ort kommen von ort_select.php													    //
// alle betroffenen Kunden werden geupdated                                                     //
// fall nicht mehr verwendet, wird der alte OT gelöscht                                         //
// nach dem Löschen wird eine Meldung über Erfolg oder Misserfolg der Löschaktion ausgegeben    //
// danach erfolgt(über einen Link) der Rücksprung zu ortsteile.php		                        //
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];			// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php
$bg_fehler = "#ff9966";								// Fehler-Farbe
unset($fehler);										// Fehlerausgabe wird zurückgesetzt


// POST- und GET-Variablen ----------------------------------------------------------------------------------------

$ja = $_POST["ja"];								// Ja-Button, Daten sollen gelöscht werden
$nein = $_POST["nein"];							// Nein-Button, ersetzen wird abgebrochen
$update = $_POST["update"];						// Ort soll upgedated werden

$pooalt = $_GET["poo"];							// POO-ID kommt vom ortsteile.php
$plzalt = $_GET["plz"];							// PLZ kommt vom ortsteile.php
$ortalt = $_GET["ort"];							// Ort kommt vom ortsteile.php
$otalt = $_GET["ortsteil"];						// Ortsteil kommt vom ortsteile.php

// ----------------------------------------------------------------------------------------------------------------

// Debugging -------------------------------------
//echo "pooalt: $pooalt<br />";
//echo "PLZalt: $plzalt<br />";
//echo "Ortalt: $ortalt<br />";
//echo "Ortsteilalt: $otalt<br />";
// -----------------------------------------------

$otup = $otalt;

// Bearbeitung ist abgebrochen, Rücksprung zu ortsteile.php -------------------------------------
if (isset($nein)) {											// Leeren-Button gedrückt
	echo "<script>location.href='ortsteile.php?plz=$plzalt&ort=$ortalt'</script>";
}
// ------------------------------------------------------------------------------------


// Update ----------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------

//if (isset($update)) {

if (isset($ja)) {

// Auswertung startet
//	Folgende Fälle sind möglich: ----------------------------------------------
//	1.		Ot neu bereits vorhanden
//	1.1.	OT neu im gleichen PLZ-Gebiet
//	1.1.1. 	Kunden mit altem OT
//	1.1.2	keine Kunden im alten OT
//	1.2.	OT neu in einem anderen PLZ-Gebiet
//	1.2.1. 	Kunden mit altem OT
//	1.2.2	keine Kunden im alten OT
//	2.		Ortsteil neu
// --------------------------------------------------------------------------

	$otneu_select = $_POST["otneuselect"];				// Ortsteil-neu aus Select-Feld
	$otneu_input = $_POST["otneuinput"];				// Ortsteil-neu aus Text-Feld
	
	if (empty($otneu_select) AND (empty($otneu_input) OR ($otneu_input == 'Ortsteil eingeben!'))) {	//nichts eingegeben
		$fehler = "Sie müssen einen Ortsteil eingeben!!";
		$bg_ot = $bg_fehler;
	}

	else {	// Ortsteil eingegeben
		if (!empty($otneu_input) AND ($otneu_input !== 'Ortsteil eingeben!')) { $otneuup = quote_smart($otneu_input); }
		elseif (!empty($otneu_select)) { $otneuup = $otneu_select; }
		$otup = $otneuup;
		
		if ($otneuup == $otalt) {	// der gleiche Ortsteil
			$fehler = "Der Name für den neuen Ortsteil muss sich vom alten unterscheiden!";
		} // ende if der gleiche Ortsteil
		
		else {	// Eingabe erfolgt
		
			$sql = " SELECT poo_id, plz, ort, poo.ortsteil_id ";			// neuer Ortsteil doch schon vorhanden?
			$sql .= " FROM poo, plz, ort, ortsteil ";
			$sql .= " WHERE poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
			$sql .= " AND ortsteil = '$otneuup'";	
			$query = myqueryi($db, $sql);
			
// OT schon vorhanden
			
			if (mysqli_num_rows($query) > '0') {
				$neu = mysqli_fetch_array($query);
				
// Ortsteil im gleichen PLZ-Gebiet
				
				if ($plzalt==$neu[1] AND $ortalt==$neu[2]) {	//Ortsteil im selben PLZ-Gebiet und Ort!
				
// alle Kunden mit der alten poo_id selektieren
					$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$pooalt'";
					$abfrage = myqueryi($db, $sql);
					
// es gibt Kunden mit poo-alt
					
					if (mysqli_num_rows($abfrage) > 0) {
						
// Kunden updaten auf neue poo_id
						for ($i=0; $i < mysqli_num_rows($abfrage); $i++) {
							$kundenid = mysqli_fetch_array($abfrage);
							$sql = " UPDATE kunden SET poo_id = '$neu[0]' WHERE kunden_id = '$kundenid[0]'";
							$query = myqueryi($db, $sql);
						}
						
// Kontrollabruf Kunden mit alter poo-id
						$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$pooalt'";
						$query = myqueryi($db, $sql);
						if (mysqli_num_rows($query)>0) {	// es gibt noch Kunden mit poo-alt
							$fehler = "Es gibt noch Kunden mit altem Ortsteil - bitte noch einmal";
						}	// ende if noch Kunden mit alter poo übrig
						else {	// keine Kunden mehr mit alter poo
// alte poo-id löschen
							$sql = "DELETE FROM poo WHERE poo_id = '$pooalt' LIMIT 1";
							$query = myqueryi($db, $sql);

// Abfrage, ob alter OT sonst noch irgendwo verwendet wird
							$sql = " SELECT poo_id, poo.ortsteil_id ";			// alter OT noch verwendet?
							$sql .= " FROM poo, plz, ort, ortsteil ";
							$sql .= " WHERE poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
							$sql .= " AND ortsteil = '$otalt'";	
							$query = myqueryi($db, $sql);
							
							if (mysqli_num_rows($query) == 0) {	// Ortsteil nirgends mehr verwendet
							
// Lösche Ortsteil komplett aus DB
								$sql = "DELETE FROM ortsteil WHERE ortsteil = '$otalt' LIMIT 1";
								$query = myqueryi($db, $sql);
							}
						}	// ende else keine Kunden mehr mit alter poo
					}	// ende if Kunden mit poo-alt vorhanden
					
// keine kunden mit poo-alt
					
					else {
// poo-alt löschen
						$sql = "DELETE FROM poo WHERE poo_id = '$pooalt' LIMIT 1";
						$query = myqueryi($db, $sql);
							
// Abfrage, ob alter OT sonst noch irgendwo verwendet wird
						$sql = " SELECT poo_id, poo.ortsteil_id ";
						$sql .= " FROM poo, plz, ort, ortsteil ";
						$sql .= " WHERE poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
						$sql .= " AND ortsteil = '$otalt'";	
						$query = myqueryi($db, $sql);
							
						if (mysqli_num_rows($query) == 0) {	// Ortsteil nirgends mehr verwendet
// Lösche Ortsteil komplett aus DB
							$sql = "DELETE FROM ortsteil WHERE ortsteil = '$otalt' LIMIT 1";
							$query = myqueryi($db, $sql);
						}
					}	// ende keine kunden mit poo-alt
				} // ende if gleiches PLZ-Gebiet



// Ortsteil in anderem PLZ-Gebiet----------------------------------------------------------------------------------
				else {	// OT in anderem PLZ Gebiet vorhanden!
// alte poo-id updaten (ortsteil_id_neu)
					$sql = " UPDATE poo SET ortsteil_id = '$neu[3]' WHERE poo_id = '$pooalt'";	//$neu[3] = OT-neu_id
					$query = myqueryi($db, $sql);
					
					// Abfrage, ob alter OT sonst noch irgendwo verwendet wird
					$sql = " SELECT poo_id, poo.ortsteil_id ";
					$sql .= " FROM poo, plz, ort, ortsteil ";
					$sql .= " WHERE poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
					$sql .= " AND ortsteil = '$otalt'";	
					$query = myqueryi($db, $sql);
							
					if (mysqli_num_rows($query) == 0) {	// Ortsteil nirgends mehr verwendet
// Lösche Ortsteil komplett aus DB
						$sql = "DELETE FROM ortsteil WHERE ortsteil = '$otalt' LIMIT 1";
						$query = myqueryi($db, $sql);
					}
				}	// ende else OT in anderem PLZ-Gebiet
			}	// ende if OT schon vorhanden


// neuer Ortsteil noch nicht vorhanden ----------------------------------------------------------------------------
			
			else {	// nein, OT nicht vorhanden
// neuen Ortsteil einfügen
				$sql = "INSERT INTO ortsteil (ortsteil) VALUES ('$otneuup')";			// Ortsteil neu
				$query = myqueryi($db, $sql);
				$otidneu = mysqli_insert_id($db);
	
// alte poo-id updaten (otidneu)
				$sql = " UPDATE poo SET ortsteil_id = '$otidneu' WHERE poo_id = '$pooalt'";
				$query = myqueryi($db, $sql);
				
// Abfrage, ob alter OT sonst noch irgendwo verwendet wird
				$sql = " SELECT poo_id, poo.ortsteil_id ";
				$sql .= " FROM poo, plz, ort, ortsteil ";
				$sql .= " WHERE poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
				$sql .= " AND ortsteil = '$otalt'";	
				$query = myqueryi($db, $sql);
							
				if (mysqli_num_rows($query) == 0) {	// Ortsteil nirgends mehr verwendet
// Lösche Ortsteil komplett aus DB
					$sql = "DELETE FROM ortsteil WHERE ortsteil = '$otalt' LIMIT 1";
					$query = myqueryi($db, $sql);
				}
			}	// ende else OT nicht vorhanden
		}	// ende else Eingabe erfolgt
	}	// ende else Ortsteil eingegeben
	if (empty($fehler)) {
		echo "<script>location.href='ortsteile.php?plz=$plzalt&ort=$ortalt'</script>";
	}

} // Ende IF UPDATE
// ------------------------------------------------------------------------------------


// Debugging -------------------------------------------------------//
	//echo "POO-alt: $pooalt<br />";
	//echo "PLZ-alt: $plzalt<br />";
	//echo "ORT-alt: $ortalt<br />";
	//echo "OT-alt: $otalt<br />";
	//echo "OT-alt-up: $otup<br />";
	//echo "OT-neu-up: $otneuup<br />";
	//echo "PLZalt: $plzalt<br />";
	//echo "ORTalt: $ortalt<br />";
	//echo "OTneu: $ot_neu<br />";
//------------------------------------------------------------------//


  
if (isset($fehler) OR !isset($ja)) {	// kein Button gedrückt oder Fehler bei der Eingabe - Formular wird angezeigt

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang = "de">
<head>
<title>Ort loeschen</title>
	<!-- allgemein/orte/ot_bearbeiten.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}

-->
</style>
</head>
<body>
<div align = "center">

<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
     		<tr>
        		<td>
					<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
					
          				<tr bgcolor="moccasin"><td colspan="2" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Ortsteil <?php echo "<span style=\"color:red;\">$otalt</span> von Ort <span style=\"color:red;\">$plzalt $ortalt</span>"; ?> bearbeiten</span></td></tr>
		 
		 				<?php if ($fehler) {
 								echo "<tr bgcolor=\"red\">";
        						echo "<td colspan=\"2\" align = \"left\" valign = \"middle\">";
								echo "<span style=\"font-weight:bold; color:white; line-height:150%;\">$fehler</span><br>";
								echo "</td></tr>";
							}
						?>

	<tr>
		<td>
			<table width="100%" border="0" cellpadding="3" cellspacing="2" bgcolor="#cccccc">
				<form name="loeschen" method="post" action="<?php $_SERVER[PHP_SELF] ?>">
				<tr>
            		<td colspan = "2" align = "left"><span style="font-size:8pt; line-height:140%;">
						Mit diesem Script können Sie den ausgewählten Ortsteil (links unten) ersetzen.<br />
						Dazu müssen Sie rechts einen Ortsteil auswählen, durch den der ausgewählte Ortsteil ersetzt werden soll. Die Eingabe eines Ortsteiles überschreibt eine Auswahl.<br />
						Eventuell im "alten" Ortsteil vorhandene Kunden werden auf den "neuen" Ortsteil upgedated.
						</span>
					</td>
				</tr>
				<tr>
					<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">Der Ortsteil</span></td>
					<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">soll ersetzt werden durch</span></td>
				</tr>
				<tr>
	 <?php
					echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ffffcc\">";
								echo "<table width=\"100%\">";
									echo "<tr>";
										// Anzeige des gewählten Ortes (alt) in Text-Feldern - aber keine Verarbeitung der Felder
										echo "<td align = \"left\"><span style=\"font-weight:bold; font-size:10pt; color:red; line-height:150%;\"><input type=\"text\" name=\"otalt\" value=\"$otalt\"></span></td>";
									echo "</tr>";
									echo "<tr>";
										echo "<td>&nbsp;</td>";
									echo "</tr>";
								echo "</table>";
							echo "</td>";
								
				// Zelle rechts - neuer Ortsteil				
							echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ccffff\">";
								echo "<table width=\"100%\">";
									echo "<tr>";
										echo "<td colspan = \"2\">";
											// Die Ortsteile werden aus der DB gelesen
											$sql  = "SELECT ortsteil FROM ortsteil ORDER BY ortsteil ASC ";
											$query = myqueryi($db, $sql);
			
											echo "<select name=\"otneuselect\" style=\"background-color:$bg_ot\">";
											while ($zeile = mysqli_fetch_array($query)) {
												if ($otneu_select == $zeile[0]) { echo "<option selected>$zeile[0]</option>"; }
												else { echo "<option>$zeile[0]</option>"; }
											}
											echo "</select>";			
										echo "</tr>";
										echo "<tr>";
											if (empty($fehler)) {
												echo "<td align = \"left\"><input type=\"text\" name=\"otneuinput\" value=\"Ortsteil eingeben!\"></span></td>";
											}
											else {
												echo "<td align = \"left\"><input type=\"text\" name=\"otneuinput\" value=\"$otneuup\" style=\"background-color:$bg_ot\"></span></td>";
											}
										echo "</tr>";
									echo "</table>";
								echo"</td>";
							echo "</tr>";
						echo "<tr>";
							echo "<td colspan = \"2\" align = \"center\">
									<input type=\"submit\" name=\"ja\" value=\"Ersetzen!\" class = \"submitt\">&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"nein\" value=\"Zurück!\" class=\"correct\">
								<td>";
						echo "</tr>";
					?>
				</form>
			</table>
		</td>
	</tr>
</table>
</div>
</body>
</html>

<?php
}	// ende else Script startet erstmalig, kein Button gedrückt
$close = mysqli_close($db);
?>