<?php

// -------------------------------------------------------------------------------------------------//
// mit diesem Script k�nnen Branchen komplett gel�scht werden										//
// nur ein Administrator darf das Script nutzen														//
// die Branche kommt von branche_select.php.php														//
// vor dem L�schen erfolgt eine Sicherheitsabfrage													//
// wenn Kunden vorhanden sind, werden diese auf branche_id =1 gesetzt								//
// falls die Branche sonst nicht mehr verwendet wird, wird sie ebenfalls gel�scht					//
// nach dem L�schen wird eine Meldung �ber Erfolg oder Misserfolg der L�schaktion ausgegeben		//
// danach erfolgt ein Refresh von branche_select und branche_bearbeiten.php							//
// -------------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen


include ("../../include/init.php");
sessiondauer();

$gruppe = $_SESSION['benutzer_gruppen'];			// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php

//  Feststellen, ob Benutzer überhaupt löschen darf --------------------------------------------------------------

if ($gruppe != "Administrator") {
	echo "<script>location.href='../../forbidden.php'</script>";	// kein Administrator - Zugriff nicht erlaubt
}


// POST- und GET-Variablen --------------------------------------------------------------------------------------

$ja = $_POST["ja"];						// Ja-Button, Daten sollen gelöscht werden
$nein = $_POST["nein"];					// Nein-Button, ersetzen wird abgebrochen
$ok = $_POST["ok"];						// Operation erfolgreich, Refresh der Frames

$branche = $_GET["branche"];			// Branche kommt vom branche_select.php

// Löschen ist abgebrochen, RÜcksprung zu branche_select.php ----------------------------------------------------------

if (isset($nein)) {										// Nein-Button gedrückt
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['branchelinks'].location.href='branche_select.php'</script>";
	echo "<script>location.href='branche_bearbeiten.php'</script>";
}
// ------------------------------------------------------------------------------------------------------------

// Bearbeitung war erfolgreich, Refresh der Frames ---------------------------------
if (isset($ok)) {
	//flush();				// Ausgabepuffer auf den Bildschirm schreiben
	//usleep(3000000);		// 3 Sekunden warten, dann Refresh
	
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['branchelinks'].location.href='branche_select.php'</script>";
	echo "<script>location.href='branche_bearbeiten.php'</script>";
}


// Branche soll gelöscht werden -----------------------------------------------------------------------------

elseif (isset($ja)) {									// Ja-Button gedrückt

	$branche = $_POST["branche"]; 				// Branche wird als hidden-Feld übertragen
	
	// Ermittlung, ob Kunden mit Branche vorhanden sind---------------------------------------
	$sql = " SELECT kunden_id FROM kunden, branche ";
	$sql .= " WHERE kunden.branche_id = branche.branche_id AND branche = '$branche' ";
	$query = myqueryi($db, $sql);
	
	if (mysqli_num_rows($query) > 0) {					// Kunden vorhanden

		for ($i=0; $i<mysqli_num_rows($query); $i++) {		// alle Kunden werden durchlaufen
			$ergebnis = mysqli_fetch_array($query);
			
			// UPDATE Kunden
			$sql = " UPDATE kunden SET branche_id = '1' WHERE kunden_id = '$ergebnis[kunden_id]' ";
			$abfrage = myqueryi($db, $sql);
		}	// ende alle Kunden durchlaufen
	}	// ende Kunden vorhanden
	
	// Sicherheitsabfrage, ob alle Kunden geupdated wurden
	$sql = " SELECT kunden_id FROM kunden, branche ";
	$sql .= " WHERE kunden.branche_id = branche.branche_id AND branche = '$branche' ";
	$result = myqueryi($db, $sql);
			
	if (mysqli_num_rows($result) > 0) {						// noch Kunden vorhanden
		$fehler = "<br />Es sind noch weitere Kunden vorhanden - bitte das Script noch einmal ausführen!<br />";
	}
			
	else {													// alle Termine geupdated
			
		// branche löschen
		$sql = " DELETE FROM branche WHERE branche = '$branche' LIMIT 1 ";		// lösche branche
		$result = myqueryi($db, $sql);
	}

?>
	
<!DOCTYPE html >
<html lang="de">
<head>
<title>Branche löschen</title>
	<!-- allgemein/branche/branche_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
		  <?php	// Ausgabe der Meldung
					if (isset($fehler)) {	// noch Kunden vorhanden
						echo "<tr bgcolor = \"#ff6633\"><td align=\"center\" valign=\"middle\"><span style = \"font-weight:bold; color:black; font-size:10pt; line-height:200%;\">$fehler</span></td></tr>";
					}
					else {
						echo "<tr bgcolor = \"#ff6633\"><td align=\"center\" valign=\"middle\"><span style = \"font-weight:bold; color:maroon; font-size:10pt; line-height:200%;\">
								Die Branche $branche wurde gelöscht.<br /></span></td></tr>";
					}
				?>
				<tr bgcolor = "moccasin">
					<form method="post" action= "<?php $_SERVER["PHP_SELF"] ?>">
					<td align="center" valign="middle"><input type="submit" name="ok" value="OK!" class = "nein"></td>
					</form>
				</tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php
} // ende löschen ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


else {	// kein Button gedrückt - Script startet erstmals

// Ermittlung, ob Kunden vorhanden sind ----------------------------------------------------------------------
$sql = " SELECT kunden_id FROM kunden, branche ";
$sql .= " WHERE kunden.branche_id = branche.branche_id AND branche = '$branche' ";
$kunden = myqueryi($db, $sql);
if (mysqli_num_rows($kunden) > 0) {
	$bemerkung1 = "Es gibt noch " . mysqli_num_rows($kunden) . " Kunden mit der Branche $branche. <br />
				Soll die Branche trotzdem gelöscht werden?<br /><br />
				Alle betroffenen Kunden verlieren dabei die Branche.<br />
				Bei großen Datenmengen (vielen Kunden) muss der Vorgang u.U. wiederholt werden.<br />";
}
else {
	$bemerkung2 = "Es existieren keine Kunden mit der Branche $branche.<br />
					Die Branche kann gelöscht werden<br />";
}
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html >
<html lang="de">
<head>
<title>Branche löschen</title>
	<!-- allgemein/branche/branche_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><form name="loeschen" method="post" action="<?php $_SERVER[PHP_SELF] ?>">
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
          <tr bgcolor="moccasin"><td colspan="2" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Branche <span style = "color:red;"><?php echo "$branche"; ?></span> löschen</span></td></tr>
		  <?php	// Ausgabe der Warnmeldung
					if (mysqli_num_rows($kunden) > 0) {	// noch Kunden vorhanden
						echo "<tr><td colspan=\"2\" align=\"center\" valign=\"middle\"><span style = \"color: red; font-weight: bold;\">$bemerkung1</span></td></tr>";
					}
					else {
						echo "<tr><td colspan=\"2\" align=\"center\" valign=\"middle\"><span style = \"color: black; font-weight: bold;\">$bemerkung2</span></td></tr>";
					}
					echo "<tr>";
						echo "<td align = \"center\" width = \"50%\">
									<input type=\"hidden\" name=\"branche\" value=\"$branche\">
								<input type=\"submit\" name=\"ja\" value=\"LÖSCHEN!\" class = \"ja\"></td>";
            			echo "<td align = \"center\" width = \"50%\"><input type=\"submit\" name=\"nein\" value=\"Abbrechen\" class=\"nein\"></td>";
					echo "</tr>";
				?>
        </table></form></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php
}	// ende else Script startet erstmalig, kein Button gedrückt

?>