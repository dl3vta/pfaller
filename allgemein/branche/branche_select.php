<?php

// -------------------------------------------------------------------------------------------------//
// mit diesem Script k�nnen Branchen neu eingegeben oder zur Bearbeitung ausgew�hlt werden  		//
// nur ein Administrator darf das Script nutzen														//
// die Weiterverarbeitung geschieht in branche_bearbeiten.php										//
// das L�schen geschieht in branche_loeschen.php													//
// -------------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];
$refresh = $_POST["refresh"];

$bg_fehler = "#ff9966";								// Fehler-Farbe


//	refresh-Button wurde gedrückt
if (isset($refresh)) {
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['brancherechts'].location.href='branche_bearbeiten.php'</script>";
}

// Speichern-Button wurde gedrückt ------------------
if (isset($speichern)) {
	$brancheneu = $_POST["brancheneu"];
	
	if (empty($brancheneu)) {		// keine Branche eingegeben
		$fehler = "Fehler: Sie müssen eine Branche eingeben!";
		$bginput = $bg_fehler;
	}

// Branche eingegeben -----------------------
	else { 
	
		$brancheneu = quote_smart($brancheneu);
		
// Test, ob eingegebene Branche schon in der Datenbank
		$sql = "SELECT branche FROM branche WHERE branche = '$brancheneu'";
		$abfrage = myqueryi($db, $sql);
			
		if (mysqli_num_rows($abfrage) != 0) {
			$fehler = "Fehler: Die Branche $brancheneu gibt es schon!";
			$bginput = $bg_fehler;
		}
		else {
			$sql = "INSERT INTO branche (branche) VALUES ('$brancheneu') ";
			$abfrage = myqueryi($db, $sql);
			$brancheneu_id = mysqli_insert_id($db);
			
			$fehler = "Die Branche $brancheneu wurde mit ID: $brancheneu_id in der Datenbank gespeichert";
		}
		echo "<script>onload=parent['brancherechts'].location.href='branche_bearbeiten.php'</script>";
	}	// ende elseBranche eingegeben
}	// Ende IF ISSET speichern


//---------------------------------------------------------------------------------------------------------------------------------------

// Start Script-Ablauf vor dem Drücken des Speichern-Buttons

	$sql  = "SELECT branche_id AS ID, branche AS Branche ";
	$sql .= " FROM branche ";
	$sql .= " WHERE branche_id > '1'";
	$sql .= " ORDER BY branche ASC";
	$ergebnis = myqueryi ($db, $sql);

?>

<!-- Ausgabe bei Start des Programms -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Branchen</title>
	<!-- allgemein/branche/branche_select.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function FrameAendern (URI, Framename) { parent[Framename].location.href = URI; }
</script>
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.submitt {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.refresh {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">

<table width="500px" border="0" cellpadding="4" cellspacing="4">
<tr>
	<td>
		<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
			<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
	<?php
					if ($fehler) {
						echo "<tr><td colspan = \"4\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold;\">$fehler</span></td></tr>";
					}
				?>
				<form name="branche_neu" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
				<tr>
					<td valign = "middle">
						<table width="100%" cellspacing="5" bgcolor="moccasin">
							<tr>
    							<td align="left" valign="middle"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">Eingabe Branche</span></td>
								<td align="left" valign="middle">
								<?php echo "<input type=\"text\" name=\"brancheneu\" size = \"20\" maxlength =\"20\" value=\"$brancheneu\" style=\"background-color:$bginput\">"; ?>
    							<td align="right" valign="middle"><input type="submit" name="speichern" value="Speichern" class = "submitt"></td>
    						</tr>
						</table>
					</td>
				</tr>
				
				
				<tr><td valign = "top">
					<table id="ausgabe" cellspacing="4" width = "100%">
				
					<!-- Ausgabe der bereits vorhandenen Branchen -------------------------------------------------------------- -->

						<tr>
							<td colspan="3" valign="middle"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">vorhandene Branchen:</span></td>
							<td align="right" valign="middle"><input type="submit" name="refresh" value="REFRESH" class="refresh"></td>
						</tr>
	 <?php
					echo "<tr>";													// Tabellenkopf
					for($i = 0; $i < mysqli_num_fields($ergebnis); $i++) {// Anzahl der Tabellenzellen

						/*$row = $ergebnis->mysqli_fetch_assoc();
						$spalte = $row->array_keys();
						$ergebnis = data_seek(0);*/

						$row = mysqli_fetch_assoc($ergebnis);
						$feldname = array_keys($row);
						mysqli_data_seek($ergebnis, 0);

						echo "<td align\"left\" valign=\"middle\"><b>$feldname[$i]</b></td>";
					}
					echo "<td align\"center\" valign=\"middle\" width=\"130px\"><div align=\"center\"><strong>Branche</strong></br>bearbeiten</div></td>";		// eine Zelle für Branche bearbeiten
					echo "<td align\"center\" valign=\"middle\" width=\"130px\"><div align=\"center\"><strong>Branche</strong></br>löschen</div></td>";			// eine Zelle für Branche l�schen

					echo "</tr>\n";													// Tabellenkopf Ende
	
					$z=0;  //zähler der datensätze für bg_colour der zeilen
					$bg1 = "#eeeeee"; //die beiden hintergrundfarben
					$bg2 = "#dddddd";	
	
					for ($j = 0; $j < mysqli_num_rows($ergebnis); $j++) {			// Anzahl der Datensätze
						$zeile = mysqli_fetch_row($ergebnis);						// Schleife für Daten-Zeilen
						$bg=($z++ % 2) ? $bg1 : $bg2;
						echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
			
						for($i = 0; $i < mysqli_num_fields($ergebnis); $i++) {					// Schleife für Felder pro Zeile
							echo "<td align\"left\" valign=\"middle\">" . $zeile[$i] . "</td>";
						}
			
  					echo "<td valign = \"middle\" align = \"center\">

						<a href=\"branche_bearbeiten.php?branche=$zeile[1]\" target=\"brancherechts\">
				  		<img src=\"../../images/bearbeite.png\" alt=\"bearbeiten\" title=\"bearbeiten\" border=\"0\" /></a></td>";
					
					echo "<td valign = \"middle\" align = \"center\">
						<a href=\"branche_loeschen.php?branche=$zeile[1]\" target=\"brancherechts\">
				  		<img src=\"../../images/loesche.png\" alt=\"loeschen\" title=\"loeschen\" border=\"0\" /></a></td>";
				echo "</tr>";
    		}
		echo "</table>";
				// Ende Ausgabe vorhandenener Branchen +++++++++++++++++++++++++++++++++++++++++++++++
			?>
		</table>
	</table>
</table>
</form>
</td>
</tr>
</table>
</div>
</body>
</html>