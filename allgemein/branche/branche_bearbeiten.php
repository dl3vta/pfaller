<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script können Branchen bearbeitet													//
// die Branche kommt von branche_select															//
// alle betroffenen Kunden werden geupdated                                                     //
// falls nicht mehr verwendet, wird die alte Branche gelöscht                                   //
// nach dem Löschen wird eine Meldung über Erfolg oder Misserfolg der Löschaktion ausgegeben    //
// die Seite branche_select.php wird refreshed		   							                //
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];			// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php
$bg_fehler = "#ff9966";								// Fehler-Farbe
unset($fehler);										// Fehlerausgabe wird zurückgesetzt

// POST- und GET-Variablen --------------------------------------------------------------------------------------

$ja = $_POST["ja"];						// Ja-Button, Daten sollen gelöscht werden
$nein = $_POST["nein"];					// Nein-Button, ersetzen wird abgebrochen
$update = $_POST["update"];				// Update-Button
$ok = $_POST["ok"];						// Operation erfolgreich, Refresh der Frames

$branchealt = $_GET["branche"];			// Branche, kommt von branche_select
$brancheupdate = $_GET["branche"];

// ---------------------------------------------------------------------------------------

// Bearbeitung ist abgebrochen, Refresh der Frames ---------------------------------
if (isset($nein)) {										// Leeren-Button gedrückt
	echo "<script>onload=parent['branchelinks'].location.href='branche_select.php'</script>";
	echo "<script>location.href='branche_bearbeiten.php'</script>";
}

// Bearbeitung war erfolgreich, Refresh der Frames ---------------------------------
if (isset($ok)) {
	echo "<script>onload=parent['branchelinks'].location.href='branche_select.php'</script>";
	echo "<script>location.href='branche_bearbeiten.php'</script>";
}

// Branche soll geupdated werden-----------------------------------------------------

if (isset($update)) {
	$brancheupdate = $_POST["brancheupdate"];	// Branche aus Select-Feld
	$brancheupdate = quote_smart($brancheupdate);
	$branchealt = $_POST["branchealt"];
	
// keine Eingabe
	if (empty($brancheupdate)) {
		$fehler = "Sie müssen eine Branche eingeben!";
		$bgupdate = $bg_fehler;
	}

// alte und neue Branche gleich!
	elseif ($brancheupdate == $branchealt) {
		$fehler = "Diese Eingabe ist sinnlos";
		$bgupdate = $bg_fehler;
	}
	
// Eingabe korrekt
	else {
	
// ist neu eingegebene Branche schon da - Fehlermeldung "Ersetzen"
// ansonsten update des branchees
		$sql = " SELECT branche_id FROM branche WHERE branche = '$brancheupdate' ";
		$query = myqueryi($db, $sql);
	
		if (mysqli_num_rows($query) > 0) {
			$fehler = "Diese Branche gibt es schon - bitte ERSETZEN (grauer Abschnitt)";
			$bgupdate = $bg_fehler;
		}
	
		else {
			$sql = " UPDATE branche SET branche = '$brancheupdate' WHERE branche = '$branchealt' ";
			$query = myqueryi($db, $sql);

			// Refresh der einzelnen Seiten in den entsprechenden Frames
			echo "<script>onload=parent['branchelinks'].location.href='branche_select.php'</script>";
			echo "<script>location.href='branche_bearbeiten.php'</script>";
		}
	}	// ende Eingabe korrekt
}	// ende update


// branche soll ersetzt werden
elseif (isset($ja)) {				// Ja-Button gedrückt
	
	$brancheinput = $_POST["brancheinput"];	// Branche aus Textfeld
	$brancheselect = $_POST["brancheselect"];	// Branche aus Select-Feld
	$brancheinput = quote_smart($brancheinput);
	$branchealt = $_POST["branchealt"];		// Branche alt wird als hidden-Feld aus dem Formular übertragen
	
	// Eingabefeld überschreibt Select-Feld
	if (isset($brancheinput) AND $brancheinput != 'Branche eingeben!') { $brancheneu = $brancheinput; }
	elseif (isset($brancheselect)) { $brancheneu = $brancheselect; }
	
// Überprüfung der Eingabewerte --------------------------------------------------------------------------------------------------
	if ((empty($brancheinput) OR $brancheinput == 'Branche eingeben!') AND empty($brancheselect)) {					// keine Eingaben gemacht
		$fehler = "Sie müssen eine Branche auswählen oder eingeben!";
		$bgbranche = $bg_fehler;
	}
	
	elseif ($brancheneu == $branchealt) {
		$fehler = "Diese Eingabe ist sinnlos";
		$bgbranche = $bg_fehler;
	}
	
//	eingaben vollzogen
	else {
		if (empty($fehler)) {
			?>

				<!-- Ausgabe solange der Rechner arbeitet -->
				<!DOCTYPE html>
				<html lang = "de">
				<head>
				<title>Branche bearbeiten</title>
					<!-- allgemein/branche/branche_bearbeiten.php -->
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
				<style type="text/css">
				<!--
				body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }
				-->
				</style>
				</head><body><div align = "center">
					<table width="500" border="0" cellspacing="0" cellpadding="3">
 						<tr>
    						<td>
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
      								<tr>
        								<td>
											<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
												<tr bgcolor = "#ff6633">
													<td><span style = "font-weight:bold; color:black; font-size:10pt; line-height:200%;">
														<div align = "center">In Arbeit ...</div>
														Bei vielen Kunden kann unter Umständen die maximale Serverlaufzeit
														überschritten werden, in diesem Fall das Programm bitte noch einmal
														ausführen.
														</span>
													</td>
												</tr>
        									</table>
										</td>
      								</tr>
    							</table>
							</td>
  						</tr>
					</table>
				</div></body>
				</html>
<?php

		// ÜberprÜfung der Eingabewerte beendet
		// Auswertung startet
		//	Folgende Fälle sind möglich: ----------------------------------------------
		//	1.		branche neu bereits vorhanden mit Kunden
		//	1.1.	branche alt mit Kunden
		//	1.2.	branche alt ohne Kunden
		//	2.		branche neu bereits vorhanden ohne Kunden
		//	2.1.	branche alt mit Kunden
		//	2.2.	branche alt ohne Kunden
		//	3.		branche neu neu
		//	3.1.	branche alt mit Kunden
		//	3.2.	branche alt ohne Kunden
		// --------------------------------------------------------------------------
	
// Ermittlung branche-id alt
		$sql  = " SELECT branche_id FROM branche WHERE  branche = '$branchealt' ";
		$branchealtquery = myqueryi($db, $sql);
		
		$branche = mysqli_fetch_array($branchealtquery);

// branche_id-neu ermitteln
		$sql1 = "SELECT branche_id FROM branche WHERE branche = '$brancheneu'";			// branche schon vorhanden
		$sql2 = "INSERT INTO branche (branche) VALUES ('$brancheneu')";				// branche neu
		$brancheneuid = id_ermitteln($db, $sql1, $sql2);

//Kunden mit branchealt-id aus der Datenbank lesen
		$sql = " SELECT kunden_id FROM kunden WHERE branche_id = '$branche[branche_id]' ";
		$kundenaltquery = myqueryi($db, $sql);
		
// wenn vorhanden, Kunden auf neue branchen-id updaten
		if (mysqli_num_rows($kundenaltquery) > 0) {
			for ($i=0; $i<mysqli_num_rows($kundenaltquery); $i++) {
				$kunde = mysqli_fetch_array($kundenaltquery);
				$sql = " UPDATE kunden SET branche_id = '$brancheneuid' WHERE kunden_id = '$kunde[kunden_id]' ";
				$query = myqueryi($db, $sql);
			}
		}

// Kontrollabruf, ob es noch Kunden mit branchealt-id gibt
		$sql = " SELECT kunden_id FROM kunden WHERE branche_id = '$branche[branche_id]' ";
		$testquery = myqueryi($db, $sql);
		
		if (mysqli_num_rows($testquery) > 0) {
			$fehler = "Es gibt noch Kunden mit dieser Branche - bitte noch einmal";
		}

// keine Kunden mehr da
		else {
			// branche-alt-id löschen
			$sql = "DELETE FROM branche WHERE branche = '$branchealt' LIMIT 1";
			$query = myqueryi($db, $sql);
		}	// ende else keine Termine mehr da
		
// Ausgabe des Ergebnisses

			?>

				<!-- Ausgabe solange der Rechner arbeitet -->
				<!DOCTYPE html>
				<html lang ="de">
				<head>
				<title>Branche bearbeiten</title>
					<!-- allgemein/branche/branche_bearbeiten.php -->
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
				<style type="text/css">
				<!--
					body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }
	
					.ja {
						border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
						background-color: #9b0013; color: #ffffff;
						font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
						width:100px;line-height:140%;
					}

					.nein {
						border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
						background-color: #D8E1EC; color: #006699;
						font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
						width:100px;line-height:140%;
					}

				-->
				</style>
				</head><body><div align = "center">
					<table width="500" border="0" cellspacing="0" cellpadding="3">
 						<tr>
    						<td>
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
      								<tr>
        								<td>
											<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
												<tr bgcolor = "#ff6633">
													<td><span style = "font-weight:bold; color:black; font-size:10pt; line-height:200%;">
														<div align = "center">Fertig ...</div>
														Die Branche <span style="color:white;"><?php echo "$branchealt"; ?></span> wurde durch <span style="color:white;"><?php echo "$brancheneu"; ?></span> ersetzt.<br />
														Alle betroffenen Kunden wurden upgedated.<br />
														</span>
													</td>
												</tr>
												<tr bgcolor = "moccasin">
													<form method="post" action= "<?php $_SERVER["PHP_SELF"] ?>">
													<td align="center" valign="middle"><input type="submit" name="ok" value="OK!" class = "nein"></td>
													</form>
												</tr>
        									</table>
										</td>
      								</tr>
    							</table>
							</td>
  						</tr>
					</table>
				</div></body>
				</html>
<?php
		
	}	// ende if empty fehler
	}	// ende else eingaben korrekt
} // ende ja gedr&uuml;ckt

  
if (isset($fehler) OR !isset($ja)) {	// kein Button gedrückt oder Fehler bei der Eingabe - Formular wird angezeigt

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Branche bearbeiten</title>
	<!-- allgemein/branche/branche_bearbeiten.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}
	
.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">

<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
     		<tr>
        		<td>
					<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
					
          				<tr bgcolor="moccasin"><td colspan="2" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Branche <span style = "color:red;"><?php echo "$branchealt"; ?></span> bearbeiten</span></td></tr>
		 
		 				<?php if ($fehler) {
 								echo "<tr bgcolor=\"red\">";
        						echo "<td colspan=\"2\" align = \"left\" valign = \"middle\">";
								echo "<span style=\"font-weight:bold; color:white; line-height:150%;\">$fehler</span><br>";
								echo "</td></tr>";
							}
						?>
						
		 				<tr>
							<td valign = "top">
								<!-- Start Formular für branche-Update ++++++++++++++++++++++++++++++++++ // -->
								<form method="post" action= "<?php $_SERVER["PHP_SELF"] ?>">
								<input type="hidden" name="branchealt" value="<?php echo "$branchealt"; ?>">
									<table id="formular" width="100%">
  										<tr>
    										<td align="left" valign="middle">Branche</td>
												<?php	if (isset($fehler)) {echo "<td align=\"left\" valign=\"middle\"><input type=\"text\" name=\"brancheupdate\" size = \"25\" value=\"$brancheupdate\" style=\"background-color:$bgupdate\"></td>"; }
													else { echo "<td align=\"left\" valign=\"middle\"><input type=\"text\" name=\"brancheupdate\" size = \"25\" value=\"$branchealt\"></td>"; }
												?>
							
											<td align="left" valign="middle"><input type="submit" name="update" value="Update!" class = "submitt"></td>
  										</tr>
									</table>      
								<!-- Ende Formular für branche-Update ++++++++++++++++++++++++++++++++++ // -->
							</td>
						</tr>
						
						<?php	//  Feststellen, ob Benutzer überhaupt löschen darf ---------------------------------------------------------------------------
							if ($gruppe == "Administrator") {
						?>
						
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" bgcolor="#cccccc">
									<tr>
										<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">Die Branche</span></td>
										<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">soll ersetzt werden durch</span></td>
									</tr>




									<tr>
		  								<?php
										echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ffffcc\">";

											echo "<table width=\"100%\">";
												echo "<tr>";
													// Anzeige der gewählten Branche (alt) in Text-Feldern - aber keine Verarbeitung der Felder
													echo "<td align = \"left\"><span style=\"font-weight:bold; font-size:10pt; color:red; line-height:150%;\"><input type=\"text\" name=\"branchealt\" value=\"$branchealt\"></span></td>";
												echo "</tr>";
											echo "</table>";
										echo "</td>";
								
										// Zelle rechts - neue Branche			
										echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ccffff\">";
											echo "<table width=\"100%\">";
												echo "<tr>";
													echo "<td colspan = \"2\">";
														// Die Branchen werden aus der DB gelesen
														$sql  = "SELECT branche FROM branche ORDER BY branche ASC ";
														$query = myqueryi($db, $sql);
			
														echo "<select name=\"brancheselect\" style=\"background-color:$bgbranche\">";
														while ($zeile = mysqli_fetch_array($query)) {
															if ($brancheselect == $zeile[0]) { echo "<option selected>$zeile[0]</option>"; }
															else { echo "<option>$zeile[0]</option>"; }
														}
														echo "</select>";			
												echo "</tr>";
											echo "<tr>";
												if (empty($fehler)) {
													echo "<td align = \"left\"><input type=\"text\" name=\"brancheinput\" value=\"Branche eingeben!\"></span></td>";
												}
												else {
													echo "<td align = \"left\"><input type=\"text\" name=\"brancheinput\" value=\"$brancheinput\" style=\"background-color:$bgbranche\"></span></td>";
												}
											echo "</tr>";
										echo "</table>";
									echo"</td>";

								echo "</tr>";
								echo "<tr>";
									echo "<td colspan = \"2\" align = \"center\">
										<input type=\"submit\" name=\"ja\" value=\"Ersetzen!\" class = \"ja\">&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"nein\" value=\"Zurück\" class=\"nein\">
									<td>";
							echo "</tr>";
							?>
							</tr>
							</form>
						</table>
						</td>
					</tr>
					<?php } // Ende Administrator darf löschen ?>
        			</table>
				</td>
      		</tr>
    	</table>
	</td>
  </tr>
</table>
</div>
</body>
</html>
<?php
}

?>




