<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

include ("../include/mysqlconnect.inc.php");
include ("../include/variablen.php");
include ("../include/funktionen.php");

/*echo "<pre>";
print_r($_SESSION);
echo "</pre>";*/

/*
$wert1 = NULL;
$wert2 = NULL;
$wert3 = NULL;
$feld1 = NULL;
$feld2 = NULL;
$feld3 = NULL;
$ort1 = NULL;
$ort2 = NULL;
$ort3 = NULL;
$richtung1 = NULL;
$richtung2 = NULL;
$richtung3 = NULL;

$spalten = NULL;
$gebbmonat = NULL;

$sort1 = NULL;
$sort2 = NULL;
$sort3 = NULL;

*/

$wert1 = $wert2 = $wert3 = NULL;
$feld1 = $feld2 = $feld3 = NULL;
$ort1 = $ort2 = $ort3 = NULL;
$richtung1 = $richtung2 = $richtung3 = NULL;

$spalten = NULL;
$gebbmonat = NULL;

$sort1 = $sort2 = $sort3 = NULL;

$telefonist = $_SESSION['benutzer_kurz'];
$gruppe = $_SESSION['benutzer_gruppen'];		// Gruppe des eingeloggten Benutzers
$suchen =  $_POST["suchen"];
$reset =  $_POST["reset"];
$formular =  $_POST["formular"];			// verstecktes Element - SUCHEN-Button wurde gedrückt
$check =  $_GET["check"];					// kennzeichnet Rücksprung von such_check zum Suchergebnis

$begriff1 = array("Telefon", "alle", "Vorwahl", "Name", "Vorname", "Ort", "Ortsteil", "PLZ", "Straße", "Bem_Termin", "Bem_Kunde", "geboren", "KdID");	// Suchbegriffe für Feld1
$begriff2 = array("Name", "Vorname", "Vorwahl", "Telefon", "Ort", "Ortsteil", "PLZ", "Straße", "Bem_Termin", "Bem_Kunde");			// Suchbegriffe für Feld1
$begriff3 = array("Vorname", "Ort", "Ortsteil", "Straße", "PLZ", "Telefon", "Vorwahl", "Bem_Termin", "Bem_Kunde", "Name");			// Suchbegriffe für Feld1

$sortierung1 = array("Name", "Vorname", "Strasse", "Ort", "Ortsteil", "PLZ", "geboren");	// Suchbegriffe für primäre Sortierung
$sortierung2 = array("Vorname", "Strasse", "Ort", "Ortsteil", "PLZ", "Name");	// Suchbegriffe für sekundäre Sortierung
$sortierung3 = array("Strasse", "Ort", "Ortsteil", "PLZ", "Name", "Vorname");	// Suchbegriffe für tertiäre Sortierung

$bgsuche = "#ffffcc";
$bgsortierung = "#ccffcc";
$bgsubmitt = "#ffccff";

if ($check) {			// Rücksprung von such_check - Suchergebnis soll angezeigt werden
	$suchen = "1";		// Suchvorgang wird ausgelöst
}
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++ SUCHEN GEDRüCKT +++++++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if ($suchen) {

	$wert1 = quote_smart($_POST["wert1_neu"]);
	$wert2 = quote_smart($_POST["wert2_neu"]);
	$wert3 = quote_smart($_POST["wert3_neu"]);

	$feld1 = $_POST["feld1_neu"];
	$feld2 = $_POST["feld2_neu"];
	$feld3 = $_POST["feld3_neu"];

	$sort1 = $_POST["sort1_neu"];
	$sort2 = $_POST["sort2_neu"];
	$sort3 = $_POST["sort3_neu"];

	$richtung1 = $_POST["richtung1"];
	$richtung2 = $_POST["richtung2"];
	$richtung3 = $_POST["richtung3"];

		if (($check) AND ($formular != "true")) {			// Rücksprung von such_check - Suchergebnis soll angezeigt werden

		$wert1	= $_SESSION['wert1c'];
		$wert2	= $_SESSION['wert2c'];
		$wert3	= $_SESSION['wert3c'];

		$feld1 = $_SESSION['feld1c'];
		$feld2 = $_SESSION['feld2c'];
		$feld3 = $_SESSION['feld3c'];

		$sort1 = $_SESSION['sort1c'];
		$sort2 = $_SESSION['sort2c'];
		$sort3 = $_SESSION['sort3c'];

		$richtung1 = $_SESSION['richtung1c'];
		$richtung2 = $_SESSION['richtung2c'];
		$richtung3 = $_SESSION['richtung3c'];

		unset($_SESSION['wert1c'], $wert1c);
		unset($_SESSION['wert2c'], $wert2c);
		unset($_SESSION['wert3c'], $wert3c);
		unset($_SESSION['feld1c'], $feld1c);
		unset($_SESSION['feld2c'], $feld2c);
		unset($_SESSION['feld3c'], $feld3c);
		unset($_SESSION['sort1c'], $sort1c);
		unset($_SESSION['sort2c'], $sort2c);
		unset($_SESSION['sort3c'], $sort3c);
		unset($_SESSION['richtung1c'], $richtung1c);
		unset($_SESSION['richtung2c'], $richtung2c);
		unset($_SESSION['richtung3c'], $richtung3c);

	}

?>

<!DOCTYPE html>
<html lang="de">
<head>
<title>Suche</title>
	<!-- allgemein/suche.php -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align="center">
<table width="700" border="0" cellpadding="4" cellspacing="4">
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">

<!-- Tabelle für Formular ----------------------------------------------------------------------- -->

<table width="100%"  border="0" cellspacing="0" cellpadding="3">
<form name="input" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
  <tr >
    <td bgcolor="<?php echo "$bgsuche"; ?>" ><img src="../images/100px.gif" width="1" height="2" /></td>
    <td bgcolor="<?php echo "$bgsuche"; ?>" ></td>
    <td bgcolor="<?php echo "$bgsortierung"; ?>" ></td>
    <td colspan="2" bgcolor="<?php echo "$bgsortierung"; ?>" ></td>
	<td bgcolor="<?php echo "$bgsubmitt"; ?>"></td>
  </tr>
  <tr >
    <td bgcolor="<?php echo "$bgsuche"; ?>" ><span style = "font-weight:bold;">Suchbegriff</span></td>
    <td bgcolor="<?php echo "$bgsuche"; ?>" ><span style = "font-weight:bold;">Suchkriterium</span></td>
    <td bgcolor="<?php echo "$bgsortierung"; ?>" ><span style = "font-weight:bold;">Sortierung nach</span></td>
    <td colspan="2" bgcolor="<?php echo "$bgsortierung"; ?>" ><span style = "font-weight:bold;">Sortierrichtung</span></td>
	<td rowspan = "4" bgcolor="<?php echo "$bgsubmitt"; ?>"><input type="submit" name="suchen" value="Suchen" class = "suche"><input type="hidden" name="formular" value="true"></td>
  </tr>
  <tr>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><input type="text" name="wert1_neu" maxlength = "20" size ="20" class = "eingabe" value = "<?php echo "$wert1"; ?>"></td>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><?php echo "<select name=\"feld1_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($begriff1); $i++)	{				// Anzahl der Zeilen im Select-Feld
					if ($begriff1[$i] == $feld1) {
						echo " <option selected>$feld1</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$begriff1[$i]</option>";				// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
	<td bgcolor="<?php echo "$bgsortierung"; ?>"><?php echo "<select name=\"sort1_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($sortierung1); $i++)	{			// Anzahl der Zeilen im Select-Feld
					if ($sortierung1[$i] == $sort1) {
						echo " <option selected>$sort1</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$sortierung1[$i]</option>";			// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
     <td bgcolor="<?php echo "$bgsortierung"; ?>">
	 	<?php if($richtung1 == "ASC") {echo "<label><input type=\"radio\" name=\"richtung1\" value=\"ASC\" checked>auf</label>"; }
		   else {echo "<label><input type=\"radio\" name=\"richtung1\" value=\"ASC\" >auf</label>"; }
		?>
	 </td>
	 <td bgcolor="<?php echo "$bgsortierung"; ?>">
	 	<?php if($richtung1 == "DESC") {echo "<label><input type=\"radio\" name=\"richtung1\" value=\"DESC\" checked>ab</label>"; }
		   else {echo "<label><input type=\"radio\" name=\"richtung1\" value=\"DESC\" >ab</label>"; }
		?>
	 </td>
  </tr>
  <tr>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><input type="text" name="wert2_neu" maxlength = "20" size ="20" class = "eingabe" value = "<?php echo "$wert2"; ?>"></td>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><?php echo "<select name=\"feld2_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($begriff2); $i++)	{				// Anzahl der Zeilen im Select-Feld
					if ($begriff2[$i] == $feld2) {
						echo " <option selected>$feld2</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$begriff2[$i]</option>";				// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
	<td bgcolor="<?php echo "$bgsortierung"; ?>"><?php echo "<select name=\"sort2_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($sortierung2); $i++)	{			// Anzahl der Zeilen im Select-Feld
					if ($sortierung2[$i] == $sort2) {
						echo " <option selected>$sort2</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$sortierung2[$i]</option>";			// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
     <td bgcolor="<?php echo "$bgsortierung"; ?>">
	 	<?php if($richtung2 == "ASC") {echo "<label><input type=\"radio\" name=\"richtung2\" value=\"ASC\" checked>auf</label>"; }
		   else {echo "<label><input type=\"radio\" name=\"richtung2\" value=\"ASC\" >auf</label>"; }
		?>
	 </td>
	 <td bgcolor="<?php echo "$bgsortierung"; ?>">
	 	<?php if($richtung2 == "DESC") {echo "<label><input type=\"radio\" name=\"richtung2\" value=\"DESC\" checked>ab</label>"; }
		   else {echo "<label><input type=\"radio\" name=\"richtung2\" value=\"DESC\" >ab</label>"; }
		?>
	 </td>
  </tr>
    <tr>
    <td bgcolor="<?php echo "$bgsuche"; ?>"><input type="text" name="wert3_neu" maxlength = "20" size ="20" class = "eingabe" value = "<?php echo "$wert3"; ?>"></td>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><?php echo "<select name=\"feld3_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($begriff3); $i++)	{				// Anzahl der Zeilen im Select-Feld
					if ($begriff3[$i] == $feld3) {
						echo " <option selected>$feld3</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$begriff3[$i]</option>";				// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
	<td bgcolor="<?php echo "$bgsortierung"; ?>"><?php echo "<select name=\"sort3_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($sortierung3); $i++)	{			// Anzahl der Zeilen im Select-Feld
					if ($sortierung3[$i] == $sort3) {
						echo " <option selected>$sort3</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$sortierung3[$i]</option>";			// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
     <td bgcolor="<?php echo "$bgsortierung"; ?>">
	 	<?php if($richtung3 == "ASC") {echo "<label><input type=\"radio\" name=\"richtung3\" value=\"ASC\" checked>auf</label>"; }
		   else {echo "<label><input type=\"radio\" name=\"richtung3\" value=\"ASC\" >auf</label>"; }
		?>
	 </td>
	 <td bgcolor="<?php echo "$bgsortierung"; ?>">
	 	<?php if($richtung3 == "DESC") {echo "<label><input type=\"radio\" name=\"richtung3\" value=\"DESC\" checked>ab</label>"; }
		   else {echo "<label><input type=\"radio\" name=\"richtung3\" value=\"DESC\" >ab</label>"; }
		?>
	 </td>
  </tr>
    <tr >
    <td bgcolor="<?php echo "$bgsuche"; ?>" ><img src="../images/100px.gif" width="1" height="2" /></td>
    <td bgcolor="<?php echo "$bgsuche"; ?>" ></td>
    <td bgcolor="<?php echo "$bgsortierung"; ?>" ></td>
    <td colspan="2" bgcolor="<?php echo "$bgsortierung"; ?>" ></td>
	<td bgcolor="<?php echo "$bgsubmitt"; ?>"></td>
  </tr>
  </form>
</table>


<!-- Ende Tabelle für Formular ----------------------------------------------------------------------- -->

</td></tr>
<!-- Ende Zeile Abfrage / Start Zeile Ergebnisse ++++++++++++++++++++++++++++++++++ // -->
<tr>
<?php

if (empty($wert1) AND empty($wert2) AND empty($wert3)) {

	echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: keine Suchbegriffe eingetragen!</span></td>";
}
elseif (($feld1 == "Telefon" AND !empty($wert1) AND mb_strlen($wert1) < $telefon_laenge) OR ($feld2 == "Telefon" AND !empty($wert2) AND mb_strlen($wert2) < 4) OR ($feld3 == "Telefon" AND !empty($wert3) AND mb_strlen($wert3) < 4)) {
	echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Telefonnummer bitte mindestens " .$telefon_laenge . "-stellig eintragen!</span></td>";
}

elseif (($feld1 == "Bem_Termin" AND empty($wert1)) OR ($feld2 == "Bemerkung" AND empty($wert2)) OR ($feld3 == "Bemerkung" AND empty($wert3))) {
	echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Der Suchbegriff Bem_Termin darf nicht leer sein!</span></td>";
}

elseif (($feld1 == "Bem_Kunde" AND empty($wert1)) OR ($feld2 == "Bemerkung" AND empty($wert2)) OR ($feld3 == "Bemerkung" AND empty($wert3))) {
	echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Der Suchbegriff Bem_Kunde darf nicht leer sein!</span></td>";
}

elseif (($feld1 == "Bem_Termin" AND !empty($wert1) AND mb_strlen($wert1) < 3) OR ($feld2 == "Bemerkung" AND !empty($wert2) AND mb_strlen($wert2) < 3) OR ($feld3 == "Bemerkung" AND !empty($wert3) AND mb_strlen($wert3) < 3)) {
	echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Bem_Termin bitte mindestens 3-stellig eintragen!</span></td>";
}

elseif (($feld1 == "Bem_Kunde" AND !empty($wert1) AND mb_strlen($wert1) < 3) OR ($feld2 == "Bemerkung" AND !empty($wert2) AND mb_strlen($wert2) < 3) OR ($feld3 == "Bemerkung" AND !empty($wert3) AND mb_strlen($wert3) < 3)) {
	echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Bem_Kunde bitte mindestens 3-stellig eintragen!</span></td>";
}

elseif (empty($wert1) AND (!empty($wert2) OR (!empty($wert3)))) {
	echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Feld 1 muss belegt sein!</span></td>";
}



else { // Daten sind eingegeben

if ($feld1 == "geboren") {
// folgende Eingaben sind erlaubt: JJJJ oder TT.MM. oder TT.MM.JJJJ
// Fall 1: nur jahreszahl eingegeben
	if (mb_strlen($wert1)=="4") {
		if (!preg_match("/^[0-9]+$/", $wert1)) {
  			echo "<tr><td bgcolor = \"red\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">$wert1: Als Jahreszahl sind nur Zahlen erlaubt!</span></td></tr>";
			exit;
		}
		else {
			$gebdatum= $wert1."-";
			$fall="1";
		}
	}
// Fall 2: Tag und Monat eingegeben
	elseif (mb_strlen($wert1)=="6") {
		if (!preg_match("/^\d{2}\.\d{2}\.$/", $wert1)) {
  			echo "<tr><td bgcolor = \"red\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">$wert1: Bitte Datum als TT.MM. eingeben!</span></td></tr>";
			exit;
		}
		else {
			$gebtag=substr ( $wert1, 0,2 );
			$gebmonat=substr ( $wert1, 3,2 );
			
			// Test auf korrekte Einträge
			
  			if ($gebtag=='00' OR $gebmonat=='00') {
  				echo "<tr><td bgcolor = \"red\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">Einen Monat oder Tag 00 gibt es nicht!</span></td></tr>";
				exit;
			}
			
			if (($gebmonat=="01" OR $gebmonat=="03" OR $gebmonat=="05" OR $gebmonat=="07" OR $gebmonat=="08" OR $gebmonat=="10" OR $gebmonat=="12") AND $gebtag > "31") {
  				echo "<tr><td bgcolor = \"red\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">Der gew&auml;hlte Monat hat max. 31 Tage!</span></td></tr>";
				exit;
			}

			if (($gebmonat=="04" OR $gebmonat=="06" OR $gebmonat=="09" OR $gebmonat=="11" OR $gebbmonat=="12") AND $gebtag > "30") {
  				echo "<tr><td bgcolor = \"red\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">Der gew&auml;hlte Monat hat max. 30 Tage!</span></td></tr>";
				exit;
			}

			if ($gebmonat=="02" AND $gebtag > "29") {
  				echo "<tr><td bgcolor = \"red\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">Der Februar hat max. 29 Tage!</span></td></tr>";
				exit;
			}
			
  			$gebdatum="-".$gebmonat."-".$gebtag;
			$fall="2";
		}
	}

// Fall 3: komplettes Datum eingegeben
	elseif (mb_strlen($wert1)=="10") {
	  	if (!preg_match("/^\d{2}\.\d{2}\.(\d{2}|\d{4})$/", $wert1)) {
  			echo "<tr><td bgcolor = \"red\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">$wert1: Bitte Datum als TT.MM.JJJJ eingeben!</span></td></tr>";
			exit;
		}
		else {
			$gebtag=substr ( $wert1, 0,2 );
			$gebmonat=substr ( $wert1, 3,2 );
			$gebjahr=substr ( $wert1, 6,4 );

			$gebcheck = checkdate ( $gebmonat, $gebtag, $gebjahr );

			if ( $gebcheck )   {
				$gebdatum=$gebjahr."-".$gebmonat."-".$gebtag;
				$fall="3";
			}
			else {
				echo "<tr><td bgcolor = \"red\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">$wert1: Das Datum ist falsch!</span></td></tr>";
				exit;
			}
		}
	}
 // Ende Fall 3

// Eingabe von der Anzahl der Zeichen her falsch
	else {
    		echo "<tr><td bgcolor = \"red\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">Eingabe des Datums nicht korrekt! </span></td></tr>";
			exit;
	}
}


// Sortierkriterien und-richtungen werden ermittelt -----------------------------------------------------
$sortierung = "$sort1" . " " . "$richtung1" . ", " . "$sort2" . " " . "$richtung2" . ", " . "$sort3" . " " . "$richtung3";

	// Feld1 ist belegt und alle ausgewählt - die anderen Felder werden ignoriert

	if ($feld1 == "alle") {
		$sql = "SELECT kunden_id AS ID, vorname AS Vorname, name AS Name, ";
		$sql .= "plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, ";
		$sql .= "strasse AS Strasse, vorwahl1 AS Vorwahl, telefon AS Telefon ";
		$sql .= "FROM kunden, name, vorname, ";
		$sql .= "plz, poo, ort, ortsteil, ";
		$sql .= "vorwahl1 ";
		$sql .= "WHERE kunden.name_id = name.name_id AND kunden.vorname_id = vorname.vorname_id ";
		$sql .= "AND kunden.poo_id = poo.poo_id ";
		$sql .= "AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
		$sql .= "AND poo.plz_id = plz.plz_id ";
		$sql .= "AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
		$sql .= "AND (kunden.telefon LIKE '%$wert1%' OR vorwahl1.vorwahl1 LIKE '%$wert1%' OR vorname.vorname LIKE '%$wert1%' OR name.name LIKE '%$wert1%' OR kunden.geboren LIKE '%$wert1%' ";
		$sql .= " OR ort.ort LIKE '%$wert1%' OR ortsteil.ortsteil LIKE '%$wert1%' OR kunden.strasse LIKE '%$wert1%' OR plz.plz LIKE '%$wert1%') ";
   		$sql .= " ORDER BY $sortierung";
	}

	elseif ($feld1 == "KdID") {
		$sql = "SELECT kunden_id AS ID, vorname AS Vorname, name AS Name, ";
		$sql .= "plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, ";
		$sql .= "strasse AS Strasse, vorwahl1 AS Vorwahl, telefon AS Telefon ";
		$sql .= "FROM kunden, name, vorname, ";
		$sql .= "plz, poo, ort, ortsteil, ";
		$sql .= "vorwahl1 ";
		$sql .= "WHERE kunden.name_id = name.name_id AND kunden.vorname_id = vorname.vorname_id ";
		$sql .= "AND kunden.poo_id = poo.poo_id ";
		$sql .= "AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
		$sql .= "AND poo.plz_id = plz.plz_id ";
		$sql .= "AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
		$sql .= "AND kunden.kunden_id = '$wert1' ";
	}
	

	// die anderen Varianten werden durchgespielt
	
	else { // Feld1 ist nicht "alle"
	
		 $sql = "SELECT DISTINCT kunden_id AS ID, vorname AS Vorname, name AS Name, "; 
		/* $sql = "SELECT kunden_id AS ID, vorname AS Vorname, name AS Name, "; */
		$sql .= "plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, ";

		if ($fall == "1" OR $fall == "2") {
			$sql .= "strasse AS Strasse, vorwahl1 AS Vorwahl, telefon AS Telefon, geboren AS geboren ";
		}
		else {
	   		$sql .= "strasse AS Strasse, vorwahl1 AS Vorwahl, telefon AS Telefon ";
		}

		$sql .= "FROM kunden, name, vorname, ";
		$sql .= "plz, poo, ort, ortsteil, ";
		
		if($feld1 == "Bem_Termin" OR $feld2 == "Bem_Termin" OR $feld3 == "Bem_Termin" ) {	
			$sql .= "vorwahl1, termin "; }
		else { $sql .= "vorwahl1 "; }
		
		$sql .= "WHERE kunden.name_id = name.name_id AND kunden.vorname_id = vorname.vorname_id ";
		$sql .= "AND kunden.poo_id = poo.poo_id ";
		$sql .= "AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
		$sql .= "AND poo.plz_id = plz.plz_id ";
		$sql .= "AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	
		// Behandlung wert1
		
		if (empty($wert2) AND empty($wert3)) {		// nur wert1 eingegeben, wert2 und wert3 sind leer
		
			switch($feld1) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert1%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert1%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert1%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert1%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert1%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert1%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert1%' ";
				break;
				case Bem_Termin:
					$sql .= "AND termin.rem_termin LIKE '%$wert1%' AND termin.kd_id = kunden.kunden_id ";
				break;
				case Bem_Kunde:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert1%' ";
				break;
				case geboren:
	 				switch($fall) {
						case 1:
							$sql .= "AND kunden.geboren LIKE '$gebdatum%' ";
						break;
						case 2:
	   						$sql .= "AND kunden.geboren LIKE '%$gebdatum' ";
						break;
						case 3:
							$sql .= "AND kunden.geboren = '$gebdatum' ";
						break;
					}
				break;
				case PLZ:
					$sql .= "AND plz.plz LIKE '%$wert1%' ";
				break;
			}// ende switch feld1
			$sql .= " ORDER BY $sortierung";
			
		} // ende if nur wert1 eingegeben, wert2 und wert3 sind leer
		
		// Behandlung wert2
		
		elseif (!empty($wert2) AND empty($wert3)) {		// wert1 und wert2 eingegeben, wert3 ist leer
		
			if ($feld1 == $feld2) { // die beiden Suchkriterien d�rfen nicht gleich sein
				echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Suchkriterien müssen unterschiedlich sein!</span></td>";
			}
			
			else { // suchkriterien sind unterschiedlich
			
			switch($feld1) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert1%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert1%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert1%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert1%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert1%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert1%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert1%' ";
				break;
				case Bem_Termin:
					$sql .= "AND termin.rem_termin LIKE '%$wert1%' AND termin.kd_id = kunden.kunden_id ";
				break;
				case Bem_Kunde:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert1%' ";
				break;
				case geboren:
					$sql .= "AND kunden.geboren = '$gebdatum' ";
				break;
				case PLZ:
					$sql .= "AND plz.plz LIKE '%$wert1%' ";
				break;
			}// ende switch feld1
			
			switch($feld2) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert2%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert2%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert2%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert2%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert2%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert2%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert2%' ";
				break;
				case Bem_Termin:
					$sql .= "AND termin.rem_termin LIKE '%$wert2%' AND termin.kd_id = kunden.kunden_id ";
				break;
				case Bem_Kunde:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert2%' ";
				break;
				case PLZ:
					$sql .= "AND plz.plz LIKE '%$wert2%' ";
				break;
			}// ende switch feld2
			$sql .= " ORDER BY $sortierung";
			//$abfrage = myquery($sql, $conn);
			} // ende else suchkriterien sind unterschiedlich
		} // ende elseif wert1 und wert2 eingegeben, wert3 ist leer
		
		elseif (empty($wert2) AND !empty($wert3)) {		// wert1 und wert3 eingegeben, wert2 ist leer
			echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Bitte Feld2 ausfüllen!</span></td>";
		}
		
		// Behandlung wert1,2,3
		
		elseif (!empty($wert2) AND !empty($wert3)) {		// wert1, wert2 und wert3 eingegeben
		
			if (($feld1 == $feld2) OR ($feld1 == $feld3) OR ($feld2 == $feld3) ) { // die Suchkriterien dürfen nicht gleich sein
				echo "<td bgcolor = \"red\" align = \"center\"><span style=\"color:white; font-weight:bold;\">Fehler: Suchkriterien müssen unterschiedlich sein!</span></td>";
			}
			
			else { // suchkriterien sind unterschiedlich
			
			switch($feld1) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert1%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert1%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert1%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert1%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert1%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert1%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert1%' ";
				break;
				case Bem_Termin:
					$sql .= "AND termin.rem_termin LIKE '%$wert1%' AND termin.kd_id = kunden.kunden_id ";
				break;
				case Bem_Kunde:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert1%' ";
				break;
				case geboren:
	 				$sql .= "AND kunden.geboren = '$gebdatum' ";
				break;
				case PLZ:
					$sql .= "AND plz.plz LIKE '%$wert1%' ";
				break;
			}// ende switch feld1
			
			switch($feld2) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert2%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert2%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert2%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert2%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert2%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert2%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert2%' ";
				break;
				case Bem_Termin:
					$sql .= "AND termin.rem_termin LIKE '%$wert2%' AND termin.kd_id = kunden.kunden_id ";
				break;
				case Bem_Kunde:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert2%' ";
				break;
				case PLZ:
					$sql .= "AND plz.plz LIKE '%$wert2%' ";
				break;
			}// ende switch feld2
			
			switch($feld3) {
				case Name:
					$sql .= "AND name.name LIKE '%$wert3%' ";
				break;
				case Vorname:
					$sql .= "AND vorname.vorname LIKE '%$wert3%' ";
				break;
				case Ort:
					$sql .= "AND ort.ort LIKE '%$wert3%' ";
				break;
				case Ortsteil:
					$sql .= "AND ortsteil.ortsteil LIKE '%$wert3%' ";
				break;
				case Straße:
					$sql .= "AND kunden.strasse LIKE '%$wert3%' ";
				break;
				case Telefon:
					$sql .= "AND kunden.telefon LIKE '%$wert3%' ";
				break;
				case Vorwahl:
					$sql .= "AND vorwahl1.vorwahl1 LIKE '%$wert3%' ";
				break;
				case Bem_Termin:
					$sql .= "AND termin.rem_termin LIKE '%$wert3%' AND termin.kd_id = kunden.kunden_id ";
				break;
				case Bem_Kunde:
					$sql .= "AND kunden.rem_kunde LIKE '%$wert3%' ";
				break;
				case PLZ:
					$sql .= "AND plz.plz LIKE '%$wert3%' ";
				break;
			}// ende switch feld3
			$sql .= " ORDER BY $sortierung";
			//$abfrage = myquery($sql, $conn);
			} // ende else suchkriterien sind unterschiedlich
		} // ende elseif alle Felder belegt
	} // ende else Feld1 ist nicht alle
	
	//echo "SQL: $sql<br>";

	$abfrage = myqueryi($db, $sql);
	
	//$abfrage = myquery($sql, $conn);
	//$_SESSION['suche'] = $sql;
	
	$_SESSION['wert1c'] = $wert1;
	$_SESSION['wert2c'] = $wert2;
	$_SESSION['wert3c'] = $wert3;
	
	$_SESSION['feld1c'] = $feld1;
	$_SESSION['feld2c'] = $feld2;
	$_SESSION['feld3c'] = $feld3;
	
	$_SESSION['sort1c'] = $sort1;
	$_SESSION['sort2c'] = $sort2;
	$_SESSION['sort3c'] = $sort3;

	$_SESSION['richtung1c'] = $richtung1;
	$_SESSION['richtung2c'] = $richtung2;
	$_SESSION['richtung3c'] = $richtung3;

if ($abfrage) { // es existiert ein Abfrageergebnis
	//if (mysql_num_rows($abfrage)>0) { // es existiert ein Abfrageergebnis

	// falls das Abfrageergebnis eindeutig ist (nur eine Ergebniszeile) wird sofort die Termin-Übersicht geladen
	// und die Kunden-ID als Variable übergeben
	
	if (mysqli_num_rows($abfrage) == 1) {
	
		$daten = mysqli_fetch_array($abfrage);
		$kunden_id = $daten[0];
	
		// Nachladen der Termin-Übersicht-Seite im oberen Frame, Kunden-Id wird als Variable übergeben
		echo "<script>onload=parent['suche_oben'].location.href='such_check.php?kd_id=$kunden_id'</script>";
	}

	echo "<td valign = \"top\">";
	echo "<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" width=\"100%\">";
	$spalten = mysqli_num_fields($abfrage);

	$anzahl = mysqli_num_rows($abfrage);

	echo "<tr><td bgcolor = \"green\" align = \"left\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold; line-height:150%;\">$anzahl Datensätze gefunden!</span></td></tr>";

	//Ausgabe der kundenliste +++++++++++++++++++++++++++++++++++++++++++++++

	//zähler der datensätze für bg_colour der zeilen
	$z=0;

	//print_r(mysqli_fetch_field($abfrage));
	
	//die beiden hintergrundfarben
	$bg1 = "#eeeeee";
	$bg2 = "#dddddd";

	echo "<tr>";
	// Tabellenkopf

/*	while ($feldname = mysqli_fetch_field($abfrage)) {
		echo $feldname->name;
		echo "<th align = \"left\">$feldname -> name</th>";
		print_r($feldname);
	}*/


	for($i = 0; $i < mysqli_num_fields($abfrage); $i++)				// Anzahl der Tabellenzellen pro Zeile
	{
		$row = mysqli_fetch_assoc($abfrage);
		$feldname = array_keys($row);
		mysqli_data_seek($abfrage, 0);
		echo "<th align = \"left\">$feldname[$i]</th>";
	}
	echo "</tr>\n";													// Tabellenkopf Ende
	while($zeile = mysqli_fetch_row($abfrage))						// Schleife für Daten-Zeilen
	{
		echo "<tr ";
		echo "bgcolor=";
		$bg=($z++ % 2) ? $bg1 : $bg2;
		echo $bg;
		echo ">";
		for($i = 0; $i < mysqli_num_fields($abfrage); $i++)			// Schleife für Felder
		{	
			  echo "<td class=\"liste\" align = \"left\"><a href=\"such_check.php?kd_id=$zeile[0]\" target=\"_self\">" . $zeile[$i] . "</a></td>";
		}					
		echo "</tr>";
    }

	echo "</table>";
	echo "</td>";
	$check = "0";
} // ende if es existiert ein Abfrageergebnis
else {	// keine Datensätze gefunden
echo "<tr><td bgcolor = \"green\" align = \"center\" colspan = \"$spalten\"><span style=\"color:white; font-weight:bold;\">Keine Datensätze gefunden!</span></td></tr>";
$check = "0";
//}
} // ende if es existiert ein Abfrageergebnis
} // ende else Daten sind eingegeben


?>

</tr></table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>

<?php

} // Ende IF SUCHEN


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++ NICHTS GEDRüCKT - SCRIPT STARTET++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

else {
?>

<!DOCTYPE html>
<html lang="de">
<head>
<title>Suche</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align="center">
<table width="700" border="0" cellpadding="4" cellspacing="4">
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">

<!-- Tabelle f�r Formular ----------------------------------------------------------------------- -->

<table width="100%"  border="0" cellspacing="0" cellpadding="3">
<form name="input" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
  <tr >
    <td bgcolor="<?php echo "$bgsuche"; ?>" ><img src="../images/100px.gif" width="1" height="2" /></td>
    <td bgcolor="<?php echo "$bgsuche"; ?>" ></td>
    <td bgcolor="<?php echo "$bgsortierung"; ?>" ></td>
    <td colspan="2" bgcolor="<?php echo "$bgsortierung"; ?>" ></td>
	<td bgcolor="<?php echo "$bgsubmitt"; ?>"></td>
  </tr>
  <tr >
    <td bgcolor="<?php echo "$bgsuche"; ?>" ><span style = "font-weight:bold;">Suchbegriff</span></td>
    <td bgcolor="<?php echo "$bgsuche"; ?>" ><span style = "font-weight:bold;">Suchkriterium</span></td>
    <td bgcolor="<?php echo "$bgsortierung"; ?>" ><span style = "font-weight:bold;">Sortierung nach</span></td>
    <td colspan="2" bgcolor="<?php echo "$bgsortierung"; ?>" ><span style = "font-weight:bold;">Sortierrichtung</span></td>
	<td rowspan = "4" bgcolor="<?php echo "$bgsubmitt"; ?>"><input type="submit" name="suchen" value="Suchen" class = "suche"></td>
  </tr>
  <tr>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><input type="text" name="wert1_neu" maxlength = "20" size ="20" class = "eingabe" value = "<?php echo "$wert1"; ?>"></td>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><?php echo "<select name=\"feld1_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($begriff1); $i++)	{				// Anzahl der Zeilen im Select-Feld
					if ($begriff1[$i] == $feld1) {							
						echo " <option selected>$feld1</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$begriff1[$i]</option>";				// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
	<td bgcolor="<?php echo "$bgsortierung"; ?>"><?php echo "<select name=\"sort1_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($sortierung1); $i++)	{			// Anzahl der Zeilen im Select-Feld
					if ($sortierung1[$i] == $sort1) {							
						echo " <option selected>$sort1</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$sortierung1[$i]</option>";			// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
     <td bgcolor="<?php echo "$bgsortierung"; ?>"><label><input type="radio" name="richtung1" value="ASC" checked>auf</label></td>
	 <td bgcolor="<?php echo "$bgsortierung"; ?>"><label><input type="radio" name="richtung1" value="DESC">ab</label></td>
  </tr>
  <tr>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><input type="text" name="wert2_neu" maxlength = "20" size ="20" class = "eingabe" value = "<?php echo "$wert2"; ?>"></td>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><?php echo "<select name=\"feld2_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($begriff2); $i++)	{				// Anzahl der Zeilen im Select-Feld
					if ($begriff2[$i] == $feld2) {							
						echo " <option selected>$feld2</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$begriff2[$i]</option>";				// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
	<td bgcolor="<?php echo "$bgsortierung"; ?>"><?php echo "<select name=\"sort2_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($sortierung2); $i++)	{			// Anzahl der Zeilen im Select-Feld
					if ($sortierung2[$i] == $sort2) {							
						echo " <option selected>$sort2</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$sortierung2[$i]</option>";			// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
     <td bgcolor="<?php echo "$bgsortierung"; ?>"><label><input type="radio" name="richtung2" value="ASC" checked>auf</label></td>
	 <td bgcolor="<?php echo "$bgsortierung"; ?>"><label><input type="radio" name="richtung2" value="DESC">ab</label></td>
  </tr>
    <tr>
    <td bgcolor="<?php echo "$bgsuche"; ?>"><input type="text" name="wert3_neu" maxlength = "20" size ="20" class = "eingabe" value = "<?php echo "$wert3"; ?>"></td>
     <td bgcolor="<?php echo "$bgsuche"; ?>"><?php echo "<select name=\"feld3_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($begriff3); $i++)	{				// Anzahl der Zeilen im Select-Feld
					if ($begriff3[$i] == $feld3) {							
						echo " <option selected>$feld3</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$begriff3[$i]</option>";				// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
	<td bgcolor="<?php echo "$bgsortierung"; ?>"><?php echo "<select name=\"sort3_neu\" class = \"eingabe\">";
				for ($i = 0; $i < count($sortierung3); $i++)	{			// Anzahl der Zeilen im Select-Feld
					if ($sortierung3[$i] == $sort3) {							
						echo " <option selected>$sort3</option>";			// bein Begriff wurde ausgewählt
					}
					else {
						echo " <option>$sortierung3[$i]</option>";			// kein Begriff ausgewählt
					}
				}
	 		echo "</select>";
	 	?>
	 </td>
     <td bgcolor="<?php echo "$bgsortierung"; ?>"><label><input type="radio" name="richtung3" value="ASC" checked>auf</label></td>
	 <td bgcolor="<?php echo "$bgsortierung"; ?>"><label><input type="radio" name="richtung3" value="DESC">ab</label></td>
  </tr>
    <tr >
    <td bgcolor="<?php echo "$bgsuche"; ?>" ><img src="../images/100px.gif" width="1" height="2" /></td>
    <td bgcolor="<?php echo "$bgsuche"; ?>" ></td>
    <td bgcolor="<?php echo "$bgsortierung"; ?>" ></td>
    <td colspan="2" bgcolor="<?php echo "$bgsortierung"; ?>" ></td>
	<td bgcolor="<?php echo "$bgsubmitt"; ?>"></td>
  </tr>
  </form>
</table>

<!-- Ende Tabelle für Formular ----------------------------------------------------------------------- -->

</td></tr>
<!-- Ende Zeile Abfrage / Start Zeile Ergebnisse ++++++++++++++++++++++++++++++++++ // -->
</table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>

<?php 	// Nachladen des Daten-Eingabe-Formulars im unteren Frame, leer - ohne Kunde oder Termin
		if ($gruppe == 'Aussendienst') {
			echo "<script>onload=parent['suche_unten'].location.href='kunde_neu_ad.php'</script>";
		}
		else {
			echo "<script>onload=parent['suche_unten'].location.href='kunde_neu.php'</script>";
		}
	} // ende else nicht gedrückt
?>