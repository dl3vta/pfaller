<?php
include ("include/ini.php");		// Session-Lifetime
session_start();
include ("../include/init.php");
sessiondauer();

$gruppe = $_SESSION['benutzer_gruppen'];			// Gruppe des eingeloggten Benutzers
$user = $_SESSION['benutzer_kurz'];					// Kürzel des eingeloggten Benutzers
$kunden_id = $_GET["kd_id"];				// aus suche.php wird die Kunden-ID übermittelt

// Hilfsvariablen zur Bestimmung der Breite von Trennstrichen etc. ---------------------------------------------------
	$spank = 12;						// Anzahl der anzuzeigenden Spalten für Kundendaten 		- ANPASSEN!!
	$spant = 20;						// Anzahl der anzuzeigenden Spalten für Termindaten 		- ANPASSEN!!
	
	$spankrem = $spank - 1;				// Anzahl der anzuzeigenden Spalten für Bemerkung Kunde		- nicht verändern!!
	$spantrem = $spant - 1;				// Anzahl der anzuzeigenden Spalten für Bemerkung Termin	- nicht verändern!!
				
	$spankadmin = $spank + 2;			// Kundendaten - Administrator (+ bearbeiten/löschen) 		- nicht verändern!!
	$spankaq = $spank + 1;				// Kundendaten - Telefonist (+ bearbeiten) 					- nicht verändern!!

	$spantadmin = $spant + 2;			// Termindaten - Administrator (+ bearbeiten/löschen) 		- nicht verändern!!
	$spantaq = $spant + 1;				// Termindaten - Telefonist (+ bearbeiten) 					- nicht verändern!!

// ---------------------------------------------------------------------------------------------------------------------


// Datenbankabfrage -----------------------------------------------------------------------------------------

	// Kundendaten
		
	$sql = "SELECT kunden_id, vorname, name, plz, ort, ortsteil, strasse, ";
	$sql .= "vorwahl1, telefon, vorwahl2, fax, email, branche, geboren, rem_kunde, ";
	$sql .= "wiedervorlage, vertragskunde, belegt, nichtkunde, verzogen, neukunde, ne, handy, datenschutz ";
	$sql .= "FROM kunden, vorname, name, poo, plz, ort, ortsteil, vorwahl1, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id'  ";
	$sql .= "And kunden.vorname_id = vorname.vorname_id  AND kunden.name_id = name.name_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= "And kunden.vorwahl1_id = vorwahl1.vorwahl1_id And kunden.vorwahl2_id = vorwahl2.vorwahl2_id  And kunden.branche_id = branche.branche_id";
	$ergebnis = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($ergebnis);

	if (empty($kunde[geboren]) OR $kunde[geboren] == "0000-00-00") {		// kein Geburtsdatum in Datenbank
		$kunde[geboren] = "";												// Anzeige bleibt leer
	}
	else {																	// Geburtsdatum vorhanden
		$kunde[geboren] = mysqldate_in_de($kunde[geboren]);					// MySQL- in deutsches Datum umwandeln
	}
	
	// Termindaten

	$sql = "SELECT termin_id, telefonist, aquiriert, termin, zeit, aussendienst, erledigt_date, ";
	$sql .= " termin.wiedervorlage_date, w_zeit, ";
	$sql .= " ges_alt, tarif_alt, von, bis, ";
	$sql .= " ges_neu, tarif_neu, seit, vonr, ";
	$sql .= " rem_termin, nichtkunde, abschluss, wiedervorlage, termin.storno, alt, kalt, einzug, edit ";
	$sql .= " FROM termin, produkt_alt, ges_alt, tarif_alt, produkt_neu, ges_neu, tarif_neu ";
	$sql .= " WHERE termin.kd_id = '$kunden_id' ";
	$sql .= " And termin.produkt_alt_id = produkt_alt.produkt_alt_id And produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
	$sql .= " And termin.produkt_neu_id = produkt_neu.produkt_neu_id And produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
	$sql .= " ORDER BY edit DESC, termin_id DESC ";
	$abfrage = myqueryi($db, $sql);
	$anzahl = mysqli_num_rows($abfrage);
	
// Ende Datenbankabfrage ---------------------------------------------------------------------------------------

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html xlang="de">
<head>
<title>Kd-Termine</title>
	<!-- allgemein/such_check.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}

#suchergebnis {
	FONT-SIZE: 8pt; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
	BORDER-LEFT: 1px solid #203C5E; BORDER-RIGHT: 1px solid #203C5E; BORDER-TOP: 1px solid #203C5E; BORDER-BOTTOM: 1px solid #203C5E;
	PADDING-LEFT: 10px; PADDING-RIGHT: 10px; PADDING-TOP: 2px; PADDING-BOTTOM: 2px; 
	TEXT-ALIGN: center; FLOAT: left; MARGIN: 1px; TEXT-DECORATION: none; COLOR: #ffffff;  BACKGROUND-COLOR: #ffffcc;
}

#sucheneu {
	FONT-SIZE: 8pt; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;
	BORDER-LEFT: 1px solid #203C5E; BORDER-RIGHT: 1px solid #203C5E; BORDER-TOP: 1px solid #203C5E; BORDER-BOTTOM: 1px solid #203C5E;
	PADDING-LEFT: 10px; PADDING-RIGHT: 10px; PADDING-TOP: 2px; PADDING-BOTTOM: 2px; 
	TEXT-ALIGN: center; FLOAT: left; MARGIN: 1px; TEXT-DECORATION: none; COLOR: #ffffff;  BACKGROUND-COLOR: #ccffff;
}
-->
</style>
</head>
<body>
<div align = "center">
<table width="1200" border="0" cellpadding="0" cellspacing="3">
	<?php	// Überschrift: Daten Kunde
		echo "<tr bgcolor = \"#eeeeee\">";
		echo "<td valign = \"middle\" align = \"left\" ><span style=\"font-weight:bold; font-size: 14px; color: blue; line-height:160%; padding-left:10px;\">Kunden-ID: $kunde[kunden_id]&nbsp;-&nbsp;$kunde[vorname]&nbsp;$kunde[name] - $anzahl&nbsp;Datensätze</span</td>";
		echo "</tr>";
	?>
<tr>
<td>

<table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#eeeeee">		<!-- Beginn Tabelle Kundendaten -->
<?php
	// Tabellenkopf Kundendaten ----------------------------------------------------------------------------------------------
		echo "<tr>";
	
		echo "<td>&nbsp;</td>";				// Leerzelle für Bearbeiten-Button
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">PLZ</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"60\"><span style=\"font-weight:bold; line-height:160%;\">Ort</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"80\"><span style=\"font-weight:bold; line-height:160%;\">Ortsteil</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"100\"><span style=\"font-weight:bold; line-height:160%;\">Straße</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Vorwahl</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Telefon</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Vorwahl2</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Telefon2</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"80\"><span style=\"font-weight:bold; line-height:160%;\">E-Mail</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">geboren</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"100\"><span style=\"font-weight:bold; line-height:160%;\">Branche</span></td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Datenschutz</span></td>";
		if ($gruppe == 'Administrator') {
			echo "<td>&nbsp;</td>";				// Leerzelle für löschen-Button (nur für Administrator!!)
		}
		echo "</tr>";
	// Ende Tabellenkopf Kundendaten -----------------------------------------------------------------------------------------
	
	// Trennstrich zwischen Tabellenkopf und -daten für Kundendaten
	
		if ($gruppe == 'Administrator') {
			echo "<tr><td colspan = \"$spankadmin\" valign = \"middle\"><hr></td></tr>";
		}
		else {
			echo "<tr><td colspan = \"$spankaq\" valign = \"middle\"><hr></td></tr>";
		}
	
	// Anzeige Kundendaten ------------------------------------------------------------------------------------------------------

		echo "<tr>";
		// Bearbeiten-Button für Kundendaten
		if ($gruppe == 'Aussendienst') {
		echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"kunde_neu_ad.php?kd_id=$kunde[0]\" target=\"suche_unten\">
				  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"Kundendaten bearbeiten\" title=\"Kundendaten bearbeiten\" border=\"0\" /></a></td>";
		}
		else {
			echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"kunde_neu.php?kd_id=$kunde[0]\" target=\"suche_unten\">
				  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"Kundendaten bearbeiten\" title=\"Kundendaten bearbeiten\" border=\"0\" /></a></td>";
		}
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[plz]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"60\">$kunde[ort]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"80\">$kunde[ortsteil]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"100\">$kunde[strasse]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[vorwahl1]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[telefon]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[vorwahl2]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[fax]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"80\">$kunde[email]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"70\">$kunde[geboren]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"100\">$kunde[branche]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"40\">";
			if ($kunde[datenschutz] == 1) {
				echo "erhalten";
			}
			else {
				echo "---";
			}
		echo "</td>";

		if ($gruppe == 'Administrator') {		// löschen-Button: User ist Administrator darf Kundendaten löschen
			echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"loeschen.php?kd_id=$kunde[0]\" target=\"_self\">
				  <img src=\"../images/loesche.png\" alt=\"Kunde löschen\" title=\"Kunde löschen\" border=\"0\" /></a></td>";
		}
		echo "</tr>";
		
		if (!empty($kunde[rem_kunde])) {		// Diese Zeile erscheint nur, wenn eine Kundenbemerkung in der Datenbank steht
			if ($gruppe == 'Administrator') {
				echo "<tr>";
				echo "<td>&nbsp;</td>";				// Leerzelle für obigen Bearbeiten-Button
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spankrem\">$kunde[rem_kunde]</td>";
				echo "<td>&nbsp;</td>";				// Leerzelle für obigen löschen-Button
				echo "</tr>";
			}
			else {
				echo "<tr>";
				echo "<td>&nbsp;</td>";				// Leerzelle für obigen Bearbeiten-Button
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spankrem\">$kunde[rem_kunde]</td>";
				echo "</tr>";
			}
		}	// Ende Zeile Kundenbemerkung
	// Ende Anzeige Kundendaten -------------------------------------------------------------------------------------------
?>
</table>																				<!-- Ende Tabelle Kundendaten -->
</td></tr>

<tr><td>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">		<!-- Beginn Tabelle Termindaten -->
<tr><td valign = "top">

<?php
	// Tabellenkopf Termindaten --------------------------------------------------------------------------------------------

		echo "<tr>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">&nbsp;</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Typ</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Termin</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"50\"><span style=\"font-weight:bold; line-height:160%;\">Zeit</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Tel.</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">angerufen</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">AD</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">erledigt</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">W-Datum</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"50\"><span style=\"font-weight:bold; line-height:160%;\">W-Zeit</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Ges.-alt</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Tarif-alt</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">von</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">bis</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Ges.-neu</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Tarif-neu</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">seit</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">VoNr</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Storno</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Bank</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">T_ID</span></td>";
		
		if ($gruppe == 'Administrator') {
			echo "<td>&nbsp;</td>";				// Leerzelle für löschen-Button (nur für Administrator!!)
		}
		
		echo "</tr>";
	// Ende Tabellenkopf Termindaten ------------------------------------------------------------------------------------------
	
	// Trennstrich zwischen Tabellenkopf und -daten für Termindaten
	
	if ($gruppe == 'Administrator') {
		echo "<tr><td colspan = \"$spantadmin\" valign = \"middle\"><hr></td></tr>";
		
	}
	else {
		echo "<tr><td colspan = \"$spantaq\" valign = \"middle\"><hr></td></tr>";
	}		
		
	// Anzeige Termindaten ------------------------------------------------------------------------------------------------------
	
	for ($i = 0; $i < $anzahl; $i++) {
				
		$termin = mysqli_fetch_array($abfrage);
		
	// deutsches Datum für Termin
		if (empty($termin[termin]) OR $termin[termin] == "0000-00-00") { $termin[termin] = ""; }
		else { $termin[termin] = mysqldate_in_de($termin[termin]); }
		
	// deutsches Datum für erledigt
		if (empty($termin[erledigt_date]) OR $termin[erledigt_date] == "0000-00-00") { $termin[erledigt_date] = ""; }
		else { $termin[erledigt_date] = mysqldate_in_de($termin[erledigt_date]); }
		
	// deutsches Datum für Wiedervorlage
		if (empty($termin[wiedervorlage_date]) OR $termin[wiedervorlage_date] == "0000-00-00") { $termin[wiedervorlage_date] = ""; }
		else { $termin[wiedervorlage_date] = mysqldate_in_de($termin[wiedervorlage_date]); }
		
	// deutsches Datum für von
		if (empty($termin[von]) OR $termin[von] == "0000-00-00") { $termin[von] = ""; }
		else { $termin[von] = mysqldate_in_de($termin[von]); }
		
	// deutsches Datum für bis
		if (empty($termin[bis]) OR $termin[bis] == "0000-00-00") { $termin[bis] = ""; }
		else { $termin[bis] = mysqldate_in_de($termin[bis]); }
		
	// deutsches Datum für seit
		if (empty($termin[seit]) OR $termin[seit] == "0000-00-00") { $termin[seit] = ""; }
		else { $termin[seit] = mysqldate_in_de($termin[seit]); }
		
	// 00:00 bei Zeit ausblenden
		if (empty($termin[zeit]) OR $termin[zeit] == "00:00") { $termin[zeit] = ""; }
		
	// 00:00 bei W-Zeit ausblenden
		if (empty($termin[w_zeit]) OR $termin[w_zeit] == "00:00") { $termin[w_zeit] = ""; }

	// aquiriert in Datum und Zeit trennen, deutsches Datum, bei Zeit sekunden ausblenden
		$aq = explode(" ", $termin[aquiriert]);
		$aq_datum = mysqldate_in_de($aq[0]);
		$aq_zeit = substr($aq[1], 0, 5);
		if ($aq_zeit == "00:00") { $aq = $aq_datum; }
		else { $aq = $aq_datum . "<br>" .$aq_zeit; }
		
	//storno
		if ($termin[storno] != '0') { $storno = $termin[storno]; }
		else { $storno = ""; }
		
	// einzug
	
		if (empty($termin[einzug])) { $einzug = ""; }
		else { $einzug = "1"; }

	//	$einzug = $termin[einzug];
		
	// Termintyp ermitteln
		if (!empty($termin[storno])) {
			$typ = "ST";
			$bg = "#ff3333";
		}
		elseif ($termin[nichtkunde] == '1') {
			$typ = "NK";
			$bg = "#999999";
		}
		elseif ($termin[wiedervorlage] == '1' AND $termin[alt] == '0') {
			$typ = "WVL";
			$bg = "#FFCCCC";
		}
		elseif ($termin[wiedervorlage] == '1' AND $termin[alt] == '1') {
			$typ = "WVLalt";
			$bg = "#FFE6CC";
		}
		elseif ($termin[abschluss] == '1' AND $termin[kalt] == '0' ) {
			$typ = "AB";
			$bg = "#00FF00";
		}
		elseif ($termin[abschluss] == '1' AND $termin[kalt] == '1' ) {
			$typ = "KAB";
			$bg = "#00A8FF";
		}
		elseif ($termin[abschluss] == '0' AND $termin[kalt] == '1' AND $termin[erledigt_date] = '0000-00-00') {
			$typ = "Kalt";
			$bg = "#00EAFF";
		}
		elseif ($termin[erledigt_date] > '0000-00-00' AND $termin[wiedervorlage] == '0' AND $termin[abschluss] == '0' ) {
			$typ = "ToAs";
			$bg = "#FFD700";
		}
		else {
			$typ = "ToA";
			$bg = "#FFFF00";
		}

		
		// Beginn Datenausgabe
		
		echo "<tr bgcolor=$bg>";
		
		if (($gruppe == "Aussendienst" AND $termin[wiedervorlage] == '0' AND $user !== $termin[aussendienst]) OR ($gruppe == 'Aquise' AND ($typ == "ST" OR $typ == "AB" OR $typ =="KAB"))) {					// Außendienstler darf nur seine eigenen Daten bearbeiten, ansonsten Leerzelle anstatt Bearbeiten-PIC
			echo "<td valign = \"middle\" align = \"center\" width = \"30\">&nbsp;</td>";
		}
		//elseif ($gruppe == 'Aquise' AND ($typ == "ST" OR $typ == "AB" OR $typ =="KAB")) {		//Gruppe Aquise darf keine Abschlüsse bearbeiten - Leerzelle statt Bearbeiten-Bild
			//echo "<td valign = \"middle\" align = \"center\" width = \"30\">&nbsp;</td>";
		//}
		else {
			if ($gruppe == 'Aussendienst') {
				echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"kunde_neu_ad.php?t_id=$termin[0]\" target=\"suche_unten\">
				  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"Termin bearbeiten\" title=\"Termin bearbeiten\" border=\"0\" /></a></td>";
			}
			else {
				echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"kunde_neu.php?t_id=$termin[0]\" target=\"suche_unten\">
				  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"Termin bearbeiten\" title=\"Termin bearbeiten\" border=\"0\" /></a></td>";
			}
		}
		//else {
			//echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"kunde_neu.php?t_id=$termin[0]\" target=\"suche_unten\">
			//	  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"Termin bearbeiten\" title=\"Termin bearbeiten\" border=\"0\" /></a></td>";
		//}
		
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$typ</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[termin]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"50\">$termin[zeit]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[telefonist]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$aq</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[aussendienst]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[erledigt_date]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[wiedervorlage_date]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"50\">$termin[w_zeit]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[ges_alt]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[tarif_alt]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[von]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[bis]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[ges_neu]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[tarif_neu]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[seit]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[vonr]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$storno</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$einzug</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[termin_id]</td>";
		
		if ($gruppe == 'Administrator') {		// löschen-Button: User ist Administrator darf Termindaten löschen
			echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"loeschen.php?t_id=$termin[0]\" target=\"_self\">
				  <img src=\"../images/loesche.png\" alt=\"Termin löschen\" title=\"Termin löschen\" border=\"0\" /></a></td>";
		}
		echo "</tr>";
		
		if (!empty($termin[rem_termin])) {		// Diese Zeile erscheint nur, wenn eine Terminbemerkung in der Datenbank steht
			if ($gruppe == 'Administrator') {
				echo "<tr bgcolor=\"$bg\">";
				echo "<td>&nbsp;</td>";				// Leerzelle für obigen Bearbeiten-Button
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spantrem\">$termin[rem_termin]</td>";
				echo "<td>&nbsp;</td>";				// Leerzelle für obigen löschen-Button
				echo "</tr>";
			}
			else {
				echo "<tr bgcolor=\"$bg\">";
				echo "<td>&nbsp;</td>";				// Leerzelle für obigen Bearbeiten-Button
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spantrem\">$termin[rem_termin]</td>";
				echo "</tr>";
			}
		}	// Ende Zeile Terminbemerkung
					
			
			// Trennstrich zwischen den einzelnen Terminen
			if ($gruppe == 'Administrator') {
				echo "<tr><td colspan = \"$spantadmin\" valign = \"middle\"><hr></td></tr>";
				
			}
			else {
				echo "<tr><td colspan = \"$spantaq\" valign = \"middle\"><hr></td></tr>";
			}
			
	}	// Ende Anzeige Termindaten ------------------------------------------------------------------------------------------------
?>
</table>
</td></tr>

<tr><td>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">		<!-- Beginn Tabelle Legende-->
	<tr>
	<td valign = "middle" align = "center" bgcolor ="#eeeeee"><span style = "line-height:160%">Legende:</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFFF00"><span style = "line-height:160%">ToA: Termin</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFD700"><span style = "line-height:160%">ToAs: Termin bearbeitet</span></td>
	<td valign = "middle" align = "center" bgcolor ="#00FF00"><span style = "line-height:160%">AB: Abschluss</span></td>
	<td valign = "middle" align = "center" bgcolor ="#00EAFF"><span style = "line-height:160%">kalt: kalter Termin</span></td>
	<td valign = "middle" align = "center" bgcolor ="#00A8FF"><span style = "line-height:160%">KAB: kalter Abschluss</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFCCCC"><span style = "line-height:160%">WVL: WVL aktiv</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFE6CC"><span style = "line-height:160%">WVLalt: WVL abgeschlossen</span></td>
	<td valign = "middle" align = "center" bgcolor ="#999999"><span style = "line-height:160%">NK: Nichtkunde</span></td>
	<td valign = "middle" align = "center" bgcolor ="#ff3333"><span style = "line-height:160%">ST: Storno</span></td>
	</tr>
	</table>
	</td>
</tr>
<?php

// Rücksprung zum aufrufenden Programmmodul
	if ($gruppe == 'Administrator') {
		echo "<tr bgcolor=\"#eeeeee\"><td colspan = \"$spantadmin\" align = \"center\">";
			echo "<table><tr>";
				echo"<td><DIV id=suchergebnis><a href=\"suche.php?check=1\" target=\"_self\">Suchergebnis</a></DIV></td>";
				echo"<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				echo"<td><DIV id=sucheneu><a href=\"suche.php\" target=\"_self\">neue Suche</a></DIV></td>";
			echo"</tr></table>";
		echo "</td></tr>";
	}
	else {
		echo "<tr bgcolor=\"#eeeeee\"><td colspan = \"$spantaq\" align = \"center\">";
			echo "<table><tr>";
				echo"<td><DIV id=suchergebnis><a href=\"suche.php?check=1\" target=\"_self\">Suchergebnis</a></DIV></td>";
				echo"<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				echo"<td><DIV id=sucheneu><a href=\"suche.php\" target=\"_self\">neue Suche</a></DIV></td>";
			echo"</tr></table>";
		echo "</td></tr>";
	}
?>
</td></tr></table>																		<!-- Ende Tabelle Termindaten -->
</td></tr></table>
</div>
</body>
</html>

<?php	// Nachladen des Daten-Eingabe-Formulars im unteren Frame, leer - ohne Kunde oder Termin
if ($gruppe == 'Aussendienst') {
	echo "<script>onload=parent['suche_unten'].location.href='kunde_neu_ad.php'</script>";
}
else {
	echo "<script>onload=parent['suche_unten'].location.href='kunde_neu.php'</script>";
}
?>