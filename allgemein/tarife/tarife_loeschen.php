<?php

// -------------------------------------------------------------------------------------------------//
// mit diesem Script k�nnen Tarife f�r eine Gesellschaft komplett gel�scht werden					//
// nur ein Administrator darf das Script nutzen														//
// Daten (produkt-id, auswahl) kommen von tarife.php												//
// vor dem L�schen erfolgt eine Sicherheitsabfrage													//
// wenn Termine vorhanden sind (nur f�r diese gesellschaft!!), werden diese auf tarif_id =1 gesetzt	//
// falls der Tarif sonst nicht mehr verwendet wird, wird er ebenfalls gel�scht						//
// nach dem L�schen wird eine Meldung �ber Erfolg oder Misserfolg der L�schaktion ausgegeben		//
// danach erfolgt ein Refresh von ges_select und tarife.php											//
// -------------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();

$gruppe = $_SESSION['benutzer_gruppen'];			// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php

//  Feststellen, ob Benutzer überhaupt löschen darf --------------------------------------------------------------

if ($gruppe != "Administrator") {
	echo "<script>location.href='../../forbidden.php'</script>";	// kein Administrator - Zugriff nicht erlaubt
}


// POST- und GET-Variablen --------------------------------------------------------------------------------------

$ja = $_POST["ja"];						// Ja-Button, Daten sollen gelöscht werden
$nein = $_POST["nein"];					// Nein-Button, ersetzen wird abgebrochen
$ok = $_POST["ok"];						// Operation erfolgreich, Refresh der Frames

$produktalt = $_GET["produktalt"];			// Produkt-ID kommt vom tarife.php
$ges = $_GET["ges"];  						// von tarife.php
$tarifalt = $_GET["tarifalt"];  			// von tarife.php
$auswahl = $_GET["auswahl"];				// von tarife.php, zur Steuerung alte/neue Gesellschaft

// Auswahl alte/neue Gesellschaften
if ($auswahl == "neu") { 
	$gesellschaft = "ges_neu";
	$gesellschaft_id = "ges_neu_id";
	$tarif = "tarif_neu";
	$tarif_id = "tarif_neu_id";
	$produkt = "produkt_neu";
	$produkt_id = "produkt_neu_id";
}
else {
	$gesellschaft = "ges_alt";
	$gesellschaft_id = "ges_alt_id";
	$tarif = "tarif_alt";
	$tarif_id = "tarif_alt_id";
	$produkt = "produkt_alt";
	$produkt_id = "produkt_alt_id";
}


// Bearbeitung war erfolgreich, Refresh der Frames ---------------------------------
if (isset($ok)) {
	//flush();				// Ausgabepuffer auf den Bildschirm schreiben
	//usleep(3000000);		// 3 Sekunden warten, dann Refresh
	echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
	echo "<script>onload=location.href='tarife.php?ges=$ges&auswahl=$auswahl'</script>";
	echo "<script>onload=location.href='ges_bearbeiten.php'</script>";
}

// L�schen ist abgebrochen, R�cksprung zu tarife.php ----------------------------------------------------------

elseif (isset($nein)) {										// Nein-Button gedr�ckt
	$auswahl = $_POST["auswahl"];				// auswahl, kommt als hidden vom Script, geht zur�ck an tarife.php
	$ges = $_POST["ges"];						// auswahl, kommt als hidden vom Script, geht zur�ck an tarife.php
	
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
	echo "<script>onload=location.href='tarife.php?ges=$ges&auswahl=$auswahl'</script>";
}
// ------------------------------------------------------------------------------------------------------------

// Tarif soll gel�scht werden -----------------------------------------------------------------------------

elseif (isset($ja)) {									// Ja-Button gedr�ckt

	$produktalt = $_POST["produktalt"]; 		// Produkt wird als hidden-Feld �bertragen
	$ges = $_POST["ges"]; 						// Gesellschaft wird als hidden-Feld �bertragen
	$tarifalt = $_POST["tarifalt"];  			// Tarif wird als hidden-Feld �bertragen
	$auswahl = $_POST["auswahl"];				// auswahl, kommt als hidden vom Script, geht zur�ck an tarife.php
	
	// Ermittlung, ob noch Termine vorhanden sind ----------------------------------------------------------------------
	$sql = " SELECT termin_id FROM termin WHERE termin.$produkt_id = '$produktalt' ";
	$query = myqueryi($db, $sql);
	
	if (mysqli_num_rows($query) > 0) {					// Kunden vorhanden

		// Ermittlung der neuen produkt_id (mit tarif_id = 1)
		$sql  = "SELECT $produkt_id FROM $produkt, $gesellschaft ";
		$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id AND $gesellschaft.$gesellschaft = '$ges' ";
		$sql .= " AND $produkt.$tarif_id = '1' ";
		$produktquery = myqueryi($db, $sql);
		$produktneu = mysqli_fetch_array($produktquery);

		for ($i=0; $i<mysqli_num_rows($query); $i++) {		// alle Termine werden durchlaufen
			$ergebnis = mysqli_fetch_array($query);
			
			// UPDATE termine
			$sql = " UPDATE termin SET $produkt_id = '$produktneu[$produkt_id]' WHERE $produkt_id = '$produktalt' ";
			$abfrage = myqueryi($db, $sql);
		}	// ende alle Termine durchlaufen
	}	// ende Termine vorhanden
	
	// Sicherheitsabfrage, ob alle Termine geupdated wurden
	$sql = " SELECT termin_id FROM termin WHERE termin.$produkt_id = '$produktalt' ";
	$result = myqueryi($db, $sql);
			
	if (mysqli_num_rows($result) > 0) {						// noch Termine vorhanden
		$fehler = "<br />Es sind noch weitere Termine vorhanden - bitte das Script noch einmal ausführen!<br />";
	}
			
	else {													// alle Termine geupdated
			
		// produkt_id f�r diesen Tarif l�schen
		$sql = " DELETE FROM $produkt WHERE $produkt_id = '$produktalt' LIMIT 1 ";		// l�sche produkt-id
		$result = myqueryi($db, $sql);
					
		// Test, ob der Tarif noch irgendwo verwendet wird
		$sql = " SELECT $produkt_id FROM $produkt, $tarif WHERE $produkt.$tarif_id = $tarif.$tarif_id AND $tarif.$tarif = '$tarifalt' ";
		$produkttest = myqueryi($db, $sql);
		if (mysqli_num_rows($produkttest) == 0) {					// Tarif-ID in keinem weiteren Produkt - löschen
			$sql = " DELETE FROM $tarif WHERE $tarif = '$tarifalt' LIMIT 1";
			$tarifdelete = myqueryi($db, $sql);
		}
	}
?>
	
<!DOCTYPE html>
<html lang="de">
<head>
<title>Tarif löschen</title>
	<!-- allgemein/tarife/tarife_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
		  <?php	// Ausgabe der Meldung
					if (isset($fehler)) {	// noch Termine vorhanden
						echo "<tr bgcolor = \"#ff6633\"><td colspan=\"2\"><span style = \"font-weight:bold; color:black; font-size:10pt; line-height:200%;\">$fehler</span></td></tr>";
					}
					else {
						echo "<tr bgcolor = \"#ff6633\"><td colspan=\"2\" align=\"center\" valign=\"middle\"><span style = \"font-weight:bold; color:black; font-size:10pt; line-height:200%;\"><br />
						Der Tarif $tarifalt <br />der Gesellschaft $ges wurde gelöscht.<br /><br />
						</span></td></tr>";
					}
				?>
				<tr bgcolor = "moccasin">
					<form method="post" action= "<?php $_SERVER["PHP_SELF"] ?>">
					<td align="center" valign="middle"><input type="submit" name="ok" value="OK!" class = "nein"></td>
					</form>
				</tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php
} // ende l�schen ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


else {	// kein Button gedr�ckt - Script startet erstmals

// Ermittlung, ob Termine vorhanden sind ----------------------------------------------------------------------
$sql = " SELECT termin_id FROM termin WHERE termin.$produkt_id = '$produktalt' ";
$terminquery = myqueryi($db, $sql);
if (mysqli_num_rows($terminquery) > 0) {
	$bemerkung1 = "Für den Tarif $tarifalt der Gesellschaft $ges <br />existieren noch " . mysqli_num_rows($terminquery) . " Termine. <br />
				Soll der Termin trotzdem gelöscht werden?<br /><br />
				Alle betroffenen Termine verlieren dabei den Tarif,<br />die Gesellschaft bleibt erhalten.<br />
				Bei großen Datenmengen (vielen Terminen) muss der Vorgang u.U. wiederholt werden.<br />";
}
else {
	$bemerkung2 = "Für den Tarif $tarifalt <br />der Gesellschaft $ges <br />existieren keine Termine mehr.<br />
					Der Tarif kann gelöscht werden<br />";
}
?>

<!-- Hier f�ngt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Tarif löschen</title>
	<!-- allgemein/tarife/tarife_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><form name="loeschen" method="post" action="<?php $_SERVER[PHP_SELF] ?>">
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
          <tr bgcolor="moccasin"><td colspan="2" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Tarif <?php echo "<span style=\"color:red;\">$tarifalt</span> von Gesellschaft <span style=\"color:red;\">$ges</span>"; ?> löschen</span></td></tr>
		  <?php	// Ausgabe der Warnmeldung
					if (mysqli_num_rows($terminquery) > 0) {	// noch Termine vorhanden
						echo "<tr><td colspan=\"2\" align=\"center\" valign=\"middle\"><span style = \"color: red; font-weight: bold;\">$bemerkung1</span></td></tr>";
					}
					else {
						echo "<tr><td colspan=\"2\" align=\"center\" valign=\"middle\"><span style = \"color: black; font-weight: bold;\">$bemerkung2</span></td></tr>";
					}
					echo "<tr>";
						echo "<td align = \"center\" width = \"50%\">
									<input type=\"hidden\" name=\"produktalt\" value=\"$produktalt\">
									<input type=\"hidden\" name=\"tarifalt\" value=\"$tarifalt\">
									<input type=\"hidden\" name=\"auswahl\" value=\"$auswahl\">
									<input type=\"hidden\" name=\"ges\" value=\"$ges\">
								<input type=\"submit\" name=\"ja\" value=\"LÖSCHEN!\" class = \"ja\"></td>";
            			echo "<td align = \"center\" width = \"50%\"><input type=\"submit\" name=\"nein\" value=\"Abbrechen\" class=\"nein\"></td>";
					echo "</tr>";
				?>
        </table></form></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php
}	// ende else Script startet erstmalig, kein Button gedr�ckt

?>