<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script k�nnen Tarife bearbeitet oder gel�scht werden					            //
// Produkt-id und art des Tarifes /alt/neu) kommen von tarife.php								//
// alle betroffenen Termine werden geupdated                                                    //
// fall nicht mehr verwendet, wird der alte Tarif gel�scht                                      //
// nach dem L�schen wird eine Meldung �ber Erfolg oder Misserfolg der L�schaktion ausgegeben    //
// danach erfolgt der R�cksprung zu tarife.php			        				                //
// die Seite ges_select.php wird refreshed		   							                    //
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];			// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php
$bg_fehler = "#ff9966";								// Fehler-Farbe
unset($fehler);										// Fehlerausgabe wird zurückgesetzt

// POST- und GET-Variablen --------------------------------------------------------------------------------------

$ja = $_POST["ja"];						// Ja-Button, Daten sollen gelöscht werden
$nein = $_POST["nein"];					// Nein-Button, ersetzen wird abgebrochen
$ok = $_POST["ok"];						// Operation erfolgreich, Refresh der Frames

$produktalt = $_GET["produkt"];			// Produkt-ID kommt vom tarife.php
$auswahl = $_GET["auswahl"];				// von ges_select.php, zur Steuerung alte/neue Gesellschaft

$tarifinput = $_POST["tarifinput"];		// Tarif aus Textfeld
$tarifselect = $_POST["tarifselect"];		// Tarif aus Select-Feld
$tarifupdate = $_POST["tarifupdate"];		// Tarif aus Select-Feld

$ges = $_GET["ges"];
$tarifalt = $_GET["tarifalt"];

// Auswahl alte/neue Gesellschaften
if ($auswahl == "neu") { 
	$gesellschaft = "ges_neu";
	$gesellschaft_id = "ges_neu_id";
	$tarif = "tarif_neu";
	$tarif_id = "tarif_neu_id";
	$produkt = "produkt_neu";
	$produkt_id = "produkt_neu_id";
}
else {
	$gesellschaft = "ges_alt";
	$gesellschaft_id = "ges_alt_id";
	$tarif = "tarif_alt";
	$tarif_id = "tarif_alt_id";
	$produkt = "produkt_alt";
	$produkt_id = "produkt_alt_id";
}

// ---------------------------------------------------------------------------------------

// Bearbeitung ist abgebrochen, R�cksprung zu tarife.php ---------------------------------
if (isset($nein)) {										// Leeren-Button gedr�ckt
	$auswahl = $_POST["auswahl"]; 			// Auswahl wird als hidden-Feld aus dem Formular �bertragen
	$ges = $_POST["ges"];						// Gesellschaft wird als hidden-Feld aus dem Formular �bertragen
	echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
	echo "<script>location.href='tarife.php?auswahl=$auswahl&ges=$ges'</script>";
}
// ---------------------------------------------------------------------------------------

// Bearbeitung war erfolgreich, Refresh der Frames ---------------------------------
if (isset($ok)) {
	echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
	echo "<script>location.href='tarife.php?ges=$ges&auswahl=$auswahl'</script>";
	echo "<script>onload=location.href='ges_bearbeiten.php'</script>";
}

// Tarif soll geupdated werden-----------------------------------------------------

if (isset($update)) {
	$auswahl = $_POST["auswahl"]; 				// Auswahl wird als hidden-Feld aus dem Formular �bertragen
	$ges = $_POST["ges"];						// Gesellschaft wird als hidden-Feld aus dem Formular �bertragen
	$tarifupdate = quote_smart($tarifupdate);
	$tarifalt = $_POST["tarifalt"];
	
// keine Eingabe
	if (empty($tarifupdate)) {
		$fehler = "Sie müssen einen Tarif eingeben!";
		$bgupdate = $bg_fehler;
	}

// alter und neuer Tarif gleich!
	elseif ($tarifupdate == $tarifalt) {
		$fehler = "Diese Eingabe ist sinnlos";
		$bgupdate = $bg_fehler;
	}
	
// Eingabe korrekt
	else {
	
// ist neu eingegebener Tarif schon da - Fehlermeldung "Ersetzen"
// ansonsten update des Tarifes
		$sql = " SELECT $tarif_id FROM $tarif WHERE $tarif = '$tarifupdate' ";
		$query = myqueryi ($db, $sql);
	
		if (mysqli_num_rows($query) > 0) {
			$fehler = "Den Tarif $tarifupdate gibt es schon für die Gesellschaft $ges - bitte ERSETZEN (grauer Abschnitt)";
			$bgupdate = $bg_fehler;
		}
	
		else {
			$sql = " UPDATE $tarif SET $tarif = '$tarifupdate' WHERE $tarif = '$tarifalt' ";
			$query = myqueryi($db, $sql);

			// Refresh der einzelnen Seiten in den entsprechenden Frames
			echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
			echo "<script>onload=location.href='tarife.php?ges=$ges&auswahl=$auswahl'</script>";
		}
	}	// ende Eingabe korrekt
}	// ende update


// Tarif soll ersetzt werden
elseif (isset($ja)) {				// Ja-Button gedr�ckt
	$auswahl = $_POST["auswahl"]; 			// Auswahl wird als hidden-Feld aus dem Formular �bertragen
	$tarifinput = quote_smart($tarifinput);
	$ges = $_POST["ges"];					// Gesellschaft wird als hidden-Feld aus dem Formular �bertragen
	$tarifalt = $_POST["tarifalt"];		// Tarif alt wird als hidden-Feld aus dem Formular �bertragen
	
	$tarifinput = quote_smart($tarifinput);
	
	// Eingabefeld �berschreibt Select-Feld
	if (isset($tarifinput) AND $tarifinput != 'Tarif eingeben!') { $tarifneu = $tarifinput; }
	elseif (isset($tarifselect)) { $tarifneu = $tarifselect; }

// �berpr�fung der Eingabewerte --------------------------------------------------------------------------------------------------
	if ((empty($tarifinput) OR $tarifinput == 'Tarif eingeben!') AND empty($tarifselect)) {					// keine Eingaben gemacht
		$fehler = "Sie müssen einen Tarif auswählen oder eingeben!";
		$bgtarif = $bg_fehler;
	}
	
	elseif ($tarifneu == $tarifalt) {
		$fehler = "Diese Eingabe ist sinnlos";
		$bgtarif = $bg_fehler;
	}
	
//	eingaben vollzogen
	else {
		if (empty($fehler)) {
			?>

				<!-- Ausgabe solange der Rechner arbeitet -->
				<!DOCTYPE html>
				<html lang="de">
				<head>
				<title>Termin bearbeiten</title>
					<!-- allgemein/tarife/tarife_bearbeiten.php -->
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
				<style type="text/css">
				<!--
				body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }
				-->
				</style>
				</head><body><div align = "center">
					<table width="500" border="0" cellspacing="0" cellpadding="3">
 						<tr>
    						<td>
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
      								<tr>
        								<td>
											<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
												<tr bgcolor = "#ff6633">
													<td><span style = "font-weight:bold; color:black; font-size:10pt; line-height:200%;">
														<div align = "center">In Arbeit ...</div>
														Bei vielen Terminen kann unter Umständen die maximale Serverlaufzeit
														überschritten werden, in diesem Fall das Programm bitte noch einmal
														ausführen.
														</span>
													</td>
												</tr>
        									</table>
										</td>
      								</tr>
    							</table>
							</td>
  						</tr>
					</table>
				</div></body>
				</html>
<?php

		// �berpr�fung der Eingabewerte beendet
		// Auswertung startet
		//	Folgende F�lle sind m�glich: ----------------------------------------------
		//	1.		Tarif neu bereits vorhanden
		//	1.1.	Tarif neu in der gleichen Gesellschaft
		//	1.1.1. 	Termine mit altem Tarif
		//	1.1.2	keine Termine mit altem Tarif
		//	1.2.	Tarif neu in einer anderen Gesellschaft vorhanden
		//	1.2.1. 	Termine mit altem Tarif
		//	1.2.2	keine Termine mit altem Tarif
		//	2.		Tarif komplett neu
		// --------------------------------------------------------------------------

// Ermittlung produktalt-id und gesellschaft-id
		$sql  = " SELECT $produkt_id, $gesellschaft.$gesellschaft_id, $tarif.$tarif_id ";
		$sql .= " FROM $produkt, $gesellschaft, $tarif ";
		$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id AND $gesellschaft.$gesellschaft = '$ges' ";
		$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id AND $tarif = '$tarifalt' ";
		$produktaltquery = myqueryi ($db, $sql);
		
		$gesalt = mysqli_fetch_array($produktaltquery);
			mysqli_data_seek($produktaltquery, 0);

// Tarif_id ermitteln
		$sql1 = "SELECT $tarif_id FROM $tarif WHERE $tarif = '$tarifneu'";		// Tarif schon vorhanden
		$sql2 = "INSERT INTO $tarif ($tarif) VALUES ('$tarifneu')";				// Tarif neu
		$tarifneuid = id_ermitteln($db, $sql1, $sql2);
		
// produktneu_id ermitteln
		$sql1 = "SELECT $produkt_id FROM $produkt WHERE $gesellschaft_id = $gesalt[$gesellschaft_id] AND $tarif_id = '$tarifneuid'";	 // Tarif schon vorhanden
		$sql2 = "INSERT INTO $produkt ($gesellschaft_id, $tarif_id) VALUES ('$gesalt[$gesellschaft_id]', '$tarifneuid')";		// Tarif neu
		$produktneuid = id_ermitteln($db, $sql1, $sql2);

// Termine mit produktalt-id aus der Datenbank lesen
		$sql = " SELECT termin_id FROM termin WHERE $produkt_id = '$gesalt[$produkt_id]' ";
		$terminaltquery = myqueryi ($db, $sql);
		
// wenn vorhanden, Termine auf neue produkt-id updaten
		if (mysqli_num_rows($terminaltquery) > 0) {
			for ($i=0; $i<mysqli_num_rows($terminaltquery); $i++) {
				$termin = mysqli_fetch_array($terminaltquery);
				$sql = " UPDATE termin SET $produkt_id = '$produktneuid' WHERE termin_id = '$termin[termin_id]' ";
				$query = myqueryi ($db, $sql);
			}
		}

// Kontrollabruf, ob es noch Termine mit produktalt-id gibt
		$sql = " SELECT termin_id FROM termin WHERE $produkt_id = '$gesalt[$produkt_id]' ";
		$testquery = myqueryi ($db, $sql);
		
		if (mysqli_num_rows($testquery) > 0) {
			$fehler = "Es gibt noch Termine mit diesem Tarif/Gesellschaft - bitte noch einmal";
		}

// keine Termine mehr da
		else {
		
			// produkt-alt-id l�schen
			$sql = "DELETE FROM $produkt WHERE $produkt_id = '$gesalt[$produkt_id]' LIMIT 1";
			$query = myqueryi ($db, $sql);
			
			// Test, ob Tarif sonst noch irgendwo verwendet wird
			$sql = " SELECT $produkt_id FROM $produkt WHERE $tarif_id = '$gesalt[$tarif_id]' ";
			$testquery = myqueryi ($db, $sql);
			
			if (mysqli_num_rows($testquery) == 0) {
				// Tarif aus Datenbank l�schen
				$sql = "DELETE FROM $tarif WHERE $tarif = '$tarifalt' LIMIT 1";
				$query = myqueryi ($db, $sql);
			}
		}	// ende else keine Termine mehr da

	}	// ende if empty fehler
		
	if (empty($fehler)) {
	
					?>

				<!-- Ausgabe solange der Rechner arbeitet -->
				<!DOCTYPE html>
				<html lang="de">
				<head>
				<title>Tarife bearbeiten</title>
					<!-- allgemein/tarife/tarife_bearbeiten.php -->
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
				<style type="text/css">
				<!--
					body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }
	
					.ja {
						border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
						background-color: #9b0013; color: #ffffff;
						font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
						width:100px;line-height:140%;
					}

					.nein {
						border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
						background-color: #D8E1EC; color: #006699;
						font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
						width:100px;line-height:140%;
					}

				-->
				</style>
				</head><body><div align = "center">
					<table width="500" border="0" cellspacing="0" cellpadding="3">
 						<tr>
    						<td>
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
      								<tr>
        								<td>
											<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
												<tr bgcolor = "#ff6633">
													<td><span style = "font-weight:bold; color:black; font-size:10pt; line-height:200%;">
														<div align = "center">Fertig ...</div>
														Der Tarif <span style="color:white;"><?php echo "$tarifalt"; ?></span> wurde durch <span style="color:white;"><?php echo "$tarifneu"; ?></span> ersetzt.<br />
														Alle betroffenen Termine wurden upgedated.<br />
														</span>
													</td>
												</tr>
												<tr bgcolor = "moccasin">
													<form method="post" action= "<?php $_SERVER["PHP_SELF"] ?>">
													<td align="center" valign="middle"><input type="submit" name="ok" value="OK!" class = "nein"></td>
													</form>
												</tr>
        									</table>
										</td>
      								</tr>
    							</table>
							</td>
  						</tr>
					</table>
				</div></body>
				</html>
 <?php
	}	// ende empty fehler
	}	// ende else eingaben korrekt
} // ende ja gedr&uuml;ckt


// Debugging -------------------------------------------------------//
	//echo "tarifalt: $tarifalt<br />";
	//echo "tarifneu: $tarifneu<br />";
	//echo "tarifneuid: $tarifid<br />";
	//while ($daten = mysqli_fetch_array($terminaltquery)) { echo"<br />"; var_dump($daten); echo "<br />"; }
	//echo "produkt-neu: $produktneuid<br /><br />";
//------------------------------------------------------------------//


  
if (isset($fehler) OR !isset($ja)) {	// kein Button gedrückt oder Fehler bei der Eingabe - Formular wird angezeigt

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html >
<html lang="de">
<head>
<title>Tarife bearbeiten</title>
	<!-- allgemein/tarife/tarife_bearbeiten.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}
	
.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">

<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
     		<tr>
        		<td>
					<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
					
          				<tr bgcolor="moccasin"><td colspan="2" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Tarif <?php echo "<span style=\"color:red;\">$tarifalt</span> von Gesellschaft <span style=\"color:red;\">$ges</span>"; ?> bearbeiten</span></td></tr>
		 
		 				<?php if ($fehler) {
 								echo "<tr bgcolor=\"red\">";
        						echo "<td colspan=\"2\" align = \"left\" valign = \"middle\">";
								echo "<span style=\"font-weight:bold; color:white; line-height:150%;\">$fehler</span><br>";
								echo "</td></tr>";
							}
						?>
						
		 				<tr>
							<td valign = "top">
								<!-- Start Formular f�r Tarif-Update ++++++++++++++++++++++++++++++++++ // -->
								<form method="post" action= "<?php $_SERVER["PHP_SELF"] ?>">
								<input type="hidden" name="tarifalt" value="<?php echo "$tarifalt"; ?>">
								<input type="hidden" name="auswahl" value="<?php echo "$auswahl"; ?>">
								<input type="hidden" name="ges" value="<?php echo "$ges"; ?>">
									<table id="formular" width="100%">
  										<tr>
    										<td align="left" valign="middle">Tarif</td>
												<?php	if (isset($fehler)) {echo "<td align=\"left\" valign=\"middle\"><input type=\"text\" name=\"tarifupdate\" size = \"25\" value=\"$tarifalt\" style=\"background-color:$bgupdate\"></td>"; }
													else { echo "<td align=\"left\" valign=\"middle\"><input type=\"text\" name=\"tarifupdate\" size = \"25\" value=\"$tarifalt\"></td>"; }
												?>
							
											<td align="left" valign="middle"><input type="submit" name="update" value="Update!" class = "submitt"></td>
  										</tr>
									</table>      
								<!-- Ende Formular f�r Tarif-Update ++++++++++++++++++++++++++++++++++ // -->
							</td>
						</tr>
						
						<?php	//  Feststellen, ob Benutzer �berhaupt l�schen darf ---------------------------------------------------------------------------
							if ($gruppe == "Administrator") {
						?>
						
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" bgcolor="#cccccc">
									<tr>
										<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">Der Tarif</span></td>
										<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">soll ersetzt werden durch</span></td>
									</tr>
									<tr>
		  								<?php
										echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ffffcc\">";
											echo "<table width=\"100%\">";
												echo "<tr>";
													// Anzeige des gew�hlten Tarifes (alt) in Text-Feldern - aber keine Verarbeitung der Felder
													echo "<td align = \"left\"><span style=\"font-weight:bold; font-size:10pt; color:red; line-height:150%;\"><input type=\"text\" name=\"tarifalt\" value=\"$tarifalt\"></span></td>";
												echo "</tr>";
											echo "</table>";
										echo "</td>";
								
										// Zelle rechts - neuer Tarif			
										echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ccffff\">";
											echo "<table width=\"100%\">";
												echo "<tr>";
													echo "<td colspan = \"2\">";
														// Die Tarife werden aus der DB gelesen
														$sql  = "SELECT $tarif FROM $tarif ORDER BY $tarif ASC ";
														$query = myqueryi ($db, $sql);
			
														echo "<select name=\"tarifselect\" style=\"background-color:$bgtarif\">";
														while ($zeile = mysqli_fetch_array($query)) {
															if ($tarifselect == $zeile[0]) { echo "<option selected>$zeile[0]</option>"; }
															else { echo "<option>$zeile[0]</option>"; }
														}
														echo "</select>";			
												echo "</tr>";
											echo "<tr>";
												if (empty($fehler)) {
													echo "<td align = \"left\"><input type=\"text\" name=\"tarifinput\" value=\"Tarif eingeben!\"></span></td>";
												}
												else {
													echo "<td align = \"left\"><input type=\"text\" name=\"tarifinput\" value=\"$tarifinput\" style=\"background-color:$bgtarif\"></span></td>";
												}
											echo "</tr>";
										echo "</table>";
									echo"</td>";
								echo "</tr>";
								echo "<tr>";
									echo "<td colspan = \"2\" align = \"center\">
										<input type=\"submit\" name=\"ja\" value=\"Ersetzen!\" class = \"ja\">&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"nein\" value=\"Zurück\" class=\"nein\">
									<td>";
							echo "</tr>";
							?>
										</tr>
							</form>
						</table>
						</td>
					</tr>
					<?php } // Ende Administrator darf l�schen ?>
        			</table>
				</td>
      		</tr>
    	</table>
	</td>
  </tr>
</table>
</div>
</body>
</html>
<?php
}

?>




