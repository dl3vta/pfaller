<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script können Gesellschaften bearbeitet oder komplett gelöscht werden		        //
// nur ein Administrator darf das Script nutzen												    //
// Gesellschaft und Art der Gesellschaft alt/neu kommen von ges_select.php					    //
// bei Löschung der Gesellschaft verlieren alle entsprechenden Kunden die Gesellschaft          //
// eine zu bearbeitende Gesellschaft muss durch eine andere ersetzt werden						//
// die Tarife beider angegebener Gesellschaften werden gemischt                      	        //
// alle betroffenen Kunden werden geupdated                                                     //
// danach wird die alte Gesellschaft gelöscht                                                   //
// nach dem Löschen wird eine Meldung über Erfolg oder Misserfolg der Löschaktion ausgegeben    //
// danach erfolgt(über einen Link) der Rücksprung zu ges_select.php		                        //
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];			// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php
$bg_fehler = "#ff9966";								// Fehler-Farbe
unset($fehler);										// Fehlerausgabe wird zurückgesetzt

// POST- und GET-Variablen --------------------------------------------------------------------------------------

$ja = $_POST["ja"];						// Ja-Button, Gesellschaft soll ersetzt werden
$nein = $_POST["nein"];					// Nein-Button, ersetzen wird abgebrochen
$delete = $_POST["delete"];				// Gesellschaft soll gelöscht werde werden
$update = $_POST["update"];				// Gesellschaft soll gelöscht werde werden
$ok = $_POST["ok"];						// Operation erfolgreich, Refresh der Frames

$ges = $_GET["ges"];  						// von ges_select.php, zur Anzeige in den Textfeldern links
$auswahl = $_GET["auswahl"];				// von ges_select.php, zur Steuerung alte/neue Gesellschaft

// Auswahl alte/neue Gesellschaften
if ($auswahl == "neu") { 
	$gesellschaft = "ges_neu";
	$gesellschaft_id = "ges_neu_id";
	$tarif = "tarif_neu";
	$tarif_id = "tarif_neu_id";
	$produkt = "produkt_neu";
	$produkt_id = "produkt_neu_id";
}
else {
	$gesellschaft = "ges_alt";
	$gesellschaft_id = "ges_alt_id";
	$tarif = "tarif_alt";
	$tarif_id = "tarif_alt_id";
	$produkt = "produkt_alt";
	$produkt_id = "produkt_alt_id";
}


// Formular leeren -------------------------
if (isset($nein)) {
	unset($gesupdate);
	unset($gesselect);
	unset($gesinput);
}
// ------------------------------------------------------------------------------------

// Bearbeitung war erfolgreich, Refresh der Frames ---------------------------------
if (isset($ok)) {
	echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
	echo "<script>onload=parent['tarife'].location.href='tarife.php'</script>";
	echo "<script>onload=location.href='ges_bearbeiten.php'</script>";
}


// Gesellschaft soll komplett gelöscht werden --------------------------------------------------

if (isset($delete)) {
	$ges = $_POST["ges"]; 						// alte Gesellschaft wird als hidden-Feld übertragen
	$auswahl = $_POST["auswahl"]; 				// Auswahl wird als hidden-Feld übertragen
	echo "<script>onload=location.href='ges_loeschen.php?ges=$ges&auswahl=$auswahl'</script>";
}

// ------------------------------------------------------------------------------------


// Gesellschaft soll geupdated werden--------------------------------------------------

if (isset($update)) {

	$ges = $_POST["ges"]; 							// alte Gesellschaft wird als hidden-Feld übertragen
	$auswahl = $_POST["auswahl"]; 					// Auswahl wird als hidden-Feld übertragen
	$gesupdate = $_POST["gesupdate"];				// Gesellschaft für Update
	$gesupdate = quote_smart($gesupdate);
	
// keine Eingabe
	if (empty($gesupdate))	{
		$fehler = "Bitte eine Gesellschaft eingeben";
		$bgupdate = $bg_fehler;
	}

// alte und neue Gesellschaft gleich!
	elseif ($gesupdate == $ges) {
		$fehler = "Diese Eingabe ist sinnlos";
		$bgupdate = $bg_fehler;
	}
	
// Eingabe korrekt
	else {
	
// ist neu eingegebene Gesellschaft schon da - Fehlermeldung "Ersetzen"
// ansonsten update der Gesellschaft
		$sql = " SELECT $gesellschaft_id FROM $gesellschaft WHERE $gesellschaft = '$gesupdate' ";
		$query = myqueryi($db, $sql);
	
		if (mysqli_num_rows($query) > 0) {
			$fehler = "Diese Gesellschaft gibt es schon - bitte ERSETZEN (grauer Abschnitt)";
			$bgupdate = $bg_fehler;
		}
	
		else {
			$sql = " UPDATE $gesellschaft SET $gesellschaft = '$gesupdate' WHERE $gesellschaft = '$ges' ";
			$query = myqueryi($db, $sql);

			// Refresh der einzelnen Seiten in den entsprechenden Frames
			echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
			echo "<script>onload=parent['tarife'].location.href='tarife.php'</script>";
			echo "<script>onload=location.href='ges_bearbeiten.php'</script>";
		}
	}	// ende Eingabe korrekt
}	// ende update

// ------------------------------------------------------------------------------------

// Gesellschaft soll ersetzt werden -------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------------------------------

elseif (isset($ja)) {							// Ja-Button gedrückt
	$ges = $_POST["ges"]; 						// alte Gesellschaft wird als hidden-Feld übertragen
	$auswahl = $_POST["auswahl"]; 				// Auswahl wird als hidden-Feld übertragen
	$gesselect = $_POST["gesselect"];			// Gesellschaft aus Select-Feld
	$gesinput = $_POST["gesinput"];				// Gesellschaft aus Textfeld
	$gesinput = quote_smart($gesinput);
	$tarifinput = $_POST["tarifinput"];			// Tarif aus Textfeld
	$tarifinput = quote_smart($tarifinput);
	$gesupdate = $_POST["gesupdate"];			// Gesellschaft für Update
	$gesupdate = quote_smart($gesupdate);
	
// Textfeld Gesellschaft überschreibt Select-Feld
	if (isset($gesinput) AND $gesinput != 'Gesellschaft!') { $gesneu = $gesinput; }
	else { $gesneu = $gesselect; }
	
// Überprüfung der Eingabewerte --------------------------------------------------------------------------------------------------
	if (empty($ges))	{
		$fehler = "Sie müssen zuerst eine Gesellschaft auswählen!";
		$bgges = $bg_fehler;
	}
	
	elseif ((empty($gesinput) OR $gesinput == 'Gesellschaft!') AND empty($gesselect)) {					// keine Eingaben gemacht
		$fehler = "Sie müssen eine Gesellschaft auswählen oder eingeben!";
		$bginput = $bg_fehler;
		$bgselect = $bg_fehler;
	}
	
	elseif ($gesneu == $ges) {
		$fehler = "Diese Eingabe ist sinnlos";
		$bginput = $bg_fehler;
		$bgselect = $bg_fehler;
	}
	
//	eingaben vollzogen
	else {

// Überprüfung der Eingabewerte beendet
// Auswertung startet
		if (empty($fehler)) {
		
			?>
			<!-- Ausgabe solange der Rechner arbeitet -->
			<!DOCTYPE html>
			<html lang="de">
			<head>
			<title>Ort bearbeiten</title>
				<!-- allgemein/tarife/ges_bearbeiten.php -->
			<meta http-equiv="Content-Type" content="text/html; utf-8" />
			<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
			<style type="text/css">
			<!--
			body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }
			-->
			</style>
			</head><body><div align = "center">
				<table width="500" border="0" cellspacing="0" cellpadding="3">
 					<tr>
    					<td>
							<table width="100%"  border="0" cellspacing="0" cellpadding="0">
      							<tr>
        							<td>
										<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
											<tr bgcolor = "#ff6633">
												<td><span style = "font-weight:bold; color:black; font-size:10pt; line-height:200%;">
													<div align = "center">In Arbeit ...</div><br />
													Bei vielen Terminen kann unter Umständen die maximale Serverlaufzeit
													überschritten werden, in diesem Fall das Programm bitte noch einmal
													ausführen.
													</span>
												</td>
											</tr>
        								</table>
									</td>
      							</tr>
    						</table>
						</td>
  					</tr>
				</table>
			</div></body>
			</html>
   <?php
	
	//	Folgende Fälle sind möglich: ----------------------------------------------
	//	1.		Tarif eingegeben - alte Gesellschaft mit allen Tarifen wird zum Tarif der neuen Gesellschaft
	//	1.1.	Gesellschaft neu nicht vorhanden
	//	1.2.	Gesellschaft neu schon vorhanden, ohne Tarife
	//	1.3. 	Gesellschaft neu schon vorhanden, eingegebener Tarif schon vorhanden
	//	1.4.	Gesellschaft neu schon vorhanden, eingegebener Tarif nicht vorhanden
	//
	//	2.		kein Tarif eingegeben - Tarife beider Gesellschaften werden gemischt
	//	2.1.	Gesellschaft neu nicht vorhanden
	//	2.2.	Gesellschaft neu schon vorhanden
	// --------------------------------------------------------------------------
	
// Abfrage der alten Gesellschaft, bei mehr als einer Zeile im query existieren Tarife, oder bei einer Zeile wenn tarif_id > 1
	
			$sql  = " SELECT $produkt_id, $gesellschaft.$gesellschaft_id, $tarif.$tarif_id ";
			$sql .= " FROM $produkt, $tarif, $gesellschaft ";
			$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id AND $gesellschaft.$gesellschaft = '$ges' ";
			$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
			$sql .= " ORDER BY $tarif.$tarif_id ASC ";
			$altquery = myqueryi($db, $sql);
		
			// ges_id neu ----------------------------------------------------------------------------
			$sql1 = "SELECT $gesellschaft_id FROM $gesellschaft WHERE $gesellschaft = '$gesneu'";		// Gesellschaft schon vorhanden
			$sql2 = "INSERT INTO $gesellschaft ($gesellschaft) VALUES ('$gesneu')";						// Gesellschaft neu
			$ges_id = id_ermitteln($db, $sql1, $sql2);
		
//	Tarif eingegeben - alte Gesellschaft mit allen Tarifen wird zum Tarif der neuen Gesellschaft
			if (!empty($tarifinput) AND $tarifinput !== 'Tarif!') {
				
				// Tarifneu schon vorhanden?
				$sql1 = "SELECT $tarif_id FROM $tarif WHERE $tarif = '$tarifinput'";		// Tarif schon vorhanden
				$sql2 = "INSERT INTO $tarif ($tarif) VALUES ('$tarifinput')";				// Tarif neu
				$tarifneu_id = id_ermitteln($db, $sql1, $sql2);

				// Abfrage, ob es die Kombination aus neuer Gesellschaft und Tarif schon gibt - Produkt-ID
				$sql  = "SELECT $produkt_id FROM $produkt   ";
				$sql .= " WHERE $gesellschaft_id = '$ges_id' AND $tarif_id = '$tarifneu_id' ";
				$produktquery = myqueryi($db, $sql);
				
				// ja, gibt es schon
				if (mysqli_num_rows($produktquery)>0) {
					$array = mysqli_fetch_array($produktquery);
					$produkt_neu = $array[0];
				}

				// nein, gibt es noch nicht, neue Produkt-ID einfügen
				else {
					$sql = "INSERT INTO $produkt ($gesellschaft_id, $tarif_id) VALUES ('$ges_id', '$tarifneu_id') ";
					$query = myqueryi($db, $sql);
					$produkt_neu = mysqli_insert_id($db);
				}

				// alle Termine mit alten Produkt-ID auslesen (alle Tarife der alten Gesellschaft)
				for ($i=0; $i<mysqli_num_rows($altquery); $i++) {
					$terminalt = mysqli_fetch_array($altquery);
							
					$sql = " SELECT termin_id FROM termin WHERE $produkt_id = '$terminalt[$produkt_id]' ";
					$termine = myqueryi($db, $sql);
							
					// keine Termine für produkt-id da - produkt-id löschen
					if (mysqli_num_rows($termine) == 0) {
						$sql = " DELETE FROM $produkt WHERE $produkt_id = '$terminalt[$produkt_id]' LIMIT 1 ";
						$delete = myqueryi($db, $sql);
					}
					
					// Termine in produkt-id alt vorhanden - updaten
					else {
							
						// Termine updaten
						for ($j=0; $j<mysqli_num_rows($termine); $j++) {
							$terminneu = mysqli_fetch_array($termine);
								
							$sql = " UPDATE termin SET $produkt_id = '$produkt_neu' WHERE termin_id = '$terminneu[termin_id]' ";
							$result1 = myqueryi($db, $sql);
						}	// ende for Termine updaten
					}	// ende else Termine für produkt-id vorhanden - updaten
				}	// ende for alle alten Termine auslesen

				// Test, ob alte Gesellschaft und alte Tarife noch verwendet werden, wenn nicht, löschen
				if (mysqli_num_rows($altquery) > 0) {
					mysqli_data_seek($altquery, 0);
					
					//	alte produkt-ids und Tarife löschen
					for ($i=0; $i<mysqli_num_rows($altquery); $i++) {
						$terminalt = mysqli_fetch_array($altquery);
						
						//Test, ab alle Termine upgedated
						usleep(100000);		// 0,1 Sekunden warten
						$sql = " SELECT termin_id FROM termin WHERE $produkt_id = '$terminalt[$produkt_id]' ";
						$result2 = myqueryi($db, $sql);
					
						// keine Termine mehr da
						if (mysqli_num_rows($result2) == 0) {
							$sql = " DELETE FROM $produkt WHERE $produkt_id = '$terminalt[$produkt_id]' LIMIT 1 ";
								$delete = myqueryi($db, $sql);
						}
						else {
							$fehler = "Es konnten nicht alle Termine geupdated werden - bitte noch einmal!";
						}

						// abfrage ob alter Tarif noch verwendet wird
						$sql = " SELECT $produkt_id FROM $produkt WHERE $tarif_id = '$terminalt[$tarif_id]' ";
						$query = myqueryi($db, $sql);
								
						if (mysqli_num_rows($query) == 0) {
							$sql = " DELETE FROM $tarif WHERE $tarif_id = '$terminalt[$tarif_id]'  LIMIT 1 ";
							$delete = myqueryi($db, $sql);
						}
					}
					
					// abfrage ob alte Gesellschaft noch verwendet wird
					$sql = " SELECT $produkt_id FROM $produkt WHERE $gesellschaft_id = '$terminalt[$gesellschaft_id]' ";
					$query = myqueryi($db, $sql);
								
					if (mysqli_num_rows($query) == 0) {
						$sql = " DELETE FROM $gesellschaft WHERE $gesellschaft_id = '$terminalt[$gesellschaft_id]' LIMIT 1 ";
						$delete = myqueryi($db, $sql);
					}
				}	// ende if Test, ob alte Gesellschaft und alte Tarife noch verwendet werden
			} // ende if Tarif eingegeben
			

// kein Tarif eingegeben, Tarif von alter und neuer Gesellschaft werden gemischt
			else {
				// Ermittlung aller produkt-ids für neue Gesellschaft
				$sql  = " SELECT $produkt_id, $gesellschaft.$gesellschaft_id, $tarif.$tarif_id ";
				$sql .= " FROM $produkt, $tarif, $gesellschaft ";
				$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id AND $gesellschaft.$gesellschaft = '$gesneu' ";
				$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
				$sql .= " ORDER BY $tarif.$tarif_id ASC ";
				$neuquery = myqueryi($db, $sql);
				
				// noch keine produkt-id vorhanden - einfügen (neue Gesellschaft eingegeben)
				if (mysqli_num_rows($neuquery) == 0) {
					$sql = " INSERT INTO $produkt ($gesellschaft_id, $tarif_id) VALUES ('$ges_id', '1') ";
					$query = myqueryi($db, $sql);
					//$produktneu_id = mysqli_insert_id($db);
				}
				
				// Ermittlung aller produkt-ids für neue Gesellschaft nach eventuellem Einfügen einer neuen Gesellschaft
				$sql  = " SELECT $produkt_id, $gesellschaft.$gesellschaft_id, $tarif.$tarif_id ";
				$sql .= " FROM $produkt, $tarif, $gesellschaft ";
				$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id AND $gesellschaft.$gesellschaft = '$gesneu' ";
				$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
				$sql .= " ORDER BY $tarif.$tarif_id ASC ";
				$neuquery = myqueryi($db, $sql);
				
				// Schleife über alle produkt-ids der alten Gesellschaft
				for ($i=0; $i<mysqli_num_rows($altquery); $i++) {
					$terminalt = mysqli_fetch_array($altquery);
					
					// schleife über alle produkt-ids der neuen Gesellschaft
					for ($k=0; $k<mysqli_num_rows($neuquery); $k++) {
						$tarifneu = mysqli_fetch_array($neuquery);
						$produktneu = $tarifneu[$produkt_id];
					
						$treffer = 0; 									// 1 wenn Tarif alt = Tarif neu
								
						// vergleich Tarif-alt mit Tarif-neu - stimmt, alle Termine auslesen und auf neue produkt-id updaten
						if  ($terminalt[$tarif_id] ==  $tarifneu[$tarif_id]) {
								
							$treffer = 1; 								// 1 wenn Tarif alt = Tarif neu
								
							// Kunden für alte produkt_id auslesen
							$sql = " SELECT termin_id FROM termin WHERE $produkt_id = '$terminalt[$produkt_id]' ";
							$result = myqueryi($db, $sql);
							
							// keine Termine mit produkt_id da - produkt-id löschen
							if (mysqli_num_rows($result) == 0) {
								$sql = " DELETE FROM $produkt WHERE $produkt_id = '$terminalt[$produkt_id]' LIMIT 1 ";
								$delete = myqueryi($db, $sql);
							}
							
							// Termine mit produkt-id vorhanden - updaten
							else {
								for ($m=0; $m<mysqli_num_rows($result); $m++) {
									$terminneu = mysqli_fetch_array($result);
								
									$sql = " UPDATE termin SET $produkt_id = '$produktneu' WHERE termin_id = '$terminneu[termin_id]' ";
									$result1 = myqueryi($db, $sql);
						
								}	// ende for Termine updaten
							}	// ende else Termine mit produkt_id vorhanden - updaten
							break;	// Schleife verlassen
						}	// ende if vergleich Tarif alt neu stimmt
						
					}	// ende for über alle produkt-id der neuen Gesellschaft
					
					mysqli_data_seek($neuquery, 0);	// neuquery für nächsten Durchlauf zurückstellen
								
					// keine Übereinstimmung in Tarifen gefunden - Tarif alt in neue Gesellschaft einfügen (neue produkt-id)
					if ($treffer == 0) {
								
						// neue produkt_id für neue Geselllschaft
						$sql = " INSERT INTO $produkt ($gesellschaft_id, $tarif_id) VALUES ('$ges_id', '$terminalt[$tarif_id]') ";
						$query = myqueryi($db, $sql);
						$produktneu = mysqli_insert_id($db);
									
						// Termine mit alter produkt-id auslesen
						$sql = " SELECT termin_id FROM termin WHERE $produkt_id = '$terminalt[$produkt_id]' ";
						$result = myqueryi($db, $sql);
							
						// keine Termine mit alter produkt-id da - löschen
						if (mysqli_num_rows($result) == 0) {
							$sql = " DELETE FROM $produkt WHERE $produkt_id = '$terminalt[$produkt_id]' LIMIT 1 ";
							$delete = myqueryi($db, $sql);
						}
							
						// Termine mit alter produkt-id vorhanden - updaten
						else {
							for ($n=0; $n<mysqli_num_rows($result); $n++) {
								$terminneu = mysqli_fetch_array($result);
								
								$sql = " UPDATE termin SET $produkt_id = '$produktneu' WHERE termin_id = '$terminneu[termin_id]' ";
								$result1 = myqueryi($db, $sql);
							
							}	// ende for Termine updaten
						}	// ende else Termine mit produkt-id vorhanden - updaten
					}	// ende if keine Übereinstimmung in Tarifen gefunden - Tarif alt in neue Gesellschaft einfügen (neue produkt-id)
				}	// ende for schleife alte Gesellschaft

			// Test, ob alte Gesellschaft und alte Tarife noch verwendet werden, wenn nicht, löschen
			if (mysqli_num_rows($altquery) > 0) {
				mysqli_data_seek($altquery, 0);
					
				//	alte produkt-ids und Tarife löschen
				for ($i=0; $i<mysqli_num_rows($altquery); $i++) {
					$terminalt = mysqli_fetch_array($altquery);
						
					//Test, ab alle Termine upgedated
					usleep(100000);		// 0,1 Sekunden warten
					$sql = " SELECT termin_id FROM termin WHERE $produkt_id = '$terminalt[$produkt_id]' ";
					$result2 = myqueryi($db, $sql);
					
					// keine Termine mehr da
					if (mysqli_num_rows($result2) == 0) {
						$sql = " DELETE FROM $produkt WHERE $produkt_id = '$terminalt[$produkt_id]' LIMIT 1 ";
							$delete = myqueryi($db, $sql);
					}
					else {
						$fehler = "Es konnten nicht alle Termine geupdated werden - bitte noch einmal!";
					}

					// abfrage ob alter Tarif noch verwendet wird
					$sql = " SELECT $produkt_id FROM $produkt WHERE $tarif_id = '$terminalt[$tarif_id]' ";
					$query = myqueryi($db, $sql);
								
					if (mysqli_num_rows($query) == 0) {
						$sql = " DELETE FROM $tarif WHERE $tarif_id = '$terminalt[$tarif_id]'  LIMIT 1 ";
						$delete = myqueryi($db, $sql);
					}
				}
					
				// abfrage ob alte Gesellschaft noch verwendet wird
				$sql = " SELECT $produkt_id FROM $produkt WHERE $gesellschaft_id = '$terminalt[$gesellschaft_id]' ";
				$query = myqueryi($db, $sql);
								
				if (mysqli_num_rows($query) == 0) {
					$sql = " DELETE FROM $gesellschaft WHERE $gesellschaft_id = '$terminalt[$gesellschaft_id]' LIMIT 1 ";
					$delete = myqueryi($db, $sql);
				}
			}	// ende if Test, ob alte Gesellschaft und alte Tarife noch verwendet werden
		}	// ende else kein Tarif eingegeben
	
/*
// Debugging -------------------------------------------------------//
//while ($daten = mysqli_fetch_array($altquery)) { echo"<br />"; var_dump($daten); echo "<br />"; }
//while ($daten = mysqli_fetch_array($neuquery)) { echo"<br />"; var_dump($daten); echo "<br />"; }
echo "PLZalt: $plz_alt<br />";										//
echo "Ortalt: $ort_alt<br />";										//
echo "PLZneu: $plz_neu<br />";										//
echo "PLZ-IDneu: $plz_id<br />";									//
echo "Ortneu: $ort_neu<br />";										//
echo "Ort-IDneu: $ort_id<br />";										//
echo "select: $plzort_neu<br />";								//
echo "Ortsteil: $ortsteil_neu<br />";							//
echo "OT-ID neu: $ortsteil_id<br />";							//
echo "poo-neu:$pooneu_id<br />";
echo "poo-alt: $terminalt[0]<br />";
echo "kundealt: $terminneu[0]<br />";							//
//------------------------------------------------------------------//
*/
	
	if (!isset($fehler)) {
	
				?>

				<!-- Ausgabe solange der Rechner arbeitet -->
				<!DOCTYPE html>
				<html lang =de"">
				<head>
				<title>Branche bearbeiten</title>
					<!-- allgemein/tarife/ges_bearbeiten.php -->
				<meta http-equiv="Content-Type" content="text/html; utf-8" />
				<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
				<style type="text/css">
				<!--
					body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }
	
					.ja {
						border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
						background-color: #9b0013; color: #ffffff;
						font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
						width:100px;line-height:140%;
					}

					.nein {
						border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
						background-color: #D8E1EC; color: #006699;
						font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
						width:100px;line-height:140%;
					}

				-->
				</style>
				</head><body><div align = "center">
					<table width="500" border="0" cellspacing="0" cellpadding="3">
 						<tr>
    						<td>
								<table width="100%"  border="0" cellspacing="0" cellpadding="0">
      								<tr>
        								<td>
											<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
												<tr bgcolor = "#ff6633">
													<td><span style = "font-weight:bold; color:black; font-size:10pt; line-height:200%;">
														<div align = "center">Fertig ...</div>
														Die Gesellschaft <span style="color:white;"><?php echo "$ges"; ?></span> wurde durch <span style="color:white;"><?php echo "$gesneu"; ?></span> ersetzt.<br />
														Alle betroffenen Termine wurden upgedated.<br />
														</span>
													</td>
												</tr>
												<tr bgcolor = "moccasin">
													<form method="post" action= "<?php $_SERVER["PHP_SELF"] ?>">
													<td align="center" valign="middle"><input type="submit" name="ok" value="OK!" class = "nein"></td>
													</form>
												</tr>
        									</table>
										</td>
      								</tr>
    							</table>
							</td>
  						</tr>
					</table>
				</div></body>
				</html>
<?php
}
	}	// Ende keine Fehler bei der Dateneingabe - Datenverarbeitung
	}	// ende else Eingaben korrekt
} // ende ja gedr&uuml;ckt



if (isset($fehler) OR !isset($ja)) {	// kein Button gedrückt oder Fehler bei der Eingabe - Formular wird angezeigt
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang = de"">
<head>
<title>Gesellschaft bearbeiten</title>
	<!-- allgemein/tarife/ges_bearbeiten.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.delete {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #ff0000; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">

<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
     		<tr>
        		<td>
					<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
					
          				<tr bgcolor="moccasin"><td colspan="2" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Gesellschaft <span style = "color:red;"><?php echo "$ges"; ?></span> bearbeiten</span></td></tr>
		 
	   <?php
							if ($fehler) {
 								echo "<tr bgcolor=\"red\">";
        						echo "<td colspan=\"2\" align = \"left\" valign = \"middle\">";
								echo "<span style=\"font-weight:bold; color:white; line-height:150%;\">$fehler</span><br>";
								echo "</td></tr>";
							}
						?>
						
		 				<tr>
							<td valign = "top">
								<!-- Start Formular für Gesellschaft-Update ++++++++++++++++++++++++++++++++++ // -->
								<form method="post" action= "<?php $_SERVER["PHP_SELF"] ?>">
								<input type="hidden" name="ges" value="<?php echo "$ges"; ?>">
								<input type="hidden" name="auswahl" value="<?php echo "$auswahl"; ?>">
								<table id="formular" width="100%">
  									<tr>
    									<td align="left" valign="middle">Gesellschaft</td>
											<?php	if (isset($fehler)) {echo "<td align=\"left\" valign=\"middle\"><input type=\"text\" name=\"gesupdate\" size = \"25\" value=\"$gesupdate\" style=\"background-color:$bgupdate\"></td>"; }
													else { echo "<td align=\"left\" valign=\"middle\"><input type=\"text\" name=\"gesupdate\" size = \"25\" value=\"$ges\"></td>"; }
											?>
										<td align="left" valign="middle"><input type="submit" name="update" value="Update!" class = "submitt"></td>
  									</tr>
								</table>      
								<!-- Ende Formular für Gesellschaft-Update ++++++++++++++++++++++++++++++++++ // -->
							</td>
						</tr>
						
						<?php	//  Feststellen, ob Benutzer überhaupt löschen darf ---------------------------------------------------------------------------
							if ($gruppe == "Administrator") {
						?>
						
						<tr>
							<td>
							
<table width="100%" border="0" cellpadding="3" cellspacing="2" bgcolor="#cccccc">
				<form method="post" action="<?php $_SERVER[PHP_SELF] ?>">

				<tr>
					<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">Die Gesellschaft</span></td>
					<td width = "50%" align = "center"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">soll ersetzt werden durch</span></td>
				</tr>
				<tr>
	 <?php
					echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ffffcc\">";
								echo "<table width=\"100%\">";
									echo "<tr>";
										// Anzeige der gewählten Gesellschaft im Text-Feld - aber keine Verarbeitung des Text-Feldes
										echo "<td align = \"left\"><span style=\"font-weight:bold; font-size:10pt; color:red; line-height:150%; background-color:$bgges;\"><input type=\"text\" name=\"ges\" value=\"$ges\" span style=\"background-color:$bgges;\"></span></td>";
									echo "</tr>";
									echo "</tr>";
										echo "<td>";
											// Auslesen der Tarife zur gewählten Gesellschaft
												$sql  = "SELECT $tarif FROM $produkt, $gesellschaft, $tarif ";
												$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id ";
												$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
												$sql .= " AND $gesellschaft.$gesellschaft = '$ges' ";
												$sql .= " AND $produkt.$tarif_id > '1' ";
												$sql .= " ORDER BY $tarif ASC ";
												$query = myqueryi($db, $sql);
											
											if (mysqli_num_rows($query) > 0) {
												echo "Mit den Tarifen<br />";
												for ($i = 0; $i < mysqli_num_rows($query); $i++) {
													$tarife = mysqli_fetch_array($query);
													echo " - $tarife[0]";
												}
											}
										echo "</td>";
									echo "</tr>";
								echo "</table>";
							echo "</td>";
								
				// Zelle rechts - neue Gesellschaft			
							echo "<td width = \"50%\" align = \"left\" valign=\"top\" bgcolor=\"#ccffff\">";
								echo "<table width=\"100%\">";
									echo "<tr>";
										echo "<td colspan = \"2\">";
											// Gesellschaften werde aus der Datenbank abgefragt
											$sql  = "SELECT $gesellschaft FROM $gesellschaft ORDER BY $gesellschaft ASC ";
											$query = myqueryi($db, $sql);
											$ergebnis = myqueryi($db, $sql);
											
											echo "<select name=\"gesselect\" style=\"background-color:$bgselect;\">";
											for ($i = 0; $i < mysqli_num_rows($ergebnis); $i++) {			// Anzahl der Datensätze
												$zeile = mysqli_fetch_array($ergebnis);						// Schleife für Daten-Zeilen
												if ($gesselect == $zeile[0]) { echo "<option selected>$zeile[0]</option>"; }
												else { echo "<option>$zeile[0]</option>"; }
											}
	  										echo "</select>";
										echo "</tr>";
										echo "<tr>";
											echo "<td align = \"left\" valign=\"middle\">";
												if (isset($fehler)) {echo "<input type=\"text\" name=\"gesinput\" size = \"25\" value=\"$gesinput\" style=\"background-color:$bginput\"\">"; }
												else { echo "<input type=\"text\" name=\"gesinput\" size = \"25\" value=\"Gesellschaft!\">"; }
											echo "</td>";
										echo "</tr>";
										echo "<tr>";
											echo "<td align = \"left\" valign=\"middle\">";
												if (isset($fehler)) {echo "<input type=\"text\" name=\"tarifinput\" size = \"25\" value=\"$tarifinput\">"; }
												else { echo "<input type=\"text\" name=\"tarifinput\" size = \"25\" value=\"Tarif!\">"; }
											echo "</td>";
										echo "</tr>";
									echo "</table>";
								echo"</td>";
							echo "</tr>";
						echo "<tr>";
							echo "<td colspan = \"2\" align = \"center\">
									<input type=\"submit\" name=\"ja\" value=\"Ersetzen!\" class = \"ja\">&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"nein\" value=\"Leeren\" class=\"nein\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"delete\" value=\"LÖSCHEN!\" class = \"delete\">
								<td>";
						echo "</tr>";
					?>
				</form>
			</table>
		</td>
	</tr>
	<?php } // Ende Administrator darf löschen ?>
        			</table>
				</td>
      		</tr>
    	</table>
	</td>
  </tr>
</table>
</div>
</body>
</html>
<?php
}
?>