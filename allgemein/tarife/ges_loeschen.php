<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script können Gesellschaften komplett gelöscht werden								//
// Gesellschaften neu können nur gelöscht werden, wenn keine Termine existieren!!!!!!			//
// nur ein Administrator darf das Script nutzen													//
// Gesellschaft und Art (neu/alt) kommen von ges_bearbeiten.php									//
// vor dem Löschen erfolgt eine Sicherheitsabfrage												//
// alle entsprechenden Kunden (Termine) werden auf Produkt=1 (keine Gesellschaft) gesetzt		//
// Gesellschaft wird gelöscht																	//
// falls die verwendeten Tarife nicht mehr verwendet werden, werden sie ebenfalls gelöscht		//
// nach dem Löschen wird eine Meldung über Erfolg oder Misserfolg der Löschaktion ausgegeben	//
// danach erfolgt ein Refresh von ges_select und ges_bearbeiten.php								//
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();

$gruppe = $_SESSION['benutzer_gruppen'];			// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php

//  Feststellen, ob Benutzer überhaupt löschen darf --------------------------------------------------------------

if ($gruppe != "Administrator") {
	echo "<script>location.href='../../forbidden.php'</script>";	// kein Administrator - Zugriff nicht erlaubt
}

// ----------------------------------------------------------------------------------------------------------------

// POST- und GET-Variablen --------------------------------------------------------------------------------------

// POST- und GET-Variablen --------------------------------------------------------------------------------------

$ja = $_POST["ja"];						// Ja-Button, Daten sollen gelöscht werden
$nein = $_POST["nein"];					// Nein-Button, ersetzen wird abgebrochen
$ok = $_POST["ok"];						// Operation erfolgreich, Refresh der Frames

$ges = $_GET["ges"];  						// von tarife.php
$auswahl = $_GET["auswahl"];				// von tarife.php, zur Steuerung alte/neue Gesellschaft

// Auswahl alte/neue Gesellschaften
if ($auswahl == "neu") { 
	$gesellschaft = "ges_neu";
	$gesellschaft_id = "ges_neu_id";
	$tarif = "tarif_neu";
	$tarif_id = "tarif_neu_id";
	$produkt = "produkt_neu";
	$produkt_id = "produkt_neu_id";
}
else {
	$gesellschaft = "ges_alt";
	$gesellschaft_id = "ges_alt_id";
	$tarif = "tarif_alt";
	$tarif_id = "tarif_alt_id";
	$produkt = "produkt_alt";
	$produkt_id = "produkt_alt_id";
}

	// Refresh der einzelnen Seiten in den entsprechenden Frames
	if (isset($ok)) {
	//flush();				// Ausgabepuffer auf den Bildschirm schreiben
	//usleep(3000000);		// 2 Sekunden warten, dann Refresh
	echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
	echo "<script>onload=parent['tarife'].location.href='tarife.php'</script>";
	echo "<script>onload=location.href='ges_bearbeiten.php'</script>";
}

// Löschen ist abgebrochen, Rücksprung zu ges_bearbeiten.php ----------------------------------------------------------
if (isset($nein)) {										// Nein-Button gedrückt
	$auswahl = $_POST["auswahl"];				// auswahl, kommt als hidden vom Script, geht zurück an tarife.php
	$ges = $_POST["ges"];						// auswahl, kommt als hidden vom Script, geht zurück an tarife.php
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
	echo "<script>onload=location.href='ges_bearbeiten.php?ges=$ges&auswahl=$auswahl'</script>";
}
// ----------------------------------------------------------------------------------------------------------------

// Gesellschaft soll gelöscht werden -----------------------------------------------------------------------------

elseif (isset($ja)) {							// Ja-Button gedrückt
	$auswahl = $_POST["auswahl"];				// auswahl, kommt als hidden vom Script, geht zurück an tarife.php
	$ges = $_POST["ges"];						// auswahl, kommt als hidden vom Script, geht zurück an tarife.php

	// Ermittlung, ob noch Termine vorhanden sind ----------------------------------------------------------------------
	$sql = " SELECT termin_id FROM termin, $gesellschaft, $tarif, $produkt ";
	$sql .= " WHERE termin.$produkt_id = $produkt.$produkt_id ";
	$sql .= " AND $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id ";
	$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
	$sql .= " AND $gesellschaft = '$ges'";
	$query = myqueryi($db, $sql);
	
// ja, Termine vorhanden, updaten nur für Gesell.alt
	if (mysqli_num_rows($query) > 0 AND $auswahl == 'alt') {	// Termine vorhanden
		for ($i=0; $i<mysqli_num_rows($query); $i++) {		// alle Termine werden durchlaufen
			$ergebnis = mysqli_fetch_array($query);
			
			// alle Termine auf Produkt=1 updaten
			$sql = " UPDATE termin SET $produkt_id = '1' WHERE termin_id = '$ergebnis[termin_id]' ";
			$abfrage = myqueryi($db, $sql);
	
		}	// ende alle Termine durchlaufen
	}	// ende Termine vorhanden

	// Sicherheitsabfrage, ob alle Termine gelöscht wurden
	$sql = " SELECT termin_id FROM termin, $gesellschaft, $tarif, $produkt ";
	$sql .= " WHERE termin.$produkt_id = $produkt.$produkt_id ";
	$sql .= " AND $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id ";
	$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
	$sql .= " AND $gesellschaft = '$ges'";
	$result = myqueryi($db, $sql);
			
	if (mysqli_num_rows($result) > 0) {						// noch Termine vorhanden
		$fehler = "<br />Es sind noch weitere Termine vorhanden -<br />bitte das Script noch einmal ausführen!<br />";
	}
	else {	//keine Kunden mehr da
	
		// alle produkt_ids für diese Gesellschaft und die Gesellschaft löschen, falls nötig, die Tarife aus der DB löschen
				
			$sql = " SELECT $produkt.$produkt_id, $tarif.$tarif_id FROM $produkt, $gesellschaft, $tarif ";
			$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id ";
			$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
			$sql .= " AND $gesellschaft = '$ges' ";
			$result = myqueryi($db, $sql);
				
			for ($j=0; $j<mysqli_num_rows($result); $j++) {								// alle produkt-ids werden durchlaufen
				$resultat = mysqli_fetch_array($result);
				$sql = " DELETE FROM $produkt WHERE $produkt_id = '$resultat[$produkt_id]' LIMIT 1 ";		// lösche produkt_id
				$result1 = myqueryi($db, $sql);
					
				// Test, ob der Tarif noch irgendwo verwendet wird
				$sql = " SELECT $produkt_id FROM $produkt WHERE $tarif_id = '$resultat[$tarif_id]' ";
				$tariftest = myqueryi($db, $sql);
				if (mysqli_num_rows($tariftest) == 0) {									// Tarif-ID in keinem weiteren Produkt - löschen
					$sql = " DELETE FROM $tarif WHERE $tarif_id = '$resultat[$tarif_id]' LIMIT 1";
					$tarifdelete = myqueryi($db, $sql);
				}
			} // ende for alle produkte
				
			// alle produkt-ids gelöscht, jetzt Gesellschaft löschen
			// Test, ob der Gesellschaft noch irgendwo verwendet wird
			$sql = " SELECT $produkt_id FROM $produkt, $gesellschaft ";
			$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id AND $gesellschaft.$gesellschaft_id = '$ges'  ";
			$gestest = myqueryi($db, $sql);
			if (mysqli_num_rows($gestest) == 0) {									// Gesellschaft in keinem weiteren Produkto - löschen
				$sql = " DELETE FROM $gesellschaft WHERE $gesellschaft = '$ges' LIMIT 1";
				$gesdelete = myqueryi($db, $sql);
			}
	}	// ende else keine Kunden mehr da
?>
	
	<!DOCTYPE html>
<html lang ="de">
<head>
<title>Gesellschaft löschen</title>
	<!-- allgemein/tarife/ges_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }
.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}
-->
</style>
</head>
<body>
<div align = "center">
<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
		  <?php	// Ausgabe der Meldung
					if (isset($fehler)) {	// noch Termine vorhanden
						echo "<tr bgcolor = \"#ff6633\"><td align = \"center\" valign = \"middle\"><span style = \"font-weight:bold; color:black; font-size:10pt; line-height:200%;\">$fehler</span></td></tr>";
					}
					else {
						echo "<tr bgcolor = \"#ff6633\"><td align = \"center\" valign = \"middle\"><span style = \"font-weight:bold; color:black; font-size:10pt; line-height:200%;\"><br />
						Die Gesellschaft $ges wurde gelöscht.<br />
						Alle dazugehörigen Kundentermine wurden auf Standard<br />(keine Gesellschaft) gesetzt.<br /><br />
						</span></td></tr>";
					}
				?>
				<tr bgcolor = "moccasin">
					<form method="post" action= "<?php $_SERVER["PHP_SELF"] ?>">
					<td align="center" valign="middle"><input type="submit" name="ok" value="OK!" class = "nein"></td>
					</form>
				</tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php

} // ende löschen ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


else {	// kein Button gedröckt - Script startet erstmals

// Ermittlung, ob noch Termine vorhanden sind ----------------------------------------------------------------------
$sql = " SELECT termin_id FROM termin, $gesellschaft, $tarif, $produkt ";
$sql .= " WHERE termin.$produkt_id = $produkt.$produkt_id ";
$sql .= " AND $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id ";
$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
$sql .= " AND $gesellschaft = '$ges'";
$query = myqueryi($db, $sql);
if (mysqli_num_rows($query) > 0 AND $auswahl == 'neu') {
	$bemerkung = "Für die Gesellschaft $ges existieren noch " . mysqli_num_rows($query) . " Termine. <br />
				Die Gesellschaft kann nicht gelöscht werden!<br />";
}
elseif (mysqli_num_rows($query) > 0 AND $auswahl == 'alt') {
	$bemerkung = "Für die Gesellschaft $ges existieren noch " . mysqli_num_rows($query) . " Termine. <br />
				Soll die Gesellschaft trotzdem gelöscht werden?<br />
				Alle Termine werden dabei auf Standard (keine Gesellschaft) zurückgesetzt.<br />
				Bei großen Datenmengen (vielen Terminen) muss der Vorgang u.U. wiederholt werden.<br />";
}
else {
	$bemerkung = "Für die Gesellschaft $ges existieren keine Termine.<br />
					Die Gesellschaft $ges kann gelöscht werden<br />";
}
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang = "de">
<head>
<title>Gesellschaft löschen</title>
	<!-- allgemein/tarife/ges_loeschen.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><form name="loeschen" method="post" action="<?php $_SERVER[PHP_SELF] ?>">
		<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
          <tr bgcolor="moccasin"><td colspan="2" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Gesellschaft <span style = "color:red;"><?php echo "$ges"; ?></span> löschen</span></td></tr>
		  <?php	// Ausgabe der Warnmeldung
					if (mysqli_num_rows($query) > 0 AND $auswahl == 'neu') {	// noch Termine vorhanden für gesell. neu
						echo "<tr><td colspan=\"2\" align = \"center\" valign = \"middle\"><span style = \"font-size:12pt; color:red; line-height:200%; font-weight:bold;\">$bemerkung</span></td></tr>";
						echo "<tr>";
							echo "<td align = \"center\" colspan=\"2\" valign = \"middle\"><input type=\"hidden\" name=\"ges\" value=\"$ges\"><input type=\"hidden\" name=\"auswahl\" value=\"$auswahl\">
								<input type=\"submit\" name=\"nein\" value=\"Zurück!\" class=\"nein\"></td>";
            			echo "</tr>";
					}
					else {
						echo "<tr><td colspan=\"2\" align = \"center\" valign = \"middle\"><span style = \"color: black; font-weight: bold;\">$bemerkung</span></td></tr>";
						echo "<tr>";
							echo "<td align = \"center\" width = \"50%\" valign = \"middle\"><input type=\"hidden\" name=\"ges\" value=\"$ges\"><input type=\"hidden\" name=\"auswahl\" value=\"$auswahl\">
								<input type=\"submit\" name=\"ja\" value=\"LÖSCHEN!\" class = \"ja\"></td>";
            				echo "<td align = \"center\" width = \"50%\" valign = \"middle\"><input type=\"submit\" name=\"nein\" value=\"ZurÜck!\" class=\"nein\"></td>";
						echo "</tr>";
					}
				?>
        </table></form></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>
</html>

<?php
}	// ende else Script startet erstmalig, kein Button gedrückt
?>