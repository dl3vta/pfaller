<?php

// -----------------------------------------------------------------------------------------------//
// mit diesem Script k�nnen Gesellschaften neu eingegeben oder zur Bearbeitung ausgew�hlt werden  //
// nur ein Administrator darf das Script nutzen												      //
// die Auswahl, ob neue oder alte Gesellschaften geschieht �ber Radiobuttons				      //
// dadurch werden auch die angesteuerten Scripte ges_bearbeiten.php und tarife.php gesteuert	  //
// von denen kommt die Auswahl der Art der Gesellschaft zur�ck                                    //
// -----------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];
$auswahl1 = $_POST["auswahl"];
$gesneu = $_POST["gesneu"];
//$speichern = $_POST_["speichern"];
$refresh = $_POST["refresh"];

$auswahl2 = $_GET["auswahl2"]; 					// Auswahl als GET von anderen Scripten

$bg_fehler = "#ff9966";								// Fehler-Farbe

if (isset($auswahl1)) { $auswahl = $auswahl1; }
elseif (isset($auswahl2)) { $auswahl = $auswahl2; }
else { $auswahl = "alt"; }

// Auswahl alte/neue Gesellschaften, beim Start werden standardm��ig die alten Gesellschaften angezeigt
if ($auswahl == "neu") { 
	$gesellschaft = "ges_neu";
	$gesellschaft_id = "ges_neu_id";
	$tarif = "tarif_neu";
	$tarif_id = "tarif_neu_id";
	$produkt = "produkt_neu";
	$produkt_id = "produkt_neu_id";
}
else {
	$gesellschaft = "ges_alt";
	$gesellschaft_id = "ges_alt_id";
	$tarif = "tarif_alt";
	$tarif_id = "tarif_alt_id";
	$produkt = "produkt_alt";
	$produkt_id = "produkt_alt_id";
}

//	refresh-Button wurde gedr�ckt
if (isset($refresh)) {
	if ($auswahl == "neu") { 
		$gesellschaft = "ges_neu";
		$gesellschaft_id = "ges_neu_id";
		$tarif = "tarif_neu";
		$tarif_id = "tarif_neu_id";
		$produkt = "produkt_neu";
		$produkt_id = "produkt_neu_id";
	}
	else {
		$gesellschaft = "ges_alt";
		$gesellschaft_id = "ges_alt_id";
		$tarif = "tarif_alt";
		$tarif_id = "tarif_alt_id";
		$produkt = "produkt_alt";
		$produkt_id = "produkt_alt_id";
	}
	// Refresh der einzelnen Seiten in den entsprechenden Frames
	echo "<script>onload=parent['gesedit'].location.href='ges_bearbeiten.php'</script>";
	echo "<script>onload=parent['tarife'].location.href='tarife.php'</script>";
}

// Speichern-Button wurde gedr�ckt ------------------
if (isset($speichern)) {
	if (empty($gesneu)) {		// keine Gesellschaft eingegeben
		$fehler = "Fehler: Sie müssen eine Gesellschaft eingeben!";
		$bginput = $bg_fehler;
	}

// Gesellschaft eingegeben -----------------------
	else { 
	
		$gesneu = quote_smart($gesneu);
		
// Test, ob eingegebene Gesellschaft schon in der Datenbank
		$sql = "SELECT $gesellschaft_id FROM $gesellschaft WHERE $gesellschaft = '$gesneu'";
		$abfrage = myqueryi($db, $sql);
			
		if (mysqli_num_rows($abfrage) != 0) {
			$fehler = "Fehler: Die Gesellschaft $gesneu gibt es schon!";
			$bginput = $bg_fehler;
		}
		else {
			$sql = "INSERT INTO $gesellschaft ($gesellschaft) VALUES ('$gesneu') ";
			$abfrage = myqueryi($db, $sql);
			$gesneu_id = mysqli_insert_id($db);
			
			$sql = "INSERT INTO $produkt ($gesellschaft_id, $tarif_id) VALUES ('$gesneu_id', '1') ";
			$abfrage = myqueryi($db, $sql);
			$produktneu_id = mysqli_insert_id($db);
			
			$fehler = "Die Gesellschaft $gesneu wurde mit ID: $gesneu_id in der Datenbank gespeichert";
		}
	}	// ende else Gesellschaft eingegeben
}	// Ende IF ISSET speichern


//---------------------------------------------------------------------------------------------------------------------------------------

// Start Script-Ablauf vor dem Dr�cken des Speichern-Buttons

	$sql  = "SELECT $gesellschaft AS Gesellschaft FROM $gesellschaft ";
	$sql .= " WHERE $gesellschaft_id > '1'";
	$sql .= " ORDER BY Gesellschaft ASC  ";
	$ergebnis = myqueryi($db, $sql);

?>

<!-- Ausgabe bei Start des Programms -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Gesellschaften/Tarife</title>
	<!-- allgemein/tarife/ges_select.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function FrameAendern (URI, Framename) { parent[Framename].location.href = URI; }
</script>
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }

.submitt {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.refresh {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">

<table width="500px" border="0" cellpadding="4" cellspacing="4">
<tr>
	<td>
		<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
			<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
	<?php
					if ($fehler) {
						echo "<tr><td colspan = \"3\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold;\">$fehler</span></td></tr>";
					}
				?>
				<form name="gesellschaft_neu" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
				<tr>
					<td valign = "middle">
						<table width="100%" cellspacing="5" bgcolor="moccasin">
							<tr>
		<?php
									if ($auswahl == "neu") {		// neue Gesellschaften sollen bearbeitet werden
										echo "<td align=\"left\" valign=\"middle\"><label><input type=\"radio\" name=\"auswahl\" value=\"alt\"></label>Gesellschaft ALT</td>";
    									echo "<td align=\"left\" valign=\"middle\" bgcolor=\"#800000\" width=\"250px\"><label><input type=\"radio\" name=\"auswahl\" value=\"neu\" checked=\"checked\"></label>
										      <span style=\"font-weight:bold; font-size:10pt; color:white; line-height:150%;\">Gesellschaft NEU</span></td>";
									}
									
									else {							// alte Gesellschaften sollen bearbeitet werden
										echo "<td align=\"left\" valign=\"middle\" bgcolor=\"#800000\" width=\"250px\"><label><input type=\"radio\" name=\"auswahl\" value=\"alt\" checked=\"checked\"></label>
										      <span style=\"font-weight:bold; font-size:10pt; color:white; line-height:150%;\">Gesellschaft ALT</span></td>";
    									echo "<td align=\"left\" valign=\"middle\"><label><input type=\"radio\" name=\"auswahl\" value=\"neu\"></label>Gesellschaft NEU</td>";
									}
								?>
							</tr>
						</table>
						
					</td>
				</tr>
				
				<tr>
					<td valign = "middle">
						<table width="100%" cellspacing="5" bgcolor="#ffffcc">
							<tr>
    							<td align="left" valign="middle"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">Eingabe Gesellschaft</span></td>
								<td align="left" valign="middle">
								<?php echo "<input type=\"text\" name=\"gesneu\" size = \"20\" maxlength =\"20\" value=\"$gesneu\" style=\"background-color:$bginput\">"; ?>
								<td align="right" valign="middle"><input type="submit" name="speichern" value="Speichern" class = "submitt"></td>
    						</tr>
						</table>
					</td>
				</tr>
				
				
				<tr><td valign = "top">
					<table id="ausgabe" cellspacing="4" width = "100%">
				
					<!-- Ausgabe der bereits vorhandenen Gesellschaften -------------------------------------------------------------- -->

						<tr>
							<td colspan="2" valign="middle"><span style="font-weight:bold; font-size:10pt; color:red; line-height:150%;">vorhandene Gesellschaften:</span></td>
							<td align="right" valign="middle"><input type="submit" name="refresh" value="REFRESH" class="refresh"></td>
						</tr>
	 <?php
					echo "<tr>";													// Tabellenkopf
					for($i = 0; $i < mysqli_num_fields($ergebnis); $i++) {		    // Anzahl der Tabellenzellen

						$row = mysqli_fetch_assoc($ergebnis);
						$spalte = array_keys($row);
						mysqli_data_seek($ergebnis, 0);

						echo "<td align\"left\" valign=\"middle\"><b>$spalte[$i]</b></td>"; // Name der Tabellenzelle

					}
					echo "<td align\"center\" valign=\"middle\" width=\"130px\"><div align=\"center\"><strong>Gesellschaft</strong></br>bearbeiten/löschen</div></td>";		// eine Zelle f�r Gesellschaft bearbeiten
					echo "<td align\"center\" valign=\"middle\" width=\"130px\"><div align=\"center\"><strong>Tarife</strong></br>bearbeiten/löschen</div></td>";			// eine Zelle f�r Tarife bearbeiten

					echo "</tr>\n";													// Tabellenkopf Ende
	
					$z=0;  //z�hler der datens�tze f�r bg_colour der zeilen
					$bg1 = "#eeeeee"; //die beiden hintergrundfarben
					$bg2 = "#dddddd";	
	
					for ($j = 0; $j < mysqli_num_rows($ergebnis); $j++) {			// Anzahl der Datens�tze
						$zeile = mysqli_fetch_row($ergebnis);						// Schleife f�r Daten-Zeilen
						
						// Abfrage, ob Tarife vorhanden sind
							$sql  = "SELECT $tarif FROM $produkt, $gesellschaft, $tarif ";
							$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id ";
							$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
							$sql .= " AND $gesellschaft = '$zeile[0]' AND $produkt.$tarif_id > '1' ";
							$result = myqueryi ($db, $sql);
							
						$bg=($z++ % 2) ? $bg1 : $bg2;
						echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
			
						for($i = 0; $i < mysqli_num_fields($ergebnis); $i++) {					// Schleife f�r Felder pro Zeile
							echo "<td align\"left\" valign=\"middle\">" . $zeile[$i] . "</td>";
						}
			
  					echo "<td valign = \"middle\" align = \"center\">
						<a href=\"ges_bearbeiten.php?ges=$zeile[0]&auswahl=$auswahl\" target=\"gesedit\" onclick=\"parent['tarife'].location.href='tarife.php'\">
				  		<img src=\"../../images/bearbeite.png\" alt=\"bearbeiten\" title=\"bearbeiten\" border=\"0\" /></a></td>";
					
					if (mysqli_num_rows($result) > 0) {
						echo "<td valign = \"middle\" align = \"center\">
						<a href=\"tarife.php?ges=$zeile[0]&auswahl=$auswahl\" target=\"tarife\" onclick=\"parent['gesedit'].location.href='ges_bearbeiten.php'\">
				  		<img src=\"../../images/bearbeite.png\" alt=\"bearbeiten\" title=\"bearbeiten\" border=\"0\" /></a></td>";
					}
					else {
						echo "<td align\"center\" valign=\"middle\"><div align=\"center\">
						<a href=\"tarife.php?ges=$zeile[0]&auswahl=$auswahl\" target=\"tarife\" onclick=\"parent['gesedit'].location.href='ges_bearbeiten.php'\">
						noch keine</a></div></td>";
					}
				echo "</tr>";
    		}
		echo "</table>";
				// Ende Ausgabe vorhandenener Gesellschaften +++++++++++++++++++++++++++++++++++++++++++++++
			?>
		</table>
	</table>
</table>
</form>
</td>
</tr>
</table>
</div>
</body>
</html>