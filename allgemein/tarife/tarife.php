<?php

// ---------------------------------------------------------------------------------------------//
// mit diesem Script k�nnen Tarife f�r eine bestimmte Gesellschaft eingegeben werden            //
// nur ein Administrator darf das Script nutzen												    //
// Gesellschaft und Art der Gesellschaft alt/neu kommen von ges_select.php					    //
// soll ein Tarif bearbeitet/gel�scht werden,                                                   //
// auf Standard (Gesell. ohne Tarif) gesetzt                                                    //
// werden die ben�tigten Daten an tarife_bearbeiten.php gesendet          		    			//
// danach werden ges_select.php und tarife.php refreshed        		                        //
// ---------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../../include/init.php");
sessiondauer();


$gruppe = $_SESSION['benutzer_gruppen'];			// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php
$bg_fehler = "#ff9966";								// Fehler-Farbe
unset($fehler);										// Fehlerausgabe wird zur�ckgesetzt

// POST- und GET-Variablen --------------------------------------------------------------------------------------

$speichern = $_POST["speichern"];			// Speichern neuer Tarif
$nein = $_POST["nein"];					// Nein-Button, ersetzen wird abgebrochen

$ges = $_GET["ges"];  						// von ges_select.php, zur Anzeige in den Textfeldern links
$auswahl = $_GET["auswahl"];				// von ges_select.php, zur Steuerung alte/neue Gesellschaft

$tarifneu = $_POST["tarifinput"];			// Tarif aus Textfeld

$gesalt = $_POST["gesalt"]; 				// alte Gesellschaft wird als hidden-Feld �bertragen

// Auswahl alte/neue Gesellschaften
if ($auswahl == "neu") { 
	$gesellschaft = "ges_neu";
	$gesellschaft_id = "ges_neu_id";
	$tarif = "tarif_neu";
	$tarif_id = "tarif_neu_id";
	$produkt = "produkt_neu";
	$produkt_id = "produkt_neu_id";
}
else {
	$gesellschaft = "ges_alt";
	$gesellschaft_id = "ges_alt_id";
	$tarif = "tarif_alt";
	$tarif_id = "tarif_alt_id";
	$produkt = "produkt_alt";
	$produkt_id = "produkt_alt_id";
}

// Bearbeitung ist abgebrochen  ---------------------------------------------
if (isset($nein)) {									// Leeren-Button gedr�ckt
	unset($tarifneu);
}
// ---------------------------------------------------------------------------

if (isset($speichern)) {																	// Speichern-Button wurde gedr�ckt

	$auswahl = $_POST["auswahl"];				// auswahl, kommt als hidden vom Script, geht zur�ck an tarife.php
	
	if (empty($ges)) { $fehler = "Fehler: Sie müssen zuerst eine Gesellschaft wählen!"; }
	elseif (empty($tarifneu)) { $fehler = "Fehler: Sie müssen einen Namen für den Tarif eingeben!"; }
	else { 	// neuer Tarif eingegeben -----------------------------------------------------------------------------------------------------------
	
		$tarifneu = quote_smart($tarifneu);
		
//	Test, ob es diesen Tarif f�r die Gesellschaft schon gibt
		$sql  = "SELECT $produkt_id ";
		$sql .= " FROM $produkt, $gesellschaft, $tarif ";
		$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id ";
		$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
		$sql .= " AND $gesellschaft.$gesellschaft = '$ges' AND $tarif.$tarif = '$tarifneu' ";
		$abfrage = myqueryi ($db, $sql);
			
		if (mysqli_num_rows($abfrage) != 0) {
			$fehler = "Fehler: Diesen Tarif gibt es schon für diese Gesellschaft!";
		}
		else {
			$sql1 = "SELECT $tarif_id FROM $tarif WHERE $tarif = '$tarifneu'";
			$sql2 = "INSERT INTO $tarif ($tarif) VALUES ('$tarifneu')";
			$tarifneu_id = id_ermitteln($db, $sql1, $sql2);
			
			$sql = "SELECT $gesellschaft_id FROM $gesellschaft WHERE $gesellschaft = '$ges'";
			$abfrage = myqueryi ($db, $sql);
			$gesneu_id = mysqli_fetch_array($abfrage);
			
			$sql = "INSERT INTO $produkt ($gesellschaft_id, $tarif_id) VALUES ('$gesneu_id[0]', '$tarifneu_id')";
			$abfrage = myqueryi ($db, $sql);
			$produktneu_id = mysqli_insert_id($db);
			
			$fehler = "Der Tarif $tarifneu wurde der Gesellschaft $ges hinzugefügt";
		}
		
		// Refresh von ges_select.php im linken Frame
		echo "<script>onload=parent['gesselect'].location.href='ges_select.php?auswahl2=$auswahl'</script>";
	}	//ende else Tarif eingegeben
}	// Ende IF ISSET speichern

// Start Script-Ablauf vor dem Dr�cken des Speichern-Buttons - Auslesen der Tarife zur gew�hlten Gesellschaft

$sql  = " SELECT $produkt_id AS ID, $tarif AS Tarif";
$sql .= " FROM $produkt, $gesellschaft, $tarif ";
$sql .= " WHERE $produkt.$gesellschaft_id = $gesellschaft.$gesellschaft_id ";
$sql .= " AND $produkt.$tarif_id = $tarif.$tarif_id ";
$sql .= " AND $gesellschaft.$gesellschaft = '$ges' ";
$sql .= " AND $produkt.$tarif_id > '1' ";
$sql .= " ORDER BY $tarif ASC ";
$ergebnis = myqueryi ($db, $sql);


?>

<!-- Hier f�ngt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Tarife</title>
	<!-- allgemein/tarife/tarife.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}

-->
</style>
</head>
<body>
<div align = "center">

<table width="500" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
     		<tr>
        		<td>
					<table width="100%"  border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
					
          				<tr bgcolor="moccasin"><td colspan="2" align = "center"><span style = "font-size:10pt; color:black; line-height:200%; font-weight:bold;">Tarife für Gesellschaft:<span style="color:red;"> <?php echo "$ges"; ?></span></span></td></tr>
		 
		 				<tr>
							<td valign = "top">
								<!-- Start Formular f�r neuen Tarif ++++++++++++++++++++++++++++++++++ // -->
								<form name="tarif_neu" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
		<?php
									echo "<input type=\"hidden\" name=\"ges\" value = \"$ges\">";
									echo "<input type=\"hidden\" name=\"auswahl\" value = \"$auswahl\">";
								?>
									<table width="100%" id="formular" cellspacing="4">
		  <?php
										if ($fehler) {
											echo "<tr><td colspan = \"2\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold;\">$fehler</span></td></tr>";
										}
										?>
										<tr>
											<td alig="left" valign = "middle">Eingabe Tarif:&nbsp;&nbsp;
	    										<input type="text" name="tarifinput" size="25" maxlength="25">&nbsp;&nbsp;
												<input type="submit" name="speichern" value="Speichern" class="submitt">&nbsp;&nbsp;<input type="reset" name="nein" value="Leeren" class="correct">
											</td>
										</tr>  
									</table>      
								<!-- Ende Formular f�r neuen Tarif ++++++++++++++++++++++++++++++++++ // -->
								</form>
							</td>
						</tr>
						
	  <?php

						if (mysqli_num_rows($ergebnis) !=0) {

						echo "<tr>
							<td valign = \"top\">";
								echo "<table id=\"ausgabe\" cellspacing=\"4\" width = \"100%\">";

								//Ausgabe der bereits vorhandener tarife +++++++++++++++++++++++++++++++++++++++++++++++
									echo "<tr><td colspan = \"5\">Bereits gespeicherte Tarife:</td></tr>";
	
									echo "<tr><td>ID</td>";								// Tabellenkopf
		
										if ($auswahl == "alt") {
											echo "<td>Gesellschaft-ALT</td>";
											echo "<td><span style = font-weight:bold;>Tarif-ALT</span></td>";	
										}
										else {
											echo "<td>Gesellschaft-NEU</td>";
											echo "<td><span style = font-weight:bold;>Tarif-NEU</span></td>";	
										}
										echo "<td>&nbsp;</td>";				// eine Zelle f�r den BEARBEITEN-Button angeh�ngt
										echo "<td>&nbsp;</td>";				// eine Zelle f�r den L�schen-Button angeh�ngt
										echo "</tr>\n";						// Tabellenkopf Ende
	
										$z=0;  								//z�hler der datens�tze f�r bg_colour der zeilen
										$bg1 = "#eeeeee"; 					//die beiden hintergrundfarben
										$bg2 = "#dddddd";
	
										for ($j = 0; $j < mysqli_num_rows($ergebnis); $j++)	{	// Anzahl der Datens�tze
											$zeile = mysqli_fetch_array($ergebnis);						// Schleife f�r Daten-Zeilen
											$bg=($z++ % 2) ? $bg1 : $bg2;
			
											echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
											echo "<td width = \"20\">" . $zeile[0] . "</td>";
											echo "<td >" . $ges . "</td>";
											echo "<td ><span style = font-weight:bold;>" . $zeile[1] . "</span></td>";
  											echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"tarife_bearbeiten.php?produkt=$zeile[0]&auswahl=$auswahl&ges=$ges&tarifalt=$zeile[1]\" target=\"_self\">
				 									 <img src=\"../../images/bearbeite.png\" alt=\"bearbeiten\" title=\"bearbeiten\" border=\"0\" /></a></td>";
											echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"tarife_loeschen.php?produktalt=$zeile[0]&auswahl=$auswahl&ges=$ges&tarifalt=$zeile[1]\" target=\"_self\">
					  								<img src=\"../../images/loesche.png\" alt=\"loeschen\" title=\"loeschen\" border=\"0\" /></a></td>";
											echo "</tr>";
    									}
								echo "</table>";
								// Ende Ausgabe vorhandener Tarife +++++++++++++++++++++++++++++++++++++++++++++++
							?>
						</td>
						</tr>
        			</table>
				</td>
      		</tr>
    	</table>
	</td>
  </tr>
</table>
</div>
</body>
</html>
<?php
}

?>