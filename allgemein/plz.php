<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

// -----------------------------------------------------------------------------------------------------//
// das Script liest alle PLZ mit den zugehörigen Werten aus, dazu wird die entsprechende Anzahl von 	//
// Wiedervorlagen und Neukunden im Moment der Abfrage ermittelt											//
// anschließend werden die Zahlen geordnet nach PLZ und Ort ausgegeben und für den PLZ-Bereich noch		//
// die Summen der Wiedervorlagen und Neukunden gebildet													//
// -----------------------------------------------------------------------------------------------------//

// Initialisierung der Hilfs-Variablen ----------------------------

$summe_wvl = 0;								// Summe der Wiedervorlagen im PLZ-Gebiez (alle Orte)
$summe_nkd = 0;								// Summe der Neukunden im PLZ-Gebiet (alle Orte)
$treffer_wvl = 0;							// für einen bestimmten Ort gibt es Wiedervorlagen
$treffer_nkd = 0;							// für einen bestimmten Ort gibt es Neukunden
$zeiger = 0;								// Zeiger auf die Datenbank-Query
$zeile = 0;									// Anzahl der Zeilen (Orte) pro PLZ


$heute = date("Y-m-d");

// alle PLZ und Orte ermitteln --------------------------------------------------------------------------------------
// diese Abfrage ist nötig, weil es kein Abfrageergebnis gibt, wenn (COUNT(termin_id)) Null ist,
// damit diese Orte in der Liste nicht fehlen (kann ja kein Termin sein, oder nur keine Wiedervorlage
// oder nur kein Neukunde) muss eine komplette Liste der Orte aus der DB gelesen werden

$sql = " SELECT DISTINCT ort, plz ";
$sql .= " FROM poo, plz, ort, ortsteil ";
$sql .= " WHERE poo.plz_id=plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
$sql .= " AND poo.poo_id > '1' ";
$sql .= " ORDER BY plz ASC, ort ASC ";

/*$statement = $pdo->prepare($sql);
if($statement->execute()) {
	$anzahl_ort = $statement->rowCount();
} else {
	echo "SQL Error <br />";
	echo $statement->queryString."<br />";
	echo $statement->errorInfo()[2];
}*/
//statement->execute(array(':user' => $_POST['user'], 'indate' => $indate));

//$query_ort = myquery($sql, $conn);
//$anzahl_ort = mysql_num_rows($query_ort);

$query_ort = myqueryi($db, $sql);
$anzahl_ort = mysqli_num_rows($query_ort);
// ------------------------------------------------------------------------------------------------------------------

// Wiedervorlagen aus Datenbank auslesen ------------------------------------------------------------------------------------

$sql = " SELECT DISTINCT ort, plz, COUNT(termin_id), kunden.poo_id ";
$sql .= " FROM termin, poo, kunden, plz, ort, ortsteil  ";
$sql .= " WHERE (termin.wiedervorlage_date BETWEEN '0000-00-01' AND '$heute') AND termin.wiedervorlage = '1'  AND termin.kd_id = kunden.kunden_id ";
$sql .= " AND kunden.verzogen = '0' AND kunden.belegt = '0' ";
$sql .= " AND kunden.handy = '0' AND termin.alt = '0' ";				// "alte" WVL sollen nicht angezeigt werden
$sql .= " And kunden.poo_id = poo.poo_id ";
$sql .= " AND poo.plz_id = plz.plz_id ";
$sql .= " AND poo.ort_id = ort.ort_id ";
$sql .= " AND poo.ortsteil_id = ortsteil.ortsteil_id ";
$sql .= " GROUP BY kunden.poo_id ";
$sql .= " ORDER BY plz ASC, ort ASC ";

/*$statement = $pdo->prepare($sql);
//$statement->execute(array(':heute' => $heute, 'indate' => $indate));
if($statement->execute()) {
	$$query_wvl = $statement->fetch();
} else {
	echo "SQL Error <br />";
	echo $statement->queryString."<br />";
	echo $statement->errorInfo()[2];
}*/

$query_wvl = myqueryi($db, $sql);
// --------------------------------------------------------------------------------------------------------------------------

// Neukunden aus Datenbank auslesen --------------------------

$sql = "SELECT DISTINCT ort, plz, COUNT(neukunde), kunden.poo_id ";
$sql .= "FROM poo, kunden, plz, ort, ortsteil  ";
$sql .= "WHERE kunden.neukunde = '1' ";
$sql .= "And kunden.poo_id = poo.poo_id ";
$sql .= "AND poo.plz_id = plz.plz_id ";
$sql .= "AND poo.ort_id = ort.ort_id ";
$sql .= "AND poo.ortsteil_id = ortsteil.ortsteil_id ";
$sql .= " GROUP BY kunden.poo_id ";
$sql .= " ORDER BY plz ASC, ort ASC ";

/*$statement = $pdo->prepare($sql);
if($statement->execute()) {
	$query_nkd = $statement->fetch();
} else {
	echo "SQL Error <br />";
	echo $statement->queryString."<br />";
	echo $statement->errorInfo()[2];
}*/


$query_nkd = myqueryi($db, $sql);
// -----------------------------------------------------------

?>

<!-- Bildschirmausgabe -->
<!DOCTYPE html>
<html lang=""de">
<head>
<title>PLZ</title>
	<!-- allgemein/plz.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	<!-- body {margin-top: 20px;} -->
</style>
</head>
<body>
<div align = "center">
<table width="500" cellpadding="1" cellspacing="0" bgcolor="#000000">				<!-- Tabelle für äußeren Rand -->
<tr>
<td>
<table width="100%" border="0" cellpadding="3" cellspacing="3" bgcolor="#eeeeee">	<!-- Tabelle für Daten -->

<?php

// Überschriftszeile --------------------------------------------------------

 echo "<tr>";
 	echo "<td><span style = \"font-weight:bold\">PLZ</td>";
	echo "<td><span style = \"font-weight:bold\">ORT</span></td>";
	echo "<td><span style = \"font-weight:bold\">WVL</span></td>";
	echo "<td><span style = \"font-weight:bold\">Neukunde</span></td>";
 echo "<tr>";
echo "<tr><td colspan = \"4\"><hr></td></tr>";
// --------------------------------------------------------------------------

// Beginn der Auswertung der Datenbankabfragen -------------------------------------
// erste PLZ (als Startwert) auslesen und dann Zeiger wieder auf Anfang setzen -----
$ort = mysqli_fetch_array($query_ort);
$plz = $ort[1];											// erste PLZ
mysqli_data_seek($query_ort, $zeiger);			// Rücksetzen des DB-Cursors
// ----------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------//	
// es folgt eine Schleife über alle Orte, wobei die Laufvariable $zeiger in der Schleife verändert wird		//
// die Schleife wird beim (n+1)ten Durchlauf nicht von Anfang an durchlaufen, sondern von der Stelle ab,		//
// an der der n-te Duchlauf abgebrochen wurde																//
// ---------------------------------------------------------------------------------------------------------//

while ($zeiger < mysqli_num_rows($query_ort)) {
	$ort = mysqli_fetch_array($query_ort);						// aktueller Ort wird ermittelt
	if ($ort[1] == $plz) {										// der oben ermittelte Ort hat die gleiche PLZ
																// wie der vorhergehende - gehören zusammen
	
		// jetzt folgt eine Schleife über alle ausgelesenen Wiedervorlagen,
		// die Schleife wird stets von Null ab durchlaufen - 
		// die Rechenzeit ließe sich durch Setzen eines variablen Zeigers verkürzen
		// der Ort der WVL wird mit dem aktuellen Ort und der aktuellen PLZ verglichen
		// denn es kann ja gleiche Ortsnamen zu unterschiedlichen PLZs geben!!!!
		// sollte ein Ergebnis gefunden werden, wird eine Hilfsvariable gesetzt, die den Treffer anzeigt
		$wvl = 0;
		for ($j=0; $j < mysqli_num_rows($query_wvl); $j++) {						// Beginn Schleife über alle WVL
			$ergebnis = mysqli_fetch_array($query_wvl);							// aktuelle WVL ermitteln
			if (($ergebnis[ort] == $ort[0]) AND ($ergebnis[plz] == $plz))  {	// der Ort der WVL stimmt mit dem aktuellen Ort überein
				$wvl = $wvl + $ergebnis[2];											// das ist die Anzahl der Wiedervorlagen
				$treffer_wvl = 1;												// ein Ergebnis wurde gefunden
			}
		}
		
		if (mysqli_num_rows($query_wvl) > 0) {
				mysqli_data_seek($query_wvl, 0);					// Datenbankcursor wird für nächsten Durchlauf zurückgesetzt
		}
			if ($treffer_wvl == 0) {							// kein Treffer gefunden
				$wvl = 0;										// Variable $wvl auf "0" setzen für Anzeige weiter unten
			}
			
		// jetzt folgt eine Schleife über alle ausgelesenen Neukunden,
		// die Schleife wird stets von Null ab durchlaufen - 
		// die Rechenzeit ließe sich durch Setzen eines variablen Zeigers verkürzen
		// der Ort des Neukunden wird mit dem aktuellen Ort und der aktuellen PLZ verglichen
		// denn es kann ja gleiche Ortsnamen zu unterschiedlichen PLZs geben!!!!
		// sollte ein Ergebnis gefunden werden, wird eine Hilfsvariable gesetzt, die den Treffer anzeigt
		$nkd = 0;
		for ($m=0; $m < mysqli_num_rows($query_nkd); $m++) {						// Beginn Schleife über alle Neukunden
			$daten = mysqli_fetch_array($query_nkd);								// aktuelle Neukundenzahl ermitteln
			if (($daten[ort] == $ort[0]) AND ($daten[plz] == $plz)) {			// der Ort der Neukunden stimmt mit dem aktuellen Ort überein
				$nkd = $nkd+$daten[2];												// das ist die Anzahl der Neukunden
				$treffer_nkd = 1;												// ein Ergebnis wurde gefunden
			}
		}
		if (mysqli_num_rows($query_nkd) > 0) {
			mysqli_data_seek($query_nkd, 0);					// Datenbankcursor wird für nächsten Durchlauf zurückgesetzt
		}
			if ($treffer_nkd == 0) {							// kein Treffer gefunden
				$nkd = 0;										// Variable $nkd auf "0" setzen für Anzeige weiter unten
			}
		
		$summe_wvl = $summe_wvl + $wvl;							// Summenbildung für Wiedervorlage (wird unten angezeigt)
		$summe_nkd = $summe_nkd + $nkd;							// Summenbildung für Neukunden (wird unten angezeigt)

		$stadt[$zeile] = $ort[0];								// Sammeln der zu einer PLZ gehörenden Orte in eienm Array
		$wvl_a[$zeile] = $wvl;									// Sammeln der zu einer PLZ gehörenden WVL in eienm Array
		$nkd_a[$zeile] = $nkd;									// Sammeln der zu einer PLZ gehörenden Neukunden in eienm Array
		
		$zeiger = $zeiger + 1;									// Hochsetzen des DB-Cursors für nächsten Durchlauf der while-Schleife
		$zeile = $zeile + 1;									// Hochsetzen des Index für die "Sammelarrays" für Ort, WVL und Neukunde
			
	} // ende if gleiche PLZ
	
	else {											// Vergleich aktuelle PLZ mit aus $query_ort ausgelesenen PLZ stimmt nicht mehr
													// Ausgabe der ermittelten Werte
		
		// Ausgabe einer "Summenzeile" es wird die PLZ und die dazugehörigen Summen WVL und Neukunde aus den "Sammelarrays" ausgegeben
		echo "<tr>";
			echo "<td><span style = \"font-weight:bold;\">$plz</span></td>";
			echo "<td><span style = \"font-weight:bold;\">Gesamt:</span></td>";
			echo "<td><span style = \"font-weight:bold;\">$summe_wvl</span></td>";
			echo "<td><span style = \"font-weight:bold;\">$summe_nkd</span></td>";
	 	echo "</tr>";
		
		// Ausgabe der einzelnen Orte mit dazugehorigen WVL und Neukunden in einer Zeile
		// die Zählvariable der Ausgebaschleife läuft bis zur Variable $zeile - die Anzahl der zu einem PLZ-Gebiet gehörenden Orte
		for ($k=0; $k<$zeile; $k++) {
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td>$stadt[$k]</td>";
				echo "<td>$wvl_a[$k]</td>";
				echo "<td>$nkd_a[$k]</td>";
	 		echo "</tr>";
		}
		
		echo "<tr><td colspan = \"4\"><hr></td></tr>";		// Trennlinie zwischen den einzelnen PLZ-Gebieten
		
		$plz = $ort[1];										// Festlegung der jetzt zu untersuchenden neuen PLZ

		// Rücksetzen der Hilfsvariablen und Zerstörung der "Sammelarrays" für den nächsten PLZ-Bereich
		$zeile = 0;
		$summe_wvl = 0;										
		$summe_nkd = 0;
		unset($wvl_a);
		unset($nkd_a);
		unset($stadt);
		
		mysqli_data_seek($query_ort, $zeiger);		// Datenbankcursor wird auf  $zeiger gesetzt,
															// damit startet die Abfrage der Orte für den nächsten PLZ-Bereich
															// nach den bereits behandelten
	} // ende else gleiche PLZ
	

	$treffer_wvl = 0;			// Rücksetzen der Hilfsvariablen - ein neuer Ort kann abgefragt werden
	$treffer_nkd = 0;			// Rücksetzen der Hilfsvariablen - ein neuer Ort kann abgefragt werden
	
}  // Ende while
// Ende Schleife -----------------------------------------------------------------------------------------------------------------------
			
		// Ausgabe der ermittelten Werte für den LETZTEN PLZ-Bereich, da für diesen die else-Bedingung neue PLZ nicht gilt,
		// es folgt ja keine neue PLZ mehr
		// --------------------------------------------------------------------------------------------------------------------------------
		
		// Ausgabe einer "Summenzeile" es wird die PLZ und die dazugehörigen Summen WVL und Neukunde aus den "Sammelarrays" ausgegeben
			echo "<tr>";
			echo "<td><span style = \"font-weight:bold;\">$plz</span></td>";
			echo "<td><span style = \"font-weight:bold;\">Gesamt:</span></td>";
			echo "<td><span style = \"font-weight:bold;\">$summe_wvl</span></td>";
			echo "<td><span style = \"font-weight:bold;\">$summe_nkd</span></td>";
	 	echo "</tr>";
		
		// Ausgabe der einzelnen Orte mit dazugehörigen WVL und Neukunden in einer Zeile
		// die Zählvariable der Ausgebaschleife läuft bis zur Variable $zeile - die Anzahl der zu einem PLZ-Gebiet gehörenden Orte
		for ($k=0; $k<$zeile; $k++) {
			echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td>$stadt[$k]</td>";
				echo "<td>$wvl_a[$k]</td>";
				echo "<td>$nkd_a[$k]</td>";
	 		echo "</tr>";
		}
		// -------------------------------------------------------------------------------------------------------------------------------
?>
		</table>	<!-- Ende Tabelle Daten -->
		</td>
	</tr>
</table>			<!-- Ende Tabelle Rand -->
</body>
</html>
