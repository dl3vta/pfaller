<?php

// -----------------------------------------------------------------------------------------------------------------//
// Dieses Script löscht eine Sperrzeit oder fügt eine Sperrzeit für einen bestimmten Außendienstler ein				//
// Die Daten kommen entweder von uebersicht_anzeige.php oder termin_aktuell.php										//
// Die Daten, die per GET von uebersicht.input.php kommen, werden wieder dahin zurückgegeben, 						//
// damit durch einen Neuaufbau der Seite der Erfolg der Aktion kontrolliert werden kann								//
// bei termin_aktuell.php liegen Außendienstler und Anzeigezeitraum in Session-Variablen vor, 						//
// es muss nichts zurückgegeben werden															 					//
// -----------------------------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

$telefonist = $_SESSION['benutzer_kurz'];
$aussendienst = $_SESSION['aussendienst'];					// Außendienstler, kommt von termin_aktuell.php

$file = $_GET["file"];										// enthält Rücksprungadresse des aufrufenden Scripts

if ($file == "uebersicht_anzeige") {
	$aussendienst = $_GET["ad"];							// Aussendienst - kommt von uebersicht_anzeige.php
	$ad = $_GET["ad"];										// Aussendienst - kommt von uebersicht_anzeige.php
}

$start1 = $_GET["start"];									// Beginn der Auswertung - kommt von uebersicht_anzeige.php
$ende1 = $_GET["ende"];										// Ende der Auswertung - kommt von uebersicht_anzeige.php
$zahl = $_GET["zahl"];										// Anzeigeperiode - kommt von uebersicht_anzeige.php

$s_datum = quote_smart($_GET["s_datum"]);					// Sperrzeit-Datum, kann von uebersicht_anzeige.php oder termin_aktuell.php kommen
$s_zeit = quote_smart($_GET["s_zeit"]);						// Sperrzeit-Zeit, kann von uebersicht_anzeige.php oder termin_aktuell.php kommen

$termin_id = quote_smart($_GET["t_id"]);					// Sperrzeit-ID, kann von uebersicht_anzeige.php oder termin_aktuell.php kommen
															// soll geläscht werden

// Debugging ------------------------
/*
echo "File: $file<br>";
echo "AD: $aussendienst<br>";
echo "Start: $start1<br>";
echo "Ende: $ende1<br>";
echo "Datum: $s_datum<br>";
echo "Zeit: $s_zeit<br>";
echo "T-ID: $termin_id<br>";
echo "Zahl: $zahl<br>";
*/
//-----------------------------------

if (empty($termin_id)) {											// neue Sperrzeit soll eingebucht werden
	$sql = "INSERT INTO termin (termin, zeit, kd_id, aussendienst, sperrzeit) ";
	$sql .= "VALUES ('$s_datum', '$s_zeit', '1', '$aussendienst', '1')";
	$abfrage = myqueryi($db, $sql);
}
else {																// vorhandene Sperrzeit soll gelöscht werden
	$sql = "DELETE FROM termin WHERE termin_id = '$termin_id' ";
	$abfrage = myqueryi($db, $sql);
}

if ($file == "uebersicht_anzeige") {								// Rücksprung zum aufrufenden Script
	echo "<script>location.href='" . $file . ".php?ad=$ad&start=$start1&ende=$ende1&zahl=$zahl'</script>";
}
else {
	echo "<script>location.href='" . $file . ".php'</script>";
}

?>

