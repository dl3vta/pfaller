<?php

// -----------------------------------------------------------------------------------------------------------------//
// Dieses Script stellt die Termine f�r einen/alle Au�endienstler/Telefonisten f�r einen bestimmten Zeitraum dar	//
// Die Daten (Mitarbeiter, Anzeigeperiode etc.) kommen per GET von uebersicht.input.php								//
// Sperrzeiten k�nnen gel�scht/eingebucht werden. Die n�tigen Daten dazu werden per GET an termin_sperrzeit.php		//
// �bergeben und kommen on daher wieder zur�ck, um den Erfolg der Aktion zu kontrollieren							//
// -----------------------------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

$gruppe = $_SESSION['benutzer_gruppen'];

// Variablendefinition -------------------------------------------------------
// ---------------------------------------------------------------------------

$file = "uebersicht_anzeige";								// wird als Kennzeichnung des aufrufenden Scripts an termin_sperrzeit.php übergeben

// Initialisierung Zeit-Array für Anzeige
 
$t_zeit = array("09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", 
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");
	
$zahl_t_zeit = count($t_zeit);						// Anzahl der Elemente im Zeit-Array

// GET-Variablen - werden an termin_sperrzeit.php übergeben und kommen wieder zurück
// ---------------------------------------------------------------------------

$ad = quote_smart($_GET["ad"]);							// Aussendienst - kommt von termin_sperrzeit.php zurück
$aq = quote_smart($_GET["aq"]);							// Telefonist - kommt von termin_sperrzeit.php zurück
$start = quote_smart($_GET["start"]);						// Beginn der Auswertung - kommt von termin_sperrzeit.php zurück
$ende = quote_smart($_GET["ende"]);						// Ende der Auswertung - kommt von termin_sperrzeit.php zurück
$zahl = quote_smart($_GET["zahl"]);						// Anzahl der anzuzeigenden Tage - kommt von termin_sperrzeit.php zurück

// Debugging -------------------------
/*
echo "AD: $ad<br />";
echo "AQ: $aq<br />";
echo "start: $start<br />";
echo "ende: $ende<br />";
echo "periode: $zahl<br />";
*/
// -----------------------------------

$span = $zahl + 1 ;		// Anzahl der anzuzeigenden Tage + eine Zelle vorn in der Tabelle für die Uhrzeit

$tag = substr($start, 6,2);
$monat = substr($start, 4,2);
$jahr = substr($start, 0,4);

// Mitarbeiter aus DB einlesen ----------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------
		
if (!empty($ad)) {													//Außendienstler sollen angezeigt werden
		
	if ($ad == 'alle') {											// alle 
		$sql = "SELECT user, vorname, name ";
		$sql .= " FROM user, name, vorname ";
		//$sql .= " WHERE user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
		//$sql .= " WHERE user.gruppen_id = '3' AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
		$sql .= " WHERE (user.gruppen_id = '3' OR user.gruppen_id = '5') AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
		$sql .= " ORDER BY user ASC";
	}
	else {															// ein bestimmter
		$sql = "SELECT user, vorname, name ";
		$sql .= " FROM user, name, vorname ";
		//$sql .= " WHERE user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id AND user = '$ad' ";
		//$sql .= " WHERE user.gruppen_id = '3' AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id AND user = '$ad' ";
		$sql .= " WHERE (user.gruppen_id = '3' OR user.gruppen_id = '5') AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id AND user = '$ad' ";
		$sql .= " ORDER BY user ASC";
	}
}
elseif (!empty($aq)) {												//Telefonisten sollen angezeigt werden
		
	if ($aq == 'alle') {											// alle 
		$sql = "SELECT user, vorname, name ";
		$sql .= " FROM user, name, vorname ";
		//$sql .= " WHERE user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
		//$sql .= " WHERE (user.gruppen_id = '2' OR user.gruppen_id = '1') AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
		$sql .= " WHERE (user.gruppen_id = '2' OR user.gruppen_id = '1' OR user.gruppen_id = '6' OR user.gruppen_id = '7') AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id ";
		$sql .= " ORDER BY user ASC";
	}
	else {															// ein bestimmter
		$sql = "SELECT user, vorname, name ";
		$sql .= " FROM user, name, vorname ";
		//$sql .= " WHERE user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id AND user = '$aq' ";
		//$sql .= " WHERE (user.gruppen_id = '2' OR user.gruppen_id = '1') AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id AND user = '$aq' ";
		$sql .= " WHERE (user.gruppen_id = '2' OR user.gruppen_id = '1' OR user.gruppen = '6' OR user.gruppen = '7') AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id AND user = '$aq' ";
		$sql .= " ORDER BY user ASC";
	}
}
		
$abfrage = myqueryi($db, $sql);

// --------------------------------------------------------------------------------------------------------------------------------------------------
// Ausgabe der Tabelle ------------------------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------------------------------------------
?>

		<!-- Hier fängt das HTML-Dokument an -->
		<!DOCTYPE html>
		<html lang="db">
		<head>
		<title>Termin-Übersicht</title>
			<!-- allgemein/uebersicht_anzeige.php -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div align = "center">
	
		<table border="0" width="600" class="zeiten" cellspacing="0" cellpadding="0">

		<?php	for ($z = 0; $z < mysqli_num_rows($abfrage); $z++) { 	// Beginn schleife für Mitarbeiter
		
				$mitarbeiter = mysqli_fetch_row($abfrage);
		
				if (!empty($ad)) { // Ausgabe für Aussendienst
					$sql = "SELECT termin_id, termin, zeit, sperrzeit, plz, abschluss, termin.nichtkunde, erledigt_date, termin.storno, kalt FROM termin, kunden, poo, plz ";
					$sql .= "WHERE termin.kd_id = kunden.kunden_id AND kunden.poo_id=poo.poo_id AND poo.plz_id=plz.plz_id AND termin.alt = '0' ";
					$sql .= "AND termin BETWEEN $start AND $ende AND termin.wiedervorlage = '0' AND aussendienst = '$mitarbeiter[0]' ";
					$sql .= "ORDER BY termin ASC, zeit ASC ";
				}
			
				elseif (!empty($aq)) { // Ausgabe für Aquise
					$sql = "SELECT termin_id, termin, zeit, sperrzeit, plz, abschluss, termin.nichtkunde, erledigt_date, termin.storno, kalt FROM termin, kunden, poo, plz ";
					$sql .= "WHERE termin.kd_id = kunden.kunden_id AND kunden.poo_id=poo.poo_id AND poo.plz_id=plz.plz_id AND termin.alt = '0'  ";
					$sql .= "AND termin BETWEEN $start AND $ende AND termin.wiedervorlage = '0' AND telefonist = '$mitarbeiter[0]' ";
					$sql .= "ORDER BY termin ASC, zeit ASC ";
				}

				$ergebnis = myqueryi($db, $sql);
				$datensatz = mysqli_num_rows($ergebnis);								// Anzahl der aus der Datenbank ausgelesenen Termine
		?>

			<table border="1" width="690" class="zeiten" cellspacing="0" cellpadding="0">
			<?php	if (!empty($ad)) {						// Außendienst wird angezeigt
					echo "<tr bgcolor = \"#ccffff\"><td colspan=\"$span\">";
						echo "<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";
							echo "<tr><td align=\"center\" valign=\"middle\"><span style=\"font-size:10pt;font-weight:bold; color:red; line-height:150%;\">Termine für:&nbsp;$mitarbeiter[1]&nbsp;$mitarbeiter[2]&nbsp;($mitarbeiter[0])</span></td></tr>";
							echo "</table>";
					echo "</td></tr>";
				}
				else {
					echo "<tr bgcolor = \"#ccccff\"><td colspan=\"$span\">";
						echo "<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";
						echo "<tr><td align=\"center\" valign=\"middle\"><span style=\"font-size:10pt;font-weight:bold; color:red; line-height:150%;\">Termine für:&nbsp;$mitarbeiter[1]&nbsp;$mitarbeiter[2]&nbsp;($mitarbeiter[0])</span></td></tr>";
							echo "</table>";
					echo "</td></tr>";
				}

  				echo "<tr>";
    				echo "<td>";
  						echo "<tr>";
  							echo "<td class=\"spalten\">&nbsp;</td>";						// Leerzelle, in den darunter liegenden Zellen steht die Uhrzeit 

  							for ($i= "0"; $i < $zahl; $i++) { 								// Termine (Datum) vom Start- bis Endewert als Kopfzeile ausgeben

								$beginn = mktime(0,0,0, $monat, $tag+$i, $jahr);
								setlocale (LC_TIME, 'de_DE');							// deutsche Benutzerumgebung eingestellt
								date_default_timezone_set("Europe/Berlin");
								//setlocale (LC_TIME, 'ge');									// deutsche Benutzerumgebung eingestellt
								$wochentag = strftime("%a", $beginn);						// Umstellung englischer Wochentag auf deutscher
			
								switch($wochentag) {
									case Sat: $wochentag = "Sa"; break;
									case Sun: $wochentag = "So"; break;
									case Mon: $wochentag = "Mo"; break;
									case Tue: $wochentag = "Di"; break;
									case Wed: $wochentag = "Mi"; break;
									case Thu: $wochentag = "Do"; break;
									case Fri: $wochentag = "Fr"; break;
								}
	
								$datum = ($wochentag) . "<br>" .(strftime("%d", $beginn)) . "." . (strftime("%m", $beginn)) . ".<br>" . (strftime("%Y", $beginn));
								$heute_datum = date("d")+0 . "-" . date("m");			// damit bei einstelligen Daten nur eine Stelle erscheint
								$aktuell = (strftime("%d", $beginn)) . "-" . (strftime("%m", $beginn));

								if ($heute_datum == $aktuell) {							// Spalte HEUTE - rot markiert
									echo "<td class=\"spalten\"><span style=\"font-weight:bold; color:red;\">$datum</span></td>";
								}
								else {
									echo "<td class=\"spalten\">$datum</td>";
								}
							} // Ende Termine (Datum) vom Start- bis Endewert als Kopfzeile ausgeben
							
						echo "</tr>";
						
						$treffer = "0";
						
						for ($j = 0; $j < $zahl_t_zeit; $j++) { 			// Schleife für Zeiten = Zeilen

							// ---------------------------------------------------------------------------------------------------------------------------------
							// es muss ermittelt werden, an welchem Tag zu der bestimmten Zeit $t_zeit[$j] mehrere Termine vorliegen
							// dazu werden alle aus der Datenbank ausgelesenen Termine durchlaufen und das Array "Teiler" geschaffen
							// das Array hat folgende Struktur: $teiler = ("0", "0", "2", "0"; ...) und wird am Anfang komplett mit 0 gefüllt
							// die einzelnen Elemente stehen für die Spalte, d.h. $datum($m), für $m=-1 (gestern) ...$m < $zahl (Anzahl der anzuzeigenden Tage)
							// für jeden gefundenen Datensatz wird das entsprechende Element um 1 erhöht
							// dies wird einmal pro Zeile gemacht, ansonsten müsste die Prozedur in jeder Zelle wiederholt werden
	
							for ($n = "0"; $n < ($zahl); $n++) {		// Initialisierung des Arrays "Teiler" für die Teilung der Zellen für Mehrfachtermine
								$teiler[$n] = "0";
							}
	
							for ($m = 0; $m < $zahl; $m++) { 			// Schleife für Tage (gestern + 14 Tage)
		
								$timestamp = mktime(0,0,0, $monat, $tag+$m, $jahr);
								$datum = (strftime("%Y", $timestamp)) . "-" .(strftime("%m", $timestamp)) . "-" . (strftime("%d", $timestamp));
		
								if ($datensatz > 0) {																			// überhaupt Termine da?
									for ($p = 0; $p < $datensatz; $p++) {	// 
										$termine = mysqli_fetch_row($ergebnis);
										if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0')) {// Vergleich Datum mit Termin und Zeit, Sperrzeiten ausgeschlossen
											$teiler[($m+1)] = $teiler[$m+1] + 1;												// da $m von -1 läuft, muss Zähler im Array um 1 erhöht werden (läuft von 0)
										}
									}
									mysqli_data_seek($ergebnis, '0');													// Rücksetzen der Ergebnisliste für nächste Zeile (Zeit)
								}
							}																									// Ende Schleife Tage
	
							// Ende Ermittlung Termin-Mehrfachbelegung
							// ---------------------------------------------------------------------------------------------------------------------------------

							// Auswertung der Daten und Darstellung in Kalenderform beginnt
	
							echo "<tr>";
								echo "<td class=\"zeiten\">$t_zeit[$j]</td>";						// Uhrzeit für Zeile
								for ($k = 0; $k < $zahl; $k++) { 									// Schleife für Tage (von gestern bis in 13 Tagen)
	
									// Hier muss die Abfrage des Arrays rein
									// Zeiten: j = zeile 0 = 9.00 Uhr, (zahl_t_zeit-1) = 20.00
									// Datum: k = Spalte k=0: heute, k=$zahl: in 14 Tagen
									// das Array heißt $termine, Datum = $termine[1], zeit = $termine[2]
									// Lösung: Suche im Array nach der Zeit $t_zeit[$j]
									// und dann nach Datum heute + $k
	
									$termin = mktime(0,0,0, $monat, $tag+$k, $jahr);
									$datum = (strftime("%Y", $termin)) . "-" .(strftime("%m", $termin)) . "-" . (strftime("%d", $termin));
									$wochentag = (strftime("%a", $termin));

									if ($datensatz > 0) {													// überhaupt Termine da?
				
										for ($l = 0; $l < $datensatz; $l++) {								// zeilenweises Auslesen der Datenbank-Query

											$termine = mysqli_fetch_row($ergebnis);
				
											if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j])) {	// Datum und Zeit stimmen überein
												if ($termine[3] == 1) {										// Termin ist Sperrzeit
													if ($ad == 'alle' OR !empty($aq)) {					// alle ADs oder alle Telefonisten ausgewählt -keine Terminbearbeitung möglich
														echo "<td class =\"sperrzeit\">S</td>";
													}
													else {
														echo "<td class =\"sperrzeit\"><a href=\"termin_sperrzeit.php?t_id=$termine[0]&ad=$mitarbeiter[0]&ende=$ende&start=$start&zahl=$zahl&file=$file\">S</a></td>";
													}
												}															// Ende Sperrzeit
										
											elseif ($teiler[($k+1)] > 1) {									// mehrere Termine auf einer Zeit
												mysqli_data_seek($ergebnis, '0');
					
												echo "<td class =\"doppelt\">";
												echo "<table width = \"100%\" cellspacing = \"0\" cellpadding = \"0\">";
		
												for ($p = 0; $p < $datensatz; $p++) {					// alle Datensätze überprüfen

													$termine = mysqli_fetch_row($ergebnis);
													if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[8])) AND ($termine[9] == '0')) {		// Abschluss
														echo "<tr>";
														echo "<td class =\"abschluss\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[8])) AND ($termine[9] == '1')) {		// kalter Abschluss
														echo "<tr>";
														echo "<td class =\"test\"><a href=\"termin_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '0') AND ($termine[6] == '0') AND (empty($termine[7]) OR ($termine[7] == '0000-00-00')) AND (empty($termine[8])) AND ($termine[9] == '0')) {	// offener Termin
														echo "<tr>";
														echo "<td class =\"toa\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[6] == '1')) {		// Nichtkunde
														echo "<tr>";
														echo "<td class =\"nichtkunde\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[5] == '0') AND ($termine[6] == '0') AND ($termine[7] > '2000-01-01') AND (empty($termine[8])) AND ($termine[9] == '0')) {	// bearbeitet
														echo "<tr>";
														echo "<td class =\"toas\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND (!empty($termine[8]))) {		// storno
														echo "<tr>";
														echo "<td class =\"storno\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
													elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[9] == '1')) {		// kalt
														echo "<tr>";
														echo "<td class =\"kalt\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
														echo "<tr>";
													}
												}
												echo "</table>";
												echo "</td>";
											} // ende mehrere Termine auf einer Zeit
						
											else { // nur ein Termin
												if (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[8])) AND ($termine[9] == '0')) {		// Abschluss
													echo "<td class =\"abschluss\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
												}
												elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '1') AND ($termine[6] == '0') AND (empty($termine[8])) AND ($termine[9] == '1')) {		// kalter Abschluss
													echo "<td class =\"test\"><a href=\"termin_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
												}
												elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[5] == '0') AND ($termine[6] == '0') AND (empty($termine[7]) OR ($termine[7] == '0000-00-00')) AND (empty($termine[8])) AND ($termine[9] == '0')) {	// offener Termin
													echo "<td class =\"toa\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
												}
												elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[6] == '1')) {		// Nichtkunde
													echo "<td class =\"nichtkunde\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
												}
												elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[5] == '0') AND ($termine[6] == '0') AND ($termine[7] > '2000-01-01') AND (empty($termine[8])) AND ($termine[9] == '0')) {	// bearbeitet
													echo "<td class =\"toas\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
												}
												elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND (!empty($termine[8]))) {		// storno
													echo "<td class =\"storno\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
												}
												elseif (($termine[1] == $datum) AND ($termine[2] == $t_zeit[$j]) AND ($termine[3] == '0') AND ($termine[9] == '1')) {		// kalt
													echo "<td class =\"kalt\"><a href=\"kunde_neu.php?t_id=$termine[0] & ad=$ad &aq=$aq & start=$start & ende=$ende & zahl=$zahl & file=termine\" target=\"uebersicht_unten\"><span style=\"font-size: xx-small; font-weight: lighter;\">$termine[4]</span></a></td>";
												}
											}		// Ende else nur ein Termin
										$treffer = "1";
									}
								}
								if (mysqli_num_rows($ergebnis) > 0) {
									mysqli_data_seek($ergebnis, '0');
								}
								if ($treffer == 0) {
									if ($wochentag === "Sun" OR $wochentag === "So") {
										echo "<td class=\"sonntag\">&nbsp;</td>";
									}
									elseif ($wochentag == "Sat" OR $wochentag == "Sa") {
										if ($ad == 'alle' OR !empty($aq)) {					// alle ADs oder alle Telefonisten ausgewählt - keine Terminbearbeitung möglich
											echo "<td class =\"samstag\">$t_zeit[$j]</td>";
										}
										else {
											echo "<td class =\"samstag\"><a href=\"termin_sperrzeit.php?s_zeit=$t_zeit[$j]&s_datum=$datum&ad=$mitarbeiter[0]&start=$start&ende=$ende&zahl=$zahl&file=$file\">$t_zeit[$j]</a></td>";
											
										}									
									}
									else {
										if ($ad == 'alle' OR !empty($aq)) {					// alle ADs oder alle Telefonisten ausgewählt - keine Terminbearbeitung möglich
											echo "<td class =\"zeiten\">$t_zeit[$j]</td>";
										}
										else {
											echo "<td class =\"zeiten\"><a href=\"termin_sperrzeit.php?s_zeit=$t_zeit[$j]&s_datum=$datum&ad=$mitarbeiter[0]&start=$start&ende=$ende&zahl=$zahl&file=$file\">$t_zeit[$j]</a></td>";
											
										}
									}
								}
								else {
									$treffer = "0";
								}
							} // ende if überhaupt Termine da?
							else {
								if ($wochentag === "Sun" OR $wochentag === "So") {
									echo "<td class=\"sonntag\">&nbsp;</td>";
								}
								elseif ($wochentag == "Sat" OR $wochentag == "Sa") {
									if ($ad == 'alle' OR !empty($aq)) {					// alle ADs oder alle Telefonisten ausgewählt - keine Terminbearbeitung möglich
										echo "<td class =\"samstag\">$t_zeit[$j]</td>";
									}
									else {
										echo "<td class =\"samstag\"><a href=\"termin_sperrzeit.php?s_zeit=$t_zeit[$j]&s_datum=$datum&ad=$mitarbeiter[0]&start=$start&ende=$ende&zahl=$zahl&file=$file\">$t_zeit[$j]</a></td>";
									}
								}
								else {
									if ($ad == 'alle' OR !empty($aq)) {					// alle ADs oder alle Telefonisten ausgewählt - keine Terminbearbeitung möglich
										echo "<td class =\"zeiten\">$t_zeit[$j]</td>";
									}
									else {
										echo "<td class =\"zeiten\"><a href=\"termin_sperrzeit.php?s_zeit=$t_zeit[$j]&s_datum=$datum&ad=$mitarbeiter[0]&start=$start&ende=$ende&zahl=$zahl&file=$file\">$t_zeit[$j]</a></td>";
									}
								}
							}
						}
						echo "</tr>";
					} // ende for zeilen
				echo "<br />";						// trennt die einzelnen Tabellen voneinander
			?>
</table>
</td></tr></table>
<?php

echo "</td></tr>";
	
	} // ende for-schleife für Mitarbeiter

?>

</table>				<!-- Ende Tabellen für Mitarbeiter -->

<br />					<!-- Abstand -->

<table bgcolor="#eeeeee" cellpadding=5>	<!-- Tabelle für Rücksprung-Link -->
<tr><td align = "center"><a href="uebersicht_input.php" target="_self"><span style="font-size:10pt; font-weight:bold; line-height:150%; color: red;">-> zur&uuml;ck zur Eingabe!</span></a></td></tr>
</table>
<br />
</div>
</body></html>

<?php	// Nachladen des Daten-Eingabe-Formulars im unteren Frame, leer - ohne Kunde oder Termin
echo "<script>onload=parent['uebersicht_unten'].location.href='kunde_neu.php'</script>";
?>