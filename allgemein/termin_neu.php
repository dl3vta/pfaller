<?php

// -------------------------------------------------------------------------------------------------------------------------------------//
// Diese Script dient zur Erzeugung von Terminen für einen bestimmten Außendienstler, der in einer Sessionvariable gespeichert ist.		//
// Die Eingangsdaten können entweder von termin_check.php oder termin_aktuell.php kommen.												//
// Zur Umschaltung (nur für Termindaten!!) dient die Variable $quelle, die als GET-Variable von termin_check.php kommt					//
// Kommen die Termindaten von termin_aktuell.php ($quelle nicht vorhanden) werden bereits existierende Termine verarbeitet				//
// kommen die Daten von termin_check.php gibt es 3 Fälle: gewählter Termin ist WVL - umwandeln, Termin ist laufender Termin - Update	//
// alle anderen Terminarten werden belassen, nur die Terminbemerkung wird in die Datenmaske übernommen und ein neuer Termin eingefügt.	//
//																																		//
// Bei Fa. Pfaller wird die Ergebnismeldung unterdrückt!!																				//
// -------------------------------------------------------------------------------------------------------------------------------------//
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

// Variablendefinition ----------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------

if (isset($_POST['login'])) $login = $_POST['login'];			// Formular wurde abgeschickt

//Formularvariablen werden initialisiert
if (isset($_GET['kd_id'])) {$kd_id = quote_smart($_GET['kd_id']);}   // kommt von termin_check.php
if (isset($_GET['t_id'])) {$t_id = quote_smart($_GET['t_id']);}       // kommt von termin_check.php
if (isset($_GET['quelle'])) {$quelle = quote_smart($_GET['quelle']);}  // kommt von termin_check.php, steuert die Behandlung der Termin-ID

$w1_zeit = $_POST["w1_zeit"];				// Wiedervorlage
$d1_zeit = $_POST["d1_zeit"];				// Termin (d = Datum)

$host = $_SERVER['HTTP_HOST'];

$telefonist = $_SESSION['benutzer_kurz'];
$aussendienst = $_SESSION['aussendienst'];

$speich = date("d.m.y");						// Datum von heute (für Speicherung)
$muster = "/^\d{2}$/";							// Test Datum, genau 2 Ziffern
$norefresh = 0;									// steuert den Refresh am ende des scriptes

$t_zeit = array("09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", 
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");			// Array für Zeiten
				
$wvl_zeit = array("", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", 
				"15:00", "16:00", "17:00", "18:00", "19:00", "20:00");			// Array für Wiedervorlage-Zeiten

$termin_id = $t_id;								// kommt vom Kalender oder von termin_check.php

// Daten kommen vom Kalender (termin_aktuell.php) oder von termin_check.php, aus der termin_id müssen die Kundendaten gewonnen werden
if (isset($t_id)) {
	$sql = "SELECT kd_id, termin, zeit, wiedervorlage_date,  w_zeit, rem_termin, ges_alt, tarif_alt, ges_neu, tarif_neu, von, bis, seit, abschluss, ";
	$sql .= " wiedervorlage, alt, nichtkunde, erledigt_date, vonr, kalt, termin.storno ";
	$sql .= " FROM termin, produkt_alt, ges_alt, tarif_alt, produkt_neu, ges_neu, tarif_neu ";
	$sql .= " WHERE termin.termin_id = '$t_id' ";
	$sql .= " AND termin.produkt_alt_id = produkt_alt.produkt_alt_id AND produkt_alt.ges_alt_id = ges_alt.ges_alt_id ";
	$sql .= " AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
	$sql .= " AND termin.produkt_neu_id = produkt_neu.produkt_neu_id AND produkt_neu.ges_neu_id = ges_neu.ges_neu_id ";
	$sql .= " AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
	$abfrage = myqueryi($db, $sql);
	$termin = mysqli_fetch_array($abfrage, MYSQLI_ASSOC);
	$kunden_id = $termin[kd_id];
	//var_dump($termin);
	
	$sql  = "SELECT vorname, name, vorwahl1, telefon, plz, ort, ortsteil, strasse, fax, email, rem_kunde, geboren, ";
	$sql  .= "einzug, nichtkunde, belegt, wiedervorlage, vertragskunde, vorwahl2, fax, branche, datenschutz ";
	$sql .= "FROM kunden, vorname, name, vorwahl1, poo, plz, ort, ortsteil, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$termin[kd_id]' AND kunden.vorname_id = vorname.vorname_id ";
	$sql .= "AND name.name_id = kunden.name_id AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	$sql .= "AND kunden.vorwahl2_id = vorwahl2.vorwahl2_id AND kunden.branche_id = branche.branche_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$ergebnis = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($ergebnis, MYSQLI_ASSOC);
}

// -----------------------------------------------------------------------------------------------------------------------------------

// Kunden-Daten kommen von termin_check.php - es soll ein komplett neuer Termin angelegt werden

else {
	$sql  = "SELECT vorname, name, vorwahl1, telefon, plz, ort, ortsteil,  strasse, fax, email, rem_kunde, geboren, ";
	$sql  .= "einzug, nichtkunde, belegt, wiedervorlage, vertragskunde, vorwahl2, fax, branche, datenschutz ";
	$sql .= "FROM kunden, vorname, name, vorwahl1, poo, plz, ort, ortsteil, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id' AND kunden.vorname_id = vorname.vorname_id ";
	$sql .= "AND name.name_id = kunden.name_id AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	$sql .= "AND kunden.vorwahl2_id = vorwahl2.vorwahl2_id AND kunden.branche_id = branche.branche_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";

	$ergebnis = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($ergebnis, MYSQLI_ASSOC);
	$kunden_id = $kd_id;
}

	$einzug = $kunde[einzug];
	$rem_kunde = $kunde[rem_kunde];
	$t_ortsteil = $kunde[ortsteil];
	$t_ort = $kunde[ort];
	$t_branche = $kunde[branche];
	$t_vorname = $kunde[vorname];
	$t_name = $kunde[name];
	$t_strasse = $kunde[strasse];
	$t_vorwahl2 = $kunde[vorwahl2];
	$t_fax = $kunde[fax];
	$t_email = $kunde[email];
	$datenschutz = $kunde[datenschutz];
	list($g_jahr, $g_monat, $g_tag) = explode("-", $kunde[geboren]);
	$g_jahr = substr($g_jahr, 2,2);
	
	$rem_termin = $termin[rem_termin];
	list($v_jahr, $v_monat, $v_tag) = explode("-", $termin[von]);
	$v_jahr = substr($v_jahr, 2, 2); 
	list($b_jahr, $b_monat, $b_tag) = explode("-", $termin[bis]);
	$b_jahr = substr($b_jahr, 2, 2); 
	list($s_jahr, $s_monat, $s_tag) = explode("-", $termin[seit]);
	$s_jahr = substr($s_jahr, 2, 2);
	
	list($d_jahr, $d_monat, $d_tag) = explode("-", $termin[termin]);		// Datum
	$d_jahr = substr($d_jahr, 2, 2);
	$d_jahr_alt = $d_jahr;
	$d_monat_alt = $d_monat;
	$d_tag_alt = $d_tag;
	$zeit_alt = $termin[zeit];
	$d_zeit = $termin[zeit];
	
	list($wvl_jahr, $wvl_monat, $wvl_tag) = explode("-", $termin[wiedervorlage_date]);		// Datum
	$wvl_jahr = substr($wvl_jahr, 2, 2);
	$wvl_jahr_alt = $wvl_jahr;
	$wvl_monat_alt = $wvl_monat;
	$wvl_tag_alt = $wvl_tag;
	$wvlzeit_alt = $termin[w_zeit];
	$wvlzeit = $termin[w_zeit];
	
	/*
	list($wvl_jahr, $wvl_monat, $wvl_tag) = explode("-", $termin[wiedervorlage_date]);		// Datum
	$wvl_jahr = substr($wvl_jahr, 2, 2);
	$wvl_jahr_alt = $wvl_jahr;
	$wvl_monat_alt = $wvl_monat;
	$wvl_tag_alt = $wvl_tag;
	$wvlzeit_alt = $termin[w_zeit];
	$wvlzeit = $termin[w_zeit];
	*/
	
	$gesselect = $termin[ges_alt];
	
// Untersuchung des in termin_check.php angeklickten Termins auf Update/Insert neuer Termin
// Folgende Fälle sind möglich:
// 1. WVL			WVL-Zeit und Datum löschen			Update Termin und Kunde
// 2. WVL-alt		alles außer Bemerkung löschen		Insert neuer Termin, Update Kunde
// 3. ToA			nichts löschen						Insert neuer Termin, Update Kunde
// 4. ToAs			alles außer Bemerkung löschen		Insert neuer Termin, Update Kunde
// 5. Abschluss		alles außer Bemerkung löschen		Insert neuer Termin, Update Kunde
// 6. Nichtkunde	alles außer Bemerkung löschen		Insert neuer Termin, Update Kunde
// 7. kalter Termin	alles außer Bemerkung löschen		Insert neuer Termin, Update Kunde
// 8. kalter AB		alles außer Bemerkung löschen		Insert neuer Termin, Update Kunde
// 9. Storno		alles außer Bemerkung löschen		Insert neuer Termin, Update Kunde

if (isset($quelle)) {
		unset($quelle);
		
		$einzug = 0;
		unset($v_jahr, $v_monat, $v_jahr, $b_jahr, $b_monat, $b_jahr, $s_jahr, $s_monat, $s_jahr);
		unset($termin[ges_neu], $termin[tarif_neu]);
	
	if ($termin[wiedervorlage] == '1' AND $termin[alt] == '0') {		// aktive WVL
		unset($d_jahr, $d_monat, $d_tag, $d_zeit, $d_jahr_alt, $d_monat_alt, $d_tag_alt, $d_zeit_alt);
	}
	elseif (!empty($termin[termin]) AND (empty($termin[erledigt_date]) OR $termin[erledigt_date] == '0000-00-00') AND $termin[alt] == '0' AND $termin[abschluss] == '0' AND $termin[nichtkunde] == '0') {		//aktiver Termin
		unset($wvl_jahr, $wvl_monat, $wvl_jahr, $wvlzeit);
	}
	elseif ($termin[nichtkunde] == '1') {		//Nichtkunde
		unset($t_id, $termin_id, $d_jahr, $d_monat, $d_tag, $d_zeit, $d_jahr_alt, $d_monat_alt, $d_tag_alt, $d_zeit, $wvl_jahr, $wvl_monat, $wvl_jahr, $wvlzeit);
		$termin[nichtkunde] = 0;		// damit aus NK-Termin eine WVL oder ein Termin werden kann
		$kunde[nichtkunde] = 0;			// damit aus NK-Termin eine WVL oder ein Termin werden kann
	}
	elseif (!empty($termin[storno]) AND $termin[abschluss]== '1') { //Storno
		unset($t_id, $termin_id, $d_jahr, $d_monat, $d_tag, $d_zeit, $d_jahr_alt, $d_monat_alt, $d_tag_alt, $d_zeit, $wvl_jahr, $wvl_monat, $wvl_jahr, $wvlzeit);
		$kunde[nichtkunde] = 0;	// damit aus Storno-Termin eine WVL oder ein Termin werden kann
	}
	else {		// alle restlichen Terminarten
		unset($t_id, $termin_id, $d_jahr, $d_monat, $d_tag, $d_zeit, $d_jahr_alt, $d_monat_alt, $d_tag_alt, $d_zeit, $wvl_jahr, $wvl_monat, $wvl_jahr, $wvlzeit);
	}
	
	$termin[nichtkunde] = 0;		// damit aus NK-Termin eine WVL oder ein Termin werden kann
}

	if ($kunde[nichtkunde] == 0 AND $termin[nichtkunde] == 0 ) { $nichtkunde = 0; }
	else { $nichtkunde = 1; }


// -----------------------------------------------------------------------------------------------------------------------------------
// es wurde der "Speichern"-Button gedrückt
// POST-Variablen werden zwecks Anzeige nach dem Wiederaufbau des Formulars in die entsprechenden Formularvariablen gespeichert
// -----------------------------------------------------------------------------------------------------------------------------------

if (isset($t_speichern)) {								// Termin speichern wurde gedrückt

$stamp = date("YmdHis");

if (!isset($telefonist) OR empty($telefonist) OR !isset($aussendienst) OR empty($aussendienst)) {
	$ausgeloggt = "Sie sind ausgeloggt!! Bitte einloggen und Termin erneut speichern!!";
}
else {	// eingeloggt

$gesinput = $_POST["gesinput"];
$gesselect = $_POST["gesselect"];

$heute = date("Y-m-d");			// Datum von heute
$aquiriert = date("Y-m-d H:i");

$t_vorname = quote_smart($kd_vorname);
$t_name = quote_smart($kd_name);
$g_tag = quote_smart($g1_tag);
$g_monat = quote_smart($g1_monat);
$g_jahr = quote_smart($g1_jahr);
$t_strasse = quote_smart($t1_strasse);
$t_vorwahl2 = quote_smart($t1_vorwahl2);
$t_fax = quote_smart($t1_fax);
$t_email = quote_smart($t1_email);
$t_branche = quote_smart($t1_branche);
$t_ortsteil_neu = quote_smart($ortsteil_neu);
$t_ortsteil = quote_smart($ortsteil_alt);
$gesinput = quote_smart($gesinput);
$datenschutz = quote_smart($datenschutz);

$w_tag = quote_smart($w1_tag);
$w_monat = quote_smart($w1_monat);
$w_jahr = quote_smart($w1_jahr);
$w_zeit = quote_smart($w1_zeit);			// Wiedervorlage
		
$d_tag = quote_smart($d1_tag);
$d_monat = quote_smart($d1_monat);
$d_jahr = quote_smart($d1_jahr);
$d_zeit = quote_smart($d1_zeit);			// Termin (d = Datum)

$rem_termin = quote_smart($rem1_termin);
$rem_kunde = quote_smart($t1_bemerkung);

$ne = ($_POST["ne_neu"]);										// Kunde wurde nicht erreicht
$doppelt = ($_POST["doppelt_neu"]);							// Termin soll doppelt belegt werden
$nichtkunde = ($_POST["nichtkunde_neu"]);						// Nichtkunde
$verzogen = ($_POST["verzogen_neu"]);							// Kunde ist verzogen
$einzug = ($_POST["einzug_neu"]);								// Einzugsermächtigung
$alt = ($_POST["alt_neu"]);									// Termin abschließen
$datenschutz = ($_POST["datenschutz_neu"]);

// Ende POST-Variablen -----------------------------------------------------------------------------------------
	
// Ermittlung der Belegung der Checkboxen ----------------------------------------------------------------------
$ne = checkbox($ne);													// Kunde nicht erreicht
$doppelt = checkbox($doppelt);											// Termin doppelt belegen
$nichtkunde = checkbox($nichtkunde);									// Nichtkunde
$verzogen = checkbox($verzogen);										// unbekannt verzogen
$einzug = checkbox($einzug);											// Einzugsermächtigung
$alt = checkbox($alt);													// Termin soll storniert werden
$datenschutz = checkbox($datenschutz);
/*
if (isset($einzug1)) { $einzug = "1"; }
else { $einzug = "0"; }

if (isset($nichtkunde1)) { $nichtkunde = "1"; }
else { $nichtkunde = "0"; }

if (isset($doppelt1)) { $doppelt = "1"; }
else { $doppelt = "0"; }

if (isset($verzogen1)) { $verzogen = "1"; }
else { $verzogen = "0"; }

if (isset($ne1)) { $ne = "1"; }
else { $ne = "0"; }
*/

//echo "Kunden-Id: $kunden_id<br>";
//echo "Kunden-Id: $kd_id<br>";
//echo "WVL: $termin[wiedervorlage_date]<br>";
//echo "Tag-alt: $w_tag_alt<br>";
//echo "Tag-neu: $w_tag<br>";
//echo "Monat-alt: $w_monat_alt<br>";
//echo "Monat-neu: $w_monat<br>";
//echo "Jahr-alt: $w_jahr_alt<br>";
//echo "Jahr-neu: $w_jahr<br>";
//echo "Vorwahl: $kunde[vorwahl1]<br>";
//echo "Telefon: $kunde[telefon]<br>";
//echo "Zeit: $d_zeit<br>";
//echo "vorwahl2 = $t_vorwahl2<br>";
//echo "fax = $t_fax<br>";
//echo "email = $t_email<br>";
//echo "t_branche = $t_branche<br>";
//echo "rem_termin = $rem_termin<br>";
//echo "bemerkung = $rem_kunde<br>";
//echo "g_tag = $g1_tag<br>";
//echo "g_monat = $g1_monat<br>";
//echo "g_jahr = $g1_jahr<br>";


	// Ortsteil eingegeben -----------------------------------------------------------------------------------------------------------------
		
				if (empty($ortsteil_neu)) {							// keine Eingabe im Feld neuer Ortsteil
					$ortsteil_alt = quote_smart($ortsteil_alt);
					if ($ortsteil_alt == $kunde[ortsteil]) {		// alter Ortsteil
						if (empty($ortsteil_alt)) {					// kein OT eingegeben - ortsteil_id = 1
							$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
							$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
							$sql .="AND ortsteil.ortsteil_id = '1' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
							$query = myqueryi($db, $sql);
							$daten = mysqli_fetch_row($query);
							$poo_id = $daten[0];
						}
						else {	//Ortsteil eingegeben
							$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
							$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
							$sql .="AND ortsteil.ortsteil = '$ortsteil_alt' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
							$query = myqueryi($db, $sql);
							$daten = mysqli_fetch_row($query);
							$poo_id = $daten[0];
						}
					}	// ende alter Ortsteil
		
					else {	// neuer Ortsteil
						if (empty($ortsteil_alt)) {					// kein OT eingegeben - ortsteil_id = 1
							$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
							$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
							$sql .="AND ortsteil.ortsteil_id = '1' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
							$query = myqueryi($db, $sql);
							$daten = mysqli_fetch_row($query);
							$poo_id = $daten[0];
						}
						else {	//Ortsteil eingegeben
							$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
							$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
							$sql .="AND ortsteil.ortsteil = '$ortsteil_alt' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
							$query = myqueryi($db, $sql);
							$daten = mysqli_fetch_row($query);
							$poo_id = $daten[0];
						}
					}	// ende else neuer Ortsteil
				} // keine Eingabe im Feld neuer Ortsteil
	
				else {	// $ortsteil_neu ist nicht leer
					$ortsteil_neu = quote_smart($ortsteil_neu);
					$sql = "SELECT poo_id FROM poo, plz, ort, ortsteil ";
					$sql .="WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id ";
					$sql .="AND ortsteil.ortsteil = '$ortsteil_neu' AND ort.ort = '$kunde[ort]' AND plz.plz = '$kunde[plz]'";
					$query = myqueryi($db, $sql);
					$daten = mysqli_fetch_row($query);
					if ($daten) {						// diese Kombi Ort/Ortsteil ist schon vorhanden
						$poo_id = $daten[0];
					}
					else {	// neue Kombi Ort/ortsteil
						$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil = '$ortsteil_neu'";	// ortsteil schon vorhanden?
						$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil_neu')";				// neuer Ortsteil
						$ortsteil_id = id_ermitteln($db, $sql1, $sql2);
			
						$sql = "SELECT ort_id FROM ort WHERE ort = '$kunde[ort]' ";
						$query = myqueryi($db, $sql);
						$ort = mysqli_fetch_row($query);
			
						$sql = "SELECT plz_id FROM plz WHERE plz = '$kunde[plz]' ";
						$query = myqueryi($db, $sql);
						$plz = mysqli_fetch_row($query);
			
						$sql = "INSERT INTO poo (ort_id, ortsteil_id, plz_id) VALUES ('$ort[0]', '$ortsteil_id', '$plz[0]')";
						$query = myqueryi($db, $sql);
						$poo_id = mysqli_insert_id($db);
					}	// ende neue Kombi Ort/ortsteil
				}	//ende else $ortsteil ist nicht leer
				
		
			// Ende Ortsteil -----------------------------------------------------------------------------------------------------------------------

			// Vorname ----------------------------------------------------------------------------------------------------
				$sql1 = "SELECT vorname_id FROM vorname WHERE vorname = '$t_vorname'";	// Vorname schon vorhanden?
				$sql2 = "INSERT INTO vorname (vorname) VALUES ('$t_vorname')";			// Vorname neu
				$vorname_id = id_ermitteln($db, $sql1, $sql2);

			// Name ----------------------------------------------------------------------------------------------------

				$sql1 = "SELECT name_id FROM name WHERE name = '$t_name'";				// Name schon vorhanden?
				$sql2 = "INSERT INTO name (name) VALUES ('$t_name')";					// Name neu
				$name_id = id_ermitteln($db, $sql1, $sql2);

			// Vorwahl 2 ----------------------------------------------------------------------------------------------------

				if (empty($t_vorwahl2)) {														// Feld Vorwahl2 ist nicht leer
					$vorwahl2_id = "1";
				}
				else {
					$vorwahl2 = quote_smart($t_vorwahl2);
					$sql1 = "SELECT vorwahl2_id FROM vorwahl2 WHERE vorwahl2 = '$t_vorwahl2'";	// Vorwahl2 schon vorhanden?
					$sql2 = "INSERT INTO vorwahl2 (vorwahl2) VALUES ('$t_vorwahl2')";			// Vorwahl2 neu
					$vorwahl2_id = id_ermitteln($db, $sql1, $sql2);
				}

				// Branche ----------------------------------------------------------------------------------------------------

			if (empty($t_branche)) {
				$branche_id = "1";
			}
			else {
				$sql = "SELECT branche_id FROM branche WHERE branche = '$t_branche'";
				$abfrage_branche = myqueryi($db, $sql);
				$daten = mysqli_fetch_array($abfrage_branche);
				$branche_id = $daten[0];
			}

// geboren ----------------------------------------------------------------------------------------------------
	if (empty($g_tag) OR empty($g_monat) OR empty($g_jahr)) {
		$geboren = "0000-00-00";
	}
	else {
		if ($g_jahr > 20) {
			$geboren = "19$g_jahr-$g_monat-$g_tag";
		}
		else {
			$geboren = "20$g_jahr-$g_monat-$g_tag";
		}
	}

// Ende Ortsteil ---------------------------------------------------------------------------------------------------------------------------------------

// Bearbeitung Gesellschaft-ALT
// Eingabe von ges_alt überschreibt select-feld
	// ---------------------------------------------------------------------------------------------------------------------------------------
	
		if (!empty($gesinput)) { $ges_alt = $gesinput; }
		elseif (!empty($gesselect)) { $ges_alt = $gesselect; }
		
		// eine Gesellschaft wurde eingegeben oder ausgewählt
		if (!empty($ges_alt)) {
		
			$sql1 = "SELECT ges_alt_id FROM ges_alt WHERE ges_alt = '$ges_alt'";	// Gesellschaft schon vorhanden?
			$sql2 = "INSERT INTO ges_alt (ges_alt) VALUES ('$ges_alt')";			// Gesellschaft neu
			$ges_alt_id = id_ermitteln($db, $sql1, $sql2);
		
			// Test, ob Gesellschaft mit tarif = 1 schon vorhanden ist, anmsonsten einfügen
			$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE ges_alt_id = '$ges_alt_id' AND tarif_alt_id = '1' ";
			$sql2 = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '1')";
			$produkt_alt_id = id_ermitteln($db, $sql1, $sql2);
			
		}
		else { $produkt_alt_id = "1"; }	// keine Gesellschaft

// ende Gesellschaft alt

// Ende Bearbeitung Kundendaten -----------------------------------------------------------------------------------------------------

// Nichtkunde ------------------------------------------------------------------------------------------------------------------------
	if ($nichtkunde == 1) {                                                // Kunde wird zum Nichtkunden
		$rem_kunde = $rem_kunde . " (NK: $telefonist: $speich) ";
		$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
		$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
		$sql .= " einzug = '$einzug', nichtkunde = '1', wiedervorlage = '0', einzug = '0', belegt = '0', neukunde = '0', datenschutz = '$datenschutz' ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		$query = myqueryi($db, $sql);

		$fehler = "Der Bestandskunde $vorname $name (ID: $kunden_id) wurde zum Nichtkunden.";

		// gewählter Termin wird zum Nichtkundentermin
		if ($t_id != 0) {
			$rem_termin = $rem_termin . " (NK:$telefonist: $speich) ";
			$sql = "UPDATE termin SET erledigt_date = '$heute', alt = '1', rem_termin = '$rem_termin', edit = '$stamp' ";
			$sql .= "WHERE termin_id = '$t_id'";
			$query = myqueryi($db, $sql);
			$fehler = $fehler . "<br />der termin $t_id wurde storniert.";
		}

	}
	// verzogen -------------------------------------------------------------------------------------------------------------------------
	
	elseif ($verzogen == 1) {
		$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
		$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
		$sql .= " telefon='000000', vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
		$sql .= " einzug = '$einzug', verzogen = '1', wiedervorlage = '0', einzug = '0', neukunde = '0', datenschutz = '$datenschutz' ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		
		$query = myqueryi($db, $sql);
		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) ist verzogen";
		$rem_termin = $rem_termin . " (abg:$telefonist: $speich) ";
		$sql = "SELECT termin_id, rem_termin FROM termin WHERE kd_id = '$kunden_id' AND wiedervorlage = '1' AND alt = '0' ";
		$wvl_abschluss = myqueryi($db, $sql);
		for ($i = 0; $i < mysqli_num_rows($wvl_abschluss); $i++) {		// alle Wiedervorlagen auf alt setzen
			$abschluss = mysqli_fetch_array($wvl_abschluss);
			$rem_termin = $abschluss[rem_termin] . " (abg:$telefonist: $speich) ";
			$sql1 = "UPDATE termin SET  erledigt_date = '$heute', alt = '1', rem_termin = '$rem_termin', edit = '$stamp' ";
			$sql1 .= "WHERE termin_id = '$abschluss[termin_id]'";
			$query = myqueryi($db, $sql1);
		}
	} // Ende Kunde ist verzogen
	
// nicht angetroffen -------------------------------------------------------------------------------------------------------------------------
	
	elseif ($ne == 1) {
		$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
		$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
		$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
		$sql .= " ne = '$heute', datenschutz = '$datenschutz'  ";
		$sql .= "WHERE kunden_id = '$kunden_id'";
		
		$query = myqueryi($db, $sql);
		$fehler = "Der Kunde $kunde[vorname] $kunde[name] (ID: $kunden_id) wurde nicht angetroffen";
	} // Ende Kunde nicht angetroffen
	
	else { //Terminbearbeitung
	
		if ($termin[abschluss] == 1 AND empty($termin[abschluss]) ) {							// Vertragskunde soll bearbeitet werden
			// der aktuelle Vertrag kann nicht bearbeitet werden
			// aus dem aktuellen Vertrag kann ein neuer Termin werden - Termindatum muss unterschiedlich sein!!
			// aus dem Vertrag kann eine Wiedervorlage werden
			
			$datum = "$d_jahr$d_monat$d_tag";
			$wvl_datum = "$w_jahr$w_monat$w_tag";
			
			if (empty($datum) AND empty($wvl_datum)) {		// weder Datum noch WVL-Datum eingegeben, Vertrag soll nicht weiterverarbeitet werden
				$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
				$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
				$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', datenschutz = '$datenschutz' ";
				//$sql .= " einzug = '$einzug', nichtkunde = '1', wiedervorlage = '0', vertragskunde = '0', einzug = '0', belegt = '0', neukunde = '0' ";
				$sql .= "WHERE kunden_id = '$kunden_id'";
				$fehler = "Die Kundendaten wurden gespeichert - Der Vertrag wurde nicht ver�ndert";
				$norefresh = 1;									// kein refresh am Ende des Scriptes
			}	// ende if Vertrag soll nicht weiterverarbeitet werden
			
			else {	// Vertrag soll weiterverarbeitet werden
			
				if (empty($d_tag) OR empty($d_monat) OR empty($d_jahr)) {		// kein Termin eingetragem
					if (empty($w_tag) OR empty($w_monat) OR empty($w_jahr)) {	// kein Wiedervorlagetermin eingetragem
		
						$fehler = "Sie müssen einen Termin/Wiedervorlagetermin eingeben!";
						$norefresh = 1;									// kein refresh am Ende des Scriptes
					}
					else {	// Wiedervorlagetermin
					// Bearbeitung Wiedervorlagetermin

						$muster = "/^\d{2}$/";							// Test Datum, genau 2 Ziffern

						if(!preg_match($muster, $w_tag) OR !preg_match($muster, $w_monat) OR !preg_match($muster, $w_jahr)) {	// Muster Datum passt nicht
							$fehler = "Fehler: Kein gültiges Datum für Wiedervorlage!";
							$norefresh = 1;									// kein refresh am Ende des Scriptes
						}
						else {  // Muster wiedervorlage WVL-Datum stimmt
							$wiedervorlage_datum = "20$w_jahr-$w_monat-$w_tag";
							if ($wiedervorlage_datum < $heute) {	// Termin in der Vergangenheit!!
								$fehler = "Die Wiedervorlage kann nicht in der Vergangenheit liegen!";
								$norefresh = 1;									// kein refresh am Ende des Scriptes
							}
							else { // Wiedervorlage in der Zukunft
								$rem_termin1 = $rem_termin . " ($telefonist: $speich) ";
								$rem_termin = $rem_termin . " ($telefonist: $speich) ";
								$sql = "INSERT INTO termin (wiedervorlage_date, w_zeit, kd_id, telefonist, aussendienst, rem_termin, ";
								$sql .= "produkt_alt_id, aquiriert, wiedervorlage, edit) ";
								$sql .= "VALUES ('$wiedervorlage_datum', '$w_zeit', '$kunden_id', '$telefonist', '$aussendienst', '$rem_termin', ";
								$sql .= "'$produkt_alt_id', '$aquiriert', '1', '$stamp') ";
								$query = myqueryi($db, $sql);
								$termin_id = mysqli_insert_id($db);
				
								$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
								$sql .= "wiedervorlage = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
			
								$fehler = "Die Kundendaten wurden gespeichert -<br />Ein Wiedervorlage-Termin mit der ID: $termin_id wurde eingefügt.";
								
								$bemerk = " ($speich abg: $telefonist) ";
								$norefresh = 0;									// refresh am Ende des Scriptes
							} // ende else Wiedervorlage in der Zukunft
						} // Ende else Muster wiedervorlage Datum stimmt
					}	// Ende else Wiedervorlage
				}	// kein if Termin eingegeben
				
				else {	// ein Termindatum wurde eingegeben
					if(!preg_match($muster, $d_tag) OR !preg_match($muster, $d_monat) OR !preg_match($muster, $d_jahr)) {	// Muster Datum passt nicht
						$fehler = "Fehler: Das ist kein gültiges Datum!!";
						$norefresh = 1;									// kein refresh am Ende des Scriptes
					}
					else {  // Muster Datum stimmt
						$termin_datum = "20$d_jahr-$d_monat-$d_tag";
						if ($termin_datum < $heute) {	// Termin in der Vergangenheit!!
							$fehler = "Der Termin kann nicht in der Vergangenheit liegen!";
							$norefresh = 1;									// kein refresh am Ende des Scriptes
						}
						else { // Termin in der Zukunft
							// Überprüfung auf doppelt
							$sql = "SELECT termin_id FROM termin WHERE termin = '$termin_datum' AND zeit = '$d_zeit' AND aussendienst = '$aussendienst' AND alt = '0' ";		// ist der Termin schon belegt?
								$query = myqueryi($db, $sql);
								$anz_doppelt = mysqli_num_rows($query);
								if (($anz_doppelt > 0) AND ($doppelt == 0)) {
									$fehler = "Fehler: Der Termin ist bereits belegt - soll er mehrfach belegt werden bitte \"2x\" anklicken!!";
									$doppler = '1';			// wird im Formular ausgewertet - Zeile Doppelt = Rot
									$norefresh = 1;									// kein refresh am Ende des Scriptes
								}
								else {
									if (isset($t_id) AND $termin[wiedervorlage] == '1' AND $termin[alt] == '0') {	// Kundentyp ist Wiedervorlage, in der Abfrage stehen nur Wiedervorlagen
										$rem_termin = $rem_termin . " ($telefonist: $speich) ";
										$sql = "UPDATE termin SET termin = '$termin_datum', zeit = '$d_zeit', aussendienst='$aussendienst', wiedervorlage_date = '0000-00-00', w_zeit = '00:00', rem_termin = '$rem_termin', ";
										$sql .= "produkt_alt_id = '$produkt_alt_id', aquiriert = '$aquiriert', wiedervorlage = '0', telefonist = '$telefonist', edit = '$stamp'  ";
										$sql .= "WHERE termin_id = '$t_id'";
										$query = myqueryi($db, $sql);
								
										$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
										$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
										$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
										$sql .= "einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
										$sql .= "WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);
																
										$bemerk = " ($speich abg: $telefonist) ";
										//$wvl_alt = wvl_alt($kunden_id, $heute, $bemerk );			// alle ALTEN WVL abschlie�en
								
										$fehler = "Die Kundendaten wurden gespeichert -<br />Der Wiedervorlage-Termin mit der ID: $t_id wurde zum Termin";
										$norefresh = 0;									// refresh am Ende des Scriptes
										} // ende Kundentyp ist Wiedervorlage
									else {
										$rem_termin = $rem_termin . " ($telefonist: $speich) ";
										$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, rem_termin, ";
										$sql .= "produkt_alt_id, aquiriert, edit) ";
										$sql .= "VALUES ('$termin_datum', '$d_zeit', '$kunden_id', '$telefonist', '$aussendienst', '$rem_termin', ";
										$sql .= "'$produkt_alt_id', '$aquiriert', '$stamp') ";
										$query = myqueryi($db, $sql);
										$termin_id = mysqli_insert_id($db);
				
										$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
										$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
										$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
										$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
										$sql .= "WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);
			
										$fehler = "Die Kundendaten wurden gespeichert - <br />Ein Termin mit der ID: $termin_id wurde eingefügt";
										$norefresh = 0;									// refresh am Ende des Scriptes
									}
								}
						} // ende else Termindatum in der Zukunft
					}	// ende else Datum stimmt
				}	// ende else ein Termindatum wurde eingegeben
			}	// ende else Vertrag soll weiterverarbeitet werden
		}	// ende if Vertragskunde
		
		else {	// kein Vertragskunde
		
			if (empty($d_tag) OR empty($d_monat) OR empty($d_jahr)) {		// kein Termin eingetragem
				if (empty($w_tag) OR empty($w_monat) OR empty($w_jahr)) {	// kein Wiedervorlagetermin eingetragem
					$fehler = "Sie müssen einen Termin/Wiedervorlagetermin eingeben!";
					$norefresh = 1;									// kein refresh am Ende des Scriptes
				}
				else {	// Wiedervorlagetermin

					if(!preg_match($muster, $w_tag) OR !preg_match($muster, $w_monat) OR !preg_match($muster, $w_jahr)) {	// Muster Datum passt nicht
						$fehler = "Fehler: Kein gültiges Datum für Wiedervorlage!";
						$norefresh = 1;									// kein refresh am Ende des Scriptes
					}
					else {  // Muster wiedervorlage Datum stimmt
			
						$wiedervorlage_datum = "20$w_jahr-$w_monat-$w_tag";
					
						if ($wiedervorlage_datum < $heute) {	// Termin in der Vergangenheit!!
							$fehler = "Die Wiedervorlage kann nicht in der Vergangenheit liegen!";
							$norefresh = 1;									// kein refresh am Ende des Scriptes
						}
						
						else { // Wiedervorlage in der Zukunft
						
							if (isset($t_id) AND $termin[wiedervorlage] == '1' AND $termin[alt] == '0') {	// Kundentyp ist Wiedervorlage, in der Abfrage stehen nur Wiedervorlagen
								
								// Wiedervorlage soll storniert werden, im Anschluss check, ob Kunde noch Vertragskunde ist -> Update
								if ($alt == '1') {
									$rem_termin = $rem_termin . " (abg:$telefonist: $speich) ";
									$sql = "UPDATE termin SET wiedervorlage_date = '$wiedervorlage_datum', w_zeit = '$w_zeit', rem_termin = '$rem_termin', ";
									$sql .= "produkt_alt_id = '$produkt_alt_id', erledigt_date = '$heute', wiedervorlage = '1', alt = '1', telefonist = '$telefonist', edit = '$stamp' ";
									$sql .= "WHERE termin_id = '$t_id'";
									$query = myqueryi($db, $sql);
									
									// Test, ob Kunde noch laufende Verträge hat
									$sql = "SELECT termin_id FROM termin WHERE abschluss = '1' AND alt = '0' AND (storno IS NULL OR storno='0')";
									$query = myqueryi($db, $sql);
									if (mysqli_num_rows($query) > 0) {
										$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
										$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
										$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
										$sql .= "wiedervorlage = '0', einzug = '$einzug', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', vertragskunde = '1', datenschutz = '$datenschutz' ";
										$sql .= "WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);
									}
									else {
										$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
										$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
										$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
										$sql .= "wiedervorlage = '0', einzug = '$einzug', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
										$sql .= "WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);
									}
																
									$bemerk = " ($speich abg: $telefonist) ";
									//$wvl_alt = wvl_alt($kunden_id, $heute, $bemerk);			// alle ALTEN WVL abschließen
								
									$fehler = "Die Kundendaten wurden gespeichert -<br />Der Wiedervorlage-Termin mit der ID: $t_id wurde storniert";
									$norefresh = 0;									// refresh am Ende des Scriptes
								
								}
								
								else {
									$rem_termin = $rem_termin . " ($telefonist: $speich) ";
									$sql = "UPDATE termin SET wiedervorlage_date = '$wiedervorlage_datum', w_zeit = '$w_zeit', rem_termin = '$rem_termin', ";
									$sql .= "produkt_alt_id = '$produkt_alt_id', aquiriert = '$aquiriert', wiedervorlage = '1', telefonist = '$telefonist', edit = '$stamp'  ";
									$sql .= "WHERE termin_id = '$t_id'";
									$query = myqueryi($db, $sql);
								
									$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
									$sql .= "wiedervorlage = '1', einzug = '$einzug', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
																
									$bemerk = " ($speich abg: $telefonist) ";
									//$wvl_alt = wvl_alt($kunden_id, $heute, $bemerk);			// alle ALTEN WVL abschließen
								
									$fehler = "Die Kundendaten wurden gespeichert -<br />Der Wiedervorlage-Termin mit der ID: $t_id wurde ver�ndert";
									$norefresh = 0;									// refresh am Ende des Scriptes
								}
					
							} // ende Kundentyp ist Wiedervorlage
						
							else { // keine alte Wiedervorlage - neue Wiedervorlage
									$rem_termin = $rem_termin . " ($telefonist: $speich) ";
									$sql = "INSERT INTO termin (wiedervorlage_date, w_zeit, kd_id, telefonist, aussendienst, rem_termin, ";
									$sql .= "produkt_alt_id, aquiriert, wiedervorlage, edit) ";
									$sql .= "VALUES ('$wiedervorlage_datum', '$w_zeit', '$kunden_id', '$telefonist', '$aussendienst', '$rem_termin', ";
									$sql .= "'$produkt_alt_id', '$aquiriert', '1', '$stamp') ";
									$query = myqueryi($db, $sql);
									$termin_id = mysqli_insert_id($db);
									
									$bemerk = " ($speich abg: $telefonist) ";
									//$wvl_alt = wvl_alt($kunden_id, $heute, $bemerk );			// alle ALTEN WVL abschließen
				
									$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
									$sql .= "wiedervorlage = '1', einzug = '$einzug',  nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
			
									$fehler = "Die Kundendaten wurden gespeichert -<br />Ein Wiedervorlage-Termin mit der ID: $termin_id wurde eingefügt";
									$norefresh = 0;									// refresh am Ende des Scriptes
								} // ende else neue Wiedervorlage
						} // ende else Termin in der Zukunft
					} // Ende else Muster wiedervorlage Datum stimmt
				}	// Ende else Wiedervorlage
			}	// kein if Termin eingegeben 
		
// Termin eingegeben ----------------------------------------------------------------------------------------------------------------------------
		
			else {	// termin eingegeben
			
			// Termindatum vorhanden, WVL-Datum eingegeben - Termin soll in WVL gewandelt werden ----------------------------------------------------------------------
			
				if($t_id AND (!empty($w_tag) AND !empty($w_monat) AND !empty($w_jahr))) { // daten kommen aus Kalender

					if(!preg_match($muster, $w_tag) OR !preg_match($muster, $w_monat) OR !preg_match($muster, $w_jahr)) {	// Muster Datum passt nicht
						$fehler = "Fehler: Kein gültiges Datum für Wiedervorlage!";
						$norefresh = 1;									// kein refresh am Ende des Scriptes
					}
					else {  // Muster wiedervorlage Datum stimmt
						$wiedervorlage_datum = "20$w_jahr-$w_monat-$w_tag";
						if ($wiedervorlage_datum < $heute) {	// Termin in der Vergangenheit!!
							$fehler = "Die Wiedervorlage kann nicht in der Vergangenheit liegen!";
							$norefresh = 1;									// kein refresh am Ende des Scriptes
						}
						else { // Wiedervorlage in der Zukunft
							$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
							$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
							$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
							$sql .= "wiedervorlage = '1', einzug = '$einzug',  belegt = '0', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);
							
							$rem_termin1 = $termin[rem_termin] . " ($speich abg: $telefonist) ";
							$sql = "UPDATE termin SET alt='1', erledigt_date='$heute', rem_termin='$rem_termin1', edit = '$stamp' WHERE termin_id = '$t_id'";		// Termin wird abgeschlossen
							$query = myqueryi($db, $sql);
							
							$rem_termin = $rem_termin . " ($telefonist: $speich) ";
							$sql = "INSERT INTO termin (wiedervorlage_date, w_zeit, kd_id, telefonist, rem_termin, ";		// neue WVL!
							$sql .= "produkt_alt_id, aquiriert, wiedervorlage, edit) ";
							$sql .= "VALUES ('$wiedervorlage_datum', '$w_zeit', '$kunden_id', '$telefonist', '$rem_termin', ";
							$sql .= "'$produkt_alt_id', '$aquiriert', '1', '$stamp') ";
							$query = myqueryi($db, $sql);
							$termin_id = mysqli_insert_id($db);
							
							$bemerk = " ($speich abg: $telefonist) ";
							//$wvl_alt = wvl_alt($kunden_id, $heute, $bemerk );			// alle ALTEN WVL abschließen
							
							$fehler = "Die Kundendaten wurden gespeichert -<br />Der Termin ID: $t_id wurde abgeschlossen -<br />ein neuer WVL-Termin mit ID: $termin_id wurde eingefügt.";
							$norefresh = 0;									// refresh am Ende des Scriptes
						} // ende else WVL in der Zukunft
					} // ende else Muster WVL stimmt
			// Ende if Termin in WVL umwandeln -----------------------------------------------------------------------------------------------------------------------
			} // ende daten kommen vom Kalender





			else { // kein WVL-Datum eingegeben
				if(!preg_match($muster, $d_tag) OR !preg_match($muster, $d_monat) OR !preg_match($muster, $d_jahr)) {	// Muster Datum passt nicht
					$fehler = "Fehler: Das ist kein gültiges Datum!!";
					$norefresh = 1;									// kein refresh am Ende des Scriptes
				}
				else {  // Muster Datum stimmt
	
					$termin_datum = "20$d_jahr-$d_monat-$d_tag";
				
					if ($termin_datum < $heute) {	// Termin in der Vergangenheit!!
						$fehler = "Der Termin kann nicht in der Vergangenheit liegen!";
						$norefresh = 1;									// kein refresh am Ende des Scriptes
					}
					
					else { // Termin in der Zukunft
					
						if ($d_tag_alt == $d_tag AND $d_monat_alt == $d_monat AND $d_jahr_alt == $d_jahr ) { // bestehenden termin bearbeiten
							if ($zeit_alt == $d_zeit) {  // termin nur bearbeiten
								$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
								$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
								
								$rem_termin = $rem_termin . " ($telefonist: $speich) ";
								$sql = "UPDATE termin SET termin = '$termin_datum', zeit = '$d_zeit', rem_termin = '$rem_termin', ";
								$sql .= "produkt_alt_id = '$produkt_alt_id', aquiriert = '$aquiriert', edit = '$stamp' ";
								$sql .= "WHERE termin_id = '$t_id'";
								$query = myqueryi($db, $sql);
							
								$fehler = "Die Kundendaten wurden gespeichert - Die Termindaten wurden verändert!";
								$norefresh = 0;									// refresh am Ende des Scriptes
							} // ende termin nur bearbeiten
							else { // termin soll verschoben werden, "alte" Termine werden nicht gezählt
								$sql = "SELECT termin_id FROM termin WHERE termin = '$termin_datum' AND zeit = '$d_zeit' AND aussendienst = '$aussendienst' AND alt = '0' ";		// ist der Termin schon belegt?
								$query = myqueryi($db, $sql);
								$anz_doppelt = mysqli_num_rows($query);
				
								if (($anz_doppelt > 0) AND ($doppelt == 0)) {
									$fehler = "Fehler: Der Termin ist bereits belegt - soll er mehrfach belegt werden bitte \"2x\" anklicken!!";
									$doppler = '1';			// wird im Formular ausgewertet - Zeile Doppelt = Rot
									$norefresh = 1;									// kein refresh am Ende des Scriptes
								}
								else {	// Termin ist noch frei oder soll doppelt belegt werden
									$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
									$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
								
									$rem_termin = $rem_termin . " ($telefonist: $speich) ";
									$sql = "UPDATE termin SET termin = '$termin_datum', zeit = '$d_zeit', rem_termin = '$rem_termin', ";
									$sql .= "produkt_alt_id = '$produkt_alt_id', aquiriert = '$aquiriert', edit = '$stamp' ";
									$sql .= "WHERE termin_id = '$t_id'";
									$query = myqueryi($db, $sql);
						
									$fehler = "Die Kundendaten wurden gespeichert -<br />Der Termin mit der ID: $termin_id wurde auf den $d_tag.$d_monat.$d_jahr, $d_zeit verschoben!";
									$norefresh = 0;									// refresh am Ende des Scriptes
								} // ende else termin noch frei
							} // ende Termin soll verschoben werden
						} // ende bestehenden termin bearbeiten
						
						else { // neue Daten für termin, "alte" Termine werden nicht gezählt
				
						$sql = "SELECT termin_id FROM termin WHERE termin = '$termin_datum' AND zeit = '$d_zeit' AND aussendienst = '$aussendienst' AND alt = '0' ";		// ist der Termin schon belegt?
						$query = myqueryi($db, $sql);
						$anzahl = mysqli_num_rows($query);
				
						if (($anzahl > 0) AND ($doppelt == 0)) {
							$fehler = "Fehler: Der Termin ist bereits belegt - soll er mehrfach belegt werden bitte \"2x\" anklicken!!";
							$doppler = '1';			// wird im Formular ausgewertet - Zeile Doppelt = Rot
							$norefresh = 1;									// kein refresh am Ende des Scriptes
						}
						else {	// Termin ist noch frei
						
							if (!empty($d_tag_alt)) { // Termin bereits vorhanden - soll verlegt werden
								$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
								$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
								$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
								$sql .= "wiedervorlage = '0', einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
								$sql .= "WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
								
								$rem_termin = $rem_termin . " ($telefonist: $speich) ";
								$sql = "UPDATE termin SET termin = '$termin_datum', zeit = '$d_zeit', rem_termin = '$rem_termin', ";
								$sql .= "produkt_alt_id = '$produkt_alt_id', aquiriert = '$aquiriert', edit = '$stamp' ";
								$sql .= "WHERE termin_id = '$t_id'";
								$query = myqueryi($db, $sql);
							
								$fehler = "Die Kundendaten wurden gespeichert -<br />Der Termin mit der ID: $termin_id wurde auf den $d_tag.$d_monat.$d_tag, $d_zeit verschoben!";
								$norefresh = 0;									// refresh am Ende des Scriptes
								} // ende termin verlegen

							else { // neuer Termin
								if (isset($t_id) AND $termin[wiedervorlage] == '1' AND $termin[alt] == '0') {	// Kundentyp ist Wiedervorlage, in der Abfrage stehen nur Wiedervorlagen
									$rem_termin = $rem_termin . " ($telefonist: $speich) ";
									$sql = "UPDATE termin SET termin = '$termin_datum', zeit = '$d_zeit', aussendienst='$aussendienst', wiedervorlage_date = '0000-00-00', w_zeit = '00:00', rem_termin = '$rem_termin', ";
									$sql .= "produkt_alt_id = '$produkt_alt_id', aquiriert = '$aquiriert', wiedervorlage = '0', telefonist = '$telefonist', edit = '$stamp'  ";
									$sql .= "WHERE termin_id = '$t_id'";
									$query = myqueryi($db, $sql);
								
									$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
									$sql .= "einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
																
									$bemerk = " ($speich abg: $telefonist) ";
									//$wvl_alt = wvl_alt($kunden_id, $heute, $bemerk );			// alle ALTEN WVL abschließen
								
									$fehler = "Die Kundendaten wurden gespeichert -<br />Der Wiedervorlage-Termin mit der ID: $t_id wurde zum Termin";
									$norefresh = 0;									// refresh am Ende des Scriptes
								} // ende Kundentyp ist Wiedervorlage
								else {
									$rem_termin = $rem_termin . " ($telefonist: $speich) ";
									$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, rem_termin, ";
									$sql .= "produkt_alt_id, aquiriert, edit) ";
									$sql .= "VALUES ('$termin_datum', '$d_zeit', '$kunden_id', '$telefonist', '$aussendienst', '$rem_termin', ";
									$sql .= "'$produkt_alt_id', '$aquiriert', '$stamp') ";
									$query = myqueryi($db, $sql);
									$termin_id = mysqli_insert_id($db);
								
									$bemerk = " ($speich abg: $telefonist) ";
									//$wvl_alt = wvl_alt($kunden_id, $heute, $bemerk );			// alle ALTEN WVL abschließen
				
									$rem_kunde = $rem_kunde . " ($telefonist: $speich) ";
									$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$t_strasse', ";
									$sql .= "vorwahl2_id = '$vorwahl2_id', fax = '$t_fax', email = '$t_email', geboren = '$geboren', rem_kunde = '$rem_kunde', branche_id = '$branche_id', ";
									$sql .= "einzug = '$einzug',  belegt = '1', nichtkunde = '0', neukunde = '0', ne = '0000-00-00', datenschutz = '$datenschutz' ";
									$sql .= "WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
			
									$fehler = "Die Kundendaten wurden gespeichert -<br />Ein Termin mit der ID: $termin_id wurde eingefügt";
									$norefresh = 0;									// refresh am Ende des Scriptes
								}
							} // ende else neuer Termin
							} //neue Daten für termin
						} // Ende else Termin in der Zukunft
					} // ende else Termin noch frei
				}	// ende else Muster Datum stimmt
				} // ende else kein WVL-Datum eingegeben
			}	// ende else Termin eingegeben
			}	// ende else kein Vertragskunde
		} // ende else Terminbearbeitung
	//}	// Ende else kann nur Termin oder Wiedervorlage folgen
	

// Ausgabe der Meldung - nicht bei Fa. Pfaller!!
if ($host != "www.dlpfaller.de") {

?>

<!DOCTYPE html>
<html lang="db">
<head>
<title>Termin neu</title><!-- allgemein/termin_neu.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 100px; margin-right: 5px; margin-bottom: 5px; }

.ja {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #9b0013; color: #ffffff;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

.nein {
	border-top: 1px solid #203C5E; border-bottom: 1px solid #203C5E; border-left: 1px solid #203C5E; border-right: 1px solid #203C5E;
	background-color: #D8E1EC; color: #006699;
	font-family: Arial, sans-serif; font-size: 8pt; font-weight: bold;
	width:100px;line-height:140%;
}

-->

</style>
</head>
<body>
<div align = "center">
<table width="400" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
		<table width="100%" border="0" cellspacing="2" cellpadding="3" bgcolor="#eeeeee">
		  <?php echo "<tr bgcolor = \"#ff6633\"><td colspan=\"2\"><span style = \"font-weight:bold; color:black; font-size:10pt; line-height:200%;\">$fehler</span></td></tr>"; ?>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</body>

<?php

	// Verzögerung um 300 Millisekunden (usleep verlangt Mikrosekunden!)
	flush();				// Ausgabepuffer auf den Bildschirm schreiben
	//usleep(3000000);		// 3 Sekunden warten, dann Refresh
	sleep(3);
	
	}	// ende Meldung für Pfaller ausblenden
	
	// Nachladen der Termin-Anzeige-Seite im linken Frame, Benutzer und Aussendienst sind als Session-Variable gespeichert
	if ($norefresh == 0) {
		echo "<script>onload=parent['aqset_mr'].location.href='termin_aktuell.php'</script>";
		echo "<script>onload=parent['aqframe_u'].location.href='termin_check.php?kd_id=$kunden_id'</script>";
	}
	
	}	// ende else eingeloggt
} // Ende isset($_speichern)
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Termine-neu</title>
	<!-- allgemein/termin_neu.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">

<table border="1" width="800" bordercolor="#006699">
<?php
	if ($fehler) {
 		echo "<tr bgcolor=\"#F0F8FF\">";
        echo "<td colspan=\"3\">";
		echo "<span style=\"font-weight:bold; color:red;\">$fehler</span><br>";
		echo "</td></tr>";
	}
		elseif ($ausgeloggt) {
 		echo "<tr bgcolor=\"red\">";
        echo "<td colspan=\"3\">";
		echo "<div align = \"center\"><span style=\"font-weight:bold; font-size: 13pt; color:white; line-height: 150%;\">$ausgeloggt</span></div>";
		echo "</td></tr>";
	}
?>
  <!-- Tabelle Rand -->
<form name="terminvereinbarung" method="post" action="<?php $_SERVER["PHP_SELF"] ?>" style="margin: 0 0;">
	<tr cellspacing="0" cellpadding="0">
    	<td colspan="3">
	<tr bgcolor="#F0F8FF">
      <td colspan="2"><span style="font-weight:bold;"><?php echo "$aussendienst: Kd.: $kunden_id $kunde[vorname] $kunde[name] - Tel.: ($kunde[vorwahl1]) $kunde[telefon] - $kunde[plz] $kunde[ort]"; ?></span></td>
	  <?php echo "<td align = \"right\"><a href='../allgemein/termin_check.php?kd_id=$kunden_id & t_id=$t_id & check=1' TARGET= '_self'><span style=\"font-weight:bold; color:red;\">Für Termin-Übersicht bitte hier klicken</span></a></td>";?>
    </tr>

<!-- Zeile zur Darstellung Termindaten / Checkboxen / Kundendaten ------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

    <tr bgcolor="#F0F8FF">

<!-- Zelle zur Darstellung Termindaten ---------------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

		<td align = "left" valign = "top">
			<table cellpadding = "2" bgcolor = "#006699">
				<tr bgcolor="#F0F8FF">
					<td width="89">Termin</td>
      				<td width="150"><input type="text"  class="eingabe" name="d1_tag" size = "2" maxlength ="2" value = "<?php echo "$d_tag"; ?>"/>
        				.
        				<input type="text"  class="eingabe" name="d1_monat" size = "2" maxlength ="2" value = "<?php echo "$d_monat"; ?>"/>
        				.
        				<input type="text"  class="eingabe" name="d1_jahr" size = "2" maxlength ="2" value = "<?php echo "$d_jahr"; ?>"/>
      				</td>
      				<td width="56">Zeit</td>
	  				<td width="101"><select name="d1_zeit" class = "eingabe">
	<?php
							for ($i = 0; $i < count($t_zeit); $i++) { // Erzeugung der Zeilen
								if ($d_zeit === $t_zeit[$i]) {
									echo " <option selected>$t_zeit[$i]</option>";
								}
								else {
									echo " <option>$t_zeit[$i]</option>";
								}
							}
	 					?>
	 					</select>
      				</td>
    			</tr>

	    		<tr bgcolor="#F0F8FF">
      				<td width="89">WVL&nbsp;</td>
      				<td><input type="text" class = "eingabe" name="w1_tag" size = "2" maxlength ="2" value = "<?php echo "$w_tag"; ?>"/>
        				.
        				<input type="text" class = "eingabe" name="w1_monat" size = "2" maxlength ="2" value = "<?php echo "$w_monat"; ?>"/>
        				.
        				<input type="text" class = "eingabe" name="w1_jahr" size = "2" maxlength ="2" value = "<?php echo "$w_jahr"; ?>"/>
					</td>
	  				<td>Zeit</td>
	  				<td><select name="w1_zeit" class = "eingabe">
	 <?php
		   					echo "$w_zeit<br>";
							for ($i = 0; $i < count($wvl_zeit); $i++) { // Erzeugung der Zeilen
								if ($w_zeit === $wvl_zeit[$i]) {
									echo " <option selected>$wvl_zeit[$i]</option>";
								}
								else {
									echo " <option>$wvl_zeit[$i]</option>";
								}
							}
	 					?>
	 					</select>
      				</td>
	  			</tr>

				<tr bgcolor="#F0F8FF">
      				<td width="89">Ges. alt</td>
      				<td><select name="gesselect">
   <?php
	  						$sql = "SELECT ges_alt FROM ges_alt ORDER BY ges_alt ASC ";
							$query_ges_alt = myqueryi($db, $sql);
							while ($zeile = mysqli_fetch_array($query_ges_alt)) {
								if ($gesselect == $zeile[0]) {
									echo "<option selected>$zeile[0]</option>";
								}
								else {
									echo "<option>$zeile[0]</option>";
								}
							}
						?>
						</select>
      				</td>
	  	  			<td>Eingabe</td>
	  				<td><input type="text" class = "eingabe" name="gesinput" size ="15" maxlength="20" value = "<?php echo "$gesinput"; ?>"></td>
    			</tr>

				<tr bgcolor="#F0F8FF">
      				<td width="89">Ges. neu</td>
      				<td><?php echo "$termin[ges_neu]"; ?></td>
	  				<td>Tarif-Neu</td>
	  				<td><?php echo "$termin[tarif_neu]"; ?></td>
    			</tr>

   				<tr bgcolor="#F0F8FF">
      				<td width="89">Bemerkung</td>
      				<td colspan="3"><textarea cols="50" rows="6" class = "eingabe" name="rem1_termin"><?php echo "$rem_termin"; ?></textarea></td>
    			</tr>
			</table>
		</td>

<!-- Ende Zelle zur Darstellung Termindaten ----------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- Zelle zur Darstellung Checkboxen  ---------------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

		<td align = "center" valign = "top">
			<table cellpadding = "2" bgcolor = "#006699">
				 <tr bgcolor="#ffffcc">
      				<td align = "center" valign = "top">
	   					<strong>ne</strong><br />
   <?php
							if ($ne == 0) { echo "<input type=\"checkbox\"  name=\"ne_neu\">"; }
							else { echo "<input type=\"checkbox\"  name=\"ne_neu\" checked>"; }
						?>
					</td>
				</tr>

 <?php
					if ($doppler == 1) { echo "<tr bgcolor=\"red\">"; }					// Termin belegt und "2x" nicht angeklickt - Zeile in Rot
					else { echo "<tr bgcolor=\"#ffff99\">"; }
				?>
      				<td align = "center" valign = "top">
	   					<strong>2x</strong><br />
   <?php
							if ($doppelt == 0) { echo "<input type=\"checkbox\"  name=\"doppelt_neu\">"; }
							else { echo "<input type=\"checkbox\" name=\"doppelt_neu\" checked>"; }
						?>
					</td>
				</tr>

				<?php	// Kennzeichnet den Nichtkunden bzw. Nichtkundentermin (Zeile mit rotem Hintergrund)

						if ($nichtkunde == 0) {
						echo "<tr bgcolor=\"#ffff66\"><td align = \"center\" valign = \"top\"><strong>Nichtk.</strong><br /><input type=\"checkbox\" name=\"nichtkunde_neu\"></td></tr>";
					}
					else {
						echo "<tr bgcolor=\"red\"><td align = \"center\" valign = \"top\"><strong>Nichtk.</strong><br /><input type=\"checkbox\"  name=\"nichtkunde_neu\" checked></td></tr>";
					}
				?>

				 <tr bgcolor="#ffff33">
      				<td align = "center" valign = "top">
	   					<strong>verzog.</strong><br />
	 <?php
							if ($verzogen == 0) { echo "<input type=\"checkbox\"  name=\"verzogen_neu\">"; }
							else { echo "<input type=\"checkbox\"  name=\"verzogen_neu\" checked>"; }
						?>
					</td>
				</tr>

				<tr bgcolor="#ffff00">
      				<td align = "center" valign = "top">
	   					<strong>Einzug</strong><br />
	 <?php
							if ($einzug == 0) { echo "<input type=\"checkbox\"  name=\"einzug_neu\">"; }
							else { echo "<input type=\"checkbox\"  name=\"einzug_neu\" checked>"; }
						?>
					</td>
				</tr>
				<!--
				<tr bgcolor="#DDDDDD">
      				<td align = "center" valign = "top"> <span style = "font-weight:bold;">Termin<br />stornieren</span><br />
				-->
   <?php
							//if ($alt == 0) { echo "<input type=\"checkbox\" name=\"alt_neu\""; }
							//else { echo "<input type=\"checkbox\" name=\"alt_neu\" checked>"; }
						?>
			<!--
					</td>
				</tr>
			-->
			</table>
		</td>

<!-- Ende Zelle zur Darstellung Checkboxen ------------------------------------------------------------------------------------------------------------------------>
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- Zelle zur Darstellung Kundendaten ---------------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

		<td align = "left" valign = "top">
			<table cellpadding = "2" bgcolor = "#006699">

				<tr bgcolor="#F0F8FF">
      				<td width="89">Vorname</td>
      				<td width="130"><input type="text" class = "eingabe" name="kd_vorname" maxlength = "20" size = "20" value = "<?php echo "$t_vorname"; ?>"/></td>
      				<td width="89">Name</td>
      				<td><input type="text" class = "eingabe" name="kd_name" maxlength = "25" size = "20" value = "<?php echo "$t_name"; ?>"/></td>
    			</tr>

				<tr bgcolor="#F0F8FF">
      				<td width="89">Ort</td>
      				<td width="130"><input type="text" class = "eingabe" name="t_ort" maxlength = "30" size = "20" value = "<?php echo "$t_ort"; ?>"/></td>
      				<td width="89">Straße</td>
      				<td><input type="text" class = "eingabe" name="t1_strasse" maxlength = "60" size = "20" value = "<?php echo "$t_strasse"; ?>"/></td>
    			</tr>

    			<tr bgcolor="#F0F8FF">
      				<td  width="89">Ortsteil</td>
      				<td  width="130">
						<select name="ortsteil_alt" class = "eingabe">
  <?php
	  						$sql = "SELECT ortsteil FROM poo, plz, ort, ortsteil ";
							$sql .= "WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id ";
							$sql .= "AND ort.ort = '$kunde[ort]' ";
							$ortsteil = myqueryi($db, $sql);

							for ($j = 0; $j < mysqli_num_rows($ortsteil); $j++)	{		// Anzahl der Datensätze
								$zeile = mysqli_fetch_row($ortsteil);						// Schleife für Daten-Zeilen
								if 	($zeile[0] == $t_ortsteil) {
									echo "<option selected>$zeile[0]</option>";
								}
								else {
									echo "<option>$zeile[0]</option>";
								}
							}
	 					?>
	  					</select>
      				</td>
      				<td  width="89"><strong>OT neu</strong></td>
      				<td><input type="text" class = "eingabe" name="ortsteil_neu" maxlength = "25" size = "20" value = "<?php echo "$t_ortsteil_neu"; ?>"/></td>
    			</tr>

	  			<tr bgcolor="#F0F8FF">
      				<td width="89">geboren</td>
      				<td>
   <?php
							if (($g_tag == "00") AND ($g_monat == "00") AND ($g_jahr == "0000")) {
	  							echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_tag\" size = \"2\" maxlength =\"2\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_monat\" size = \"2\" maxlength =\"2\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_jahr\" size = \"2\" maxlength =\"2\">";
							}
							else {
								echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_tag\" size = \"2\" maxlength =\"2\" value = \"$g_tag\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_monat\" size = \"2\" maxlength =\"2\" value = \"$g_monat\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g1_jahr\" size = \"2\" maxlength =\"2\" value = \"$g_jahr\">";
							}
						?>
      				</td>
      				<td width="89">&nbsp;</td>
      				<td>&nbsp;</td>
   				 </tr>

    			<tr bgcolor="#F0F8FF">
      				<td width="89">Vorwahl 2</td>
      				<td width="130"><input type="text" class = "eingabe" name="t1_vorwahl2" size="8" value = "<?php echo "$t_vorwahl2"; ?>"/></td>
      				<td width="">Tel. 2</td>
      				<td><input type="text" class = "eingabe" name="t1_fax" size="12" value = "<?php echo "$t_fax"; ?>"/></td>
    			</tr>

    			<tr bgcolor="#F0F8FF">
     				<td width="89">E-Mail</td>
      				<td width="130"><input type="text" class = "eingabe" name="t1_email" size="20" maxlength = "20" value = "<?php echo "$t_email"; ?>"/></td>
      				<td width="89">Branche</td>
      				<td>
        				<select name="t1_branche" class = "eingabe">
	<?php
		   					$sql = "SELECT branche FROM branche WHERE branche_id > '1' ORDER BY branche ASC";
							$query_branche = myqueryi($db, $sql);

							echo " <option></option>";
							for ($j = 0; $j < mysqli_num_rows($query_branche); $j++)	{			// Anzahl der Datensätze
								$zeile = mysqli_fetch_row($query_branche);						// Schleife für Daten-Zeilen
								if ($zeile[0] == $t_branche) {
									echo " <option selected>$zeile[0]</option>";
								}
								else {
									echo " <option>$zeile[0]</option>";
								}
							}
	 					?>
    					</select>
     				</td>
    			</tr>

   				<tr bgcolor="#F0F8FF">
					<td width="89">Bemerkung</td>
					<td ><textarea cols="48" rows="4" class = "eingabe" name="rem_kunde_neu"><?php echo "$rem_kunde"; ?></textarea></td>
					<td ><strong>Datenschutz-<br>erklärung</strong></td>
					<td>
						<?php
						if ($datenschutz == 0) { echo "<input type=\"checkbox\" name=\"datenschutz_neu\">"; }
						else { echo "<input type=\"checkbox\" name=\"datenschutz_neu\" checked>"; }
						?>
						erhalten
					</td>
				</tr>
			</table>
		</td>

<!-- Ende Zelle zur Darstellung Kundendaten ----------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->


	</tr>

<!-- Ende Zeile zur Darstellung Termindaten / Checkboxen/ Kundendaten -------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->


	<tr bgcolor="#F0F8FF">
      <td colspan ="3" align = "center">
        <input type="submit" name="t_speichern" value = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Speichern&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" class="submitt"/>
      </td>
    </tr>
  </table>
</form>
<!-- Ende Tabelle Rand -->

</div>
</body>
</html>

<?php  $close = mysqli_close($db); ?>