<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

$ges_neu_id = $_GET["id"];
$ges_neu_id = quote_smart($ges_neu_id);
$name = $_GET["name"];
$name = quote_smart($name);

if (isset($speichern)) {																		// Speichern-Button wurde gedrückt
	if (empty($tarif_neu)) {
		$fehler = "Fehler: Der Tarifname kann nicht leer sein!";
	}
	
	else { 	// neuer Tarif eingegeben -----------------------------------------------------------------------------------------------------------
	
		$tarif_neu = quote_smart($tarif_neu);
		
		$sql  = "SELECT produkt_neu_id FROM produkt_neu, tarif_neu, ges_neu ";
		$sql .= "WHERE produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
		$sql .= "AND ges_neu.ges_neu_id = '$ges_neu_id' AND tarif_neu.tarif_neu = '$tarif_neu'";
		$abfrage = myqueryi($db, $sql);
		
		if (mysqli_num_rows($abfrage) != 0) {
			$fehler = "Fehler: Diesen Tarif gibt es schon für diese Gesellschaft!";
		}
		else {
		
			$sql1 = "SELECT tarif_neu_id FROM tarif_neu WHERE tarif_neu = '$tarif_neu'";
			$sql2 = "INSERT INTO tarif_neu (tarif_neu) VALUES ('$tarif_neu')";
			$tarif_neu_id = id_ermitteln($db, $sql1, $sql2);
			
			$sql = "INSERT INTO produkt_neu (ges_neu_id, tarif_neu_id) VALUES ('$ges_neu_id', '$tarif_neu_id')";
			$produkt_id = myqueryi($db, $sql);
			
			
			$sql = "SELECT ges_alt_id FROM ges_alt WHERE ges_alt = '$name'";
			$abfrage = myqueryi($db, $sql);
			$ges_alt_id = mysqli_fetch_array($abfrage);
			
			$sql1 = "SELECT tarif_alt_id FROM tarif_alt WHERE tarif_alt = '$tarif_neu'";
			$sql2 = "INSERT INTO tarif_alt (tarif_alt) VALUES ('$tarif_neu')";
			$tarif_alt_id = id_ermitteln($db, $sql1, $sql2);

			$sql = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id[0]', '$tarif_alt_id')";
			$produkt_id = myqueryi($db, $sql);
			
			$fehler = "Der Tarif $tarif_neu wurde hinzugefügt";
		
		}
	}
}	// Ende IF ISSET speichern

if (isset($update)) {

	$tarif = $_POST["tarif"];
	$tarif = quote_smart($tarif);
	$prod_id = $_POST["prod_id"];
	$prod_id = quote_smart($prod_id);
	//$ges_neu_id = $_POST["ges_id"];
	//$ges_neu_id = quote_smart($ges_neu_id);
	$name = $_POST["name"];
	$name = quote_smart($name);
	
	if (empty($tarif)) {
		$fehler = "Fehler: Der Name des Tarifes darf nicht leer sein!";
	}
	else {
		
		$sql = "SELECT tarif_neu_id FROM produkt_neu WHERE produkt_neu_id = '$prod_id'";
		$abfrage = myqueryi($db, $sql);
		$ergebnis = mysqli_fetch_array($abfrage);
		$tarif_neu_id = $ergebnis[0];
		
		$sql = "UPDATE tarif_neu SET tarif_neu = '$tarif' WHERE tarif_neu_id = '$tarif_neu_id'";
		$abfrage = myqueryi($db, $sql);
		
		$sql = "UPDATE tarif_alt SET tarif_alt = '$tarif' WHERE tarif_alt_id = '$tarif_neu_id'";
		$abfrage = myqueryi($db, $sql);
	}
} // Ende IF UPDATE


//---------------------------------------------------------------------------------------------------------------------------------------

// Start Script-Ablauf vor dem Drücken des Speichern-Buttons
	$sql  = "SELECT produkt_neu_id AS ID, tarif_neu AS Tarif ";
	$sql .= "FROM produkt_neu, tarif_neu, ges_neu ";
	$sql .= "WHERE produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
	$sql .= "AND ges_neu.ges_neu = '$name' AND tarif_neu.tarif_neu_id > '1'";
	
	$ergebnis = myqueryi($db, $sql);
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Tarife</title>
	<!-- allgemein/tarif_neu.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<table width="600" border="0" cellpadding="4" cellspacing="4">
<tr>
<td><br><br><h2 class="Stil1">Tarife bearbeiten</h2><td>
</tr>
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">
<!-- Start Formular für neue Gesellschaft ++++++++++++++++++++++++++++++++++ // -->
<form name="tarif_neu" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
<table id="formular" cellspacing="4">
	<tr>
	<td><span style="font-weight:bold;">gewählte Gesellschaft: <?php echo "$name"; ?></span></td>
	<td>&nbsp;&nbsp; -->&nbsp;neuer Tarif:</td>
	<td><input type="text" name="tarif_neu" size="15" maxlength="25"></td>
    <td><input type="submit" name="speichern" value="Speichern" class="submitt"></td
	</tr>  
</table>      
</form>
<!-- Ende Formular für neue Gesellschaft ++++++++++++++++++++++++++++++++++ // -->
</td>
</tr>
<?php

if (mysqli_num_rows($ergebnis) !=0) {

	echo "<tr><td valign = \"top\">";
	echo "<table id=\"ausgabe\" cellspacing=\"4\" width = \"100%\">";

	//Ausgabe der bereits vorhandenen Tarife +++++++++++++++++++++++++++++++++++++++++++++++
	echo "<tr><td colspan = \"4\">Bereits gespeicherte Tarife:</td></tr>";

	$z=0;  //zähler der datensätze für bg_colour der zeilen
	$bg1 = "#eeeeee"; //die beiden hintergrundfarben
	$bg2 = "#dddddd";

	for ($j = 0; $j < mysqli_num_rows($ergebnis); $j++)				// Anzahl der Datensätze
	{
		$zeile = mysqli_fetch_row($ergebnis);						// Schleife für Daten-Zeilen
			
			$bg=($z++ % 2) ? $bg1 : $bg2;
			
			echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
			
			echo "<td width = \"20\">" . $zeile[0] . "</td>";
			echo "<td >" . $name . "</td>";
			echo "<td >" . $zeile[1] . "</td>";
			echo "<td align=\"center\"> <a href=\"tarif_update.php?prod_id=$zeile[0]&tarif=$zeile[1]&name=$name\">BEARBEITEN</a></td>"; //Link zum Tarif-Bearbeiten-Script
			echo "</tr>";
    }
	if ($fehler) {
	echo "<tr><td colspan = \"3\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold;\">$fehler</span></td></tr>";
}
	echo "<tr><td colspan = \"4\" align=\"center\"><a href=\"ges_registration.php\" target = \"_self\"><strong><br>Zurück zur Gesellschaft</strong></a></td></tr>";
	echo "</table>";
// Ende Ausgabe vorhandene Gesellschaft +++++++++++++++++++++++++++++++++++++++++++++++
?>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>
<?php
}
else {
?>
</td></tr>
<tr><td colspan = "4" align="center"><a href="ges_registration.php" target = "_self"><strong><br>Zurück zur Gesellschaft</strong></a></td></tr>
</table>
</td></tr>
</table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>
<?php
}
mysqli_close($db);
?>