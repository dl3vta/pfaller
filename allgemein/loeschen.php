<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

//  Feststellen, ob Benutzer überhaupt löschen darf ---------------------------------------------------------------------------

$gruppe = $_SESSION['benutzer_gruppen'];						// Benutzer Gruppe, kommt als Session-Variable aus cc4pa.php

if ($gruppe != "Administrator") {
	echo "<script>location.href='../forbidden.php'</script>";	// kein Administrator - Zugriff nicht erlaubt
}

//-----------------------------------------------------------------------------------------------------------------------------


// POST- und GET-Variablen ---------------------------------------------------------------------------------------------------- 

$ja = $_POST["ja"];									// Ja-Button, Daten sollen gelöscht werden
$nein = $_POST["nein"];								// Nein-Button, Löschen wird abgebrochen

if (isset($_POST["kunden_id"])) {						// Variante Termin löschen: kunden-id kommt aus dem hidden-Feld des Formulars
	$kunden_id = $_POST["kunden_id"];
}

if (isset($_POST["kd"])) {								// Variante Kunde löschen: kunden-id kommt aus dem hidden-Feld des Formulars
	$kunden_id = $_POST["kd"];
	$kd = $_POST["kd"];
}

if (isset($_GET["kd_id"])) {							// Variante Kunde löschen: kunden-id kommt von such_check.php

	$kunden_id = $_GET["kd_id"];
}

if (isset($_GET["t_id"])) {							// Variante Termin löschen: termin-id kommt von such_check.php
	
	$termin_id = $_GET["t_id"];
}

$t = $_POST["t"];										// Variante Termin löschen: Termin_id kommt aus dem hidden-Feld des Formulars

// ---------------------------------------------------------------------------------------------------------------------------------


// Kunde soll komplett mit allen Terminen gelöscht werden!!!! ---------------------------------------------------------------------------------------

if (isset($kunden_id)) {

	$sql = "SELECT vorname, name, plz, ort ";
	$sql .= "FROM kunden, vorname, name, poo, plz, ort, ortsteil ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id'  ";
	$sql .= "And kunden.vorname_id = vorname.vorname_id  AND kunden.name_id = name.name_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$query = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($query);
	
	$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' ";			// alle Termine auslesen, Anzahl ermitteln
	$query = myqueryi($db, $sql);
	$anzahl = mysqli_num_rows($query);

}
// ---------------------------------------------------------------------------------------------------------------------------------------------------


// nur ein bestimmter Termin soll gelöscht werden!!!! -------------------------------------------------------------------------------------------------

if (isset($termin_id)) {
	$sql = "SELECT kd_id, vorname, name, plz, ort ";
	$sql .= "FROM termin, kunden, vorname, name, poo, plz, ort, ortsteil ";
	$sql .= "WHERE termin.kd_id = kunden.kunden_id AND termin.termin_id = '$termin_id' ";
	$sql .= "AND kunden.vorname_id = vorname.vorname_id  AND kunden.name_id = name.name_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$query = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($query);
	$kd_id = $kunde[0];														// Ermittlung der Kunden-ID aus der Termin-ID
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------


// Löschen ist abgebrochen, Rücksprung zu such_check.php ----------------------------------------------------------------------------------------------

if (isset($nein)) {																// Nein-Button gedrückt
	echo "<script>onload=parent['suche_oben'].location.href='such_check.php?kd_id=$kunden_id'</script>";
}
// ---------------------------------------------------------------------------------------------------------------------------------------------------


// Daten sollen gelöscht werden -----------------------------------------------------------------------------------------------------------------------

elseif (isset($ja)) {															// Ja-Button gedrückt
	// Kunde soll mit allen Terminen gelöscht werden!!!! ----------------------------------------------------
	
	if (isset($kd)) {
		$kunden_id = $kd;
		
		$sql = "SELECT vorname, name ";											// Noch mal ne Abfrage, um eine personalisierte Meldung ausgeben zu können
		$sql .= "FROM kunden, vorname, name ";
		$sql .= "WHERE kunden.kunden_id = '$kunden_id'  ";
		$sql .= "And kunden.vorname_id = vorname.vorname_id  AND kunden.name_id = name.name_id ";
		$query = myqueryi($db, $sql);
		$kunde = mysqli_fetch_array($query);
	
		$sql = "DELETE FROM termin WHERE kd_id = '$kunden_id' ";				// alle Termine für Kunden löschen
		$query = myqueryi($db, $sql);
		$del_termine = mysqli_affected_rows($db);									// Anzahl der gelöschten Termine, bei Misserfolg = -1
		
		$sql = "DELETE FROM kunden WHERE kunden_id = '$kunden_id' LIMIT 1";		// Kunden selbst löschen, zur Sicherheit nur ein Datensatz
		$query = myqueryi($db, $sql);
		$del_kunden = mysqli_affected_rows($db);									// Anzahl der gelöschten Kunden (sollte immer 1 sein!!1), bei Misserfolg = -1;
		
		// Ausgabe einer Meldung und Rücksprung zur suche.php, da der Kunde und alle seine Termine ja nicht mehr existieren
		
		?>
		
		<!DOCTYPE html>
		<html lang="de">
		<head>
		<title>Kunde gelöscht</title>
			<!-- allgemein/loeschen.php -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
		<!--
		body {
		margin-left: 5px;
		margin-top: 100px;				
		margin-right: 5px;
		margin-bottom: 5px;
		}
		-->
		</style>
		</head>
		<body>
		<div align = "center">								
		
		<table width="500" border="0" cellpadding="0" cellspacing="3">
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#eeeeee">
						<?php	if ($del_kunden == -1) {					// Löschen Kunde nicht erfolgreich
								echo "<tr bgcolor = \"#ff6633\">";
									echo "<td align = \"left\">";
									echo "<span style = \"font-weight:bold; color:maroon; font-size:10pt; line-height:200%;\">Der Kunde $kunde[vorname]&nbsp;$kunde[name] konnte nicht gelöscht werden!</span>";
									echo "</td>";
								echo "</tr>";
							}
							else {										// Kunde wurde gelöscht
							echo "<tr bgcolor = \"#66ff99\">";
								echo "<td align = \"left\">";
								echo "<span style = \"font-weight:bold; color:black; font-size:10pt; line-height:200%;\">Der Kunde $kunde[vorname]&nbsp;$kunde[name] wurde gelöscht!</span>";
								echo "</td>";
								echo "</tr>";
							}
							if ($del_termine == -1) {					// Termine konnten nicht gelöscht werden
							echo "<tr bgcolor = \"#ff6633\">";
								echo "<td align = \"left\">";
								echo "<span style = \"font-weight:bold; color:maroon; font-size:10pt; line-height:200%;\">Es wurden keine Termine für den Kunden $kunde[vorname]&nbsp;$kunde[name] gelöscht!</span>";
								echo "</td>";
								echo "</tr>";
							}
							else {										// Termine gelöscht
							echo "<tr bgcolor = \"#66ff99\">";
								echo "<td align = \"left\">";
								echo "<span style = \"font-weight:bold; color:black; font-size:10pt; line-height:200%;\">$del_termine Termine des Kunden $kunde[vorname]&nbsp;$kunde[name] wurden gelöscht!</span>";
								echo "</td>";
								echo "</tr>";
							}
						?>
						
						<!-- Rücksprung zu suche.php (nicht such_check.php), weil der Kunde und alle Termine ja gelöscht wurden -->
						<tr bgcolor="#eeeeee">
							<td valign = "middle" align = "center"><a href = "suche.php" target = "_self"><span style="font-size:10pt; font-weight:bold; line-height:200%; color:red;">-> zur&uuml;ck zur Suche</span></a></td>
						</tr>
						
					</table>
				</td>
			</tr>
		</table>
		</div>
		</body>
		</html>
 <?php
	}
	// ende Kunde soll gelöscht werden ----------------------------------------------------------------------------------------------------------------------------------------------
	
	
	// ein spezieller Termin soll gelöscht werden ------------------------------------------------------------------------------------------------------------------------------------
	
	elseif (isset($t)) {
	
		$sql = "SELECT kd_id, vorname, name, plz, ort ";
		$sql .= "FROM termin, kunden, vorname, name, poo, plz, ort, ortsteil ";
		$sql .= "WHERE termin.kd_id = kunden.kunden_id AND termin.termin_id = '$t' ";
		$sql .= "AND kunden.vorname_id = vorname.vorname_id  AND kunden.name_id = name.name_id ";
		$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
		$query = myqueryi($db, $sql);
		$kunde = mysqli_fetch_array($query);
		$kunden_id = $kunde[0];												// noch mal ne Abfrage, um Daten für die Ausgabe zu erhalten
	
		$sql = "DELETE FROM termin WHERE termin_id = '$t' LIMIT 1 ";		// Termin löschen, zur Sicherheit LIMIT = 1
		$query = myqueryi($db, $sql);
		$del_termine = mysqli_affected_rows($db);								// Anzahl der gelöschten Termine, bei Misserfolg = -1
		
		// Ausgabe einer Meldung und Rücksprung zur such_check.php, Kontrolle der Löschung und der anderen Termine
		
		?>
		
		<!DOCTYPE html>
		<html lang="de">
		<head>
		<title>Termin gelöscht</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
		<!--
		body {
		margin-left: 5px;
		margin-top: 100px;
		margin-right: 5px;
		margin-bottom: 5px;
		}
		-->
		</style>
		</head>
		<body>
		<div align = "center">
		
		<table width="500" border="0" cellpadding="0" cellspacing="3">
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#eeeeee">
						<?php	if ($del_termine == -1) {					// Termine konnten nicht gelöscht werden
							echo "<tr bgcolor = \"#ff6633\">";
							echo "<td align = \"left\">";
							echo "<span style = \"font-weight:bold; color:maroon; font-size:10pt; line-height:200%;\">Der Termin mit der ID: $t konnte nicht gelöscht werden!</span>";
							echo "</td>";
							echo "</tr>";
						}
						else {										// Termine gelöscht
						echo "<tr bgcolor = \"#66ff99\">";
							echo "<td align = \"left\">";
							echo "<span style = \"font-weight:bold; color:maroon; font-size:10pt; line-height:200%;\">Der Termin mit der ID: $t des Kunden $kunde[vorname]&nbsp;$kunde[name] wurde gelöscht!</span>";
							echo "</td>";
							echo "</tr>";
						}
					?>
						
					<!-- Rücksprung zu such_check.php mit Rückgabe der Kunden-ID zum Refreshen von such_check.php (Kontrolle löschen) -->
					<tr bgcolor="#eeeeee">
						<td valign = "middle" align = "center"><?php echo "<a href = \"such_check.php?kd_id=$kunden_id\" target = \"_self\"><span style=\"font-size:10pt; font-weight:bold; line-height:200%; color:red;\">Zurück zur Termin-Übersicht</span></a>"; ?></td>
					</tr>			
				</table>
			</td>
		</tr>
	</table>
</div>
</body>
</html>
 <?php
	} // ende termin soll gelöscht werden -----------------------------------------------------------------------------------------------------------------------------------------------

} // ende löschen ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


else {	// kein Button gedrückt - Script startet erstmals
?>

<!-- Hier f�ngt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Kd-Termine</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 100px;
	margin-right: 5px;
	margin-bottom: 5px;
}
.ja {
	border-top: 1px solid #203C5E;
	border-bottom: 1px solid #203C5E;
	border-left: 1px solid #203C5E;
	border-right: 1px solid #203C5E;
	background-color: #9b0013;
	color: #ffffff;
	font-family: Arial, sans-serif;
	font-size: 10pt;
	font-weight: bold;
	width:100px;
	line-height:140%;
	
}
.nein {
	border-top: 1px solid #203C5E;
	border-bottom: 1px solid #203C5E;
	border-left: 1px solid #203C5E;
	border-right: 1px solid #203C5E;
	background-color: #D8E1EC;
	color: #006699;
	font-family: Arial, sans-serif;
	font-size: 10pt;
	font-weight: bold;
	width:100px;
	line-height:140%;
}

-->
</style>
</head>
<body>
<div align = "center">
<table width="400" border="0" cellpadding="0" cellspacing="3">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="3" cellspacing="2" bgcolor="#eeeeee">
			
				<?php	// unterschiedliche Ausgaben für Löschung von Kunde oder Terrmin
					if (isset($kunden_id)) {
					
						echo "<form name=\"loeschen\" method=\"post\" action=\"$_SERVER[PHP_SELF]\">";
						echo "<tr>";
							echo "<td width=\"140px\" align = \"right\" nowrap>Soll der Kunde</td>";
							echo "<td align=\"left\"><span style=\"font-weight:bold; font-size:10pt; line-height:150%;\">$kunde[vorname]&nbsp;$kunde[name]</span></td>";
						echo "</tr>";
						echo "<tr>";
							echo "<td width=\"140px\" align = \"right\" nowrap>aus</td>";
							echo "<td align=\"left\"><span style=\"font-weight:bold; font-size:10pt; line-height:150%;\">$kunde[plz]&nbsp;$kunde[ort]</span></td>";
						echo "</tr>";
						echo "<tr>";
							echo "<td width=\"140px\" align = \"right\" nowrap>mit seinen</td>";
							echo "<td align=\"left\"><span style=\"font-weight:bold; font-size:10pt; line-height:150%;\">$anzahl</span> Terminen</td>";
						echo "</tr>";
						echo "<tr>";
							echo "<td colspan = \"2\" align = \"center\"><span style=\"font-weight:bold; font-size:10pt; color:red; line-height:150%;\">unwiederbringlich gelöscht werden?</span></td>";
						echo "</tr>";
						echo "<tr>";
							echo "<td colspan = \"2\" align = \"center\">
									<input type=\"hidden\" name=\"kd\" value=\"$kunden_id\">
									<input type=\"submit\" name=\"ja\" value=\"JA!\" class = \"ja\">&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"nein\" value=\"NEIN!\" class=\"nein\">
								<td>";
						echo "</tr>";
						echo "</form>";
					}
						
					elseif (isset($termin_id)) {
					
						echo "<form name=\"loeschen\" method=\"post\" action=\"$_SERVER[PHP_SELF]\">";
						echo "<tr>";
							echo "<td width=\"140px\" align = \"right\" nowrap>Wollen Sie den Termin</td>";
							echo "<td align=\"left\"><span style=\"font-weight:bold; font-size:10pt; line-height:150%;\">$termin_id</span></td>";
						echo "</tr>";
						echo "<tr>";
							echo "<td width=\"140px\" align = \"right\" nowrap>des Kunden</td>";
							echo "<td align=\"left\"><span style=\"font-weight:bold; font-size:10pt; line-height:150%;\">$kunde[vorname]&nbsp;$kunde[name]</span></td>";
						echo "</tr>";
						echo "<tr>";
							echo "<td width=\"140px\" align = \"right\" nowrap>aus</td>";
							echo "<td align=\"left\"><span style=\"font-weight:bold; font-size:10pt; line-height:150%;\">$kunde[plz]&nbsp;$kunde[ort]</td>";
						echo "</tr>";
						echo "<tr>";
							echo "<td colspan = \"2\" align = \"center\"><span style=\"font-weight:bold; font-size:10pt; color:red; line-height:150%;\">wirklich unwiederbringlich löschen?</span></td>";
						echo "</tr>";
						echo "<tr>";
							echo "<td colspan = \"2\" align = \"center\">
									<input type=\"hidden\" name=\"t\" value=\"$termin_id\">
									<input type=\"hidden\" name=\"kunden_id\" value=\"$kd_id\">
									<input type=\"submit\" name=\"ja\" value=\"JA!\" class = \"ja\">&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"nein\" value=\"NEIN!\" class=\"nein\">
								<td>";
						echo "</tr>";
						echo "</form>";
					}
				?>
			</table>
		</td>
	</tr>
</table>
</div>
</body>
</html>

<?php
}	// ende else Script startet erstmalig, kein Button gedröckt
?>