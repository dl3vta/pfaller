<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

function variablen_freigeben() {						// löscht die in diesem Script erzeugten Wahl-Variablen
	unset ($_SESSION['plz']);
	unset ($_SESSION['plz_id']);
	unset ($_SESSION['ort']);
	unset ($_SESSION['ort_id']);
	unset ($_SESSION['kundentyp']);
	unset ($_SESSION['strasse']);
	unset ($_SESSION['wvlheute']);
	unset ($_SESSION['wvl_tag']);
	unset ($_SESSION['wvl_monat']);
	unset ($_SESSION['wvl_jahr']);
	unset ($_SESSION['switch']);
	//session_unregister("switch");
	unset ($_SESSION['plz']);
}

$switch = "1";											// 2 Variablen zum Umschalten der Sortierung straße

if (!empty($_SESSION['wvl_tag'])) { $wvl_tag = $_SESSION['wvl_tag']; }
if (!empty($_SESSION['wvl_monat'])) { $wvl_monat = $_SESSION['wvl_monat']; }
if (!empty($_SESSION['wvl_jahr'])) { $wvl_jahr = $_SESSION['wvl_jahr']; }
if (!empty($_SESSION['wvlheute'])) { $wvlheute = "1"; }
if (!empty($_SESSION['kundentyp'])) { $kundentyp = $_SESSION['kundentyp']; }
if (!empty($_SESSION['plz'])) { $plz = $_SESSION['plz']; }
if (!empty($_SESSION['ort'])) { $ort = $_SESSION['ort']; }
if (!empty($_SESSION['strasse'])) { $strasse = $_SESSION['strasse']; }
if (!empty($_SESSION['switch'])) { $switch = $_SESSION['switch']; }

$switch = abs($switch - $sort);

$host = $_SERVER['HTTP_HOST'];

$telefonist = $_SESSION['benutzer_kurz'];
$aussendienst = $_SESSION['aussendienst'];

$musterdatum = "/^\d{2}$/";														// Test Datum, genau 2 Ziffern
$musterplz = "/^\d{5}$/";														// Test PLZ, genau 5 Ziffern

$heute = date("Y-m-d");									
$nofault = 0;																	// steuert die Ausgabe am ende des scriptes
$fehler = "Keine Eingangsdaten vorhanden";										// Ausgabe der (Fehler-)Meldungen			

if (isset($_POST['wvlheute'])) $wvlheute = ($_POST['wvlheute']);				// Wiedervorlagen für heute
if (isset($_POST['wvl_tag'])) $wvl_tag = quote_smart($_POST['wvl_tag']);		// Wiedervorlagen: Tag
if (isset($_POST['wvl_monat'])) $wvl_monat = quote_smart($_POST['wvl_monat']);	// Wiedervorlagen: Monat
if (isset($_POST['wvl_jahr'])) $wvl_jahr = quote_smart($_POST['wvl_jahr']);		// Wiedervorlagen: Jahr

if (isset($_POST['plz'])) $plz = quote_smart($_POST['plz']);					// PLZ aus Formular
if (isset($_POST['ort'])) $ort = quote_smart($_POST['ort']);					// Ort aus Formular
if (isset($_POST['strasse'])) $strasse = quote_smart($_POST['strasse']);		// Straße aus Formular
if (isset($_POST['kundentyp'])) $kundentyp = quote_smart($_POST['kundentyp']);	// Kundentyp aus Formular

if (isset($_POST['speichern'])) $speichern = $_POST['speichern'];				// Speichern-Button
if (isset($_POST['suchen'])) $suchen = $_POST['suchen'];						// Suchen-Button

$typ = array("Neukunde", "Wiedervorlage", "ne", "Vertragskunde", "Nichtkunde", "alle", "Handy");			// Array für Kundentyp

if (isset($_POST['korrektur'])) {												// Korrektur-Button gedrückt
	variablen_freigeben();
	echo "<script>location.href='telefon_liste.php'</script>";
	exit();
 }
 
if (isset($_POST['refresh'])) {	
	//echo "<script>location.href='telefon_liste.php'</script>";
	//exit();
} // ende refresh

// Speichern gedrückt - Variablen aus Formular werden verarbeitet ----------------------------------------------- //
// ------------------------------------------------------------------------------------------------------------- //

if (isset($_POST['speichern']) OR isset($_POST['refresh']) OR isset($_POST['suchen']) OR (isset($wvlheute)) OR (!empty($wvl_tag)) OR (!empty($ort)) ) {

// Checkbox "heute" ist angeklickt ------------------------------------------------------------------------------//
// --------------------------------------------------------------------------------------------------------------//

	if (isset($wvlheute)) {		// WVL-heute angeklickt
		$wvlheute = $heute;
		$heutewvl = substr($wvlheute,8,2) . "." . substr($wvlheute,5,2) . "." . substr($wvlheute,0,4);

		if (!empty($plz)) {					// heute + PLZ gesetzt

			if(!preg_match($musterplz, $plz)) {							// Muster PLZ passt nicht
				$fehler = "Fehler: Keine gultige PLZ!";
				$nofault = "1";
			} // Ende IF Muster PLZ passt nicht
					
			else { // Muster PLZ passt

			$sql = "SELECT plz_id FROM plz WHERE plz = $plz";


			//$sql = "SELECT plz_id FROM plz WHERE plz = :plz";
			//$statement = $pdo->prepare($sql);
			//$statement->execute(array(':plz' => $plz));
			//$statement->execute();


				
			//$sql = "SELECT plz_id FROM plz WHERE plz = :plz";
			//	$sql = "SELECT plz_id FROM plz WHERE plz = :plz";
		//	$statement = $pdo->prepare($sql);
			//$statement->execute(array(':plz' => $plz));
				//$statement->execute();

			//$daten = $statement->fetch(PDO::FETCH_NUM);

			//if(!$statement->execute()) {
			//	echo "SQL Error <br />";
			//	echo $statement->queryString."<br />";
			//	print_r($statement->errorInfo());
			//}

			$ergebnis = myqueryi($db, $sql);						// PLZ wird mit Datenbank vergleichen
			$daten = mysqli_fetch_array($ergebnis, MYSQLI_NUM);
					
			if (!$daten) {											// PLZ ist unbekannt
				$fehler = "Fehler: Unbekannte PLZ!";
				$nofault = "1";
			} 														// Ende IF PLZ ist unbekannt
					
			else {													// PLZ in DB
				if (empty($ort) ) {									// kein ort eingegeben oder bekannt
						
					if ((isset($_POST['speichern']) OR !empty($plz)) and !(isset($_POST['suchen']))) {		//Anzeige Daten für heute und ganzes PLZ-Gebiet
							
						$sql1 = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
						$sql1 .= " vorwahl1 AS Vorw, telefon AS Telefon, plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, strasse AS Strasse, w_zeit AS Zeit ";
						$sql1 .= " FROM kunden, vorname, name, termin, vorwahl1, plz, ort, poo, ortsteil ";
						$sql1 .= " WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id ";
						$sql1 .= " AND termin.kd_id = kunden.kunden_id AND kunden.verzogen = '0'  ";
						$sql1 .= " AND termin.wiedervorlage_date = :wvlheute AND termin.wiedervorlage = '1' AND termin.alt = '0' ";
						$sql1 .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
						$sql1 .= " AND plz.plz = :plz ";
						
						if (isset($strasse) AND ($switch == 1)) {					// Straße eingegeben
							//$sql1 .= " AND strasse LIKE :strasse ORDER BY ort ASC, strasse ASC ";
							$sql1 .= " AND strasse LIKE '$strasse%' ORDER BY ort ASC, strasse ASC ";

						}	
						else { // Sortierung absteigend
							//$sql1 .= " AND strasse LIKE :strasse ORDER BY ort ASC, strasse DESC ";
							$sql1 .= " AND strasse LIKE '$strasse%' ORDER BY ort ASC, strasse DESC ";
						}

						//$strasse1 = $strasse . "%";

						//$statement = $pdo->prepare($sql1);
						//$statement->execute(array(':wvlheute' => '$wvlheute', ':plz' => $plz, ':strasse' => $strasse1));

						//$anzahl = $statement->rowCount();

						//if(!$statement->execute()) {
						//	echo "SQL Error <br />";
						//	echo $statement->queryString."<br />";
						//	echo $statement->errorInfo()[2];
						//}
						
						$ergebnis = myqueryi($db, $sql1);
						$anzahl = mysqli_num_rows($ergebnis);
						$fehler = "Anzahl der Datens&auml;tze:&nbsp;" . $anzahl;
								
						//Sessionvariablen
						$_SESSION['wvlheute'] = $wvlheute;
						$_SESSION['plz'] = $plz;
		
						$speichern = 1;
					}
						
					elseif (isset($_POST['suchen'])) {							// suchen button gedrückt
						
						$sql = "Select DISTINCT ort from `poo`, `ort`, `plz` ";
					/*	$sql .=" where poo.plz_id=plz.plz_id and plz.plz=:plz and poo.ort_id=ort.ort_id ";
						$statement = $pdo->prepare($sql);
						$statement -> execute(array(':plz' => $plz));
						$query_ort = $statement->fetch();

						if(!$statement->execute()) {
							echo "SQL Error <br />";
							echo $statement->queryString."<br />";
							echo $statement->errorInfo()[2];
						}*/

						$query_ort = myqueryi($db, $sql);
					}	// ende suchen button gedrückt
				}
						
				else {												// Ort eingeben
					if (isset($_POST['suchen'])) {							// suchen button gedrückt
						$sql = "Select DISTINCT ort from `poo`, `ort`, `plz` ";
						$sql .=" where poo.plz_id=plz.plz_id and plz.plz='$plz' and poo.ort_id=ort.ort_id ";
						$query_ort = myqueryi($db, $sql);
									
					}	// ende suchen button gedrückt
							
					else {
						
						$sql1 = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
						$sql1 .= " vorwahl1 AS Vorw, telefon AS Telefon, plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, strasse AS Strasse, w_zeit AS Zeit ";
						$sql1 .= " FROM kunden, vorname, name, termin, vorwahl1, plz, ort, poo, ortsteil ";
						$sql1 .= " WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id ";
						$sql1 .= " AND termin.kd_id = kunden.kunden_id AND kunden.verzogen = '0' ";
						$sql1 .= " AND termin.wiedervorlage_date = '$wvlheute' AND termin.wiedervorlage = '1' AND termin.alt = '0' ";
						$sql1 .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
						$sql1 .= " AND ort.ort = '$ort' AND plz.plz = '$plz' ";
							
						if (isset($strasse) AND ($switch == 1)) {					// Straße eingegeben
							$sql1 .= " AND strasse LIKE '$strasse%' ORDER BY ort ASC, strasse ASC ";
						}	
						else { // Sortierung absteigend
							$sql1 .= " AND strasse LIKE '$strasse%' ORDER BY ort ASC, strasse DESC ";
						}
								
						$ergebnis = myqueryi($db, $sql1);
						$anzahl = mysqli_num_rows($ergebnis);
						$fehler = "Anzahl der Datensätze:&nbsp;" . $anzahl;
							
						//Sessionvariablen
						$_SESSION['wvlheute'] = $wvlheute;
						$_SESSION['plz'] = $plz;
						$_SESSION['ort'] = $ort;
						$_SESSION['strasse'] = $strasse;
						$_SESSION['switch'] = $switch;
						
						$speichern = 1;
					}
						
				} // ende Ort eingegeben
			}	// ende PLZ in DB
		}	// ende PLZ-muster passt
	} // ende heute + PLZ gesetzt
		
	else {								// nur heute gesetzt - alle PLZs auswerten

		$sql1 = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
		$sql1 .= " vorwahl1 AS Vorw, telefon AS Telefon, plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, strasse AS Strasse, w_zeit AS Zeit ";
		$sql1 .= " FROM kunden, vorname, name, termin, vorwahl1, plz, ort, poo, ortsteil ";
		$sql1 .= " WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id ";
		$sql1 .= " AND termin.kd_id = kunden.kunden_id AND kunden.verzogen = '0' ";
		$sql1 .= " AND termin.wiedervorlage_date = '$wvlheute' AND termin.wiedervorlage = '1' AND termin.alt = '0' ";
		$sql1 .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
		$sql1 .= " ORDER BY PLZ ASC, ort ASC, strasse ASC ";	
		$ergebnis = myqueryi($db, $sql1);
		$anzahl = mysqli_num_rows($ergebnis);
		$fehler = "Anzahl der Datensätze:&nbsp;" . $anzahl;
			
		//Sessionvariablen	
		$_SESSION['wvlheute'] = $wvlheute;
			
		$speichern = 1;
			
	}	// ende nur heute gesetzt
	
} // ende if wvlheute gesetzt
	
// --------------------------------------------------------------------------------------------------------//	
// Ende Checkbox "heute" angeklickt -----------------------------------------------------------------------//
	
	
	
// --- es ist ein Wiedervorlage-Datum eingegeben worden ---------------------------------------
// --------------------------------------------------------------------------------------------

	elseif (!empty($wvl_tag) OR !empty($wvl_monat) OR !empty($wvl_jahr)) {

		if (empty($wvl_tag) OR empty($wvl_monat) OR empty($wvl_jahr)) {		// Termin unvollständig
			$fehler = "Termindaten für Wiedervorlage unvollständig!";
			$nofault = 1;
		}
		else {																// alle Terminfelder eingegeben
			if(!preg_match($musterdatum, $wvl_tag) OR !preg_match($musterdatum, $wvl_monat) OR !preg_match($musterdatum, $wvl_jahr)) {	// Muster Datum passt nicht
				$fehler = "Fehler bei der Termineingabe!";
				$nofault = 1;									// kein refresh am Ende des Scriptes
			}
			else {  // Muster wiedervorlage WVL-Datum stimmt
			
				$wvldatum = $wvl_jahr . "-" . $wvl_monat . "-" . $wvl_tag;
				$heutewvl = $wvl_tag . "." . $wvl_monat . "." . $wvl_jahr;

				if (!empty($plz)) {					// heute + PLZ gesetzt
	
					if(!preg_match($musterplz, $plz)) {							// Muster PLZ passt nicht
						$fehler = "Fehler: Keine gültige PLZ!";
						$nofault = "1";
					} // Ende IF Muster PLZ passt nicht
					
					else { // Muster PLZ passt
				
					$sql = "SELECT plz_id FROM plz WHERE plz = '$plz'";
					$query_plz = myqueryi($db, $sql);						// PLZ wird mit Datenbank vergleichen
					$daten_plz = mysqli_fetch_array($query_plz, MYSQLI_NUM);
					
					if (!$daten_plz) {											// PLZ ist unbekannt
						$fehler = "Fehler: Unbekannte PLZ!";
						$nofault = "1";
					} 														// Ende IF PLZ ist unbekannt
					
					else {													// PLZ in DB
					
						if (empty($ort)) {									// kein ort eingegeben oder bekannt
						
							if ((isset($_POST['suchen']) OR !empty($plz)) and !(isset($_POST['suchen']))) {	//Anzeige Daten für heute und ganzes PLZ-Gebiet

								$sql1 = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
								$sql1 .= " vorwahl1 AS Vorw, telefon AS Telefon, plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, strasse AS Strasse, w_zeit AS Zeit ";
								$sql1 .= " FROM kunden, vorname, name, termin, vorwahl1, plz, ort, poo, ortsteil ";
								$sql1 .= " WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id ";
								$sql1 .= " AND termin.kd_id = kunden.kunden_id AND kunden.verzogen = '0' ";
								$sql1 .= " AND termin.wiedervorlage_date = '$wvldatum' AND termin.wiedervorlage = '1' AND termin.alt = '0' ";
								$sql1 .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
								$sql1 .= " AND plz.plz = '$plz' ";
												
								if (isset($strasse) AND ($switch == 1)) {					// Straße eingegeben
									$sql1 .= " AND strasse LIKE '$strasse%' ORDER BY ort ASC, strasse ASC ";
								}	
								else { // Sortierung absteigend
									$sql1 .= " AND strasse LIKE '$strasse%' ORDER BY ort ASC, strasse DESC ";
								}

								$ergebnis = myqueryi($db, $sql1);
								$anzahl = mysqli_num_rows($ergebnis);
								$fehler = "Anzahl der Datensätze:&nbsp;" . $anzahl;
									
								//Sessionvariablen
								$_SESSION['wvl_tag'] = $wvl_tag;
								$_SESSION['wvl_monat'] = $wvl_monat;
								$_SESSION['wvl_jahr'] = $wvl_jahr;
								$_SESSION['plz'] = $plz;
								
								$speichern = 1;
								
							}
						
							elseif (isset($_POST['suchen'])) {							// suchen button gedrückt
						
								$sql = "Select DISTINCT ort from `poo`, `ort`, `plz` ";
								$sql .=" where poo.plz_id=plz.plz_id and plz.plz='$plz' and poo.ort_id=ort.ort_id ";
								$query_ort = myqueryi($db, $sql);
							}	// ende suchen button gedrückt
						}
						
						else {															// Ort eingeben
					
							if (isset($_POST['suchen'])) {								// suchen button gedrückt
								$sql = "Select DISTINCT ort from `poo`, `ort`, `plz` ";
								$sql .=" where poo.plz_id=plz.plz_id and plz.plz='$plz' and poo.ort_id=ort.ort_id ";
								$query_ort = myqueryi($db, $sql);
									
							}	// ende suchen button gedrückt
							
							else {
						
								$sql1 = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
								$sql1 .= " vorwahl1 AS Vorw, telefon AS Telefon, plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, strasse AS Strasse, w_zeit AS Zeit ";
								$sql1 .= " FROM kunden, vorname, name, termin, vorwahl1, plz, ort, poo, ortsteil ";
								$sql1 .= " WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id ";
								$sql1 .= " AND termin.kd_id = kunden.kunden_id AND kunden.verzogen = '0' ";
								$sql1 .= " AND termin.wiedervorlage_date = '$wvldatum' AND termin.wiedervorlage = '1' AND termin.alt = '0' ";
								$sql1 .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
								$sql1 .= " AND ort.ort = '$ort' AND plz.plz = '$plz' ";
							
								if (isset($strasse) AND ($switch == 1)) {					// Straße eingegeben
									$sql1 .= " AND strasse LIKE '$strasse%' ORDER BY ort ASC, strasse ASC ";
								}	
								else { // Sortierung absteigend
									$sql1 .= " AND strasse LIKE '$strasse%' ORDER BY ort ASC, strasse DESC ";
								}
								
								$ergebnis = myqueryi($db, $sql1);
								$anzahl = mysqli_num_rows($ergebnis);
								$fehler = "Anzahl der Datensätze:&nbsp;" . $anzahl;
							
								//Sessionvariablen
								$_SESSION['wvl_tag'] = $wvl_tag;
								$_SESSION['wvl_monat'] = $wvl_monat;
								$_SESSION['wvl_jahr'] = $wvl_jahr;
								$_SESSION['plz'] = $plz;
								$_SESSION['ort'] = $ort;
								$_SESSION['strasse'] = $strasse;
								$_SESSION['switch'] = $switch;
								
								$speichern = 1;
							}
						} // ende Ort eingegeben
					}	// ende PLZ in DB
				}	// ende PLZ-muster passt
			} // ende Datum + PLZ gesetzt	
				
			else {								// nur Datum gesetzt - alle PLZs auswerten
		
				$sql1 = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
				$sql1 .= " vorwahl1 AS Vorw, telefon AS Telefon, plz AS PLZ, ort AS Ort, ortsteil AS Ortsteil, strasse AS Strasse, w_zeit AS Zeit ";
				$sql1 .= " FROM kunden, vorname, name, termin, vorwahl1, plz, ort, poo, ortsteil ";
				$sql1 .= " WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id ";
				$sql1 .= " AND termin.kd_id = kunden.kunden_id AND kunden.verzogen = '0' ";
				$sql1 .= " AND termin.wiedervorlage_date = '$wvldatum' AND termin.wiedervorlage = '1' AND termin.alt = '0' ";
				$sql1 .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
				$sql1 .= " ORDER BY PLZ ASC, ort ASC, strasse ASC ";
				$ergebnis = myqueryi($db, $sql1);
				$anzahl = mysqli_num_rows($ergebnis);
				$fehler = "Anzahl der Datensätze:&nbsp;" . $anzahl;
								
				//Sessionvariablen
				$_SESSION['wvl_tag'] = $wvl_tag;
				$_SESSION['wvl_monat'] = $wvl_monat;
				$_SESSION['wvl_jahr'] = $wvl_jahr;
				
				$speichern = 1;
				
								
			}	// ende nur Datum gesetzt
		} // ende Muster WVL-Datum stimmt	
	} // ende alle Terminfelder eingegeben
} // ende elseif WVL-Datum eingegeben

// --------------------------------------------------------------------------------------------------------------//
// Ende WVL-Datum eingegeben ------------------------------------------------------------------------------------//

	
// --- kein WVL-Datum eingegeben - Auswertung Kunde - PLZ - Ort - Straße ----------------------------------------//
// --------------------------------------------------------------------------------------------------------------//
	
	elseif (!isset($wvlheute) and empty($wvl_tag) and empty($wvl_monat) and empty($wvl_jahr)) {			// PLZ eingegeben
		if(!preg_match($musterplz, $plz)) {							// Muster PLZ passt nicht
			$fehler = "Fehler: Keine g&uuml;ltige PLZ!";
			$nofault = "1";
		} // Ende IF Muster PLZ passt nicht
					
		else { // Muster PLZ passt
				
		$sql = "SELECT plz_id FROM plz WHERE plz = '$plz'";
			$ergebnis = myqueryi($db, $sql);						// PLZ wird mit Datenbank vergleichen
			$daten = mysqli_fetch_array($ergebnis, MYSQLI_NUM);
					
			if (!$daten) {											// PLZ ist unbekannt
				$fehler = "Fehler: Unbekannte PLZ!";
				$nofault = "1";
			} 														// Ende IF PLZ ist unbekannt
				
			else {													// PLZ in DB
				if (empty($ort)) {									// kein ort eingegeben oder bekannt
						
					if (isset($_POST['speichern'])) {				// Speichern gedrückt, kein Ort eingegeben
						$fehler = "Bitte PLZ UND Ort eingeben!";
						$nofault = "1";
					}
						
					elseif (isset($_POST['suchen'])) {				// suchen button gedrückt
						$sql = "Select DISTINCT ort from `poo`, `ort`, `plz` ";
						$sql .=" where poo.plz_id=plz.plz_id and plz.plz='$plz' and poo.ort_id=ort.ort_id ";
						$query_ort = myqueryi($db, $sql);
					}	// ende suchen button gedrückt
				}	// ende kein Ort eingegeben
				
				else {											// Ort im Formular ausgewühlt
					switch($kundentyp) {
						case Vertragskunde:
							$sql1  = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
							$sql1 .= "vorwahl1 AS Vorw, telefon AS Telefon, ortsteil AS Ortsteil, strasse AS Strasse ";
							$sql1 .= "FROM kunden, vorname, name, vorwahl1, plz, ort, poo, ortsteil ";
							$sql1 .= "WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id  ";
							$sql1 .= "AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
							$sql1 .= "AND ort.ort = '$ort' AND plz.plz = '$plz' AND strasse LIKE '$strasse%' AND kunden.nichtkunde ='0' ";
							$sql1 .= " AND kunden.vertragskunde ='1' AND kunden.verzogen = '0'  AND kunden.belegt = '0' AND kunden.handy = '0' AND (kunden.ne IS NULL OR kunden.ne != '$heute') ";
						break;	
						case Wiedervorlage:
							$sql1  = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
							$sql1 .= " vorwahl1 AS Vorw, telefon AS Telefon, ortsteil AS Ortsteil, strasse AS Strasse, w_zeit AS Zeit, wiedervorlage_date AS wvl ";
							$sql1 .= " FROM kunden, vorname, name, vorwahl1, termin, plz, ort, poo, ortsteil  ";
							$sql1 .= " WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id  ";
							$sql1 .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
							$sql1 .= " AND ort.ort = '$ort' AND plz.plz = '$plz' AND strasse LIKE '$strasse%' ";
							$sql1 .= " AND (termin.wiedervorlage_date BETWEEN '0000-00-01' AND '$heute') AND termin.wiedervorlage = '1'  AND termin.kd_id = kunden.kunden_id ";
							$sql1 .= " AND kunden.verzogen = '0' AND kunden.belegt = '0' AND kunden.nichtkunde ='0' ";
							$sql1 .= " AND kunden.handy = '0' AND (kunden.ne IS NULL OR kunden.ne != '$heute') AND termin.alt = '0' ";
						break;
						case Nichtkunde:
							$sql1  = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
							$sql1 .= "vorwahl1 AS Vorw, telefon AS Telefon, ortsteil AS Ortsteil, strasse AS Strasse ";
							$sql1 .= "FROM kunden, vorname, name, vorwahl1, plz, ort, poo, ortsteil ";
							$sql1 .= "WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id  ";
							$sql1 .= "AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
							$sql1 .= "AND ort.ort = '$ort' AND plz.plz = '$plz'  AND strasse LIKE '$strasse%' ";
							$sql1 .= " AND kunden.nichtkunde ='1' AND kunden.verzogen = '0' AND kunden.belegt = '0' AND (kunden.ne IS NULL OR kunden.ne != '$heute') ";
						break;			
						case Neukunde:
							$sql1  = "SELECT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
							$sql1 .= "vorwahl1 AS Vorw, telefon AS Telefon, ortsteil AS Ortsteil, strasse AS Strasse ";
							$sql1 .= "FROM kunden, vorname, name, vorwahl1, plz, ort, poo, ortsteil ";
							$sql1 .= "WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id  ";
							$sql1 .= "AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
							$sql1 .= "AND ort.ort = '$ort' AND plz.plz = '$plz'  AND strasse LIKE '$strasse%' ";
							//$sql1 .= "AND ort.ort_id = '786' AND plz.plz_id = '224'  AND strasse LIKE '$strasse%' ";
							$sql1 .= " AND (kunden.ne IS NULL OR kunden.ne != '$heute') ";
							$sql1 .= " AND kunden.neukunde ='1' AND kunden.nichtkunde ='0' ";
						break;
						case alle:
							$sql1  = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
							$sql1 .= " vorwahl1 AS Vorw, telefon AS Telefon, ortsteil AS Ortsteil, strasse AS Strasse ";
							$sql1 .= " FROM kunden, vorname, name, vorwahl1, plz, ort, poo, ortsteil ";
							$sql1 .= " WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id  ";
							$sql1 .= " AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
							$sql1 .= " AND ort.ort = '$ort' AND plz.plz = '$plz'  AND strasse LIKE '$strasse%' ";
							$sql1 .= " AND kunden.verzogen = '0' ";
						break;
						case Handy:
							$sql1  = "SELECT DISTINCT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
							$sql1 .= "vorwahl1 AS Vorw, telefon AS Telefon, ortsteil AS Ortsteil, strasse AS Strasse ";
							$sql1 .= "FROM kunden, vorname, name, vorwahl1, plz, ort, poo, ortsteil ";
							$sql1 .= "WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id  ";
							$sql1 .= "AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
							$sql1 .= "AND ort.ort = '$ort' AND plz.plz = '$plz' AND strasse LIKE '$strasse%' ";
							$sql1 .= " AND kunden.verzogen = '0'  AND kunden.belegt = '0' AND kunden.handy = '1' AND (kunden.ne IS NULL OR kunden.ne != '$heute') ";
						break;
						case ne:
							$sql1  = "SELECT kunden_id AS ID, name AS Name, vorname AS Vorname, ";
							$sql1 .= "vorwahl1 AS Vorw, telefon AS Telefon, ortsteil AS Ortsteil, strasse AS Strasse ";
							$sql1 .= "FROM kunden, vorname, name, vorwahl1, plz, ort, poo, ortsteil ";
							$sql1 .= "WHERE vorname.vorname_id = kunden.vorname_id AND name.name_id = kunden.name_id AND vorwahl1.vorwahl1_id = kunden.vorwahl1_id  ";
							$sql1 .= "AND kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
							$sql1 .= "AND ort.ort = '$ort' AND plz.plz = '$plz' AND strasse LIKE '$strasse%' AND kunden.nichtkunde ='0' ";
							$sql1 .= " AND kunden.verzogen = '0'  AND kunden.belegt = '0' AND (kunden.ne = '$heute') ";
						break;
					}
						
					if ($switch == 1) { // Sortierung aufsteigend
						$sql1 .= " ORDER BY strasse ASC ";
					}
					else { // Sortierung absteigend
						$sql1 .= " ORDER BY strasse DESC ";
					}
					
					$ergebnis = myqueryi($db, $sql1);
					$anzahl = mysqli_num_rows($ergebnis);
					$fehler = "Anzahl der Datensätze:&nbsp;" . $anzahl;
								
					//Sessionvariablen
					$_SESSION['plz'] = $plz;
					$_SESSION['ort'] = $ort;
					$_SESSION['strasse'] = $strasse;
					$_SESSION['kundentyp'] = $kundentyp;
				
					$speichern = 1;
				} // ende Ort im Formular ausgewählt
					
			}	// ende PLZ in DB
		}	// ende PLZ-muster passt	
	} 

// ------------------------------------------------------------------------------------------------------------//
// ende elseif kein WVL-Datum eingegeben - Auswertung Kunde - PLZ - Ort - Straße ------------------------------//


// gar nichts eingegeben --------------------------------------------------------------------------------------//
// ------------------------------------------------------------------------------------------------------------//
	
	else {								// gar nichts eingegeben
		$fehler = "Ung&uuml;ltige Dateineingabe!";
		$nofault = "1";
	}
// ------------------------------------------------------------------------------------------------------------//
// Ende gar nichts eingegeben ---------------------------------------------------------------------------------//


} // ende if Speichern gedrückt ------------------------------------------------------------------------------//


?>

<!-- Erstmalige Darstellung des Formulars, Script startet -->

<!DOCTYPE html>
<html lang="de">
<head>
<title>Kunden-Liste</title>
	<!-- allgemein/telefon-liste.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align="left">
<table width="600" border="0" cellpadding="4" cellspacing="4">				<!-- Tabelle außen -->
<tr><td>
	<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">	<!-- Tabelle für Rand -->
		<tr><td>
			<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
				<tr>											<!-- Zeile für Formular -->
				<td valign = "top">								<!-- Zelle für Formular -->
				<!-- Start Formular für Startdaten ++++++++++++++++++++++++++++++++++ // -->
				<form name="startdaten" method="post" action="<?php $_SERVER["PHP_SELF"] ?>" style="margin: 0 0;">
					<table width="100%" cellspacing="1">
						<?php
						if (!empty($heutewvl) OR isset($wvlheute)) {
							echo "<tr bgcolor=\"#00ffff\">"; 								// Zeile für Wiedervorlage
  							echo "<td>WVL-Datum</td>";
    						echo "<td ><input type=\"text\" class = \"eingabe\" name=\"wvl_tag\" size = \"2\" maxlength =\"2\" value = \"$wvl_tag\">
								. 
        						<input type=\"text\" class = \"eingabe\" name=\"wvl_monat\" size = \"2\" maxlength =\"2\" value = \"$wvl_monat\">
        						. 
        						<input type=\"text\" class = \"eingabe\" name=\"wvl_jahr\" size = \"2\" maxlength =\"2\" value = \"$wvl_jahr\"></td>";
        						
							if (isset($wvlheute)) { echo "<td>oder WVL von heute<input type=\"checkbox\" name=\"wvlheute\" checked /></td>";}
								else { echo "<td>oder WVL von heute<input type=\"checkbox\" name=\"wvlheute\" /></td>";}
							echo "<td>&nbsp;</td>";	
							echo "</tr>";													
						}
						else {
							echo "<tr>"; 								// Zeile für Wiedervorlage
  							echo "<td>WVL-Datum</td>";
    						echo "<td><input type=\"text\" class = \"eingabe\" name=\"wvl_tag\" size = \"2\" maxlength =\"2\" value = \"$wvl_tag\">
								. 
        						<input type=\"text\" class = \"eingabe\" name=\"wvl_monat\" size = \"2\" maxlength =\"2\" value = \"$wvl_monat\">
        						. 
        						<input type=\"text\" class = \"eingabe\" name=\"wvl_jahr\" size = \"2\" maxlength =\"2\" value = \"$wvl_jahr\"></td>";
							echo "<td>oder WVL von heute<input type=\"checkbox\" name=\"wvlheute\" /></td>";
							echo "<td>&nbsp;</td>";	
							echo "</tr>";
						
						}
						?>
					
  						<tr>								<!-- Zeile für Kundentyp -->
  						<td align = "left">Kunde:</td>
  						<td align = "left"><select name="kundentyp" class = "eingabe">
		 				<?php
								if (isset($kundentyp)) {
									for ($i = 0; $i < count($typ); $i++) { // Erzeugung der Zeilen
										if ($kundentyp === $typ[$i]) {
											echo " <option selected>$typ[$i]</option>";
										}
										else {
											echo " <option>$typ[$i]</option>";
										}
									}
								} // ende if kundentyp gesetzt
								else {
	  								echo "<option selected>Neukunde</option>";
									echo "<option>Wiedervorlage</option>";
									echo "<option>ne</option>";
									echo "<option>Vertragskunde</option>";
									echo "<option>Nichtkunde</option>";
									echo "<option>alle</option>";
									echo "<option>Handy</option>";
								}
								
							?>
								</select>
						</td>
						<td>&nbsp;</td>
      					</tr>								<!-- ende Zeile für Kundentyp -->
					
	  					<tr>								<!-- Zeile für PLZ/Ort/Ortsteil -->
        				<td align = "left">PLZ:</td>
        				<td align = "left"><input type="text" maxlength = "5" size ="5" name="plz" value = "<?php echo "$plz"; ?>" class = "eingabe">&nbsp;&nbsp;<input type="submit" value = "Ort?" name="suchen" class = "suche"></td>
						<td align = "left">Ort&nbsp;
							<?php if(isset($suchen)) {
									echo "<select name=\"ort\">";
									while ($zeile = mysqli_fetch_array($query_ort)) {
										echo "<option>$zeile[0]</option>";
									}
									echo "</select>";
								}
								else {
									echo "<input type=\"text\" name=\"ort\" value = \"$ort\" class = \"eingabe\">";
								}
							?>
						</td>
						<td align = "left">Stra&szlig;e&nbsp;<input type="text" name="strasse" size = "10" value = "<?php echo "$strasse"; ?>" class = "eingabe"></td>
      					</tr>								<!-- Zeile für PLZ/Ort/Ortsteil -->
					
	  					<tr><td colspan = "4">&nbsp;</td></tr>		<!-- "Leer"-Zeile -->
					
    					<tr>									<!-- Zeile für Submitt -->
							<td colspan = "4" align = "center"><input type="submit" value = "Senden" name="speichern" class = "submitt">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value = "RESET" name="korrektur" class = "correct"></td>
     					</tr>
					</table>
				</form>										<!-- Ende Formular für Startdaten ++++++++++++++++++++++++++++++++++ // -->
		</td>		<!-- ende Zelle für Formular -->
	</tr>			<!-- Zeile für Formular -->

	<tr> <td bgcolor = "red" align = "center"><span style="background-color:red; color:white; font-weight:bold;"><?php echo"$fehler"; ?></span></td></tr>

</td>
</tr>	<!-- Ende Zeile Abfrage / Start Zeile Ergebnisse ++++++++++++++++++++++++++++++++++ // -->
			</table>


<?php

// kein Fehler bei der Eingabe - Ausgabe der Daten -------------------------------------------------------
//-------------------------------------------------------------------------------------------------------

if ($nofault == "0" AND isset($speichern)) {					
?>

<tr>
	<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">	<!-- Tabelle Ergebnisse ++++++++++++++++++++++++++++++++++ // -->
<form name="neu" method="post" action="<?php $_SERVER["PHP_SELF"] ?>" style="margin: 0 0;">

<tr><td valign ="top">
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
		<tr align = "left" valign="top"><td><input type="submit" value = "REFRESH" name="refresh" class ="submitt"></td>
   <?php
				if (isset($_POST['wvlheute']) OR !empty($heutewvl)) {
					echo "<td><span style=\"font-weight:bold;\">Telefonliste: Wiedervorlagen für: $heutewvl<br></span></td>";
				}
				else {
					echo "<td><span style=\"font-weight:bold;\">Telefonliste: $plz $ort, Kunden: $kundentyp</span></td>";
				}
		echo "</tr></table>";

		$daten = mysqli_fetch_array($ergebnis);
		if ($anzahl > 0) { 
			mysqli_data_seek($ergebnis, 0);						// Cursor von Ergebnis wieder auf Null stellen
		}
		
		//Ausgabe der Telefonliste +++++++++++++++++++++++++++++++++++++++++++++++
	
	echo "<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" width=\"100%\">";

		echo "<tr>";													// Tabellenkopf
			echo "<td align = \"left\"><span style=\"font-weight:bold;\">ID</td>";
			echo "<td align = \"left\"><span style=\"font-weight:bold;\">Name</td>";
			echo "<td align = \"left\"><span style=\"font-weight:bold;\">Vorname</td>";
			echo "<td align = \"left\"><span style=\"font-weight:bold;\">V-Wahl</td>";
			echo "<td align = \"left\"><span style=\"font-weight:bold;\">Tel.</td>";
			if (isset($_POST['wvlheute']) OR !empty($heutewvl)) {
				echo "<td align = \"left\"><span style=\"font-weight:bold;\">PLZ<br />Ort</td>";
			}
			echo "<td align = \"left\"><span style=\"font-weight:bold;\">OT</th>";
			echo "<td align = \"left\"><span style=\"font-weight:bold;\"><a href=\"telefon_liste.php?sort=$switch\" target=\"_self\">Straße</a></td>";
			if ($kundentyp == "Wiedervorlage") {
				echo "<td align = \"left\"><span style=\"font-weight:bold;\">Zeit</td>";
			}
		echo "</tr>\n";	
	
		
		$z=0;  //zähler der datensätze für bg_colour der zeilen
	
		$bg1 = "#eeeeee"; //die beiden hintergrundfarben
		$bg2 = "#dddddd";
		
		// Abfrage, ob in der gewählten Kundenkategorie Kunden mit aktiven WVL sind (nur, wenn keine WVL abgefragt werden!!
	
	if (!isset($_POST['wvlheute']) AND ($kundentyp == "Vertragskunde")) {
	
		$sql  = "SELECT DISTINCT kunden_id  ";
		$sql .= " FROM kunden, termin, plz, ort, poo, ortsteil ";
		$sql .= " WHERE kunden.poo_id = poo.poo_id AND poo.ort_id = ort.ort_id AND poo.plz_id = plz.plz_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
		$sql .= " AND ort.ort_id = '$ort_id' AND plz.plz_id = '$plz_id' AND strasse LIKE '$strasse%' AND kunden.vertragskunde ='1'  ";
		$sql .= " AND termin.wiedervorlage_date BETWEEN '0000-00-01' AND '2250-12-31' AND termin.alt = '0' AND termin.wiedervorlage = '1'  ";
		$sql .= " AND kunden.kunden_id = termin.kd_id ";
		$wvlquery = myqueryi($db, $sql);
	}
	
	while($zeile = mysqli_fetch_row($ergebnis))						// Schleife für Daten-Zeilen
	{	
		$bg=($z++ % 2) ? $bg1 : $bg2;
		echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
		
		if (isset($_POST['wvlheute']) OR !empty($heutewvl) OR !empty($wvl_tag)) {
			if ($zeile[9] == "00:00") { $zeile[9] = "&nbsp;"; }
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[0] . "</a></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[1] . "</a></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[2] . "</a></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[3] . "</a></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[4] . "</a></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[5] ."<br />" . $zeile[6] . "</a></span></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[7] . "</a></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[8] . "</a></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[9] . "</a></td>";
			echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[10] . "</a></td>";
		}
		
		elseif (($kundentyp == "Wiedervorlage")) {
			for($i = 0; $i < (mysqli_num_fields($ergebnis)-1); $i++)	{		// Schleife für Feld für WVL-Daum ausblened
				if ($zeile[7] == "00:00") { $zeile[7] = "&nbsp;"; }
				//elseif ($zeile[10] < $heute) { $zeile[7] = "&nbsp;"; } // Wiedervorlagedatum ist nicht heute - Zeit ausbleneden!!
				echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[$i] . "</a></td>";
			}
		}
		
		elseif (($kundentyp == "Vertragskunde")) {
		
			// die Query $wylquery wird in jeder Schleife der query $ergebnis durchlaufen und die Kunden-IDs verglichen
			// stimmen sie überein, hat der Vertragskunde noch offene WVL -> wird nicht angezeigt
			$treffer=0;				// kennzeichen für Übereinstimung zurücksetzen
			for ($j=0; $j<mysqli_num_rows($wvlquery); $j++) {
				$test=mysqli_fetch_array($wvlquery);
				if ($test[0] == $zeile[0]) { $treffer = 1; }
			}
			
			if ($treffer == 0) {		// keine Wiedervorlagen gefunden
				for($i = 0; $i < (mysqli_num_fields($ergebnis)); $i++)	{		// Schleife für Felder - PLZ und Ort ausblenden
					//if ($zeile[7] == "00:00") { $zeile[7] = "&nbsp;"; }
					echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[$i] . "</a></td>";
				}
			}
			else { $z = $z-1;}							// Zeilenzähler bei ausgeblendeter Zeile um 1 verringern(für Hintergrundfarbe)
			if (mysqli_num_rows($wvlquery) > 0) {
				mysqli_data_seek($wvlquery,0);		// Zeiger von $wvlquery rücksetzen für nächste Schleife $ergebnis
			}
		}
		elseif (($kundentyp == "Neukunde")) {
			for($i = 0; $i < (mysqli_num_fields($ergebnis)); $i++)	{		// Schleife für Felder - PLZ und Ort ausblenden
				if ($zeile[7] == "00:00") { $zeile[7] = "&nbsp;"; }
				echo "<td class=\"liste\" align = \"left\"><a href=\"termin_neu.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[$i] . "</a></td>";
			}
		}
		else {
			for($i = 0; $i < (mysqli_num_fields($ergebnis)); $i++)	{		// Schleife für Felder - PLZ und Ort ausblenden
				if ($zeile[7] == "00:00") { $zeile[7] = "&nbsp;"; }
				echo "<td class=\"liste\" align = \"left\"><a href=\"termin_check.php?kd_id=$zeile[0]\" target=\"aqframe_u\">" . $zeile[$i] . "</a></td>";
			}
		}
		echo "</tr>";
    }		// ende while Datenzeile
	
		if (!isset($_POST['wvlheute'])) {
			echo "<tr><td colspan = \"6\" align = \"center\"><input type=\"submit\" value = \"REFRESH\" name=\"refresh\" class = \"submitt\"></td></tr>";
		}
		else {
			echo "<tr><td colspan = \"9\" align = \"center\"><input type=\"submit\" value = \"REFRESH\" name=\"refresh\" class = \"submitt\"></td></tr>";
		}
		
	echo "</td></tr>";
echo "</table>";
echo "</form>";	// Ende Telefonliste +++++++++++++++++++++++++++++++++++++++++++++++
}
$close = mysqli_close($db);
?>
			</td></tr>
		</table>	<!-- ende Tabelle Rand -->
	</td></tr>
</table>	<!-- ende Tabelle außen -->
</div>
</body>
</html>