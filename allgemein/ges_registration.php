<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

if (isset($speichern)) {																		// Speichern-Button wurde gedrückt
	if (empty($ges_neu)) {
		$fehler = "Fehler: Der Name der Gesellschaft kann nicht leer sein!";				// kein User-Name oder Passwort
	}
	
	else { 	// neue ges_neu -----------------------------------------------------------------------------------------------------------
	
		$ges_neu = quote_smart($ges_neu);
		
		$sql = "SELECT ges_neu_id FROM ges_neu WHERE ges_neu = '$ges_neu'";
		$abfrage = myqueryi($db, $sql);
				
		if (mysqli_num_rows($abfrage) != 0) {
			$fehler = "Fehler: Diese Gesellschaft gibt es schon!";
		}
		else {
			$sql = "INSERT INTO ges_neu (ges_neu) VALUES ('$ges_neu') ";
			$abfrage = myqueryi($db, $sql);
			$ges_neu_id = mysqli_insert_id($db);
			
			$sql = "INSERT INTO produkt_neu (ges_neu_id, tarif_neu_id) VALUES ('$ges_neu_id', '1')";
			$abfrage = myqueryi($db, $sql);
	
			$sql = "INSERT INTO ges_alt (ges_alt) VALUES ('$ges_neu') ";
			$abfrage = myqueryi($db, $sql);
			$ges_alt_id = mysqli_insert_id($db);
			
			$sql = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '1')";
			$abfrage = myqueryi($db, $sql);
			
			$fehler = "Gesellschaft \"$ges_neu\" wurde eingefügt";
		}
	}
}	// Ende IF ISSET speichern


if (isset($update)) {
	if (empty($ges_neu)) {
		$fehler = "Fehler: Der Name der Gesellschaft kann nicht leer sein!";
	}
	else {
		$ges_neu = quote_smart($ges_neu);
		
		$sql = "UPDATE ges_neu SET ges_neu = '$ges_neu' WHERE ges_neu_id = '$ges_neu_id'";
		$ergebnis = myqueryi($db, $sql);
		
		$sql = "UPDATE ges_alt SET ges_alt = '$ges_neu' WHERE ges_alt_id = '$ges_neu_id'";
		$ergebnis = myqueryi($db, $sql);
	}
} // Ende IF UPDATE


//---------------------------------------------------------------------------------------------------------------------------------------

// Start Script-Ablauf vor dem Drücken des Speichern-Buttons

	$sql  = "SELECT ges_neu_id AS ID, ges_neu AS Gesellschaft ";
	$sql .= "FROM ges_neu ";
	$sql .= "WHERE ges_neu_id > '1'";
	
	$ergebnis = myqueryi($db, $sql);
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html >
<html lang="de">
<head>
<title>Ges./Tarife</title>
	<!-- allgemein/ges_registration.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">
<table width="600" border="0" cellpadding="4" cellspacing="4">
<tr>
<td><h2 class="Stil1">Gesellschaften und Tarife bearbeiten</h2><td>
</tr>
<tr><td>
<table width="100%" cellpadding="1" cellspacing="0" bgcolor="#000000">
<tr>
<td>
<table width="100%" border="0" cellpadding="1" cellspacing="0" bgcolor="#eeeeee">
<tr><td valign = "top">
<!-- Start Formular für neue Gesellschaft ++++++++++++++++++++++++++++++++++ // -->
<form name="ges_neu_neu" method="post" action="<?php $_SERVER["PHP_SELF"] ?>">
<table id="formular" cellspacing="8">
  <tr>
    <td>neue Gesellschaft:</td>
    <td><input type="text" name="ges_neu" size = "20" maxlength ="20"></td>
    <td><input type="submit" name="speichern" value="Speichern" class = "submitt"></td>
</table>      
</form>
<!-- Ende Formular für neue Gesellschaft ++++++++++++++++++++++++++++++++++ // -->
</td>
</tr>

<?php

echo "<tr><td valign = 'top'>";
echo "<table id='ausgabe' cellspacing='4' width = '100%'>";


//Ausgabe der bereits vorhandene Gesellschaften neu +++++++++++++++++++++++++++++++++++++++++++++++

	echo "<tr>vorhandene Gesellschaften:</tr>";
	echo "<tr>";													// Tabellenkopf
	for($i = 0; $i < mysqli_num_fields($ergebnis); $i++)			    // Anzahl der Tabellenzellen

		$row = mysqli_fetch_assoc($ergebnis);
		$feldname = array_keys($row);
		mysqli_data_seek($ergebnis, 0);
	{
		echo "<td><b>$feldname[$i]</b></td>";
	}
	echo "<td>&nbsp;</td>";											// eine Zelle für den BEARBEITEN-Button angehängt
	echo "<td>&nbsp;</td>";											// eine Zelle für den Termin-Button angehängt
	echo "</tr>\n";													// Tabellenkopf Ende
	
	$z=0;  //zähler der datensätze für bg_colour der zeilen
	$bg1 = "#eeeeee"; //die beiden hintergrundfarben
	$bg2 = "#dddddd";	
	
	for ($j = 0; $j < mysqli_num_rows($ergebnis); $j++)				// Anzahl der Datensätze
	{
		$zeile = mysqli_fetch_row($ergebnis);						// Schleife für Daten-Zeilen
			
			$bg=($z++ % 2) ? $bg1 : $bg2;
			echo "<tr bgcolor=$bg onMouseOver=\"this.bgColor='moccasin';\" onMouseOut=\"this.bgColor='$bg';\">";
			
			for($i = 0; $i < mysqli_num_fields($ergebnis); $i++)	// Schleife für Felder pro Zeile + bearbeiten- + Termin-Link
			{	
				echo "<td >" . $zeile[$i] . "</td>";
			}					
			echo "<td align=\"center\"> <a href=\"ges_update.php?id=$zeile[0]&name=$zeile[1]\">Gesell. bearbeiten</a></td>"; //Link zum Gesellschaften-Bearbeiten-Script
			echo "<td align=\"center\"> <a href=\"tarif_neu.php?id=$zeile[0]&name=$zeile[1]\">Tarife</a></td>"; //Link zum Gesellschaften-Bearbeiten-Script
			echo "</tr>";
    }
	if ($fehler) {
	echo "<tr><td colspan = \"4\" bgcolor = \"red\"><span style=\"color:white; font-weight:bold;\">$fehler</span></td></tr>";
}
	echo "</table>";
// Ende Ausgabe vorhandene Gesellschaft +++++++++++++++++++++++++++++++++++++++++++++++
?>

</td>
</table>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</div>
</body>
</html>