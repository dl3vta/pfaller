<?php
include ("include/ini.php");		// Session-Lifetime
session_start();



// Debug Meldungen einschalten
error_reporting(-1);

include ("../include/mysqlconnect.inc.php");
include ("../include/variablen.php");
include ("../include/funktionen.php");


error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen


// SESSION-Variablen ---------------------------------------------------------------

if (isset($_SESSION['benutzer_gruppen'])) {$gruppe = ($_SESSION['benutzer_gruppen']);}    	// Gruppe des eingeloggten Benutzers
if (isset($_SESSION['benutzer_kurz'])) {$telefonist = quote_smart($_SESSION['benutzer_kurz']);}  // Kürzel eingeloggter Benutzer

//$gruppe = $_COOKIE['cookie']['benutzer_gruppen'];
//$telefonist = $_COOKIE['cookie']['benutzer_kurz'];

/*echo "<pre>";
echo "Telefonist: " . $telefonist;
echo "<br>";
echo "Gruppe: " . $gruppe;*/


//$gruppe = $_SESSION['benutzer_gruppen'];
//$telefonist = quote_smart($_SESSION['benutzer_kurz']);

// GET-Variablen aus such_check.php/termine ----------------------------------------

if (isset($_GET['kd_id'])) {$kunden_id = quote_smart($_GET['kd_id']);}
if (isset($_GET['t_id'])) {$t_id = quote_smart($_GET['t_id']);}
if (isset($_GET['ad'])) {$adtermin = quote_smart($_GET['ad']);}

if (isset($_GET['aq'])) {$aqtermin = quote_smart($_GET['aq']);}
if (isset($_GET['start'])) {$start = quote_smart($_GET['start']);}
if (isset($_GET['ende'])) {$ende = quote_smart($_GET['ende']);}
if (isset($_GET['zahl'])) {$zahltermin = quote_smart($_GET['zahl']);}
if (isset($_GET['file'])) {$file = quote_smart($_GET['file']);}

// POST-Variablen (Buttons aus Formular) --------------------------------------------

if (isset($_POST['speichern'])) {$speichern = $_POST['speichern'];}    // Speichern-Button
if (isset($_POST['reset'])) {$reset = $_POST['reset'];}   	// Korrektur-Button, leert das Formular vollständig
if (isset($_POST['tneu'])) {$tneu = $_POST['tneu'];}  // Suchbutton für Tarif neu
if (isset($_POST['talt'])) {$talt = $_POST['talt'];}    // Suchbutton für Tarif alt
if (isset($_POST['plzneu'])) {$plzneu = $_POST['plzneu'];}	// Suchbutton für PLZ aus SELECT-Feld
if (isset($_POST['ortneu'])) {$ortneu = $_POST['ortneu'];}      	// Suchbutton für Ort aus SELECT-Feld
if (isset($_POST['test'])) {$test = $_POST['test'];}	// Test-Button für Telefonnummer

// POST-Variablen (Buttons aus Formular) -------------------------------------------

//if(!$termin){ $termin = '';}
//if(!$kunde){ $kunde = '';}
$kunde = array('vorname', 'name', 'vorwahl1', 'telefon', 'plz', 'ort', 'ortsteil','strasse',
				'fax', 'email', 'rem_kunde', 'geboren', 'nichtkunde',
				'belegt', 'wiedervorlage', 'vertragskunde', 'vorwahl2', 'fax', 'branche', 'verzogen',
				'ne', 'kunden.einzug', 'kunden.datenschutz');
				
$termin = array('termin', 'zeit', 'kd_id', 'telefonist', 'aussendienst', 'ges_alt', 'tarif_alt',
				'von', 'bis', 'ges_neu', 'tarif_neu', 'seit', 'rem_termin', 'aquiriert', 'erledigt_date',
				'vonr', 'termin.storno', 'sperrzeit', 'wiedervorlage_date', 'w_zeit', 'nichtkunde',
				'abschluss', 'wiedervorlage', 'alt', 'kalt', 'thandy', 'einzug');



// *******************************************************************************************************************************************

//if ($gruppe == "Aquise" AND $file == "termine" ) { unset($t_id); }	// Telefonist darf aus der Terminübersicht keinen termin bearbeiten

// *******************************************************************************************************************************************

// Konstanten ----------------------------------------------------------------------
$aquiriert = date("Y-m-d H:i");					// Datum von heute und aktuelle Zeit
$heute = date("Y-m-d");							// Datum von heute
$speich = date("d.m.y");						// Datum von heute (für Speicherung)
$norefresh = 0;									// steuert das Refresh nach dem Speichern - nur wenn $norefresh = 0, dann refresh

// Hintergrundfarben für die einzelnen Formularbereiche ----------------------------
// diese werden im Fehlerfall durch die Fehler-Farbe ersetzt -----------------------

$bg = "#F0F9FF";								// Hintergrundfarbe für Tabellenzeilen
$bg_ad = "#F0F9FF";								// Hintergrundfarbe für Tabellenzeilen
$bg_termin = "#F0F9FF";							// Hintergrundfarbe für Termin
$bg_wvl = "#F0F9FF";							// Hintergrundfarbe für Wiedervorlage

$bg_ges_alt = "#E0F9FF";						// Hintergrundfarbe für Formularabschnitt "Gesellschaft alt"
$bg_vom = "#E0F9FF";							// Hintergrundfarbe für vom-Datum
$bg_bis = "#E0F9FF";							// Hintergrundfarbe für bis-Datum

$bg_ges_neu = "#F0F9BB";						// Hintergrundfarbe für Formularabschnitt "Gesellschaft neu"
$bg_seit = "#F0F9BB";							// Hintergrundfarbe für seit-Datum

$bg_vorname = "#F0F9FF";						// Hintergrundfarbe für Vorname
$bg_name = "#F0F9FF";							// Hintergrundfarbe für Name

$bg_plz_alt = "#D0F9FF";						// Hintergrundfarbe für Formularabschnitt "PLZ schon vorhanden"
$bg_ort = "#D0F9FF";							// Hintergrundfarbe für Ort neu (PLZ vorhanden)
$bg_ot = "#D0F9FF";								// Hintergrundfarbe für Ortsteil neu (PLZ vorhanden)

$bg_plz_neu = "#C0F9BB";						// Hintergrundfarbe für Formularabschnitt "Plz neu"
$bg_plz = "#C0F9BB";							// Hintergrundfarbe für PLZ-neu
$bg_ortneu = "#C0F9BB";							// Hintergrundfarbe für Ort neu PLZ-neu
$bg_otneu = "#C0F9BB";							// Hintergrundfarbe für Ortsteil neu PLZ-neu

$bg_geboren = "#F0F9FF";						// Hintergrundfarbe für geboren
$bg_vorwahl1 = "#F0F9FF";						// Hintergrundfarbe für Vorwahl1
$bg_telefon = "#F0F9FF";						// Hintergrundfarbe für Telefon1
$bg_vorwahl2 = "#F0F9FF";						// Hintergrundfarbe für Vorwahl2
$bg_fax = "#F0F9FF";							// Hintergrundfarbe für Telefon2

$bg_fehler = "#ff9966";							// Fehler-Farbe


$ta_zeit = array("", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30",
				"15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00");					// Array für Zeiten

$wa_zeit = array("", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00",
				"15:00", "16:00", "17:00", "18:00", "19:00", "20:00");																// Array für Wiedervorlage-Zeiten


// wenn $kunden__id gesetzt ist - nur Kundendaten sollen geündert werden -------------------------------------------------

if ($kunden_id) {

	$sql  = "SELECT vorname, name, vorwahl1, telefon, plz, ort, ortsteil,  strasse, fax, email, rem_kunde, geboren, ";
	$sql  .= "nichtkunde, belegt, wiedervorlage, vertragskunde, vorwahl2, fax, branche, verzogen, ne, kunden.einzug, kunden.datenschutz ";
	$sql .= "FROM kunden, vorname, name, vorwahl1, poo, plz, ort, ortsteil, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id' AND kunden.vorname_id = vorname.vorname_id ";
	$sql .= "AND name.name_id = kunden.name_id AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	$sql .= "AND kunden.vorwahl2_id = vorwahl2.vorwahl2_id AND kunden.branche_id = branche.branche_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$ergebnis = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($ergebnis, MYSQLI_ASSOC);
}

// wenn $t_id gesetzt ist - Kunden und Termindaten sollen geändert werden -------------------------------------------------

elseif ($t_id) {

	$sql = "SELECT termin, zeit, kd_id, telefonist, aussendienst, ges_alt, tarif_alt, von, bis, ges_neu, tarif_neu, seit, ";
	$sql .= " rem_termin, aquiriert, erledigt_date, vonr, termin.storno, sperrzeit, wiedervorlage_date, w_zeit, ";
	$sql .= " nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, einzug ";
	$sql .= " FROM termin, produkt_alt, ges_alt, tarif_alt, produkt_neu, ges_neu, tarif_neu ";
	$sql .= " WHERE termin.termin_id = '$t_id' ";
	$sql .= " AND termin.produkt_alt_id = produkt_alt.produkt_alt_id AND produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
	$sql .= " AND termin.produkt_neu_id = produkt_neu.produkt_neu_id AND produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
	$query_termin = myqueryi($db, $sql);
	$termin = mysqli_fetch_array($query_termin, MYSQLI_ASSOC);
	$kunden_id = $termin['kd_id'];

	$sql  = "SELECT vorname, name, vorwahl1, telefon, plz, ort, ortsteil,  strasse, fax, email, rem_kunde, geboren, ";
	$sql  .= "nichtkunde, belegt, wiedervorlage, vertragskunde, vorwahl2, fax, branche, verzogen, ne, kunden.einzug, kunden.datenschutz ";
	$sql .= "FROM kunden, vorname, name, vorwahl1, poo, plz, ort, ortsteil, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id' AND kunden.vorname_id = vorname.vorname_id ";
	$sql .= "AND name.name_id = kunden.name_id AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id ";
	$sql .= "AND kunden.vorwahl2_id = vorwahl2.vorwahl2_id AND kunden.branche_id = branche.branche_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$query_kunde = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($query_kunde, MYSQLI_ASSOC);

	// Ermittlung von Vorname und Name des Mitarbeiters, der den Termin gespeichert hat

	$sql = " SELECT vorname, name FROM vorname, name, user ";
	$sql .= " WHERE user.user = '$termin[telefonist]' AND user.vorname_id = vorname.vorname_id AND user.name_id = name.name_id";
	$query_aquise = myqueryi($db, $sql);
	$aquise = mysqli_fetch_array($query_aquise);
	$mitarbeiter = $aquise[0] . "&nbsp;" . $aquise[1];										// von wem wurde der Termin gespeichert
}

// Erzeugung der Inhalte für die Formularfelder - Variablen kommen aus der Datenbank -----------------------------------------------

$ad = $termin['aussendienst'];

list($t_jahr, $t_monat, $t_tag) = explode("-", $termin['termin']);					// Termin extrahieren
$t_jahr = substr($t_jahr, 2, 2); 													// vom Jahr nur die letzten 2 Stellen darstellen
$t_zeit = $termin['zeit'];															// Uhrzeit des Termins

$wvl = $termin['wiedervorlage'];														// Wiedervorlage-Checkbox
list($wvl_jahr, $wvl_monat, $wvl_tag) = explode("-", $termin['wiedervorlage_date']);	// Wiedervorlage-Datum extrahieren
$wvl_jahr = substr($wvl_jahr, 2, 2); 												// vom Jahr nur die letzten 2 Stellen darstellen
$wvl_zeit = $termin['w_zeit'];														// Uhrzeit des Wiedervorlage-Termins

$ges_alt = $termin['ges_alt'];														// Gesellschaft alt
$tarif_alt = $termin['tarif_alt'];													// Tarif alt
list($v_jahr, $v_monat, $v_tag) = explode("-", $termin['von']);						// von-Datum extrahieren Ges. alt
$v_jahr = substr($v_jahr, 2, 2); 													// vom Jahr nur die letzten 2 Stellen darstellen
list($b_jahr, $b_monat, $b_tag) = explode("-", $termin['bis']);						// seit-Datum extrahieren Ges. alt
$b_jahr = substr($b_jahr, 2, 2); 													// vom Jahr nur die letzten 2 Stellen darstellen

$ges_neu = $termin['ges_neu'];														// Gesellschaft neu
$tarif_neu = $termin['tarif_neu'];													// Tarif neu
list($s_jahr, $s_monat, $s_tag) = explode("-", $termin['seit']);						// seit-Datum extrahieren Ges. neu
$s_jahr = substr($s_jahr, 2, 2); 													// vom Jahr nur die letzten 2 Stellen darstellen

$vonr = $termin['vonr'];																// VO-Nr.

$storno=$termin['storno']; 															// Storno

$rem_termin = $termin['rem_termin'];													// Termin-Bemerkung

$speicherzeit = explode(" ", $termin['aquiriert']);									// aquiriert in Datum und Zeit trennen,
$aq_datum = mysqldate_in_de($speicherzeit[0]);										// in deutsches Datum umwandelt
if ($aq_datum == "00.00.0000") {
$gespeichert = "";
}
else {
$gespeichert = $aq_datum . "&nbsp;&nbsp;" .$speicherzeit[1];
}

$aq  = $termin['telefonist'];															// und von wem (Kürzel)

$erledigt = $termin['erledigt_date'];													// Datum, wann Termin abgeschlossen wurde
if ($erledigt == "0000-00-00") { $erledigt = ""; }
$kalt = $termin['kalt'];																// kalter Termin/Abschluss
$alt = $termin['alt'];																// Termin ist als alt(abgeschlossen) gekennzeichnet

$thandy = $termin['thandy'];															// Termin ist ein Handy-Termin

$vorname = $kunde['vorname'];															// Vorname
$name = $kunde['name'];																// Name

$plz = $kunde['plz'];																	// PLZ
$ort = $kunde['ort'];																	// Ort
$strasse = $kunde['strasse'];															// Straße

$ot = $kunde['ortsteil'];																// Ortsteil

list($g_jahr, $g_monat, $g_tag) = explode("-", $kunde['geboren']);					// Geburtstag extrahieren
$g_jahr = substr($g_jahr, 2,2);														// vom Jahr nur die letzten 2 Stellen darstellen

$vorwahl1 = $kunde['vorwahl1'];														// Vorwahl 1
$telefon = $kunde['telefon'];															// Telefon
$vorwahl2 = $kunde['vorwahl2'];														// Vorwahl2
$fax = $kunde['fax'];																	// Telefon 2
$email = $kunde['email'];																// E-Mail
$branche = $kunde['branche'];															// Branche
$rem_kunde = $kunde['rem_kunde'];														// Bemerkung Kunde
$datenschutz = $kunde['datenschutz'];													// Datenschutzerklärung erhalten?

$einzug = $termin['einzug'];															// Einzugsermächtigung erteilt

if ($kunde['nichtkunde'] == 0 AND $termin['nichtkunde'] == 0) { $nichtkunde = 0; }		// Nichtkunde
else { $nichtkunde = 1; }

$verzogen = $kunde['verzogen'];														// verzogen

if ($termin['wiedervorlage'] == 1) {
	$wvl = 1;
}

if ($kunde['ne'] === $heute) {
	$ne = 1;
}

// --------------------------------------------------------------------------------------------------------------------------------------

// "Leeren" wurde gedrückt, alle Eingaben (und Datenbank-Variablen) sollen verworfen werden ----------------------------------------------

if (isset($reset)) {

	unset($kunden_id, $t_id, $ad, $aq);
	unset($t_tag, $t_monat, $t_jahr, $t_zeit);
	unset($wvl, $wvl_tag, $wvl_monat, $wvl_jahr, $wvl_zeit);
	unset($ges_alt, $ges_alt_input, $tarif_alt, $tarif_alt_input, $v_tag, $v_monat, $v_jahr, $b_tag, $b_monat, $b_jahr);
	unset($ges_neu, $ges_neu_input, $tarif_neu, $tarif_neu_input, $s_tag, $s_monat, $s_jahr, $vonr, $storno, $rem_termin);
	unset($erledigt, $alt);
	unset($vorname, $name);
	unset($plz, $plz_input, $ort, $ort_input, $ort_input1, $strasse, $ot, $ot_input, $ot_input1);
	unset($g_tag, $g_monat, $g_jahr);
	unset($vorwahl1, $telefon, $vorwahl2, $fax, $email);
	unset($branche, $branche_input);
	unset($rem_kunde);
	unset($datenschutz);
	unset($einzug, $nichtkunde, $verzogen, $doppelt, $ne, $thandy, $alt, $new, $kalt, $ne, $norefresh, $absch);

	//Verzögerung um 300 Millisekunden (usleep verlangt Mikrosekunden!)
	//flush();
	//sleep(1);

	//Nachladen der Termin-Übersicht-Seite im oberen Frame, Kunden-ID wird übergeben, Terminübersicht für den Kunden wird geladen
	//echo "<script>onload=parent['suche_oben'].location.href='such_check.php?kd_id=$kunden_id'</script>";


}
// Ende Formular leeren --------------------------------------------------------------------------------------------------------------


// Speichern wurde gedrückt oder Suche nach Tarif-neu oder Suche nach Tarif alt, PLZ, Ort oder nur Telefon-Test - Daten werden überprüft
// -----------------------------------------------------------------------------------------------------------------------------------

if (isset($speichern) OR (isset($tneu)) OR (isset($talt)) OR (isset($plzneu)) OR (isset($ortneu)) OR (isset($test))) {


	if (isset($_POST['tid'])) {$t_id = $_POST['tid'];} else { $t_id = '';}  // Termin_ID
	if (isset($_POST['kundenid'])) {$kunden_id = $_POST['kundenid'];} else { $kunden_id = '';}  // Kunden_ID

	//$t_id = ($_POST['tid']);
	//$kunden_id = ($_POST['kundenid']);								// Kunden_ID

	unset($fehler);															// Fehlerausgabe wird zurückgesetzt

	// POST-Variablen werden gesichert, um Anzeige der geänderten Formulardaten nach Bildwiederaufbau zu ermöglichen
	// --------------------------------------------------------------------------------------------------------------

	$ad = quote_smart($_POST['ad_neu']);							// Außendienstler
	$aq = quote_smart($_POST['aq_neu']);							// Telefonist

	$t_tag = ($_POST['t_tag_neu']);								// Datum Termin
	$t_monat = ($_POST['t_monat_neu']);							// Datum Termin
	$t_jahr = ($_POST['t_jahr_neu']);								// Datum Termin
	$t_zeit = ($_POST['t_zeit_neu']);								// Zeit Termin

	$wvl = ($_POST['wvl_neu']);									// Wiedervorlage-Checkbox
	$wvl_tag = quote_smart($_POST['wvl_tag_neu']);					// Datum Wiedervorlage
	$wvl_monat = quote_smart($_POST['wvl_monat_neu']);				// Datum Wiedervorlage
	$wvl_jahr = quote_smart($_POST['wvl_jahr_neu']);				// Datum Wiedervorlage
	$wvl_zeit = quote_smart($_POST['wvl_zeit_neu']);				// Zeit Wiedervorlage

	$ges_alt = quote_smart($_POST['ges_alt_neu']);					// Gesellschaft alt - aus select-Feld
	$ges_alt_input = quote_smart($_POST['ges_alt_input_neu']);		// Gesellschaft alt - neu eingegeben
	$tarif_alt = quote_smart($_POST['tarif_alt_neu']);				// Tarif alt - aus select-Feld
	$tarif_alt_input = quote_smart($_POST['tarif_alt_input_neu']);	// Tarif alt - neu eingegeben
	$v_tag = quote_smart($_POST['v_tag_neu']);						// von-Datum Ges. alt
	$v_monat = quote_smart($_POST['v_monat_neu']);					// von-Datum Ges. alt
	$v_jahr = quote_smart($_POST['v_jahr_neu']);					// von-Datum Ges. alt
	$b_tag = quote_smart($_POST['b_tag_neu']);						// bis-Datum Ges. alt
	$b_monat = quote_smart($_POST['b_monat_neu']);					// bis-Datum Ges. alt
	$b_jahr = quote_smart($_POST['b_jahr_neu']);					// bis-Datum Ges. alt

	$ges_neu = quote_smart($_POST['ges_neu_neu']);					// Gesellschaft neu - aus select-Feld
	$ges_neu_input = quote_smart($_POST['ges_neu_input_neu']);		// Gesellschaft neu - neu eingegeben
	$tarif_neu = quote_smart($_POST['tarif_neu_neu']);				// Tarif neu - aus select-Feld
	$tarif_neu_input = quote_smart($_POST['tarif_neu_input_neu']);	// Tarif_neu - neu eingegeben
	$s_tag = quote_smart($_POST['s_tag_neu']);						// seit-Datum Ges. neu
	$s_monat = quote_smart($_POST['s_monat_neu']);					// seit-Datum Ges. neu
	$s_jahr = quote_smart($_POST['s_jahr_neu']);					// seit-Datum Ges. neu

	$vonr = quote_smart($_POST['vonr_neu']);						// VO-Nr.
	$storno = ($_POST['storno_neu']);								// Storno
	if(empty($storno)) {$storno = 0; }

	$rem_termin = quote_smart($_POST['rem_termin_neu']);

	$erledigt = quote_smart($_POST['erledigt']);					// Datum, wann Termin abgeschlossen wurde

	$einzug = ($_POST['einzug_neu']);								// Einzugsermächtigung

	$vorname = quote_smart($_POST['vorname_neu']);					// Vorname
	$name = quote_smart($_POST['name_neu']);						// Name

	$plz = quote_smart($_POST['plz_neu']);							// PLZ
	$plz_input = quote_smart($_POST['plz_input_neu']);				// PLZ - neu eingegeben

	$ort = quote_smart($_POST['ort_neu']);							// Ort
	$ort_input = quote_smart($_POST['ort_input_neu']);				// Ort - neu eingegeben, aber PLZ schon vorhanden
	$ort_input1 = quote_smart($_POST['ort_input1_neu']);			// Ort - neu eingegeben, PLZ neu

	$strasse = quote_smart($_POST['strasse_neu']);					// Straße

	$ot = quote_smart($_POST['ot_neu']);							// Ortsteil
	$ot_input = quote_smart($_POST['ot_input_neu']);				// Ortsteil - neu eingegeben, aber PLZ schon vorhanden
	$ot_input1 = quote_smart($_POST['ot_input1_neu']);				// Ortsteil - neu eingegeben, PLZ neu

	$g_tag = quote_smart($_POST['g_tag_neu']);						// Geburtstag
	$g_monat = quote_smart($_POST['g_monat_neu']);					// Geburtstag
	$g_jahr = quote_smart($_POST['g_jahr_neu']);					// Geburtstag


	$vorwahl1 = quote_smart($_POST['vorwahl1_neu']);				// Vorwahl 1
	$telefon = quote_smart($_POST['telefon_neu']);					// Telefon
	$vorwahl2 = quote_smart($_POST['vorwahl2_neu']);				// Vorwahl2
	$fax = quote_smart($_POST['fax_neu']);							// Telefon 2
	$email = quote_smart($_POST['email_neu']);						// E-Mail
	$branche = quote_smart($_POST['branche_neu']);					// Branche im Select-Feld
	$branche_input = quote_smart($_POST['branche_input_neu']);		// Branche neu eingegeben
	$rem_kunde = quote_smart($_POST['rem_kunde_neu']);				// Bemerkung Kunde
	$datenschutz = ($_POST['datenschutz']);						// Datenschutzerklärung
    $nichtkunde = ($_POST['nichtkunde_neu']);						// Nichtkunde
	$verzogen = ($_POST['verzogen_neu']);							// Kunde ist verzogen
	$doppelt = ($_POST['doppelt_neu']);							// Termin soll doppelt belegt werden
	$ne = ($_POST['ne_neu']);										// Kunde wurde nicht erreicht
	$thandy = ($_POST['thandy_neu']);								// Termin ist ein Handy-Termin
	$alt = ($_POST['alt_neu']);									// Termin abschließen
	$new = ($_POST['new_neu']);									// neuer Kunde aus vorhandenen Daten!
	$kalt = ($_POST['kalt_neu']);									// kalter Termin
	$absch = ($_POST['absch_neu']);								// Termin abschließen

	// Ende POST-Variablen -----------------------------------------------------------------------------------------

	// Ermittlung der Belegung der Checkboxen ----------------------------------------------------------------------
	$wvl = checkbox($wvl);													// Wiedervorlage-Checkbox
	$doppelt = checkbox($doppelt);											// Termin doppelt belegen
	$einzug = checkbox($einzug);											// Einzugsermächtigung
	$nichtkunde = checkbox($nichtkunde);									// Nichtkunde
	$verzogen = checkbox($verzogen);										// unbekannt verzogen
	$alt = checkbox($alt);													// Termin soll storniert werden
	$thandy = checkbox($thandy);											// Handy-Termin
	$new = checkbox($new);													// neuer Kunde
	$kalt = checkbox($kalt);												// kalter Termin
	$ne = checkbox($ne);													// Kunde nicht erreicht
	$absch = checkbox($absch);												// Termin abschließen

    if($_POST['datenschutz'] == 'on') {
        $datenschutz = 1;
    }

	//$datenschutz = checkbox($datenschutz);

	// Debugging ---------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------
	//echo "Gruppe: $gruppe<br>";								// Gruppe des eingeloggter User
	//echo "Telefonist: $telefonist<br>";						// eingeloggter User
	//echo "Kunden-ID: $kunden_id<br>";							// Kunden-ID
	//echo "Termin-ID: $t_id<br>";								// Termin-ID
	//echo "AD: $ad<br>";										// Außendienstler
	//echo "AQ: $aq<br>";										// Telefonist
	//echo "Termin: $t_tag.$t_monat.$t_jahr<br>";				// Datum Termin
	//echo "Termin-Zeit: $t_zeit<br>";							// Zeit Termin
	//echo "WVL: $wvl<br>";										// Wiedervorlage-Checkbox
	//echo "Termin-WVL: $wvl_tag.$wvl_monat.$wvl_jahr<br>";		// Datum Wiedervorlage
	//echo "WVL-Zeit: $wvl_zeit<br>";							// Zeit Wiedervorlage
	//echo "Ges.alt: $ges_alt<br>";								// Gesellschaft alt - aus select-Feld
	//echo "Ges.alt-Input: $ges_alt_input<br>";					// Gesellschaft alt - neu eingegeben
	//echo "Tarif.alt: $tarif_alt<br>";							// Tarif alt - aus select-Feld
	//echo "Tarif.alt-Input: $tarif_alt_input<br>";				// Tarif alt - neu eingegeben
	//echo "vom: $v_tag.$v_monat.$v_jahr<br>";					// von-Datum Ges. alt
	//echo "bis: $b_tag.$b_monat.$b_jahr<br>";					// bis-Datum Ges. alt
	//echo "Ges.neu: $ges_neu<br>";								// Gesellschaft neu - aus select-Feld
	//echo "Ges.neu-Input: $ges_neu_input<br>";					// Gesellschaft neu - neu eingegeben
	//echo "Tarif.neu: $tarif_neu<br>";							// Tarif neu - aus select-Feld
	//echo "Tarif.neu-Input: $tarif_neu_input<br>";				// Tarif_neu - neu eingegeben
	//echo "seit: $s_tag.$s_monat.$s_jahr<br>";					// seit-Datum Ges. neu
	//echo "VONR: $vonr<br>";									// VO-Nr.
	//echo "Storno: $storno<br>";								// Storno
	//echo "Einzug: $einzug<br>";									// Einzugsermächtigung
	//echo "Bemerk.Termin: $rem_termin<br>";					// Bemerkung zum Termin
	//echo "erledigt: $erledigt<br>";							// Datum, wann Termin abgeschlossen wurd
	//echo "alt: $alt<br>";										// Termin ist als alt gekennzeichnet oder soll abgeschlossen werden
	//echo "Vorname: $vorname<br>";								// Vorname
	//echo "Name: $name<br>";									// Name
	//echo "PLZ: $plz<br>";										// PLZ
	//echo "PLZ-Input: $plz_input<br>";							// PLZ - neu eingegeben
	//echo "Ort: $ort<br>";										// Ort
	//echo "Ort-Input: $ort_input<br>";							// Ort - neu eingegeben, aber PLZ schon vorhanden
	//echo "Ort-Input-neue PLZ: $ort_input1<br>";				// Ort - neu eingegeben, PLZ neu
	//echo "Strasse: $strasse<br>";								// Straße
	//echo "Ortsteil: $ot<br>";									// Ortsteil
	//echo "Ortsteil-Input: $ot_input<br>";						// Ortsteil - neu eingegeben, aber PLZ schon vorhanden
	//echo "Ortsteil-Input-neue PLZ: $ot_input1<br>";			// Ortsteil - neu eingegeben, PLZ ne
	//echo "geboren: $g_tag.$g_monat.$g_jahr<br>";				// Geburtstag
	//echo "Vorwahl1: $vorwahl1<br>";							// Vorwahl 1
	//echo "Telefon: $telefon<br>";								// Telefon
	//echo "Vorwahl2: $vorwahl2<br>";							// Vorwahl2
	//echo "Telefon2: $fax<br>";								// Telefon 2
	//echo "E-Mail: $email<br>";								// E-Mail
	//echo "Branche: $branche<br>";								// Branche im Select-Feld
	//echo "Branche-Input: $branche_input<br>";					// Branche neu eingegeben
	//echo "Bemerk.Kunde: $rem_kunde<br>";						// Bemerkung Kunde
	//echo "Einzug: $einzug<br>";								// Einzugsermächtigung erteilt
	//echo "Nichtkunde: $nichtkunde<br>";						// Nichtkunde
	//echo "verzogen: $verzogen<br>";							// Kunde ist verzogen
	//echo "doppelt: $doppelt<br>";								// Termin soll doppelt belegt werden
	//echo "NE: $ne<br>";										// Kunde wurde nicht erreicht
	//echo "Abschluss: $absch<br>";								// Termin soll abgeschlossen werden
	//echo "T-Handy: $thandy<br>";								// Handy-Termin
	//echo "Datenschutz: $datenschutz<br>";						// datenschutzerklärung erhalten?

   /* echo "<pre>";
    echo "Cookies:<br>";
    print_r($_COOKIE);
    echo "Session:<br>";
    print_r($_SESSION);
    echo "POST:<br>";
    print_r($_POST);
    echo "<br>";*/
    //exit;

	// Ende Debugging ------------------------------------------------------------------------------------------------------

	// Verifizierung der eingegebenen Daten ----------------------------------------------------------------------------------

	$muster_datum = "/^\d{2}$/";								// Test Datum, genau 2 Ziffern
	$muster_plz = "/^\d{5}$/";									// Test PLZ, genau 5 Ziffern
	$muster_zahl = "/^\d*$/";									// Test, ob nur Zahlen im String enthalten sind
	$muster_txt = "/^[^(0-9,',\\,\?,!,\s)]*$/";					// keine Ziffern, Hochkommata, Backslashes im String

	// Termin ----------------------------------------------------------------------------------------------------------------

	$termin_test = datum_check($t_tag, $t_monat, $t_jahr, $muster_datum, "Termin");
	if (!empty($termin_test)) {
		$bg_termin = $bg_fehler;
		$fehler = $fehler . $termin_test;
	}
	else {
		$zahl_test = 0;
		if (!empty($t_tag)) {
			$zahl_test = $zahl_test +1;
		}
		if (!empty($t_monat)) {
			$zahl_test = $zahl_test +1;
		}
		if (!empty($t_jahr)) {
			$zahl_test = $zahl_test +1;
		}
		if ($zahl_test == 3) {
			$termin_date = "20$t_jahr-$t_monat-$t_tag";
		}
	}

	// Wiedervorlage --------------------------------------------------------------------------------------------------------

	$termin_test = datum_check($wvl_tag, $wvl_monat, $wvl_jahr, $muster_datum, "WVL");
	if (!empty($termin_test)) {
		$bg_wvl = $bg_fehler;
		$fehler = $fehler . $termin_test;
	}
		else {
		$zahl_test = 0;
		if (!empty($wvl_tag)) {
			$zahl_test = $zahl_test +1;
		}
		if (!empty($wvl_monat)) {
			$zahl_test = $zahl_test +1;
		}
		if (!empty($wvl_jahr)) {
			$zahl_test = $zahl_test +1;
		}
		if ($zahl_test == 3) {
			$wvl_date = "20$wvl_jahr-$wvl_monat-$wvl_tag";
		}
	}

	// von-Datum alte Gesellschaft --------------------------------------------------------------------------------------

	$termin_test = datum_check($v_tag, $v_monat, $v_jahr, $muster_datum, "von");
	if (!empty($termin_test)) {
		$bg_vom = $bg_fehler;
		$fehler = $fehler . $termin_test;
	}

	// bis-Datum alte Gesellschaft --------------------------------------------------------------------------------------

	$termin_test = datum_check($b_tag, $b_monat, $b_jahr, $muster_datum, "bis");
	if (!empty($termin_test)) {
		$bg_bis = $bg_fehler;
		$fehler = $fehler . $termin_test;
	}

	// seit-Datum neue Gesellschaft - nur für Administrator -------------------------------------------------------------

		if ($gruppe == 'Administrator') {

			$termin_test = datum_check($s_tag, $s_monat, $s_jahr, $muster_datum, "seit");
				if (!empty($termin_test)) {
				$bg_seit = $bg_fehler;
				$fehler = $fehler . $termin_test;
			}
		}

	// geboren --------------------------------------------------------------------------------------------------------

	$termin_test = datum_check($g_tag, $g_monat, $g_jahr, $muster_datum, "geboren");
	if (!empty($termin_test)) {
		$bg_geboren = $bg_fehler;
		$fehler = $fehler . $termin_test;
	}

	/*
	// Vorname -----------------------------------------------------------------------------------------------------

	if (!empty($vorname)) {													// Überprüfung Vorname
		if(!preg_match($muster_txt, $vorname)) {							// Muster Text passt nicht
			$fehler = $fehler . "- Vorname";
			$bg_vorname = "#ff9966";
		}
	}

	// Name --------------------------------------------------------------------------------------------------------

	if (!empty($name)) {														// Überprüfung Name
		if(!preg_match($muster_txt, $name)) {								// Muster Text passt nicht
			$fehler = $fehler . "- Name";
			$bg_name = "#ff9966";
		}
	}

	// Ort neu (PLZ vorhanden) --------------------------------------------------------------------------------------

	if (!empty($ort_input)) {												// Überprufung Ort
		if(!preg_match($muster_txt, $ort_input)) {							// Muster Text passt nicht
			$fehler = $fehler . "- Ort";
			$bg_ort = "#ff9966";
		}
	}

	// Ortsteil neu (PLZ vorhanden) ----------------------------------------------------------------------------------

	if (!empty($ot_input)) {													// Überprufung Ortsteil
		if(!preg_match($muster_txt, $ot_input)) {							// Muster Text passt nicht
			$fehler = $fehler . "- Ortsteil";
			$bg_ot = "#ff9966";
		}
	}
	*/
	// neue PLZ --------------------------------------------------------------------------------------------------------

	if (!empty($plz_input) OR ($plz_input == '0')) {												// Überprufung neue PLZ
		if(!preg_match($muster_plz, $plz_input)) {							// Muster PLZ passt nicht
			$fehler = $fehler . "- PLZ-neu";
			$bg_plz = $bg_fehler;
		}
	}

	// Vorwahl1 --------------------------------------------------------------------------------------------------------

	if (!empty($vorwahl1)) {												// Überprufung Vorwahl1
		if(!preg_match($muster_zahl, $vorwahl1)) {							// Muster Zahl passt nicht
			$fehler = $fehler . "- Vorwahl1";
			$bg_vorwahl1 = $bg_fehler;
		}
	}

	// Telefon --------------------------------------------------------------------------------------------------------

	if (!empty($telefon)) {													// Überprufung Telefon
		if(!preg_match($muster_zahl, $telefon)) {							// Muster Zahl passt nicht
			$fehler = $fehler . "- Telefon";
			$bg_telefon = $bg_fehler;
		}
		elseif (empty($vorwahl1)) {
			$fehler = $fehler . " Vorwahl für Telefon fehlt";
			$bg_vorwahl1 = $bg_fehler;
		}
	}

	// Vorwahl2 --------------------------------------------------------------------------------------------------------

	if (!empty($vorwahl2)) {												// Überprufung Vorwahl2
		if(!preg_match($muster_zahl, $vorwahl2)) {							// Muster Zahl passt nicht
			$fehler = $fehler . "- Vorwahl2";
			$bg_vorwahl2 = $bg_fehler;
		}
	}

	// Fax --------------------------------------------------------------------------------------------------------

	if (!empty($fax)) {														// Überprufung Fax
		if(!preg_match($muster_zahl, $fax)) {								// Muster Zahl passt nicht
			$fehler = $fehler . "- Telefon2";
			$bg_fax = $bg_fehler;
		}
		elseif (empty($vorwahl2)) {
			$fehler = $fehler . " Vorwahl für Telefon 2 fehlt";
			$bg_vorwahl2 = $bg_fehler;
		}
	}

	if (!empty($fehler)) {													// es gab Fehler bei der Eingabe der Formularfelder
		$fehler = "&nbsp;Fehler bei Eingabe:&nbsp;" . $fehler;
		$norefresh = 1;														// Steuerelement für Refresh ausgeschaltet

	}
} // Ende Daten werden überprüft ----------------------------------------------------------------------------------------------------------


// Testen wurde gedrückt - Test, ob Telefonnummer bereits vergeben ist ---------------------------------------------------------------------
// erfordert Eingabe nur von Vorwahl und Telefonnummer -------------------------------------------------------------------------------------

if (isset($test)) {

	if (!empty($fehler)) {													// es gab Fehler bei der Eingabe der Formularfelder
		$fehler = "&nbsp;Fehler bei Eingabe:&nbsp;" . $fehler;
	}

	elseif (empty($vorwahl1) OR empty($telefon)) { 	// Minimaleingaben unvollständig
		$fehler = "Sie müssen Vorwahl1 UND Telefon eingeben!!";
	}

	else {	//Abfrage, ob Telefonnummer im Vorwahlgebiet bereits vergeben ist
		$sql = "SELECT name, vorname, strasse, kunden_id FROM kunden, name, vorname, vorwahl1 ";
		$sql .= " WHERE kunden.vorwahl1_id = vorwahl1.vorwahl1_id AND vorwahl1.vorwahl1 = '$vorwahl1' AND telefon = '$telefon' ";
		$sql .= " AND kunden.name_id = name.name_id AND kunden.vorname_id = vorname.vorname_id ";
		$query = myqueryi($db, $sql);
		$ergebnis = mysqli_fetch_array($query);

		if ($ergebnis) {
			$fehler = "Diese Telefonnummer ist an den Kunden $ergebnis[vorname] $ergebnis[name], $ergebnis[strasse] (ID: $ergebnis[kunden_id]) vergeben!";
		}
		else {
			$fehler = "Diese Telefonnummer ist noch nicht vergeben!";
		}
	}
	unset ($t_id);					// Termin-ID wird zurückgesetzt, damit beim Speichern kein Unsinn passiert (Eingabevariablen sind nicht mehr vorhanden!)!
}


// Speichern wurde gedrückt - Daten werden verarbeitet -------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------------------------

if (isset($speichern)) {

	$timestamp = date("YmdHis");

	if (!isset($telefonist) OR empty($telefonist) OR !isset($gruppe) OR empty($gruppe)) {
		$ausgeloggt = "Sie sind ausgeloggt!! Bitte einloggen und Termin erneut speichern!!";
		$norefresh = 1;
	}
	else {

	if (!empty($fehler)) {													// es gab Fehler bei der Eingabe der Formularfelder
		$fehler = $fehler;
		$norefresh = 1;
	}
	elseif (empty($name) OR empty($vorname) OR empty($vorwahl1) OR empty($telefon) OR empty($strasse) 	// Minimaleingaben unvollständig
			OR (empty($plz) AND empty($plz_input))  OR (empty($ort) AND empty($ort_input) AND empty($ort_input1))   ) {
			$fehler = "Sie müssen mindestens Vorname, Name, PLZ, Ort, Straße, Vorwahl1 und Telefon eingeben!!";
			$norefresh = 1;
	}
	else {																	// keine Fehler bei der Auswertung des Formulars, Minimaleingaben vollständig
																			// Auswertung geht weiter

		if ($absch == 1) {													// Termin soll abgeschlossen werden - alle anderen Steuerbuttons werden wirkungslos
			$alt = 0;
			$nichtkunde = 0;
			$verzogen = 0;

		}

		$norefresh = 0;

		// die Kundendaten werden verarbeitet --------------------------------------------------------------------
		// --------------------------------------------------------------------------------------------------------

		// Vorname -------------------------------------------------------------------------------------------------
		$sql1 = "SELECT vorname_id FROM vorname WHERE vorname = '$vorname'";	// Vorname schon vorhanden?
		$sql2 = "INSERT INTO vorname (vorname) VALUES ('$vorname')";			// Vorname neu
		$vorname_id = id_ermitteln($db, $sql1, $sql2);

		// Name ----------------------------------------------------------------------------------------------------
		$sql1 = "SELECT name_id FROM name WHERE name = '$name'";				// Name schon vorhanden?
		$sql2 = "INSERT INTO name (name) VALUES ('$name')";					// Name neu
		$name_id = id_ermitteln($db, $sql1, $sql2);

		// Vorwahl1 ------------------------------------------------------------------------------------------------
		$sql1 = "SELECT vorwahl1_id FROM vorwahl1 WHERE vorwahl1 = '$vorwahl1'";// Vorwahl1 schon vorhanden
		$sql2 = "INSERT INTO vorwahl1 (vorwahl1) VALUES ('$vorwahl1')";			// Vorwahl1 neu
		$vorwahl1_id = id_ermitteln($db, $sql1, $sql2);

		// Vorwahl 2 ----------------------------------------------------------------------------------------------------
		if (empty($vorwahl2)) {														// Feld Vorwahl2 ist leer
			$vorwahl2_id = "1";
		}
		else {
			$sql1 = "SELECT vorwahl2_id FROM vorwahl2 WHERE vorwahl2 = '$vorwahl2'";	// Vorwahl2 schon vorhanden?
			$sql2 = "INSERT INTO vorwahl2 (vorwahl2) VALUES ('$vorwahl2')";				// Vorwahl2 neu
			$vorwahl2_id = id_ermitteln($db, $sql1, $sql2);
		}

		// geboren ----------------------------------------------------------------------------------------------------
		if (empty($g_tag) OR empty($g_monat) OR empty($g_jahr)) { $geboren = "0000-00-00"; }
		elseif ($g_jahr > 20) { $geboren = "19$g_jahr-$g_monat-$g_tag"; }
		else { $geboren = "20$g_jahr-$g_monat-$g_tag"; }

		// Branche ------------------------------------------------------------------------------------------------------
		// eine Eingabe im Textfeld überschreibt eine Auswahl im Select Feld!! ------------------------------------------
		if (empty($branche_input)) {											// keine Eingabe im Textfeld - Selectfeld zählt!

			if (empty($branche)) {												// auch Select-Feld ist leer
				$branche_id = "1";
			}
			else {
				$sql = "SELECT branche_id FROM branche WHERE branche = '$branche'";
				$query = myqueryi($db, $sql);
				$ergebnis = mysqli_fetch_array($query);
				$branche_id = $ergebnis[0];
			}
		}																				// ende if keine Eingabe im Textfeld
		else {																			// im textfeld steht was
			$sql1 = "SELECT branche_id FROM branche WHERE branche = '$branche_input'";	// Branche schon vorhanden?
			$sql2 = "INSERT INTO branche (branche) VALUES ('$branche_input')";			// Branche neu
			$branche_id = id_ermitteln($db, $sql1, $sql2);
		}

		// PLZ -------------------------------------------------------------------------------------------------------------
		// die neu eingegebene PLZ überschreibt die PLZ aus dem Select-Feld ------------------------------------------------
		if (empty($plz_input)) {										// PLZ aus Select zählt!
			$sql = " SELECT plz_id FROM plz WHERE plz = '$plz' ";
			$query = myqueryi($db, $sql);
			$ergebnis = mysqli_fetch_array($query);
			$plz_id = $ergebnis[0];
		}
		else {															// neue PLZ zählt!!
			$sql1 = "SELECT plz_id FROM plz WHERE plz = '$plz_input'";	// PLZ evtl doch schon vorhanden
			$sql2 = "INSERT INTO plz (plz) VALUES ('$plz_input')";		// PLZ komplett neu
			$plz_id = id_ermitteln($db, $sql1, $sql2);
		}

		// Ort ------------------------------------------------------------------------------------------------------------
		// Reihenfolge der Abarbeitung: Ort für neue PLZ --> neuer Ort für alte PLZ --> Ort aus Select-Feld
		if (!empty($ort_input1)) {										// Ort für neue PLZ eingetragen
			$sql1 = "SELECT ort_id FROM ort WHERE ort = '$ort_input1'";	// Ort evtl doch schon vorhanden
			$sql2 = "INSERT INTO ort (ort) VALUES ('$ort_input1')";		// Ort komplett neu
			$ort_id = id_ermitteln($db, $sql1, $sql2);
		}
		elseif (!empty($ort_input)) {									// neuer Ort für PLZ aus Select-Feld eingetragen
			$sql1 = "SELECT ort_id FROM ort WHERE ort = '$ort_input'";	// Ort evtl doch schon vorhanden
			$sql2 = "INSERT INTO ort (ort) VALUES ('$ort_input')";		// Ort komplett neu
			$ort_id = id_ermitteln($db, $sql1, $sql2);
		}
		else {															// Ort kommt aus Select-Feld
			$sql = " SELECT ort_id FROM ort WHERE ort = '$ort' ";
			$query = myqueryi($db, $sql);
			$ergebnis = mysqli_fetch_array($query);
			$ort_id = $ergebnis[0];
		}

		// Ortsteil -----------------------------------------------------------------------------------------------------------------
		// Reihenfolge der Abarbeitung: Ortsteil für neue PLZ --> neuer Ortsteil für alte PLZ --> Ortsteil aus Select-Feld --> ot_id=1
		if (!empty($ot_input1)) {														// Ortsteil für neue PLZ eingetragen
			$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil = '$ot_input1'";	// Ortsteil evtl doch schon vorhanden
			$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ot_input1')";			// Ortsteil komplett neu
			$ortsteil_id = id_ermitteln($db, $sql1, $sql2);
		}
		elseif (!empty($ot_input)) {													// neuer Ortsteil für PLZ aus Select-Feld eingetragen
			$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil = '$ot_input'";	// Ortsteil evtl doch schon vorhanden
			$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ot_input')";				// Ortsteil komplett neu
			$ortsteil_id = id_ermitteln($db, $sql1, $sql2);
		}
		elseif (!empty($ot)) {															// Ortsteil kommt aus Select-Feld
			$sql = " SELECT ortsteil_id FROM ortsteil WHERE ortsteil = '$ot' ";
			$query = myqueryi($db, $sql);
			$ergebnis = mysqli_fetch_array($query);
			$ortsteil_id = $ergebnis[0];
		}
		else {																			// überhaupt kein Ortsteil eingetragen
			$ortsteil_id = '1';
		}

		// poo - Kombination von PLZ/Ort/Ortsteil ----------------------------------------------------------------------------------
		$sql = "SELECT poo_id FROM poo WHERE plz_id = '$plz_id' AND ort_id = '$ort_id' AND ortsteil_id = '$ortsteil_id'"; // poo schon vorhanden?
		$query = myqueryi($db, $sql);
		$ergebnis = mysqli_fetch_array($query);

		if ($ergebnis) {																						// ja!
			$poo_id = $ergebnis[0];
		}
		else {																									// nein!
			$sql = "INSERT INTO poo (plz_id, ort_id, ortsteil_id) ";
			$sql .= "VALUES ('$plz_id', '$ort_id', '$ortsteil_id') ";
			$query = myqueryi($db, $sql);
			$poo_id = mysqli_insert_id($db);																		// neue poo-id eingefügt
		}


// keine Kunden-ID oder Termin_id gesetzt - neuer Kunde --------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------

		if (empty($kunden_id)) {

			//Abfrage, ob Telefonnummer bereits vergeben ist
			$sql = "SELECT name, vorname, strasse, kunden_id, rem_kunde FROM kunden, name, vorname, vorwahl1 ";
			$sql .= " WHERE kunden.vorwahl1_id = vorwahl1.vorwahl1_id AND telefon = '$telefon' ";
			$sql .= " AND kunden.name_id = name.name_id AND kunden.vorname_id = vorname.vorname_id ";
			$query = myqueryi($db, $sql);
			$ergebnis = mysqli_fetch_array($query);
			$rem_kunde1 = $rem_kunde . " ($speich: $telefonist)";

			$kunden_id = $ergebnis[kunden_id];											// ID des gefundenen Kunden, wird evtl. geändert
			if ($ergebnis) {															// Telefonnummer bereits vergeben
				if ($ergebnis[strasse] == $strasse) {									// Telefon ist auf Adresse geblieben
					if ($ergebnis[name] == $name AND $ergebnis[vorname] == $vorname) {	// Bestandskunde, da Name und Vorname stimmen
						if ($nichtkunde == 1) {											// nichtkunde
							$sql = "UPDATE kunden SET vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
							$sql .= "geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1', ";
							$sql .= "nichtkunde = '1', neukunde = '0', belegt = '0' ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);

							$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);	// alle Wiedervorlagen müssen auf "alt" gesetzt werden

							$fehler = "Dieser Kunde ist bereits vorhanden (ID: $kunden_id) und wurde auf NICHTKUNDE gesetzt -";
							$fehler = $fehler . "<br />Alle Wiedervorlagen wurden storniert";
						}	// Ende Nichtkunde
						elseif ($ne == 1) {												// nicht erreicht
							$sql = "UPDATE kunden SET vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
							$sql .= "geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1', ";
							$sql .= "ne = '$heute' ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);
							$fehler = "Dieser Kunde ist bereits vorhanden (ID: $kunden_id), wurde aber nicht erreicht";
						}	// Ende nicht erreicht
						elseif ($verzogen == 1) {											// unbekannt verzogen
							$sql = "UPDATE kunden SET telefon = '000000', vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
							$sql .= "geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1', ";
							$sql .= "nichtkunde = '1', neukunde = '0', belegt = '0', verzogen = '1' ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);

							$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);	// alle Wiedervorlagen müssen auf "alt" gesetzt werden

							$fehler = "Der Bestandskunde (ID: $kunden_id) ist verzogen -";
							$fehler = $fehler . "<br />Alle Wiedervorlagen wurden storniert";
						}	// Ende Nichtkunde
						else {															// Kundendaten werden geändert
							$sql = "UPDATE kunden SET vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
							$sql .= "geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1' ";
							$sql .= "WHERE kunden_id = '$kunden_id'";
							$query = myqueryi($db, $sql);
							$fehler = "Dieser Kunde ist bereits vorhanden (ID: $kunden_id) - die Daten wurden geändert";
						}
					}																	// Ende if Bestandskunde
			else {																// neuer Kunde in Wohnung von Bestandkunden!!

				// vorher dort wohnender Kunde wird als "verzogen" markiert!!
				//$rem_kunde_alt = $ergebnis[rem_kunde] . " VERZOGEN!!";

				//$sql = "UPDATE kunden SET telefon = '000000', nichtkunde = '1', neukunde = '0', verzogen = '1', rem_kunde = '$rem_kunde_alt' WHERE kunden_id = '$kunden_id' ";
				//$query = myqueryi($db, $sql);
				//$kunden_id_verzogen = $kunden_id;

				//$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);	// alle Wiedervorlagen müssen auf "alt" gesetzt werden

				// ein neuer Kunde wird eingefügt
				// Problem - der neue Kunde muss vorher auch irgendwo gewohnt haben - wo????

				if ($nichtkunde == 1) {
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, fax, ";
					$sql .= " email, rem_kunde, branche_id, geboren, neukunde, nichtkunde) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$fax', ";
					$sql .= " '$email', '$rem_kunde1', '$branche_id', '$geboren', '0', '1') ";
					$query = myqueryi($db, $sql);
					$kunden_id = mysqli_insert_id($db);
					$fehler = "Ein neuer Kunde (ID: $kunden_id) wurde als NICHTKUNDE eingefügt"; //, der vorher dort wohnende
						      // $ergebnis[vorname] $ergebnis[name] (ID: $kunden_id_verzogen) ist verzogen, alle Wiedervorlagen wurden storniert!";
				}
				elseif ($ne == 1) {
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, fax, ";
					$sql .= " email, rem_kunde, branche_id, geboren, neukunde, ne) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$fax', ";
					$sql .= " '$email', '$rem_kunde1', '$branche_id', '$geboren', '1', '$heute') ";
					$query = myqueryi($db, $sql);
					$kunden_id = mysqli_insert_id($db);
					$fehler = "Ein neuer Kunde (ID: $kunden_id) wurde eingefügt aber nicht angetroffen"; //, der vorher dort wohnende
						     // $ergebnis[vorname] $ergebnis[name] (ID: $kunden_id_verzogen) ist verzogen, dessen Wiedervorlagen wurden storniert!";
				}
				else {
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, fax, ";
					$sql .= " email, rem_kunde, branche_id, geboren, neukunde) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$fax', ";
					$sql .= " '$email', '$rem_kunde1', '$branche_id', '$geboren', '1') ";
					$query = myqueryi($db, $sql);
					$kunden_id = mysqli_insert_id($db);
					$fehler = "Ein neuer Kunde (ID: $kunden_id) wurde eingefügt"; //, der vorher dort wohnende
						              // $ergebnis[vorname] $ergebnis[name] (ID: $kunden_id_verzogen) ist verzogen, seine Wiedervorlagen wurden storniert!";
				}
			}	// Ende neuer Kunde in Wohnung von Bestandskunde

		} 	// Ende if Kunde bereits vorhanden

				else {	// Kunde ist verzogen und hat Telefonnummer mitgenommen

					if ($ergebnis[name] == $name AND $ergebnis[vorname] == $vorname) {	// Name und Vorname stimmen, Bestandskunde ist verzogen
						$sql = "UPDATE kunden SET strasse = '$strasse', poo_id = '$poo_id', vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
						$sql .= "geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1' ";

						if ($nichtkunde == 1) {
							$sql .= ", nichtkunde = '1', neukunde = '0' ";

							//$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);	// alle Wiedervorlagen müssen auf "alt" gesetzt werden
						}
						if ($ne == 1) {
							$sql .= ", ne = '$heute' ";
						}
						$sql .= "WHERE kunden_id = '$kunden_id'";
						$query = myqueryi($db, $sql);
						$fehler = "Der Bestandskunde $ergebnis[vorname] $ergebnis[name] (ID: $kunden_id) ist umgezogen!";
					}																	// Ende if Bestandskunde ist verzogen

					else {																// neuer Kunde in neuer Wohnung mit vorhandener Tel. Nr.
						// vorher dort wohnender Kunde wird als "verzogen" markiert!!
						/*
						$rem_kunde_alt = $ergebnis[rem_kunde] . " VERZOGEN!!";
						$sql = "UPDATE kunden SET telefon = '000000', neukunde = '0', nichtkunde = '1', verzogen = '1', rem_kunde = '$rem_kunde_alt' WHERE kunden_id = '$kunden_id' ";
						$query = myqueryi($db, $sql);
						$kunden_id_verzogen = $kunden_id;

						$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);	// alle Wiedervorlagen müssen auf "alt" gesetzt werden
						*/
						// ein neuer Kunde wird eingefügt
						// Problem - der neue Kunde muss vorher auch irgendwo gewohnt haben - wo????

						if ($nichtkunde == 1) {
							$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, fax, ";
							$sql .= " email, rem_kunde, branche_id, geboren, neukunde, nichtkunde) ";
							$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$fax', ";
							$sql .= " '$email', '$rem_kunde1', '$branche_id', '$geboren', '0', '1') ";
							$query = myqueryi($db, $sql);
							$kunden_id = mysqli_insert_id($db);
							$fehler = "Ein neuer Kunde (ID: $kunden_id) wurde als NICHTKUNDE eingefügt ";
							          // der vorher die Tel.-Nr. ($vorwahl1) $telefon innehabende
						              // $ergebnis[vorname] $ergebnis[name] ist verzogen, alle Wiedervorlagen wurden storniert!";
						}
						elseif ($ne == 1) {
							$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, fax, ";
							$sql .= " email, rem_kunde, branche_id, geboren, neukunde, ne) ";
							$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$fax', ";
							$sql .= " '$email', '$rem_kunde1', '$branche_id', '$geboren', '1', '$heute') ";
							$query = myqueryi($db, $sql);
							$kunden_id = mysqli_insert_id($db);
							$fehler = "Ein neuer Kunde (ID: $kunden_id) wurde eingefügt aber nicht angetroffen";
									 //  der vorher die Tel.-Nr. ($vorwahl1) $telefon innehabende
						             //  $ergebnis[vorname] $ergebnis[name] ist verzogen, alle Wiedervorlagen wurden storniert!";
						}
						else {
							$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, fax, ";
							$sql .= " email, rem_kunde, branche_id, geboren, neukunde, datenschutz) ";
							$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$fax', ";
							$sql .= " '$email', '$rem_kunde1', '$branche_id', '$geboren', '1', $datenschutz) ";
							$query = myqueryi($db, $sql);
							$kunden_id = mysqli_insert_id($db);
							$fehler = "Ein neuer Kunde $vorname $name wurde eingefügt (ID: $kunden_id)"; //, der vorher die Tel.-Nr. ($vorwahl1) $telefon innehabende
						        //  $ergebnis[vorname] $ergebnis[name] ist verzogen, alle Wiedervorlagen wurden storniert!";
						}
					}																	// Ende else neuer Kunde in neuer Wohnung mit vorhandener Tel. Nr.
				} 																		// Ende else Kunde ist verzogen und hat Telefonnummer mitgenommen
			}																			// Ende if Telefonnummer bereits vergeben

			else {																		// Telefonnummer nicht vorhanden - echt neuer Kunde
				if ($nichtkunde == 1) {
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon,  ";
					$sql .= " vorwahl2_id, fax, email, rem_kunde, branche_id, geboren, neukunde, nichtkunde) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', ";
					$sql .= " '$vorwahl2_id', '$fax', '$email', '$rem_kunde1', '$branche_id', '$geboren', '0', '1') ";			// die Checkboxen folgen!!
					$query = myqueryi($db, $sql);
					$kunden_id = mysqli_insert_id($db);
					$fehler = "Ein neuer Kunde $vorname $name (ID: $kunden_id) wurde als NICHTKUNDE eingefügt!";
				}
				elseif ($ne == 1) {
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon,  ";
					$sql .= " vorwahl2_id, fax, email, rem_kunde, branche_id, geboren, neukunde, ne) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', ";
					$sql .= " '$vorwahl2_id', '$fax', '$email', '$rem_kunde1', '$branche_id', '$geboren', '1', $heute') ";			// die Checkboxen folgen!!
					$query = myqueryi($db, $sql);
					$kunden_id = mysqli_insert_id($db);
					$fehler = "Ein neuer Kunde $vorname $name (ID: $kunden_id) wurde eingefügt, aber nicht erreicht!";
				}
				else {
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon,  ";
					$sql .= " vorwahl2_id, fax, email, rem_kunde, branche_id, geboren, neukunde, datenschutz) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', ";
					$sql .= " '$vorwahl2_id', '$fax', '$email', '$rem_kunde1', '$branche_id', '$geboren', '1', $datenschutz) ";			// die Checkboxen folgen!!
					$query = myqueryi($db, $sql);
					$kunden_id = mysqli_insert_id($db);
					$fehler = "Ein neuer Kunde $vorname $name (ID: $kunden_id) wurde eingefügt!";
				}
			}	// ende else Telefonnummer nicht vorhanden - echt neuer Kunde
		}	// Ende if $t_id UND $kunden_id leer - neuer Kunde -------------


// Kunden-ID oder Termin_id gesetzt - Kunde bereits vorhanden --------------------------------------------------
// durch Änderung der Angaben werden gespeicherte Angaben geändert / fehlerhafte Eingaben korrigiert ------------
// Bestandskunde, eventuell umgezogen

		else { // kunden_id gesetzt
			$rem_kunde1 = $rem_kunde . " ($speich: $telefonist)";

			if ($new == 0) {			// Button "Kunde neu" nicht gedrückt, Daten des Bestandskunden sollen geändert werden

				if ($kunde[nichtkunde] == 0 AND $nichtkunde == 1) {												// Kunde wird zum Nichtkunden
					$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$strasse', ";
					$sql .= " vorwahl1_id = '$vorwahl1_id', vorwahl2_id = '$vorwahl2_id', fax = '$fax', branche_id = '$branche_id', ";
					$sql .= " geboren = '$geboren', email = '$email', wiedervorlage = '0', rem_kunde = '$rem_kunde1', nichtkunde = '1', neukunde = '0', belegt = '0', einzug = '0', datenschutz = '$datenschutz' ";
					$sql .= " WHERE kunden_id = '$kunden_id'";
					$query = myqueryi($db, $sql);
					$fehler = "Der Bestandskunde $vorname $name (ID: $kunden_id) wurde zum Nichtkunden.";

					if (empty($t_id)) {
						// alle offenen Termine bearbeiten
						$termin_nk = termin_nk($kunden_id, $heute, $speich, $telefonist);

						// alle offenen Wiedervorlagen müssen auf "alt" gesetzt werden
						$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);
						$fehler = $fehler . "<br />alle offenen Termine wurden storniert<br />alle Wiedervorlagen wurden abgeschlossen!!";
					}
				}
				elseif ($kunde[nichtkunde] == 1 AND $nichtkunde == 0) {											// Nichtkunde wird reaktiviert
					$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$strasse', ";
					$sql .= " vorwahl1_id = '$vorwahl1_id', telefon = '$telefon', vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
					$sql .= " geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1', nichtkunde = '0', neukunde = '0', datenschutz = '$datenschutz'  ";
					$sql .= " WHERE kunden_id = '$kunden_id'";
					$query = myqueryi($db, $sql);
					$fehler = "Der Nichtkunde $vorname $name (ID: $kunden_id) wurde wieder zum Bestandskunden.";
				}
				elseif ($ne == 1) {																				// Kunde nicht erreicht
					$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$strasse', ";
					$sql .= " vorwahl1_id = '$vorwahl1_id', vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
					$sql .= " geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1', ne = '$heute' ";
					$sql .= " WHERE kunden_id = '$kunden_id'";
					$query = myqueryi($db, $sql);
					$fehler = "Der Bestandskunde $vorname $name (ID: $kunden_id) wurde nicht angetroffen";
				}
				elseif ($ne == 0 AND $kunde[ne] === $heute) {													// Kunde wieder erreicht
					$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$strasse', ";
					$sql .= " vorwahl1_id = '$vorwahl1_id', vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
					$sql .= " geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1', ne = '' , , datenschutz = '$datenschutz' ";
					$sql .= " WHERE kunden_id = '$kunden_id'";
					$query = myqueryi($db, $sql);
					$fehler = "Der Bestandskunde $vorname $name (ID: $kunden_id) wurde angetroffen";
				}
				elseif ($kunde[verzogen] == 0 AND $verzogen == 1) {												// Kunde ist verzogen
					$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$strasse', ";
					$sql .= " vorwahl1_id = '$vorwahl1_id', telefon = '000000', vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
					$sql .= " geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1', verzogen = '1', nichtkunde = '1', neukunde = '0', belegt = '0', einzug = '0' ";
					$sql .= " WHERE kunden_id = '$kunden_id'";
					$query = myqueryi($db, $sql);
					$fehler = "Der Bestandskunde $vorname $name (ID: $kunden_id) ist verzogen!";
					//$fehler = $fehler . "<br />Alle Wiedervorlagen wurden storniert";

					//$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);	// alle Wiedervorlagen müssen auf "alt" gesetzt werden

				}
				elseif ($kunde[verzogen] == 1 AND $verzogen == 0) {												// Kundenadresse ist wieder aktuell
					$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$strasse', ";
					$sql .= " vorwahl1_id = '$vorwahl1_id', telefon = '$telefon', vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
					$sql .= " geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1', verzogen = '0', nichtkunde = '0', , datenschutz = '$datenschutz'  ";
					$sql .= " WHERE kunden_id = '$kunden_id'";
					$query = myqueryi($db, $sql);
					$fehler = "Die Adresse des verzogenen Kunden $vorname $name (ID: $kunden_id) ist wieder aktuell!!";
				}
				else {																							// einfache Datenänderung
					$fehler = "Die Daten des Kunden $vorname $name (ID: $kunden_id) wurden geändert!";
					$sql = "UPDATE kunden SET vorname_id = '$vorname_id', name_id = '$name_id', poo_id = '$poo_id', strasse = '$strasse', ";
					$sql .= " vorwahl1_id = '$vorwahl1_id', telefon = '$telefon', vorwahl2_id = '$vorwahl2_id', fax = '$fax', ";
					$sql .= " geboren = '$geboren', email = '$email', branche_id = '$branche_id', rem_kunde = '$rem_kunde1', datenschutz = '$datenschutz'  ";
					$sql .= " WHERE kunden_id = '$kunden_id'";
					$query = myqueryi($db, $sql);
				}
			}							// Ende Button "Kunde neu" nicht gedrückt, Daten des Bestandskunden sollen geändert werden

			else {						// Button "Kunde neu" ist gedrückt - neuer Kunde - alter Kunde auf dieser Adresse/Tel.-Nr. ist verzogen

				// vorher dort wohnender Kunde wird als "verzogen" markiert!!
				$rem_kunde_alt = $kunde[rem_kunde] . " VERZOGEN!!";

				$sql = "UPDATE kunden SET telefon = '000000', neukunde = '0', nichtkunde = '1', verzogen = '1', rem_kunde1 = '$rem_kunde_alt', belegt = '0', einzug = '0' WHERE kunden_id = '$kunden_id' ";
				$query = myqueryi($db, $sql);
				$kunden_id_verzogen = $kunden_id;

				//$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);	// alle Wiedervorlagen müssen auf "alt" gesetzt werden

				// ein neuer Kunde wird eingefügt
				// Problem - der neue Kunde muss vorher auch irgendwo gewohnt haben - wo????

				if ($nichtkunde == 1) {
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, fax, ";
					$sql .= " email, rem_kunde, branche_id, geboren, neukunde, nichtkunde, datenschutz) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$fax', ";
					$sql .= " '$email', '$rem_kunde1', '$branche_id', '$geboren', '0', '1', $datenschutz) ";
					$query = myqueryi($db, $sql);
					$kunden_id = mysqli_insert_id($db);
					$fehler = "Ein neuer Kunde (ID: $kunden_id) wurde als Nichtkunde eingefügt, der vorher dort wohnende
				                $kunde[vorname] $kunde[name] (ID: $kunden_id_verzogen) ist verzogen, dessen Wiedervorlagen wurden storniert!";
				}
				elseif ($ne == 1) {
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, fax, ";
					$sql .= " email, rem_kunde, branche_id, geboren, neukunde, ne, datenschutz) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$fax', ";
					$sql .= " '$email', '$rem_kunde1', '$branche_id', '$geboren', '1', '$heute', $datenschutz) ";
					$query = myqueryi($db, $sql);
					$kunden_id = mysqli_insert_id($db);
					$fehler = "Ein neuer Kunde (ID: $kunden_id) wurde eingefügt, aber nicht angetroffen;<br /> der vorher dort wohnende
				         $kunde[vorname] $kunde[name] (ID: $kunden_id_verzogen) ist verzogen, dessen Wiedervorlagen wurden storniert!!";
				}
				else {
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, fax, ";
					$sql .= " email, rem_kunde, branche_id, geboren, neukunde, datenschutz) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$fax', ";
					$sql .= " '$email', '$rem_kunde1', '$branche_id', '$geboren', '1', $datenschutz) ";
					$query = myqueryi($db, $sql);
					$kunden_id = mysqli_insert_id($db);
					$fehler = "Ein neuer Kunde (ID: $kunden_id) wurde eingefügt,<br />der vorher dort wohnende
				               $kunde[vorname] $kunde[name] (ID: $kunden_id_verzogen) ist verzogen, dessen Wiedervorlagen wurden storniert!!";

				}
			}	// Ende Button "Kunde neu" gedrückt - neuer Kunde - alter Kunde auf dieser Adresse/Tel.-Nr. ist verzogen
		} // Ende else Kunden-ID oder Termin_id gesetzt - Kunde bereits vorhanden




// es folgt die Verarbeitung der Terminfelder --------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------------------------------
		if (empty($termin_date) AND empty($wvl_date) AND empty($t_id)) {												// keine Termindaten oder T_ID
			$fehler = $fehler . "<br />Es wurde kein Termin gespeichert!";
		}
		else {																										// Termindaten eingegeben oder T-ID gesetzt
			// Ermittlung der Produkt-alt-ID ---------------------------------------------------------
			// eingegebene Werte überschreiben die aus den Select-Feldern

			// Ermittlung der Gesellschaft_alt-ID ---------------------------------------------------------------------
			// --------------------------------------------------------------------------------------------------------

			if (empty($ges_alt) AND (empty($ges_alt_input))) {					// keine alten Gesellschaften eingegeben
				$tarif_alt_id = 1;
				$ges_alt_id = 1;
				$von = "0000-00-00";
				$bis = "0000-00-00";
			} // ende keine Gesellschaft_alt eingegeben
			elseif (empty($ges_alt_input)) {									// Input-Feld ist leer - Select-Feld zählt
				$sql = " SELECT ges_alt_id FROM ges_alt WHERE ges_alt = '$ges_alt' ";
				$query = myqueryi($db, $sql);
				$ergebnis = mysqli_fetch_array($query);
				$ges_alt_id = $ergebnis[0];
			}	// ende Input-Feld leer
			else {																// Input-Feld nicht leer - überschreibt Select-feld
				$sql1 = "SELECT ges_alt_id FROM ges_alt WHERE ges_alt = '$ges_alt_input' ";
				$sql2 = "INSERT INTO ges_alt (ges_alt) VALUES ('$ges_alt_input')";
				$ges_alt_id = id_ermitteln($db, $sql1, $sql2);
			}	// Ende Input-Feld nicht leer


			// Ermittlung der Tarif_alt-ID -----------------------------------------------------------------------------
			// ---------------------------------------------------------------------------------------------------------

			if (empty($tarif_alt) AND (empty($tarif_alt_input))) {					// keine alten Tarife eingegeben
				$tarif_alt_id = 1;
			} // ende keine alten Tarife eingegeben
			elseif (empty($tarif_alt_input)) {									// Input-Feld ist leer - Select-Feld zählt
				$sql = " SELECT tarif_alt_id FROM tarif_alt WHERE tarif_alt = '$tarif_alt' ";
				$query = myqueryi($db, $sql);
				$ergebnis = mysqli_fetch_array($query);
				$tarif_alt_id = $ergebnis[0];
			}	// ende Input-Feld leer
			else {																// Input-Feld nicht leer - überschreibt Select-feld
				$sql1 = "SELECT tarif_alt_id FROM tarif_alt WHERE tarif_alt = '$tarif_alt_input' ";
				$sql2 = "INSERT INTO tarif_alt (tarif_alt) VALUES ('$tarif_alt_input')";
				$tarif_alt_id = id_ermitteln($db, $sql1, $sql2);
			}	// Ende Input-Feld nicht leer


			// Ermittlung der Produkt_alt_id -------------------------------------------------------------------------------
			// -------------------------------------------------------------------------------------------------------------

			// wenn es noch keine produkt-id mit tarif-id=1 für diese Gesellschaft gibt, einfügen
			$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE ges_alt_id = '$ges_alt_id' AND tarif_alt_id = '1' ";
			$sql2 = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '1') ";
			$produkttest = id_ermitteln($db, $sql1, $sql2);

			$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE ges_alt_id = '$ges_alt_id' AND tarif_alt_id = '$tarif_alt_id' ";
			$sql2 = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '$tarif_alt_id') ";
			$produkt_alt_id = id_ermitteln($db, $sql1, $sql2);

			if (empty($v_jahr)) {												// kein von-Datum eingegeben
				$von = "0000-00-00";
			}
			else {
				$von = "20$v_jahr-$v_monat-$v_tag";
			}
			if (empty($b_jahr)) {												// kein bis-Datum eingegeben
				$bis = "0000-00-00";
			}
			else {
				$bis = "20$b_jahr-$b_monat-$b_tag";
			}

			// Ermittlung der Produkt_neu-ID --------------------------------------------------------------------------------------
			// --------------------------------------------------------------------------------------------------------------------

			if (empty($ges_neu)) {										// keine neue Gesellschaft eingegeben
				$produkt_neu_id = 1;
				$seit = "0000-00-00";
			}
			else {
				$sql = " SELECT ges_neu_id FROM ges_neu WHERE ges_neu = '$ges_neu' ";
				$query = myqueryi($db, $sql);
				$ergebnis = mysqli_fetch_array($query);
				$ges_neu_id = $ergebnis[0];

				if (empty($tarif_neu)) {
					$tarif_neu_id = 1;
				}
				else {
					$sql = " SELECT tarif_neu_id FROM tarif_neu WHERE tarif_neu = '$tarif_neu' ";
					$query = myqueryi($db, $sql);
					$ergebnis = mysqli_fetch_array($query);
					$tarif_neu_id = $ergebnis[0];
				}
				$sql = "SELECT produkt_neu_id FROM produkt_neu WHERE ges_neu_id = '$ges_neu_id' AND tarif_neu_id = '$tarif_neu_id' ";
				$query = myqueryi($db, $sql);
				$ergebnis = mysqli_fetch_array($query);
				$produkt_neu_id = $ergebnis[0];
				if (empty($s_jahr)) {									// kein Datum eingegeben
					$seit = "0000-00-00";
				}
				else {
					$seit = "20$s_jahr-$s_monat-$s_tag";
				}
			}



// keine Termin_id - es soll ein neuer termin eingegeben werden ----------------------------------------------------------------------------
// es kann nur ein neuer Termin oder ein neuer Wiedervorlagetermin angelegt werden ---------------------------------------------------------
// Achtung!! Außendienstler und Telefonist eintragen!! -------------------------------------------------------------------------------------

			if (empty($t_id)) {													// keine T-ID: neuer Termin soll eingegeben werden!

		// Wiedervorlage soll neu eingegeben werden ----------------------------------------------------------------------------------------
				if ($wvl == 1) {												// Button "Wiedervorlage" ist angeklickt - Wiedervorlage
					if (empty($aq)) {											// kein Telefonist ausgewählt - Telefonist ist eingeloggter User!!
						$aq = $telefonist;
					}

					if (empty($wvl_tag)) {										// kein Wiedervorlagetermin eingegeben
						$fehler = $fehler . "<br />Bitte WVL-Termin eingeben - Termin wurde nicht gespeichert.";
						$norefresh = 1;							// kein Refresh am Ende des Scriptes
					} // ende if kein Wiedervorlagetermin eingegeben

					else { // Wiedervorlagetermin eingegeben
							if ($wvl_date < $heute) {					// Wiedervorlagetermin in der Vergangenheit!!
								$fehler = $fehler . "<br />Die Wiedervorlage kann nicht in der Vergangenheit liegen! - Termin wurde nicht gespeichert.";
								$bg_wvl = $bg_fehler;							// Hintergrund in Fehler-Farbe
								$norefresh = 1;							// kein Refresh am Ende des Scriptes
							}
							else { // Wiedervorlage in der Zukunft
								$rem_termin = $rem_termin . " ($speich: $telefonist) ";
								$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, von, bis, produkt_neu_id, seit, ";
								$sql .= "rem_termin, aquiriert, erledigt_date, vonr, termin.storno, sperrzeit, wiedervorlage_date, w_zeit,  ";
								$sql .= "nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, edit) ";
								$sql .= "VALUES ('0000-00-00', '00:00', '$kunden_id', '$aq', '', '$produkt_alt_id', '$von', '$bis', '1', '', ";
								$sql .= "'$rem_termin', '$aquiriert', '0000-00-00', '$vonr', '0', '0', '$wvl_date', '$wvl_zeit',  ";
								$sql .= "'0', '0', '1', '0', '0', '0', '$timestamp') ";
								$query = myqueryi($db, $sql);
								$t_id = mysqli_insert_id($db);

								$sql = "UPDATE kunden SET wiedervorlage = '1', nichtkunde = '0', neukunde = '0' WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);

								$fehler = $fehler . "<br /> Ein neuer Wiedervorlagetermin mit der ID: $t_id wurde gespeichert";
								$norefresh = 0;

							} // Ende else Wiedervorlage in der Zukunft
						} // ende else Wiedervorlagetermin vollständig
					}
		// ende neue Wiedervorlage -------------------------------------------------------------------------------------------------------

		// neuer Termin soll eingegeben werden

				else {
					if (empty($t_tag) OR empty($t_monat) OR empty($t_jahr) OR empty($t_zeit)) {					// Termin nicht vollständig
						$fehler = $fehler . "<br />Termin-Datum nicht vollständig! - Termin wurde nicht gespeichert.";
						$norefresh = 1;							// kein Refresh am Ende des Scriptes
					} // ende if Termin nicht vollständig
					elseif (empty($ad)) {									// ein Außendienstler muss zwingend angegeben werden!!
						$fehler = $fehler . "<br />Bitte Außendienstler eingeben! - Termin wurde nicht gespeichert.";
						$norefresh = 1;							// kein Refresh am Ende des Scriptes
					}	// ende keine AD
					else { // Termin vollständig und AD eingegeben
						if ($gruppe == 'Aquise') {
							$aq = $telefonist;												// fär eingeloggten Telefonist wird das eigene Benutzerkärzel als $aq verwendet
						}
						if (empty($aq) AND $gruppe == 'Administrator' AND $kalt == '0') {		// Kein telefonist ausgewählt und kein kalter Termin
							$fehler = $fehler . "<br />Bitte Telefonist auswählen! - Termin wurde nicht gespeichert.";
							$norefresh = 1;							// kein Refresh am Ende des Scriptes
						}
						else {

							// Überprufung, ob Termin bereits belegt ist

							$sql = "SELECT termin_id FROM termin WHERE termin = '$termin_date' AND zeit = '$t_zeit' AND aussendienst = '$ad' AND alt = '0' ";		// ist der Termin schon belegt?
							$query = myqueryi($db, $sql);
							$anz_doppelt = mysqli_num_rows($query);

							if (($anz_doppelt > 0) AND ($doppelt == 0) AND ($kalt == 0)) {					//Termin soll doppelt belegt werden, ist aber kein kalter Termin!
								$fehler = $fehler . "<br />Fehler: Der Termin ist bereits belegt - soll er mehrfach belegt werden bitte \"2x\" anklicken!!";
								$doppler = '1';			// wird im Formular ausgewertet - Zeile Doppelt = Rot
								$norefresh = 1;							// kein Refresh am Ende des Scriptes
							}
							else {	// Termin ist noch frei oder soll doppelt belegt werden
								$norefresh = 0;
				// kalter Abschluss
								if ($produkt_neu_id > 1 AND $kalt == 1) {
									$rem_termin = $rem_termin . " ($speich: $telefonist) ";
									$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, von, bis, produkt_neu_id, seit, ";
									$sql .= "rem_termin, aquiriert, erledigt_date, vonr, storno, sperrzeit, wiedervorlage_date, w_zeit,  ";
									$sql .= "nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, einzug, edit) ";
									$sql .= "VALUES ('$termin_date', '$t_zeit', '$kunden_id', '$ad', '$ad', '$produkt_alt_id', '$von', '$bis', '$produkt_neu_id', '$seit', ";
									$sql .= "'$rem_termin', '$aquiriert', '$heute', '$vonr', '0', '0', '0000-00-00', '00:00',  ";
									$sql .= "'0', '1', '0', '0', '1', '0', '$einzug', '$timestamp') ";
									$query = myqueryi($db, $sql);
									$t_id = mysqli_insert_id($db);

									if ($einzug == 1) {				// Abschluss mit Bankeinzug
										$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', einzug = '$einzug' WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);
									}
									else {							// Abschluss ohne Bankeinzug
										$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1' WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);
									}

									$fehler = $fehler . "<br />Ein neuer Vertragsabschluss (kalt) mit der ID: $t_id wurde gespeichert !!";
								}	// Ende Abschluss kalt

				// normaler Abschluss
								elseif ($produkt_neu_id > 1 AND $kalt == 0) {
									$rem_termin = $rem_termin . " ($speich: $telefonist) ";
									$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, von, bis, produkt_neu_id, seit, ";
									$sql .= "rem_termin, aquiriert, erledigt_date, vonr, storno, sperrzeit, wiedervorlage_date, w_zeit,  ";
									$sql .= "nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, einzug, edit) ";
									$sql .= "VALUES ('$termin_date', '$t_zeit', '$kunden_id', '$aq', '$ad', '$produkt_alt_id', '$von', '$bis', '$produkt_neu_id', '$seit', ";
									$sql .= "'$rem_termin', '$aquiriert', '$heute', '$vonr', '0', '0', '0000-00-00', '00:00',  ";
									$sql .= "'0', '1', '0', '0', '0', '0', '$einzug', '$timestamp') ";
									$query = myqueryi($db, $sql);
									$t_id = mysqli_insert_id($db);

									if ($einzug == 1) {				// Abschluss mit Bankeinzug
										$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', einzug = '$einzug' WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);
									}
									else {							// Abschluss ohne Bankeinzug
										$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1' WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);
									}

									$fehler = $fehler . "<br />Ein neuer Vertragsabschluss mit der ID: $t_id wurde gespeichert !!";

								}	// ende Abschluss normal

				// kalter Termin
								elseif ($produkt_neu_id = 1 AND $kalt == 1) {
									$rem_termin = $rem_termin . " ($speich $telefonist) ";
									if ($nichtkunde == 0) {
										$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, von, bis, produkt_neu_id, seit, ";
										$sql .= "rem_termin, aquiriert, erledigt_date, vonr, storno, sperrzeit, wiedervorlage_date, w_zeit,  ";
										$sql .= "nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, edit) ";
										$sql .= "VALUES ('$termin_date', '$t_zeit', '$kunden_id', '$ad', '$ad', '$produkt_alt_id', '$von', '$bis', '1', '', ";
										$sql .= "'$rem_termin', '$aquiriert', '0000-00-00', '$vonr', '0', '0', '0000-00-00', '00:00',  ";
										$sql .= "'$nichtkunde', '0', '0', '0', '1', '0', '$timestamp') ";
										$query = myqueryi($db, $sql);
										$t_id = mysqli_insert_id($db);
										$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', belegt = '1' WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);
										$fehler = $fehler . "<br />Ein neuer Termin (kalt) mit der ID: $t_id wurde gespeichert !!";
									}
									else {
										$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, von, bis, produkt_neu_id, seit, ";
										$sql .= "rem_termin, aquiriert, erledigt_date, vonr, storno, sperrzeit, wiedervorlage_date, w_zeit,  ";
										$sql .= "nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, edit) ";
										$sql .= "VALUES ('$termin_date', '$t_zeit', '$kunden_id', '$ad', '$ad', '$produkt_alt_id', '$von', '$bis', '1', '', ";
										$sql .= "'$rem_termin', '$aquiriert', '$heute', '$vonr', '0', '0', '0000-00-00', '00:00',  ";
										$sql .= "'$nichtkunde', '0', '0', '0', '1', '0', '$timestamp') ";
										$query = myqueryi($db, $sql);
										$t_id = mysqli_insert_id($db);
										$sql = "UPDATE kunden SET nichtkunde = '1', neukunde = '0', belegt = '0' WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);

										// alle offenen Termine abschließen
										$sql = " SELECT termin_id, rem_termin FROM termin WHERE kd_id = '$kunden_id' AND termin >= '$heute' AND erledigt_date = '0000-00-00' AND alt = '0' AND sperrzeit = '0' ";
										$query = myqueryi($db, $sql);
										if (mysqli_num_rows($query) > 0) {
											for ($y=0; $y < mysqli_num_rows($query); $y++) {
												$offen = mysqli_fetch_array($query);
												$rem_termin = $offen[rem_termin] . " ($speich st: $telefonist) ";
												$sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '1', edit = '$timestamp' WHERE termin_id = '$offen[termin_id]'";
												$abfrage = myquery($sql1, $conn);
											}
										}
										// alle offenen Wiedervorlagen mßssen auf "alt" gesetzt werden
										$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);
										$fehler = $fehler . "<br />Ein neuer Termin (kalt) mit der ID: $t_id wurde gespeichert,";
										$fehler = $fehler . "<br />alle offenen Termine und<br />alle Wiedervorlagen wurden storniert!!";
									}



								}	// Ende Termin kalt

				// normaler Termin
								elseif ($produkt_neu_id = 1 AND $kalt == 0) {
									$rem_termin = $rem_termin . " ($speich: $telefonist) ";
									if ($nichtkunde == 0) {
										$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, von, bis, produkt_neu_id, seit, ";
										$sql .= "rem_termin, aquiriert, erledigt_date, vonr, storno, sperrzeit, wiedervorlage_date, w_zeit,  ";
										$sql .= "nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, edit) ";
										$sql .= "VALUES ('$termin_date', '$t_zeit', '$kunden_id', '$aq', '$ad', '$produkt_alt_id', '$von', '$bis', '1', '', ";
										$sql .= "'$rem_termin', '$aquiriert', '0000-00-00', '$vonr', '0', '0', '0000-00-00', '00:00',  ";
										$sql .= "'$nichtkunde', '0', '0', '0', '0', '0', '$timestamp') ";
										$query = myqueryi($db, $sql);
										$t_id = mysqli_insert_id($db);

										$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', belegt = '1' WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);

										$fehler = $fehler . "<br />Ein neuer Termin mit der ID: $t_id wurde gespeichert !!";
									}
									else {
										$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, von, bis, produkt_neu_id, seit, ";
										$sql .= "rem_termin, aquiriert, erledigt_date, vonr, storno, sperrzeit, wiedervorlage_date, w_zeit,  ";
										$sql .= "nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, edit) ";
										$sql .= "VALUES ('$termin_date', '$t_zeit', '$kunden_id', '$aq', '$ad', '$produkt_alt_id', '$von', '$bis', '1', '', ";
										$sql .= "'$rem_termin', '$aquiriert', '$heute', '$vonr', '0', '0', '0000-00-00', '00:00',  ";
										$sql .= "'$nichtkunde', '0', '0', '0', '0', '0', '$timestamp') ";
										$query = myqueryi($db, $sql);
										$t_id = mysqli_insert_id($db);
										$sql = "UPDATE kunden SET nichtkunde = '1', neukunde = '0', belegt = '0' WHERE kunden_id = '$kunden_id'";
										$query = myqueryi($db, $sql);

										// alle offenen Termine abschließen
										$sql = " SELECT termin_id, rem_termin FROM termin WHERE kd_id = '$kunden_id' AND termin >= '$heute' AND erledigt_date = '0000-00-00' AND alt = '0' AND sperrzeit = '0' ";
										$query = myqueryi($db, $sql);
										if (mysqli_num_rows($query) > 0) {
											for ($y=0; $y < mysqli_num_rows($query); $y++) {
												$offen = mysqli_fetch_array($query);
												$rem_termin = $offen[rem_termin] . " ($speich st: $telefonist) ";
												$sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '1', edit = '$timestamp' WHERE termin_id = '$offen[termin_id]'";
												$abfrage = myqueryi($db, $sql1);
											}
										}
										// alle offenen Wiedervorlagen müssen auf "alt" gesetzt werden
										$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);
										$fehler = $fehler . "<br />Ein neuer Termin mit der ID: $t_id wurde gespeichert,";
										$fehler = $fehler . "<br />alle offenen Termine und<br />alle Wiedervorlagen wurden storniert!!";
									}
								}	// Ende normaler Termin
							}	// ende Termin noch frei oder doppelt
						}	// ende else Telefonist eingegeben
					}	// ende else termin vollständig
				}
		// ende neuer Termin ----------------------------------------------------------------------------------------------------------------------

			} //ende if empty t_id - ende neuer termin


// es existiert eine Termin-ID - ein bestehender Termin soll bearbeitet werden --------------------------------------------------------------------------
// Fall 1: Ein Termin soll geändert werden (Korrektur, Verlegung, Storno ...)
// Fall 2: Ein Termin soll abgeschlossen werden (Vertrag, Nichtkunde, alt ...)
// Fall 3: Ein Termin soll in eine Wiedervorlage umgewandelt werden
// Fall 4: Eine Wiedervorlage soll in einen Termin gewandelt werden
// Fall 5: Eine Wiedervorlage soll bearbeitet werden
// Fall 6: Eine Wiedervorlage soll storniert werden
// Fall 7: Eine stornierte Wiedervorlage wird wieder aktiviert
// Fall 8: aus einer Wiedervorlage wird ein Nichtkunde

			else {													// Termin_id vorhanden - bestehender Termin soll geändert werden
				if ($wvl == 1) {									// vwl=1 - Wiedervorlage
					if (empty($wvl_tag)) {							// kein WVL-Datum eingegeben
						$fehler = $fehler . "<br />Sie müssen ein Wiedervorlage-Datum eingeben - Termin wurde nicht gespeichert";
						$norefresh = 1;							// kein Refresh am Ende des Scriptes
					}
					else {											// WVL-Datum eingegeben

						$wvl_date = "20$wvl_jahr-$wvl_monat-$wvl_tag";

						//if ($wvl_date < $heute) {					// Wiedervorlagetermin in der Vergangenheit!!
						//	$fehler = $fehler . "<br />Die Wiedervorlage kann nicht in der Vergangenheit liegen! - Termin wurde nicht gespeichert.";
						//	$bg_wvl = $bg_fehler;					// Hintergrund in Fehler-Farbe
						//	$norefresh = 1;							// kein Refresh am Ende des Scriptes
						//}
						//else { // Wiedervorlage in der Zukunft
							$norefresh = 0;

			// Fall 6: WVL-Termin soll storniert werden
							if ($alt == 1 AND ($termin[alt] == 0) AND ($termin[wiedervorlage] == 1)) {
								$rem_termin = $rem_termin . " ($speich st: $telefonist)";
								$sql = "UPDATE termin SET produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', seit = '', ";
								$sql .= "rem_termin = '$rem_termin', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '$wvl_date',  w_zeit = '$wvl_zeit', erledigt_date = '$heute', ";
								$sql .= "nichtkunde = '0', abschluss = '0', wiedervorlage = '1', alt = '1', kalt = '0', thandy = '0', edit = '$timestamp' ";
								$sql .= "WHERE termin_id = '$t_id'";
								$query = myqueryi($db, $sql);

								$fehler = $fehler . "<br />Die Wiedervorlage ID: $t_id wurde storniert.";

								// auslesen, ob es noch offene WVL gibt, wenn nicht, Kunde ist kein Wiedervorlage-Kunde mehr
								$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND wiedervorlage = '1' AND alt != '1' AND termin_id != '$t_id' ";
								$query = myqueryi($db, $sql);
								$test = mysqli_num_rows($query);
								if ($test > 0) {	// es gibt noch offene Wiedervorlagen
									$fehler = $fehler . "<br />Der Kunde bleibt in der Wiedervorlage, da noch offene WVL existieren";
								}
								else {
									$sql = "UPDATE kunden SET wiedervorlage = '0' WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
									$fehler = $fehler . "<br />Der Kunde ist kein Wiedervorlage-Kunde mehr.";
								}
							}
			// Fall 6: ende if // WVL-Termin soll storniert werden

			// Fall 7: stornierter WVL-Termin soll aktiviert werden

							elseif ($alt == 0 AND ($termin[alt] == 1) AND ($termin[wiedervorlage] == 1)) {
								$rem_termin = $rem_termin . " ($speich akt: $telefonist)";
								$sql = "UPDATE termin SET produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', seit = '', ";
								$sql .= "rem_termin = '$rem_termin', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '$wvl_date',  w_zeit = '$wvl_zeit', erledigt_date = '0000-00-00', ";
								$sql .= "nichtkunde = '0', abschluss = '0', wiedervorlage = '1', alt = '0', kalt = '0', thandy = '0', edit = '$timestamp' ";
								$sql .= "WHERE termin_id = '$t_id'";
								$query = myqueryi($db, $sql);
								$fehler = $fehler . "<br />Die Wiedervorlage ID: $t_id wurde wieder aktiviert.";

								$sql = "UPDATE kunden SET wiedervorlage = '1'  WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
							}
			// Fall 7: ende stornierter WVL-Termin soll aktiviert werden

			// Fall 8: aus einer Wiedervorlage wird ein Nichtkunde
							elseif ($nichtkunde == 1) {
								$rem_termin = $rem_termin . " ($speich nk: $telefonist)";
								$sql = "UPDATE termin SET produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', seit = '', ";
								$sql .= "rem_termin = '$rem_termin', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '$wvl_date',  w_zeit = '$wvl_zeit', erledigt_date = '$heute', ";
								$sql .= "nichtkunde = '0', abschluss = '0', wiedervorlage = '1', alt = '1', kalt = '0', thandy = '0', edit = '$timestamp' ";
								$sql .= "WHERE termin_id = '$t_id'";
								$query = myqueryi($db, $sql);

								// alle offenen Termine bearbeiten
								$termin_alt = termin_alt($kunden_id, $heute, $speich, $telefonist);

								// alle offenen Wiedervorlagen stornieren
								$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);
								$fehler = $fehler . "<br />alle offenen Termine und<br />alle Wiedervorlagen wurden storniert!!";
							}
			// Fall 8 : ende aus einer Wiedervorlage wird ein Nichtkunde

			// Fall 3: Termin soll in eine Wiedervorlage umgewandelt werden

							elseif ((!empty($termin[zeit])) AND ($termin[wiedervorlage] == 0) AND ($termin[abschluss] == 0) AND ($termin[alt] == 0)) {
								//$rem_termin_alt = $termin[rem_termin] . " ($speich st: $telefonist)";
								//$sql = "UPDATE termin SET produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', seit = '', ";
								//$sql .= "rem_termin = '$rem_termin_alt', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '$wvl_date',  w_zeit = '$wvl_zeit', erledigt_date = '$heute', ";
								//$sql .= "alt = '1', thandy = '0', edit = '$timestamp' ";
								//$sql .= "WHERE termin_id = '$t_id'";
								//$query = myqueryi($db, $sql);

								if ($gruppe == 'Administrator' AND empty($aq)) {
										$aq = $telefonist;			// für eingeloggten Telefonist wird das eigene Benutzerkürzel als $aq verwendet
								}
								elseif ($gruppe = 'Aquise') {
									$aq = $telefonist;
								}
								$rem_termin = $rem_termin . " ($speich aus T-ID: $t_id: $aq )";
								$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, von, bis, produkt_neu_id, seit, ";
								$sql .= "rem_termin, aquiriert, erledigt_date, vonr, termin.storno, sperrzeit, wiedervorlage_date, w_zeit,  ";
								$sql .= "nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, edit) ";
								$sql .= "VALUES ('0000-00-00', '00:00', '$kunden_id', '$aq', '', '$produkt_alt_id', '$von', '$bis', '1', '', ";
								$sql .= "'$rem_termin', '$aquiriert', '0000-00-00', '$vonr', '0', '0', '$wvl_date', '$wvl_zeit',  ";
								$sql .= "'0', '0', '1', '0', '0', '0', '$timestamp') ";
								$query = myqueryi($db, $sql);
								$t_id_neu = mysqli_insert_id($db);
								$fehler = $fehler . "<br />Aus dem Termin ID: $t_id wurde eine Wiedervorlage ID: $t_id_neu eingefügt.";

								$sql = "UPDATE kunden SET wiedervorlage = '1' WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
								/*
								// auslesen, ob es noch offene Termine gibt, wenn nicht, Kunde ist nicht mehr belegt
								$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND termin > 2000-01-01 AND erledigt_date = 0000-00-00 AND alt = '0' AND sperrzeit = '0' AND termin_id != '$t_id' ";
								$query = myqueryi($db, $sql);
								$test = mysql_num_rows($query);
								if ($test > 0) {	// es gibt noch offene Termine
									$sql = "UPDATE kunden SET wiedervorlage = '1' WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
									$fehler = $fehler . "<br />Der Kunde ist in der Wiedervorlage, bleibt aber belegt, da noch offene Termine existieren";
								}
								else {
									$sql = "UPDATE kunden SET wiedervorlage = '1', belegt = '0' WHERE kunden_id = '$kunden_id'";
									$query = myqueryi($db, $sql);
									$fehler = $fehler . "<br />Der Kunde hat keinen offenen Termin mehr - Wiedervorlage-Kunde";
								}
								*/
							}
			// Fall 3: ende Termin soll in eine Wiedervorlage umgewandelt werden

			// Fall 9: Abschluss soll in eine Wiedervorlage umgewandelt werden

							elseif ((!empty($termin[zeit])) AND ($termin[wiedervorlage] == 0) AND ($termin[abschluss] == 1)) {
								if ($gruppe == 'Administrator' AND empty($aq)) {
										$aq = $telefonist;			// für eingeloggten Telefonist wird das eigene Benutzerkürzel als $aq verwendet
								}
								elseif ($gruppe = 'Aquise') {
									$aq = $telefonist;
								}
								$rem_termin = $rem_termin . " ($speich aus T-ID: $t_id : $aq)";
								$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, von, bis, produkt_neu_id, seit, ";
								$sql .= "rem_termin, aquiriert, erledigt_date, vonr, termin.storno, sperrzeit, wiedervorlage_date, w_zeit,  ";
								$sql .= "nichtkunde, abschluss, wiedervorlage, alt, kalt, thandy, edit) ";
								$sql .= "VALUES ('0000-00-00', '00:00', '$kunden_id', '$aq', '', '$produkt_alt_id', '$von', '$bis', '1', '', ";
								$sql .= "'$rem_termin', '$aquiriert', '0000-00-00', '$vonr', '0', '0', '$wvl_date', '$wvl_zeit',  ";
								$sql .= "'0', '0', '1', '0', '0', '0', '$timestamp') ";
								$query = myqueryi($db, $sql);
								$t_id_neu = mysqli_insert_id($db);
								$fehler = $fehler . "<br />Aus dem Vertrag ID: $t_id wurde eine Wiedervorlage ID: $t_id_neu eingefügt.";

								$sql = "UPDATE kunden SET wiedervorlage = '1' WHERE kunden_id = '$kunden_id'";
								$query = myqueryi($db, $sql);
								$fehler = $fehler . "<br />Der Kunde ist Wiedervorlage-Kunde";
							}
			// Fall 9: ende Abschluss soll in eine Wiedervorlage umgewandelt werden

			// Fall 5: WVL-Termin soll bearbeitet werden

							elseif (($termin[wiedervorlage] == 1) AND ($termin[alt] == 0)) {
								if ($gruppe == 'Administrator' AND empty($aq)) {
									$aq = $telefonist;			// für eingeloggten Telefonist wird das eigene Benutzerkürzel als $aq verwendet
								}
								elseif ($gruppe = 'Aquise') {
									$aq = $telefonist;
								}
								$rem_termin = $rem_termin . " ($speich b: $aq)";
								$sql = "UPDATE termin SET produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '$produkt_neu_id', seit = '$seit', ";
								$sql .= "rem_termin = '$rem_termin', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '$wvl_date',  w_zeit = '$wvl_zeit', ";
								$sql .= "nichtkunde = '0', abschluss = '0', wiedervorlage = '1', alt = '0', kalt = '0', thandy = '0', edit = '$timestamp' ";
								$sql .= "WHERE termin_id = '$t_id'";
								$query = myqueryi($db, $sql);
								$fehler = $fehler . "<br />Die Wiedervorlage ID: $t_id wurde verändert.";
							}

			// Fall 5: ende WVL-Termin soll bearbeitet werden
						//}	// ende else Wiedervorlage in der Zukunft
					}	// ende else WVL-Datum eingegeben
				}	// ende if vwl=1 - Wiedervorlage

// ein vorhandener Termin (keine Wiedervorlage) soll bearbeitet werden -------------------------------------
// ---------------------------------------------------------------------------------------------------------

				else {																// wvl = 0 - termin
					if (empty($ad)) {												// kein Außendienstler eingegeben
						$fehler = $fehler . "<br />Bitte Außendienstler eingeben - Termin nicht gespeichert!";
						$norefresh = 1;							// kein Refresh am Ende des Scriptes
					}	// ende if kein Außendienstler eingegeben

					else {															//Außendienstler eingegeben
						if (empty($t_tag)) {										// kein Termin-Datum eingegeben
							$fehler = $fehler . "<br />Sie müssen ein Termin-Datum eingeben - Termin wurde nicht gespeichert";
							$norefresh = 1;							// kein Refresh am Ende des Scriptes
						}
						else {														// Termin-Datum eingegeben
							//$termin_date = "20$t_jahr-$t_monat-$t_tag";
							if (empty($t_zeit)) {									// keine Zeit eingegeben
								$fehler = $fehler . "<br />Sie müssen eine Termin-Zeit eingeben - Termin wurde nicht gespeichert";
								$norefresh = 1;							// kein Refresh am Ende des Scriptes
							}
							else {													// Termin-Zeit eingegeben
								if ($gruppe == 'Administrator' AND empty($aq)) {
									$aq = $telefonist;			// für eingeloggten Telefonist wird das eigene Benutzerkürzel als $aq verwendet
								}
								elseif ($gruppe == 'Aquise') {
									$aq = $telefonist;
								}

				// Laufender Termin soll bearbeitet werden
								if ($termin[wiedervorlage] == 0) {
									$norefresh = 0;
									if ($alt == 0 AND $termin[alt] == 0) {						// Termin wird bearbeitet

							// Abschluss
										if ($produkt_neu_id > 1) {
											if (!empty($storno)) {								// Abschluss wurde storniert
												$rem_termin = $rem_termin . " ($speich st: $telefonist) ";
												$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', ";
												$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '$produkt_neu_id', seit = '$seit', ";
												$sql .= "rem_termin = '$rem_termin', erledigt_date = '$heute', vonr = '$vonr', termin.storno = '$storno', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
												$sql .= "nichtkunde = '0', abschluss = '1', wiedervorlage = '0', alt = '0', thandy = '0', einzug = '0', edit = '$timestamp' ";
												$sql .= "WHERE termin_id = '$t_id'";
												$query = myqueryi($db, $sql);

												// Test, ob noch weitere Verträge da sind
												$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND produkt_neu_id > '1' AND (termin.storno IS NULL OR termin.storno = '0') AND abschluss = '1' AND termin_id != '$t_id' ";
												$query = myqueryi($db, $sql);
												if (mysqli_num_rows($query) == 0) {					// keine Verträge mehr da, alles storniert
													$sql = "UPDATE kunden SET vertragskunde = '0', nichtkunde = '0', einzug = '0', kunden.storno = '1' WHERE kunden_id = '$kunden_id' ";
													$query = myqueryi($db, $sql);
													$fehler = $fehler . "<br />Der Vertrag mit der ID: $t_id wurde storniert, der Kunde ist kein Vertragskunde mehr.";
												}
												else {											// es gibt noch laufende Verträge

													// Ermittlung, ob es noch Verträge mit Bankeinzug gibt
													$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND produkt_neu_id > '1' AND (termin.storno IS NULL OR termin.storno = '0') AND abschluss = '1' AND einzug = '1' AND termin_id != '$t_id' ";
													$query = myqueryi($db, $sql);
													if (mysqli_num_rows($query) == 0) {				// keine Verträge mit Bankeinzug mehr da
														$sql = "UPDATE kunden SET einzug = '0', kunden.storno = '1' WHERE kunden_id = '$kunden_id'";
														$query = myqueryi($db, $sql);
														$fehler = $fehler . "<br />Der Vertrag mit der ID: $t_id wurde storniert - der Kunde $vorname $name hat keine Vertr�ge mit Bankeinzug mehr, bleibt aber Vertragskunde.";
													}
													else {											//	noch Verträge mit Bankeinzug da
														$sql = "UPDATE kunden SET kunden.storno = '1' WHERE kunden_id = '$kunden_id'";
														$query = myqueryi($db, $sql);
														$fehler = $fehler . "<br />Der Vertrag mit der ID: $t_id wurde storniert - der Kunde $vorname $name hat weitere Vertr�ge mit Bankeinzug.";
													}
												}
											}	// ende storno

											elseif (empty($storno) AND (!empty($termin[storno]))) {	// storno soll rückgängig gemacht werden
												$rem_termin = $rem_termin . " ($speich akt: $telefonist) ";
												$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', ";
												$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '$produkt_neu_id', seit = '$seit', ";
												$sql .= "rem_termin = '$rem_termin', erledigt_date = '0000-00-00', vonr = '$vonr', termin.storno = '0', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
												$sql .= "nichtkunde = '0', abschluss = '1', wiedervorlage = '0', alt = '0', thandy = '0', einzug = '$einzug', edit = '$timestamp' ";
												$sql .= "WHERE termin_id = '$t_id'";
												$query = myqueryi($db, $sql);

												// Test, ob noch andere Stornos da sind
												$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND produkt_neu_id > '1' AND (termin.storno != '0') AND abschluss = '1' AND termin_id != '$t_id' ";
												$query = myqueryi($db, $sql);
												if (mysqli_num_rows($query) > 0) {				// noch stornos da
													if ($kunde[einzug] == 1) {					// Kunde ist bereits einzugskunde
														$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1' WHERE kunden_id = '$kunden_id'";
														$query = myqueryi($db, $sql);
													}
													else {										// kunde ist noch kein einzugskunde
														$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', einzug = '$einzug' WHERE kunden_id = '$kunden_id'";
														$query = myqueryi($db, $sql);
													}

												}
												else {											// keine weiteren stornos mehr da
													if ($kunde[einzug] == 1) {					// Kunde ist bereits einzugskunde
														$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', storno = '0' WHERE kunden_id = '$kunden_id'";
														$query = myqueryi($db, $sql);
														$fehler = $fehler . "<br />Der Vertrag mit der ID: $t_id wurde storniert - der Kunde $vorname $name hat weitere Verträge mit Bankeinzug.";
													}
													else {										// kunde ist noch kein einzugskunde
														$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', einzug = '$einzug', storno = '0' WHERE kunden_id = '$kunden_id'";
														$query = myqueryi($db, $sql);
													}
												}
												$fehler = $fehler . "<br />Der stornierte Vertrags mit der ID: $t_id wurde reaktiviert,<br />
													der Kunde $vorname $name ist wieder Vertragskunde";

											}	// ende storno rückgängig


											else {													// Vertragsabschluss
												$rem_termin = $rem_termin . " ($speich v: $telefonist) ";
												$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', ";
												$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '$produkt_neu_id', seit = '$seit', ";
												$sql .= "rem_termin = '$rem_termin', erledigt_date = '$heute', vonr = '$vonr', termin.storno = '0', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
												$sql .= "nichtkunde = '0', abschluss = '1', wiedervorlage = '0', alt = '0', kalt = '$kalt', thandy = '0', einzug = '$einzug', edit = '$timestamp' ";
												$sql .= "WHERE termin_id = '$t_id'";
												$query = myqueryi($db, $sql);

												// Test, ob es noch weitere offene Termine gibt
												$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND termin > 2000-01-01 AND erledigt_date = 0000-00-00 AND alt = '0' AND sperrzeit = '0' AND termin_id != '$t_id' ";
												$query = myqueryi($db, $sql);
												$test = mysqli_num_rows($query);

												if ($test > 0) {						// es gibt noch offene Termine

													if ($einzug == 1) {					// Vertragsabschluss mit Bankeinzug
														$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', einzug = '$einzug' WHERE kunden_id = '$kunden_id'";
														$query = myqueryi($db, $sql);
													}
													else {								// Vertragsabschluss ohne Bankeinzug
														// Test, ob es noch weitere Termine mit Bankeinzug gibt
														$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND produkt_neu_id > '1' AND (termin.storno IS NULL OR termin.storno = '0') AND abschluss = '1' AND einzug = '1' AND termin_id != '$t_id' ";
														$query = myqueryi($db, $sql);
														if (mysqli_num_rows($query) == 0) {				// nein
															$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', einzug = '$einzug' WHERE kunden_id = '$kunden_id'";
															$query = myqueryi($db, $sql);
														}
														else {											//	ja
															$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1' WHERE kunden_id = '$kunden_id'";
															$query = myqueryi($db, $sql);
														}
													}
													$fehler = $fehler . "<br />Ein Vertragsabschluss mit der ID: $t_id wurde gespeichert,<br />
														der Kunde bleibt aber belegt, da noch offene Termine existieren";
												}	// ende noch offene Termine

												else {									// keine offenen Termine mehr da

													if ($einzug == 1) {					// Vertragsabschluss mit Bankeinzug
														$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', einzug = '$einzug', belegt = '0' WHERE kunden_id = '$kunden_id'";
														$query = myqueryi($db, $sql);
													}
													else {								// Vertragsabschluss ohne Bankeinzug
														// Test, ob es noch weitere Termine mit Bankeinzug gibt
														$sql = "SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND produkt_neu_id > '1' AND (termin.storno IS NULL OR termin.storno = '0') AND abschluss = '1' AND einzug = '1' AND termin_id != '$t_id' ";
														$query = myqueryi($db, $sql);
														if (mysqli_num_rows($query) == 0) {				// nein
															$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', einzug = '$einzug', belegt = '0' WHERE kunden_id = '$kunden_id'";
															$query = myqueryi($db, $sql);
														}
														else {											//	ja
															$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', belegt = '0' WHERE kunden_id = '$kunden_id'";
															$query = myqueryi($db, $sql);
														}
													}
													$fehler = $fehler . "<br />Ein Vertragsabschluss mit der ID: $t_id wurde gespeichert,<br />
														der Kunde ist freigegeben (keine offenen Termine mehr)";
												}	// ende keine offenen Termine mehr da
											}
										}	// ende $produkt_neu_id > 1
							// ende Abschluss

							// Termindaten werden verarbeitet
										else {
											if ($nichtkunde == 1) {		// Termin soll zum NK-Termin werden
												$rem_termin = $rem_termin . " ($speich nk: $telefonist) ";
												$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', telefonist = '$aq', ";
												$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', ";
												$sql .= "rem_termin = '$rem_termin', erledigt_date = '$heute', vonr = '$vonr', termin.storno = '$storno', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
												$sql .= "nichtkunde = '$nichtkunde', abschluss = '0', wiedervorlage = '0', alt = '$alt', kalt = '$kalt', einzug = '$einzug', edit = '$timestamp' ";
												$sql .= "WHERE termin_id = '$t_id'";
												$query = myqueryi($db, $sql);

												// alle offenen Termine bearbeiten
												$termin_alt = termin_alt($kunden_id, $heute, $speich, $telefonist);

												// alle offenen Wiedervorlagen müssen auf "alt" gesetzt werden
												$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);
												$fehler = $fehler . "<br />Aus dem Termin ID: $t_id wurde ein Nichtkunde<br />alle offenen Termine und<br />alle Wiedervorlagen wurden storniert!!";
											}

											elseif ($nichtkunde == 0 AND $termin[nichtkunde] == 1) {		// NK-Termin soll reaktiviert werden
												$rem_termin = $rem_termin . " ($speich nk: $telefonist) ";
												$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', telefonist = '$aq', ";
												$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', ";
												$sql .= "rem_termin = '$rem_termin', erledigt_date = '0000-00-00', vonr = '$vonr', termin.storno = '$storno', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
												$sql .= "nichtkunde = '$nichtkunde', abschluss = '0', wiedervorlage = '0', alt = '0', kalt = '$kalt', einzug = '$einzug', edit = '$timestamp' ";
												$sql .= "WHERE termin_id = '$t_id'";
												$query = myqueryi($db, $sql);

												// Kunde ist wieder belegt
												$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', belegt = '1' WHERE kunden_id = '$kunden_id'";
												$query = myqueryi($db, $sql);
												$fehler = $fehler . "<br />Der Termin ID: $t_id wurde wieder aktiviert, der Kunde ist belegt";
											}

											elseif ($absch == 1 AND $nichtkunde == 0) {
												$rem_termin = $rem_termin . " ($speich abg: $telefonist) ";
												$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', ";
												$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', seit = '', ";
												$sql .= "rem_termin = '$rem_termin', erledigt_date = '$heute', vonr = '$vonr', termin.storno = '$storno', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
												$sql .= "nichtkunde = '$nichtkunde', abschluss = '0', wiedervorlage = '0', alt = '0', kalt = '$kalt', thandy = '0', einzug = '$einzug', edit = '$timestamp' ";
												$sql .= "WHERE termin_id = '$t_id'";
												$query = myqueryi($db, $sql);
												$fehler = $fehler . "<br />Der Termin ID: $t_id wurde abgeschlossen";


												// auslesen, ob es noch offene Termine gibt, wenn nicht, Kunde ist nicht mehr belegt
												$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND termin > 2000-01-01 AND erledigt_date = 0000-00-00 AND alt = '0' AND sperrzeit = '0' AND termin_id != '$t_id' ";
												$query = myqueryi($db, $sql);
												$test = mysqli_num_rows($query);
												if ($test > 0) {	// es gibt noch offene Termine
													$fehler = $fehler . "<br />Der Kunde bleibt belegt, da noch offene Termine existieren";
												}
												else {				// keine offenen Termine mehr
													$sql = "UPDATE kunden SET belegt = '0' WHERE kunden_id = '$kunden_id'";
													$query = myqueryi($db, $sql);
													$fehler = $fehler . "<br />Der Kunde hat keinen offenen Termin mehr.";
												}
											}

											else {
												$rem_termin = $rem_termin . " ($speich b: $telefonist) ";
												$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', ";
												$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', seit = '', ";
												$sql .= "rem_termin = '$rem_termin', erledigt_date = '$termin[erledigt_date]', vonr = '$vonr', termin.storno = '$storno', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
												$sql .= "nichtkunde = '$nichtkunde', abschluss = '0', wiedervorlage = '0', alt = '$alt', kalt = '$kalt', einzug = '$einzug', edit = '$timestamp' ";
												$sql .= "WHERE termin_id = '$t_id'";
												$query = myqueryi($db, $sql);
												$fehler = $fehler . "<br />Die Daten des Termins ID: $t_id wurden verändert!!";
										/*
												// auslesen, ob es offene Termine gibt, wenn nicht, Kunde ist nicht mehr belegt
												$sql = "SELECT termin_id FROM termin ";
												$sql .= " WHERE kd_id = '$kunde[0]' AND termin > '2000-01-01' AND (erledigt_date IS NULL OR erledigt_date='0000-00-00') ";
												$sql .= " AND nichtkunde = '0' AND abschluss = '0' AND wiedervorlage = '0' AND alt = '0'";
												$query = mysql_query($sql, $conn);
												$test = mysql_num_rows($query);
												if (mysql_num_rows($query) > 0) {	// es gibt noch offene Termine
													$sql = "UPDATE kunden SET belegt = '1' WHERE kunden_id = '$kunden_id'";
													$query = myqueryi($db, $sql);
												}
												*/
											}
										}	// ende "normaler" termin
									}	//ende if Termin wird bearbeitet

									elseif ($nichtkunde == 1 AND $alt == 1) {		// Termin soll zum NK-Termin und storniert werden
										$rem_termin = $rem_termin . " ($speich nk: $telefonist) ";
										$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', telefonist = '$aq', ";
										$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', ";
										$sql .= "rem_termin = '$rem_termin', erledigt_date = '$heute', vonr = '$vonr', termin.storno = '$storno', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
										$sql .= "nichtkunde = '$nichtkunde', abschluss = '0', wiedervorlage = '0', alt = '$alt', kalt = '$kalt', einzug = '$einzug', edit = '$timestamp' ";
										$sql .= "WHERE termin_id = '$t_id'";
										$query = myqueryi($db, $sql);

										// alle offenen Termine bearbeiten
										$termin_alt = termin_alt($kunden_id, $heute, $speich, $telefonist);

										// alle offenen Wiedervorlagen müssen auf "alt" gesetzt werden
										$wvl_alt = wvl_alt($kunden_id, $heute, $speich, $telefonist);
										$fehler = $fehler . "<br />Aus dem Termin ID: $t_id wurde ein Nichtkunde<br />alle offenen Termine und<br />alle Wiedervorlagen wurden storniert!!";
									}

									elseif ($alt == 1 AND $termin[alt] == 0 AND $nichtkunde == 0)	{		// Termin soll auf "alt" gesetzt werden
										$rem_termin = $rem_termin . " ($speich st: $telefonist)";
										$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', ";
										$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', seit = '', ";
										$sql .= "rem_termin = '$rem_termin', erledigt_date = '$heute', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
										$sql .= "nichtkunde = '$nichtkunde', abschluss = '0', wiedervorlage = '0', alt = '1', kalt = '$kalt', thandy = '0', edit = '$timestamp' ";
										$sql .= "WHERE termin_id = '$t_id'";
										$query = myqueryi($db, $sql);
										$fehler = $fehler . "<br />Der Termin ID: $t_id wurde storniert.";

										// auslesen, ob es noch offene Termine gibt, wenn nicht, Kunde ist nicht mehr belegt
										$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND termin > 2000-01-01 AND erledigt_date = 0000-00-00 AND alt = '0' AND sperrzeit = '0' AND termin_id != '$t_id' ";
										$query = myqueryi($db, $sql);
										$test = mysqli_num_rows($query);
										if ($test > 0) {	// es gibt noch offene Termine
											$fehler = $fehler . "<br />Der Kunde bleibt belegt, da noch offene Termine existieren";
										}
										else {	// keine offenen termine mehr
											$sql = "UPDATE kunden SET belegt = '0' WHERE kunden_id = '$kunden_id'";
											$query = myqueryi($db, $sql);
											$fehler = $fehler . "<br />Der Kunde hat keinen offenen Termin mehr.";
										}

									}	// ende Termin soll auf "alt" gesetzt werden

									elseif ($alt == 0 AND $termin[alt] == 1)	{				// "alter" Termin soll reaktiviert werden
										if ($produkt_neu_id > 1) {
											$rem_termin = $rem_termin . " ($speich aktiv: $telefonist)";
											$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', ";
											$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = 'produkt_neu_id', seit = '$seit', ";
											$sql .= "rem_termin = '$rem_termin', erledigt_date = '$termin[erledigt_date]', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
											$sql .= "nichtkunde = '0', abschluss = '1', wiedervorlage = '0', alt = '0', kalt = '$kalt', thandy = '0', einzug = '$einzug', edit = '$timestamp' ";
											$sql .= "WHERE termin_id = '$t_id'";
											$query = myqueryi($db, $sql);

											if ($einzug == 1) {					// Vertragsabschluss mit Bankeinzug
												$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1', einzug = '$einzug' WHERE kunden_id = '$kunden_id'";
												$query = myqueryi($db, $sql);
											}
											else {								// Vertragsabschluss ohne Bankeinzug
												$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1' WHERE kunden_id = '$kunden_id'";
												$query = myqueryi($db, $sql);
											}
											$fehler = $fehler . "<br />Der Termin ID: $t_id wurde wieder aktiviert und zum Vertrag";
										} // ende alter Termin reaktiviert

									else {
											$rem_termin = $rem_termin . " ($speich aktiv: $telefonist)";
											$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', ";
											$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '$produkt_neu_id', seit = '$seit', ";
											$sql .= "rem_termin = '$rem_termin', erledigt_date = '0000-00-00', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
											$sql .= "nichtkunde = '0', abschluss = '0', wiedervorlage = '0', alt = '0', kalt = '$kalt', thandy = '0', edit = '$timestamp' ";
											$sql .= "WHERE termin_id = '$t_id'";
											$query = myqueryi($db, $sql);
											$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', belegt = '1' WHERE kunden_id = '$kunden_id'";
											$query = myqueryi($db, $sql);
											$fehler = $fehler . "<br />Der Termin ID: $t_id wurde wieder aktiviert, der Kunde ist belegt";
										}
									}	// ende Termin soll auf "alt" gesetzt werden

									elseif ($alt == 1 AND $termin[alt] == 1)	{				// "alter" Termin soll bearbeitet werden
											$rem_termin = $rem_termin . " ($speich b: $telefonist) ";
											$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', aussendienst = '$ad', ";
											$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', ";
											$sql .= "rem_termin = '$rem_termin', erledigt_date = '$termin[erledigt_date]' ";
											$sql .= "nichtkunde = '$nichtkunde', abschluss = '0', wiedervorlage = '0', alt = '1', kalt = '$kalt', thandy = '0', edit = '$timestamp' ";
											$sql .= "WHERE termin_id = '$t_id'";
											$query = myqueryi($db, $sql);
											$fehler = $fehler . "<br />Die Daten des Termins ID: $t_id wurden ver�ndert!!";
									}
								}	// ende $termin[wiedervorlage] == 0
				// ende laufender Termin soll bearbeitet werden ------------------------------------------------------------------------------------

				// Wiedervorlage soll zum Termin werden ---------------------------------------------------------------------------------------------
								elseif ($termin[wiedervorlage] == 1) {
								// Überprüfung, ob Termin bereits belegt ist
									$sql = "SELECT termin_id FROM termin WHERE termin = '$termin_date' AND zeit = '$t_zeit' AND aussendienst = '$ad' AND alt = '0' ";		// ist der Termin schon belegt?
									$query = myqueryi($db, $sql);
									$anz_doppelt = mysqli_num_rows($query);
									if (($anz_doppelt > 0) AND ($doppelt == 0)) {
										$fehler = $fehler . "<br />Fehler: Der Termin ist bereits belegt - soll er mehrfach belegt werden bitte \"2x\" anklicken!!";
										$doppler = '1';							// wird im Formular ausgewertet - Zeile Doppelt = Rot
										$norefresh = 1;							// kein Refresh am Ende des Scriptes
									}
									else {	// Termin ist noch frei oder soll doppelt belegt werden
										$norefresh = 0;
										if ($produkt_neu_id > 1) {
											if ($kalt == 1) { $aq = $ad; }
											$rem_termin = $rem_termin . " ($speich v: $telefonist) ";
											$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', telefonist = '$aq', aussendienst = '$ad', ";
											$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '$produkt_neu_id', seit = '$seit', ";
											$sql .= "rem_termin = '$rem_termin', erledigt_date = '$heute', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
											$sql .= "nichtkunde = '0', abschluss = '1', wiedervorlage = '0', alt = '0', kalt = '$kalt', einzug = '$einzug', thandy = '0', edit = '$timestamp' ";
											$sql .= "WHERE termin_id = '$t_id'";
											$query = myqueryi($db, $sql);
											if ($einzug == 1) {
												$sql = "UPDATE kunden SET neukunde = '0', vertragskunde = '1', einzug = '1' WHERE kunden_id = '$kunden_id'";
												$query = myqueryi($db, $sql);
												$fehler = $fehler . "<br />Die Wiedervorlage ID: $t_id wurde zum Vertrag mit Bankeinzug";
											}
											else {
												$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', vertragskunde = '1' WHERE kunden_id = '$kunden_id'";
												$query = myqueryi($db, $sql);
												$fehler = $fehler . "<br />Die Wiedervorlage ID: $t_id wurde zum Vertrag";
											}
										}
										else {
											if ($kalt == 1) { $aq = $ad; }
											$rem_termin = $rem_termin . " ($speich t: $telefonist) ";
											$sql = "UPDATE termin SET termin = '$termin_date', zeit = '$t_zeit', telefonist = '$aq', aussendienst = '$ad', ";
											$sql .= "produkt_alt_id = '$produkt_alt_id', von = '$von', bis = '$bis', produkt_neu_id = '1', seit = '', ";
											$sql .= "rem_termin = '$rem_termin', erledigt_date = '', vonr = '$vonr', sperrzeit = '0', wiedervorlage_date = '0000-00-00',  w_zeit = '00:00', ";
											$sql .= "nichtkunde = '0', abschluss = '0', wiedervorlage = '0', alt = '0', kalt = '$kalt', thandy = '0', edit = '$timestamp' ";
											$sql .= "WHERE termin_id = '$t_id'";
											$query = myqueryi($db, $sql);

											$sql = "UPDATE kunden SET nichtkunde = '0', neukunde = '0', belegt = '1' WHERE kunden_id = '$kunden_id'";
											$query = myqueryi($db, $sql);
											$fehler = $fehler . "<br />Die Wiedervorlage ID: $t_id wurde zum Termin";
										}

										// auslesen, ob es noch offene WVL gibt, wenn nicht, Kunde ist kein Wiedervorlage-Kunde mehr
										$sql = " SELECT termin_id FROM termin WHERE kd_id = '$kunden_id' AND wiedervorlage = '1' AND alt != '1' AND termin_id != '$t_id' ";
										$query = myqueryi($db, $sql);
										$test = mysqli_num_rows($query);
										if ($test > 0) {	// es gibt noch offene Wiedervorlagen
											$fehler = $fehler . "<br />Der Kunde bleibt in der Wiedervorlage, da noch offene WVL existieren";
										}
										else {
											$sql = "UPDATE kunden SET wiedervorlage = '0' WHERE kunden_id = '$kunden_id'";
											$query = myqueryi($db, $sql);
											$fehler = $fehler . "<br />Der Kunde ist kein Wiedervorlage-Kunde mehr.";
										}
									}	// ende else Termin noch frei oder doppelt
								}

				// ende Wiedervorlage soll zum Termin werden ---------------------------------------------------------------------------------------
							}	// ende else Termin-Zeit eingegeben
						} 	// ende else Termin-Datum eingegeben
					}	// ende else Außendienstler eingegeben
				}	// ende else vwl = 0 - termin
			} // ende t_id vorhanden -terminänderung
		} // // Termindaten eingegeben oder T-ID gesetzt
	} // Ende keine Fehler bei der Formularüberprüfung
}	// ende if ausgeloggt
} // Ende isset($_speichern)
?>

<!DOCTYPE html>
<html lang="de">
<head>
<title>Termine-neu</title>
	<!-- allgemein/kunde_neu.php -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div align = "center">

<table border="1" width="800" bordercolor="#006699">
<?php
	if ($fehler) {
 		echo "<tr bgcolor=\"red\">";
        echo "<td colspan=\"3\" align = \"left\" valign = \"middle\">";
		echo "<span style=\"font-weight:bold; color:white; line-height:150%;\">$fehler</span><br>";
		echo "</td></tr>";
	}
	elseif ($ausgeloggt) {
 		echo "<tr bgcolor=\"red\">";
        echo "<td colspan=\"3\">";
		echo "<div align = \"center\"><span style=\"font-weight:bold; font-size: 13pt; color:white; line-height: 150%;\">$ausgeloggt</span></div>";
		echo "</td></tr>";
	}
?>
  <!-- Tabelle Rand -->
<form name="terminvereinbarung" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
<input type="hidden" name="plz" value = "<?php echo "$plz"; ?>"/>
	<tr cellspacing="0" cellpadding="0">
    	<td colspan="3">
	<tr bgcolor="<?php echo "$bg_ad";?>">
		<td colspan="3" align = "left"><span style="font-weight:bold;">
	 <?php
	  			echo "AD:&nbsp;";
	  			// Abfrage der Außendienstler
	  			$sql = "SELECT user FROM user WHERE gruppen_id = '3' ORDER BY user ASC";
				$query_ad = myqueryi($db, $sql);


	  			// Darstellung der Außendienster als SELECT-Liste

				echo "<select name=\"ad_neu\" class = \"adselect\">";
				echo " <option></option>";			 						// erste Zeile als Leerzeile, falls noch kein AD ausgewählt wurde
				for ($j = 0; $j < mysqli_num_rows($query_ad); $j++)	{		// Anzahl der Datensätze
					$zeile = mysqli_fetch_row($query_ad);					// Schleife für Daten-Zeilen
					if ($zeile[0] == $ad) {									// bestehender Termin wurde angeklickt - AD bereits vergeben
						echo " <option selected>$zeile[0]</option>";
					}
					else {
						echo " <option>$zeile[0]</option>";
					}
				}
	 			echo "</select>";

				// nur Administrator darf Telefonist ändern! --------------------------------------------------------------------
				// --------------------------------------------------------------------------------------------------------------

				if ($gruppe == 'Administrator') {

					echo "&nbsp;&nbsp;Telefonist:&nbsp;";

	  				$sql = "SELECT user FROM user WHERE gruppen_id = '1' OR gruppen_id = '2' ORDER BY user ASC";	// Abfrage der Telefonisten
					$query_aq = myqueryi($db, $sql);;

	  				// Darstellung der Telefonisten als SELECT-Liste

					echo "<select name=\"aq_neu\" class = \"aqselect\">";
					echo " <option></option>";			 						// erste Zeile als Leerzeile, falls noch kein AD ausgewählt wurde
					for ($j = 0; $j < mysqli_num_rows($query_aq); $j++)	{		// Anzahl der Datensätze
						$zeile = mysqli_fetch_row($query_aq);					// Schleife für Daten-Zeilen
						if ($zeile[0] == $aq) {									// bestehender Termin wurde angeklickt - AD bereits vergeben
							echo " <option selected>$zeile[0]</option>";
						}
						else {
							echo " <option>$zeile[0]</option>";
						}
					}
	 				echo "</select>";
				}

				if (!empty($name)) {										// Angaben zum Kunden bei leerem Dokument ausblenden
	  				echo "<span style=\"font-size:10pt;\">&nbsp;&nbsp;$vorname $name - Tel.: ($vorwahl1) $telefon - $plz $ort</span";
				}
	 		 ?>
	  	</span>
		</td>
    </tr>

<!-- Zeile zur Darstellung Termindaten / Checkboxen / Kundendaten ------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

    <tr bgcolor="<?php echo "$bg";?>">

<!-- Zelle zur Darstellung Termindaten ---------------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

		<td align = "left" valign = "top">
			<table cellpadding = "2" bgcolor = "#006699">

				<tr bgcolor="<?php echo "$bg";?>">
					<td width="89"><span style = "font-weight:bold;">Kd.-ID</span></td>
      				<td><span style = "font-weight:bold;"><?php echo "$kunden_id"; ?></span></td>
      				<td width="89"><span style = "font-weight:bold;">Term.-ID</span></td>
	  				<td width="101"><span style = "font-weight:bold;"><?php echo "$t_id"; ?></span></td>
    			</tr>

				<tr bgcolor="<?php echo "$bg";?>">
					<td width="89">Termin</td>
					<?php	echo "<td bgcolor=\"$bg_termin\">";
						if (($t_tag === "00") AND ($t_monat === "00") AND ($t_jahr === "00")) {
	  						echo "<input type=\"text\"  class=\"eingabe\" name=\"t_tag_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"t_monat_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"t_jahr_neu\" size = \"2\" maxlength =\"2\">";
						}
						else {
							echo "<input type=\"text\"  class=\"eingabe\" name=\"t_tag_neu\" size = \"2\" maxlength =\"2\" value = \"$t_tag\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"t_monat_neu\" size = \"2\" maxlength =\"2\" value = \"$t_monat\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"t_jahr_neu\" size = \"2\" maxlength =\"2\" value = \"$t_jahr\">";
						}
						echo "</td>";
					?>
      				<td width="89">Zeit</td>
	  				<td width="101"><select name="t_zeit_neu" class = "eingabe">
		  <?php
							for ($i = 0; $i < count($ta_zeit); $i++) { 						// Erzeugung der Zeilen
								if ($t_zeit === $ta_zeit[$i]) {
									echo " <option selected>$ta_zeit[$i]</option>";
								}
								else {
									echo " <option>$ta_zeit[$i]</option>";
								}
							}
	 					?>
	 					</select>
      				</td>
    			</tr>

	    		<tr bgcolor="<?php echo "$bg";?>">
      				<td width="89">WVL&nbsp;
						<?php if($wvl == 1) {
								echo "<input type=\"checkbox\" name=\"wvl_neu\" checked>";
							}
							else {
								echo "<input type=\"checkbox\" name=\"wvl_neu\">";
							}
						?>
					</td>
					<?php	echo "<td bgcolor=\"$bg_wvl\">";
						if (($wvl_tag === "00") AND ($wvl_monat === "00") AND ($wvl_jahr === "00")) {
	  						echo "<input type=\"text\"  class=\"eingabe\" name=\"wvl_tag_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"wvl_monat_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"wvl_jahr_neu\" size = \"2\" maxlength =\"2\">";
						}
						else {
							echo "<input type=\"text\"  class=\"eingabe\" name=\"wvl_tag_neu\" size = \"2\" maxlength =\"2\" value = \"$wvl_tag\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"wvl_monat_neu\" size = \"2\" maxlength =\"2\" value = \"$wvl_monat\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"wvl_jahr_neu\" size = \"2\" maxlength =\"2\" value = \"$wvl_jahr\">";
						}
						echo "</td>";
					?>
	  				<td>Zeit</td>
	  				<td><select name="wvl_zeit_neu" class = "eingabe">
		   <?php
							for ($i = 0; $i < count($wa_zeit); $i++) { // Erzeugung der Zeilen
								if ($wvl_zeit === $wa_zeit[$i]) {
									echo " <option selected>$wa_zeit[$i]</option>";
								}
								else {
									echo " <option>$wa_zeit[$i]</option>";
								}
							}
	 					?>
	 					</select>
      				</td>
	  			</tr>

	    		<tr bgcolor="<?php echo "$bg";?>">
      				<td width="89">angelegt</td>
      				<td><?php echo "$mitarbeiter"; ?></td>
	  				<td>am</td>
	  				<td><?php echo "$gespeichert"; ?></td>
	  			</tr>

			 <tr bgcolor="<?php echo "$bg_ges_alt";?>">
      			<td colspan="4" align = "center"><span style = "font-weight:bold; color:red;">Gesellschaft und Tarif - ALT</span></td>

			 <?php	// Wenn Gesellschaft alt existiert, wird eine Zeile mit Gesellschaft alt und Tarif alt angezeigt
			 	// und eine zweite Zeile zur Eingabe von neuen Date

			 		echo "<tr bgcolor= \"$bg_ges_alt\">";
			 			echo "<td width=\"89\">Gesell.</td>";
						echo "<td>	<select name=\"ges_alt_neu\">";
	   									$sql = "SELECT ges_alt FROM ges_alt ORDER BY ges_alt ASC ";
										$query_ges_alt = myqueryi($db, $sql);

										while ($zeile = mysqli_fetch_array($query_ges_alt)) {
											if ($ges_alt == $zeile[0]) {
												echo "<option selected>$zeile[0]</option>";
											}
											else {
												echo "<option>$zeile[0]</option>";
											}
										}
									echo "</select>";
						echo "</td>";
						echo "<td>Tarif</td>";
	  					echo "<td>	<select name=\"tarif_alt_neu\">";
	   									$sql = "SELECT tarif_alt FROM produkt_alt, tarif_alt, ges_alt ";
										$sql .= " WHERE produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
										$sql .= " AND ges_alt.ges_alt = '$ges_alt' ";
										$sql .= " ORDER BY tarif_alt ASC ";
										$query_tarif_alt = myqueryi($db, $sql);
										while ($zeile = mysqli_fetch_array($query_tarif_alt)) {
											if ($tarif_alt == $zeile[0]) {
												echo "<option selected>$zeile[0]</option>";
											}
											else {
												echo "<option>$zeile[0]</option>";
											}
										}
									echo "</select>";
									echo "<input type=\"submit\" name=\"talt\" value = \"?\" class=\"suche\"/>";
						echo "</td>";
    				echo "</tr>";


					echo "<tr bgcolor= \"$bg_ges_alt\">";
      					echo "<td width=\"89\"><span style = \"font-weight:bold;\">G.-neu</span></td>";
      					echo "<td><input type=\"text\" class = \"eingabe\" name=\"ges_alt_input_neu\" size =\"20\" maxlength=\"20\" value = \"$ges_alt_input\"</td>";
	  	  				echo "<td width=\"89\"><span style = \"font-weight:bold;\">T.-neu</span></td>";
	  					echo "<td><input type=\"text\" class = \"eingabe\" name=\"tarif_alt_input_neu\" size =\"20\" maxlength=\"20\" value = \"$tarif_alt_input\"</td>";
    				echo "</tr>";
			 ?>
			 <tr bgcolor="<?php echo "$bg_ges_alt";?>">
				<td width="89">vom</td>
				<?php	echo "<td bgcolor=\"$bg_vom\">";
						if (($v_tag === "00") AND ($v_monat === "00") AND ($v_jahr === "00")) {
	  						echo "<input type=\"text\"  class=\"eingabe\" name=\"v_tag_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"v_monat_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"v_jahr_neu\" size = \"2\" maxlength =\"2\">";
						}
						else {
							echo "<input type=\"text\"  class=\"eingabe\" name=\"v_tag_neu\" size = \"2\" maxlength =\"2\" value = \"$v_tag\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"v_monat_neu\" size = \"2\" maxlength =\"2\" value = \"$v_monat\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"v_jahr_neu\" size = \"2\" maxlength =\"2\" value = \"$v_jahr\">";
						}
					echo "</td>";
				?>
				<td>bis</td>
				<?php	echo "<td bgcolor=\"$bg_bis\">";
						if (($b_tag === "00") AND ($b_monat === "00") AND ($b_jahr === "00")) {
	  						echo "<input type=\"text\"  class=\"eingabe\" name=\"b_tag_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"b_monat_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"b_jahr_neu\" size = \"2\" maxlength =\"2\">";
						}
						else {
							echo "<input type=\"text\"  class=\"eingabe\" name=\"b_tag_neu\" size = \"2\" maxlength =\"2\" value = \"$b_tag\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"b_monat_neu\" size = \"2\" maxlength =\"2\" value = \"$b_monat\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"b_jahr_neu\" size = \"2\" maxlength =\"2\" value = \"$b_jahr\">";
						}
					echo "</td>";
				?>
		  		</tr>

	<?php
				//Es folgt die Eingabe der neuen Gesellschaft = Vertragsabschluss ------------------------------------------------------------------
				//Nur für Administrator möglich - Telefonist sieht leere Felder --------------------------------------------------------------------

					if ($gruppe == 'Administrator') {
				?>

						<tr bgcolor="<?php echo "$bg_ges_neu";?>">
      					<td colspan="4" align = "center"><span style = "font-weight:bold; color:red;">Gesellschaft und Tarif - NEU == VERTRAGSABSCHLUSS</span></td>
						</tr>

	  <?php
						// Wenn Gesellschaft neu existiert, wird eine Zeile mit Gesellschaft neu und Tarif zum ändern angezeigt

							echo "<tr bgcolor= \"$bg_ges_neu\">";
			 					echo "<td width=\"89\">Gesell.</td>";
								echo "<td>	<select name=\"ges_neu_neu\">";
	   									$sql = "SELECT ges_neu FROM ges_neu ORDER BY ges_neu ASC ";
										$query_ges_neu = myqueryi($db, $sql);
										while ($zeile = mysqli_fetch_array($query_ges_neu)) {
											if ($ges_neu == $zeile[0]) {
												echo "<option selected>$zeile[0]</option>";
											}
											else {
												echo "<option>$zeile[0]</option>";
											}
										}
									echo "</select>";
						echo "</td>";
						echo "<td>Tarif</td>";
	  					echo "<td>	<select name=\"tarif_neu_neu\">";
	   									$sql = "SELECT tarif_neu FROM produkt_neu, tarif_neu, ges_neu ";
										$sql .= " WHERE produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
										$sql .= " AND ges_neu.ges_neu = '$ges_neu' ";
										$sql .= " ORDER BY tarif_neu ASC ";
										$query_tarif_neu = myqueryi($db, $sql);
										while ($zeile = mysqli_fetch_array($query_tarif_neu)) {
											if ($tarif_neu == $zeile[0]) {
												echo "<option selected>$zeile[0]</option>";
											}
											else {
												echo "<option>$zeile[0]</option>";
											}
										}
									echo "</select>";
									echo "<input type=\"submit\" name=\"tneu\" value = \"?\" class=\"suche\"/>";
						echo "</td>";
    				echo "</tr>";

			?>
			<tr bgcolor="<?php echo "$bg_ges_neu";?>">
				<td width="89">seit</td>
				<?php	echo "<td bgcolor=\"$bg_seit\">";
						if (($s_tag === "00") AND ($s_monat === "00") AND ($s_jahr === "00")) {
	  						echo "<input type=\"text\"  class=\"eingabe\" name=\"s_tag_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"s_monat_neu\" size = \"2\" maxlength =\"2\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"s_jahr_neu\" size = \"2\" maxlength =\"2\">";
						}
						else {
							echo "<input type=\"text\"  class=\"eingabe\" name=\"s_tag_neu\" size = \"2\" maxlength =\"2\" value = \"$s_tag\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"s_monat_neu\" size = \"2\" maxlength =\"2\" value = \"$s_monat\"> . ";
        					echo "<input type=\"text\"  class=\"eingabe\" name=\"s_jahr_neu\" size = \"2\" maxlength =\"2\" value = \"$s_jahr\">";
						}
					echo "</td>";
				?>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
	  		</tr>

			<tr bgcolor="<?php echo "$bg_ges_neu";?>">
				<td width="89">Vo-Nr.</td>
	  			<td><input type="text" class="eingabe" name="vonr_neu" size = "20" maxlength ="20" value = "<?php echo "$vonr";?>"/></td>
				<td>Einzug</td>
				<td> <?php
							if ($einzug == 0) {
								echo "<input type=\"checkbox\" name=\"einzug_neu\">";
							}
							else {
								echo "<input type=\"checkbox\" name=\"einzug_neu\" checked>";
							}
						?>
				</td>
	  		</tr>

			<tr bgcolor="<?php echo "$bg_ges_neu";?>">
				<td width="89">Storno</td>
				<?php if ($storno != '0') {
						echo "<td><input type=\"text\" class=\"eingabe\" name=\"storno_neu\" size = \"20\" maxlength =\"20\" value = \"$storno\"/></td>";
					}
					else {
						echo "<td><input type=\"text\" class=\"eingabe\" name=\"storno_neu\" size = \"20\" maxlength =\"20\"></td>";
					}
				?>

				<td>&nbsp;</td>
				<td>&nbsp;</td>
	  		</tr>

   <?php
				}	// Ende Administrator Gesellschaft neu

	// Anzeige Gesellschaft neu für Aquise - nur Anzeige, keine Eingabe möglich

				elseif ($gruppe == 'Aquise') {
			?>
						<tr bgcolor="<?php echo "$bg_ges_neu";?>">
      					<td colspan="4" align = "center"><span style = "font-weight:bold; color:red;">Gesellschaft und Tarif - NEU (Eingabe nur für Administrator)</span></td>
						</tr>

	  <?php
						// Wenn Gesellschaft neu existiert, wird eine Zeile mit Gesellschaft neu und Tarif zum ändern angezeigt

							echo "<tr bgcolor= \"$bg_ges_neu\">";
			 					echo "<td width=\"89\">Gesell.</td>";
								echo "<td>$ges_neu</td>";
								echo "<td>Tarif</td>";
								echo "<td>$tarif_neu</td>";
	   						echo "</tr>";


							echo "<tr bgcolor= \"$bg_ges_neu\">";
			 					echo "<td width=\"89\">seit</td>";
								echo "<td bgcolor=\"$bg_seit\">";
								if (($s_tag === "00") AND ($s_monat === "00") AND ($s_jahr === "00")) {

								}
								elseif (!empty($s_tag) AND !empty($s_monat) AND !empty($s_jahr)) {
									echo "$s_tag.$s_monat.$s_jahr";
								}
								echo "</td>";
								echo "<td>&nbsp;</td>";
								echo "<td>&nbsp;</td>";
	   						echo "</tr>";

							echo "<tr bgcolor= \"$bg_ges_neu\">";
								echo "<td width=\"89\">Vo-Nr.</td>";
								echo "<td>$vonr</td>";
								echo "<td>Einzug</td>";
								echo "<td>";
									if ($einzug == 1) {
										echo "ja";
									}
								echo "</td>";
	   						echo "</tr>";

							echo "<tr bgcolor= \"$bg_ges_neu\">";
								echo "<td width=\"89\">Storno</td>";
								echo "<td>$storno</td>";
								echo "<td>&nbsp;</td>";
								echo "<td>&nbsp;</td>";
	   						echo "</tr>";
				}	// Ende Anzeige Gesellschaft neu für Aquise
			?>

			<tr bgcolor="<?php echo "$bg";?>">
      			<td width="89">Bemerkung</td>
      			<td colspan="3"><textarea cols="50" rows="8" class = "eingabe" name="rem_termin_neu"><?php echo "$rem_termin"; ?></textarea></td>
    		</tr>
		</table>
	</td>

<!-- Ende Zelle zur Darstellung Termindaten ----------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- Zelle zur Darstellung Checkboxen  ---------------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

		<td align = "center" valign = "top">
			<table cellpadding = "2" bgcolor = "#006699">
				 <tr bgcolor="#ffffcc">
      				<td align = "center" valign = "top"><span style = "font-weight:bold;">ne</span><br />
		 <?php
							if ($ne == 1) {
								echo "<input type=\"checkbox\" name=\"ne_neu\" checked>";
							}
							else {
								echo "<input type=\"checkbox\" name=\"ne_neu\">";
							}
						?>
					</td>
				</tr>

				<tr bgcolor="<?php echo "$bg";?>"><td><img src="../images/100px.gif" width="100" height="1" /></td></tr>

	<?php
					if ($doppler == 1) { echo "<tr bgcolor=\"red\">"; }					// Termin belegt und "2x" nicht angeklickt - Zeile in Rot
					else { echo "<tr bgcolor=\"#ffff99\">"; }
				?>
      				<td align = "center" valign = "top"><span style = "font-weight:bold;">2x</span><br />
		 <?php
							if ($doppelt == 1) { echo "<input type=\"checkbox\" name=\"doppelt_neu\" checked>"; }
							else { echo "<input type=\"checkbox\" name=\"doppelt_neu\">"; }
						?>
					</td>
				</tr>

	<?php
				//if (empty($t_id) OR (!empty($t_id) AND ($termin[erledigt_date] == '0000-00-00'))     ) {
				//if (empty($t_id) OR (!empty($t_id) AND ($termin[erledigt_date] > '0000-00-00'))) {
				?>
				<tr bgcolor="<?php echo "$bg";?>"><td><img src="../images/100px.gif" width="100" height="1" /></td></tr>

				 <tr bgcolor="#999999">
      				<td align = "center" valign = "top">
	   					<span style = "font-weight:bold;">Nichtkunde</span><br />
		 <?php
							if ($nichtkunde == 0) { echo "<input type=\"checkbox\" name=\"nichtkunde_neu\">"; }
							else { echo "<input type=\"checkbox\" name=\"nichtkunde_neu\" checked>"; }
						?>
					</td>
				</tr>
	<?php
				//}

 				if (!empty($t_id) AND ($termin[abschluss] == '0')) {
				?>
				<tr bgcolor="<?php echo "$bg";?>"><td><img src="../images/100px.gif" width="100" height="1" /></td></tr>

				<tr bgcolor="#FFE6CC">
      				<td align = "center" valign = "top">
	   					<span style = "font-weight:bold;">Termin<br />stornieren</span><br />
		   <?php
							if ($alt == 0) { echo "<input type=\"checkbox\" name=\"alt_neu\">"; }
							else { echo "<input type=\"checkbox\" name=\"alt_neu\" checked>"; }
						?>
					</td>
				</tr>
	<?php
				}
				?>

				<?php // diese Zeile erscheint nur, wenn ein bestehender termin angeklickt wird

					if (!empty($t_id) AND ($termin[erledigt_date] == '0000-00-00' OR empty($termin[erledigt_date])) AND $termin[wiedervorlage] == 0) {
						echo"<tr bgcolor=\"$bg\"><td><img src=\"../images/100px.gif\" width=\"100\" height=\"1\" /></td></tr>";
						echo"<tr bgcolor=\"#FFD700\">";
      						echo"<td align = \"center\" valign = \"top\">";
	   							echo"<span style = \"font-weight:bold;\">Termin<br />abschließen</span><br />";

									if ($absch == 0) { echo "<input type=\"checkbox\" name=\"absch_neu\">"; }
									else { echo "<input type=\"checkbox\" name=\"absch_neu\" checked>"; }

							echo"</td>";
						echo"</tr>";
					}
				?>

				<tr bgcolor="<?php echo "$bg";?>"><td><img src="../images/100px.gif" width="100" height="1" /></td></tr>

				<tr bgcolor="#00EAFF">
      				<td align = "center" valign = "top">
	   					<span style = "font-weight:bold;">kalter T./AB.</span><br />
		   <?php
							if ($kalt == 0) { echo "<input type=\"checkbox\" name=\"kalt_neu\">"; }
							else { echo "<input type=\"checkbox\" name=\"kalt_neu\" checked>"; }
						?>
					</td>
				</tr>

				<?php // diese Zeile erscheint nur, wenn Kunde bereits vorhanden und Daten nur geändert werden sollen und KEIN Termin ausgewählt sit

					if (!empty($kunden_id) AND empty($t_id)) {
						echo"<tr bgcolor=\"$bg\"><td><img src=\"../images/100px.gif\" width=\"100\" height=\"1\" /></td></tr>";
						echo"<tr bgcolor=\"#ff9966\">";
      						echo"<td align = \"center\" valign = \"top\">";
	   							echo"<span style = \"font-weight:bold;\">neuer Kunde</span><br />";

									if ($new == 0) { echo "<input type=\"checkbox\" name=\"new_neu\">"; }
									else { echo "<input type=\"checkbox\" name=\"new_neu\" checked>"; }

							echo"</td>";
						echo"</tr>";
					}
				?>

			</table>
		</td>

<!-- Ende Zelle zur Darstellung Checkboxen ------------------------------------------------------------------------------------------------------------------------>
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- Zelle zur Darstellung Kundendaten ---------------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->

		<td align = "left" valign = "top">
			<table cellpadding = "2" bgcolor = "#006699">

				<tr bgcolor="<?php echo "$bg";?>">
      				<td width="89">Vorname</td>
      				<td width="130" bgcolor="<?php echo "$bg_vorname";?>"><input type="text" class = "eingabe" name="vorname_neu" maxlength = "20" size = "20" value = "<?php echo "$vorname"; ?>"/></td>
      				<td width="89" bgcolor="<?php echo "$bg_name";?>">Name</td>
      				<td><input type="text" class = "eingabe" name="name_neu" maxlength = "25" size = "20" value = "<?php echo "$name"; ?>"/></td>
    			</tr>

				<tr bgcolor="<?php echo "$bg_plz_alt";?>">
      				<td colspan="4" align = "center"><span style = "font-weight:bold; color:red;">PLZ, Ort, Ortsteil - PLZ schon VORHANDEN!</span></td>
				</tr>

				<tr bgcolor="<?php echo "$bg_plz_alt";?>">
      				<td width="89">PLZ</td>
      				<td>	<select name="plz_neu" class = "eingabe">
		 <?php
	  							$sql  = "SELECT plz FROM plz ORDER BY plz ASC";
								$query_plz = myqueryi($db, $sql);
								for ($j = 0; $j < mysqli_num_rows($query_plz); $j++)	{			// Anzahl der Datensätze
									$zeile = mysqli_fetch_row($query_plz);						// Schleife für Daten-Zeilen
									if 	($zeile[0] == $plz) {
										echo "<option selected>$zeile[0]</option>";
									}
									else {
										echo "<option>$zeile[0]</option>";
									}
								}
	 						?>
	  					</select>
						<input type="submit" name="plzneu" value = "?" class="suche"/>
					</td>
      				<td width="89">&nbsp;</td>
      				<td>&nbsp;</td>
    			</tr>

				<tr bgcolor="<?php echo "$bg_plz_alt";?>">
      				<strong><td width="89">Ort</td></strong>
      				<td>	<select name="ort_neu" class = "eingabe">
							<?php	$sql = "SELECT DISTINCT ort FROM poo, ort, plz, ortsteil ";
								$sql .= " WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id ";
								$sql .= " AND plz.plz = '$plz' ";
								$sql .= " ORDER BY ort ASC ";
								$query_ort = myqueryi($db, $sql);

								for ($j = 0; $j < mysqli_num_rows($query_ort); $j++)	{			// Anzahl der Datensätze
									$zeile = mysqli_fetch_row($query_ort);						// Schleife für Daten-Zeilen
									if 	($zeile[0] == $ort) {
										echo "<option selected>$zeile[0]</option>";
									}
									else {
										echo "<option>$zeile[0]</option>";
									}
								}
							?>
							</select>
							<input type="submit" name="ortneu" value = "?" class="suche"/>
					</td>
      				<td width="89"><span style = "font-weight:bold;">Ort-neu</span></td>
      				<td width="130" bgcolor="<?php echo "$bg_ort";?>"><input type="text" class = "eingabe" name="ort_input_neu" maxlength = "30" size = "20" value = "<?php echo "$ort_input"; ?>"/></td>
    			</tr>

				<tr bgcolor="<?php echo "$bg_plz_alt";?>">
      				<td width="89">Ortsteil</td>
      				<td>	<select name="ot_neu" class = "eingabe">
		 <?php
	  							$sql = "SELECT ortsteil FROM poo, plz, ort, ortsteil ";
								$sql .= "WHERE poo.ortsteil_id = ortsteil.ortsteil_id AND poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id ";
								$sql .= "AND ort.ort = '$ort' ";
								$query_ot = myqueryi($db, $sql);

								for ($j = 0; $j < mysqli_num_rows($query_ot); $j++)	{			// Anzahl der Datensätze
									$zeile = mysqli_fetch_row($query_ot);						// Schleife für Daten-Zeilen
									if 	($zeile[0] == $ot) {
										echo "<option selected>$zeile[0]</option>";
									}
									else {
										echo "<option>$zeile[0]</option>";
									}
								}
	 						?>
	  						</select>
					</td>
       				<td width="89"><span style = "font-weight:bold;">Ot-neu</span></td>
      				<td width="130" bgcolor="<?php echo "$bg_ot";?>"><input type="text" class = "eingabe" name="ot_input_neu" maxlength = "30" size = "20" value = "<?php echo "$ot_input"; ?>"/></td>
    			</tr>


<!-- PLZ / Ort / Ortsteil neu ---------------------------------------------------------------------------------------------------------------------------->
				<tr bgcolor="<?php echo "$bg_plz_neu";?>">
      				<td colspan="4" align = "center"><span style = "font-weight:bold; color:red;">PLZ, Ort, Ortsteil - komplett NEU!<br />
					ACHTUNG: Die nachfolgenden Felder ÜBERSCHREIBEN die obenstehenden!!</span></td>
				</tr>

				<tr bgcolor="<?php echo "$bg_plz_neu";?>">
      				<td width="89"><span style = "font-weight:bold;">PLZ - neu</span></td>
      				<td bgcolor="<?php echo "$bg_plz";?>"><input type="text" class = "eingabe" name="plz_input_neu" maxlength = "5" size = "5" value = "<?php echo "$plz_input"; ?>"/></td>
					</td>
      				<td width="89"><span style = "font-weight:bold;">Ort-neu</span></td>
      				<td width="130" bgcolor="<?php echo "$bg_ortneu";?>"><input type="text" class = "eingabe" name="ort_input1_neu" maxlength = "30" size = "20" value = "<?php echo "$ort_input1"; ?>"/></td>
    			</tr>

				<tr bgcolor="<?php echo "$bg_plz_neu";?>">
      				<td width="89">&nbsp;</td>
      				<td>&nbsp;</td>
      				<td width="89"><span style = "font-weight:bold;">OT-neu</span></td>
      				<td bgcolor="<?php echo "$bg_otneu";?>"><input type="text" class = "eingabe" name="ot_input1_neu" maxlength = "25" size = "20" value = "<?php echo "$ot_input1"; ?>"/></td>
    			</tr>

				<tr bgcolor="<?php echo "$bg";?>">
      				<td width="89">Straße</td>
      				<td><input type="text" class = "eingabe" name="strasse_neu" maxlength = "60" size = "20" value = "<?php echo "$strasse"; ?>"/></td>
      				<td width="89">&nbsp;</td>
      				<td>&nbsp;</td>
    			</tr>

	  			<tr bgcolor="<?php echo "$bg";?>">
      				<td width="89">geboren</td>
      				<td bgcolor="<?php echo "$bg_geboren";?>">
			<?php
							if (($g_tag === "00") AND ($g_monat === "00") AND ($g_jahr === "00")) {
	  							echo "<input type=\"text\"  class=\"eingabe\" name=\"g_tag_neu\" size = \"2\" maxlength =\"2\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g_monat_neu\" size = \"2\" maxlength =\"2\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g_jahr_neu\" size = \"2\" maxlength =\"2\">";
							}
							else {
								echo "<input type=\"text\"  class=\"eingabe\" name=\"g_tag_neu\" size = \"2\" maxlength =\"2\" value = \"$g_tag\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g_monat_neu\" size = \"2\" maxlength =\"2\" value = \"$g_monat\"> . ";
        						echo "<input type=\"text\"  class=\"eingabe\" name=\"g_jahr_neu\" size = \"2\" maxlength =\"2\" value = \"$g_jahr\">";
							}
						?>
      				</td>
      				<td width="89">&nbsp;</td>
      				<td>&nbsp;</td>
   				 </tr>
		 		 <tr bgcolor="<?php echo "$bg";?>">
      				<td width="89">Vorwahl 1</td>
      				<td width="130" bgcolor="<?php echo "$bg_vorwahl1";?>"><input type="text" class = "eingabe" name="vorwahl1_neu" size="8" value = "<?php echo "$vorwahl1"; ?>"/></td>
      				<td width="">Telefon</td>
      				<td bgcolor="<?php echo "$bg_telefon";?>"><input type="text" class = "eingabe" name="telefon_neu" size="12" value = "<?php echo "$telefon"; ?>"/></td>
    			</tr>
    			<tr bgcolor="<?php echo "$bg";?>">
      				<td width="89">Vorwahl 2</td>
      				<td width="130" bgcolor="<?php echo "$bg_vorwahl2";?>"><input type="text" class = "eingabe" name="vorwahl2_neu" size="8" value = "<?php echo "$vorwahl2"; ?>"/></td>
      				<td width="">Tel. 2</td>
      				<td bgcolor="<?php echo "$bg_fax";?>"><input type="text" class = "eingabe" name="fax_neu" size="12" value = "<?php echo "$fax"; ?>"/></td>
    			</tr>

    			<tr bgcolor="<?php echo "$bg";?>">
     				<td width="89">E-Mail</td>
      				<td width="130"><input type="text" class = "eingabe" name="email_neu" size="20" maxlength = "20" value = "<?php echo "$email"; ?>"/></td>
      				<td width="89">&nbsp;</td>
      				<td>&nbsp;</td>
    			</tr>

				<tr bgcolor="<?php echo "$bg";?>">
     				<td width="89">Branche</td>
					<td>
        				<select name="branche_neu" class = "eingabe">
		  <?php
		   					$sql = "SELECT branche FROM branche WHERE branche_id > '1' ORDER BY branche ASC";
							$query_branche = myqueryi($db, $sql);

							echo " <option></option>";
							for ($j = 0; $j < mysqli_num_rows($query_branche); $j++)	{			// Anzahl der Datensätze
								$zeile = mysqli_fetch_row($query_branche);						// Schleife für Daten-Zeilen
								if ($zeile[0] == $branche) {
									echo " <option selected>$zeile[0]</option>";
								}
								else {
									echo " <option>$zeile[0]</option>";
								}
							}
	 					?>
    					</select>
     				</td>

      				<td width="89">Branche<br />neu</td>
      				<td width="130"><input type="text" class = "eingabe" name="branche_input_neu" size="20" maxlength = "20" value = "<?php echo "$branche_input"; ?>"/></td>
    			</tr>

   				<tr bgcolor="<?php echo "$bg";?>">
      				<td width="89">Bemerkung</td>
      				<td ><textarea cols="48" rows="4" class = "eingabe" name="rem_kunde_neu"><?php echo "$rem_kunde"; ?></textarea></td>
					<td ><strong>Datenschutz-<br>erklärung</strong></td>
					<td>
						<?php
							if ($datenschutz == 0) { echo "<input type=\"checkbox\" name=\"datenschutz\">"; }
							else { echo "<input type=\"checkbox\" name=\"datenschutz\" checked>"; }
						?>
						erhalten
					</td>
				</tr>
				</tr>
			</table>
		</td>

<!-- Ende Zelle zur Darstellung Kundendaten ----------------------------------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->


	</tr>

<!-- Ende Zeile zur Darstellung Termindaten / Checkboxen/ Kundendaten -------------------------------------------------------------------------------------------->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------->
<?php
	if (!isset($speichern) OR $norefresh !=0) {		// Buttons nur beim Programmstart oder bei fehlerhafter Eingabe sichtbar, nach Speichern ausgeblendet, um Mehrfachbetätigung auszuschließen
?>
		<tr bgcolor="<?php echo "$bg";?>">
      		<td colspan ="3" align = "center">
	  			<input type="hidden" name="tid" value = "<?php echo "$t_id"; ?>">								<!-- Termin-ID wird verdeckt mit übertragen -->
				<input type="hidden" name="kundenid" value = "<?php echo "$kunden_id"; ?>">					<!-- Kunden-ID wird verdeckt mit übertragen -->
				<input type="submit" name="test" value = "Testen!" class="testbreit"/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       			 <input type="submit" name="speichern" value = "Speichern" class="submittbreit"/>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="submit" name="reset" value = "Leeren" class="suchebreit"/>
      		</td>
    	</tr>
<?php
	}
?>
  </table>
</form>
<!-- Ende Tabelle Rand -->
</div>
</body>
</html>

<?php
	if (isset($speichern) AND $norefresh == 0) {
		flush();						// Ausgabepuffer an den Browser senden
		//usleep(3000000);				//Verzögerung um 3 Sekunden (usleep verlangt Mikrosekunden!)
		sleep(3);						//Verzögerung um 3 Sekunden
		//Nachladen der Termin-Übersicht-Seite im oberen Frame, AD und Start-/Endezeitraum sowie Anzeigeperiode werden übergeben
		if ($file == "termine") {
			echo "<script>onload=parent['uebersicht_oben'].location.href='uebersicht_anzeige.php?ad=$adtermin &aq=$aqtermin & start=$start & ende=$ende & zahl=$zahltermin'</script>";
		}
		//Nachladen der Termin-übersicht-Seite im oberen Frame, Kunden-Id wird als Variable übergeben
		else {
			echo "<script>onload=parent['suche_oben'].location.href='such_check.php?kd_id=$kunden_id'</script>";
		}
	}
?>
