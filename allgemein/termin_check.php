<?php
include ("include/ini.php");		// Session-Lifetime
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

sessiondauer();

$heute = date("Y-m-d");							// Datum von heute

// Hilfsvariablen zur Bestimmung der Breite von Trennstrichen etc. ---------------------------------------------------
	$spank = 10;						// Anzahl der anzuzeigenden Spalten für Kundendaten 		- ANPASSEN!!
	$spant = 21;						// Anzahl der anzuzeigenden Spalten für Termindaten 		- ANPASSEN!!

	$spankrem = $spank - 1;				// Anzahl der anzuzeigenden Spalten für Bemerkung Kunde		- nicht verändern!!
	$spantrem = $spant - 1;				// Anzahl der anzuzeigenden Spalten für Bemerkung Termin	- nicht verändern!!
				
	$spankadmin = $spank + 1;			// Kundendaten - Administrator (+ bearbeiten		 		- nicht verändern!!
	$spankaq = $spank + 1;				// Kundendaten - Telefonist (+ bearbeiten) 					- nicht verändern!!
	$spankad = $spank;					// Kundendaten - Aussendienst 								- nicht verändern!!

	$spantadmin = $spant + 1;			// Termindaten - Administrator (+ bearbeiten		) 		- nicht verändern!!
	$spantaq = $spant + 1;				// Termindaten - Telefonist (+ bearbeiten) 					- nicht verändern!!
	$spantad = $spant;					// Termindaten - Aussendienst 								- nicht verändern!!

// ---------------------------------------------------------------------------------------------------------------------


// Datenbankabfrage -----------------------------------------------------------------------------------------

	$kunden_id = $_GET["kd_id"];			// aus telefon_liste.php wird die Kunden-ID übermittelt

	// Abfrage Daten Kunden
		
	$sql = "SELECT kunden_id, vorname, name, plz, ort, ortsteil, strasse, ";
	$sql .= "vorwahl1, telefon, vorwahl2, fax, email, branche, geboren, rem_kunde, ";
	$sql .= "wiedervorlage, vertragskunde, belegt, nichtkunde, verzogen, neukunde, ne, handy, datenschutz ";
	$sql .= "FROM kunden, vorname, name, poo, plz, ort, ortsteil, vorwahl1, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id'  ";
	$sql .= "And kunden.vorname_id = vorname.vorname_id  AND kunden.name_id = name.name_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= "And kunden.vorwahl1_id = vorwahl1.vorwahl1_id And kunden.vorwahl2_id = vorwahl2.vorwahl2_id  And kunden.branche_id = branche.branche_id";
	$ergebnis = myqueryi($db, $sql);
	$kunde = mysqli_fetch_array($ergebnis);

	if (empty($kunde[geboren]) OR $kunde[geboren] == "0000-00-00") {		// kein Geburtsdatum in Datenbank
		$kunde[geboren] = "";												// Anzeige bleibt leer
	}
	else {																	// Geburtsdatum vorhanden
		$kunde[geboren] = mysqldate_in_de($kunde[geboren]);					// MySQL- in deutsches Datum umwandeln
	}

	// Termindaten zum Kunden ermitteln

	$sql = "SELECT termin_id, telefonist, aquiriert, termin, zeit, aussendienst, erledigt_date, ";
	$sql .= " termin.wiedervorlage_date, w_zeit, ";
	$sql .= " ges_alt, tarif_alt, von, bis, ";
	$sql .= " ges_neu, tarif_neu, seit, vonr, ";
	$sql .= " rem_termin, nichtkunde, abschluss, wiedervorlage, termin.storno, alt, kalt, einzug, edit ";
	$sql .= " FROM termin, produkt_alt, ges_alt, tarif_alt, produkt_neu, ges_neu, tarif_neu ";
	$sql .= " WHERE termin.kd_id = '$kunden_id' ";
	$sql .= " And termin.produkt_alt_id = produkt_alt.produkt_alt_id And produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
	$sql .= " And termin.produkt_neu_id = produkt_neu.produkt_neu_id And produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
	$sql .= " ORDER BY edit DESC, termin_id DESC ";
	$abfrage = myqueryi($db, $sql);
	$anzahl = mysqli_num_rows($abfrage);
	
// Ende Datenbankabfrage ---------------------------------------------------------------------------------------

?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
<title>Kd-Termine</title>
	<!-- allgemein/termin_check.php -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body { margin-left: 5px; margin-top: 5px; margin-right: 5px; margin-bottom: 5px; }
.headline { font-weight:bold; font-size: 14px; color: blue; line-height:160%; padding-left:10px; }
.kundekopf { font-weight:bold; line-height:160%; }
.terminkopf { font-weight:bold; line-height:160%; }

-->
</style>
</head>
<body>
<div align = "center">
<table width="1200" border="0" cellpadding="0" cellspacing="3">
	<?php	// Überschrift: Daten Kunde
		echo "<tr bgcolor = \"#eeeeee\">";
		echo "<td valign = \"middle\" align = \"left\" class=\"headline\">Kunden-ID: $kunde[kunden_id]&nbsp;-&nbsp;$kunde[vorname]&nbsp;$kunde[name] - Telefon ($kunde[vorwahl1]) $kunde[telefon]</td>";
	?>
<tr>
<td>

<table width="100%" cellpadding="2" cellspacing="0" bgcolor="#eeeeee" border="1" rules="rows">		<!-- Beginn Tabelle Kundendaten -->
<?php
	// Tabellenkopf Kundendaten ----------------------------------------------------------------------------------------------
		echo "<tr>";
			echo "<td valign = \"middle\" align = \"left\" width = \"30\" class=\"kundekopf\">&nbsp;</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"40\" class=\"kundekopf\">PLZ</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"60\" class=\"kundekopf\">Ort</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"80\" class=\"kundekopf\">Ortsteil</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"180\" class=\"kundekopf\">Straße</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"40\" class=\"kundekopf\">Vorwahl2</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"40\" class=\"kundekopf\">Telefon2</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"80\" class=\"kundekopf\">E-Mail</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"70\" class=\"kundekopf\">geboren</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"100\" class=\"kundekopf\">Branche</td>";
			echo "<td valign = \"middle\" align = \"left\" width = \"40\" class=\"kundekopf\">Datenschutz</td>";
		echo "</tr>";
	// Ende Tabellenkopf Kundendaten -----------------------------------------------------------------------------------------
	
	// Trennstrich zwischen Tabellenkopf und -daten für Kundendaten
		//echo "<tr><td colspan = \"$spank\" valign = \"middle\"><hr></td></tr>";
	
	// Anzeige Kundendaten ------------------------------------------------------------------------------------------------------

		echo "<tr>";
			echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"termin_neu.php?kd_id=$kunden_id\" target=\"_self\">
				  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"kundendaten\" title=\"kundendaten\" border=\"0\" /></a></td>";
			echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[plz]</td>";
			echo "<td valign = \"top\" align = \"left\" width = \"60\">$kunde[ort]</td>";
			echo "<td valign = \"top\" align = \"left\" width = \"80\">$kunde[ortsteil]</td>";
			echo "<td valign = \"top\" align = \"left\" width = \"180\">$kunde[strasse]</td>";
			echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[vorwahl2]</td>";
			echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[fax]</td>";
			echo "<td valign = \"top\" align = \"left\" width = \"80\">$kunde[email]</td>";
			echo "<td valign = \"top\" align = \"left\" width = \"70\">$kunde[geboren]</td>";
			echo "<td valign = \"top\" align = \"left\" width = \"100\">$kunde[branche]</td>";
			echo "<td valign = \"top\" align = \"left\" width = \"40\">";
				if ($kunde[datenschutz] == 1) {
					echo "erhalten";
				}
				else {
					echo "---";
				}
echo "</td>";
		echo "</tr>";
		
		if (!empty($kunde[rem_kunde])) {		// Diese Zeile erscheint nur, wenn eine Kundenbemerkung in der Datenbank steht
			echo "<tr>";
			echo "<td valign = \"top\"  width = \"30\"><span style = \"font-weight:bold;\">Bem.:</td>";
			echo "<td valign = \"top\" align = \"left\" colspan = \"$spankrem\">$kunde[rem_kunde]</td>";
			echo "</tr>";
		}		// Ende Zeile Kundenbemerkung
		
	// Ende Anzeige Kundendaten -------------------------------------------------------------------------------------------
?>
</table>																				<!-- Ende Tabelle Kundendaten -->
</td></tr>

<tr><td>
<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#eeeeee" border="0">		<!-- Beginn Tabelle Termindaten -->
<tr><td valign = "top">

<?php
	// Tabellenkopf Termindaten --------------------------------------------------------------------------------------------

		echo "<tr>";
			echo "<td valign = \"middle\" align = \"center\" width = \"30\" class=\"terminkopf\">&nbsp;</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"40\" class=\"terminkopf\">Typ</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">Termin</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"50\" class=\"terminkopf\">Zeit</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"40\" class=\"terminkopf\">Tel.</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">angerufen</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"40\" class=\"terminkopf\">AD</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">erledigt</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">W-Datum</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"50\" class=\"terminkopf\">W-Zeit</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">Ges.-alt</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">Tarif-alt</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">von</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">bis</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">Ges.-neu</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">Tarif-neu</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"70\" class=\"terminkopf\">seit</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"40\" class=\"terminkopf\">VoNr</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"40\" class=\"terminkopf\">Storno</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"40\" class=\"terminkopf\">Bank</td>";
			echo "<td valign = \"middle\" align = \"center\" width = \"40\" class=\"terminkopf\">T_ID</td>";
		echo "</tr>";
	// Ende Tabellenkopf Termindaten ------------------------------------------------------------------------------------------
	
	// Trennstrich zwischen Tabellenkopf und -daten für Termindaten
	
		echo "<tr height=\"3\"><td colspan = \"$spant\" valign = \"middle\"><hr></td></tr>";
		
		
	// Anzeige Termindaten ------------------------------------------------------------------------------------------------------
	
	for ($i = 0; $i < $anzahl; $i++) {
				
		$termin = mysqli_fetch_array($abfrage);
		
	// deutsches Datum für Termin
		if (empty($termin[termin]) OR $termin[termin] == "0000-00-00") { $termin[termin] = ""; }
		else { $termin[termin] = mysqldate_in_de($termin[termin]); }
		
	// deutsches Datum für erledigt
		if (empty($termin[erledigt_date]) OR $termin[erledigt_date] == "0000-00-00") { $termin[erledigt_date] = ""; }
		else { $termin[erledigt_date] = mysqldate_in_de($termin[erledigt_date]); }
		
	// deutsches Datum für Wiedervorlage
		if (empty($termin[wiedervorlage_date]) OR $termin[wiedervorlage_date] == "0000-00-00") { $termin[wiedervorlage_date] = ""; }
		else { $termin[wiedervorlage_date] = mysqldate_in_de($termin[wiedervorlage_date]); }
		
	// deutsches Datum für von
		if (empty($termin[von]) OR $termin[von] == "0000-00-00") { $termin[von] = ""; }
		else { $termin[von] = mysqldate_in_de($termin[von]); }
		
	// deutsches Datum für bis
		if (empty($termin[bis]) OR $termin[bis] == "0000-00-00") { $termin[bis] = ""; }
		else { $termin[bis] = mysqldate_in_de($termin[bis]); }
		
	// deutsches Datum für seit
		if (empty($termin[seit]) OR $termin[seit] == "0000-00-00") { $termin[seit] = ""; }
		else { $termin[seit] = mysqldate_in_de($termin[seit]); }
		
	// 00:00 bei Zeit ausblenden
		if (empty($termin[zeit]) OR $termin[zeit] == "00:00") { $termin[zeit] = ""; }
		
	// 00:00 bei W-Zeit ausblenden
		if (empty($termin[w_zeit]) OR $termin[w_zeit] == "00:00") {	$termin[w_zeit] = ""; }

	// aquiriert in Datum und Zeit trennen, deutsches Datum, bei Zeit sekunden ausblenden
		$aq = explode(" ", $termin[aquiriert]);
		$aq_datum = mysqldate_in_de($aq[0]);
		$aq_zeit = substr($aq[1], 0, 5);
		
		if ($aq_zeit == "00:00") { $aq = $aq_datum; }
		else { $aq = $aq_datum . "<br>" .$aq_zeit; }
		
	//storno
		if ($termin[storno] != '0') { $storno = $termin[storno]; }
		else { $storno = "0"; }
		
	// einzug
		if (empty($termin[einzug])) { $einzug = ""; }
		else { $einzug = "1"; }
		
		
	// Termintyp ermitteln und Hintergrundfarbe der Terminzeile festlegen 
		if (!empty($termin[storno])) { $typ = "ST"; $bg = "#ff3333"; }
		elseif ($termin[abschluss] == '1' AND $termin[kalt] == '0' ) { $typ = "AB"; $bg = "#00FF00"; }			// Abschluss
		elseif ($termin[abschluss] == '1' AND $termin[kalt] == '1' ) { $typ = "KAB"; $bg = "#00A8FF"; }			// kalter Abschluss
		elseif ($termin[abschluss] == '0' AND $termin[kalt] == '1' ) { $typ = "Kalt"; $bg = "#00EAFF"; }		// kalter Termin
		elseif ($termin[nichtkunde] == '1') { $typ = "NK"; $bg = "#999999"; }									// Nichtkunde
		elseif ($termin[wiedervorlage] == '1' AND $termin[alt] == '0') { $typ = "WVL"; $bg = "#FFCCCC"; }		// Wiedervorlage
		elseif ($termin[wiedervorlage] == '1' AND $termin[alt] == '1') { $typ = "WVLalt"; $bg = "#FFE6CC"; }	// abgeschlossene Wiedervorlage
		elseif ($termin[erledigt_date] > 0000-00-00) { $typ = "ToAs"; $bg = "#FFD700"; }						// abgeschlossener Termin ohne Abschluss
		else { $typ = "ToA"; $bg = "#FFFF00"; }																	// laufender termin
	
		// Beginn Datenausgabe
		
		echo "<tr bgcolor=$bg>";
		echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"termin_neu.php?kd_id=$kunden_id&t_id=$termin[termin_id]&quelle=check\" target=\"_self\">
				  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"Termin neu\" title=\"Termin neu\" border=\"0\" /></a></td>";
			echo "<td valign = \"top\" align = \"center\" width = \"40\">$typ</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[termin]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"50\">$termin[zeit]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[telefonist]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$aq</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[aussendienst]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[erledigt_date]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[wiedervorlage_date]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"50\">$termin[w_zeit]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[ges_alt]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[tarif_alt]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[von]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[bis]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[ges_neu]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[tarif_neu]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[seit]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[vonr]</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"40\">$storno</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"40\">$einzug</td>";
			echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[termin_id]</td>";
		echo "</tr>";
		
		if (!empty($termin[rem_termin])) {		// Diese Zeile erscheint nur, wenn eine Terminbemerkung in der Datenbank steht
			echo "<tr bgcolor=\"$bg\">";
			echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
			echo "<td valign = \"middle\" align = \"left\" colspan = \"$spant\">$termin[rem_termin]</td>";
			echo "</tr>";
		}

	// Trennstrich zwischen den einzelnen Terminen
		echo "<tr height=\"3\"><td colspan = \"$spant\" valign = \"middle\"><hr></td></tr>";
	}	// Ende Anzeige Termindaten ------------------------------------------------------------------------------------------------
?>
</table>
</td></tr>

<tr><td>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">		<!-- Beginn Tabelle Legende-->
	<tr>
	<td valign = "middle" align = "center" bgcolor ="#eeeeee"><span style = "line-height:160%">Legende:</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFFF00"><span style = "line-height:160%">ToA: Termin</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFD700"><span style = "line-height:160%">ToAs: Termin bearbeitet</span></td>
	<td valign = "middle" align = "center" bgcolor ="#00FF00"><span style = "line-height:160%">AB: Abschluss</span></td>
	<td valign = "middle" align = "center" bgcolor ="#00EAFF"><span style = "line-height:160%">kalt: kalter Termin</span></td>
	<td valign = "middle" align = "center" bgcolor ="#00A8FF"><span style = "line-height:160%">KAB: kalter Abschluss</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFCCCC"><span style = "line-height:160%">WVL: WVL aktiv</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFE6CC"><span style = "line-height:160%">WVLalt: WVL abgeschlossen</span></td>
	<td valign = "middle" align = "center" bgcolor ="#999999"><span style = "line-height:160%">NK: Nichtkunde</span></td>
	<td valign = "middle" align = "center" bgcolor ="#ff3333"><span style = "line-height:160%">ST: Storno</span></td>
	</tr>
	</table>
	</td>
</tr>
<?php 	echo "<tr bgcolor=\"white\">";
	echo "<td colspan = \"19\" align = \"center\"><a href=\"termin_neu.php\" target=\"_self\"><span style=\"font-size:14px; font-weight:bold; color: red; line-height:200%;\">Zur Dateneingabe&nbsp;</span><span style=\"font-size:10px; font-weight:bold; color: black;\">(leeres Formular)</span></a></td>";
	echo"</tr>";

?>

</td></tr></table>																		<!-- Ende Tabelle Termindaten -->
</td></tr></table>
</div>
</body>
</html>

<?php $close = mysqli_close($db); ?>