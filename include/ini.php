<?php
/**
 * Created by PhpStorm.
 * User: Michael Müller
 * Date: 21.08.2018
 * Time: 15:33
 */

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 7200);
ini_set('session.gc_maxlifetime', 7200);
//ini_set('session.save_path', '/sessions');

ini_set('display_errors', TRUE);

?>