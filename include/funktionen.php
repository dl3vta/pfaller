<?php

function sessiondauer() {

	ini_set("session.cookie_lifetime", 0);

	// Zeit bis zum »timeout« in Sekunden
		$session_timeout = 36000; // 36000 Sek./60 Sek. = 10 Stunden

	// Variable last_visit ist noch nicht vorhanden - wird neu gesetzt
	if (!isset($_SESSION['last_visit'])) {
		$_SESSION['last_visit'] = time();
	}

	// variable last visit ist älter als variable timeout -> session wird zerstört
	if((time() - $_SESSION['last_visit']) > $session_timeout) {
		session_destroy();
		// Aktion der Session wird erneut ausgeführt
	}

	//variable last visit wird gesetzt
	$_SESSION['last_visit'] = time();

}

// Funktion zur Mysql-Abfrage mit Fehlerausgabe
function myquery ($query) {
       $abfrage = mysql_query($query);
       if (mysql_errno())
           echo "MySQL error ".mysql_errno().": ".mysql_error()."\n<br>bei Ausführung von:<br>\n$query\n<br>";
       return $abfrage;
	   //mysql_close($conn);
	   $close = mysql_close($conn);
   }

// Funktion zur Mysqli-Abfrage mit Fehlerausgabe
function myqueryi($db, $sql) {
	global $db;
	$abfrage = mysqli_query($db, $sql);
	if (mysqli_errno($db))
		echo "MySQL error ".mysqli_errno($db).": ".mysqli_error($db)."\n<br>bei Ausführung von:<br>\n$sql\n<br>";
	return $abfrage;
}

// Funktion zur Aufbereitung von dynamischen (Formular-) Daten für Mysql-Abfragen
// verhindert Mysql_injektion
function quote_smart($variable) {
	$variable = trim($variable);		// Whitespaces entfernen
    // stripslashes, falls nötig
//	if (get_magic_quotes_gpc()) {
        $variable = stripslashes($variable);
	// }

    // quotieren, falls kein integer
   // if (!is_int($variable)) {
		$variable = strip_tags($variable);
		//$variable = mysqli_real_escape_string($db, $variable);
   // }
    return $variable;
}

// ermittelt eine ID aus der Datenbank bzw. fügt neuen Datensatz hinzu und gibt die ID zurück
// -------------------------------------------------------------------------------------------
function id_ermitteln($db, $sql1, $sql2) {
	$abfrage = myqueryi($db, $sql1);
	if ($ergebnis = mysqli_fetch_array($abfrage, MYSQLI_NUM)) {
		$id = $ergebnis[0];
	}
	else {
		$abfrage = myqueryi($db, $sql2);
		$id = mysqli_insert_id($db);
	}
	return $id;
}

//wandelt deutsches Datum nach MySQL-DATE.
//-------------------------------------------------------------
function dtdate_in_mysql($termin)						
{	
	list($tag, $monat, $jahr) = explode(".", $termin);
	
	if ($jahr >= 1000) {										//Jahr ist vierstellig
		return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
	}
	elseif ($jahr <= 20) {										//Jahr liegt zwischen 2000 und 2020
		$jahr = "20" . $jahr;
		return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
	}
	else {														//Jahr liegt zwischen 1921 und 1999
		$jahr = "19" . $jahr;
		return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
	}
    
}

//wandelt MySQL-DATE in deutsches Datum
//-------------------------------------------------------------
function mysqldate_in_de($termin)						
{	
	list($jahr, $monat, $tag) = explode("-", $termin);
	return sprintf("%02d.%02d.%04d", $tag, $monat, $jahr);
    
}

// gibt ein Deutsches Datum zurück
function datum($anzahl) {
	setlocale(LC_TIME, "de_DE.utf8");						// deutsche Benutzerumgebung eingestellt
	date_default_timezone_set("Europe/Berlin");				// Zeitzone eingestellt
	$heute = mktime(0,0,0,date("m"), date("d")+$anzahl);
	
	$wochentag = strftime("%a", $heute);						// Umstellung englischer Wochentag auf deutscher
	switch($wochentag) {
		case Sat:	
			$wochentag = "Sa";
			break;
		case Sun:
			$wochentag = "So";
			break;
		case Mon:
			$wochentag = "Mo";
			break;
		case Tue:	
			$wochentag = "Di";
			break;
		case Wed:
			$wochentag = "Mi";
			break;
		case Thu:
			$wochentag = "Do";
			break;
		case Fri:
			$wochentag = "Fr";
			break;
	}
	
	//$datum = (strftime("%a", $heute)) . "<br>" .(strftime("%d", $heute)) . "." . (strftime("%m", $heute)) . ".";
	
	$datum = ($wochentag) . "<br>" .(strftime("%d", $heute)) . "." . (strftime("%m", $heute)) . ".<br>" . (strftime("%Y", $heute));

	return $datum;
}

// diese Funktion ermittelt, ob eine Checkbox gesetzt ist oder nicht
function checkbox($checkbox) {
	if (isset($checkbox)) {	$checkbox = 1; }
	else { $checkbox = 0; }
	return $checkbox;
}

// alle Wiedervorlagen werden abgeschlossen
function wvl_alt($kunden_id, $heute, $speich, $telefonist) {
	$sql1 = "SELECT termin_id, rem_termin FROM termin WHERE kd_id = '$kunden_id' AND wiedervorlage = '1' AND alt = '0' ";
	$abfrage = myqueryi($db, $sql1);
	for ($z = 0; $z < mysqli_num_rows($abfrage); $z++) {		// Wiedervorlage auf alt setzen
		$daten = mysqli_fetch_array($abfrage);
		$rem_termin = $daten[rem_termin] . " ($speich st: $telefonist) ";
		$sql1 = "UPDATE termin SET  rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '1' ";
		$sql1 .= "WHERE termin_id = '$daten[termin_id]'";
		$query = myqueryi($db, $sql1);
	}
}

// alle offenen Termine in der Zukunft stornieren und in der Vergangenheit abschließen
function termin_alt($kunden_id, $heute, $speich, $telefonist) {
	$sql = " SELECT termin_id, rem_termin, termin FROM termin WHERE kd_id = '$kunden_id' AND termin >= '2000-01-01' AND erledigt_date = '0000-00-00' AND alt = '0' AND sperrzeit = '0' ";
	$query = myqueryi($db, $sql);
	if (mysqli_num_rows($query) > 0) {
		for ($y=0; $y < mysqli_num_rows($query); $y++) {
			$offen = mysqli_fetch_array($query);
			if ($offen[termin] >= $heute) {
				$rem_termin = $offen[rem_termin] . " (st $telefonist: $speich) ";
				$sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '1' WHERE termin_id = '$offen[termin_id]'";
			}
			else {
				$rem_termin = $offen[rem_termin] . " (nk $telefonist: $speich) ";
				$sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '0' WHERE termin_id = '$offen[termin_id]'";
			}
			$abfrage = myqueryi($db, $sql1);
		}
	}
}

// alle offenen Termine in der Zukunft stornieren und in der Vergangenheit zum Nichtkunden machen
function termin_nk($kunden_id, $heute, $speich, $telefonist) {
	$sql = " SELECT termin_id, rem_termin, termin FROM termin WHERE kd_id = '$kunden_id' AND termin >= '2000-01-01' AND erledigt_date = '0000-00-00' AND alt = '0' AND sperrzeit = '0' ";
	$query = myqueryi($db, $sql);
	if (mysqli_num_rows($query) > 0) {
		for ($y=0; $y < mysqli_num_rows($query); $y++) {
			$offen = mysqli_fetch_array($query);
			if ($offen[termin] >= $heute) {
				$rem_termin = $offen[rem_termin] . " (st $telefonist: $speich) ";
				$sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '1' WHERE termin_id = '$offen[termin_id]'";
			}
			else {
				$rem_termin = $offen[rem_termin] . " (nk $telefonist: $speich) ";
				$sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', nichtkunde = '1', alt = '0' WHERE termin_id = '$offen[termin_id]'";
			}
			$abfrage = myqueryi($db, $sql1);
		}
	}
}

// Die Funktion datum_check überprüft die Eingabe von Datumsangaben, es müssen alle 3 Werte (Tag, Monat, Jahr) zweistellig eingegeben werden
// für Tage und Monate sind die Eingaben von "0" und "00" nicht zugelassen, das Jahr kann den Wert "00" annehmen
// die Funktion testet, ob die Werte zweistellig eingegeben worden sind, und ob nur Zahlen auftreten
// in Abhängigkeit von Tag und Monat wird auch noch ein Maximalwert abgetestet (Tag: 31, Monat: 12)
// eine Überprufung des Schaltjahres findet nicht statt, für den Februar sind 29 Tage erlaubt
// der Rückgabewert ist das akkumulierte Fehlerprotokoll der Überprüfung

function datum_check($tag, $monat, $jahr, $muster, $art) {

	$datum_zahl = 0;													// Zähler für eingegebene Werte, alle 3 Werte (Tag, Monat, Jahr) müssen eingegeben werden
	if (!empty($tag) OR ($tag === "0") OR ($tag === "00")) {			// Überprüfung Tag
		if (($tag === '0') OR ($tag === '00')) {						// Tag darf nicht "0" oder "00" sein
			$fehler = $fehler . " - " . $art ."-Tag";					// Ausgabe des Fehlers
		}
		elseif (!empty($tag)) {											// Eingabe ist erfolgt
			if(!preg_match($muster, $tag) OR ($tag > 31)) {				// Muster passt nicht oder Zahl zu groß
				$fehler = $fehler . " - " . $art ."-Tag";				// Ausgabe des Fehlers
			}
		}
		$datum_zahl = $datum_zahl +1;									// Eingabe Tag ok
	}
	
	if (!empty($monat) OR ($monat === "0") OR ($monat === "00")) {		// Überprüfung Monat
		if (($monat === '0') OR ($monat === '00')) {					// Monat darf nicht "0" oder "00" sein
			$fehler = $fehler . " - " . $art ."-Monat";					// Ausgabe des Fehlers
		}
		elseif (!empty($monat)) {										// Eingabe ist erfolgt
			if(!preg_match($muster, $monat) OR ($monat > 12)) {			// Muster passt nicht oder Zahl zu groß
				$fehler = $fehler . " - " . $art ."-Monat";				// Ausgabe des Fehlers
			}
			elseif ($monat === "02") {														// Test auf Februar
				if ($tag > 29) {
					$fehler = $fehler . " - kein gültiger Tag für diesen Monat";			// Ausgabe des Fehlers
				}
			}
			elseif ($monat === "04" OR $monat === "06" OR $monat === "09" OR $monat === "11") {		// Test auf April, Juni, September, November
				if ($tag == 31) {
					$fehler = $fehler . " - kein gültiger Tag für diesen Monat";				// Ausgabe des Fehlers
				}
			}
		}
		$datum_zahl = $datum_zahl +1;									// Eingabe Monat ok
	}
	
	if (!empty($jahr) OR ($jahr === "0") OR ($jahr === "00")) {			// Überprüfung Jahr
		if (($jahr === "0")) {											// Jahr darf nicht "0" sein - "00" ist erlaubt!!
			$fehler = $fehler . " - " . $art ."-Jahr";					// Fehler
		}
		elseif (!empty($jahr)) {										// Eingabe erfolgt
			if(!preg_match($muster, $jahr)) {							// Muster passt nicht
				$fehler = $fehler . " - " . $art ."-Jahr";				// Fehler !!
			}
		}
		$datum_zahl = $datum_zahl +1;									// Eingabe Jahr ok
	}
	
																		// test, ob Tag, Monat und Jahr eingegeben worden sind
	if ($datum_zahl == 1 OR $datum_zahl == 2) {							// nur ein oder zwei Werte eingegeben - Fehlermeldung
		$fehler = $fehler . " - " . $art ."-Datum unvollständig";
	}
	return $fehler;																// Rückgabe
}



   
?>