<?php

// Funktion zur Mysql-Abfrage mit Fehlerausgabe
function myquery ($query) {
    $abfrage = mysql_query($query);
    if (mysql_errno())
        echo "MySQL error ".mysql_errno().": ".mysql_error()."\n<br>bei Ausf�hrung von:<br>\n$query\n<br>";
    return $abfrage;
    //mysql_close($conn);
    $close = mysql_close($conn);
}

// Funktion zur Aufbereitung von dynamischen (Formular-) Daten f�r Mysql-Abfragen
// verhindert Mysql_injektion
function quote_smart($variable) {
    $variable = trim($variable);		// Whitespaces entfernen
    // stripslashes, falls n�tig
    if (get_magic_quotes_gpc()) {
        $variable = stripslashes($variable);
    }

    // quotieren, falls kein integer
    if (!is_int($variable)) {
        $variable = strip_tags($variable);
        $variable = mysqli_real_escape_string($variable);
    }
    return $variable;
}

// ermittelt eine ID aus der Datenbank bzw. f�gt neuen Datensatz hinzu und gibt die ID zur�ck
// -------------------------------------------------------------------------------------------
function id_ermitteln($sql1, $sql2, $conn) {
    $abfrage = myquery($sql1, $conn);
    if ($ergebnis = mysql_fetch_array($abfrage, MYSQL_NUM)) {
        $id = $ergebnis[0];
    }
    else {
        $abfrage = myquery($sql2, $conn);
        $id = mysql_insert_id();
    }
    return $id;
}

//wandelt deutsches Datum nach MySQL-DATE.
//-------------------------------------------------------------
function dtdate_in_mysql($termin)
{
    list($tag, $monat, $jahr) = explode(".", $termin);

    if ($jahr >= 1000) {										//Jahr ist vierstellig
        return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
    }
    elseif ($jahr <= 20) {										//Jahr liegt zwischen 2000 und 2020
        $jahr = "20" . $jahr;
        return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
    }
    else {														//Jahr liegt zwischen 1921 und 1999
        $jahr = "19" . $jahr;
        return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
    }

}

//wandelt MySQL-DATE in deutsches Datum
//-------------------------------------------------------------
function mysqldate_in_de($termin)
{
    list($jahr, $monat, $tag) = explode("-", $termin);
    return sprintf("%02d.%02d.%04d", $tag, $monat, $jahr);

}

// gibt ein Deutsches Datum zur�ck
function datum($anzahl) {
    $heute = mktime(0,0,0,date("m"), date("d")+$anzahl);
    setlocale (LC_TIME, 'ge');								// deutsche Benutzerumgebung eingestellt

    $wochentag = strftime("%a", $heute);						// Umstellung englischer Wochentag auf deutscher
    switch($wochentag) {
        case Sat:
            $wochentag = "Sa";
            break;
        case Sun:
            $wochentag = "So";
            break;
        case Mon:
            $wochentag = "Mo";
            break;
        case Tue:
            $wochentag = "Di";
            break;
        case Wed:
            $wochentag = "Mi";
            break;
        case Thu:
            $wochentag = "Do";
            break;
        case Fri:
            $wochentag = "Fr";
            break;
    }

    //$datum = (strftime("%a", $heute)) . "<br>" .(strftime("%d", $heute)) . "." . (strftime("%m", $heute)) . ".";

    $datum = ($wochentag) . "<br>" .(strftime("%d", $heute)) . "." . (strftime("%m", $heute)) . ".<br>" . (strftime("%Y", $heute));

    return $datum;
}

// diese Funktion ermittelt, ob eine Checkbox gesetzt ist oder nicht
function checkbox($checkbox) {
    if (isset($checkbox)) {	$checkbox = 1; }
    else { $checkbox = 0; }
    return $checkbox;
}

// alle Wiedervorlagen werden abgeschlossen
function wvl_alt($kunden_id, $heute, $speich, $telefonist) {
    $sql1 = "SELECT termin_id, rem_termin FROM termin WHERE kd_id = '$kunden_id' AND wiedervorlage = '1' AND alt = '0' ";
    $abfrage = myquery($sql1, $conn);
    for ($z = 0; $z < mysql_num_rows($abfrage); $z++) {		// Wiedervorlage auf alt setzen
        $daten = mysql_fetch_array($abfrage);
        $rem_termin = $daten[rem_termin] . " ($speich st: $telefonist) ";
        $sql1 = "UPDATE termin SET  rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '1' ";
        $sql1 .= "WHERE termin_id = '$daten[termin_id]'";
        $query = myquery($sql1, $conn);
    }
}

// alle offenen Termine in der Zukunft stornieren und in der Vergangenheit abschlie�en
function termin_alt($kunden_id, $heute, $speich, $telefonist) {
    $sql = " SELECT termin_id, rem_termin, termin FROM termin WHERE kd_id = '$kunden_id' AND termin >= '2000-01-01' AND erledigt_date = '0000-00-00' AND alt = '0' AND sperrzeit = '0' ";
    $query = myquery($sql, $conn);
    if (mysql_num_rows($query) > 0) {
        for ($y=0; $y < mysql_num_rows($query); $y++) {
            $offen = mysql_fetch_array($query);
            if ($offen[termin] >= $heute) {
                $rem_termin = $offen[rem_termin] . " (st $telefonist: $speich) ";
                $sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '1' WHERE termin_id = '$offen[termin_id]'";
            }
            else {
                $rem_termin = $offen[rem_termin] . " (nk $telefonist: $speich) ";
                $sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '0' WHERE termin_id = '$offen[termin_id]'";
            }
            $abfrage = myquery($sql1, $conn);
        }
    }
}

// alle offenen Termine in der Zukunft stornieren und in der Vergangenheit zum Nichtkunden machen
function termin_nk($kunden_id, $heute, $speich, $telefonist) {
    $sql = " SELECT termin_id, rem_termin, termin FROM termin WHERE kd_id = '$kunden_id' AND termin >= '2000-01-01' AND erledigt_date = '0000-00-00' AND alt = '0' AND sperrzeit = '0' ";
    $query = myquery($sql, $conn);
    if (mysql_num_rows($query) > 0) {
        for ($y=0; $y < mysql_num_rows($query); $y++) {
            $offen = mysql_fetch_array($query);
            if ($offen[termin] >= $heute) {
                $rem_termin = $offen[rem_termin] . " (st $telefonist: $speich) ";
                $sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', alt = '1' WHERE termin_id = '$offen[termin_id]'";
            }
            else {
                $rem_termin = $offen[rem_termin] . " (nk $telefonist: $speich) ";
                $sql1 = "UPDATE termin SET rem_termin = '$rem_termin', erledigt_date = '$heute', nichtkunde = '1', alt = '0' WHERE termin_id = '$offen[termin_id]'";
            }
            $abfrage = myquery($sql1, $conn);
        }
    }
}

// Die Funktion datum_check �berpr�ft die Eingabe von Datumsangaben, es m�ssen alle 3 Werte (Tag, Monat, Jahr) zweistellig eingegeben werden
// f�r Tage und Monate sind die Eingaben von "0" und "00" nicht zugelassen, das Jahr kann den Wert "00" annehmen
// die Funktion testet, ob die Werte zweistellig eingegeben worden sind, und ob nur Zahlen auftreten
// in Abh�ngigkeit von Tag und Monat wird auch noch ein Maximalwert abgetestet (Tag: 31, Monat: 12)
// eine �berpr�fung des Schaltjahres findet nicht statt, f�r den Februar sind 29 Tage erlaubt
// der R�ckgabewert ist das akkumukierte Fehlerprotokoll der m�berpr�fung

function datum_check($tag, $monat, $jahr, $muster, $art) {

    $datum_zahl = 0;													// Z�hler f�r eingegebene Werte, alle 3 Werte (Tag, Monat, Jahr) m�ssen eingegeben werden
    if (!empty($tag) OR ($tag === "0") OR ($tag === "00")) {			// �berpr�fung Tag
        if (($tag === '0') OR ($tag === '00')) {						// Tag darf nicht "0" oder "00" sein
            $fehler = $fehler . " - " . $art ."-Tag";					// Ausgabe des Fehlers
        }
        elseif (!empty($tag)) {											// Eingabe ist erfolgt
            if(!preg_match($muster, $tag) OR ($tag > 31)) {				// Muster passt nicht oder Zahl zu gro�
                $fehler = $fehler . " - " . $art ."-Tag";				// Ausgabe des Fehlers
            }
        }
        $datum_zahl = $datum_zahl +1;									// Eingabe Tag ok
    }

    if (!empty($monat) OR ($monat === "0") OR ($monat === "00")) {		// �berpr�fung Monat
        if (($monat === '0') OR ($monat === '00')) {					// Monat darf nicht "0" oder "00" sein
            $fehler = $fehler . " - " . $art ."-Monat";					// Ausgabe des Fehlers
        }
        elseif (!empty($monat)) {										// Eingabe ist erfolgt
            if(!preg_match($muster, $monat) OR ($monat > 12)) {			// Muster passt nicht oder Zahl zu gro�
                $fehler = $fehler . " - " . $art ."-Monat";				// Ausgabe des Fehlers
            }
            elseif ($monat === "02") {														// Test auf Februar
                if ($tag > 29) {
                    $fehler = $fehler . " - kein g�ltiger Tag f�r diesen Monat";			// Ausgabe des Fehlers
                }
            }
            elseif ($monat === "04" OR $monat === "06" OR $monat === "09" OR $monat === "11") {		// Test auf April, Juni, September, November
                if ($tag == 31) {
                    $fehler = $fehler . " - kein g�ltiger Tag f�r diesen Monat";				// Ausgabe des Fehlers
                }
            }
        }
        $datum_zahl = $datum_zahl +1;									// Eingabe Monat ok
    }

    if (!empty($jahr) OR ($jahr === "0") OR ($jahr === "00")) {			// �berpr�fung Jahr
        if (($jahr === "0")) {											// Jahr darf nicht "0" sein - "00" ist erlaubt!!
            $fehler = $fehler . " - " . $art ."-Jahr";					// Fehler
        }
        elseif (!empty($jahr)) {										// Eingabe erfolgt
            if(!preg_match($muster, $jahr)) {							// Muster passt nicht
                $fehler = $fehler . " - " . $art ."-Jahr";				// Fehler !!
            }
        }
        $datum_zahl = $datum_zahl +1;									// Eingabe Jahr ok
    }

    // test, ob Tag, Monat und Jahr eingegeben worden sind
    if ($datum_zahl == 1 OR $datum_zahl == 2) {							// nur ein oder zwei Werte eingegeben - Fehlermeldung
        $fehler = $fehler . " - " . $art ."-Datum unvollst�ndig";
    }
    return $fehler;																// R�ckgabe
}




?>