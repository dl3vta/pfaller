<?php

// ---------------------------------------------------------------------------------//
// Dieses Script stellt f�r einen Kunden die "Terminhistorie" dar.					//
// Die Kunden-ID kommt von suche.php												//
// In Abh�ngigkeit von der Benutzergruppe des eingeloggten Users sind verschiedene	//
// Bearbeitungsm�glichkeiten gegeben:												//
// Administrator: kann Kunden und Termine bearbeiten und l�schen					//
// Telefonist: kann Kunden und Termine bearbeiten									//
// Au�endienstler: kann Daten nur ansehen 											//
// die Weiterverarbeitung der selektierten Daten geschieht in						//
// l�schen: loeschen.php															//
// bearbeiten: kunde_neu_php														//
// ---------------------------------------------------------------------------------//

session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

$gruppe = $_SESSION['benutzer_gruppen'];			// Gruppe des eingeloggten Benutzers

// Hilfsvariablen zur Bestimmung der Breite von Trennstrichen etc. ---------------------------------------------------
	$spank = 11;						// Anzahl der anzuzeigenden Spalten f�r Kundendaten 		- ANPASSEN!!
	$spant = 21;						// Anzahl der anzuzeigenden Spalten f�r Termindaten 		- ANPASSEN!!
	
	$spankrem = $spank - 1;				// Anzahl der anzuzeigenden Spalten f�r Bemerkung Kunde		- nicht ver�ndern!!
	$spantrem = $spant - 1;				// Anzahl der anzuzeigenden Spalten f�r Bemerkung Termin	- nicht ver�ndern!!
				
	$spankadmin = $spank + 2;			// Kundendaten - Administrator (+ bearbeiten/l�schen) 		- nicht ver�ndern!!
	$spankaq = $spank + 1;				// Kundendaten - Telefonist (+ bearbeiten) 					- nicht ver�ndern!!
	$spankad = $spank;					// Kundendaten - Aussendienst 								- nicht ver�ndern!!

	$spantadmin = $spant + 2;			// Termindaten - Administrator (+ bearbeiten/l�schen) 		- nicht ver�ndern!!
	$spantaq = $spant + 1;				// Termindaten - Telefonist (+ bearbeiten) 					- nicht ver�ndern!!
	$spantad = $spant;					// Termindaten - Aussendienst 								- nicht ver�ndern!!

// ---------------------------------------------------------------------------------------------------------------------


// Datenbankabfrage -----------------------------------------------------------------------------------------

	$kunden_id = $_GET["kd_id"];			// aus suche.php wird die Kunden-ID �bermittelt

	// Abfrage Daten Kunden
		
	$sql = "SELECT kunden_id, vorname, name, plz, ort, ortsteil, strasse, ";
	$sql .= "vorwahl1, telefon, vorwahl2, fax, email, branche, geboren, rem_kunde ";
	$sql .= "wiedervorlage, vertragskunde, belegt, nichtkunde, verzogen, neukunde, ne, handy ";
	$sql .= "FROM kunden, vorname, name, poo, plz, ort, ortsteil, vorwahl1, vorwahl2, branche ";
	$sql .= "WHERE kunden.kunden_id = '$kunden_id'  ";
	$sql .= "And kunden.vorname_id = vorname.vorname_id  AND kunden.name_id = name.name_id ";
	$sql .= "AND kunden.poo_id = poo.poo_id and poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= "And kunden.vorwahl1_id = vorwahl1.vorwahl1_id And kunden.vorwahl2_id = vorwahl2.vorwahl2_id  And kunden.branche_id = branche.branche_id";
	$ergebnis = myquery($sql, $conn);
	$kunde = mysql_fetch_array($ergebnis);

	if (empty($kunde[geboren]) OR $kunde[geboren] == "0000-00-00") {		// kein Geburtsdatum in Datenbank
		$kunde[geboren] = "";												// Anzeige bleibt leer
	}
	else {																	// Geburtsdatum vorhanden
		$kunde[geboren] = mysqldate_in_de($kunde[geboren]);					// MySQL- in deutsches Datum umwandeln
	}
	
	// Status des Kunden ermitteln --------------------------------------------------------------------------------------
	/*
	if ($kunde[wiedervorlage] == 1 AND $kunde[belegt] == '0') {
		$status = "WVL";
	}
	elseif ($kunde[neukunde] == 1) {
		$status = "NEU-K.";
	}
	elseif ($kunde[verzogen] == 1) {
		$status = "verzog.";
	}
	elseif ($kunde[belegt] == 1) {
		$status = "belegt";
	}
	elseif ($kunde[vertragskunde] == 1) {
		$status = "Vertrag";
	}
	elseif ($kunde[nichtkunde] == 1 AND $kunde[verzogen] == 0) {
		$status = "Nicht-K.";
	}
	elseif ($kunde[handy] == 1) {
		$status = "Handy";
	}
	elseif ($kunde[ne] == '$heute') {
		$status = "NE";
	}
	else {
		$status = "alle";
	}
	*/
	// Ende Statusermittlung --------------------------------------------------------------------------------------------
		
	// Termindaten zum Kunden ermitteln

	$sql = "SELECT termin_id, telefonist, aquiriert, termin, zeit, aussendienst, erledigt_date, ";
	$sql .= " termin.wiedervorlage_date, w_zeit, ";
	$sql .= " ges_alt, tarif_alt, von, bis, ";
	$sql .= " ges_neu, tarif_neu, seit, vonr, ";
	$sql .= " rem_termin, nichtkunde, abschluss, wiedervorlage, storno, alt, kalt, einzug ";
	$sql .= " FROM termin, produkt_alt, ges_alt, tarif_alt, produkt_neu, ges_neu, tarif_neu ";
	$sql .= " WHERE termin.kd_id = '$kunden_id' ";
	$sql .= " And termin.produkt_alt_id = produkt_alt.produkt_alt_id And produkt_alt.ges_alt_id = ges_alt.ges_alt_id AND produkt_alt.tarif_alt_id = tarif_alt.tarif_alt_id ";
	$sql .= " And termin.produkt_neu_id = produkt_neu.produkt_neu_id And produkt_neu.ges_neu_id = ges_neu.ges_neu_id AND produkt_neu.tarif_neu_id = tarif_neu.tarif_neu_id ";
	$sql .= " ORDER BY termin_id ASC ";
	$abfrage = myquery($sql, $conn);
	$anzahl = mysql_num_rows($abfrage);
	
// Ende Datenbankabfrage ---------------------------------------------------------------------------------------

?>

<!-- Hier f�ngt das HTML-Dokument an -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Kd-Termine</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="../css/preisagentur.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 5px;
	margin-top: 5px;
	margin-right: 5px;
	margin-bottom: 5px;
}
-->
</style>
</head>
<body>
<div align = "center">
<table width="1200" border="0" cellpadding="0" cellspacing="3">
	<?php	// �berschrift: Daten Kunde
		echo "<tr bgcolor = \"#eeeeee\">";
		echo "<td valign = \"middle\" align = \"left\" ><span style=\"font-weight:bold; font-size: 14px; color: blue; line-height:160%; padding-left:10px;\">Kunden-ID: $kunde[kunden_id]&nbsp;-&nbsp;$kunde[vorname]&nbsp;$kunde[name] - $anzahl&nbsp;Datens�tze</span</td>";
		echo "</tr>";
	?>
<tr>
<td>

<table width="100%" border="0" cellpadding="2" cellspacing="0" bgcolor="#eeeeee">		<!-- Beginn Tabelle Kundendaten -->
<?php
	// Tabellenkopf Kundendaten ----------------------------------------------------------------------------------------------
		echo "<tr>";
	
		if ($gruppe != 'Aussendienst') {
			echo "<td>&nbsp;</td>";				// Leerzelle f�r Bearbeiten-Button (nicht f�r Au�endienstler!!)
		}
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">PLZ</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"60\"><span style=\"font-weight:bold; line-height:160%;\">Ort</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"80\"><span style=\"font-weight:bold; line-height:160%;\">Ortsteil</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"100\"><span style=\"font-weight:bold; line-height:160%;\">Stra�e</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Vorwahl</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Telefon</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Vorwahl2</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Telefon2</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"80\"><span style=\"font-weight:bold; line-height:160%;\">E-Mail</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">geboren</span</td>";
		echo "<td valign = \"middle\" align = \"left\" width = \"100\"><span style=\"font-weight:bold; line-height:160%;\">Branche</span</td>";
		//echo "<td valign = \"middle\" align = \"left\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Status</span</td>";
		if ($gruppe == 'Administrator') {
			echo "<td>&nbsp;</td>";				// Leerzelle f�r L�schen-Button (nur f�r Administrator!!)
		}
		echo "</tr>";
	// Ende Tabellenkopf Kundendaten -----------------------------------------------------------------------------------------
	
	// Trennstrich zwischen Tabellenkopf und -daten f�r Kundendaten
	
		if ($gruppe == 'Aquise') {
			echo "<tr><td colspan = \"$spankaq\" valign = \"middle\"><hr></td></tr>";
		}
		elseif ($gruppe == 'Aussendienst') {
			echo "<tr><td colspan = \"$spankad\" valign = \"middle\"><hr></td></tr>";
		}
		else {
			echo "<tr><td colspan = \"$spankadmin\" valign = \"middle\"><hr></td></tr>";
		}
	
	// Anzeige Kundendaten ------------------------------------------------------------------------------------------------------

		echo "<tr>";
		if ($gruppe != "Aussendienst") {		// Bearbeiten-Button: User ist Administrator oder Telefonist, darf Kundendaten bearbeiten
			echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"kunde_neu.php?kd_id=$kunde[0]\" target=\"suche_unten\">
				  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"Kundendaten bearbeiten\" title=\"Kundendaten bearbeiten\" border=\"0\" /></a></td>";
		
		}
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[plz]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"60\">$kunde[ort]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"80\">$kunde[ortsteil]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"100\">$kunde[strasse]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[vorwahl1]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[telefon]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[vorwahl2]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"40\">$kunde[fax]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"80\">$kunde[email]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"70\">$kunde[geboren]</td>";
		echo "<td valign = \"top\" align = \"left\" width = \"100\">$kunde[branche]</td>";
		//echo "<td valign = \"top\" align = \"left\" width = \"40\">$status</td>";
		
		if ($gruppe == 'Administrator') {		// L�schen-Button: User ist Administrator darf Kundendaten l�schen
			echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"loeschen.php?kd_id=$kunde[0]\" target=\"_self\">
				  <img src=\"../images/loesche.png\" alt=\"Kunde l�schen\" title=\"Kunde l�schen\" border=\"0\" /></a></td>";
		}
		echo "</tr>";
		
		if (!empty($kunde[rem_kunde])) {		// Diese Zeile erscheint nur, wenn eine Kundenbemerkung in der Datenbank steht
			if ($gruppe == 'Administrator') {
				echo "<tr>";
				echo "<td>&nbsp;</td>";				// Leerzelle f�r obigen Bearbeiten-Button
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spankrem\">$kunde[rem_kunde]</td>";
				echo "<td>&nbsp;</td>";				// Leerzelle f�r obigen L�schen-Button
				echo "</tr>";
			}
			elseif ($gruppe == 'Aquise') {
				echo "<tr>";
				echo "<td>&nbsp;</td>";				// Leerzelle f�r obigen Bearbeiten-Button
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spankrem\">$kunde[rem_kunde]</td>";
				echo "</tr>";
			}
			else {
				echo "<tr>";
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spankrem\">$kunde[rem_kunde]</td>";
				echo "</tr>";
			}
		}	// Ende Zeile Kundenbemerkung
	// Ende Anzeige Kundendaten -------------------------------------------------------------------------------------------
?>
</table>																				<!-- Ende Tabelle Kundendaten -->
</td></tr>

<tr><td>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">		<!-- Beginn Tabelle Termindaten -->
<tr><td valign = "top">

<?php
	// Tabellenkopf Termindaten --------------------------------------------------------------------------------------------

		echo "<tr>";
		
		//if ($gruppe != 'Aussendienst') {
			//echo "<td>&nbsp;</td>";				// Leerzelle f�r Bearbeiten-Button (nicht f�r Au�endienstler!!)
		//}
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">&nbsp;</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Typ</span></td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Termin</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"50\"><span style=\"font-weight:bold; line-height:160%;\">Zeit</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Tel.</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">angerufen</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">AD</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">erledigt</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">W-Datum</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"50\"><span style=\"font-weight:bold; line-height:160%;\">W-Zeit</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Ges.-alt</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Tarif-alt</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">von</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">bis</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Ges.-neu</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">Tarif-neu</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"70\"><span style=\"font-weight:bold; line-height:160%;\">seit</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">VoNr</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Storno</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">Bank</span</td>";
		echo "<td valign = \"middle\" align = \"center\" width = \"40\"><span style=\"font-weight:bold; line-height:160%;\">T_ID</span</td>";
		
		if ($gruppe == 'Administrator') {
			echo "<td>&nbsp;</td>";				// Leerzelle f�r L�schen-Button (nur f�r Administrator!!)
		}
		
		echo "</tr>";
	// Ende Tabellenkopf Termindaten ------------------------------------------------------------------------------------------
	
	// Trennstrich zwischen Tabellenkopf und -daten f�r Termindaten
	
	if ($gruppe == 'Aquise') {
		echo "<tr><td colspan = \"$spantaq\" valign = \"middle\"><hr></td></tr>";
	}
	elseif ($gruppe == 'Aussendienst') {
		echo "<tr><td colspan = \"$spantad\" valign = \"middle\"><hr></td></tr>";
	}
	else {
		echo "<tr><td colspan = \"$spantadmin\" valign = \"middle\"><hr></td></tr>";
	}
		
		
	// Anzeige Termindaten ------------------------------------------------------------------------------------------------------
	
	for ($i = 0; $i < $anzahl; $i++) {
				
		$termin = mysql_fetch_array($abfrage);
		
	// deutsches Datum f�r Termin
		if (empty($termin[termin]) OR $termin[termin] == "0000-00-00") {	
			$termin[termin] = "";
		}
		else {
			$termin[termin] = mysqldate_in_de($termin[termin]);
		}
		
	// deutsches Datum f�r erledigt
		if (empty($termin[erledigt_date]) OR $termin[erledigt_date] == "0000-00-00") {	
			$termin[erledigt_date] = "";
		}
		else {
			$termin[erledigt_date] = mysqldate_in_de($termin[erledigt_date]);
		}
		
	// deutsches Datum f�r Wiedervorlage
		if (empty($termin[wiedervorlage_date]) OR $termin[wiedervorlage_date] == "0000-00-00") {	
			$termin[wiedervorlage_date] = "";
		}
		else {
			$termin[wiedervorlage_date] = mysqldate_in_de($termin[wiedervorlage_date]);
		}
		
	// deutsches Datum f�r von
		if (empty($termin[von]) OR $termin[von] == "0000-00-00") {	
			$termin[von] = "";
		}
		else {
			$termin[von] = mysqldate_in_de($termin[von]);
		}
		
	// deutsches Datum f�r bis
		if (empty($termin[bis]) OR $termin[bis] == "0000-00-00") {	
			$termin[bis] = "";
		}
		else {
			$termin[bis] = mysqldate_in_de($termin[bis]);
		}
		
	// deutsches Datum f�r seit
		if (empty($termin[seit]) OR $termin[seit] == "0000-00-00") {	
			$termin[seit] = "";
		}
		else {
			$termin[seit] = mysqldate_in_de($termin[seit]);
		}
		
	// 00:00 bei Zeit ausblenden
		if (empty($termin[zeit]) OR $termin[zeit] == "00:00") {	
			$termin[zeit] = "";
		}
		
	// 00:00 bei W-Zeit ausblenden
		if (empty($termin[w_zeit]) OR $termin[w_zeit] == "00:00") {	
			$termin[w_zeit] = "";
		}

	// aquiriert in Datum und Zeit trennen, deutsches Datum, bei Zeit sekunden ausblenden
		$aq = explode(" ", $termin[aquiriert]);
		$aq_datum = mysqldate_in_de($aq[0]);
		$aq_zeit = substr($aq[1], 0, 5);
		if ($aq_zeit == "00:00") {
				$aq = $aq_datum;
		}
		else {
			$aq = $aq_datum . "<br>" .$aq_zeit;
		}
		
	//storno
		if ($termin[storno] != '0') {
			$storno = $termin[storno];
		}
		else {
			$storno = "";
		}
		
	// einzug
	
		if (empty($termin[einzug])) {
			$einzug = "";
		}
		else {
			$einzug = "1";
		}

	//	$einzug = $termin[einzug];
		
	// Termintyp ermitteln
		if (!empty($termin[storno])) {
			$typ = "ST";
		}
		elseif ($termin[abschluss] == '1' AND $termin[kalt] == '0' ) {
			$typ = "AB";
		}
		elseif ($termin[abschluss] == '1' AND $termin[kalt] == '1' ) {
			$typ = "KAB";
		}
		elseif ($termin[abschluss] == '0' AND $termin[kalt] == '1' ) {
			$typ = "Kalt";
		}
		elseif ($termin[nichtkunde] == '1') {
			$typ = "NK";
		}
		elseif ($termin[wiedervorlage] == '1' AND $termin[alt] == '0') {
			$typ = "WVL";
		}
		elseif ($termin[wiedervorlage] == '1' AND $termin[alt] == '1') {
			$typ = "WVLalt";
		}
		elseif ($termin[erledigt_date] > 0000-00-00) {
			$typ = "ToAs";
		}
		else {
			$typ = "ToA";
		}
	
	// Hintergrundfarbe der Zeile in Abh�ngigkeit des Termintyps ermitteln

		switch ($typ) {
			case ST:
				$bg = "#ff3333";
				break;
			case AB:
				$bg = "#00FF00";
				break;
			case Kalt:
				$bg = "#00EAFF";
				break;
			case KAB:
				$bg = "#00A8FF";
			break;
			case NK:
				$bg = "#999999";
				break;
			case WVL:
				$bg = "#FFCCCC";
				break;
			case WVLalt:
				$bg = "#FFE6CC";
				break;
			case ToA:
				$bg = "#FFFF00";
				break;
			case ToAs:
				$bg = "#FFD700";
				break;
		}
		
		// Beginn Datenausgabe
		
		echo "<tr bgcolor=$bg>";
		
		//if ($gruppe == "Aussendienst") {		// Bearbeiten-Button: User ist Administrator oder Telefonist, darf Kundendaten bearbeiten
		//	echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"kunde_neu.php?t_id=$termin[0]\" target=\"suche_unten\">
		//		  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"Termin bearbeiten\" title=\"Termin bearbeiten\" border=\"0\" /></a></td>";
		//}
		
		if ($gruppe == 'Aquise' AND ($typ == "ST" OR $typ == "AB" OR $typ =="KAB")) {				//Gruppe Aquise darf keine Abschl�sse bearbeiten - Leerzelle statt Bearbeiten-Bild
			echo "<td valign = \"middle\" align = \"center\" width = \"30\">&nbsp;</td>";
		}
		else {
			echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"kunde_neu.php?t_id=$termin[0]\" target=\"suche_unten\">
				  <img src=\"../images/bearbeite.png\" width=\"16\" height=\"16\" alt=\"Termin bearbeiten\" title=\"Termin bearbeiten\" border=\"0\" /></a></td>";
		}
		
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$typ</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[termin]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"50\">$termin[zeit]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[telefonist]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$aq</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[aussendienst]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[erledigt_date]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[wiedervorlage_date]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"50\">$termin[w_zeit]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[ges_alt]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[tarif_alt]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[von]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[bis]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[ges_neu]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[tarif_neu]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"70\">$termin[seit]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[vonr]</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$storno</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$einzug</td>";
		echo "<td valign = \"top\" align = \"center\" width = \"40\">$termin[termin_id]</td>";
		
		if ($gruppe == 'Administrator') {		// L�schen-Button: User ist Administrator darf Termindaten l�schen
			echo "<td valign = \"middle\" align = \"center\" width = \"30\"><a href=\"loeschen.php?t_id=$termin[0]\" target=\"_self\">
				  <img src=\"../images/loesche.png\" alt=\"Termin l�schen\" title=\"Termin l�schen\" border=\"0\" /></a></td>";
		}
		echo "</tr>";
		
		if (!empty($termin[rem_termin])) {		// Diese Zeile erscheint nur, wenn eine Terminbemerkung in der Datenbank steht
			if ($gruppe == 'Administrator') {
				echo "<tr bgcolor=\"$bg\">";
				echo "<td>&nbsp;</td>";				// Leerzelle f�r obigen Bearbeiten-Button
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spantrem\">$termin[rem_termin]</td>";
				echo "<td>&nbsp;</td>";				// Leerzelle f�r obigen L�schen-Button
				echo "</tr>";
			}
			elseif ($gruppe == 'Aquise') {
				echo "<tr bgcolor=\"$bg\">";
				echo "<td>&nbsp;</td>";				// Leerzelle f�r obigen Bearbeiten-Button
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spantrem\">$termin[rem_termin]</td>";
				echo "</tr>";
			}
			else {
				echo "<tr bgcolor=\"$bg\">";
				echo "<td><span style = \"font-weight:bold;\">Bem.:</td>";
				echo "<td valign = \"middle\" align = \"left\" colspan = \"$spantrem\">$termin[rem_termin]</td>";
				echo "</tr>";
			}
			// Trennstrich zwischen den einzelnen Terminen
			if ($gruppe == 'Aquise') {
				echo "<tr><td colspan = \"$spantaq\" valign = \"middle\"><hr></td></tr>";
			}
			elseif ($gruppe == 'Aussendienst') {
				echo "<tr><td colspan = \"$spantad\" valign = \"middle\"><hr></td></tr>";
			}
			else {
				echo "<tr><td colspan = \"$spantadmin\" valign = \"middle\"><hr></td></tr>";
			}
		}	// Ende Zeile Terminbemerkung
	}	// Ende Anzeige Termindaten ------------------------------------------------------------------------------------------------
?>
</table>
</td></tr>

<tr><td>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">		<!-- Beginn Tabelle Legende-->
	<tr>
	<td valign = "middle" align = "center" bgcolor ="#eeeeee"><span style = "line-height:160%">Legende:</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFFF00"><span style = "line-height:160%">ToA: Termin</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFD700"><span style = "line-height:160%">ToAs: Termin bearbeitet</span></td>
	<td valign = "middle" align = "center" bgcolor ="#00FF00"><span style = "line-height:160%">AB: Abschluss</span></td>
	<td valign = "middle" align = "center" bgcolor ="#00EAFF"><span style = "line-height:160%">kalt: kalter Termin</span></td>
	<td valign = "middle" align = "center" bgcolor ="#00A8FF"><span style = "line-height:160%">KAB: kalter Abschluss</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFCCCC"><span style = "line-height:160%">WVL: WVL aktiv</span></td>
	<td valign = "middle" align = "center" bgcolor ="#FFE6CC"><span style = "line-height:160%">WVLalt: WVL abgeschlossen</span></td>
	<td valign = "middle" align = "center" bgcolor ="#999999"><span style = "line-height:160%">NK: Nichtkunde</span></td>
	<td valign = "middle" align = "center" bgcolor ="#ff3333"><span style = "line-height:160%">ST: Storno</span></td>
	</tr>
	</table>
	</td>
</tr>
<?php

// R�cksprung zum aufrufenden Programmmodul
	if ($gruppe == 'Administrator') {
		echo "<tr bgcolor=\"#eeeeee\"><td colspan = \"$spantadmin\" align = \"center\"><a href=\"suche.php\" target=\"_self\"><span style=\"font-size:10pt; font-weight:bold; line-height:200%; color: red;\">-> zur&uuml;ck zur Suche</span></a></td></tr>";
	}
	elseif ($gruppe == 'Aquise') {
		echo "<tr bgcolor=\"#eeeeee\"><td colspan = \"$spantaq\" align = \"center\"><a href=\"suche.php\" target=\"_self\"><span style=\"font-size:10pt; font-weight:bold; line-height:200%; color: red;\">-> zur&uuml;ck zur Suche</span></a></td></tr>";
	}
	else {
		echo "<tr bgcolor=\"#eeeeee\"><td colspan = \"$spantad\" align = \"center\"><a href=\"suche.php\" target=\"_self\"><span style=\"font-size:10pt; font-weight:bold; line-height:200%; color: red;\">-> zur&uuml;ck zur Suche</span></a></td></tr>";
	}

	
?>
</td></tr></table>																		<!-- Ende Tabelle Termindaten -->
</td></tr></table>
</div>
</body>
</html>

<?php	// Nachladen des Daten-Eingabe-Formulars im unteren Frame, leer - ohne Kunde oder Termin
	echo "<script>onload=parent['suche_unten'].location.href='kunde_neu.php'</script>";
?>