<?php
session_start();								// darf kein html-code davorstehen!!
setlocale(LC_TIME, "de_DE.utf8");

include ("include/mysqlconnect.inc.php");
include ("include/variablen.php");
include ("include/funktionen.php");

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE Notizen

//Login-Button gedr�ckt
if (($_POST['login'] == 'Einloggen')) {

	$pwd = md5(($_POST['pwd']));                                                //Passwort verschl�sseln

	$indate = date("Ymd");
	$intime = date("H:i");
	$sessionid = session_id();

	$benutzer = quote_smart($_POST['user']);

	// Test, ob schon eingeloggt

	$sql = "SELECT user FROM logins ";
	$sql .= "WHERE user = :user AND indate = :indate AND outdate = '0'";
	$statement = $pdo->prepare($sql);
	//$statement->execute(array(':user' => $_POST['user'], 'indate' => $indate));

	if ($statement) {

		if ($statement->rowCount() > 0) {
			echo "<script>location.href='loggedin.php'</script>";
			exit();
		} else {

			$input['user'] = $_POST['user'];
			$input['pwd'] = md5($_POST['pwd']);
			$input['name'] = $_POST['name'];

			$sql = "SELECT vorname, gruppen, user_id FROM vorname, name, user, gruppen ";
			$sql .= " WHERE gruppen.gruppen_id = user.gruppen_id AND vorname.vorname_id = user.vorname_id AND name.name_id = user.name_id";
			$sql .= " AND user = :user AND pwd = :pwd AND name.name = :name";

			$statement = $pdo->prepare($sql);
			$statement->execute($input);
			$input = array();
			if ($statement) {

				if ($statement->rowCount() == 0) {
					echo "<script>location.href='login_failed.php'</script>";
					exit();
				} else {
					$daten = $statement->fetch();

					$input['user'] = $_POST['user'];
					$input['indate'] = $indate;
					$input['intime'] = $intime;
					$input['sessionid'] = $sessionid;

					$sql = "INSERT INTO logins (user, indate, intime, session_id) VALUES (:user, :indate, :intime, :sessionid)";
					$statement = $pdo->prepare($sql);
					$statement->execute($input);

					$neue_id = $pdo->lastInsertId();

					if ($neue_id) {

						$_SESSION['benutzer_kurz'] = $_POST['user'];                                    //Session wird erstellt
						$_SESSION['benutzer_id'] = $daten["user_id"];
						$_SESSION['benutzer_name'] = $_POST['name'];
						$_SESSION['benutzer_vorname'] = $daten["vorname"];
						$_SESSION['benutzer_gruppen'] = $daten["gruppen"];

						switch ($daten["gruppen"]) {
							case 'Administrator': //Administrator
								echo "<script>location.href='admin/admin_start.php?SID=$sessionid'</script>";
								break;
							case 'Aquise':
								echo "<script>location.href='telefon/telefon_start.php?SID=$sessionid'</script>";
								break;
							case 'Aussendienst':
								echo "<script>location.href='aussen/aussen_start.php?SID=$sessionid'</script>";
								break;
						}
					} else {
						echo "SQL Error <br />";
						echo $statement->queryString . "<br />";
						echo $statement->errorInfo()[2];
					}
				}
			} else {
				echo "SQL Error <br />";
				echo $statement->queryString . "<br />";
				echo $statement->errorInfo()[2];
			}
		}
	} else {
		echo "SQL Error <br />";
		echo $statement->queryString . "<br />";
		echo $statement->errorInfo()[2];
	}


} // ende if login


?>

<!-- Hier f�ngt das HTML-Dokument an -->
<!DOCTYPE html>
<html>
<head>
<title>Preisagentur: Login</title>
<meta charset=utf-8" />
<link href="css/preisagentur.css" rel="stylesheet" type="text/css" />
</head>
<body onload="self.focus();document.login.name.focus()">			<!-- login.name = Name des Formulars.Name des Feldes -->
<div align="center">
<table width="400" border="0" cellpadding="3" cellspacing="0">
<tr>
  <td colspan="2">	<br /><br />
	<h1 class="Stil1">Login</h1>
    <div class="Stil1">Bitte Benutzerdaten eingeben:</div>
    <form name="login" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
	<table id="login" width="73%" cellpadding="1" cellspacing="0" bgcolor="#000000">
	<tr><td>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" bgcolor="#eeeeee">
	          <tr>
          <td>Name</td>
          <td><input name="name" type="text" class="effecteingabe" /></td>
        </tr>
        <tr>
          <td>Benutzername</td>
          <td><input name="user" type="text" class="effecteingabe" /></td>
        </tr>
        <tr>
          <td>Password</td>
          <td><input name="pwd" type="password" class="effecteingabe" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" name="login" value="Einloggen"  class = "submitt" /></td>
        </tr>
      </table>
	  </td></tr></table>
    </form></td>
 </tr>
</table>
</div>
</body>
</html>