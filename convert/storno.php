<?php
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

//------------------------------------------------------------------------------------------//
// Dieses Script liest alle Stornos aus der Datenbank, deren Kunde kein Stornokunde ist		//
// und setzt dann die entsprechenden Kunden wieder auf Storno = 1 							//
// -----------------------------------------------------------------------------------------//

//Variable definieren
$senden = $_POST["senden"];

// Script startet
//*********************************************************************************************************************
if (isset($senden)) {

	$anfang= time();								// Start der Überprüfung in Sekunden
	
	$sql = " SELECT kunden_id FROM kunden, termin ";
	$sql .= " WHERE kunden.kunden_id = termin.kd_id AND (termin.storno != '0') AND kunden.storno = '0' ";
	$query = myquery($sql, $conn);
	$anzahl = mysql_num_rows($query);
	
	echo "<table width = \"700\">";
	echo "<tr><td colspan = \"9\">Anzahl Vertragskunden mit Kennung 0: $anzahl</td></tr>";
	
	for ($i=0; $i<mysql_num_rows($query); $i++) {
		$daten = mysql_fetch_array($query);
		echo "Kunde: $daten[0]<br>";

		// alle Kunden updaten
		$sql = " UPDATE kunden SET storno = '1' WHERE kunden_id = '$daten[0]'";
		//$abfrage = myquery($sql, $conn);

	}
		
	$ende= time();										// Ende der Überprüfung in Sekunden
	$dauer = $ende-$anfang;								// Dauer der Überprüfung in Sekunden
	
	echo "<tr><td colspan =\"9\"><span style = \"font-size:10pt\">Programmstart:&nbsp;" . date("d.m.Y H:i:s", $anfang) ."</span></td></tr>";	// Ausgabe Startdatum / -zeit
	echo "<tr><td colspan =\"9\"><span style = \"font-size:10pt\">Programmende:&nbsp;" . date("d.m.Y H:i:s", $ende) ."</span></td></tr>";		// Ausgabe Endedatum / -zeit
	echo "<tr><td colspan =\"9\"><span style = \"font-size:10pt\">Dauer:&nbsp;" . ($dauer) . "&nbsp;Sekunden</span></td></tr>";					// Programmlaufzeit
	
	echo "<table>";

	$db_close = mysql_close($conn);
		
} // Ende ISSET(Senden)

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>Storno</title></head>
<body>
<form action="<?php $_SERVER['PHP_SELF']?>" method="post">
<table>
<tr><td><b>Dieses Script liest alle Stornos aus der Datenbank, deren Kunde kein Stornokunde ist</b><br>und setzt dann die entsprechenden Kunden wieder auf Storno = 1<br></td></tr>
<tr><td align="center"><input name="senden" type="submit" value="UPDATE!"></td></tr>          
</table>
</form>
<?php
//}
?>

</body>
</html>