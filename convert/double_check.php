<?php

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

//----------------------------------------------------------------------------------------------------------//
// Dieses Script �berpr�ft die Datei convert.txt auf doppelte Datens�tze und inkorrekte Telefonnummern		//
// Die Datensatznummern der gefundenen Fehler werden ausgegeben												//
// ---------------------------------------------------------------------------------------------------------//

//Variable definieren
$output = '';
if (isset($_POST['senden'])) //$eingabe = $_POST['eingabe'];

// Script startet, Konvertiere-Button wurde gedr�ckt
//*********************************************************************************************************************

//if (isset($senden))
{
	$output = '';
	
	$anfang= time();								// Start der �berpr�fung in Sekunden
	
	$dateiname = "convert.txt";
	$datei = fopen($dateiname, "r");				// Datei wird ge�ffnet - nur lesend!
	$groesze = filesize($dateiname);				// Gr��e der Datei wird ermittelt
	$inhalt = fread($datei, $groesze);				// Datei eingelesen
	fclose($datei);									// Datei wird wieder geschlossen
	
	// Das Zeilentrennzeichen (ENTER) wird definiert und die Anzahl der Datens�tze (Zeilen) ermittelt
	// danach wird die Datei in die einzelnen Datens�tze (Zeilen) aufgetrennt, es entsteht ein ARRAY $zeile
	// die einzelnen Felder in einem Datensatz sind durch Tabstopp getrennt
	
	$trenner = chr(13);									//Zeilentrennzeichen
	$trenner .= chr(10);								//Dezimalwert von CR in ASCII umgewandelt - hex 0D0A
	$anzahl = substr_count($inhalt, $trenner);			// Anzahl Zeilen = Anzahl der Zeilentrennzeichen
	$zeile = explode($trenner, $inhalt);
	
	$tabstop = chr(9);							    	// Dezimalwert vom Tabstop in ASCII - hex 09 - Feldtrennzeichen
	
	$feld = explode($tabstop, $zeile[0]);		 		// Zerlegung der Zeile 0 in die einzelnen Felder, Trennzeichen ist das Tabstop
	
	//echo "<pre>\n";
	//print_r($feld);
	//echo "</pre>\n";

	echo "PLZ-Gebiet: $feld[3] - Datens�tze: $anzahl<br><br>";	// Ausgabe der oben ermittelten PLZ
	
	// �u�ere Schleife - stellt die Basis-Daten f�r den Vergleich zur Verf�gung
	// die �u�ere Schleife beginnt bei Zeile "0" und endet beim vorletzten!! Datensatz, der dann mit dem letzten verglichen wird
	
	for($i=0; $i < ($anzahl-1); $i++)	{
	
		$datensatz = $i+1;								// Nummer des Datensatzes der Works-Datei, da Array bei "0" beginnt +1
										
		$output = '';									// R�cksetzen der Ausgabemeldung
			
		$feld = explode($tabstop, $zeile[$i]);		 	// Zerlegung der Zeile in die einzelnen Felder, Trennzeichen ist das Tabstop
			
		$name = $feld[1];								// Zuordnung der einzelnen Array-Elemente zu den Variablen
		$vorname = $feld[2];
		$plz = $feld[3];
		$ort = $feld[4];
		$strasse = $feld[5];
		$telefon = $feld[11];
		
		// Telefon wird in Feld Telefon und das neue feld[23] - Vorwahl - gesplittet
		$trenner = chr(41);									// Trennzeichen ")" = hex 29
		$telefon = explode($trenner, $telefon);
		$vorwahl = str_replace(chr(40),'', $telefon[0]); 	// Vorwahl - telefon[0]: Klammer auf l�schen
		$telefon = trim($telefon[1]);				     	// Telefon = telefon[1]: Whitespaces l�schen
		
		$muster = "/^\d{0,}$/";								// Test Telefon, nur Ziffern
		if(!preg_match($muster, $telefon)) {				// Muster passt nicht - Telefonnummer inkorrekt!!
			echo "<font color=\"#FF0000\">Datensatz: $datensatz&nbsp;-&nbsp; Keine g�ltige Telefonnummer</font><br>";	// Fehlerausgabe
		} // Ende IF Muster Telefon passt nicht
		
		$doppelt = 0;										// Initialisierung der Variablen $doppelt,
															// enth�lt sp�ter die Anzahl der gleichen Datens�tze zur Basis
															// und steuert das Weiterr�cken der �u�eren Schleife im Text-File
															
		// innere Schleife - stellt die Vergleichs-Daten zur Verf�gung
		// die innere Schleife beginnt logischerweise immer eine Zeile nach der �u�eren Schleife und endet beim letzten Datensatz
		// es werden 20 Datens�tze verglichen - mehr Telefonnummern sollte ein Kunde nicht haben
		
		for ($k=($i+1); $k < ($i+20); $k++)	{
		
			$feld_neu = explode($tabstop, $zeile[$k]);		// Zerlegung der Zeile in die einzelnen Felder, Trennzeichen ist das Tabstop
			
			$name_neu = $feld_neu[1];						// Zuordnung der Vergleichsdaten zu den Vergleichsvariablen
			$vorname_neu = $feld_neu[2];
			$plz_neu = $feld_neu[3];
			$ort_neu = $feld_neu[4];
			$strasse_neu = $feld_neu[5];
			
			// gleicher Kunde bedeutet - Name und Anschrift gleich, aber unterschiedliche Telefonnummer (ISDN!)
			
			if ($name == $name_neu AND $vorname == $vorname_neu AND $plz == $plz_neu AND $ort == $ort_neu AND $strasse == $strasse_neu) {
			
				if ($doppelt >= 1) {												// an die Ausgabemeldung wird die (Works)-Datensatznummer
					$output = $output . " - " . ($k+1);								// f�r jeden weiteren doppelten Datensatz angeh�ngt
				}
				else {
					$output = "Datensatz doppelt!! $datensatz - " . ($k+1);			// Ausgabemeldung f�r ersten doppelten Datensatz in der Schleife
				}
				
				$doppelt = $doppelt + 1;
		
			} // ende if gleich
			
		} // ende innere Schleife
		
		if ($output) {									// Ausgabe einer Meldung, falls doppelte Datens�tze f�r den aktuellen
			echo "$output<br>";							// Schleifendurchlauf existieren
		}
		
		// Variable $dopplet enth�lt Anzahl der doppelten Datens�tze - die �u�ere Schleife wird um diese Zahl weitergeschaltet
		// damit werden bereits verglichene Datens�tze nicht noch einmal bearbeitet (die �u�ere Schleife wird auf den Datensatz
		// dahinter eingestellt
		
		$i = $i + $doppelt;

	} //ende for

	$ende= time();										// Ende der �berpr�fung in Sekunden
	$dauer = $ende-$anfang;								// Dauer der �berpr�fung in Sekunden

	echo "<br>Fertig!<br><br>";
	echo "<span style = \"font-size:10pt\">Programmstart:&nbsp;" . date("d.m.Y H:i:s", $anfang) ."</span><br>";		// Ausgabe Startdatum / -zeit
	echo "<span style = \"font-size:10pt\">Programmende:&nbsp;" . date("d.m.Y H:i:s", $ende) ."</span><br>";		// Ausgabe Endedatum / -zeit
	echo "<span style = \"font-size:10pt\">Dauer:&nbsp;" . ($dauer) . "&nbsp;Sekunden</span><br>";					// Programmlaufzeit

} // Ende ISSET(Senden)

else	// Senden nicht gedr�ckt, Anzeige der HTML-Datei beim Start des Programms
{
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>Convert-Checker</title></head>
<body>
<form action="<?php $_SERVER['PHP_SELF']?>" method="post">
<table>
<tr><td><b>Script zur �berpr�fung der Datenbanken der Fa. Zschornack<br>auf doppelte Datens�tze und inkorrekte Telefonnummern</b><br><br></td></tr>
<tr><td>Der Name des zu �berpr�fenden Scriptes muss sein: convert.txt</td></tr>
<tr><td>Dateiformat des zu �berpr�fenden Scriptes muss sein: txt, tabstop getrennt</td></tr>
<tr><td>Speicherort der Datei convert.txt: im Ordner des Scriptes</td></tr>
<tr><td bgcolor = "red" align = "center"><br>&nbsp;&nbsp;<strong>Achtung! Anzahl der Spalten (Vorwahl) kontrollieren!!&nbsp;&nbsp;</strong><br><br></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td align="center"><input name="senden" type="submit" value="Check it!"></td></tr>          
</table>
</form>
<?php
}
?>

</body>
</html>
