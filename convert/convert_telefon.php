<?php

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/mysqlconnect.inc.php");

if (isset($_POST['senden'])) $senden = $_POST['senden'];

//Funktionsdefinitionen

//wandelt deutsches Datum nach MySQL-DATE.
//-------------------------------------------------------------
function mysqldate_in_de($termin)						
{	
	list($tag, $monat, $jahr) = explode(".", $termin);
	
	if ($jahr >= 1000) {										//Jahr ist vierstellig
		return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
	}
	elseif ($jahr <= 20) {										//Jahr liegt zwischen 2000 und 2020
		$jahr = "20" . $jahr;
		return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
	}
	else {														//Jahr liegt zwischen 1921 und 1999
		$jahr = "19" . $jahr;
		return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
	}
    
}

// ermittelt eine ID aus der Datenbank bzw. f�gt neuen Datensatz hinzu und gibt die ID zur�ck
// -------------------------------------------------------------------------------------------
function id_ermitteln($sql1, $sql2, $conn)
{
	$abfrage = mysql_query($sql1, $conn);
		if (mysql_errno()) { 
			echo "<font color=\"#FF0000\">Fehler in Zeile $datensatz: $sql1</font><br>";														// MySQL-Fehler
			echo mysql_errno() . ": " . mysql_error() . "<br>";
		}
	if ($ergebnis = mysql_fetch_array($abfrage, MYSQL_NUM)) {
		$id = $ergebnis[0];
	}
	else {
		$abfrage = mysql_query($sql2, $conn);
			if (mysql_errno()) { 
			echo "<font color=\"#FF0000\">Fehler in Zeile $datensatz: $sql2</font><br>";													// MySQL-Fehler
			echo mysql_errno() . ": " . mysql_error() . "<br>";
		}
		$id = mysql_insert_id();
	}
	return $id;
}

$heute = date("Y") . "-" . date("m") . "-" . date("d");						// heute

// Script startet, Konvertiere-Button wurde gedr�ckt
//*********************************************************************************************************************
if (isset($senden))
{
	$anfang= time();								// Start der �berpr�fung in Sekunden

	$dateiname = "convert.txt";

	if (!file_exists($dateiname)) {
   		print "Keine Datei vorhanden!!";
		clearstatcache();
		exit();
	}
	else { // File vorhanden
		ob_start();
		
		echo "Beginne Konvertierung!<br><br>";
	
	// Datei wird ge�ffnet, eingelesen und wieder geschlossen

	$datei = fopen($dateiname, "r");
	$groesze = filesize($dateiname);
	$inhalt = fread($datei, $groesze);
	fclose($datei);
	
	// die nicht richtig exportierten Zeichen werden ersetzt
/*	
	$inhalt = str_replace(chr(245), "�", $inhalt);			// hex f5
	$inhalt = str_replace(chr(247), "�", $inhalt);			// hex f7
	$inhalt = str_replace(chr(179), "�", $inhalt);			// hex b3
	$inhalt = str_replace(chr(63), "�", $inhalt);			// hex 3f
	$inhalt = str_replace(chr(199), "EUR", $inhalt);		// hex c7
*/
	// Excel-Spezial: Textfelder stehen in einer tabstop getrennten txt-Datei  in Anf�hrungszeichen - entfernen (hex 22)

//	$inhalt = str_replace(chr(34),'', $inhalt);

	// Das Zeilentrennzeichen (ENTER) wird definiert und die Anzahl der Datens�tze (Zeilen) ermittelt
	// danach wird die Datei in die einzelnen Datens�tze (Zeilen) aufgetrennt, es entsteht ein ARRAY $zeile
	// die einzelnen Felder in einem Datensatz sind durch Tabstop getrennt
	
	$trenner = chr(13);								//Zeilentrennzeichen
	$trenner .= chr(10);							//Dezimalwert von CR in ASCII umgewandelt - hex 0D0A
	$anzahl = substr_count($inhalt, $trenner);
	$zeile = explode($trenner, $inhalt);
	$zaehler = 0;									// Zaehler f�r eingef�gte Datens�tze
	$tabstop = chr(9);							    // Dezimalwert vom Tabstop in ASCII - hex 09 - Feldtrennzeichen

	echo "Anzahl Datens�tze: $anzahl<br>";

	for($i=0; $i < $anzahl; $i++)	{				// Array beginnt bei 0, deshalb Z�hlung bis kleiner Anzahl
	
		$datensatz = $i+1;							// Nummer des Datensatzes der Works-Datei
		
		echo "Datensatz: $datensatz&nbsp;-&nbsp;";
	
		$menge = substr_count($zeile[$i], $tabstop); // Anzahl der Tabstops je Zeile +1 = Anzahl der Felder!!!!!
		$feld = explode($tabstop, $zeile[$i]);		 // Zerlegung der Zeile in die einzelnen Felder, Trennzeichen ist das Tabstop
			
		//Variablendefinition

		/* feld[0]  = Branche     feld[1]  = Name       feld[2]  = Vorname   feld[3]  = PLZ      feld[4]  = Ort   	feld[5]  = Stra�e
	       feld[6]  = rem_kunde   feld[7]  = erledigt   feld[8]  = Ges.alt   feld[9]  = von      feld[10]  = bis 	feld[11] = Vorwahl 
		   feld[12] = Tel	      feld[13] = VoNr		feld[14] = Termin    feld[15] = Zeit     feld[16] = Ges.neu feld[17] = seit
		   feld[18] = Bank 	      feld[19] = geb		feld[20] = Storno    feld[21] = TV       feld[22] = Realis	feld[23] = ortsteil
	   */
		
		$branche = $feld[0];
		$name = $feld[1];
		$vorname = $feld[2];
		$plz = $feld[3];
		$ort = $feld[4];
		$strasse = $feld[5];
		$rem_kunde = $feld[6];
		$erledigt = $feld[7];
		$ges_alt = $feld[8];
		$von = $feld[9];
		$bis = $feld[10];
		$telefon = $feld[11];
		
		if(isset($feld[12])) { $vonr = $feld[12]; }
		if(isset($feld[13])) { $termin = $feld[13]; }
		if(isset($feld[14])) { $zeit = $feld[14]; }
		if(isset($feld[15])) { $ges_neu = $feld[15]; }
		if(isset($feld[16])) { $seit = $feld[16]; }
		if(isset($feld[17])) { $einzug = $feld[17];}
		if(isset($feld[18])) {$geboren = $feld[18]; }
		if(isset($feld[19])) { $storno = $feld[19]; }
		if(isset($feld[20])) { $tv = $feld[20]; }
		if(isset($feld[21])) { $realis = $feld[21]; }
		if(isset($feld[22])) { $ortsteil = $feld[22];}
		
		$vorwahl = $feld[23];		
		

		//Ende Variablendefinition
		
		$rem_kunde = trim($rem_kunde);					 //evtl. Lehrzeichen am Anfang und Ende der rem_kunde entfernen
		
		// terminsumwandlung in Mysql-Format
		if (!empty($erledigt)) {
			$erledigt = mysqldate_in_de($erledigt);
		}
		if (!empty($von)) {
			$von = mysqldate_in_de($von);
		}
		if (!empty($bis)) {
			$bis = mysqldate_in_de($bis);
		}
		if (!empty($termin)) {
			$termin = mysqldate_in_de($termin);
		}
		if (!empty($seit)) {
			$seit = mysqldate_in_de($seit);
		}
		
		if (!empty($geboren)) {
			$geboren = mysqldate_in_de($geboren);
		}
			
		 //Telefon wird in feld Telefon und das neue feld[23] - Vorwahl - gesplittet
		$trenner = chr(41);								  // Trennzeichen ")" = hex 29
		$telefon = explode($trenner, $telefon);
		$vorwahl = str_replace(chr(40),'', $telefon[0]); // Vorwahl - telefon[0]: Klammer auf l�schen
		$telefon = trim($telefon[1]);				      // Telefon = telefon[1]: Whitespaces l�schen
		
		$muster = "/^\d{0,}$/";								// Test Telefon, nur Ziffern
		if(!preg_match($muster, $telefon)) {					// Muster passt nicht
			echo "<font color=\"#FF0000\">&nbsp;&nbsp;&nbsp;Keine g�ltige Telefonnummer</font><br>";
			$telefon = "0";
		} // Ende IF Muster Telefon passt nicht

// Start Datenaufbereitung f�r Tabelle Termine
//*********************************************************************************************************************

		// Branche --------------------------------------------------------------------------------------------------------------
			if (empty($branche)) {																// Feld Branche ist leer
				$branche_id = 1;																// Standard gesetzt
			}
			else {
				$branche = trim($branche);														// Whitespaces entfernen
				$sql1 = "SELECT branche_id FROM branche WHERE branche.branche = '$branche'";	// Branche schon vorhanden?
				$sql2 = "INSERT INTO branche (branche) VALUES ('$branche')";				    // Branche neu
				$branche_id = id_ermitteln($sql1, $sql2, $conn);
			}
			
	   	// Name --------------------------------------------------------------------------------------------------------------
			if (empty($name)) {																// Feld Name ist leer
				$name_id = 1;																	// Standard gesetzt
			}
			else {
				$name = trim($name);	
				$sql1 = "SELECT name_id FROM name WHERE name.name = '$name'";				// Name schon vorhanden?
				$sql2 = "INSERT INTO name (name) VALUES ('$name')";							// Name neu
				$name_id = id_ermitteln($sql1, $sql2, $conn);
			}
					
	   	// Vorname --------------------------------------------------------------------------------------------------------------
			if (empty($vorname)) {																// Feld Vorname ist leer
				$vorname_id = 1;																	// Standard gesetzt
			}
			else {
				$vorname = trim($vorname);														// Whitespaces entfernen
				$sql1 = "SELECT vorname_id FROM vorname WHERE vorname.vorname = '$vorname'";	// Vorname schon vorhanden?
				$sql2 = "INSERT INTO vorname (vorname) VALUES ('$vorname')";				    // Vorname neu
				$vorname_id = id_ermitteln($sql1, $sql2, $conn);
			}
			
	   	// Ort --------------------------------------------------------------------------------------------------------------
			if (empty($ort)) {																// Feld Ort ist leer
				$ort_id = 1;																	// Standard gesetzt
			}
			else {
				$ort = trim($ort);														// Whitespaces entfernen
				$sql1 = "SELECT ort_id FROM ort WHERE ort.ort = '$ort'";					// Ort schon vorhanden?
				$sql2 = "INSERT INTO ort (ort) VALUES ('$ort')";							// Ort neu
				$ort_id = id_ermitteln($sql1, $sql2, $conn);
			}
					
		// Ortsteil --------------------------------------------------------------------------------------------------------------
			if (empty($ortsteil)) {																// Feld Ort ist leer
				$ortsteil_id = 1;																	// Standard gesetzt
			}
			else {
				$ortsteil = trim($ortsteil);														// Whitespaces entfernen
				$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil.ortsteil = '$ortsteil'";					// Ort schon vorhanden?
				$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil')";							// Ort neu
				$ortsteil_id = id_ermitteln($sql1, $sql2, $conn);
			}
					
	   	// PLZ --------------------------------------------------------------------------------------------------------------
			if (empty($plz)) {																// Feld PLZ ist leer
				$plz_id = 1;																	// Standard gesetzt
			}
				else {
					$plz = trim($plz);
					$muster = "/^\d{5}$/";								// Test PLZ, genau 5 Ziffern
					if(!preg_match($muster, $plz)) {					// Muster PLZ passt nicht
						echo "&nbsp;&nbsp;&nbsp;Keine g�ltige PLZ<br>";
						$plz = "0";
					} // Ende IF Muster PLZ passt nicht
					else { // Muster PLZ passt
						$sql1 = "SELECT plz_id FROM plz WHERE plz.plz = '$plz'";		// PLZ+Ort schon vorhanden?
						$sql2 = "INSERT INTO plz (plz) VALUES ('$plz')";				         // PLZ neu
						$plz_id = id_ermitteln($sql1, $sql2, $conn);
					}
				}
					
		// Kombination PLZ/Ort/Ortsteil
			$sql1 = "SELECT poo_id FROM poo WHERE ort_id = '$ort_id' AND ortsteil_id = '$ortsteil_id' AND plz_id = '$plz_id'";
			$sql2 = "INSERT INTO poo (ort_id, ortsteil_id, plz_id) VALUES ('$ort_id', '$ortsteil_id', '$plz_id')";
			$poo_id = id_ermitteln($sql1, $sql2, $conn);
			
			
	   	// Vorwahl1 --------------------------------------------------------------------------------------------------------------
			if (empty($vorwahl)) {																// Feld Vorwahl1 ist leer
				$vorwahl1_id = 1;																// Standard gesetzt
			}
			else {
				$vorwahl = trim($vorwahl);
				$muster = "/^\d{0,}$/";															// Test Vorwahl, nur Ziffern
				if(!preg_match($muster, $vorwahl)) {											// Muster passt nicht
					echo "&nbsp;&nbsp;&nbsp;keine g�ltige Vorwahl<br>";
					$vorwahl = "0";
				} // Ende IF Muster Vorwahl passt nicht
				else { // Muster  passt																// Whitespaces entfernen
					$sql1 = "SELECT vorwahl1_id FROM vorwahl1 WHERE vorwahl1.vorwahl1 = '$vorwahl'";		// Vorwahl schon vorhanden?
					$sql2 = "INSERT INTO vorwahl1 (vorwahl1) VALUES ('$vorwahl')";				            // Vorwahl neu
					$vorwahl1_id = id_ermitteln($sql1, $sql2, $conn);
				}
			}
								
		// Vorwahl2  --------------------------------------------------------------------------------------------------------------
			$vorwahl2_id = 1;
			
			
		// Abfrage der Kundentabelle
	
		$sql = "SELECT kunden_id, rem_kunde FROM kunden  ";
		$sql .= " WHERE telefon = '$telefon' AND vorwahl1_id = '$vorwahl1_id' ";
		$sql .= " AND vorname_id = '$vorname_id' AND name_id = '$name_id' ";
			
		$abfrage = mysql_query($sql, $conn);
		if (mysql_errno()) { echo mysql_errno() . ": " . mysql_error(); }			// MySQL-Fehler
			
		if ($ergebnis = mysql_fetch_array($abfrage)) {								// Kunde schon vorhanden
			$kd_id = $ergebnis[0];													// Kunden-Id
			
			$bemerkung = $rem_kunde . " " . $ergebnis[1];
					
			$sql = "UPDATE kunden SET rem_kunde = '$bemerkung' WHERE kunden_id = '$kd_id'";
			$abfrage = mysql_query($sql, $conn);
				if (mysql_errno()) { 
					//$ausgabe[$i] = $ausgabe[$i] . "Bestandskunde - Fehler bei Kd-Insert" . mysql_errno() . ": " . mysql_error();
					echo "Datensatz: $datensatz&nbsp;-&nbsp;Bestandskunde - Fehler bei Kd-Insert" . mysql_errno() . ": " . mysql_error() . "<br>";
				}
				else {
					//$ausgabe[$i] = $ausgabe[$i] . "Bestandskunde $kd_id - Bemerkung angef�gt";
					echo "Datensatz: $datensatz&nbsp;-&nbsp;Bestandskunde - Bemerkung angef�gt<br>";
				}
		} // ende Kunde schon vorhanden
					
		else { // neukunde
			$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
			$sql .= "neukunde, rem_kunde) ";
			$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
			$sql .= "'1', '$rem_kunde')";
					
			$abfrage = mysql_query($sql, $conn);
			if (mysql_errno()) { 
				//$ausgabe[$i] = $ausgabe[$i] . "neuer Kunde - Fehler bei Kd-Insert </font>" . mysql_errno() . ": " . mysql_error();
				echo "Datensatz: $datensatz&nbsp;-&nbsp;Neukunde - Fehler bei Kd-Insert" . mysql_errno() . ": " . mysql_error() . "<br>";
			}
			else {
				$kd_id = mysql_insert_id();
				//$ausgabe[$i] = $ausgabe[$i] . "neuer Kunde - $kd_id eingef�gt";
				echo "Datensatz: $datensatz&nbsp;-&nbsp;Neukunde - $kd_id eingef�gt<br>";
			}
		} // ende else Neukunde
			
		flush();

	} // Ende FOR-Schleife - Datei komplett verarbeitet
	
		ob_end_flush();	
	
		echo "<br>PLZ-Gebiet: $plz - $datensatz Datens�tze eingef�gt.<br>";
		//echo "Branchen eingef�gt&nbsp;:$branche_insert<br>";
		//echo "Vornamen eingef�gt&nbsp;:$vorname_insert<br>";
		//echo "Namen eingef�gt&nbsp;:$name_insert<br>";
		//echo "Vorwahlen eingef�gt&nbsp;:$vorwahl_insert<br>";
		//echo "Orte eingef�gt&nbsp;:$ort_insert<br>";
		//echo "Ortsteile eingef�gt&nbsp;:$ortsteil_insert<br>";
		//echo "PLZ eingef�gt&nbsp;:$plz_insert<br>";
		//echo "Poos eingef�gt&nbsp;:$poo_insert<br>";
		echo "<br>";
		
		$ende= time();										// Ende der �berpr�fung in Sekunden
		$dauer = $ende-$anfang;								// Dauer der �berpr�fung in Sekunden

		echo "<span style = \"font-size:10pt\">Programmstart:&nbsp;" . date("d.m.Y H:i:s", $anfang) ."</span><br>";		// Ausgabe Startdatum / -zeit
		echo "<span style = \"font-size:10pt\">Programmende:&nbsp;" . date("d.m.Y H:i:s", $ende) ."</span><br>";		// Ausgabe Endedatum / -zeit
		echo "<span style = \"font-size:10pt\">Dauer:&nbsp;" . ($dauer) . "&nbsp;Sekunden</span><br>";					// Programmlaufzeit
		
		echo "<br>FERTIG!";
		
		exit();
		
	} // ende Datei vorhanden
		
		echo "$datensatzD atens�tze eingef�gt.";
		echo "<br><br>FERTIG!";
		exit();

} // Ende ISSET(Senden)

//Konvertiere-Button noch nicht gedr�ckt
else	// Senden nicht gedr�ckt
{
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>Datenbank-Konverter</title></head>
<body>

<table>
<tr><td><b>Script zur Konvertierung der Datenbanken der Fa. Zschornack</b></td></tr>
<tr><td>Der Name des zu konvertierenden Scriptes muss sein: convert.txt</td></tr>
<tr><td>Dateiformat des zu konvertierenden Scriptes muss sein: txt, tabstop getrennt</td></tr>
<tr><td>Speicherort der Datei convert.txt: im Ordner des Scriptes</td></tr>
<tr><td bgcolor = "red" align = "center"><br>&nbsp;&nbsp;<strong>Achtung! Anzahl der Spalten (Vorwahl) kontrollieren!!&nbsp;&nbsp;</strong><br><br></td></tr>
<tr><td>
<form action="<?php $_SERVER['PHP_SELF']?>" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr><td class="werte"><p><input name="senden" type="submit" value="Konvertiere!"></p></td></tr>          
</table>
</form>
</td></tr>
</table>
<?php
}
?>

</body>
</html>
