<?php
session_start();

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/init.php");

//----------------------------------------------------------------------------------------------------------//
// Dieses Script l�scht aus der Datenbank einen kompletten PLZ-Bereich mit allen Terminen und Kunden		//
// anschlie�end werden evtl. Orts- und Ortsteilnamen aus der Datenbank gel�scht								//
// ---------------------------------------------------------------------------------------------------------//

//Variable definieren
$senden = $_POST["senden"];
$muster_plz = "/^\d{5}$/";									// Test PLZ, genau 5 Ziffern

// Script startet, Konvertiere-Button wurde gedr�ckt
//*********************************************************************************************************************
if (isset($senden)) {

		//ob_start();									// Ausgabepuffer wird umgelenkt

		$plz = $_POST["plz_neu"];

		if(!preg_match($muster_plz, $plz)) {				// Muster passt nicht - PLZ inkorrekt!!
			echo "<font color=\"#FF0000\">Keine g�ltige Postleitzahl</font><br>";	// Fehlerausgabe
		} // Ende IF Muster Telefon passt nicht
		
		else {	// muster stimmt
		
		$plz1 = $plz;								// Zwischenspeicherung f�r Ausgabe
		
		$anfang= time();								// Start der �berpr�fung in Sekunden
	
	//$sql = " SELECT poo_id, ort, ortsteil FROM poo, plz, ort, ortsteil ";
	$sql = " SELECT poo.poo_id, ort.ort, ortsteil.ortsteil, poo.ort_id, poo.ortsteil_id FROM poo, plz, ort, ortsteil ";
	$sql .= " WHERE plz.plz = '$plz' AND poo.plz_id = plz.plz_id AND poo.ort_id = ort.ort_id AND poo.ortsteil_id = ortsteil.ortsteil_id ";
	$sql .= " ORDER BY ort ASC, ortsteil ASC ";
	$query = myquery($sql, $conn);
	$poo_anzahl = mysql_num_rows($query);
	
	echo "<table width = \"700\">";
	echo "<tr><td colspan = \"9\">PLZ: $plz1&nbsp;&nbsp;&nbsp;Anzahl Orte/Ortsteile (poo_ids): $poo_anzahl</td></tr>";
	echo "<tr><td>poo</td><td>PLZ</td><td>Ort</td><td>Ortsteil</td><td>Kd. gesamt</td><td>N-Kunden</td><td>T-Kunden</td><td>Termine</td><td>Rest</td></tr>";
	
	$anzahl_termine = "0";

	for ($i=0; $i<mysql_num_rows($query); $i++) {
		$daten = mysql_fetch_array($query);
		

		//zuerst alle Kunden auslesen
		$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$daten[0]'";
		$abfrage = myquery($sql, $conn);
		$anzahl = mysql_num_rows($abfrage);
		// zuerst alle Neukunden l�schen -------------------------------------------
		$sql = " DELETE FROM kunden WHERE poo_id = '$daten[0]' AND neukunde = '1' ";
		$loesche_neukunde = myquery($sql, $conn);
		$neukunde = mysql_affected_rows($conn);
				
		// jetzt alle restlichen Kunden aus dem poo-Bereich auslesen
		$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$daten[0]'";
		$terminkunden = myquery($sql, $conn);
		$restkunden=mysql_num_rows($terminkunden);
		
		for ($j=0; $j<mysql_num_rows($terminkunden); $j++) {
			$daten1 = mysql_fetch_array($terminkunden);
			$sql = " DELETE FROM termin WHERE termin.kd_id = '$daten1[0]'";
			$loesche_termin = myquery($sql, $conn);
			$termin = mysql_affected_rows($conn);
			
			$anzahl_termine = $anzahl_termine + $termin;
		}
			
		// jetzt alle restlichen Kunden l�schen -------------------------------------------
		$sql = " DELETE FROM kunden WHERE poo_id = '$daten[0]'";
		$loesche_terminkunde = myquery($sql, $conn);
		$terminkunde = mysql_affected_rows($conn);
		
		$uebrig = $anzahl-$neukunde-$terminkunde;
		// Ausgabe der Ergebnisse
		
		// sind noch Kunden im poo-Bereich �briggeblieben?
		$sql = " SELECT kunden_id FROM kunden WHERE poo_id = '$daten[0]'";
		$uebrigekunden = myquery($sql, $conn);
		$nichtgeloescht=mysql_num_rows($uebrigekunden);
		
		if ($nichtgeloescht == 0) {		// alle Kunden gel�scht
		
			// poo kann gel�scht werden
			$sql = "DELETE FROM poo WHERE poo_id = '$daten[0]' LIMIT 1 ";
			$loesche_poo = myquery($sql, $conn);
			if (mysql_affected_rows($conn) == 1) {
				$daten[0] = $daten[0] . "-L";
			}
			
			// Ortsteil kann gel�scht werden, falls einer vorhanden ist  und nicht mehr gebraucht wird
			
			if ($daten[4] != 1) {	// Ortsteil vorhanden
				$sql = "SELECT poo_id FROM poo WHERE ortsteil_id = '$daten[4]' ";
				$ortsteil_rest = myquery($sql, $conn);
				if (mysql_num_rows($ortsteil_rest) == 0) {	// keine poo_id mit diesem Ortsteil mehr da - Ortsteil l�schen!!
					// Ortsteil kann gel�scht werden
					$sql = "DELETE FROM ortsteil WHERE ortsteil_id = '$daten[4]' LIMIT 1 ";
					$loesche_ortsteil = myquery($sql, $conn);
					if (mysql_affected_rows($conn) == 1) {
						$daten[2] = $daten[2] . "-L";
					}
				}
			}
			
			// eventuell kann der Ort gel�scht werden
			$sql = "SELECT poo_id FROM poo WHERE poo.ort_id = '$daten[3]' ";
			$ort_rest = myquery($sql, $conn);
			if (mysql_num_rows($ort_rest) == 0) {		// keine poo_id mit diesem Ort mehr da - Ort l�schen!!
				// Ort kann gel�scht werden
				$sql = "DELETE FROM ort WHERE ort_id = '$daten[3]' LIMIT 1 ";
				$loesche_ort = myquery($sql, $conn);
				if (mysql_affected_rows($conn) == 1) {
					$daten[1] = $daten[1] . "-L";
				}
			}
			
			// eventuell kann die PLZ gel�scht werden
			$sql = "SELECT poo_id FROM poo, plz WHERE poo.plz_id = plz.plz_id AND plz.plz = '$plz' ";
			$plz_rest = myquery($sql, $conn);
			if (mysql_num_rows($plz_rest) == 0) {		// keine poo_id mit dieser PLZ mehr da - PLZ l�schen!!
				// PLZ kann gel�scht werden
				$sql = "DELETE FROM plz WHERE plz = '$plz' LIMIT 1 ";
				$loesche_plz = myquery($sql, $conn);;
				if (mysql_affected_rows($conn) == 1) {
					$plz = $plz . "-L";
				}
			}
		}
		echo "<tr>";
		echo "<td align = \"center\">$daten[0]</td><td align = \"center\">$plz</td><td>$daten[1]</td><td>$daten[2]</td><td align = \"center\">$anzahl</td><td align = \"center\">$neukunde</td><td align = \"center\">$restkunden</td><td align = \"center\">$anzahl_termine</td><td align = \"center\">$nichtgeloescht</td>";
		echo "</tr>";
		
		
		//echo "<tr>";
		//echo "<td>$poo_loesch</td><td>$plz_loesch</td><td>$ort_loesch</td><td>$ortsteil_loesch</td><td>$anzahl</td><td>$neukunde</td><td>$restkunden</td><td>$anzahl_termine</td><td>$nichtgeloescht</td>";
		//echo "</tr>";
		
		$anzahl_termine = "0";
	
	}
	
		$ende= time();										// Ende der �berpr�fung in Sekunden
		$dauer = $ende-$anfang;								// Dauer der �berpr�fung in Sekunden
	
		echo "<tr><td colspan =\"9\"><span style = \"font-size:10pt\">Programmstart:&nbsp;" . date("d.m.Y H:i:s", $anfang) ."</span></td></tr>";	// Ausgabe Startdatum / -zeit
		echo "<tr><td colspan =\"9\"><span style = \"font-size:10pt\">Programmende:&nbsp;" . date("d.m.Y H:i:s", $ende) ."</span></td></tr>";		// Ausgabe Endedatum / -zeit
		echo "<tr><td colspan =\"9\"><span style = \"font-size:10pt\">Dauer:&nbsp;" . ($dauer) . "&nbsp;Sekunden</span></td></tr>";					// Programmlaufzeit
	
	echo "<table>";
/*
	echo "<br>Fertig!<br>";
	echo "<span style = \"font-size:10pt\">Programmstart:&nbsp;" . date("d.m.Y H:i:s", $anfang) ."</span><br>";		
	echo "<span style = \"font-size:10pt\">Programmende:&nbsp;" . date("d.m.Y H:i:s", $ende) ."</span><br>";		
	echo "<span style = \"font-size:10pt\">Dauer:&nbsp;" . ($dauer) . "&nbsp;Sekunden</span><br>";					
*/

 //	$dateiinhalt=ob_get_contents(); 			// in der Variable $dateiinhalt ist der komplette Ausgabepuffer drin
 //	$fh=fopen("$plz1.htm", "w");				// eine Datei wird ge�ffnet mit Schreibberechtigung
//	fwrite($fh,$dateiinhalt);					// Ausgabepuffer wird in Datei geschrieben
// 	fclose($fh);								// Datei wird wieder geschlossen

 // Ausgabepuffer wird zum Bildschirm geschickt
// ob_end_flush();

// Alternativ: Ausgabepuffer verwerfen
//ob_end_clear();


	$db_close = mysql_close($conn);
		
	} // ende else muster_plz stimmt
} // Ende ISSET(Senden)

//else	// Senden nicht gedr�ckt, Anzeige der HTML-Datei beim Start des Programms
//{
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>PLZ-Loeschen</title></head>
<body>
<form action="<?php $_SERVER['PHP_SELF']?>" method="post">
<table>
<tr><td><b>Dieses Script l�scht aus der Datenbank einen kompletten PLZ-Bereich mit allen Terminen und Kunden<br>anschlie�end werden evtl. Orts- und Ortsteilnamen aus der Datenbank gel�scht	</b><br><br></td></tr>
<tr><td><input type="text" name="plz_neu" size="5" maxlength="5" value = "<?phpecho"$plz1";?>"></td></tr>
<tr><td align="center"><input name="senden" type="submit" value="L�sche!"></td></tr>          
</table>
</form>
<?php
//}
?>

</body>
</html>
