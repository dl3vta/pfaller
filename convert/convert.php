<?php

error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

include ("../include/mysqlconnect.inc.php");
//Variable definieren
$senden = $_POST["senden"];

//Funktionsdefinitionen

//wandelt deutsches Datum nach MySQL-DATE.
//-------------------------------------------------------------
function mysqldate_in_de($termin)						
{	
	list($tag, $monat, $jahr) = explode(".", $termin);
	
	if ($jahr >= 1000) {										//Jahr ist vierstellig
		return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
	}
	elseif ($jahr <= 20) {										//Jahr liegt zwischen 2000 und 2020
		$jahr = "20" . $jahr;
		return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
	}
	else {														//Jahr liegt zwischen 1921 und 1999
		$jahr = "19" . $jahr;
		return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
	}
    
}

// ermittelt eine ID aus der Datenbank bzw. f�gt neuen Datensatz hinzu und gibt die ID zur�ck
// -------------------------------------------------------------------------------------------
function id_ermitteln($sql1, $sql2, $conn)
{
	$abfrage = mysql_query($sql1, $conn);
		if (mysql_errno()) { 
			echo "<font color=\"#FF0000\">Fehler in Zeile $datensatz: $sql1</font><br>";														// MySQL-Fehler
			echo mysql_errno() . ": " . mysql_error() . "<br>";
		}
	if ($ergebnis = mysql_fetch_array($abfrage, MYSQL_NUM)) {
		$id = $ergebnis[0];
	}
	else {
		$abfrage = mysql_query($sql2, $conn);
			if (mysql_errno()) { 
			echo "<font color=\"#FF0000\">Fehler in Zeile $datensatz: $sql2</font><br>";													// MySQL-Fehler
			echo mysql_errno() . ": " . mysql_error() . "<br>";
		}
		$id = mysql_insert_id();
	}
	return $id;
}

$heute = date("Y") . "-" . date("m") . "-" . date("d");						// heute

// Script startet, Konvertiere-Button wurde gedr�ckt
//*********************************************************************************************************************
if (isset($senden))
{
	echo "Beginne Konvertierung!<br><br>";
	
// Datei wird ge�ffnet, eingelesen und wieder geschlossen

	$dateiname = "convert.txt";
	$datei = fopen($dateiname, "r");
	$groesze = filesize($dateiname);
	$inhalt = fread($datei, $groesze);
	fclose($datei);
	
	// die nicht richtig exportierten Zeichen werden ersetzt
/*	
	$inhalt = str_replace(chr(245), "�", $inhalt);			// hex f5
	$inhalt = str_replace(chr(247), "�", $inhalt);			// hex f7
	$inhalt = str_replace(chr(179), "�", $inhalt);			// hex b3
	$inhalt = str_replace(chr(63), "�", $inhalt);			// hex 3f
	$inhalt = str_replace(chr(199), "EUR", $inhalt);		// hex c7
*/
	// Excel-Spezial: Textfelder stehen in einer tabstop getrennten txt-Datei  in Anf�hrungszeichen - entfernen (hex 22)

//	$inhalt = str_replace(chr(34),'', $inhalt);

	// Das Zeilentrennzeichen (ENTER) wird definiert und die Anzahl der Datens�tze (Zeilen) ermittelt
	// danach wird die Datei in die einzelnen Datens�tze (Zeilen) aufgetrennt, es entsteht ein ARRAY $zeile
	// die einzelnen Felder in einem Datensatz sind durch Tabstop getrennt
	
	$trenner = chr(13);								//Zeilentrennzeichen
	$trenner .= chr(10);							//Dezimalwert von CR in ASCII umgewandelt - hex 0D0A
	$anzahl = substr_count($inhalt, $trenner);
	$zeile = explode($trenner, $inhalt);
	$zaehler = 0;									// Zaehler f�r eingef�gte Datens�tze
	$tabstop = chr(9);							    // Dezimalwert vom Tabstop in ASCII - hex 09 - Feldtrennzeichen

	echo "Anzahl Datens�tze: $anzahl<br>";

	for($i=0; $i < $anzahl; $i++)	{				// Array beginnt bei 0, deshalb Z�hlung bis kleiner Anzahl
	
		$datensatz = $i+1;							// Nummer des Datensatzes der Works-Datei
		
		echo "Datensatz: $datensatz&nbsp;-&nbsp;";
	
		$menge = substr_count($zeile[$i], $tabstop); // Anzahl der Tabstops je Zeile +1 = Anzahl der Felder!!!!!
		$feld = explode($tabstop, $zeile[$i]);		 // Zerlegung der Zeile in die einzelnen Felder, Trennzeichen ist das Tabstop
			
		//Variablendefinition

		/* feld[0]  = Branche     feld[1]  = Name       feld[2]  = Vorname   feld[3]  = PLZ      feld[4]  = Ort   	feld[5]  = Stra�e
	       feld[6]  = rem_kunde   feld[7]  = erledigt   feld[8]  = Ges.alt   feld[9]  = von      feld[10]  = bis 	feld[11] = Tel  
	       feld[12] = VoNr		  feld[13] = Termin     feld[14] = Zeit      feld[15] = Ges.neu  feld[16] = seit  	feld[17] = Bank 
	       feld[18] = geb		  feld[19] = Storno     feld[20] = TV        feld[21] = Realis	 feld[22] = ortsteil
	       feld[22] = Vorwahl - wird aus feld[10] erzeugt
	   */
		
		$branche = $feld[0];
		$name = $feld[1];
		$vorname = $feld[2];
		$plz = $feld[3];
		$ort = $feld[4];
		$strasse = $feld[5];
		$rem_kunde = $feld[6];
		$erledigt = $feld[7];
		$ges_alt = $feld[8];
		$von = $feld[9];
		$bis = $feld[10];
		$telefon = $feld[11];
		$vonr = $feld[12];
		$termin = $feld[13];
		$zeit = $feld[14];
		$ges_neu = $feld[15];
		$seit = $feld[16];
		$einzug = $feld[17];
		$geboren = $feld[18];
		$storno = $feld[19];
		$tv = $feld[20];
		$realis = $feld[21];
		$ortsteil = $feld[22];
		$vorwahl = $feld[23];

		//Ende Variablendefinition
		
		$rem_kunde = trim($rem_kunde);					 //evtl. Lehrzeichen am Anfang und Ende der rem_kunde entfernen
		
		// terminsumwandlung in Mysql-Format
		if (!empty($erledigt)) {
			$erledigt = mysqldate_in_de($erledigt);
		}
		if (!empty($von)) {
			$von = mysqldate_in_de($von);
		}
		if (!empty($bis)) {
			$bis = mysqldate_in_de($bis);
		}
		if (!empty($termin)) {
			$termin = mysqldate_in_de($termin);
		}
		if (!empty($seit)) {
			$seit = mysqldate_in_de($seit);
		}
		
		if (!empty($geboren)) {
			$geboren = mysqldate_in_de($geboren);
		}
			
		// Telefon wird in feld Telefon und das neue feld[23] - Vorwahl - gesplittet
		$trenner = chr(41);								  // Trennzeichen ")" = hex 29
		$telefon = explode($trenner, $telefon);
		$vorwahl = str_replace(chr(40),'', $telefon[0]); // Vorwahl - telefon[0]: Klammer auf l�schen
		$telefon = trim($telefon[1]);				      // Telefon = telefon[1]: Whitespaces l�schen
		
		$muster = "/^\d{0,}$/";								// Test Telefon, nur Ziffern
		if(!preg_match($muster, $telefon)) {					// Muster passt nicht
			echo "<font color=\"#FF0000\">&nbsp;&nbsp;&nbsp;Keine g�ltige Telefonnummer</font><br>";
			$telefon = "0";
		} // Ende IF Muster Telefon passt nicht

// Start Datenaufbereitung f�r Tabelle Termine
//*********************************************************************************************************************

		// Branche --------------------------------------------------------------------------------------------------------------
			if (empty($branche)) {																// Feld Branche ist leer
				$branche_id = 1;																// Standard gesetzt
			}
			else {
				$branche = trim($branche);														// Whitespaces entfernen
				$sql1 = "SELECT branche_id FROM branche WHERE branche.branche = '$branche'";	// Branche schon vorhanden?
				$sql2 = "INSERT INTO branche (branche) VALUES ('$branche')";				    // Branche neu
				$branche_id = id_ermitteln($sql1, $sql2, $conn);
			}
			
	   	// Name --------------------------------------------------------------------------------------------------------------
			if (empty($name)) {																// Feld Name ist leer
				$name_id = 1;																	// Standard gesetzt
			}
			else {
				$name = trim($name);	
				$sql1 = "SELECT name_id FROM name WHERE name.name = '$name'";				// Name schon vorhanden?
				$sql2 = "INSERT INTO name (name) VALUES ('$name')";							// Name neu
				$name_id = id_ermitteln($sql1, $sql2, $conn);
			}
					
	   	// Vorname --------------------------------------------------------------------------------------------------------------
			if (empty($vorname)) {																// Feld Vorname ist leer
				$vorname_id = 1;																	// Standard gesetzt
			}
			else {
				$vorname = trim($vorname);														// Whitespaces entfernen
				$sql1 = "SELECT vorname_id FROM vorname WHERE vorname.vorname = '$vorname'";	// Vorname schon vorhanden?
				$sql2 = "INSERT INTO vorname (vorname) VALUES ('$vorname')";				    // Vorname neu
				$vorname_id = id_ermitteln($sql1, $sql2, $conn);
			}
			
	   	// Ort --------------------------------------------------------------------------------------------------------------
			if (empty($ort)) {																// Feld Ort ist leer
				$ort_id = 1;																	// Standard gesetzt
			}
			else {
				$ort = trim($ort);														// Whitespaces entfernen
				$sql1 = "SELECT ort_id FROM ort WHERE ort.ort = '$ort'";					// Ort schon vorhanden?
				$sql2 = "INSERT INTO ort (ort) VALUES ('$ort')";							// Ort neu
				$ort_id = id_ermitteln($sql1, $sql2, $conn);
			}
					
		// Ortsteil --------------------------------------------------------------------------------------------------------------
			if (empty($ortsteil)) {																// Feld Ort ist leer
				$ortsteil_id = 1;																	// Standard gesetzt
			}
			else {
				$ortsteil = trim($ortsteil);														// Whitespaces entfernen
				$sql1 = "SELECT ortsteil_id FROM ortsteil WHERE ortsteil.ortsteil = '$ortsteil'";					// Ort schon vorhanden?
				$sql2 = "INSERT INTO ortsteil (ortsteil) VALUES ('$ortsteil')";							// Ort neu
				$ortsteil_id = id_ermitteln($sql1, $sql2, $conn);
			}
					
	   	// PLZ --------------------------------------------------------------------------------------------------------------
			if (empty($plz)) {																// Feld PLZ ist leer
				$plz_id = 1;																	// Standard gesetzt
			}
				else {
					$plz = trim($plz);
					$muster = "/^\d{5}$/";								// Test PLZ, genau 5 Ziffern
					if(!preg_match($muster, $plz)) {					// Muster PLZ passt nicht
						echo "&nbsp;&nbsp;&nbsp;Keine g�ltige PLZ<br>";
						$plz = "0";
					} // Ende IF Muster PLZ passt nicht
					else { // Muster PLZ passt
						$sql1 = "SELECT plz_id FROM plz WHERE plz.plz = '$plz'";		// PLZ+Ort schon vorhanden?
						$sql2 = "INSERT INTO plz (plz) VALUES ('$plz')";				         // PLZ neu
						$plz_id = id_ermitteln($sql1, $sql2, $conn);
					}
				}
					
		// Kombination PLZ/Ort/Ortsteil
			$sql1 = "SELECT poo_id FROM poo WHERE ort_id = '$ort_id' AND ortsteil_id = '$ortsteil_id' AND plz_id = '$plz_id'";
			$sql2 = "INSERT INTO poo (ort_id, ortsteil_id, plz_id) VALUES ('$ort_id', '$ortsteil_id', '$plz_id')";
			$poo_id = id_ermitteln($sql1, $sql2, $conn);
			
			
	   	// Vorwahl1 --------------------------------------------------------------------------------------------------------------
			if (empty($vorwahl)) {																// Feld Vorwahl1 ist leer
				$vorwahl1_id = 1;																// Standard gesetzt
			}
			else {
				$vorwahl = trim($vorwahl);
				$muster = "/^\d{0,}$/";															// Test Vorwahl, nur Ziffern
				if(!preg_match($muster, $vorwahl)) {											// Muster passt nicht
					echo "&nbsp;&nbsp;&nbsp;keine g�ltige Vorwahl<br>";
					$vorwahl = "0";
				} // Ende IF Muster Vorwahl passt nicht
				else { // Muster  passt																// Whitespaces entfernen
					$sql1 = "SELECT vorwahl1_id FROM vorwahl1 WHERE vorwahl1.vorwahl1 = '$vorwahl'";		// Vorwahl schon vorhanden?
					$sql2 = "INSERT INTO vorwahl1 (vorwahl1) VALUES ('$vorwahl')";				            // Vorwahl neu
					$vorwahl1_id = id_ermitteln($sql1, $sql2, $conn);
				}
			}
					
		// Vorwahl2  --------------------------------------------------------------------------------------------------------------
			$vorwahl2_id = 1;

	   	// Bank ---------------------------------------------------------------------------------------------------------------------
			if (!empty($einzug)) {												// Feld bank ist leer
						$einzug = 1;													// Standard gesetzt
			}
		
		// Zeit ------------------------------------------------------------------------------------------------------------
			//if (empty($feld[14]) OR ($feld[14] == "TV")) {									// Feld Termin ist leer
			//	$zeit = "00:00";															// Feld Zeit = Standard
			//}
		
		// Gesellschaft alt -------------------------------------------------------------------------------------------------------
			if (empty($ges_alt)) {																// Feld gesellschaft ALT ist leer
				$produkt_alt_id = 1;															// Standard gesetzt
			}
			else {
				$ges_alt = trim($ges_alt);														// Whitespaces entfernent
				$sql1 = "SELECT ges_alt_id FROM ges_alt WHERE ges_alt.ges_alt = '$ges_alt'";	// gesellschaft ALT schon vorhanden?
				$sql2 = "INSERT INTO ges_alt (ges_alt) VALUES ('$ges_alt')";					// gesellschaft ALT neu
				$ges_alt_id = id_ermitteln($sql1, $sql2, $conn);
																										// auch in Tabelle ges_neu eintragen
				// Kombination Gesellschaft alt - Termin alt
				$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE ges_alt_id = '$ges_alt_id' AND tarif_alt_id = '1'";
				$sql2 = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_alt_id', '1')";
				$produkt_alt_id = id_ermitteln($sql1, $sql2, $conn);
				
				$sql1 = "SELECT ges_neu_id FROM ges_neu WHERE ges_neu.ges_neu = '$ges_alt'";	// gesellschaft NEU schon vorhanden?
				$sql2 = "INSERT INTO ges_neu (ges_neu) VALUES ('$ges_alt')";					// gesellschaft NEU neu
				$ges_id = id_ermitteln($sql1, $sql2, $conn);								// dummy-Variable, wird nicht benutzt
				
				// Kombination Gesellschaft neu - Termin neu
				$sql1 = "SELECT produkt_neu_id FROM produkt_neu WHERE ges_neu_id = '$ges_id' AND tarif_neu_id = '1'";
				$sql2 = "INSERT INTO produkt_neu (ges_neu_id, tarif_neu_id) VALUES ('$ges_id', '1')";
				$produkt_id = id_ermitteln($sql1, $sql2, $conn);									// dummy variable
			}
					
	   	// Gesellschaft neu --------------------------------------------------------------------------------------------------------
			if (empty($ges_neu)) {																	// Feld gesellschaft NEU ist leer
				$produkt_neu_id = 1;																// Standard gesetzt
			}
			else {
				$ges_neu = trim($ges_neu);															// Whitespaces entfernent
				$sql1 = "SELECT ges_neu_id FROM ges_neu WHERE ges_neu.ges_neu = '$ges_neu'";		// gesellschaft schon vorhanden?
				$sql2 = "INSERT INTO ges_neu (ges_neu) VALUES ('$ges_neu')";						// gesellschaft neu
				$ges_neu_id = id_ermitteln($sql1, $sql2, $conn);
																											// auch in Tabelle ges_alt eintragen
				// Kombination Gesellschaft alt - Termin alt
				$sql1 = "SELECT produkt_neu_id FROM produkt_neu WHERE ges_neu_id = '$ges_neu_id' AND tarif_neu_id = '1'";
				$sql2 = "INSERT INTO produkt_neu (ges_neu_id, tarif_neu_id) VALUES ('$ges_neu_id', '1')";
				$produkt_neu_id = id_ermitteln($sql1, $sql2, $conn);
				
				$sql1 = "SELECT ges_alt_id FROM ges_alt WHERE ges_alt.ges_alt = '$ges_neu'";		// gesellschaft schon vorhanden?
				$sql2 = "INSERT INTO ges_alt (ges_alt) VALUES ('$ges_neu')";						// gesellschaft neu
				$ges_id = id_ermitteln($sql1, $sql2, $conn);
				
				// Kombination Gesellschaft alt - Termin alt
				$sql1 = "SELECT produkt_alt_id FROM produkt_alt WHERE ges_alt_id = '$ges_id' AND tarif_alt_id = '1'";
				$sql2 = "INSERT INTO produkt_alt (ges_alt_id, tarif_alt_id) VALUES ('$ges_id', '1')";
				$produkt_id = id_ermitteln($sql1, $sql2, $conn);									// dummy variable
				
			}
		
		// TV --------------------------------------------------------------------------------------------------------------
			if (!empty($tv)) {														// Feld TV ist nicht leer
				$tv = trim($tv);												// Whitespaces entfernen

				$sql1 = "SELECT user_id FROM user WHERE user.user = '$tv'";			// User schon vorhanden?
				$sql2 = "INSERT INTO user (user, gruppen_id) VALUES ('$tv', '2')";	// User neu
					
				$aquise_id = id_ermitteln($sql1, $sql2, $conn);
			}
					
	   // Realis --------------------------------------------------------------------------------------------------------------
			if (!empty($realis)) {														// Feld Realis ist nicht leer
				$realis = trim($realis);												// Whitespaces entfernen
					
				$sql1 = "SELECT user_id FROM user WHERE user.user = '$realis'";			// User schon vorhanden?
				$sql2 = "INSERT INTO user (user, gruppen_id) VALUES ('$realis', '3')";	// User neu
					
				$bearb_id = id_ermitteln($sql1, $sql2, $conn);
			}
					
		//VoNr -----------------------------------------------------------------------------------------------------------------
			//if (empty($vonr)) {
			//	$feld[11] = 0;
			//}

		// Storno --------------------------------------------------------------------------------------------------------------
			//if (!empty($storno)) {
			//	$storno = "j";
			//}
			
//Kunde schon vorhanden? => Vorname, Name, Vorwahl, Telefon m�ssen stimmen - �berpr�fung auf neuer Termin
//*********************************************************************************************************************
		
			$sql = "SELECT kunden_id, rem_kunde FROM kunden  ";
			$sql .= " WHERE telefon = '$telefon' AND vorwahl1_id = '$vorwahl1_id' ";
			$sql .= " AND vorname_id = '$vorname_id' AND name_id = '$name_id' ";
			
			$abfrage = mysql_query($sql, $conn);
				if (mysql_errno()) 														// MySQL-Fehler
				echo mysql_errno() . ": " . mysql_error();
			
			if ($ergebnis = mysql_fetch_array($abfrage)) {								// Kunde schon vorhanden
				$kd_id = $ergebnis[0];													// Kunden-Id
			
			/*
			
			$sql = "SELECT kunden_id, rem_kunde ";
			$sql .= " FROM kunden, vorname, name, vorwahl1 ";
			$sql .= " WHERE kunden.telefon = '$telefon' AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id AND vorwahl1.vorwahl1 = '$vorwahl' ";
			$sql .= " AND kunden.vorname_id = vorname.vorname_id AND vorname.vorname = '$vorname' ";
			$sql .= " AND kunden.name_id = name.name_id AND name.name = '$name' ";
			
			$abfrage = mysql_query($sql, $conn);
				if (mysql_errno()) 														// MySQL-Fehler
				echo mysql_errno() . ": " . mysql_error();
			
			if ($ergebnis = mysql_fetch_array($abfrage)) {								// Kunde schon vorhanden
				$kd_id = $ergebnis[0];													// Kunden-Id
			
			
			
			$sql = "SELECT kunden_id, rem_kunde ";
			$sql .= " FROM kunden  ";
			//$sql .= " WHERE kunden.telefon = '$telefon' AND kunden.vorwahl1_id = vorwahl1.vorwahl1_id AND vorwahl1.vorwahl1 = '$vorwahl' ";
			$sql .= " WHERE vorname_id = '$vorname_id' AND name_id = '$name_id' AND poo_id = '$poo_id' AND strasse = '$strasse' ";
			
			$abfrage = mysql_query($sql, $conn);
				if (mysql_errno()) 														// MySQL-Fehler
				echo mysql_errno() . ": " . mysql_error();
			
			if ($ergebnis = mysql_fetch_array($abfrage)) {								// Kunde schon vorhanden
				$kd_id = $ergebnis[0];													// Kunden-Id
			*/																	

// Termin f�r vorhandenen Kunden einlesen
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

			echo "Kunde $kd_id schon vorhanden!&nbsp;&nbsp;";
			
		// Fall 1: Neukunde - nur evtl. Bemerkung anh�ngen ----------------------------------------------------------------------------------------------------
	
				if(empty($erledigt) AND (empty($ges_alt)) AND (empty($ges_neu)) AND (empty($termin)) AND (empty($zeit)) AND (empty($tv)) AND (empty($realis))) {				// Neukunde
								
					$bemerkung = $rem_kunde . " " . $ergebnis[1];
					
					$sql = "UPDATE kunden SET rem_kunde = '$bemerkung'  ";
					//$sql = "UPDATE kunden SET rem_kunde = '$bemerkung', vorwahl1_id = '$vorwahl1_id', telefon = '$telefon' ";
					$sql .= "WHERE kunden_id = '$kd_id'";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">Bestandskunde - Fehler bei Kd-Insert 1</font>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
					echo "&nbsp;&nbsp;&nbsp;Bestandskunde - Bemerkung angef�gt<br>";
				}
				
		// Ende Fall 1 - Neukunde
		
		
		// Fall 3: Kunde mit Abschluss ----------------------------------------------------------------------------------------------------------------------------
	
				elseif (!empty($ges_neu)) { 
				//elseif (!empty($erledigt) AND (!empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
					if (empty($tv)) {
						$tv = "";
					}
					
					if (empty($realis)) {
						$realis = "";
					}
					if (empty($erledigt)) {
						$erledigt = "0000-00-00";
					}
				
					$sql = "UPDATE kunden SET poo_id = '$poo_id', strasse = '$strasse', ";
					$sql .= " geboren = '$geboren', branche_id = '$branche_id', ";
					$sql .= " vertragskunde = '1', einzug = '$einzug', neukunde = '0' ";
					$sql .= " WHERE kunden_id = '$kd_id'";
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "Fehler bei Update Bestandskunde Fall 3: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= " von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, abschluss) ";
					$sql .= " VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= " '$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 3</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als Vertragskunde eingef�gt <br>";
				} 
	// Ende Fall 3
		
						
		// Fall 2: Wiedervorlage --------------------------------------------------------------------------------------------------------

				elseif($feld[14] == "TV" AND (!empty($termin)) AND (!empty($erledigt))) {					// TV im Zeitfeld = Wiedervorlage
				
					$sql = " SELECT termin_id, erledigt_date, termin, wiedervorlage_date, nichtkunde, abschluss, wiedervorlage ";
					$sql .= " FROM termin WHERE kd_id = '$kd_id' AND alt = '0'  ";				// nur nichtgeschlossene Termine abfragen
					$query = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei Termin Select- Fall2</font>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
				
					$termin_alt = mysql_fetch_array($query);
					
					if (empty($termin_alt[1])) {  // kein erledigt Datum vorhanden
					
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">kd_id: $kd_id - kein erledigt Datum - ignoriert</font><br>";
					
					}
					
					elseif ($termin_alt[1] > $erledigt) {			// der vorhandene Termin ist j�nger
					
						$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, wiedervorlage_date, vonr, storno, wiedervorlage, alt ) ";
						$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$vonr', '$storno', '1', '1' )";
						
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;<font color=\"#FF0000\">$kd_id: Fehler bei T-Insert Fall 2</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						
						echo "&nbsp;&nbsp;&nbsp;Bestandskunde - abgeschlossene Wiedervorlage eingef�gt <br>";
					}
					
					else {	// der neue Termin ist j�nger
					
						$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, wiedervorlage_date, vonr, storno, wiedervorlage ) ";
						$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$vonr', '$storno', '1' )";
						
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;<font color=\"#FF0000\">$kd_id: Fehler bei T-Insert Fall 2</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						
						// vorhandenen Wiedervorlage-Termin oder Nichtkunden-termin abschlie�en
						
						if ($termin_alt[4] == '1' OR $termin_alt[6] == '1' ) {
						
							$sql = "UPDATE termin SET alt = '1' ";
							$sql .= "WHERE termin_id = '$termin_alt[0]'";
						
							$daten = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "&nbsp;&nbsp;<font color=\"#FF0000\">$kd_id: Fehler bei T-Update Fall 2</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
							}
						}
						
						// Kunde updaten
						
						$sql = "UPDATE kunden SET wiedervorlage = '1', nichtkunde = '0', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "$kd_id: Fehler bei Kunden-Update " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						echo "&nbsp;&nbsp;&nbsp;Bestandskunde - neue Wiedervorlage eingef�gt, vorhandener Termin abgeschlossen <br>";
					}
				}
						
			// Fall 2-1
				elseif($feld[14] == "TV" AND (empty($termin)) AND (!empty($erledigt))) {					// TV im Zeitfeld = Wiedervorlage
				
					$sql = " SELECT termin_id, erledigt_date, termin, wiedervorlage_date, nichtkunde, abschluss, wiedervorlage ";
					$sql .= " FROM termin WHERE kd_id = '$kd_id' AND alt = '0'  ";				// nur nichtgeschlossene Termine abfragen
					$query = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei Termin Select- Fall2</font>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
				
					$termin_alt = mysql_fetch_array($query);
					
					if (empty($termin_alt[1])) {  // kein erledigt Datum vorhanden
					
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">kd_id: $kd_id - kein erledigt Datum - ignoriert</font><br>";
					
					}
					
					elseif ($termin_alt[1] > $erledigt) {			// der vorhandene Termin ist j�nger
					
						$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, wiedervorlage, alt ) ";
						$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1', '1' )";
						
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;<font color=\"#FF0000\">$kd_id: Fehler bei T-Insert Fall 2-1</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						
						echo "&nbsp;&nbsp;&nbsp;Bestandskunde - abgeschlossene Wiedervorlage eingef�gt <br>";
					}
					
					else {	// der neue Termin ist j�nger
					
						$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, wiedervorlage ) ";
						$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1' )";
						
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;<font color=\"#FF0000\">$kd_id: Fehler bei T-Insert Fall 2-1</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						
						// vorhandenen Wiedervorlage-Termin oder Nichtkunden-termin abschlie�en
						
						if ($termin_alt[4] == '1' OR $termin_alt[6] == '1' ) {
						
							$sql = "UPDATE termin SET alt = '1' ";
							$sql .= "WHERE termin_id = '$termin_alt[0]'";
						
							$daten = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "&nbsp;&nbsp;<font color=\"#FF0000\">$kd_id: Fehler bei T-Update Fall 2-1</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
							}
						}
						
						// Kunde updaten
						
						$sql = "UPDATE kunden SET wiedervorlage = '1', nichtkunde = '0', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "$kd_id: Fehler bei Kunden-Update " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						echo "&nbsp;&nbsp;&nbsp;Bestandskunde - neue Wiedervorlage eingef�gt, vorhandener Termin abgeschlossen <br>";
					}
					
				} 
	// ende Fall 2
	
		// Fall 3: Nichtkunde -----------------------------------------------------------------------------------------------------------------
	
				elseif (!empty($erledigt) AND (empty($termin)) AND (empty($zeit)) AND (empty($ges_neu))) { 
				
					$sql = " SELECT termin_id, erledigt_date, termin, wiedervorlage_date, nichtkunde, abschluss, wiedervorlage ";
					$sql .= " FROM termin WHERE kd_id = '$kd_id' AND alt = '0'  ";				// nur nichtgeschlossene Termine abfragen
					$query = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei Termin Select- Fall3</font>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
				
					$termin_alt = mysql_fetch_array($query);
					
					if (empty($termin_alt[1])) {  // kein erledigt Datum vorhanden
					
						// Kunde updaten
						
						$bemerkung = $rem_kunde . " " . $ergebnis[1];
						
						$sql = "UPDATE kunden SET nichtkunde = '1', neukunde = '0', belegt = '0', rem_kunde = '$bemerkung' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "$kd_id: Fehler bei Kunden-Update " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						echo "&nbsp;&nbsp;&nbsp;Bestandskunde - kein offener Termin - als Nichtkunde markiert <br>";
					
					}
					
					elseif ($termin_alt[1] >= $erledigt) {			// der vorhandene Termin ist j�nger
					
						$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, nichtkunde, alt ) ";
						$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1', '1' )";
						
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;<font color=\"#FF0000\">$kd_id: Fehler bei T-Insert Fall 3</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						
						echo "&nbsp;&nbsp;&nbsp;Bestandskunde - abgeschlossener Nichtkunde eingef�gt <br>";
						
						// Kunde updaten
						
						$sql = "UPDATE kunden SET nichtkunde = '1', neukunde = '0', belegt = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "$kd_id: Fehler bei Kunden-Update " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						echo "&nbsp;&nbsp;&nbsp;Bestandskunde - kein offener Termin - als Nichtkunde markiert <br>";
					}
					
					else {	// der neue Termin ist j�nger
					
						$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, nichtkunde ) ";
						$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1' )";
						
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;<font color=\"#FF0000\">$kd_id: Fehler bei T-Insert Fall 3</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						
						// vorhandenen Wiedervorlage-Termin oder Nichtkunden-termin abschlie�en
						
						if ($termin_alt[4] == '1' OR $termin_alt[6] == '1' ) {
						
							$sql = "UPDATE termin SET alt = '1' ";
							$sql .= "WHERE termin_id = '$termin_alt[0]'";
						
							$daten = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "&nbsp;&nbsp;<font color=\"#FF0000\">$kd_id: Fehler bei T-Update Fall 3</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
							}
						}
						
						// Kunde updaten
						
						$sql = "UPDATE kunden SET wiedervorlage = '0', nichtkunde = '1', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$daten = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "$kd_id: Fehler bei Kunden-Update " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						echo "&nbsp;&nbsp;&nbsp;Bestandskunde - als Nichtkunde markiert <br>";
					}
				} 
	// ende Fall 3
	
	// Fall 7: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif($feld[14] == "TV" AND (!empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
					$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kd_id'";
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Update</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
	
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$termin', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 7</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;Bestandskunde - $kd_id eine Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 7
	
		// Fall 8: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif(empty($feld[14]) AND (empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
					$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kd_id'";
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Update</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
	
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$heute', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 8<</font>br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;Bestandskunde - $kd_id eine Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 8
	
	// Fall 9: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif($feld[14] == "TV" AND (!empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (empty($tv)) AND (empty($realis))) { 
				
					$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kd_id'";
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Update</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
	
					$sql = "INSERT INTO termin (kd_id, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$termin', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 7</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;Bestandskunde - $kd_id eine Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 9
	
	// Fall 10: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif($feld[14] == "TV" AND (!empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (!empty($tv)) AND (empty($realis))) { 
				
					$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kd_id'";
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Update</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
	
					$sql = "INSERT INTO termin (kd_id, telefonist, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$termin', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 10<</font>br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;Bestandskunde - $kd_id eine Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 10
	
	// Fall 11: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif($feld[14] == "TV" AND (empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (empty($tv)) AND (empty($realis))) { 
				
					$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kd_id'";
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Update</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
	
					$sql = "INSERT INTO termin (kd_id, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$heute', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 8<</font>br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;Bestandskunde - $kd_id eine Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 10
	
		// Fall 5: laufender termin ---------------------------------------------------------------------------------------------------------------------------
					elseif (empty($erledigt) AND (!empty($termin)) AND (!empty($zeit)) AND ($zeit != "TV") AND (empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
						$muster = "/^\d{0,2}:\d{2,2}$/";	// Muster f�r Zeit: 1 oder 2 Ziffern, Doppelpunkt 2 Ziffern
						if(preg_match($muster, $feld[14])) {  // Test auf Zeit-Match: ok
						
							$sql = "UPDATE kunden SET belegt = '1', wiedervorlage = '0', neukunde = '0' ";
							$sql .= "WHERE kunden_id = '$kd_id'";
							$abfrage = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "Fehler bei Update Termin Fall 5: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
							}
	
							$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
							$sql .= "von, bis, produkt_neu_id, seit, rem_termin, vonr, storno) ";
							$sql .= "VALUES ('$termin', '$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
							$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$vonr', '$storno' )";
					
							$abfrage = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 5</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
							}
							echo "&nbsp;Bestandskunde - $kd_id - lfd. Termin eingef�gt <br>";
							
							
							
						
						
						} // ende Uhrzeit ist Ok
						else { // Mustercheck stimmt nicht
			
							echo "<font color=\"#FF0000\">Fehler bei Zeit-Match!</font><br>";
						}
					} 
	// ende Fall 5
	
	
	
	else {
	
		echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">Bestandskunde - unbekannter Datensatz!</font><br>";
	}

/*
				if($feld[14] == "TV") {
	// TV im Zeitfeld
	// Fall 1: kein erledigt und kein Termin
					if(empty($erledigt) AND (empty($termin))) {
	
						$sql = "INSERT INTO termin (zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, vonr, storno, wiedervorlage ) ";
						$sql .= "VALUES ('$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$heute', '$vonr', '$storno', '1' )";
					
						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Insert Termin Fall 1: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}

						$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Update Termin Fall 1: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					} 
	// ende Fall 1
	
	// Fall 2: erledigt und Termin vorhanden
	
				elseif (!empty($erledigt) AND (!empty($termin))) { 
	
					$sql = "INSERT INTO termin (zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, aquiriert, wiedervorlage_date, vonr, storno, wiedervorlage  ) ";
					$sql .= "VALUES ('$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$vonr', '$storno', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "Fehler bei Insert Termin Fall 2: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
					}
		
					$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kd_id'";
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "Fehler bei Update Termin Fall 2: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
					}
				} 
	// ende Fall 2
	
	// Fall 3: erledigt aber kein Termin
				elseif (!empty($erledigt) AND (empty($termin))) { 
	
					$sql = "INSERT INTO termin (zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, aquiriert, wiedervorlage_date, vonr, storno, wiedervorlage ) ";
					$sql .= "VALUES ('$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$heute', '$vonr', '$storno', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Insert Termin Fall 3: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
					}
		
					$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kd_id'";
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "Fehler bei Update Termin Fall 3: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
					}
				} 
	// Fall 3
				
	// Fall 4: kein erledigt aber Termin
				elseif (empty($erledigt) AND (!empty($termin))) { 
	
					$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, vonr, storno, wiedervorlage) ";
					$sql .= "VALUES ('$termin', '$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$termin', '$vonr', '$storno', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Insert Termin Fall 4: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
					}
		
					$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
					$sql .= "WHERE kunden_id = '$kd_id'";
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "Fehler bei Update Termin Fall 4: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
					}
				} 
	// Ende Fall 4
			}
	// Ende if TV im Zeitfeld
	
	// TV-Feld ist leer
			elseif (empty($feld[14])) {
	
	
	
	// Fall 6: erledigt und kein termin - Nichtkunde!!!
				elseif(!empty($erledigt) AND (empty($termin))) {
				
					if (empty($ges_neu)) {	
	
						$sql = "INSERT INTO termin (zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, nichtkunde ) ";
						$sql .= "VALUES ('$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1' )";

						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Insert Termin Fall 6: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}	
		
						$sql = "UPDATE kunden SET nichtkunde = '1', wiedervorlage = '0', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Update Termin Fall 6: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					}
					
					else {
					
						$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, aquiriert, erledigt_date, vonr, storno, abschluss ) ";
						$sql .= "VALUES ('$termin', '$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$vonr', '$storno', '1' )";

						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Insert Termin Fall 7-1: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}	
		
						$sql = "UPDATE kunden SET vertragskunde = '1', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Update Termin Fall 7-1: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					}
				}
	// ende Fall 6
	
	// Fall 7 + 8: erledigt und termin
	
				elseif(!empty($erledigt) AND (!empty($termin))) {
				
					if (empty($ges_neu)) {		// Fall 8 - Wiedervorlage - kein Vertragsabschluss
					
						$sql = "INSERT INTO termin (zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, aquiriert, wiedervorlage_date, vonr, storno, wiedervorlage ) ";
						$sql .= "VALUES ('$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$vonr', '$storno', '1' )";

						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Insert Termin Fall 8: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}	
		
						$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Update Termin Fall 8: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					}			// Ende Fall 8: kein Vertragsabschluss
					else {		// Fall 7: erledigt und termin und Ges.Neu - Vertragskunde

						$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, aquiriert, erledigt_date, vonr, storno, abschluss ) ";
						$sql .= "VALUES ('$termin', '$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$vonr', '$storno', '1' )";

						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Insert Termin Fall 7: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}	
		
						$sql = "UPDATE kunden SET vertragskunde = '1', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Update Termin Fall 7: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					}
				}
	// ende Fall 7 + 8
	
	// Fall 9: kein erledigt, aber termin
				elseif(empty($erledigt) AND (!empty($termin))) {
	
					if (!empty($rem_kunde)) {
		
						$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, vonr, storno, wiedervorlage ) ";
						$sql .= "VALUES ('$termin', '$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$termin', '$vonr', '$storno', '1' )";

						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Insert Termin Fall 9: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}	
		
						$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Update Termin Fall 9: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					}		
				}
	// ende Fall 9
			}
	// ende elseif kein TV im Zeitfeld
	
	// im TV-Feld steht eine Zeit
		else {
			$muster = "/^\d{0,2}:\d{2,2}$/";	// Muster f�r Zeit: 1 oder 2 Ziffern, Doppelpunkt 2 Ziffern
			if(preg_match($muster, $feld[14])) {  // Test auf Zeit-Match: ok
			
				if (empty($termin)) { 
					echo "Fehler bei Zeile $datensatz: Termin fehlt, aber Zeit vorhanden";
				}
				else { // Termin und Zeit ok
			
					if ($heute >= $termin) {  // abgeschlossener Termin!!
					
	// Fall 11 - kein Vertragsabschluss
						if (empty($ges_neu)) { // Termin, aber kein Vertragsabschluss
						
							$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
							$sql .= "von, bis, produkt_neu_id, seit, rem_termin, aquiriert, erledigt_date, wiedervorlage_date, vonr, storno, wiedervorlage ) ";
							$sql .= "VALUES ('$termin', '$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
							$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$termin', '$vonr', '$storno', '1' )";

							$abfrage = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "Fehler bei Insert Termin Fall 11: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
							}	
		
							$sql = "UPDATE kunden SET wiedervorlage = '1', neukunde = '0' ";
							$sql .= "WHERE kunden_id = '$kd_id'";
							$abfrage = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "Fehler bei Update Termin Fall 11: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
							}
						} // ende if kein abschluss
	// ende fall 11: kein vertragsabschluss
	
	// Fall 10: vertragsabschluss
						
						else { // Gesellschaft neu - Vertragskunde
						
							$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
							$sql .= "von, bis, produkt_neu_id, seit, rem_termin, aquiriert, erledigt_date, vonr, storno, abschluss ) ";
							$sql .= "VALUES ('$termin', '$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
							$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$vonr', '$storno', '1' )";

							$abfrage = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "Fehler bei Insert Termin Fall 10: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
							}	
		
							$sql = "UPDATE kunden SET vertragskunde = '1', wiedervorlage = '0', neukunde = '0' ";
							$sql .= "WHERE kunden_id = '$kd_id'";
							$abfrage = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "Fehler bei Update Termin Fall 10: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
							}
						} // ende else Vertragskunde
	// ende fall 10
	
					} // ende if abgeschlossener Termin
					
	// Fall 12: laufender termin
					else { // laufender Termin
						$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, aquiriert, vonr, storno) ";
						$sql .= "VALUES ('$termin', '$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno' )";

						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Insert Termin Fall 12: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}	
		
						$sql = "UPDATE kunden SET belegt = '1', wiedervorlage = '0', neukunde = '0' ";
						$sql .= "WHERE kunden_id = '$kd_id'";
						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "Fehler bei Update Termin Fall 12: Zeile $datensatz: " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					} // ende else laufender termin
	// ende fall 12
	
				} // ende Termin und Zeit ok
			} // Test auf Zeit-Match: ok
			else { // Mustercheck stimmt nicht
			
				echo "Fehler bei Zeit-Match: Zeile $datensatz:";
			}
		} // Ende else im TV-Feld steht eine Zeit
		
*/

		
	} // ende Kunde schon vorhanden

	
else { // neuer Kunde
//*********************************************************************************************************************

	// Fall 1: Neukunde ---------------------------------------------------------------------------------------------------------
	
				if(empty($erledigt) AND (empty($ges_alt)) AND (empty($ges_neu)) AND (empty($termin)) AND (empty($zeit)) AND (empty($tv)) AND (empty($realis))) {				// Neukunde
								
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, neukunde, rem_kunde) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1', '$rem_kunde')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert </font>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id eingef�gt<br>";
				}
				
	// Ende Fall 1 - Neukunde
	
	// Fall 2: Wiedervorlage --------------------------------------------------------------------------------------------------------

				elseif($feld[14] == "TV" AND (!empty($termin)) AND (!empty($erledigt))) {					// TV im Zeitfeld = Wiedervorlage
					
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, wiedervorlage) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert </font>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
					
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, wiedervorlage_date, vonr, storno, wiedervorlage ) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$vonr', '$storno', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 2</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id und Wiedervorlage-Termin eingef�gt <br>";
				} 
				
				// Fall 2-1
					elseif($feld[14] == "TV" AND (empty($termin)) AND (!empty($erledigt))) {					// TV im Zeitfeld = Wiedervorlage
					
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, wiedervorlage) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "<font color=\"#FF0000\">&nbsp;&nbsp;&nbsp;neuer Kunde - Fehler bei Kd-Insert </font>" . mysql_errno() . ": " . mysql_error() . "</font><br>";
						}
					$kd_id = mysql_insert_id();
					
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, wiedervorlage ) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 2-1</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id und Wiedervorlage-Termin eingef�gt <br>";
				} 
	// ende Fall 2
	
	// Fall 3: Nichtkunde -----------------------------------------------------------------------------------------------------------------
	
				elseif (!empty($erledigt) AND (empty($termin)) AND (empty($zeit)) AND (empty($ges_neu))) { 
				
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, nichtkunde) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
	
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, nichtkunde ) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 3</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als Nichtkunde eingef�gt <br>";
				} 
	// ende Fall 3
				
	// Fall 4: Kunde mit Abschluss ----------------------------------------------------------------------------------------------------------------------------
	
				elseif (!empty($ges_neu)) { 
				//elseif (!empty($erledigt) AND (!empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
					if (empty($tv)) {
						$tv = "";
					}
					
					if (empty($realis)) {
						$realis = "";
					}
					if (empty($erledigt)) {
						$erledigt = "0000-00-00";
					}
				
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, vertragskunde) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
	
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, abschluss) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 4</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als Vertragskunde eingef�gt <br>";
				} 
	// Ende Fall 4
			
	// Fall 5: laufender termin ---------------------------------------------------------------------------------------------------------------------------
					elseif (empty($erledigt) AND (!empty($termin)) AND (!empty($zeit)) AND ($zeit != "TV") AND (empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
						$muster = "/^\d{0,2}:\d{2,2}$/";	// Muster f�r Zeit: 1 oder 2 Ziffern, Doppelpunkt 2 Ziffern
						if(preg_match($muster, $feld[14])) {  // Test auf Zeit-Match: ok
						
							$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
							$sql .= "geboren, einzug, belegt) ";
							$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
							$sql .= "'$geboren', '$einzug', '1')";
					
							$abfrage = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
							}
							$kd_id = mysql_insert_id();
	
							$sql = "INSERT INTO termin (termin, zeit, kd_id, telefonist, aussendienst, produkt_alt_id, ";
							$sql .= "von, bis, produkt_neu_id, seit, rem_termin, vonr, storno) ";
							$sql .= "VALUES ('$termin', '$zeit', '$kd_id', '$tv', '$realis','$produkt_alt_id', ";
							$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$vonr', '$storno' )";
					
							$abfrage = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 5</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
							}
							echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als lfd. Termin eingef�gt <br>";
						
						} // ende Uhrzeit ist Ok
						else { // Mustercheck stimmt nicht
			
							echo "<font color=\"#FF0000\">Fehler bei Zeit-Match!</font><br>";
						}
					} 
	// ende Fall 5
	
	// Fall 6: Termin ohne erfolgreichen Abschluss----------------------------------------------------------------------------------------------------------
	
				elseif (!empty($erledigt) AND (!empty($termin)) AND (empty($zeit)) AND (empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
	
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, termin, vonr, storno) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$termin', '$vonr', '$storno')";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 6</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als ToA eingef�gt <br>";
				} 
	// Ende Fall 6
	
		// Fall 7: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif($feld[14] == "TV" AND (!empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, wiedervorlage) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
	
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$termin', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 7</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 7
	
		// Fall 8: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif(empty($feld[14]) AND (empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, wiedervorlage) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
	
					$sql = "INSERT INTO termin (kd_id, telefonist, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$heute', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 8<</font>br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 8
	
	// Fall 9: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif($feld[14] == "TV" AND (!empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (empty($tv)) AND (empty($realis))) { 
				
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, wiedervorlage) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
	
					$sql = "INSERT INTO termin (kd_id, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$termin', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 7</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 9
	
	// Fall 10: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif($feld[14] == "TV" AND (!empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (!empty($tv)) AND (empty($realis))) { 
				
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, wiedervorlage) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
	
					$sql = "INSERT INTO termin (kd_id, telefonist, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$tv', '$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$termin', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 10<</font>br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 10
	
	// Fall 11: Wiedervorlage ----------------------------------------------------------------------------------------------------------------------------
	
				elseif($feld[14] == "TV" AND (empty($termin)) AND (empty($erledigt)) AND (empty($ges_neu)) AND (empty($tv)) AND (empty($realis))) { 
				
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, wiedervorlage) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
	
					$sql = "INSERT INTO termin (kd_id, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, wiedervorlage_date, wiedervorlage) ";
					$sql .= "VALUES ('$kd_id', '$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$heute',  '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 8<</font>br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als Wiedervorlage eingef�gt <br>";
				} 
	// Ende Fall 10
	
	// Fall 12: Termin ohne erfolgreichen Abschluss----------------------------------------------------------------------------------------------------------
	
				elseif (!empty($erledigt) AND (empty($termin))  AND (!empty($zeit)) AND ($zeit != "TV")  AND (empty($ges_neu)) AND (!empty($tv)) AND (!empty($realis))) { 
				
					$muster = "/^\d{0,2}:\d{2,2}$/";	// Muster f�r Zeit: 1 oder 2 Ziffern, Doppelpunkt 2 Ziffern
					if(preg_match($muster, $feld[14])) {  // Test auf Zeit-Match: ok
				
						$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
						$sql .= "geboren, einzug) ";
						$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
						$sql .= "'$geboren', '$einzug')";
					
						$abfrage = mysql_query($sql, $conn);
							if (mysql_errno()) { 
								echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
							}
						$kd_id = mysql_insert_id();
	
						$sql = "INSERT INTO termin (kd_id, zeit, telefonist, aussendienst, produkt_alt_id, ";
						$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno) ";
						$sql .= "VALUES ('$kd_id', '$zeit', '$tv', '$realis','$produkt_alt_id', ";
						$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno')";
					
						$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 12</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
						}
						echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als ToA eingef�gt <br>";
						
					} // ende Uhrzeit ist Ok
					else { // Mustercheck stimmt nicht
			
						echo "<font color=\"#FF0000\">Fehler bei Zeit-Match!</font><br>";
					}
				} 
	// Ende Fall 12
	
	// Fall 13: Kunde mit Abschluss ----------------------------------------------------------------------------------------------------------------------------
	
				elseif (!empty($erledigt) AND (!empty($ges_neu)) AND (empty($tv)) AND (!empty($realis))) { 
				
					$sql = "INSERT INTO kunden (vorname_id, name_id, poo_id, strasse, vorwahl1_id, telefon, vorwahl2_id, branche_id, ";
					$sql .= "geboren, einzug, vertragskunde ) ";
					$sql .= "VALUES ('$vorname_id', '$name_id', '$poo_id', '$strasse', '$vorwahl1_id', '$telefon', '$vorwahl2_id', '$branche_id', ";
					$sql .= "'$geboren', '$einzug', '1')";
					
					$abfrage = mysql_query($sql, $conn);
						if (mysql_errno()) { 
							echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - Fehler bei Kd-Insert</font> " . mysql_errno() . ": " . mysql_error() . "<br>";
						}
					$kd_id = mysql_insert_id();
	
					$sql = "INSERT INTO termin (kd_id, aussendienst, produkt_alt_id, ";
					$sql .= "von, bis, produkt_neu_id, seit, rem_termin, erledigt_date, vonr, storno, abschluss) ";
					$sql .= "VALUES ('$kd_id', '$realis','$produkt_alt_id', ";
					$sql .= "'$von', '$bis', '$produkt_neu_id', '$seit', '$rem_kunde', '$erledigt', '$vonr', '$storno', '1' )";
					
					$abfrage = mysql_query($sql, $conn);
					if (mysql_errno()) { 
						echo "&nbsp;&nbsp;<font color=\"#FF0000\">Fehler bei T-Insert Fall 13</font><br>" . mysql_errno() . ": " . mysql_error() . "<br>";
					}
					echo "&nbsp;&nbsp;&nbsp;neuer Kunde - $kd_id als Vertragskunde eingef�gt <br>";
				} 
	// Ende Fall 13
	
	// unbekannter Datensatz ------------------------------------------------------------------------------------------------------------------------------
	
					else {
					
						echo "&nbsp;&nbsp;&nbsp;<font color=\"#FF0000\">neuer Kunde - unbekannter Datensatz!</font><br>";
					
					}
	} // ende else neuer Kunde
} // Ende FOR-Schleife - Datei komplett verarbeitet
		
		echo "$datensatz Datens�tze eingef�gt.";
		echo "<br><br>FERTIG!";
		exit();

} // Ende ISSET(Senden)

//Konvertiere-Button noch nicht gedr�ckt
else	// Senden nicht gedr�ckt
{
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><title>Datenbank-Konverter</title></head>
<body>

<table>
<tr><td><b>Script zur Konvertierung der Datenbanken der Fa. Zschornack</b></td></tr>
<tr><td>Der Name des zu konvertierenden Scriptes muss sein: convert.txt</td></tr>
<tr><td>Dateiformat des zu konvertierenden Scriptes muss sein: txt, tabstop getrennt</td></tr>
<tr><td>Speicherort der Datei convert.txt: im Ordner des Scriptes</td></tr>
<tr><td bgcolor = "red" align = "center"><br>&nbsp;&nbsp;<strong>Achtung! Anzahl der Spalten (Vorwahl) kontrollieren!!&nbsp;&nbsp;</strong><br><br></td></tr>
<tr><td>
<form action="<?php $_SERVER['PHP_SELF']?>" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr><td class="werte"><p><input name="senden" type="submit" value="Konvertiere!"></p></td></tr>          
</table>
</form>
</td></tr>
</table>
<?php
}
?>

</body>
</html>
