<!DOCTYPE >
<html lang="de">
<head>
<title>Schon eingeloggt!</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/preisagentur.css" rel="stylesheet" type="text/css" />

    <style>
        #loginform, #logfailform, #logoutform, #logfailform, #loggedinform {
            background: #ddd;
            border: 1px solid #666;
            font-size: 1.4em;
            color: #333;
            position: absolute;
            width: 400px;
            height: 260px;
            top: 50%;
            left: 50%;
            margin-top: -130px;
            margin-left: -200px;
            padding: 20px;
            border-radius: 10px;
        }

        #logfailform {
            text-align: center;
            border: 8px solid green;
        }

        #logoutform {
            text-align: center;
            border: 8px solid red;
        }

        #logfailform, #loggedinform {
            text-align: center;
            border: 8px solid orange;
        }

        .innerdiv {
            background-color: white;
            height: 100%;
        }

        input[type="text"], input[type="password"] {
            font-size: 1.3em; padding: 5px 12px;
            font-family: Arial, sans-serif;
            font-weight: 300;
            color: teal;
            border: 1px solid silver;
            background-image: linear-gradient(to top, gainsboro 0%, white 90%);
            border-radius: 5px;
            margin-left: 1em;
        }

        input[type="submit"].login {
            font-size: 1.3em; padding: 5px 12px;
            font-family: Arial, sans-serif;
            font-weight: 300;
            color: teal;
            border: 1px solid silver;
            background-image: linear-gradient(to top, gainsboro 0%, white 90%);
            border-radius: 10px;
        }

        label { display: inline-block; width: 7em; }

        .submitlogin {
            margin-left: 9em;
            margin-top: 1em;
        }

        .inputlogin {
            margin-bottom: 1em;
        }

        #einloggen {
            margin-top: 2em;
        }

        #logfailform h3, #logfailform h3 {
            margin-top: 0;
            line-height: 150%;
        }

        .back {
            width: 80%;
            margin: 0 auto;
            border: 1px solid silver;
            background-image: linear-gradient(to top, gainsboro 0%, white 90%);
            border-radius: 10px;
        }

        .back a{
            font-size: 2em;
            padding: 5px 12px;
            font-family: Arial, sans-serif;
            font-weight: 300;
            color: teal;
        }

        .back a:hover{
            font-size: 2em;
            padding: 5px 12px;
            font-family: Arial, sans-serif;
            font-weight: 300;
            color: orange;
        }
    </style>
</head>
<body>


<div id="loggedinform">
<div class="innerdiv">
  <h3>Sie sind bereits eingeloggt!</h3>
  <p>Wenden Sie sich bitte an den Administrator!</p>
   </div>
  

</div> <!-- ende loginform -->

</body>
</html>




