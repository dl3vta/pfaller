<?php
session_start();								// darf kein html-code davorstehen!!
include ("include/mysqlconnect.inc.php");
include ("include/variablen.php");
include ("include/funktionen.php");

//include ("include/init.php");

sessiondauer();

// Zeit bis zum »timeout« in Sekunden
$session_timeout = 36000; // 36000 Sek./60 Sek. = 10 Stunden

// Variable last_visit ist noch nicht vorhanden - wird neu gesetzt
if (!isset($_SESSION['last_visit'])) {
	$_SESSION['last_visit'] = time();
}

// variable last visit ist älter als variable timeout -> session wird zerstört
if((time() - $_SESSION['last_visit']) > $session_timeout) {
	session_destroy();
	// Aktion der Session wird erneut ausgeführt
}

//variable last vosit wird gesetzt
$_SESSION['last_visit'] = time();





error_reporting(E_ALL & ~E_NOTICE);       //alle Fehler ABER KEINE alle Notizen
//error_reporting(E_ALL);                     //alle Fehler UND alle Notizen

if (isset($_POST['login'])) $login = $_POST['login'];			// Formular wurde abgeschickt

//Formularvariablen werden initialisiert
if (isset($_POST['name'])) {$name = quote_smart($_POST['name']);} else { $name = '';}
if (isset($_POST['user'])) {$user = quote_smart($_POST['user']);} else { $user = '';}
if (isset($_POST['pwd'])) {$pwd = quote_smart($_POST['pwd']);} else { $pwd = '';}


//Login-Button gedrückt
if (isset($login)) {

	$pwd = md5($pwd); 												//Passwort verschlüsseln

	$indate = date("Ymd");
	$intime = date("H:i");
	$sessionid = session_id();
	$benutzer = $user;

	// Test, ob schon eingeloggt

	$sql = "SELECT user FROM logins ";
	$sql .= "WHERE user = '$benutzer' AND indate = '$indate' AND outdate = '0'";
	//$abfrage = myquery($sql, $conn);
	$abfrage = mysqli_query($db, $sql);
	$daten = mysqli_fetch_array($abfrage);

	if ($daten) {												// schon eingeloggt

		//header("Location: loggedin.php");

		echo "<script>location.href='loggedin.php'</script>";

		exit();
	} // ende if schon eingeloggt
	else {  									// noch nicht eingeloggt

		$sql  = "SELECT vorname, gruppen, user_id FROM vorname, name, user, gruppen ";
		$sql .= " where (user = '$user') AND (gruppen.gruppen_id = user.gruppen_id)";
		$sql .= "AND pwd = '$pwd' AND vorname.vorname_id = user.vorname_id AND name.name_id = user.name_id ";
		$sql .= "AND name.name = '$name'";
		//$output = myquery($sql, $conn);
		$output = mysqli_query($db, $sql);
		$daten = mysqli_fetch_array($output);
		if (!$daten) { 													    	// MySQL-Fehler
			echo "<script>location.href='login_failed.php'</script>";
		}

		//jetzt test auf Gruppe!!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		switch($daten["gruppen"]) {
			case 'Administrator':																	//Administrator

				$sql = "INSERT INTO logins (user, indate, intime, session_id) VALUES ('$user', '$indate', '$intime', '$sessionid')";
				$output = mysqli_query($db, $sql);

				// Session-Variablen schreiben

				$_SESSION['benutzer_kurz'] = $_POST['user'];									//Session wird erstellt
				$_SESSION['benutzer_id'] = $daten["user_id"];
				$_SESSION['benutzer_name'] = $_POST['name'];
				$_SESSION['benutzer_vorname'] = $daten["vorname"];
				$_SESSION['benutzer_gruppen'] = $daten["gruppen"];

				echo "<script>location.href='admin/admin_start.php?SID=$sessionid'</script>";

				break;
			case 'Aquise':																	//Telefonist

				$sql = "INSERT INTO logins (user, indate, intime, session_id) VALUES ('$user', '$indate', '$intime', '$sessionid')";
				$output = mysqli_query($db, $sql);

				// Session-Variablen schreiben

				$_SESSION['benutzer_kurz'] = $_POST['user'];									//Session wird erstellt
				$_SESSION['benutzer_id'] = $daten["user_id"];
				$_SESSION['benutzer_name'] = $_POST['name'];
				$_SESSION['benutzer_vorname'] = $daten["vorname"];
				$_SESSION['benutzer_gruppen'] = $daten["gruppen"];

				echo "<script>location.href='telefon/telefon_start.php?SID=$sessionid'</script>";
				break;
			case 'Aussendienst':																	//Aussendienstler

				$sql = "INSERT INTO logins (user, indate, intime, session_id) VALUES ('$user', '$indate', '$intime', '$sessionid')";
				$output = mysqli_query($db, $sql);

				// Session-Variablen schreiben

				$_SESSION['benutzer_kurz'] = $_POST['user'];									//Session wird erstellt
				$_SESSION['benutzer_id'] = $daten["user_id"];
				$_SESSION['benutzer_name'] = $_POST['name'];
				$_SESSION['benutzer_vorname'] = $daten["vorname"];
				$_SESSION['benutzer_gruppen'] = $daten["gruppen"];

				echo "<script>location.href='aussen/aussen_start.php?SID=$sessionid'</script>";
				break;
		} // ende switch
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//}
	} // Ende else noch nicht eingeloggt
} // ende if (isset($login))
?>

<!-- Hier fängt das HTML-Dokument an -->
<!DOCTYPE html>
<html lang="de">
<head>
	<title>Preisagentur: Login</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="css/preisagentur.css" rel="stylesheet" type="text/css" />

	<style>
	#loginform, #logfailform, #logoutform, #logfailform, #loggedinform {
	background: #ddd;
	border: 1px solid #666;
	font-size: 1.4em;
	color: #333;
	position: absolute;
	width: 400px;
	height: 260px;
	top: 50%;
	left: 50%;
	margin-top: -130px;
	margin-left: -200px;
	padding: 20px;
	border-radius: 10px;
	}

	#logfailform {
	text-align: center;
	border: 8px solid green;
	}

	#logoutform {
	text-align: center;
	border: 8px solid red;
	}

	#logfailform, #loggedinform {
	text-align: center;
	border: 8px solid orange;
	}

	.innerdiv {
	background-color: white;
	height: 100%;
	}

	input[type="text"], input[type="password"] {
	font-size: 1.3em; padding: 5px 12px;
	font-family: Arial, sans-serif;
	font-weight: 300;
	color: teal;
	border: 1px solid silver;
	background-image: linear-gradient(to top, gainsboro 0%, white 90%);
	border-radius: 5px;
	margin-left: 1em;
	}

	input[type="submit"] {
	font-size: 1.3em; padding: 5px 12px;
	font-family: Arial, sans-serif;
	font-weight: 300;
	color: teal;
	border: 1px solid silver;
	background-image: linear-gradient(to top, gainsboro 0%, white 90%);
	border-radius: 10px;
	}

	input[type="submit"]:hover {
		color: black;
		border: 1px solid black;
	}

	label { display: inline-block; width: 7em; }

	.submitlogin {
	margin-left: 9em;
	margin-top: 1em;
	}

	.inputlogin {
	margin-bottom: 1em;
	}

	#einloggen {
	margin-top: 2em;
	}

	#logfailform h3, #logfailform h3 {
	margin-top: 0;
	line-height: 150%;
	}

	.back {
	width: 80%;
	margin: 0 auto;
	border: 1px solid silver;
	background-image: linear-gradient(to top, gainsboro 0%, white 90%);
	border-radius: 10px;
	}

	.back a{
	font-size: 2em;
	padding: 5px 12px;
	font-family: Arial, sans-serif;
	font-weight: 300;
	color: teal;
	}

	.back a:hover{
	font-size: 2em;
	padding: 5px 12px;
	font-family: Arial, sans-serif;
	font-weight: 300;
	color: orange;
	}
	</style>


</head>
<body onload="self.focus();document.login.name.focus()">			<!-- login.name = Name des Formulars.Name des Feldes -->


<div id="loginform">
  <h3>Login - Bitte Benutzerdaten eingeben:</h3>
  
  <form id="einloggen" name="login" method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
  
  	<div class="inputlogin"><label for="name" placeholder="">Name</label> 
    	<!--<input id="name" readonly type="text" onfocus="if (this.hasAttribute('readonly')) {
    			this.removeAttribute('readonly');
    				// fix for mobile safari to show virtual keyboard
    			this.blur();    this.focus();  }" /> -->
    
    
    <input name="name" type="text" autocomplete="Name"/>
    
    </div>
    
    <div class="inputlogin"><label for="user"  placeholder="">Benutzername</label> 
    	<!--<input id="user" readonly type="text" onfocus="if (this.hasAttribute('readonly')) {
    			this.removeAttribute('readonly');
    				// fix for mobile safari to show virtual keyboard
    			this.blur();    this.focus();  }" /> -->
    
    
    <input name="user" type="text" autocomplete="Benutzer"/> 
    
    </div>
    
    <div class="inputlogin"><label for="pwd">Passwort</label> <input name="pwd" type="password" autocomplete="Passwort"></div>
    
	<div  class="submitlogin"><input type="submit" name="login" value="Einloggen" /></div>
  
  </form>
</div> <!-- ende loginform -->


</body>
</html>
